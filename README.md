# <img alt="adfsee" src="static/adfsee.png" width="30px"/> ADFSee

![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg?maxAge=1)

*ADFSee* is an application for **quick inspection of [Allotrope Data Format (ADF)](https://www.allotrope.org/) files** and graph data.

Target audience of *ADFSee* are developers who quickly need to **understand data in ADF files**.

*ADFSee* provides different approaches of visualizing graph data such as:
*  networks with various layouts powered by [cytoscape](http://js.cytoscape.org/),
*  networks with various layouts powered by [vis.js](http://visjs.org/)
*  networks with various layouts powered by [3d force graph](https://github.com/vasturiano/3d-force-graph)
*  networks with layouting powered by [pixi](https://github.com/pixijs/pixi.js)/WebGL
*  networks with layouting powered by [stardust](https://stardustjs.github.io/)/WebGL/GPU
*  networks with clustered layout powered by [t-SNE](https://github.com/karpathy/tsnejs)
*  networks with planar layout powered by [biofabric](http://www.biofabric.org/)
*  graph exploration powered by [ontodia.js](https://github.com/metaphacts/ontodia)
*  different representations as textual trees
*  query ADF data description with [yasgui](https://github.com/TriplyDB/Yasgui)

There are **viewers** for data stored in the **data package** as well as for **data cubes** (peak table view). *ADFSee* supports **extraction of data** from **data package** and **data description**. General metadata about an ADF file itself can be inspected in the **ADF info view** (e.g. checksum, api version). *ADFSee* also allows detailed inspection of **audit trail data**.

Additionally, *ADFSee* offers an embedded [Fuseki](https://jena.apache.org/documentation/fuseki2/) triple store for querying RDF graph data ([see below](#triplestore)).

## [User Manual](https://allotrope-open-source.gitlab.io/adfsee/)

some screenshots |     |     |
--- | --- | --- |
<img alt="chromatogram" src="static/cube-example.png" width="600px"/><br/><img alt="table" src="static/table-example.png" width="600px"/> | <img alt="dagre" src="static/dagre-example.png" width="300px"/>         | <img alt="klay" src="static/klay-example.png" width="400px"/>       |
<img alt="graphviz" src="static/gviz-example.png" width="400px"/>                                                                      | <img alt="tree" src="static/tree-example.png" width="400px"/>           | <img alt="audit" src="static/sunburst-example.png" width="400px"/>  |
<img alt="datadescription" src="static/dd-example.png" width="280px"/>                                                                 | <img alt="cytoscape" src="static/cytoscape-example.png" width="400px"/> | <img alt="vis" src="static/vis-ex.png" width="400px"/>              |
<img alt="vis" src="static/vis-tree.png" width="600px"/>                                                                               | <img alt="vis" src="static/vis-f1.png" width="400px"/>                  | <img alt="vis" src="static/vis-f2.png" width="400px"/>              |
<img alt="viz-tree" src="static/vis-tree-top.png" width="600px"/>                                                                      | <img alt="texttree" src="static/texttree-example.png" width="400px"/>   | <img alt="treemap" src="static/treemap-example.png" width="400px"/> |
<img alt="scale" src="static/scale-by-degree.png" width="600px"/>                                                                      | <img alt="3d" src="static/3d.png" width="400px"/>                       | <img alt="VR" src="static/vr.png" width="400px"/>                   |
<img alt="ontodia" src="static/ontodia.png" width="600px"/>                                                                            | <img alt="fuseki" src="static/fuseki.png" width="400px"/>       | <img alt="stardust" src="static/stardust-example.png" width="600px"/>|
<img alt="pixi" src="static/pixi-example.png" width="400px"/>                                                                          | <img alt="aft-stardust" src="static/AFT-REC-2020-03.png" width="400px"/> | <img alt="biofabric" src="static/biofabric.png" width="400px"/>|

*ADFSee* is a Spring Boot web application. Extension with new views or features is straightforward.

## How to get *ADFSee*?
There are two options: getting an executable package or building your own.

### Obtain prebuilt version:
You need access to file repository of Allotrope ([join Allotrope](https://www.allotrope.org/join-us)).
*  Download [archive from file repository](https://highq.in/8eq2zsln4y).
*  Unzip package
*  Execute `start-adfsee.bat` by double-clicking
*  Open `http://localhost:8181` in your local Chrome browser

### Build from scratch:
In order to build *ADFSee* you can execute following steps:
*  Check out this repository
*  Get [adf.repackaged-1.5.0-shaded.jar from file repository](https://highq.in/7m0mx4nsud) (Please refer to [Allotrope](https://www.allotrope.org) in order to find out how to get access to ADF libraries)
*  Copy adf.repackaged-1.5.0-shaded.jar to src/main/resources/lib/
*  Run `maven package`
*  Go to folder target
*  Execute from command line: `java -jar ./adfsee-x.y.z.war` (replace x.y.z with correct version numbers)
*  Open `http://localhost:8181` in your local [Chrome](https://www.google.com/chrome/) browser

## What happens to my uploaded data files?
*ADFSee* is a client/server application. Client and server are both running on your local machine which means that inspected ADF files always remain on your computer and stay under your control. During processing of an ADF file it will be copied ("uploaded") to a temporary folder on your local computer. After processing is complete all temporary files are deleted.

## What are system requirements?
*ADFSee* server requires Windows7+, *ADFSee* client requires a Chrome browser version 60+. Other browsers are not well tested and not well supported.<br/>
Currently, *ADFSee* supports 2 different file formats: .adf and .ttl files. Alternatively, you can specify a SPARQL endpoint and a graph URL in order to retrieve your data.

## How does it work?
Once the *ADFSee* is running, navigate to `http://localhost:8181` in your local Chrome browser. Click `Select data` at the right edge of the browser window. You can select one or more data files to be uploaded for visualization. Either click into the upper upload box in order to bring up a file browser or drop your data files directly into the upper upload box. If needed you can provide additional data files containing vocabulary. These additional files have to be dropped into the lower upload box. Alternatively, you can specify a SPARQL endpoint where to get your graph data from. In this case check the checkbox to query an endpoint and enter URL of SPARQL endpoint in the textbox below (optionally enter an URL of a named graph, otherwise the default graph is used). Optionally, you can provide a SPARQL construct expression in the corresponding text area to be applied to your data. <br/>
Click button `See!` to start processing uploaded data. Duration of processing depends on size of your graph model.

## How to view graph from SPARQL endpoint?
<ol>
	<li>Open sidebar by clicking "Select data" at right border of browser window.</li>
	<li>Check checkbox to query an endpoint.</li>
	<li>Enter URL of SPARQL endpoint in textbox below, e.g. http://localhost:3030/dataset/sparql</li>
	<li>Optionally enter URL of a named graph that is to be queried or leave unchanged in order to query default graph.</li>
	<li>Optionally enter a construct query in text field below. By specifying a custom construct query it is possible to create a custom graph view based on the given SPARQL endpoint, e.g.
		<details><summary>SPARQL for viewing a taxonomy</summary>
			<pre>
PREFIX owl: &lt;http://www.w3.org/2002/07/owl#&gt;
PREFIX rdf: &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#&gt;
PREFIX rdfs: &lt;http://www.w3.org/2000/01/rdf-schema#&gt;
PREFIX skos: &lt;http://www.w3.org/2004/02/skos/core#&gt;<br/>
CONSTRUCT {
&ensp;&ensp;&ensp;&ensp;?sub rdfs:subClassOf ?super .
&ensp;&ensp;&ensp;&ensp;?sub skos:prefLabel ?sublabel .
&ensp;&ensp;&ensp;&ensp;?super skos:prefLabel ?superlabel .
}
WHERE {
&ensp;&ensp;&ensp;&ensp;SELECT DISTINCT ?super ?superlabel ?sub ?sublabel WHERE {
&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;?super rdf:type owl:Class .
&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;?super skos:prefLabel ?superlabel .
&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;?sub rdfs:subClassOf ?super .
&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;?sub skos:prefLabel ?sublabel .
&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;FILTER(?super != ?sub)
&ensp;&ensp;&ensp;&ensp;}
}
		</pre>
	</details>
</ol>

## How to view AFT or other ontology files?
Download AFT from [http://purl.allotrope.org](http://purl.allotrope.org/), e.g [AFT/REC/2019/03](http://purl.allotrope.org/voc/afo/REC/2019/03/aft.ttl). You should include BFO into the graph in order to get a useful visualization of the AFT backbone. Allotrope's curated version of BFO can be downloaded from [http://purl.allotrope.org](http://purl.allotrope.org/), direct link [BFO/REC/2018/04](http://purl.allotrope.org/voc/bfo/REC/2018/04/bfo-2-0.ttl).

In *ADFSee*, upload Turtle files with vocabulary into the upper upload box ("Drop data file here"). Upload also your Turtle file of BFO to the upper upload box. Select option "See vocabulary", **unselect** "Load AFO2.0" and **unselect** "Load AFT1.1.5" (in order to restrict the visualization to the uploaded turtle files). Optionally, select "Exclude Literals" in order to reduce the resulting graph to nodes of resources. Unselect "Try example" and unselect "Query endpoint". Click button `See!`.

AFT/REC/2019/03 results in a graph of several thousand nodes. Preparing and rendering a visualization will take time.

## How to save/load a cytoscape graph layout?
Click "Export json" at the top-left of the cytoscape view in order to download a json representation of the current graph. For loading an exported graph layout, drop your json file containing the exported graph layout into the upper upload box located at the right edge of the screen (make sure that you drop only a single json file into the upper upload box and the lower upload box is empty). Click button `See!`.

## How to execute SPARQL queries on an ADF file?
Upload an ADF file by dropping it into the upper upload box located at the right edge of the screen. Click button `See!`. At the same time of generation of a graph view of the data description of the ADF, *ADFSee* loads all contained triples into the Fuseki triple store (the triple store is started as a separate process during startup of *ADFSee*). *ADFSee* creates the same default graph and named graphs as given in the data description. Please note that the triple store is reset when uploading an ADF file. All graphs of the triple store are cleared before each loading process.<br/>
Click link "triple store" or open up [http://localhost:9999/dataset.html?tab=query&ds=/adfsee](http://localhost:9999/dataset.html?tab=query&ds=/adfsee) in your web browser in order to access the SPARQL web interface. [Fuseki](https://jena.apache.org/documentation/fuseki2/) also offers REST endpoints and different APIs.

## How to execute SPARQL queries on RDF files?
Upload RDF data by dropping instance data into the upper upload box and additional vocabulary into the lower upload box, both boxes are located at the right edge of the screen. Click button `See!`. At the same time of generation of a graph view of the uploaded RDF data description, *ADFSee* loads all triples into the Fuseki triple store (the triple store is started as a separate process during startup of *ADFSee*). *ADFSee* loads all instance data into named graph [http://localhost/adfsee/instances](), vocabulary into named graph [http://localhost/adfsee/vocabulary]() and all triples together into a merged graph [http://localhost/adfsee/merged](). Please note that the triple store is reset when uploading data files. All graphs of the triple store are cleared before each loading process.<br/>
Click link "triple store" or open up [http://localhost:9999/dataset.html?tab=query&ds=/adfsee](http://localhost:9999/dataset.html?tab=query&ds=/adfsee) in your web browser in order to access the SPARQL web interface. [Fuseki](https://jena.apache.org/documentation/fuseki2/) also offers REST endpoints and different APIs.

## What is [Ontodia](https://github.com/metaphacts/ontodia), how to explore ADF data with the help of [Ontodia](https://github.com/metaphacts/ontodia)?
[Ontodia](https://github.com/metaphacts/ontodia) provides a useful way of graph exploration starting from single selected nodes of the RDF data. Instead of loading a complete view of a graph as for the cytoscape view, Ontodia allows the user to select certain instances or classes and then explore the neighborhood of the selected nodes by following available links.<br/>
*ADFSee* applies Ontodia to connect to uploaded RDF data in a Fuseki triple store. Upload an ADF file or RDF data by dropping into the upload boxes located at the right edge of the screen. Drop the ADF file or RDF instance data into the upper upload box. Additional vocabulary files can be uploaded by dropping into the lower upload box. Click button `See!`. At the same time of generation of a graph view of the uploaded data, *ADFSee* loads all contained triples into the Fuseki triple store. Once processing is complete (spinning logo stops and server response is printed at the bottom right) you can access the Ontodia explorer by clicking link "ontodia" at the left side of the screen. In the Ontodia view, search/filter for classes and instances, drag and drop selected nodes of classes/instances on the working area, extend the graph view by clicking different icons at the border of boxes of nodes.

## How do I start the embedded triple store? <a name="triplestore"></a>
If you have downloaded the prebuilt version of *ADFSee* then you can start *ADFSee* together with the triple store by double-clicking `start-adfsee-with-triplestore.bat`. Otherwise, apply commandline argument `tripleStore` in order to start up *ADFSee* with the embedded Fuseki triple store, e.g. `java -jar ./adfsee-1.5.1.war tripleStore`. The triple store is started as a separate process together with a controller [service](#services). It takes significantly more time to start up or stop *ADFSee* including the triple store.

## How to speed up loading, rendering, layouting? How to handle large graphs?
Viewing graph data from ADF files requires several steps. The ADF has to be processed and information extracted. Extracted data is transformed for viewing and processed to find a useful layout. After the graph data has been returned to the browser it gets processed and rendered within Chrome. Processing time depends on the size of the graph. For graphs with about 10k nodes, you can expect a total processing time of some minutes. If you want to render views of larger graphs faster then you can try to skip server-side layouting. Uncheck option `Do layout server-side`. Select e.g. pixi or stardust for fast client-side rendering.

## Where do I get support?
*ADFSee* is developed by Helge Krieg, [OSTHUS GmbH](http://www.osthus.com). Please contact helge&nbsp;.&nbsp;krieg&nbsp;@&nbsp;osthus&nbsp;.&nbsp;com if you have questions to application or extension of *ADFSee*.
If you find any issues please post to the issue tracker. We are happy to get your feedback and feature requests!

## What alternatives do I have to inspect ADF files?
There exist several ways to open ADF files and inspect content.

- Use [ADF explorer](https://gitlab.com/allotrope/adf-explorer), the officially supported tool from Allotrope. It offers a great tree view of data package and data cubes as well as useful views on audit trails. There also exist some [good plugins](https://gitlab.com/allotrope-open-source/adf-explorer-plugins) with useful extensions. ADF Explorer is available for download from [Allotrope repository](https://clientconnect.drinkerbiddle.com/drinkerbiddle/documentHome.action?metaData.siteID=2034&metaData.parentFolderID=111012). For access ([join Allotrope](https://www.allotrope.org/join-us)).
- Use *Allotrope shell* in order to process ADF from command line. (Please refer to [Allotrope](https://www.allotrope.org) in order to find out how to get access to Allotrope shell).
- Use *ADF API* in order to process ADF programmatically. ADF API and documentation is available for download from [Allotrope repository](https://clientconnect.drinkerbiddle.com/drinkerbiddle/documentHome.action?metaData.siteID=2034&metaData.parentFolderID=111102). (Please refer to [Allotrope](https://www.allotrope.org) in order to find out how to get access to ADF libraries)
- Use [Human Oriented Allotrope Resource Dump(HOARD)](https://gitlab.com/allotrope-open-source/hoard) to quickly dump triples of a data description as a tree structure to the command line or a file. Triples are grouped and artificial IDs are replaced by human readable labels.


## What is the license?
*ADFSee* is published under the MIT license.

## Changelog
### [1.8.3](https://highq.in/8eq2zsln4y)
Fixed #16

Changed branding to Allotrope Foundation

Updated license

### [1.8.2]()
Changed default values of sliders: node size, node border, edge size, edge width to 49, 1, 41, 1

### [1.8.1]()
Added [Yasgui](https://github.com/TriplyDB/Yasgui) SPARQL query editor

### [1.8.0](https://highq.in/7qesvtvkgw)
Added planar view with biofabric

Fixed handling of paths with whitespace in data package

Changed branding

Changed triple store to [Fuseki2](https://jena.apache.org/documentation/fuseki2/)

### [1.7.9](https://highq.in/7m0lmz269l)
Added view with pixi.js

Added view with stardust.js

Added view with clustered layout based on t-SNE

Added commandline argument to start triple store

Added option to do layout server-side

### [1.7.5]()
Import/export cytoscape graph layout

Added skolemization of exported data description

Migrated to Spring Boot 2

### [1.7.2]()
Added embedded Blazegraph triple store

Added Ontodia view

New feature: Visualizing multiple disconnected graphs

### [1.7.0](https://highq.in/46m4zgi73v)
Improved visualization of ontologies

Better handling of bidirectional links

### [1.6.1](https://highq.in/447ec467xa)
Support Allotrope vocabulary WD/2019/01/25

Added fisheye view

Added scaling by degree

Added VR graph

Added 3d force graph

Added 3d tree graph

Added 2d force graph

Fixes issues [#4](https://gitlab.com/allotrope-open-source/adfsee/issues/4), [#3](https://gitlab.com/allotrope-open-source/adfsee/issues/3), [#2](https://gitlab.com/allotrope-open-source/adfsee/issues/2), [#1](https://gitlab.com/allotrope-open-source/adfsee/issues/1)

### [1.5.1](https://highq.in/3t1rvx7jw5)
Improved handling of anonymous rdf:type statements

### [1.5.0](https://highq.in/3h0va2a5vh)
Support viewing Allotrope LC-UV model (2018/09)

### [1.4.5](https://highq.in/3h0xlj7yu2)
New feature: untangling graph by splitting nodes

Improved processing of datacubes

### [1.4.4](https://highq.in/349usk72c8)
Apply construct queries to create graph from uploaded data

### 1.4.3
New feature: support construct queries to create graph from SPARQL endpoint

Removed duplicate literal nodes

### 1.4.1
New feature: support reading graph data from SPARQL endpoint

### 1.3.1
first public release


## Services (from 1.7.5) <a name="services"></a>

By default, *ADFSee* runs as a single process. If *ADFSee* is [started together with its embedded Fuseki triple store](#triplestore) then following services are executed.

### *ADFSee*
*ADFSee* is the main application server that processes uploaded data. During startup of *ADFSee* with commandline argument `tripleStore` several further microservices are spawned as separate processes. Depending on the system it can take up to one minute until all processes are fully running and have passed their initialization. The triple store can be used as soon as all required processes have finished their initialization. *ADFSee* is accessible in the Chrome browser several seconds after launching.

### ADFSee Controller
During startup of *ADFSee* with commandline argument `tripleStore` a controller service is spawned as a separate process. The controller service watches all running services of *ADFSee* and makes sure that all processes shut down when *ADFSee* stops.

### Fuseki
During startup of *ADFSee* with commandline argument `tripleStore` a Fuseki2 triple store is spawned as a separate process. The service provides a user interface at [http://localhost:9999/](http://localhost:9999/) including a SPARQL interface.<br/>
If it should happen in exceptional cases (e.g. SIGKILL) that *ADFSee* and the *ADFSee* controller stopped working but Fuseki is still running as an orphan process then the orphan process will be replaced with a fresh instance of Fuseki at the next start of *ADFSee*.

### *ADFSee* does not start up and how to resolve error messages such as "Unable to access jarfile" or "ADFSee controller service is not responding."?
Some users have observed issues related to the services that are required for the triple store. Please try to fully reset *ADFSee* if you experience such issues or *ADFSee* does not start up correctly.

1.	Stop *ADFSee*
     1. Close its console window or hit ctrl-c to interrupt the process
1.	Wait for one minute to let child processes shut down
1.	Delete folders with temporary data of *ADFSee*, on your local system probably:
      *  C:\Users\your-name\AppData\Local\Temp\controller
      *  C:\Users\your-name\AppData\Local\Temp\controller*SomeID*
      *  C:\Users\your-name\AppData\Local\Temp\proxy
      *  C:\Users\your-name\AppData\Local\Temp\proxy*SomeID*
      *  C:\Users\your-name\AppData\Local\Temp\triplestore
      *  C:\Users\your-name\AppData\Local\Temp\triplestore*SomeID*
1.	Start *ADFSee* with `start-adfsee.bat`
1.	Open Chrome at http://localhost:8181
1.	In Chrome, push `F12` to open the developer console
1.	Open tab "application"
      1.  Click "Clear Site Data"
      1.  Keep the developer console open
1.	Right-click the Refresh Button of Chrome (to the left of URL entry field) and select "Empty Cache And Hard Reload" to completely reload *ADFSee* in Chrome
1.	Close developer console

### Opening the webpage of *ADFSee* is slow and there is a spinning symbol for a long period of time. How can I reset the browser application?
Probably, there is a lot of data in the cache, clear site data.

1.	In Chrome, push `F12` to open the developer console
1.	Open tab "application"
      1.  Click "Clear Site Data"
      1.  Keep the developer console open
1.	Right-click the Refresh Button of Chrome (to the left of URL entry field) and select "Empty Cache And Hard Reload" to completely reload *ADFSee* in Chrome
1.	Close developer console

### Why has the embedded Blazegraph triple store been replaced with Fuseki?
The reason is better compatibility of the Apache 2 license of the Fuseki triple store. 

### Why does Ontodia slowly fill lists with values?
Ontodia queries the Fuseki triple store in the background. Compared to Blazegraph, the performance of Fuseki responding SPARQL queries with OPTIONAL statements seems to be considerably slower. 

## Why does the application have the name *ADFSee*?
*ADFSee* stands for **A** *llotrope* **D** *ata* **F** *ormat See*. The core purpose of the tool is to provide users a simple way to get insight to ADF files and *see* the meaning of the metadata.

*ADFSee* was formerly called *OSee*. **O** standing for [**O**STHUS](http://www.osthus.com) which is the company that the main contributor works with. *OSee* was renamed to *ADFSee* in order to better represent the focus of the tool and from a legal perspective, in order to better comply with existing software in the market with the same name.

ADFSee &copy;2020 Allotrope Foundation<br/>
Created from OSee ©2019 Helge Krieg