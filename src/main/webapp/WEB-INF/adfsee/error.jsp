<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
	<head>
		<title layout:title-pattern="$CONTENT_TITLE">ADFSee (not found)</title>
	</head>
	<body>
		<h1>Oops! Page not found?</h1>
		<a href="./index.html" style="font-size: 20pt; color: #000000; cursor: pointer">Go back to main page</a><br/>
		<div th:include="core :: footerFragment" th:remove="tag"></div>
	</body>
</html>