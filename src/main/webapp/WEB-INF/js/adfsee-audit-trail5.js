/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
var data;
var treeData;
var audittrailData;
var audittrail;
var tabledata;
var loadafo = true;
var loadaft = true;
var dolayout = true;
var loadexample = false;
var multigraph = false;
var skolemize = false;
var includeadf = false;
var columnWidths;
var columnHeaders;

initGraphStore();

document.addEventListener('DOMContentLoaded', function() {
		var loading = document.getElementById('loading');
		loading.classList.remove('loaded');

		if (localStorage.getItem('audittrailStatus') && localStorage.getItem('audittrailStatus') == freshStartStatus) {
			resetLocalDbAndDataOfAudittrails().then(createTreeMap);
		} else if (localStorage.getItem('audittrailStatus') && localStorage.getItem('audittrailStatus') == audittrailAvailableStatus) {
			selectAllFromAudittrails()
				.then(updateAudittrailStateWithSelectedResults)
				.then(createTreeMap);
		} else {
			localStorage.setItem('audittrailStatus', freshStartStatus);
			resetLocalDbAndDataOfAudittrails()
				.then(function() {
					console	.log("No state information found in local audittrail store. Restarting from scratch.");
					return Promise.resolve();
				})
				.then(createTreeMap);
		}

		loading.classList.add('loaded');
});

async function createTreeMap() {
	var data = await createJsonTreeMapData(audittrail);
	try {
		var __next_objid=1;
		function objectId(obj) {
		    if (obj==null) return null;
		    if (obj.__obj_id==null) obj.__obj_id=__next_objid++;
		    return obj.__obj_id;
		}
		
		var root = d3.hierarchy(data)
	      .eachBefore(function(d) { d.data.id = (d.parent ? d.parent.data.id + " --> " : "") + d.data.name; })
	      .sum(sumByCount)
	      .sort(function(a, b) { return b.height - a.height || b.value - a.value; });

		var totalLeaves = root.leaves().length;
		var height = 30 * totalLeaves;
		if (height < 800) {
			height = 800;
		}
		var width = 1200;
		
		var svg = d3.select("svg")
		.style("width", width + "px")
		.style("height", height + "px");
		
		var fader = function(color) { return d3.interpolateRgb(color, "blue")(0.2); },
		color = d3.scaleOrdinal(d3.schemePaired.map(fader));
		
		var format = d3.format(",d");
		
		var treemap = d3.treemap()
		    .tile(d3.treemapSlice)
		    .size([width, height])
		    .round(true)
		    .paddingInner(1);
		
		treemap(root);
		
		var cell = svg.selectAll("g")
			.data(root.leaves())
			.enter().append("g")
			.attr("transform", function(d) { return "translate(" + d.x0 + "," + d.y0 + ")"; });

		cell.append("rect")
			.attr("id", function(d) { return d.data.id; })
			.attr("width", function(d) { return d.x1 - d.x0; })
			.attr("height", function(d) { return d.y1 - d.y0; })
			.attr("fill", function(d) { return color(d.parent.data.id); });

		cell.append("clipPath")
			.attr("id", function(d) { return "clip-" + d.data.id; })
			.append("use")
			.attr("xlink:href", function(d) { return "#" + d.data.id; });

		cell.append("text")
			.attr("clip-path", function(d) { return "url(#clip-" + d.data.id + ")"; })
			.selectAll("tspan")
			.data(function(d) { return d.data.name.split(/(?=[A-Z][^A-Z])/g); })
			.enter().append("tspan")
			.attr("x", 4)
			.attr("y", function(d, i) { return 13 + i * 10; })
			.text(function(d) { return d; });

		cell.append("title")
			.text(function(d) { return d.data.id; });
	} catch(error) {
		console.log("Error during creation of treemap: " + error);
		loading.classList.add('loaded');
		return Promise.reject();
	}
	return Promise.resolve();
}

function sumByCount(d) {
	return d.children ? 0 : 1;
}

async function createJsonTreeMapData(jsonData) {
	loading.classList.remove('loaded');
	var tmptree = {};
	
	try {
		if (!jsonData) {
			tmptree = initTreeMapData;
		} else {
			tmptree = audittrail;
		}
		if (localStorage.getItem('audittrailStatus') && localStorage.getItem('audittrailStatus') == audittrailAvailableStatus) {
			tmptree = await anyJson2NormalizedView(tmptree, null);
		}

		tmptree = await anyJson2NormalizedView(tmptree, null);
	} catch(error) {
		console.log("Error during transformation from audittrail to treemap: " + error);
		loading.classList.add('loaded');
		return Promise.reject();
	}
	loading.classList.add('loaded');
	return Promise.resolve(tmptree);
}