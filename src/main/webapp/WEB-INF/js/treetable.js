/*
 * jQuery treetable Plugin 3.2.0
 * http://ludo.cubicphuse.nl/jquery-treetable
 *
 * Copyright 2013, Ludo van den Boom
 * Dual licensed under the MIT or GPL Version 2 licenses.
 */
(function($) {
  var Node, Tree, methods;

  Node = (function() {
    function Node(row, tree, settings) {
      var parentId;

      this.row = row;
      this.tree = tree;
      this.settings = settings;

      // TODO Ensure id/parentId is always a string (not int)
      this.id = this.row.data(this.settings.nodeIdAttr);

      // TODO Move this to a setParentId function?
      parentId = this.row.data(this.settings.parentIdAttr);
      if (parentId != null && parentId !== "") {
        this.parentId = parentId;
      }

      this.treeCell = $(this.row.children(this.settings.columnElType)[this.settings.column]);
      this.expander = $(this.settings.expanderTemplate);
      this.indenter = $(this.settings.indenterTemplate);
      this.children = [];
      this.initialized = false;
      this.treeCell.prepend(this.indenter);
    }

    Node.prototype.addChild = function(child) {
      return this.children.push(child);
    };

    Node.prototype.ancestors = function() {
      var ancestors, node;
      node = this;
      ancestors = [];
      while (node = node.parentNode()) {
        ancestors.push(node);
      }
      return ancestors;
    };

    Node.prototype.collapse = function() {
      if (this.collapsed()) {
        return this;
      }

      this.row.removeClass("expanded").addClass("collapsed");

      this._hideChildren();
      this.expander.attr("title", this.settings.stringExpand);

      if (this.initialized && this.settings.onNodeCollapse != null) {
        this.settings.onNodeCollapse.apply(this);
      }

      return this;
    };

    Node.prototype.collapsed = function() {
      return this.row.hasClass("collapsed");
    };

    // TODO destroy: remove event handlers, expander, indenter, etc.

    Node.prototype.expand = function() {
      if (this.expanded()) {
        return this;
      }

      this.row.removeClass("collapsed").addClass("expanded");

      if (this.initialized && this.settings.onNodeExpand != null) {
        this.settings.onNodeExpand.apply(this);
      }

      if ($(this.row).is(":visible")) {
        this._showChildren();
      }

      this.expander.attr("title", this.settings.stringCollapse);

      return this;
    };

    Node.prototype.expanded = function() {
      return this.row.hasClass("expanded");
    };

    Node.prototype.hide = function() {
      this._hideChildren();
      this.row.hide();
      return this;
    };

    Node.prototype.isBranchNode = function() {
      if(this.children.length > 0 || this.row.data(this.settings.branchAttr) === true) {
        return true;
      } else {
        return false;
      }
    };

    Node.prototype.updateBranchLeafClass = function(){
      this.row.removeClass('branch');
      this.row.removeClass('leaf');
      this.row.addClass(this.isBranchNode() ? 'branch' : 'leaf');
    };

    Node.prototype.level = function() {
      return this.ancestors().length;
    };

    Node.prototype.parentNode = function() {
      if (this.parentId != null) {
        return this.tree[this.parentId];
      } else {
        return null;
      }
    };

    Node.prototype.removeChild = function(child) {
      var i = $.inArray(child, this.children);
      return this.children.splice(i, 1)
    };

    Node.prototype.render = function() {
      var handler,
          settings = this.settings,
          target;

      if (settings.expandable === true && this.isBranchNode()) {
        handler = function(e) {
          $(this).parents("table").treetable("node", $(this).parents("tr").data(settings.nodeIdAttr)).toggle();
          return e.preventDefault();
        };

        this.indenter.html(this.expander);
        target = settings.clickableNodeNames === true ? this.treeCell : this.expander;

        target.off("click.treetable").on("click.treetable", handler);
        target.off("keydown.treetable").on("keydown.treetable", function(e) {
          if (e.keyCode == 13) {
            handler.apply(this, [e]);
          }
        });
      }

      this.indenter[0].style.paddingLeft = "" + (this.level() * settings.indent) + "px";

      return this;
    };

    Node.prototype.reveal = function() {
      if (this.parentId != null) {
        this.parentNode().reveal();
      }
      return this.expand();
    };

    Node.prototype.setParent = function(node) {
      if (this.parentId != null) {
        this.tree[this.parentId].removeChild(this);
      }
      this.parentId = node.id;
      this.row.data(this.settings.parentIdAttr, node.id);
      return node.addChild(this);
    };

    Node.prototype.show = function() {
      if (!this.initialized) {
        this._initialize();
      }
      this.row.show();
      if (this.expanded()) {
        this._showChildren();
      }
      return this;
    };

    Node.prototype.toggle = function() {
      if (this.expanded()) {
        this.collapse();
      } else {
        this.expand();
      }
      return this;
    };

    Node.prototype._hideChildren = function() {
      var child, _i, _len, _ref, _results;
      _ref = this.children;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        child = _ref[_i];
        _results.push(child.hide());
      }
      return _results;
    };

    Node.prototype._initialize = function() {
      var settings = this.settings;

      this.render();

      if (settings.expandable === true && settings.initialState === "collapsed") {
        this.collapse();
      } else {
        this.expand();
      }

      if (settings.onNodeInitialized != null) {
        settings.onNodeInitialized.apply(this);
      }

      return this.initialized = true;
    };

    Node.prototype._showChildren = function() {
      var child, _i, _len, _ref, _results;
      _ref = this.children;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        child = _ref[_i];
        _results.push(child.show());
      }
      return _results;
    };

    return Node;
  })();

  Tree = (function() {
    function Tree(table, settings) {
      this.table = table;
      this.settings = settings;
      this.tree = {};

      // Cache the nodes and roots in simple arrays for quick access/iteration
      this.nodes = [];
      this.roots = [];
    }

    Tree.prototype.collapseAll = function() {
      var node, _i, _len, _ref, _results;
      _ref = this.nodes;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        node = _ref[_i];
        _results.push(node.collapse());
      }
      return _results;
    };

    Tree.prototype.expandAll = function() {
      var node, _i, _len, _ref, _results;
      _ref = this.nodes;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        node = _ref[_i];
        _results.push(node.expand());
      }
      return _results;
    };

    Tree.prototype.findLastNode = function (node) {
      if (node.children.length > 0) {
        return this.findLastNode(node.children[node.children.length - 1]);
      } else {
        return node;
      }
    };

    Tree.prototype.loadRows = function(rows) {
      var node, row, i;

      if (rows != null) {
        for (i = 0; i < rows.length; i++) {
          row = $(rows[i]);

          if (row.data(this.settings.nodeIdAttr) != null) {
            node = new Node(row, this.tree, this.settings);
            this.nodes.push(node);
            this.tree[node.id] = node;

            if (node.parentId != null && this.tree[node.parentId]) {
              this.tree[node.parentId].addChild(node);
            } else {
              this.roots.push(node);
            }
          }
        }
      }

      for (i = 0; i < this.nodes.length; i++) {
        node = this.nodes[i].updateBranchLeafClass();
      }

      return this;
    };

    Tree.prototype.move = function(node, destination) {
      // Conditions:
      // 1: +node+ should not be inserted as a child of +node+ itself.
      // 2: +destination+ should not be the same as +node+'s current parent (this
      //    prevents +node+ from being moved to the same location where it already
      //    is).
      // 3: +node+ should not be inserted in a location in a branch if this would
      //    result in +node+ being an ancestor of itself.
      var nodeParent = node.parentNode();
      if (node !== destination && destination.id !== node.parentId && $.inArray(node, destination.ancestors()) === -1) {
        node.setParent(destination);
        this._moveRows(node, destination);

        // Re-render parentNode if this is its first child node, and therefore
        // doesn't have the expander yet.
        if (node.parentNode().children.length === 1) {
          node.parentNode().render();
        }
      }

      if(nodeParent){
        nodeParent.updateBranchLeafClass();
      }
      if(node.parentNode()){
        node.parentNode().updateBranchLeafClass();
      }
      node.updateBranchLeafClass();
      return this;
    };

    Tree.prototype.removeNode = function(node) {
      // Recursively remove all descendants of +node+
      this.unloadBranch(node);

      // Remove node from DOM (<tr>)
      node.row.remove();

      // Remove node from parent children list
      if (node.parentId != null) {
        node.parentNode().removeChild(node);
      }

      // Clean up Tree object (so Node objects are GC-ed)
      delete this.tree[node.id];
      this.nodes.splice($.inArray(node, this.nodes), 1);

      return this;
    }

    Tree.prototype.render = function() {
      var root, _i, _len, _ref;
      _ref = this.roots;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        root = _ref[_i];

        // Naming is confusing (show/render). I do not call render on node from
        // here.
        root.show();
      }
      return this;
    };

    Tree.prototype.sortBranch = function(node, sortFun) {
      // First sort internal array of children
      node.children.sort(sortFun);

      // Next render rows in correct order on page
      this._sortChildRows(node);

      return this;
    };

    Tree.prototype.unloadBranch = function(node) {
      // Use a copy of the children array to not have other functions interfere
      // with this function if they manipulate the children array
      // (eg removeNode).
      var children = node.children.slice(0),
          i;

      for (i = 0; i < children.length; i++) {
        this.removeNode(children[i]);
      }

      // Reset node's collection of children
      node.children = [];

      node.updateBranchLeafClass();

      return this;
    };

    Tree.prototype._moveRows = function(node, destination) {
      var children = node.children, i;

      node.row.insertAfter(destination.row);
      node.render();

      // Loop backwards through children to have them end up on UI in correct
      // order (see #112)
      for (i = children.length - 1; i >= 0; i--) {
        this._moveRows(children[i], node);
      }
    };

    // Special _moveRows case, move children to itself to force sorting
    Tree.prototype._sortChildRows = function(parentNode) {
      return this._moveRows(parentNode, parentNode);
    };

    return Tree;
  })();

  // jQuery Plugin
  methods = {
    init: function(options, force) {
      var settings;

      settings = $.extend({
        branchAttr: "ttBranch",
        clickableNodeNames: false,
        column: 0,
        columnElType: "td", // i.e. 'td', 'th' or 'td,th'
        expandable: false,
        expanderTemplate: "<a href='#'>&nbsp;</a>",
        indent: 19,
        indenterTemplate: "<span class='indenter'></span>",
        initialState: "collapsed",
        nodeIdAttr: "ttId", // maps to data-tt-id
        parentIdAttr: "ttParentId", // maps to data-tt-parent-id
        stringExpand: "Expand",
        stringCollapse: "Collapse",

        // Events
        onInitialized: null,
        onNodeCollapse: null,
        onNodeExpand: null,
        onNodeInitialized: null
      }, options);

      return this.each(function() {
        var el = $(this), tree;

        if (force || el.data("treetable") === undefined) {
          tree = new Tree(this, settings);
          tree.loadRows(this.rows).render();

          el.addClass("treetable").data("treetable", tree);

          if (settings.onInitialized != null) {
            settings.onInitialized.apply(tree);
          }
        }

        return el;
      });
    },

    destroy: function() {
      return this.each(function() {
        return $(this).removeData("treetable").removeClass("treetable");
      });
    },

    collapseAll: function() {
      this.data("treetable").collapseAll();
      return this;
    },

    collapseNode: function(id) {
      var node = this.data("treetable").tree[id];

      if (node) {
        node.collapse();
      } else {
        throw new Error("Unknown node '" + id + "'");
      }

      return this;
    },

    expandAll: function() {
      this.data("treetable").expandAll();
      return this;
    },

    expandNode: function(id) {
      var node = this.data("treetable").tree[id];

      if (node) {
        if (!node.initialized) {
          node._initialize();
        }

        node.expand();
      } else {
        throw new Error("Unknown node '" + id + "'");
      }

      return this;
    },

    loadBranch: function(node, rows) {
      var settings = this.data("treetable").settings,
          tree = this.data("treetable").tree;

      // TODO Switch to $.parseHTML
      rows = $(rows);

      if (node == null) { // Inserting new root nodes
        this.append(rows);
      } else {
        var lastNode = this.data("treetable").findLastNode(node);
        rows.insertAfter(lastNode.row);
      }

      this.data("treetable").loadRows(rows);

      // Make sure nodes are properly initialized
      rows.filter("tr").each(function() {
        tree[$(this).data(settings.nodeIdAttr)].show();
      });

      if (node != null) {
        // Re-render parent to ensure expander icon is shown (#79)
        node.render().expand();
      }

      return this;
    },

    move: function(nodeId, destinationId) {
      var destination, node;

      node = this.data("treetable").tree[nodeId];
      destination = this.data("treetable").tree[destinationId];
      this.data("treetable").move(node, destination);

      return this;
    },

    node: function(id) {
      return this.data("treetable").tree[id];
    },

    removeNode: function(id) {
      var node = this.data("treetable").tree[id];

      if (node) {
        this.data("treetable").removeNode(node);
      } else {
        throw new Error("Unknown node '" + id + "'");
      }

      return this;
    },

    reveal: function(id) {
      var node = this.data("treetable").tree[id];

      if (node) {
        node.reveal();
      } else {
        throw new Error("Unknown node '" + id + "'");
      }

      return this;
    },

    sortBranch: function(node, columnOrFunction) {
      var settings = this.data("treetable").settings,
          prepValue,
          sortFun;

      columnOrFunction = columnOrFunction || settings.column;
      sortFun = columnOrFunction;

      if ($.isNumeric(columnOrFunction)) {
        sortFun = function(a, b) {
          var extractValue, valA, valB;

          extractValue = function(node) {
            var val = node.row.find("td:eq(" + columnOrFunction + ")").text();
            // Ignore trailing/leading whitespace and use uppercase values for
            // case insensitive ordering
            return $.trim(val).toUpperCase();
          }

          valA = extractValue(a);
          valB = extractValue(b);

          if (valA < valB) return -1;
          if (valA > valB) return 1;
          return 0;
        };
      }

      this.data("treetable").sortBranch(node, sortFun);
      return this;
    },

    unloadBranch: function(node) {
      this.data("treetable").unloadBranch(node);
      return this;
    }
  };

  $.fn.treetable = function(method) {
    if (methods[method]) {
      return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
    } else if (typeof method === 'object' || !method) {
      return methods.init.apply(this, arguments);
    } else {
      return $.error("Method " + method + " does not exist on jQuery.treetable");
    }
  };

  // Expose classes to world
  this.TreeTable || (this.TreeTable = {});
  this.TreeTable.Node = Node;
  this.TreeTable.Tree = Tree;
})(jQuery);
//Copyright (c) 2013 Ludo van den Boom, http://ludovandenboom.com
//
//	Permission is hereby granted, free of charge, to any person obtaining
//	a copy of this software and associated documentation files (the
//	"Software"), to deal in the Software without restriction, including
//	without limitation the rights to use, copy, modify, merge, publish,
//	distribute, sublicense, and/or sell copies of the Software, and to
//	permit persons to whom the Software is furnished to do so, subject to
//	the following conditions:
//
//	The above copyright notice and this permission notice shall be
//	included in all copies or substantial portions of the Software.
//
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
//	MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
//	LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
//	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
//	WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// source https://github.com/ludo/jquery-treetable
//
//GNU GENERAL PUBLIC LICENSE
//Version 2, June 1991
//
//Copyright (C) 1989, 1991 Free Software Foundation, Inc.
//51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//Everyone is permitted to copy and distribute verbatim copies
//of this license document, but changing it is not allowed.
//
//Preamble
//
//The licenses for most software are designed to take away your
//freedom to share and change it.  By contrast, the GNU General Public
//License is intended to guarantee your freedom to share and change free
//software--to make sure the software is free for all its users.  This
//General Public License applies to most of the Free Software
//Foundation's software and to any other program whose authors commit to
//using it.  (Some other Free Software Foundation software is covered by
//the GNU Lesser General Public License instead.)  You can apply it to
//your programs, too.
//
//When we speak of free software, we are referring to freedom, not
//price.  Our General Public Licenses are designed to make sure that you
//have the freedom to distribute copies of free software (and charge for
//this service if you wish), that you receive source code or can get it
//if you want it, that you can change the software or use pieces of it
//in new free programs; and that you know you can do these things.
//
//To protect your rights, we need to make restrictions that forbid
//anyone to deny you these rights or to ask you to surrender the rights.
//These restrictions translate to certain responsibilities for you if you
//distribute copies of the software, or if you modify it.
//
//For example, if you distribute copies of such a program, whether
//gratis or for a fee, you must give the recipients all the rights that
//you have.  You must make sure that they, too, receive or can get the
//source code.  And you must show them these terms so they know their
//rights.
//
//We protect your rights with two steps: (1) copyright the software, and
//(2) offer you this license which gives you legal permission to copy,
//distribute and/or modify the software.
//
//Also, for each author's protection and ours, we want to make certain
//that everyone understands that there is no warranty for this free
//software.  If the software is modified by someone else and passed on, we
//want its recipients to know that what they have is not the original, so
//that any problems introduced by others will not reflect on the original
//authors' reputations.
//
//Finally, any free program is threatened constantly by software
//patents.  We wish to avoid the danger that redistributors of a free
//program will individually obtain patent licenses, in effect making the
//program proprietary.  To prevent this, we have made it clear that any
//patent must be licensed for everyone's free use or not licensed at all.
//
//The precise terms and conditions for copying, distribution and
//modification follow.
//
//GNU GENERAL PUBLIC LICENSE
//TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
//
//0. This License applies to any program or other work which contains
//a notice placed by the copyright holder saying it may be distributed
//under the terms of this General Public License.  The "Program", below,
//refers to any such program or work, and a "work based on the Program"
//means either the Program or any derivative work under copyright law:
//that is to say, a work containing the Program or a portion of it,
//either verbatim or with modifications and/or translated into another
//language.  (Hereinafter, translation is included without limitation in
//the term "modification".)  Each licensee is addressed as "you".
//
//Activities other than copying, distribution and modification are not
//covered by this License; they are outside its scope.  The act of
//running the Program is not restricted, and the output from the Program
//is covered only if its contents constitute a work based on the
//Program (independent of having been made by running the Program).
//Whether that is true depends on what the Program does.
//
//1. You may copy and distribute verbatim copies of the Program's
//source code as you receive it, in any medium, provided that you
//conspicuously and appropriately publish on each copy an appropriate
//copyright notice and disclaimer of warranty; keep intact all the
//notices that refer to this License and to the absence of any warranty;
//and give any other recipients of the Program a copy of this License
//along with the Program.
//
//You may charge a fee for the physical act of transferring a copy, and
//you may at your option offer warranty protection in exchange for a fee.
//
//2. You may modify your copy or copies of the Program or any portion
//of it, thus forming a work based on the Program, and copy and
//distribute such modifications or work under the terms of Section 1
//above, provided that you also meet all of these conditions:
//
//a) You must cause the modified files to carry prominent notices
//stating that you changed the files and the date of any change.
//
//b) You must cause any work that you distribute or publish, that in
//whole or in part contains or is derived from the Program or any
//part thereof, to be licensed as a whole at no charge to all third
//parties under the terms of this License.
//
//c) If the modified program normally reads commands interactively
//when run, you must cause it, when started running for such
//interactive use in the most ordinary way, to print or display an
//announcement including an appropriate copyright notice and a
//notice that there is no warranty (or else, saying that you provide
//a warranty) and that users may redistribute the program under
//these conditions, and telling the user how to view a copy of this
//License.  (Exception: if the Program itself is interactive but
//does not normally print such an announcement, your work based on
//the Program is not required to print an announcement.)
//
//These requirements apply to the modified work as a whole.  If
//identifiable sections of that work are not derived from the Program,
//and can be reasonably considered independent and separate works in
//themselves, then this License, and its terms, do not apply to those
//sections when you distribute them as separate works.  But when you
//distribute the same sections as part of a whole which is a work based
//on the Program, the distribution of the whole must be on the terms of
//this License, whose permissions for other licensees extend to the
//entire whole, and thus to each and every part regardless of who wrote it.
//
//Thus, it is not the intent of this section to claim rights or contest
//your rights to work written entirely by you; rather, the intent is to
//exercise the right to control the distribution of derivative or
//collective works based on the Program.
//
//In addition, mere aggregation of another work not based on the Program
//with the Program (or with a work based on the Program) on a volume of
//a storage or distribution medium does not bring the other work under
//the scope of this License.
//
//3. You may copy and distribute the Program (or a work based on it,
//under Section 2) in object code or executable form under the terms of
//Sections 1 and 2 above provided that you also do one of the following:
//
//a) Accompany it with the complete corresponding machine-readable
//source code, which must be distributed under the terms of Sections
//1 and 2 above on a medium customarily used for software interchange; or,
//
//b) Accompany it with a written offer, valid for at least three
//years, to give any third party, for a charge no more than your
//cost of physically performing source distribution, a complete
//machine-readable copy of the corresponding source code, to be
//distributed under the terms of Sections 1 and 2 above on a medium
//customarily used for software interchange; or,
//
//c) Accompany it with the information you received as to the offer
//to distribute corresponding source code.  (This alternative is
//allowed only for noncommercial distribution and only if you
//received the program in object code or executable form with such
//an offer, in accord with Subsection b above.)
//
//The source code for a work means the preferred form of the work for
//making modifications to it.  For an executable work, complete source
//code means all the source code for all modules it contains, plus any
//associated interface definition files, plus the scripts used to
//control compilation and installation of the executable.  However, as a
//special exception, the source code distributed need not include
//anything that is normally distributed (in either source or binary
//form) with the major components (compiler, kernel, and so on) of the
//operating system on which the executable runs, unless that component
//itself accompanies the executable.
//
//If distribution of executable or object code is made by offering
//access to copy from a designated place, then offering equivalent
//access to copy the source code from the same place counts as
//distribution of the source code, even though third parties are not
//compelled to copy the source along with the object code.
//
//4. You may not copy, modify, sublicense, or distribute the Program
//except as expressly provided under this License.  Any attempt
//otherwise to copy, modify, sublicense or distribute the Program is
//void, and will automatically terminate your rights under this License.
//However, parties who have received copies, or rights, from you under
//this License will not have their licenses terminated so long as such
//parties remain in full compliance.
//
//5. You are not required to accept this License, since you have not
//signed it.  However, nothing else grants you permission to modify or
//distribute the Program or its derivative works.  These actions are
//prohibited by law if you do not accept this License.  Therefore, by
//modifying or distributing the Program (or any work based on the
//Program), you indicate your acceptance of this License to do so, and
//all its terms and conditions for copying, distributing or modifying
//the Program or works based on it.
//
//6. Each time you redistribute the Program (or any work based on the
//Program), the recipient automatically receives a license from the
//original licensor to copy, distribute or modify the Program subject to
//these terms and conditions.  You may not impose any further
//restrictions on the recipients' exercise of the rights granted herein.
//You are not responsible for enforcing compliance by third parties to
//this License.
//
//7. If, as a consequence of a court judgment or allegation of patent
//infringement or for any other reason (not limited to patent issues),
//conditions are imposed on you (whether by court order, agreement or
//otherwise) that contradict the conditions of this License, they do not
//excuse you from the conditions of this License.  If you cannot
//distribute so as to satisfy simultaneously your obligations under this
//License and any other pertinent obligations, then as a consequence you
//may not distribute the Program at all.  For example, if a patent
//license would not permit royalty-free redistribution of the Program by
//all those who receive copies directly or indirectly through you, then
//the only way you could satisfy both it and this License would be to
//refrain entirely from distribution of the Program.
//
//If any portion of this section is held invalid or unenforceable under
//any particular circumstance, the balance of the section is intended to
//apply and the section as a whole is intended to apply in other
//circumstances.
//
//It is not the purpose of this section to induce you to infringe any
//patents or other property right claims or to contest validity of any
//such claims; this section has the sole purpose of protecting the
//integrity of the free software distribution system, which is
//implemented by public license practices.  Many people have made
//generous contributions to the wide range of software distributed
//through that system in reliance on consistent application of that
//system; it is up to the author/donor to decide if he or she is willing
//to distribute software through any other system and a licensee cannot
//impose that choice.
//
//This section is intended to make thoroughly clear what is believed to
//be a consequence of the rest of this License.
//
//8. If the distribution and/or use of the Program is restricted in
//certain countries either by patents or by copyrighted interfaces, the
//original copyright holder who places the Program under this License
//may add an explicit geographical distribution limitation excluding
//those countries, so that distribution is permitted only in or among
//countries not thus excluded.  In such case, this License incorporates
//the limitation as if written in the body of this License.
//
//9. The Free Software Foundation may publish revised and/or new versions
//of the General Public License from time to time.  Such new versions will
//be similar in spirit to the present version, but may differ in detail to
//address new problems or concerns.
//
//Each version is given a distinguishing version number.  If the Program
//specifies a version number of this License which applies to it and "any
//later version", you have the option of following the terms and conditions
//either of that version or of any later version published by the Free
//Software Foundation.  If the Program does not specify a version number of
//this License, you may choose any version ever published by the Free Software
//Foundation.
//
//10. If you wish to incorporate parts of the Program into other free
//programs whose distribution conditions are different, write to the author
//to ask for permission.  For software which is copyrighted by the Free
//Software Foundation, write to the Free Software Foundation; we sometimes
//make exceptions for this.  Our decision will be guided by the two goals
//of preserving the free status of all derivatives of our free software and
//of promoting the sharing and reuse of software generally.
//
//NO WARRANTY
//
//11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
//FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN
//OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES
//PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED
//OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS
//TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE
//PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,
//REPAIR OR CORRECTION.
//
//12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
//WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR
//REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,
//INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING
//OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED
//TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY
//YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER
//PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE
//POSSIBILITY OF SUCH DAMAGES.