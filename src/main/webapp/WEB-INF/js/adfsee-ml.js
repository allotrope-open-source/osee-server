/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
var data;
var treeData;
var tree;
var nodeData;
var mlGrapg;
var hiddenNodes = [];
var hiddenLinks = [];
var nodes = null;
var links = null;

var nodeColors = {};
var labels = {};
var counter = 0;
var needToRemoveSelfLinks = true;
var showlabels = false;
var simulation = true;
var forceSimulation;
var showlabels = false;
var labelsvisible = false;
var popups = null;
var opt = {
	    epsilon: 10,
	    perplexity: 30,
	    dim: 2
	  };

var tsne;

var colorValues = {
	"INFORMATION": "#FFCC00", 
	'INSTANCE': '#98BDF9', 
	'ALLOTROPE': '#DD6C07', 
	'COMMON': '#FFFFFF',
	'MATERIAL': '#C2E7C2',
	'PROCESS': '#CAA3F1',
	'RESULT': '#FFCC00',
	'EQUIPMENT': '#AAD5FF',
	'PROPERTY': '#FFFFFF',
	'QUALITY': '#FFFF99',
	'CONTEXTUAL_ROLE': '#FFC896',
	'REALIZABLE_ENTITY': '#FF99CC',
	'OBO': '#D5ECF7',
	'BFO': '#236099',
	'IAO': '#FFCC00',
	'OBI': '#449d44',
	'PATO': '#234979',
	'RO': '#FFFFFF',
	'CHMO': '#8EC1C5',
	'ENVO': '#A5FF4B',
	'NCBI': '#326597',
	'CHEBI': '#DAD734',
	'EXAMPLE': '#FFDFF7',
	'OTHER': '#FFFFFF',
	'LITERAL' : "#FFF0B3"
};

initGraphStore();

document.addEventListener('DOMContentLoaded',function(){
	loading.classList.remove('loaded');
	if (localStorage.getItem('treeStatus') && localStorage.getItem('treeStatus') == freshStartStatus) {
		resetLocalDbAndDataOfTrees()
		.then(drawMlGraph);
	} else if (localStorage.getItem('treeStatus') && localStorage.getItem('treeStatus') == treeAvailableStatus) {
		selectAllFromGraphs()
			.then(updateGraphStateWithSelectedResults)
			.then(selectAllFromInitialPositions)
			.then(updatePositionsWithSelectedResults);
		selectAllFromTrees()
			.then(updateTreeStateWithSelectedResults)
			.then(drawMlGraph);
	} else {
		localStorage.setItem('treeStatus', freshStartStatus);
		resetLocalDbAndDataOfTrees()
		.then(drawMlGraph);
		console.log("No state information found in local tree store. Restarting from scratch.");
	}
});

var numNodes;
var numClusters = 0;

function drawMlInternal() {
	// This function is based on I. N. Karaca, Graph layouts and machine learning, 2019
	
  var theNodeData = data;
  var graphdata = {
	nodes : [],
	links : []
  };
  graphdata = getNodesAndLinksWithClusters(theNodeData, graphdata, 0);

  const nodeData = graphdata.nodes.map(node => ({
    id: node.index.toString(),
    iri: node.id,
    label: node.name,
    color: node.color,
    group: parseInt(node.cluster)
  }));

  const relData = graphdata.links.map(rel => ({
      source: rel.from.toString(),
      target: rel.to.toString(),
      sourceIri: rel.source,
      targetIri: rel.target,
      label: rel.name,
      color: rel.color
    }));
   
  	setup({
	    nodes: nodeData,
	    links: relData
	});

  nodes = nodeData;
  links = relData;
  
  const dists = nodes.map(function(node) {
	  var vector = [];
	  for (let i = 0; i <= numClusters; i++) {
		  vector.push(0);
	  }
	  var clusterId = node.group;
	  vector[clusterId] = 1;
	  node.vector = vector;
	  return vector;
  });
  tsne.initDataRaw(dists);

  let iterations = 0;

  const step = () => {
    for (let j = 0; j < 3; j++) {
      tsne.step();
    }

    iterations++;

    var yArray = tsne.getSolution();

    yArray.forEach((xy, index) => {
      const node = nodeData[index];
      node.x = xy[0] * 35;
      node.y = xy[1] * 35;
    });

    draw({
      nodes: nodeData,
      links: relData
    });

    if (iterations < 500) {
      if (iterations > 100) {
        setTimeout(() => window.requestAnimationFrame(step), 5);
      } else {
        window.requestAnimationFrame(step);
      }
    }
  };

  window.requestAnimationFrame(step);
}

async function drawMlGraph() {
	let promise = await new Promise(
		function (resolve, reject) {
			loading.classList.remove('loaded');
			try {
				tsne = new window.tsnejs.tSNE(opt);
				drawMlInternal();
			} catch(error) {
				loading.classList.add('loaded');
				console.log("Error during creation of ML graph: " + error);
				reject();
			}
			loading.classList.add('loaded');
			resolve(mlGrapg);
		}
	)
	.catch(error => { 
		loading.classList.add('loaded');
		console.log("Error during creation of ML graph: " + error);
	});
	return promise;
}

function redrawMlGraph() {
	drawMlGraph()
		.then(reload);
}

function reload() {
	var reloadAllowed = localStorage.getItem('reloadMlGraphAllowed');
	if (reloadAllowed && reloadAllowed == "1") {
		localStorage.setItem('reloadMlGraphAllowed',"0");
		window.location.reload();
	} else if (!reloadAllowed) {
		localStorage.setItem('reloadMlGraphAllowed',"0");
		window.location.reload();
	} else if (reloadAllowed && reloadAllowed == "0") {
		localStorage.setItem('reloadMlGraphAllowed',"1");
	}
	return Promise.resolve();
}

document.getElementById("showlabels").addEventListener('click', function() {
	if (showlabels) {
		showlabels = false;
		console.log("labels hidden");
		svg.selectAll("text").style("display", "none");
	} else {
		showlabels = true;
		console.log("labels visible");
		svg.selectAll("text").style("display", "block");
	}
});
	
function getNodesAndLinksWithClusters(elementData, nodeslinks, parentIndex) {
	var arrayLength = elementData.length;
	var nodes = [];
	var edges = [];
	var index = 0;
	for (var i = 0; i < arrayLength; i++) {
		var curElement = elementData[i];
		if (curElement.group && curElement.group == "nodes") {
			var iri = curElement.data.id;
			if (containsElement(nodes, iri)) {
				continue;
			}
			
			var cluster = curElement.clusterId;
			if (numClusters < parseInt(cluster)) {
				numClusters = parseInt(cluster);
			}

			var label = curElement.data.label;
			var color = curElement.data.bg;
			
			var newNode = {
					"index" : index++,
					"id" : iri,
					"name" : label,
					"color" : color,
					"radius" : 5, 
					"cluster" : cluster
				}; 
				
			nodes.push(newNode);
		} 
	}
	
	for (var i = 0; i < arrayLength; i++) {
		var curElement = elementData[i];
		if (curElement.group && curElement.group == "edges") {
			var iri = curElement.data.id;
			if (containsElement(edges, iri)) {
				continue;
			}
			
			var cluster = curElement.clusterId;
			var label = curElement.data.label;
			var color = curElement.data.color;
			var source = curElement.data.source;
			var target = curElement.data.target;
			var fromId = getNodeId(source,nodes);
			var toId = getNodeId(target,nodes);
			
			var newEdge = {
					"index" : index++,
					"id" : iri,
					"name" : label,
					"color" : color,
					"source" : source, 
					"target" : target, 
					"from" : fromId, 
					"to" : toId, 
					"cluster" : cluster
				}; 
				
			edges.push(newEdge);
		}
	}
	
	nodeslinks.nodes = nodes;
	nodeslinks.links = edges;
	return nodeslinks;
}

function containsElement(elements, iri) {
	var arrayLength = elements.length;
	for (var i = 0; i < arrayLength; i++) {
		var curElement = elements[i];
		if (curElement.id == iri) {
			return true;
		}
	}
}

function getNodeId(iri, nodes) {
	var arrayLength = nodes.length;
	for (var i = 0; i < arrayLength; i++) {
		var curNode = nodes[i];
		if (curNode.id == iri) {
			return curNode.index;
		}
	}
}

function hexToHex(hex){
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        return c;
    }
    throw new Error('Bad Hex');
}

function hexToRgb(hex) {
	  var result = /^0x?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	  var red = parseInt(result[1], 16)/255.0;
	  var green = parseInt(result[2], 16)/255.0;
	  var blue = parseInt(result[3], 16)/255.0;
	  if (red >= 0.9 && green >= 0.9 && blue >= 0.9) {
		  red = green = blue = 0;
	  }
	  var rgb = [];
	  rgb.push(red);
	  rgb.push(green);
	  rgb.push(blue);
	  rgb.push(1);
	  
	  if (result) {
		  return rgb;
	  }
	  
	  return null;
}