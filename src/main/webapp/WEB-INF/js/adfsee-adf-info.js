/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
var data;
var treeData;
var adfInfo;
var tabledata;
var loadafo = true;
var loadaft = true;
var loadexample = false;
var multigraph = false;
var skolemize = false;
var includeadf = false;
var columnWidths;
var columnHeaders;

initGraphStore();

document.addEventListener('DOMContentLoaded', function() {
		var loading = document.getElementById('loading');
		loading.classList.remove('loaded');

		if (localStorage.getItem('adfInfoStatus') && localStorage.getItem('adfInfoStatus') == freshStartStatus) {
			resetLocalDbAndDataOfAdfInfos().then(createAdfInfo);
		} else if (localStorage.getItem('adfInfoStatus') && localStorage.getItem('adfInfoStatus') == adfInfoAvailableStatus) {
			selectAllFromAdfInfos()
				.then(updateAdfInfoStateWithSelectedResults)
				.then(createAdfInfo);
		} else {
			localStorage.setItem('adfInfoStatus', freshStartStatus);
			resetLocalDbAndDataOfAdfInfos()
				.then(function() {
					console	.log("No state information found in local adfInfo store. Restarting from scratch.");
					return Promise.resolve();
				})
				.then(createAdfInfo);
		}

		$('#collapse').off("click").click(function() {
			$('#hdfstructure').html("");
			$('#adfversion').html("");
			$('#adfchecksum').html("");
			$('#ddstructure').html("");
			createRenderJson(0);
		});

		$('#expand').off("click").click(function() {
			$('#hdfstructure').html("");
			$('#hdfstructure').html("");
			$('#adfversion').html("");
			$('#adfchecksum').html("");
			$('#ddstructure').html("");
			createRenderJson("all");
		});    	

		loading.classList.add('loaded');
});

async function activeButtons() {
	document.getElementById("expand").classList.remove("inactivebutton");
	document.getElementById("expand").classList.add("activebutton");
	document.getElementById("collapse").classList.remove("inactivebutton");
	document.getElementById("collapse").classList.add("activebutton");
	return Promise.resolve();
}

async function inactiveButtons() {
	document.getElementById("expand").classList.remove("activebutton");
	document.getElementById("expand").classList.add("inactivebutton");
	document.getElementById("collapse").classList.remove("activebutton");
	document.getElementById("collapse").classList.add("inactivebutton");
	return Promise.resolve();
}

async function createRenderJson(level) {
	console.log("renderjson");
	var loading = document.getElementById('loading');
	loading.classList.remove('loaded');
	var unescapedAdfInfo = adfInfo;
	unescapedAdfInfo = htmlUnescape(JSON.stringify(unescapedAdfInfo));
	unescapedAdfInfo = JSON.parse(unescapedAdfInfo);
	document.getElementById("hdfstructure").appendChild(renderjson.set_show_by_default(true).set_show_to_level(level).set_sort_objects(true).set_icons('+', '-').set_max_string_length(200)(unescapedAdfInfo["hdfStructure"]));
	document.getElementById("adfversion").appendChild(renderjson.set_show_by_default(true).set_show_to_level(level).set_sort_objects(true).set_icons('+', '-').set_max_string_length(200)(unescapedAdfInfo["versionInfo"]));
	document.getElementById("adfchecksum").appendChild(renderjson.set_show_by_default(true).set_show_to_level(level).set_sort_objects(true).set_icons('+', '-').set_max_string_length(200)(unescapedAdfInfo["checksumInfo"]));
	document.getElementById("ddstructure").appendChild(renderjson.set_show_by_default(true).set_show_to_level(level).set_sort_objects(true).set_icons('+', '-').set_max_string_length(200)(unescapedAdfInfo["ddStructure"]));
	loading.classList.add('loaded');
	return Promise.resolve();
}

/** To stop XSS attacks by using JSON with HTML nodes **/
function htmlUnescape(str) {
	const tagsToReplace = {
		"&amp;": "&",
		"&lt;": "<",
		"&gt;": ">"
	};
	return str.replace(/(&amp;|&lt;|&gt;)/g, function(tag) {
		return tagsToReplace[tag] || tag;
	});
}

async function createAdfInfo() {
	console.log("adf info");
	var loading = document.getElementById('loading');
	loading.classList.remove('loaded');
	try {
		createRenderJson(5);
		activeButtons();
	} catch (error) {
		console.log("Error during creation of adfInfo: " + error);
		loading.classList.add('loaded');
	} finally {
		loading.classList.add('loaded');
		return Promise.resolve();
	}
}