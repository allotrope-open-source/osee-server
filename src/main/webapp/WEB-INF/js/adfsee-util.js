/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
var counter = 0;

async function anyJson2NormalizedView(jsonObject, parentObjectNormalized) {
	try {
		console.log("HANDLING jsonObject= " + JSON.stringify(JSON.decycle(jsonObject)));
		var name = "";

		if (!parentObjectNormalized) {
			counter = 0;
			parentObjectNormalized = {};
			parentObjectNormalized.name = "root";
			parentObjectNormalized.children = [];
		}
		
		name = await getName(jsonObject, parentObjectNormalized, name);

		if (isArray(jsonObject)) {
			var children = [];
			for (i in jsonObject) {
				console.log("i= " + i);
				// handle arrayitem
				if (isArray(jsonObject[i])) {
					// handle array
					child = await handleArray(jsonObject[i], name);
					children.push(child);
				} else if (isObject(jsonObject[i])) {
					// handle object
					child = await handleObject(jsonObject[i], name);
					children.push(child);
				} else {
					// handle literal
					console.log("SKIPPED LITERAL jsonObject[" + i + "]= " + JSON.stringify(JSON.decycle(jsonObject[i])));
					continue;
				}
			}
			parentObjectNormalized.children = children;
			if (!parentObjectNormalized.name) {
				parentObjectNormalized.name = name;
			}
		} else if (isObject(jsonObject)) {
			name = await getName(jsonObject, parentObjectNormalized, name);
			var children = null;
			for (i in jsonObject) {
				console.log("i= " + i);
				// handle field of object
				if (isArray(jsonObject[i])) {
					if (!children) {
						children = [];
					}
					// handle array
					child = await handleArray(jsonObject[i], name);
					children.push(child);
				} else if (isObject(jsonObject[i])) {
					if (!children) {
						children = [];
					}
					// handle object
					child = await handleObject(jsonObject[i], name);
					children.push(child);
				} else {
					// handle literal
					console.log("SKIPPED LITERAL jsonObject[" + i + "]= " + JSON.stringify(JSON.decycle(jsonObject[i])));
					continue;
				}
				parentObjectNormalized.children = children;
				if (!parentObjectNormalized.name) {
					parentObjectNormalized.name = name;
				}
			}
		} else {
			// handle literal
			console.log("SKIPPED LITERAL jsonObject= " + JSON.stringify(JSON.decycle(jsonObject)));
		}
	
		return Promise.resolve(parentObjectNormalized);
	} catch(error) {
		console.log("Error during normalization of json: " + error);
		return Promise.reject();
	}
}

async function handleArray(jsonObject, name) {
	var name = await getName(jsonObject, null, name);
	var parentObject = {};
	parentObject.name = name;
	var normalizedObject = await anyJson2NormalizedView(jsonObject, parentObject); 
	return Promise.resolve(normalizedObject);
}

async function handleObject(jsonObject, name) {
	var name = await getName(jsonObject, null, name);
	var parentObject = {};
	parentObject.name = name;
	var normalizedObject = await anyJson2NormalizedView(jsonObject, parentObject); 
	return Promise.resolve(normalizedObject);
}

async function getName(jsonObject, parentObject, name) {
	if (isArray(jsonObject)) {
		if (parentObject) {
			name = parentObject.name;
		} else if (!name) {
			name = "object" + counter++;
		}
	} else {
		for (i in jsonObject) {
			if (typeof i !== 'string' && !(i instanceof String)) {
				continue;
			}
			var field = i.toLowerCase();
			if ((field === "label" || field === "name" || field === "title") && !name) {
				// use label/name/title if id is missing
				name = jsonObject[i];
			} else if ((field === "id" || field === "identifier")) {
				// use id (overwrites label)
				name = jsonObject[i];
				break;
			} 
		}
	}
	if (!name) {
		name = "object" + counter++;
	}
	console.log("name= " + name);
	return Promise.resolve(name);
}

function isArrayAndParentIsNotArray(object, parentobject) {
	return isArray(object) && Object.prototype.toString.call(parentobject) != '[object Array]';
}

function isArrayAndParentIsArray(object, parentobject) {
	 return isArray(object) && Object.prototype.toString.call(parentobject) == '[object Array]';
}

function isArray(object) {
	return isObject(object) && Object.prototype.toString.call(object) == '[object Array]';
}

function isObjectAndParentIsNotArray(object, parentobject) {
	return isObject(object) && Object.prototype.toString.call(parentobject) != '[object Array]';
}

function isObject(object) {
	return object && typeof object === 'object';
}

function getNodesAndLinksFromCytoscapeGraph(elementData, nodeslinks, parentIndex) {
	var arrayLength = elementData.length;
	var nodes = [];
	var edges = [];
	var index = 0;
	for (var i = 0; i < arrayLength; i++) {
		var curElement = elementData[i];
		if (curElement.group && curElement.group == "nodes") {
			var iri = curElement.data.id;
			if (containsElement(nodes, iri)) {
				continue;
			}
			
			var cluster = curElement.clusterId;
			if (numClusters < parseInt(cluster)) {
				numClusters = parseInt(cluster);
			}

			var label = curElement.data.label;
			
			var detail = "";
			if (curElement.data.detail) {
				detail = curElement.data.detail;
			}

			var popup = [];
			if (curElement.data.popup) {
				popup = curElement.data.popup;
			}

			var newNodeColor = "#FFFFFF";
			
			if (curElement.domains && curElement.domains[0]) {
				if (colorValues[curElement.domains[0]]) {
					newNodeColor = colorValues[curElement.domains[0]];
				}
			} else if (curElement.data && curElement.data.bg) {
				newNodeColor = curElement.data.bg;
			} else if (curElement.color) {
				newNodeColor = curElement.color;
			}
			
			var bg = curElement.data.bg;
			var shape = curElement.data.shape;
			var borderwidth = curElement.data.borderwidth;

			var newNode = {
					"id" : index++,
					"iri" : iri,
					"name" : label,
					"detail" : detail,
					"popup" : popup,
					"color" : hexToHex(newNodeColor),
					"bg" : bg,
					"shape" : shape,
					"borderwidth" : borderwidth,
					"radius" : 5, 
					"categories" : domains,
					"cluster" : cluster
				}; 
				
			nodes.push(newNode);
		} 
	}
	
	for (var i = 0; i < arrayLength; i++) {
		var curElement = elementData[i];
		if (curElement.group && curElement.group == "edges") {
			var iri = curElement.data.id;
			if (containsElement(edges, iri)) {
				continue;
			}
			

			var domains = [];
			if (curElement.domains) {
				domains = curElement.domains;
			}
			
			var cluster = curElement.clusterId;
			var label = curElement.data.label;
			var detail = curElement.data.detail;
			var color = curElement.data.color;
			var curvestyle = curElement.data.curveStyle;
			var linewidth = curElement.data.lineWidth;
			var linecolor = curElement.data.lineColor;
			var supertype = curElement.data.supertype;
			var propertyIri = curElement.data.iri;
			var source = curElement.data.source;
			var target = curElement.data.target;
			var fromId = getNodeId(source,nodes);
			var toId = getNodeId(target,nodes);

			var newEdge = {
					"id" : index++,
					"nodeiri" : iri,
					"propertyiri" : propertyIri,
					"supertype" : supertype,
					"text" : label,
					"detail" : detail,
					"color" : color,
					"curvestyle" : curvestyle,
					"linewidth" : linewidth,
					"linecolor" : linecolor,
					"source" : fromId, 
					"target" : toId, 
					"sourceIri" : source, 
					"targetIri" : target, 
					"cluster" : cluster,
					"curvature" : 0.5,
					"rotation" : Math.PI * 1 / 6,
	   				"categories" : domains,
	   				"value" : 1.0
				}; 
				
			edges.push(newEdge);
		}
	}
	
	nodeslinks.nodes = nodes;
	nodeslinks.links = edges;
	return nodeslinks;
}

function containsElement(elements, iri) {
	var arrayLength = elements.length;
	for (var i = 0; i < arrayLength; i++) {
		var curElement = elements[i];
		if (curElement.id == iri) {
			return true;
		}
	}
}

function getNodeId(iri, nodes) {
	var arrayLength = nodes.length;
	for (var i = 0; i < arrayLength; i++) {
		var curNode = nodes[i];
		if (curNode.iri == iri) {
			return curNode.id;
		}
	}
}

function hexToRgbA(hex){
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+',1)';
    }
    throw new Error('Bad Hex');
}

function hexToHex(hex){
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        return c;
    }
    throw new Error('Bad Hex');
}

function hexToRgb(hex) {
	  var result = /^0x?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	  var red = parseInt(result[1], 16)/255.0;
	  var green = parseInt(result[2], 16)/255.0;
	  var blue = parseInt(result[3], 16)/255.0;
	  if (red >= 0.9 && green >= 0.9 && blue >= 0.9) {
		  red = green = blue = 0;
	  }
	  var rgb = [];
	  rgb.push(red);
	  rgb.push(green);
	  rgb.push(blue);
	  rgb.push(1);
	  
	  if (result) {
		  return rgb;
	  }
	  
	  return null;
}