/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
var data;
var treeData;
var loadafo = true;
var loadaft = true;
var dolayout = true;
var multigraph = false;
var skolemize = false;
var loadexample = false;
var tree;
var includeadf = false;

initGraphStore();

document.addEventListener('DOMContentLoaded',function(){
	if (tree) {
		tree.removeAll();
	}
	if (localStorage.getItem('treeStatus') && localStorage.getItem('treeStatus') == freshStartStatus) {
		resetLocalDbAndDataOfTrees()
		.then(initTree);
	} else if (localStorage.getItem('treeStatus') && localStorage.getItem('treeStatus') == treeAvailableStatus) {
		selectAllFromTrees()
		.then(updateTreeStateWithSelectedResults)
		.then(initTree);
	} else {
		localStorage.setItem('treeStatus', freshStartStatus);
		resetLocalDbAndDataOfTrees()
		.then(function() {
			console.log("No state information found in local tree store. Restarting from scratch.");
			return Promise.resolve();
		})
		.then(initTree);
	}
});

async function initTree() {
	let promise = await new Promise(
		function (resolve, reject) {
			var loading = document.getElementById('loading');
			loading.classList.remove('loaded');
			try {
				resetTree(tree)
					.then(configRequireJs)
					.then(createInspireTree)
					.then(refreshTree)
					.then(expandLoaded)
					.then(logSuccess)
					.then(reload);
			} catch (error) {
				console.log("Error during drawing tree: " + error);
				loading.classList.add('loaded');
				reject();
			}
			resolve(tree);
		}
	)
	.catch(error => { 
		console.log("Error during drawing inspire tree: " + error);
	});
	return promise;
}

async function drawTree() {
	let promise = await new Promise(
		function (resolve, reject) {
			var loading = document.getElementById('loading');
			loading.classList.remove('loaded');
			try {
				resetTree(tree)
					.then(updateInspireTree)
					.then(refreshTree)
					.then(expandLoaded)
					.then(logSuccess)
					.then(reload);
			} catch (error) {
				console.log("Error during drawing tree: " + error);
				loading.classList.add('loaded');
				reject();
			}
			resolve(tree);
		}
	)
	.catch(error => { 
		console.log("Error during drawing inspire tree: " + error);
	});
	return promise;
}

$("#expand").click(function () {
	tree.expandDeep();                  
});

$("#collapse").click(function () {
	tree.collapseDeep();                  
});

function reload(tree) {
	var theTree = document.getElementById('textualtree');
	if (!tree) {
		var reloadAllowed = localStorage.getItem('reloadAllowed');
		if (reloadAllowed && reloadAllowed == "1") {
			localStorage.setItem('reloadAllowed',"0");
			window.location.reload();
		} else if (!reloadAllowed) {
			localStorage.setItem('reloadAllowed',"0");
			window.location.reload();
		} else if (reloadAllowed && reloadAllowed == "0") {
			localStorage.setItem('reloadAllowed',"1");
		}
	}
	return Promise.resolve();
}

function logSuccess(newTree) {
	console.log("Tree drawn successfully.");
	loading.classList.add('loaded');
	tree = newTree;
	return Promise.resolve(tree);
}

function expandLoaded(tree) {
	tree.on('model.loaded', function() {
		tree.expand();
		tree.selectFirstAvailableNode();
	});
	return Promise.resolve(tree);
}

function refreshTree(tree) {
	tree.applyChanges();
	return Promise.resolve(tree);
}

function resetTree(tree) {
	if (tree) {
		tree.removeAll();
	}
	return Promise.resolve(tree);
}

function configRequireJs(tree) {
	try {
		require.config({
		    paths: {
		        'inspire-tree': '/js/inspire-tree',
		        'inspire-tree-dom': '/js/inspire-tree-dom',
		        'lodash': '/js/lodash',
		        'prism': '/js/prism'
		    }
		});
	} catch(error) {
		console.log("Error during config of requireJs: " + error);
		return Promise.reject()
	}
	return Promise.resolve(tree);
}

async function createInspireTree(tree) {
	let promise = await new Promise(
		function (resolve, reject) {
			require(['inspire-tree', 'inspire-tree-dom', 'prism'], function(InspireTree, InspireTreeDOM) {
			    tree = new InspireTree({
			        data: treeData,  
			        selection: {
			        	multiple: true
					},
					sort: "text",
					editable: true
			    });
			
			    new InspireTreeDOM(tree, {
			        target: '#textualtree', 
			        dragAndDrop: true, 
			        sort: "text"
			    });

			    resolve(tree);
			});
		}
	)
	.catch(error => { 
		console.log("Error during creation of inspire tree: " + error);
	});
	return promise;
}

async function updateInspireTree(tree) {
	let promise = await new Promise(
		function (resolve, reject) {
			try {
				tree.load(treeData);
			    resolve(tree);
			} catch(error) {
				console.log("Error during loading new data into inspire tree: " + error);
				reject();
			}	
		}
	)
	.catch(error => { 
		console.log("Error during updating inspire tree: " + error);
	});
	return promise;
}
