/**
*
* @author Helge.Krieg, OSTHUS GmbH
*
*/
document.addEventListener('DOMContentLoaded',function(){
	loading.classList.remove('loaded');
	drawYasGui();
});

async function drawYasGui() {
	let promise = await new Promise(
		function (resolve, reject) {
			loading.classList.add('loaded');
			window.yasgui = new Yasgui(document.getElementById("yasgui"), {
				corsProxy: "",
				requestConfig: {
					endpoint: " http://localhost:9999/adfsee/sparql",
				}
			});
			resolve(true);
		}
	)
	.catch(error => { 
		loading.classList.add('loaded');
		console.log("Error during creation of yasgui: " + error);
	});
	return promise;
}