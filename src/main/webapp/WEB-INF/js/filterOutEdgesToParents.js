/**
 * @author Helge.Krieg, OSTHUS GmbH
 */
function getEdgesToParents( cy ) {
	var edges = cy.edges();
	var filteredEdges = [];
	for(var i = 0; i < edges.length; i++){
		var curEdge = edges[i];
		var id = curEdge.data('id');
		var sourceId = curEdge.data('source');
		var targetId = curEdge.data('target');
		var sourceNode = cy.elements().getElementById(sourceId);
		var targetNode = cy.elements().getElementById(targetId);
		if ((sourceNode.data('parent') && sourceNode.data('parent') === targetId) ||(targetNode.data('parent') && targetNode.data('parent') === sourceId)) {
			filteredEdges.push(curEdge);
		};
	};
	return filteredEdges;
};

function removeEdges( cy, edgesToRemove ) {
	for(var i = 0; i < edgesToRemove.length; i++){
		var id = edgesToRemove[i].data('id');
		cy.remove(cy.elements().getElementById(id));
	}
	return cy;
};

function addEdges( cy, edgesToAdd ) {
	for(var i = 0; i < edgesToAdd.length; i++){
		cy.add(edgesToAdd[i]);
	}
	return cy;
};

