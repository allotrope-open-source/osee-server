/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
var freshStartStatus = 'started from scratch';
var graphAvailableStatus = 'graph data is available';
var datacubesAvailableStatus = 'datacubes data is available';
var data;
var treeData;
var queryResultsData;
var datacubes;
var initialPositions;
var d3Data = [];

var loadafo = true;
var loadaft = true;
var dolayout = true;
var multigraph = false;
var skolemize = false;
var loadexample = false;
var includeadf = false;
var counter; 

var loading = document.getElementById('loading');
	
initGraphStore();

document.addEventListener('DOMContentLoaded',function(){
	loading.classList.remove('loaded');
	if (localStorage.getItem('treeStatus') && localStorage.getItem('treeStatus') == freshStartStatus) {
		resetLocalDbAndDataOfTrees()
		.then(prepareData)
		.then(createD3);
	} else if (localStorage.getItem('treeStatus') && localStorage.getItem('treeStatus') == treeAvailableStatus) {
		selectAllFromTrees()
			.then(updateTreeStateWithSelectedResults)
			.then(prepareData)
			.then(createD3);
	} else {
		localStorage.setItem('treeStatus', freshStartStatus);
		resetLocalDbAndDataOfTrees()
			.then(prepareData)
			.then(createD3);
		console.log("No state information found in local tree store. Restarting from scratch.");
	}
	loading.classList.add('loaded');
});

async function drawD3() {
	var list = document.getElementById("d3graph");
	if (list.hasChildNodes()) {
	    list.removeChild(list.childNodes[0]);
	}
	d3Data = await prepareData();
	await createD3(d3Data);
	return Promise.resolve();
}

async function prepareData() {
	try {
		if (!treeData) {
			console.log("No treeData found.");
			return Promise.reject();
		}
	
		counter = 0;
		d3Data = [];
		d3Data = await getNodesAndConnections(treeData);
		
	} catch(error) {
		console.log("Error during preparation of data for D3 hive plot: " + error);
		loading.classList.add('loaded');
		return Promise.reject();
	}
	
	return Promise.resolve(d3Data);
}

async function getNodesAndConnections(nodeData) {
	var arrayLength = nodeData.length;
	for (var i = 0; i < arrayLength; i++) {
		var curNode = nodeData[i]
		var	label = curNode.label;

		var domains = [];
		if (curNode.domains) {
			domains = curNode.domains;
		}

		var children = curNode.children;

		var newNode = {
			"id" : counter++,
			"name" : label,
			"categories" : domains, 
			"imports" : children
		}; 
		d3Data.push(newNode);

		if (children) {
			getNodesAndConnections(children);
		}
	}
	return d3Data;

}


async function createD3(d3Data) {
	try {
		console.log("creating D3 graph");
		loading.classList.remove('loaded');
		
		document.getElementById('plot').addEventListener('click', function () {
			var plot = document.querySelector('input[name="plot"]:checked').value;
			console.log("showing plot: " + plot);
		});
		
		document.getElementById('hive').click();
		
		await createHivePlot(d3Data);
		loading.classList.add('loaded');
	} catch (error) {
		console.log("Error during creation of D3 graph: " + error);
		loading.classList.add('loaded');
		return Promise.reject();
	}
	return Promise.resolve();
}

async function createHivePlot(d3Data) {
	try {
		var width = $(window).width(); //960,
		    height = $(window).height() - 100; //850,
		    innerRadius = 40,
		    outerRadius = 640,
		    majorAngle = 2 * Math.PI / 3,
		    minorAngle = 1 * Math.PI / 12;
	
		var angle = d3.scale.ordinal()
		    //.domain(["process", "material", "equipment", "result", "information", "quality", "function", "role", "other"])
		    .domain(["source", "source-target", "target-source", "target"])
		    .range([0, majorAngle - minorAngle, majorAngle + minorAngle, 2 * majorAngle]);
	
		var radius = d3.scale.linear()
		    .range([innerRadius, outerRadius]);
	
		var color = d3.scale.category10();
	
		var svg = d3.select("#d3graph").append("svg")
		    .attr("width", width)
		    .attr("height", height)
		    .append("g")
		    .attr("transform", "translate(" + width * .50 + "," + height * .8 + ")");
	
		await prepareNodes(d3Data);
		
		async function prepareNodes(nodes) {
		var nodesByName = {},
		      links = [],
		      formatNumber = d3.format(",d"),
		      defaultInfo;
	
		  // Construct an index by node name.
		  nodes.forEach(function(d) {
		    d.connectors = [];
		    d.packageName = d.name.split(".")[1];
		    nodesByName[d.name] = d;
		  });
	
		  // Convert the import lists into links with sources and targets.
		  nodes.forEach(function(source) {
			if (source.imports) {
			    source.imports.forEach(function(targetNode) {
			      var targetName = targetNode.label;	
			      var target = nodesByName[targetName];
			      if (!source.source) source.connectors.push(source.source = {node: source, degree: 0});
			      if (!target.target) target.connectors.push(target.target = {node: target, degree: 0});
			      links.push({source: source.source, target: target.target});
			    });
			}
		  });
	
		  // Determine the type of each node, based on incoming and outgoing links.
		  nodes.forEach(function(node) {
		    if (node.source && node.target) {
		      node.type = node.source.type = "target-source";
		      node.target.type = "source-target";
		    } else if (node.source) {
		      node.type = node.source.type = "source";
		    } else if (node.target) {
		      node.type = node.target.type = "target";
		    } else {
		      node.connectors = [{node: node}];
		      node.type = "source";
		    }
		  });
//		  nodes.forEach(function(node) {
//			if (!node.categories) {
//				node.type = "other";
//				return;
//			}
//			
//			if ( $.inArray( 'INFORMATION', node.categories ) > -1 ) {
//				node.type = "information";
//				return;
//			}
//			
//			if ( $.inArray( 'MATERIAL', node.categories ) > -1 ) {
//				node.type = "material";
//				return;
//			}
//			
//			if ( $.inArray( 'PROCESS', node.categories ) > -1 ) {
//				node.type = "process";
//				return;
//			}
//			
//			if ( $.inArray( 'RESULT', node.categories ) > -1 ) {
//				node.type = "result";
//				return;
//			}
//			
//			if ( $.inArray( 'EQUIPMENT', node.categories ) > -1 ) {
//				node.type = "equipment";
//				return;
//			}
//			
//			if ( $.inArray( 'QUALITY', node.categories ) > -1 ) {
//				node.type = "quality";
//				return;
//			}
//			
//			if ( $.inArray( 'FUNCTION', node.categories ) > -1 ) {
//				node.type = "function";
//				return;
//			}
//			
//			if ( $.inArray( 'ROLE', node.categories ) > -1 ) {
//				node.type = "role";
//				return;
//			}
//			
//			node.type = "other";
//			return;
//		  });
	
		  // Initialize the info display.
		  var info = d3.select("#info")
		      .text(defaultInfo = "Showing " + formatNumber(links.length) + " relations among " + formatNumber(nodes.length) + " nodes.");
	
		  // Nest nodes by type, for computing the rank.
		  var nodesByType = d3.nest()
		      .key(function(d) { return d.type; })
		      .sortKeys(d3.ascending)
		      .entries(nodes);
	
		  // Duplicate the target-source axis as source-target.
		  nodesByType.push({key: "source-target", values: nodesByType[2].values});

		  // Compute the rank for each type, with padding between packages.
		  nodesByType.forEach(function(type) {
		    var lastName = type.values[0].packageName, count = 0;
		    type.values.forEach(function(d, i) {
		      if (d.packageName != lastName) lastName = d.packageName, count += 2;
		      d.index = count++;
		    });
		    type.count = count - 1;
		  });
	
		  // Set the radius domain.
		  radius.domain(d3.extent(nodes, function(d) { return d.index; }));
	
		  // Draw the axes.
		  svg.selectAll(".axis")
		      .data(nodesByType)
		    .enter().append("line")
		      .attr("class", "axis")
		      .attr("transform", function(d) { return "rotate(" + degrees(angle(d.key)) + ")"; })
		      .attr("x1", radius(-2))
		      .attr("x2", function(d) { return radius(d.count + 2); });
	
		  // Draw the links.
		  svg.append("g")
		      .attr("class", "links")
		    .selectAll(".link")
		      .data(links)
		    .enter().append("path")
		      .attr("class", "link")
		      .attr("d", link()
		      .angle(function(d) { return angle(d.type); })
		      .radius(function(d) { return radius(d.node.index); }))
		      .on("mouseover", linkMouseover)
		      .on("mouseout", mouseout);
	
		  // Draw the nodes. Note that each node can have up to two connectors,
		  // representing the source (outgoing) and target (incoming) links.
		  svg.append("g")
		      .attr("class", "nodes")
		    .selectAll(".node")
		      .data(nodes)
		    .enter().append("g")
		      .attr("class", "node")
		      .style("fill", function(d) { return color(d.type); })
		    .selectAll("circle")
		      .data(function(d) { return d.connectors; })
		    .enter().append("circle")
		      .attr("transform", function(d) { return "rotate(" + degrees(angle(d.type)) + ")"; })
		      .attr("cx", function(d) { return radius(d.node.index); })
		      .attr("r", 4)
		      .on("mouseover", nodeMouseover)
		      .on("mouseout", mouseout);
	
		  // Highlight the link and connected nodes on mouseover.
		  function linkMouseover(d) {
		    svg.selectAll(".link").classed("active", function(p) { return p === d; });
		    svg.selectAll(".node circle").classed("active", function(p) { return p === d.source || p === d.target; });
		    info.text(d.source.node.name + " is connected to " + d.target.node.name);
		  }
	
		  // Highlight the node and connected links on mouseover.
		  function nodeMouseover(d) {
		    svg.selectAll(".link").classed("active", function(p) { return p.source === d || p.target === d; });
		    d3.select(this).classed("active", true);
		    info.text(d.node.name);
		  }
	
		  // Clear any highlighted nodes or links.
		  function mouseout() {
		    svg.selectAll(".active").classed("active", false);
		    info.text(defaultInfo);
		  }
		  return Promise.resolve();
		};
	} catch(error) {
		console.log("Error during creation of D3 plot: " + error);
		loading.classList.add('loaded');
		return Promise.reject();
	}
	
	return Promise.resolve();
}

function link() {
  var source = function(d) { return d.source; },
      target = function(d) { return d.target; },
      angle = function(d) { return d.angle; },
      startRadius = function(d) { return d.radius; },
      endRadius = startRadius,
      arcOffset = -Math.PI / 2;

  function link(d, i) {
    var s = node(source, this, d, i),
        t = node(target, this, d, i),
        x;
    if (t.a < s.a) x = t, t = s, s = x;
    if (t.a - s.a > Math.PI) s.a += 2 * Math.PI;
    var a1 = s.a + (t.a - s.a) / 3,
        a2 = t.a - (t.a - s.a) / 3;
    return s.r0 - s.r1 || t.r0 - t.r1
        ? "M" + Math.cos(s.a) * s.r0 + "," + Math.sin(s.a) * s.r0
        + "L" + Math.cos(s.a) * s.r1 + "," + Math.sin(s.a) * s.r1
        + "C" + Math.cos(a1) * s.r1 + "," + Math.sin(a1) * s.r1
        + " " + Math.cos(a2) * t.r1 + "," + Math.sin(a2) * t.r1
        + " " + Math.cos(t.a) * t.r1 + "," + Math.sin(t.a) * t.r1
        + "L" + Math.cos(t.a) * t.r0 + "," + Math.sin(t.a) * t.r0
        + "C" + Math.cos(a2) * t.r0 + "," + Math.sin(a2) * t.r0
        + " " + Math.cos(a1) * s.r0 + "," + Math.sin(a1) * s.r0
        + " " + Math.cos(s.a) * s.r0 + "," + Math.sin(s.a) * s.r0
        : "M" + Math.cos(s.a) * s.r0 + "," + Math.sin(s.a) * s.r0
        + "C" + Math.cos(a1) * s.r1 + "," + Math.sin(a1) * s.r1
        + " " + Math.cos(a2) * t.r1 + "," + Math.sin(a2) * t.r1
        + " " + Math.cos(t.a) * t.r1 + "," + Math.sin(t.a) * t.r1;
  }

  function node(method, thiz, d, i) {
    var node = method.call(thiz, d, i),
        a = +(typeof angle === "function" ? angle.call(thiz, node, i) : angle) + arcOffset,
        r0 = +(typeof startRadius === "function" ? startRadius.call(thiz, node, i) : startRadius),
        r1 = (startRadius === endRadius ? r0 : +(typeof endRadius === "function" ? endRadius.call(thiz, node, i) : endRadius));
    return {r0: r0, r1: r1, a: a};
  }

  link.source = function(_) {
    if (!arguments.length) return source;
    source = _;
    return link;
  };

  link.target = function(_) {
    if (!arguments.length) return target;
    target = _;
    return link;
  };

  link.angle = function(_) {
    if (!arguments.length) return angle;
    angle = _;
    return link;
  };

  link.radius = function(_) {
    if (!arguments.length) return startRadius;
    startRadius = endRadius = _;
    return link;
  };

  link.startRadius = function(_) {
    if (!arguments.length) return startRadius;
    startRadius = _;
    return link;
  };

  link.endRadius = function(_) {
    if (!arguments.length) return endRadius;
    endRadius = _;
    return link;
  };

  return link;
}

function degrees(radians) {
  return radians / Math.PI * 180 - 90;
}