/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
var data;
var treeData;
var datadescription;
var tabledata;
var loadafo = true;
var loadaft = true;
var dolayout = true;
var multigraph = false;
var skolemize = false;
var loadexample = false;
var includeadf = false;
var columnWidths;
var columnHeaders;

initGraphStore();

document.addEventListener('DOMContentLoaded', function() {
		var loading = document.getElementById('loading');
		loading.classList.remove('loaded');

		if (localStorage.getItem('datadescriptionStatus') && localStorage.getItem('datadescriptionStatus') == freshStartStatus) {
			resetLocalDbAndDataOfDatadescription().then(createDatadescription);
		} else if (localStorage.getItem('datadescriptionStatus') && localStorage.getItem('datadescriptionStatus') == datadescriptionAvailableStatus) {
			selectAllFromDatadescription()
				.then(updateDatadescriptionStateWithSelectedResults)
				.then(createDatadescription);
		} else {
			localStorage.setItem('datadescriptionStatus', freshStartStatus);
			resetLocalDbAndDataOfDatadescription()
				.then(function() {
					console	.log("No state information found in local datadescription store. Restarting from scratch.");
					return Promise.resolve();
				})
				.then(createDatadescription);
		}
});

function createDatadescription() {
	var loading = document.getElementById('loading');
	loading.classList.remove('loaded');
	var tempTree = [];
	tempTree.push(datadescription);
	try {
		if ($('#datadescription').tree('getTree')) {
			$('#datadescription').tree('loadData', tempTree);
		} else {
			$(function() {
				$('#datadescription').tree({
					data: tempTree,
					dragAndDrop: false,
					autoOpen: 3, 
					saveState: true,
					animationSpeed: 100,
					useContextMenu: false,
					autoEscape: false,
					onCreateLi: function(node, $li) {
						if (node.name === "dataset" || node.name === "zipped") {
							$li.find('.jqtree-element').append('<a href="' + node.url +'" class="download" download><span style="font-weight:900;">&nbsp;&nbsp;&#x21B7;</span></a>');
						} else if (node.children.length <= 0) {
							$li.find('.jqtree-element').append('<a href="' + node.url +'" class="download" download><span style="font-weight:900;">&nbsp;&nbsp;&#x21B7; TTL</span></a>');
							$li.find('.jqtree-element').append('<a href="' + node.urlN3 +'" class="download" download><span style="font-weight:900;">&nbsp;&nbsp;&#x21B7; N3</span></a>');
							$li.find('.jqtree-element').append('<a href="' + node.urlSkolemized +'" class="download" download><span style="font-weight:900;">&nbsp;&nbsp;&#x21B7; TTL<sub><i>skolemized</i></sub></span></a>');
							$li.find('.jqtree-element').append('<a href="' + node.urlN3Skolemized +'" class="download" download><span style="font-weight:900;">&nbsp;&nbsp;&#x21B7; N3<sub><i>skolemized</i></sub></span></a>');
						}
			        },
					closedIcon: $('<i class="fa fa-arrow-circle-right"></i>'),
					openedIcon: $('<i class="fa fa-arrow-circle-down"></i>')
				});
			});
			
			var $tree = $('#datadescription');
			$tree.on(
			    'tree.click',
			    function(event) {
			        event.preventDefault();
			        var node = event.node;
			        $("#details").html("");
			        var htmlString = "";
			        var keys = Object.keys(node);
			        for (var key in keys) {
			        	var value = node[keys[key]];
			        	if (isObject(value)) {
			        		continue;
			        	}
			        	htmlString = htmlString + "<span class=\"key\">" + keys[key] + ":</span>&nbsp;&nbsp;<span class=\"value\">" + value + "</span><br/>";
			        }
			        $("#details").html(htmlString);
			    }
			);
		}
	} catch (error) {
		console.log("Error during drawing jqtree of datadescription: " + error);
		loading.classList.add('loaded');
		return Promise.reject();
	}
	finally {
		loading.classList.add('loaded');

		$('#collapse').click(function() {
			var tree = $('#datadescription').tree('getTree');
			tree.iterate(function(node) {
				if (node.hasChildren()) {
					$('#datadescription').tree('closeNode', node, true);
				}
				return true;
			});
		});

		$('#expand').click(function() {
			var tree = $('#datadescription').tree('getTree');
			tree.iterate(function(node) {
				if (node.hasChildren()) {
					$('#datadescription').tree('openNode', node, true);
				}
				return true;
			});
		});    	
	}
	return Promise.resolve();
}

var getKeys = function(obj){
   var keys = [];
   for(var key in obj){
      keys.push(key);
   }
   return keys;
}