/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
var data;
var treeData;
var tree;
var nodeData;
var stardustGraph;
var hiddenNodes = [];
var hiddenLinks = [];
var nodes = null;
var links = null;

var nodeColors = {};
var labels = {};
var counter = 0;
var needToRemoveSelfLinks = true;
var showlabels = false;
var simulation = true;
var forceSimulation;
var showlabels = false;
var labelsvisible = false;
var popups = null;

var numNodes;
var numClusters = 0;
var doRender;

var colorValues = {
	"INFORMATION": "#FFCC00", 
	'INSTANCE': '#98BDF9', 
	'ALLOTROPE': '#DD6C07', 
	'COMMON': '#FFFFFF',
	'MATERIAL': '#C2E7C2',
	'PROCESS': '#CAA3F1',
	'RESULT': '#FFCC00',
	'EQUIPMENT': '#AAD5FF',
	'PROPERTY': '#FFFFFF',
	'QUALITY': '#FFFF99',
	'CONTEXTUAL_ROLE': '#FFC896',
	'REALIZABLE_ENTITY': '#FF99CC',
	'OBO': '#D5ECF7',
	'BFO': '#236099',
	'IAO': '#FFCC00',
	'OBI': '#449d44',
	'PATO': '#234979',
	'RO': '#FFFFFF',
	'CHMO': '#8EC1C5',
	'ENVO': '#A5FF4B',
	'NCBI': '#326597',
	'CHEBI': '#DAD734',
	'EXAMPLE': '#FFDFF7',
	'OTHER': '#FFFFFF',
	'LITERAL' : "#FFF0B3"
};

initGraphStore();

document.addEventListener('DOMContentLoaded',function(){
	loading.classList.remove('loaded');
	if (localStorage.getItem('graphStatus') && localStorage.getItem('graphStatus') == freshStartStatus) {
		resetLocalDbAndDataOfGraphs()
			.then(drawStardustGraph);
	} else if (localStorage.getItem('graphStatus') && localStorage.getItem('graphStatus') == graphAvailableStatus) {
		selectAllFromGraphs()
			.then(updateGraphStateWithSelectedResults)
			.then(selectAllFromInitialPositions)
			.then(updatePositionsWithSelectedResults)
			.then(drawStardustGraph);
	} else {
		localStorage.setItem('graphStatus', freshStartStatus);
		resetLocalDbAndDataOfGraphs()
			.then(drawStardustGraph);
		console.log("No state information found in local graph store. Restarting from scratch.");
	}
	loading.classList.add('loaded');
});

var triggerSimulation = function(){
	  if (simulation) {
		  forceSimulation.stop();
		  simulation = false;
	  } else {
		  forceSimulation.restart();
		  simulation = true;
	  }
	};

var triggerlabels = function(){
		  if (showlabels) {
			  showlabels = false;
		  } else {
			  showlabels = true;
		  }
		  if (!simulation) {
			  doRender();
		  }
};

var numNodes;

async function drawStardustGraph() {
	let promise = await new Promise(
		function (resolve, reject) {
			try {
				// This code was adapted from: https://stardustjs.github.io/
				// Copyright Donghao Ren, Bongshin Lee, and Tobias Höllerer, 2016-2017. License: BSD-3-Clause.
				var canvas = document.getElementById("stardust");
			    var width = $(window).width(); //960,
			    var height = $(window).height() - 100; //850,
			    var platform = Stardust.platform("webgl-2d", canvas, width, height);
			    platform.pixelRatio = window.devicePixelRatio || 1;

			    var snodes = Stardust.mark.create(Stardust.mark.circle(8), platform);
			    var snodesBG = Stardust.mark.create(Stardust.mark.circle(8), platform);
			    var snodesSelected = Stardust.mark.create(Stardust.mark.circle(8), platform);
			    var sedges = Stardust.mark.create(Stardust.mark.line(), platform);
//			    var text = Stardust.mark.createText("2d", platform);

				var graphdata = {
						nodes : [],
						links : []
				};
				var theNodeData = data;
				graphdata = getNodesAndLinksFromCytoscapeGraph(data, graphdata, 0);

			      var nodes = graphdata.nodes;
			      var edges = graphdata.links;
			      var N = nodes.length;
			      numNodes = N;

//			      var textLabels = [];
			      

		          for (var i = 0; i < N; i++) {
			        nodes[i].index = i;
			        nodes[i].x = Math.random() * width;
			        nodes[i].y = Math.random() * height;
//			        textLabels.push(nodes[i].name);
			      }

		          if (!popups) {
		        	  popups = [];
		        	  for (var i = 0; i < N; i++) {
		        		  var thetooltip = d3.select("#stardustdiv")
		        		  .append("div")
		        		  .style("position", "absolute")
		        		  .style("visibility", "visible")
		        		  .text(nodes[i].name)
		        		  .style("top", nodes[i].y + "px")
		        		  .style("left", nodes[i].x + "px");
		        		  popups.push(thetooltip);
		        		  labelsvisible = true;
		        	  }
		          }

		        let colors = [
			        [31, 119, 180],
			        [255, 127, 14],
			        [44, 160, 44],
			        [214, 39, 40],
			        [148, 103, 189],
			        [140, 86, 75],
			        [227, 119, 194],
			        [127, 127, 127],
			        [188, 189, 34],
			        [23, 190, 207]
			      ];
			      colors = colors.map(x => [x[0] / 255, x[1] / 255, x[2] / 255, 1]);

//			      snodes.attr("radius", 2).attr("color", d => colors[d.cluster]);
			      snodes.attr("radius", 2).attr("color", d => hexToRgb(d.color));
			      snodesBG.attr("radius", 3).attr("color", [1, 1, 1, 0.5]);

			      snodesSelected.attr("radius", 4).attr("color", [228 / 255, 26 / 255, 28 / 255, 1]);

			      sedges.attr("width", 0.5).attr("color", d => {
				        return [0, 0, 0, 0.5];
				      });
			      
//			      text.attr("text", d => d);
//			      text.attr("up", [0, 1]);
//			      text.attr("position", d => d);
//			      text.attr("fontFamily", "Arial");
//			      text.attr("fontSize", 12);
//			      text.attr("color", [0, 0, 0, 1]);
//			      			      
//			      text.data(textLabels);
			      
			      forceSimulation = d3.forceSimulation();
			      var force = forceSimulation
			        .force(
			          "link",
			          d3.forceLink().id(function(d) {
			            return d.index;
			          })
			        )
			        .force("charge", d3.forceManyBody())
			        .force("forceX", d3.forceX(width / 2))
			        .force("forceY", d3.forceY(height / 2));

			      force.nodes(nodes);
			      force.force("link").links(edges);

			      force.force("forceX").strength(0.5);
			      force.force("forceY").strength(0.5);
			      force.force("link").distance(50);
			      force.force("link").strength(0.05);
			      force.force("charge").strength(-40);

			      function makeSlider(name, attr, min, max, defaultValue) {
			        d3.select(".sliders")
			          .append("label")
			          .text(name);
			        var slider = d3.select(".sliders").append("input");
			        slider
			          .attr("type", "range")
			          .attr("min", 0)
			          .attr("max", 1000)
			          .attr("value", ((defaultValue - min) / (max - min)) * 1000);
			        slider.on("input", () => {
			          var val = +slider.property("value");
			          var d = (val / 1000) * (max - min) + min;
			          if (attr == "gravity") {
			            force.force("forceX").strength(d);
			            force.force("forceY").strength(d);
			          }
			          if (attr == "linkDistance") {
			            force.force("link").distance(d);
			          }
			          if (attr == "linkStrength") {
			            force.force("link").strength(d);
			          }
			          if (attr == "charge") {
			            force.force("charge").strength(d);
			          }
			          force.alphaTarget(0.3).restart();
			        });
			      }

			      // makeSlider("Friction", "friction", 0.01, 1, 0.5);
			      makeSlider("gravity", "gravity", 0.01, 0.2, 0.05);
			      makeSlider("charge", "charge", -200, 0, -20);
			      makeSlider("link Distance", "linkDistance", 0, 100, 5);
			      makeSlider("link Strength", "linkStrength", 0, 1.0, 0.5);

			      var positions = Stardust.array("Vector2")
			        .value(d => [d.x, d.y])
			        .data(nodes);

			      var positionScale = Stardust.scale.custom("array(pos, value)").attr("pos", "Vector2Array", positions);
			      snodesSelected.attr("center", positionScale(d => d.index));
			      snodes.attr("center", positionScale(d => d.index));
			      snodesBG.attr("center", positionScale(d => d.index));
			      sedges.attr("p1", positionScale(d => d.source.index));
			      sedges.attr("p2", positionScale(d => d.target.index));

			      snodesBG.data(nodes);
			      snodes.data(nodes);
			      sedges.data(edges);

			      force.on("tick", () => {
			        if (isDragging && selectedNode && draggingLocation) {
			          selectedNode.x = draggingLocation[0];
			          selectedNode.y = draggingLocation[1];
			        }
			        positions.data(nodes);
			        requestRender();
			      });

			      requestRender();

			      // Handle dragging

			      let selectedNode = null;

			      var requested = null;
			      function requestRender() {
			        if (requested) return;
			        requested = requestAnimationFrame(render);
			      }

			      var fps = new FPS();

			      function render() {
			        requested = null;
			        snodesSelected.data(selectedNode ? [selectedNode] : []);

			        // Cleanup and re-render.
			        platform.clear([1, 1, 1, 1]);
			        sedges.render();
			        snodesBG.render();
			        
			        var nodeSize = Math.max(2, 500/numNodes);
			        snodes.attr("radius", nodeSize);
			        snodes.render();

			        snodesSelected.render();

			        // Render the picking buffer.
			        platform.beginPicking(canvas.width, canvas.height);
			        snodes.attr("radius", nodeSize + 6); // make radius larger so it's easier to select.
			        snodes.render();
			        
//			        text.attr("position", d => d);
//			        text.attr("alignX", 0.5);
//			        text.attr("alignY", 0);
//			        text.render();
			        
			        platform.endPicking();

			        fps.update();
			        
			        if (showlabels) {
		        		for (var i = 0; i < N; i++) {
		        			popups[i].style("top", nodes[i].y + "px").style("left", nodes[i].x + "px");
		        			popups[i].style("visibility", "visible");
					    }
		        		labelsvisible = true;
			        } else if (labelsvisible){
		        		for (var i = 0; i < N; i++) {
		        			popups[i].style("visibility", "hidden");
		        		}
		        		labelsvisible = false;
			        }
			      }
			      
			      doRender = render;
			      
			      var tooltip = d3.select("#stardustdiv")
			      .append("div")
			        .style("position", "absolute")
			        .style("visibility", "hidden")
			        .text("test");
			      
			      var isDragging = false;
			      var draggingLocation = null;
			      // Handle dragging.
			      canvas.onmousedown = function(e) {
			        var x = e.clientX - canvas.getBoundingClientRect().left;
			        var y = e.clientY - canvas.getBoundingClientRect().top;
			        var p = platform.getPickingPixel(x * platform.pixelRatio, y * platform.pixelRatio);
			        if (p) {
			          selectedNode = nodes[p[1]];
			          requestRender();
			          isDragging = true;
			          draggingLocation = [selectedNode.x, selectedNode.y];
			          var onMove = function(e) {
			            var nx = e.clientX - canvas.getBoundingClientRect().left;
			            var ny = e.clientY - canvas.getBoundingClientRect().top;
			            selectedNode.x = nx;
			            selectedNode.y = ny;
			            draggingLocation = [nx, ny];
			            force.alphaTarget(0.3).restart();
			            requestRender();
			            if (!showlabels) {
				            tooltip.text(selectedNode.name);
				            tooltip.style("visibility", "visible");
						    tooltip.style("top", ny + "px").style("left", nx + "px");
			            }
			          };
			          var onUp = function() {
			            window.removeEventListener("mousemove", onMove);
			            window.removeEventListener("mouseup", onUp);
			            selectedNode = null;
			            draggingLocation = null;
			            isDragging = false;
					    tooltip.style("visibility", "hidden");
			          };
			          window.addEventListener("mousemove", onMove);
			          window.addEventListener("mouseup", onUp);
			        }
			      };

			      canvas.onmousemove = function(e) {
			        if (isDragging) return;
			        var x = e.clientX - canvas.getBoundingClientRect().left;
			        var y = e.clientY - canvas.getBoundingClientRect().top;
			        var p = platform.getPickingPixel(x * platform.pixelRatio, y * platform.pixelRatio);
			        if (p) {
			          if (selectedNode != nodes[p[1]]) {
			            selectedNode = nodes[p[1]];
			            requestRender();
			            if (!showlabels) {
				            tooltip.text(selectedNode.name);
				            tooltip.style("visibility", "visible");
						    tooltip.style("top", y + "px").style("left", x + "px");
			            }
			          }
			        } else {
			          if (selectedNode != null) {
			            selectedNode = null;
			            requestRender();
					    tooltip.style("visibility", "hidden");
			          }
			        }
			      };
		} catch(error) {
				loading.classList.add('loaded');
				console.log("Error during creation of stardust graph: " + error);
				reject();
			}
			loading.classList.add('loaded');
			resolve(stardustGraph);
		}
	)
	.catch(error => { 
		loading.classList.add('loaded');
		console.log("Error during creation of stardust graph: " + error);
	});
	return promise;
}

function redrawStardustGraph() {
	drawStardustGraph()
		.then(reload);
}

function reload() {
	var reloadAllowed = localStorage.getItem('reloadStardustAllowed');
	if (reloadAllowed && reloadAllowed == "1") {
		localStorage.setItem('reloadStardustAllowed',"0");
		window.location.reload();
	} else if (!reloadAllowed) {
		localStorage.setItem('reloadStardustAllowed',"0");
		window.location.reload();
	} else if (reloadAllowed && reloadAllowed == "0") {
		localStorage.setItem('reloadStardustAllowed',"1");
	}
	return Promise.resolve();
}