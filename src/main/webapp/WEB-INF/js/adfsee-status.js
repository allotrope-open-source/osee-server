/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
var freshStartStatus = 'started from scratch';
var graphAvailableStatus = 'graph data is available';
var treeAvailableStatus = 'tree data is available';
var queryResultsAvailableStatus = 'query results data is available';
var datacubesAvailableStatus = 'datacubes data is available';
var audittrailAvailableStatus = 'audittrail data is available';
var adfInfoAvailableStatus = 'adfInfo data is available';
var datapackageAvailableStatus = 'datapackage data is available';
var datadescriptionAvailableStatus = 'datadescription data is available';
var inputAvailableStatus = 'input data is available';

function updateGraphStatusInLocalStorage(status) {
	localStorage.setItem('graphStatus', status);
	console.log("Current graph state: " + localStorage.getItem('graphStatus')); 
	return Promise.resolve();
}

function updateTreeStatusInLocalStorage(status) {
	localStorage.setItem('treeStatus', status);
	console.log("Current tree state: " + localStorage.getItem('treeStatus')); 
	return Promise.resolve();
}

function updateAudittrailStatusInLocalStorage(status) {
	localStorage.setItem('audittrailStatus', status);
	console.log("Current audit trail state: " + localStorage.getItem('audittrailStatus')); 
	return Promise.resolve();
}

function updateAdfInfoStatusInLocalStorage(status) {
	localStorage.setItem('adfInfoStatus', status);
	console.log("Current adfInfo state: " + localStorage.getItem('adfInfoStatus')); 
	return Promise.resolve();
}

function updateDatapackageStatusInLocalStorage(status) {
	localStorage.setItem('datapackageStatus', status);
	console.log("Current datapackage state: " + localStorage.getItem('datapackageStatus')); 
	return Promise.resolve();
}

function updateDatadescriptionStatusInLocalStorage(status) {
	localStorage.setItem('datadescriptionStatus', status);
	console.log("Current datadescription state: " + localStorage.getItem('datadescriptionStatus')); 
	return Promise.resolve();
}

function updateQueryResultsStatusInLocalStorage(status) {
	localStorage.setItem('queryResultsStatus', status);
	console.log("Current query results state: " + localStorage.getItem('queryResultsStatus')); 
	return Promise.resolve();
}

function updateDatacubesStatusInLocalStorage(status) {
	localStorage.setItem('datacubesStatus', status);
	console.log("Current datacubes state: " + localStorage.getItem('datacubesStatus')); 
	return Promise.resolve();
}

function updateInputStatusInLocalStorage(status) {
	localStorage.setItem('inputStatus', status);
	console.log("Current input state: " + localStorage.getItem('inputStatus')); 
	return Promise.resolve();
}

function updateGraphStateWithSelectedResults(results) {
	if (results && results[0] && results[0].data) {
		data = JSON.parse(results[0].data);
		console.log("loading graph data from local database #entities = " + data.length);
		updateGraphStatusInLocalStorage(graphAvailableStatus);
	} else {
		data = initData;
		console.log("No persisted graph data found. Restarting.");
		updateGraphStatusInLocalStorage(freshStartStatus);
	}
	return Promise.resolve();
}

function updatePositionsWithSelectedResults(results) {
	if (results && results[0] && results[0].data) {
		initialPositions = JSON.parse(results[0].data);
		console.log("loading positions data from local database #entities = " + data.length);
	} else {
		console.log("No preset positions found in local database.");
		return Promise.reject();
	}
	return Promise.resolve();
}

function updateTreeStateWithSelectedResults(results) {
	if (results && results[0] && results[0].data) {
		treeData = JSON.parse(results[0].data);
		console.log("loading tree data from local database #entities = " + treeData.length);
		updateTreeStatusInLocalStorage(treeAvailableStatus);
	} else {
		treeData = initTreeData;
		console.log("No persisted tree data found. Restarting.");
		updateTreeStatusInLocalStorage(freshStartStatus);
	}
	return Promise.resolve();
}

function updateAudittrailStateWithSelectedResults(results) {
	if (results && results[0] && results[0].data) {
		audittrail = JSON.parse(results[0].data);
		console.log("loading audittrail data from local database #entities = " + audittrail.length);
		updateAudittrailStatusInLocalStorage(audittrailAvailableStatus);
	} else {
		audittrail = initTreeData;
		console.log("No audittrail data found. Restarting.");
		updateAudittrailStatusInLocalStorage(freshStartStatus);
	}
	return Promise.resolve();
}

function updateAdfInfoStateWithSelectedResults(results) {
	if (results && results[0] && results[0].data) {
		adfInfo = JSON.parse(results[0].data);
		console.log("loading adfInfo data from local database #entities = " + adfInfo.length);
		updateAdfInfoStatusInLocalStorage(adfInfoAvailableStatus);
	} else {
		adfInfo = initTreeData;
		console.log("No adfInfo data found. Restarting.");
		updateAdfInfoStatusInLocalStorage(freshStartStatus);
	}
	return Promise.resolve();
}

function updateDatapackageStateWithSelectedResults(results) {
	if (results && results[0] && results[0].data) {
		datapackage = JSON.parse(results[0].data);
		console.log("loading datapackage data from local database #entities = " + datapackage.length);
		updateDatapackageStatusInLocalStorage(datapackageAvailableStatus);
	} else {
		datapackage = initTreeData;
		console.log("No datapackage data found. Restarting.");
		updateDatapackageStatusInLocalStorage(freshStartStatus);
	}
	return Promise.resolve();
}

function updateDatadescriptionStateWithSelectedResults(results) {
	if (results && results[0] && results[0].data) {
		datadescription = JSON.parse(results[0].data);
		console.log("loading datadescription data from local database #entities = " + datadescription.length);
		updateDatadescriptionStatusInLocalStorage(datadescriptionAvailableStatus);
	} else {
		datadescription = initTreeData;
		console.log("No datadescription data found. Restarting.");
		updateDatadescriptionStatusInLocalStorage(freshStartStatus);
	}
	return Promise.resolve();
}

function updateQueryResultsStateWithSelectedResults(results) {
	if (results && results[0] && results[0].data) {
		queryResultsData = JSON.parse(results[0].data);
		console.log("loading query results data from local database");
		updateQueryResultsStatusInLocalStorage(queryResultsAvailableStatus);
	} else {
		console.log("No persisted query results data found. Restarting.");
		updateQueryResultsStatusInLocalStorage(freshStartStatus);
	}
	return Promise.resolve();
}

function updateDatacubesStateWithSelectedResults(results) {
	if (results && results[0] && results[0].data) {
		datacubes = JSON.parse(results[0].data);
		console.log("loading datacubes from local database");
		updateDatacubesStatusInLocalStorage(datacubesAvailableStatus);
	} else {
		console.log("No persisted datacubes found. Restarting.");
		updateDatacubesStatusInLocalStorage(freshStartStatus);
	}
	return Promise.resolve();
}

function updatInputsStateWithSelectedResults(results) {
	if (results && results[0] && results[0].data) {
		inputs = JSON.parse(results[0].data);
		console.log("loading inputs from local database");
		updateInputStatusInLocalStorage(inputAvailableStatus);
	} else {
		console.log("No persisted inputs found. Restarting.");
		updateInputStatusInLocalStorage(freshStartStatus);
	}
	return Promise.resolve();
}

