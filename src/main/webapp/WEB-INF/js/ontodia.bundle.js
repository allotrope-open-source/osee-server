/**
 * ontodia.js
 * https://github.com/metaphacts/ontodia
 *
 * Ontodia data diagramming library
 * Copyright (C) 2018 Sputniq GmbH.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, you can receive a copy
 * of the GNU Lesser General Public License from http://www.gnu.org/
 */
/******/
(function(modules) { // webpackBootstrap
    /******/ // install a JSONP callback for chunk loading
    /******/
    function webpackJsonpCallback(data) {
        /******/
        var chunkIds = data[0];
        /******/
        var moreModules = data[1];
        /******/
        var executeModules = data[2];
        /******/
        /******/ // add "moreModules" to the modules object,
        /******/ // then flag all "chunkIds" as loaded and fire callback
        /******/
        var moduleId, chunkId, i = 0,
            resolves = [];
        /******/
        for (; i < chunkIds.length; i++) {
            /******/
            chunkId = chunkIds[i];
            /******/
            if (installedChunks[chunkId]) {
                /******/
                resolves.push(installedChunks[chunkId][0]);
                /******/
            }
            /******/
            installedChunks[chunkId] = 0;
            /******/
        }
        /******/
        for (moduleId in moreModules) {
            /******/
            if (Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
                /******/
                modules[moduleId] = moreModules[moduleId];
                /******/
            }
            /******/
        }
        /******/
        if (parentJsonpFunction) parentJsonpFunction(data);
        /******/
        /******/
        while (resolves.length) {
            /******/
            resolves.shift()();
            /******/
        }
        /******/
        /******/ // add entry modules from loaded chunk to deferred list
        /******/
        deferredModules.push.apply(deferredModules, executeModules || []);
        /******/
        /******/ // run deferred modules when all chunks ready
        /******/
        return checkDeferredModules();
        /******/
    };
    /******/
    function checkDeferredModules() {
        /******/
        var result;
        /******/
        for (var i = 0; i < deferredModules.length; i++) {
            /******/
            var deferredModule = deferredModules[i];
            /******/
            var fulfilled = true;
            /******/
            for (var j = 1; j < deferredModule.length; j++) {
                /******/
                var depId = deferredModule[j];
                /******/
                if (installedChunks[depId] !== 0) fulfilled = false;
                /******/
            }
            /******/
            if (fulfilled) {
                /******/
                deferredModules.splice(i--, 1);
                /******/
                result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
                /******/
            }
            /******/
        }
        /******/
        return result;
        /******/
    }
    /******/
    /******/ // The module cache
    /******/
    var installedModules = {};
    /******/
    /******/ // object to store loaded and loading chunks
    /******/ // undefined = chunk not loaded, null = chunk preloaded/prefetched
    /******/ // Promise = chunk loading, 0 = chunk loaded
    /******/
    var installedChunks = {
        /******/
        "sparql": 0
            /******/
    };
    /******/
    /******/
    var deferredModules = [];
    /******/
    /******/ // The require function
    /******/
    function __webpack_require__(moduleId) {
        /******/
        /******/ // Check if module is in cache
        /******/
        if (installedModules[moduleId]) {
            /******/
            return installedModules[moduleId].exports;
            /******/
        }
        /******/ // Create a new module (and put it into the cache)
        /******/
        var module = installedModules[moduleId] = {
            /******/
            i: moduleId,
            /******/
            l: false,
            /******/
            exports: {}
            /******/
        };
        /******/
        /******/ // Execute the module function
        /******/
        modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
        /******/
        /******/ // Flag the module as loaded
        /******/
        module.l = true;
        /******/
        /******/ // Return the exports of the module
        /******/
        return module.exports;
        /******/
    }
    /******/
    /******/
    /******/ // expose the modules object (__webpack_modules__)
    /******/
    __webpack_require__.m = modules;
    /******/
    /******/ // expose the module cache
    /******/
    __webpack_require__.c = installedModules;
    /******/
    /******/ // define getter function for harmony exports
    /******/
    __webpack_require__.d = function(exports, name, getter) {
        /******/
        if (!__webpack_require__.o(exports, name)) {
            /******/
            Object.defineProperty(exports, name, { enumerable: true, get: getter });
            /******/
        }
        /******/
    };
    /******/
    /******/ // define __esModule on exports
    /******/
    __webpack_require__.r = function(exports) {
        /******/
        if (typeof Symbol !== 'undefined' && Symbol.toStringTag) {
            /******/
            Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
            /******/
        }
        /******/
        Object.defineProperty(exports, '__esModule', { value: true });
        /******/
    };
    /******/
    /******/ // create a fake namespace object
    /******/ // mode & 1: value is a module id, require it
    /******/ // mode & 2: merge all properties of value into the ns
    /******/ // mode & 4: return value when already ns object
    /******/ // mode & 8|1: behave like require
    /******/
    __webpack_require__.t = function(value, mode) {
        /******/
        if (mode & 1) value = __webpack_require__(value);
        /******/
        if (mode & 8) return value;
        /******/
        if ((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
        /******/
        var ns = Object.create(null);
        /******/
        __webpack_require__.r(ns);
        /******/
        Object.defineProperty(ns, 'default', { enumerable: true, value: value });
        /******/
        if (mode & 2 && typeof value != 'string')
            for (var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
        /******/
        return ns;
        /******/
    };
    /******/
    /******/ // getDefaultExport function for compatibility with non-harmony modules
    /******/
    __webpack_require__.n = function(module) {
        /******/
        var getter = module && module.__esModule ?
            /******/
            function getDefault() { return module['default']; } :
            /******/
            function getModuleExports() { return module; };
        /******/
        __webpack_require__.d(getter, 'a', getter);
        /******/
        return getter;
        /******/
    };
    /******/
    /******/ // Object.prototype.hasOwnProperty.call
    /******/
    __webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
    /******/
    /******/ // __webpack_public_path__
    /******/
    __webpack_require__.p = "";
    /******/
    /******/
    var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
    /******/
    var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
    /******/
    jsonpArray.push = webpackJsonpCallback;
    /******/
    jsonpArray = jsonpArray.slice();
    /******/
    for (var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
    /******/
    var parentJsonpFunction = oldJsonpFunction;
    /******/
    /******/
    /******/ // add entry module to deferred list
    /******/
    deferredModules.push(["./src/examples/sparql.ts", "commons"]);
    /******/ // run deferred modules when ready
    /******/
    return checkDeferredModules();
    /******/
})
/************************************************************************/
/******/
({

    /***/
    "./src/examples/sparql.ts":
    /*!********************************!*\
      !*** ./src/examples/sparql.ts ***!
      \********************************/
    /*! no static exports found */
    /***/
        (function(module, exports, __webpack_require__) {

        "use strict";

        Object.defineProperty(exports, "__esModule", { value: true });
        var tslib_1 = __webpack_require__( /*! tslib */ "./node_modules/tslib/tslib.es6.js");
        var react_1 = __webpack_require__( /*! react */ "./node_modules/react/react.js");
        var ReactDOM = __webpack_require__( /*! react-dom */ "./node_modules/react-dom/index.js");
        var index_1 = __webpack_require__( /*! ../index */ "./src/index.ts");
        var common_1 = __webpack_require__( /*! ./common */ "./src/examples/common.ts");

        function onWorkspaceMounted(workspace) {
            if (!workspace) {
                return;
            }
            var diagram = common_1.tryLoadLayoutFromLocalStorage();
            workspace.getModel().importLayout({
                diagram: tslib_1.__assign({}, diagram, {
                    linkTypeOptions: [{
                        '@type': 'LinkTypeOptions',
                        property: 'http://www.researchspace.org/ontology/group',
                        visible: false,
                    }, ]
                }),
                validateLinks: true,
                dataProvider: new index_1.SparqlDataProvider({
                    endpointUrl: 'http://localhost:9999/adfsee/sparql',
                    imagePropertyUris: [
                        'http://collection.britishmuseum.org/id/ontology/PX_has_main_representation',
                        'http://xmlns.com/foaf/0.1/img',
                    ],
                    queryMethod: index_1.SparqlQueryMethod.GET,
                    acceptBlankNodes: true,
                }, index_1.OWLStatsSettings),
            });
        }
        var props = {
            ref: onWorkspaceMounted,
            onSaveDiagram: function(workspace) {
                var diagram = workspace.getModel().exportLayout();
                window.location.hash = common_1.saveLayoutToLocalStorage(diagram);
                window.location.reload();
            },
            viewOptions: {
                onIriClick: function(_a) {
                    var iri = _a.iri;
                    return window.open(iri);
                },
                groupBy: [
                    { linkType: 'http://www.researchspace.org/ontology/group', linkDirection: 'in' },
                ],
            },
            languages: [
                { code: 'en', label: 'English' },
                { code: 'de', label: 'German' },
                { code: 'ru', label: 'Russian' },
            ],
            language: 'ru',
            elementTemplateResolver: function(types) {
                if (types.indexOf('http://www.ics.forth.gr/isl/CRMinf/I2_Belief') !== -1) {
                    return index_1.GroupTemplate;
                }
                return undefined;
            },
        };
        common_1.onPageLoad(function(container) { return ReactDOM.render(react_1.createElement(index_1.Workspace, props), container); });


        /***/
    })

    /******/
});
//# sourceMappingURL=sparql.bundle.js.map