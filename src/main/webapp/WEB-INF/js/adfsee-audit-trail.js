/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
var data;
var treeData;
var audittrailData;
var audittrail;
var tabledata;
var loadafo = true;
var loadaft = true;
var dolayout = true;
var multigraph = false;
var skolemize = false;
var loadexample = false;
var includeadf = false;
var columnWidths;
var columnHeaders;
var audittrailtypes = [ "jsontree", "renderjson", "jsonhtml" ];

initGraphStore();

document.addEventListener('DOMContentLoaded', function() {
		var loading = document.getElementById('loading');
		loading.classList.remove('loaded');

		if (localStorage.getItem('audittrailStatus') && localStorage.getItem('audittrailStatus') == freshStartStatus) {
			resetLocalDbAndDataOfAudittrails().then(createJsonTree);
		} else if (localStorage.getItem('audittrailStatus') && localStorage.getItem('audittrailStatus') == audittrailAvailableStatus) {
			selectAllFromAudittrails()
				.then(updateAudittrailStateWithSelectedResults)
				.then(createJsonTree);
		} else {
			localStorage.setItem('audittrailStatus', freshStartStatus);
			resetLocalDbAndDataOfAudittrails()
				.then(function() {
					console	.log("No state information found in local audittrail store. Restarting from scratch.");
					return Promise.resolve();
				})
				.then(createJsonTree);
		}

		$('#jsontree').click(function() {
			document.getElementById("auditcss").setAttribute("href", "../css/audit-trail.css");
			$('#audittrail').html("");
			createJsonTree();
			activeButtons();
		});

		$('#renderjson').click(function() {
			document.getElementById("auditcss").setAttribute("href", "../css/audit-trail.css");
			$('#audittrail').html("");
			createRenderJson(5);
			activeButtons();

			$('#collapse').off("click").click(function() {
				$('#audittrail').html("");
				createRenderJson(0);
			});

			$('#expand').off("click").click(function() {
				$('#audittrail').html("");
				createRenderJson("all");
			});    	
		});

		$('#jsonhtml').click(function() {
			document.getElementById("auditcss").setAttribute("href", "../css/jsonhtml.css");
			var element = document.getElementById("audittrail");
			element.outerHTML = "";
			delete element;	
			
			var newDiv = document.createElement('div');
			newDiv.id = 'audittrail';
			document.getElementsByTagName('body')[0].appendChild(newDiv);
			
			$('#audittrail').html("");
			createJsonHtml();
			activeButtons();

			$('#collapse').off("click").click(function() {
				$('#audittrail').html("");
				console.log("collapse jsonhtml");
				createJsonHtml();
			});

			$('#expand').off("click").click(function() {
				console.log("expand jsonhtml");
				var elements = Array.from($('div.package'));
				if (elements) {
					elements.forEach(function(element){
						element.classList.remove("closed");
						element.classList.add("open");
					});
				}
			});    	
		});

		loading.classList.add('loaded');
});

async function createJsonTree() {
	console.log("jsontree");
	await changeActiveClass("jsontree");
	var loading = document.getElementById('loading');
	loading.classList.remove('loaded');
	try {
		let tree = new jsonTree(audittrail, "#audittrail", false);
	} catch (error) {
		console.log("Error during creation of jsontree: " + error);
		loading.classList.add('loaded');
	} finally {
		$('#collapse').click(function() {
			var selector = "#audittrail";
			var parent = "li";
			var child = "ul";
			var parents = Array.from(document.querySelectorAll(`${selector} ${parent}`));
			parents.forEach(function(element){
				var filter = Array.from(element.children).filter((el) => el.tagName.toLowerCase() === child.toLowerCase().toString());
				if (filter.length > 0) { // It's a parent!
					element.classList.remove("selected");
				} 
			});
			
			parent = "ul";
			child = "li";
			parents = Array.from(document.querySelectorAll(`${selector} ${parent}`));
			parents.forEach(function(element){
				const filter = Array.from(element.children).filter((el) => el.tagName.toLowerCase() === child.toLowerCase().toString());
				if (filter.length > 0) { // It's a parent!
					element.classList.remove("selected");
				} 
			});
		});
	
		$('#expand').click(function() {
			var selector = "#audittrail";
			var parent = "li";
			var child = "ul";
			var parents = Array.from(document.querySelectorAll(`${selector} ${parent}`));
			parents.forEach(function(element){
				var filter = Array.from(element.children).filter((el) => el.tagName.toLowerCase() === child.toLowerCase().toString());
				if (filter.length > 0) { // It's a parent!
					element.classList.add("selected");
				} 
			});

			parent = "ul";
			child = "li";
			parents = Array.from(document.querySelectorAll(`${selector} ${parent}`));
			parents.forEach(function(element){
				var filter = Array.from(element.children).filter((el) => el.tagName.toLowerCase() === child.toLowerCase().toString());
				if (filter.length > 0) { // It's a parent!
					element.classList.add("selected");
				} 
			});
		});    	
	}
	loading.classList.add('loaded');
	activeButtons();
	return Promise.resolve();
}

async function changeActiveClass(id) {
	audittrailtypes.forEach(function(element) {
		var theElement = document.getElementById(element);
		if (element === id) {
			theElement.classList.remove("inactiveaudit");
			theElement.classList.add("activeaudit");
		} else {
			theElement.classList.remove("activeaudit");
			theElement.classList.add("inactiveaudit");
		}
	});
	return Promise.resolve();
}

async function activeButtons() {
	document.getElementById("expand").classList.remove("inactivebutton");
	document.getElementById("expand").classList.add("activebutton");
	document.getElementById("collapse").classList.remove("inactivebutton");
	document.getElementById("collapse").classList.add("activebutton");
	return Promise.resolve();
}

async function inactiveButtons() {
	document.getElementById("expand").classList.remove("activebutton");
	document.getElementById("expand").classList.add("inactivebutton");
	document.getElementById("collapse").classList.remove("activebutton");
	document.getElementById("collapse").classList.add("inactivebutton");
	return Promise.resolve();
}

async function createRenderJson(level) {
	console.log("renderjson");
	await changeActiveClass("renderjson");
	var loading = document.getElementById('loading');
	loading.classList.remove('loaded');
	var unescapedAudittrail = audittrail;
	unescapedAudittrail = htmlUnescape(JSON.stringify(unescapedAudittrail));
	unescapedAudittrail = JSON.parse(unescapedAudittrail);
	document.getElementById("audittrail").appendChild(renderjson.set_show_by_default(true).set_show_to_level(level).set_sort_objects(true).set_icons('+', '-').set_max_string_length(200)(unescapedAudittrail));
	loading.classList.add('loaded');
	return Promise.resolve();
}

/** To stop XSS attacks by using JSON with HTML nodes **/
function htmlUnescape(str) {
	const tagsToReplace = {
		"&amp;": "&",
		"&lt;": "<",
		"&gt;": ">"
	};
	return str.replace(/(&amp;|&lt;|&gt;)/g, function(tag) {
		return tagsToReplace[tag] || tag;
	});
}

async function createJsonHtml() {
	console.log("jsonhtml");
	await changeActiveClass("jsonhtml");
	vizJson2html();
	return Promise.resolve();
}

function vizJson2html() {
	$('#audittrail').html('');
	$('#audittrail').css("width", "95%");
	$('#audittrail').css("top", "20px");
	$('#audittrail').json2html(convert('json',audittrail,'open'),transforms.object);
	regEvents();		
}

var transforms = {
		'object':{'tag':'div','class':'package ${show} ${type}','children':[
			{'tag':'div','class':'header','children':[
				{'tag':'div','class':function(obj){

					var classes = ["arrow"];

					if( getValue(obj.value) !== undefined ) classes.push("hide");
					
					return(classes.join(' '));
				}},
				{'tag':'span','class':'name','html':'${name}'},
				{'tag':'span','class':'value','html':function(obj) {
					var value = getValue(obj.value);
					if( value !== undefined ) return(" : " + value);
					else return('');
				}},
				{'tag':'span','class':'type','html':'${type}'}
			]},
			{'tag':'div','class':'children','children':function(obj){return(children(obj.value));}}
		]}
};

function getValue(obj) {
	var type = $.type(obj);

	//Determine if this object has children
	switch(type) {
		case 'array':
		case 'object':
			return(undefined);
		break;

		case 'function':
			//none
			return('function');
		break;

		case 'string':
			return("'" + obj + "'");
		break;

		default:
			return(obj);
		break;
	}
}

//Transform the children
function children(obj){
	var type = $.type(obj);

	//Determine if this object has children
	switch(type) {
		case 'array':
		case 'object':
			return(json2html.transform(obj,transforms.object));
		break;

		default:
			//This must be a literal
		break;
	}
}

function convert(name,obj,show) {
	
	var type = $.type(obj);

	if(show === undefined) show = 'closed';
	
	var children = [];

	//Determine the type of this object
	switch(type) {
		case 'array':
			//Transform array
			//Itterrate through the array and add it to the elements array
			var len=obj.length;
			for(var j=0;j<len;++j){	
				//Concat the return elements from this objects tranformation
				children[j] = convert(j,obj[j]);
			}
		break;

		case 'object':
			//Transform Object
			var j = 0;
			for(var prop in obj) {
				children[j] = convert(prop,obj[prop]);
				j++;
			}	
		break;

		default:
			//This must be a litteral (or function)
			children = obj;
		break;
	}

	return( {'name':name,'value':children,'type':type,'show':show} );
	
}

function regEvents() {

	$('.header').click(function(){
		var parent = $(this).parent();

		if(parent.hasClass('closed')) {
			parent.removeClass('closed');
			parent.addClass('open');
		} else {
			parent.removeClass('open');
			parent.addClass('closed');
		}		
	});
}