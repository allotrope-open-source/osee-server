/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
var data;
var treeData;
var loadafo = true;
var loadaft = true;
var dolayout = true;
var multigraph = false;
var skolemize = false;
var loadexample = false;
var includeadf = false;
var tree;

initGraphStore();

document.addEventListener('DOMContentLoaded',function(){
	if (localStorage.getItem('treeStatus') && localStorage.getItem('treeStatus') == freshStartStatus) {
		resetLocalDbAndDataOfTrees()
		.then(mainTree);
	} else if (localStorage.getItem('treeStatus') && localStorage.getItem('treeStatus') == treeAvailableStatus) {
		selectAllFromTrees()
		.then(updateTreeStateWithSelectedResults)
		.then(mainTree);
	} else {
		localStorage.setItem('treeStatus', freshStartStatus);
		resetLocalDbAndDataOfTrees()
		.then(function() {
			console.log("No state information found in local tree store. Restarting from scratch.");
			return Promise.resolve();
		})
		.then(mainTree);
	}
});

function mainTree() {
	document.getElementById('loading').classList.remove('loaded');
	var data = treeData[0];
	Vue.component('item', 
	{
		template: '#item-template',
		props: {
			model: Object
		},
		data: function () {
			return {
				open: false
			}
		},
		computed: {
			isFolder: function () {
				return this.model.children && this.model.children.length
			}
		},
		methods: {
			toggle: function () {
				if (this.isFolder) {
					this.open = !this.open
				}
			},
			changeType: function () {
				if (!this.isFolder) {
					Vue.set(this.model, 'children', []);
					this.addChild();
					this.open = true;
				}
			},
			addChild: function () {
				this.model.children.push({
					name: 'new item'
				});
			}
		}
	});
	
	var tree = new Vue({
		el: "#tree",
		data: {
			"treeData": data
		}
	});

	$('#collapse').click(function() {
		toggleRecursively(tree, false);
	});

	$('#expand').click(function() {
		toggleRecursively(tree, true);
	});    	
	
	function toggleRecursively(item, toggle) {
		var children = item.$children;
		if (children && children[0]) {
			item.open = toggle;
			for (var i = 0; i < children.length; i++) {
				toggleRecursively(children[i], toggle);
			}
		}
	}  
	
	document.getElementById('loading').classList.add('loaded');
	return Promise.resolve();
};

function reloadTree() {
	window.location.reload();
	return Promise.resolve();
}