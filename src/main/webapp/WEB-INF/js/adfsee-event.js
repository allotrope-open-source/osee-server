/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
document.getElementById("footer").addEventListener("click", function() {
	window.open('https://www.osthus.com/','_blank');
});

function fireMouseEvents( query, eventNames ){
	var element = document.querySelector(query);
	if(element && eventNames && eventNames.length){
		for(var index in eventNames){
			var eventName = eventNames[index];
			if(element.fireEvent ){
				element.fireEvent( 'on' + eventName );     
			} else {   
				var eventObject = document.createEvent( 'MouseEvents' );
				eventObject.initEvent( eventName, true, false );
				element.dispatchEvent(eventObject);
			}
		}
	}
}