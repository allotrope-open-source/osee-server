function writefile( fileNameToSaveAs , textToSave, typeToSave ) {
    var textToSaveAsBlob;
	
	if ((typeToSave.type == "image/png") || (typeToSave.type == "image/jpg")) {
		textToSaveAsBlob = b64toBlob(textToSave, typeToSave.type);
	} else {
		textToSaveAsBlob = new Blob([textToSave], typeToSave );
	}
    var textToSaveAsURL = window.URL.createObjectURL(textToSaveAsBlob);
 
    var downloadLink = document.createElement("a");
    downloadLink.download = fileNameToSaveAs;
    downloadLink.innerHTML = "Download File";
    downloadLink.href = textToSaveAsURL;
    downloadLink.onclick = destroyClickedElement;
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);
    downloadLink.click();
};

function destroyClickedElement(event) {
    document.body.removeChild(event.target);
};
// based on https://thiscouldbebetter.wordpress.com/2012/12/18/loading-editing-and-saving-a-text-file-in-html5-using-javascrip/