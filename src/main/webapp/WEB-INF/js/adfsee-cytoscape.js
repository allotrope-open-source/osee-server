/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
var freshStartStatus = 'started from scratch';
var graphAvailableStatus = 'graph data is available';
var datacubesAvailableStatus = 'datacubes data is available';
var data;
var treeData;
var queryResultsData;
var datacubes;
var initialPositions;

function willDrawGraph() {
	try {
		drawGraph();
	} catch (error) {
		console.error("Error drawing graph: " + error);
		return Promise.reject();
	}
	console.log("Graph drawn successfully.")
	return Promise.resolve();
}

var loadafo = true;
var loadaft = true;
var dolayout = true;
var dolayout = true;
var multigraph = false;
var skolemize = false;
var loadexample = false;
var includeadf = false;
var cy;
	
initGraphStore();

document.addEventListener('DOMContentLoaded',function(){
	if (localStorage.getItem('graphStatus') && localStorage.getItem('graphStatus') == freshStartStatus) {
		resetLocalDbAndDataOfGraphs()
			.then(willDrawGraph);
	} else if (localStorage.getItem('graphStatus') && localStorage.getItem('graphStatus') == graphAvailableStatus) {
		selectAllFromGraphs()
			.then(updateGraphStateWithSelectedResults)
			.then(selectAllFromInitialPositions)
			.then(updatePositionsWithSelectedResults)
			.then(willDrawGraph);
	} else {
		localStorage.setItem('graphStatus', freshStartStatus);
		resetLocalDbAndDataOfGraphs()
			.then(willDrawGraph);
		console.log("No state information found in local graph store. Restarting from scratch.");
	}
});

function drawGraph() {
	console.log("creating cytoscape graph");
	cy = cytoscape({
		container: document.getElementById('cy'),
			elements: data,
			style: [
			{ selector: 'node',
			  style: { 
				'label': 'data(label)', 
				'background-color': 'data(bg)',
				'shape': 'data(shape)',
				'width': 'label',
				'height': 'label',
				'padding' : '5px',
				'text-wrap': 'wrap',
				'text-max-width': '300px',
				'text-valign': 'center',
				'text-halign': 'center',
				"font-size": "12pt",
				"color": "#000000",
				"border-width" : "data(borderwidth)", 
				"border-style" : "solid",
				"border-color" : "#000000",
				"border-opacity" : "1",
				"visibility" : "data(visibility)"
			  }
			},
			{ selector: 'edge',
			  style: {
				'curve-style': 'data(curveStyle)',
				'width': 'data(lineWidth)', 
				'target-arrow-shape': 'triangle',
				'line-color': 'data(lineColor)',
				'target-arrow-color': 'data(lineColor)',
				"text-outline-color": "#fff",
				"text-outline-width": "0.2px",
				'text-wrap': 'wrap',
				'text-max-width': '80px',
				'color': 'data(color)', 
				'label': 'data(label)',
				"font-size": "10pt", 
				"visibility": "data(visibility)"
				//"text-rotation" : "autorotate"
			  }
			},
			{ selector: 'edge.meta',
			  style: {
				'width': 2,
				'line-color': 'red'
			  }
			},
			{ selector: ':parent',
			  style: {
				'text-halign': 'center',
				'text-valign': 'top',
				'background-opacity': 0.333
			  }
			},
			{ selector: "node.cy-expand-collapse-collapsed-node",
			  style: {
				"background-color": "white",
				"shape": "rectangle",
				'width': 'label',
				'height': 'label',
				'padding' : '5px',
				'text-wrap': 'wrap',
				'text-max-width': '300px',
				'text-valign': 'center',
				'text-halign': 'center'
			  }
			},
			{ selector: ':selected',
			  style: {
				"border-width": 3,
				'background-opacity': 0.5 
			  }
			}
		],
		layout: {
			name: 'preset'
		},
		wheelSensitivity : 0.1
	});
	var options = {
		//name: 'cose-bilkent', //quick and good
		//name: 'cose',
		//name: 'springy', //nice visually
		//name: 'spread', //uses full space
		name: 'preset',
		//name: 'klay',
		//name: 'euler',
		//name: 'dagre', //good tree
		//name: 'cola',
		//name: 'grid',
		//name: 'circle',
		//name: 'breadthfirst',
		//name: 'concentric',
		//name: 'arbor', //seems to be buggy
		//name: 'cytoscape-ngraph.forcelayout', //seems to be buggy
		//name: 'd3-force',
		ready: function(){},
		stop: function(){},
		animate: 'end',
		animationEasing: 'ease-out',// 'spring(200,20)' Easing of the animation for animate:'end'
		animationDuration: 250,// The duration of the animation for animate:'end'
		animateFilter: function ( node, i ){ return true; },
		animationThreshold: 250,// The layout animates only after this many milliseconds for animate:true
		refresh: 20,// Number of iterations between consecutive screen positions update
		fit: true,// Whether to fit the network view after when done
		minDist: 20, // spread
		expandingFactor: -1, // spread: If the network does not satisfy the minDist criterium then it expands the network of this amount, If it is set to -1.0 the amount of expansion is automatically calculated based on the minDist, the aspect ratio and the number of nodes
		maxFruchtermanReingoldIterations: 50, // spread: Maximum number of initial force-directed iterations
		maxExpandIterations: 10, // spread: Maximum number of expanding iterations
		padding: 30,// Padding on fit
		boundingBox: undefined,// Constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
		nodeDimensionsIncludeLabels: true,// Excludes the label when calculating node bounding boxes for the layout algorithm
		randomize: true,// Randomize the initial positions of the nodes (true) or use existing positions (false)
		componentSpacing: 50,// Extra spacing between components in non-compound graphs
		//nodeRepulsion: function( node ){ return 2048; },// Node repulsion (non overlapping) multiplier
		nodeRepulsion: 50,// Cose-Bilkent: Node repulsion (non overlapping) multiplier
		nodeOverlap: 10,// Node repulsion (overlapping) multiplier
		//idealEdgeLength: function( edge ){ return 32; },// Ideal edge (non nested) length
		idealEdgeLength: 150,// Cose-Bilkent: Ideal edge (non nested) length
		edgeElasticity: 0.1,// Cose-Bilkent: Divisor to compute edge forces
		//edgeElasticity: function( edge ){ return 32; },// Divisor to compute edge forces
		nestingFactor: 0.1,// Nesting factor (multiplier) to compute ideal edge length for nested edges
		gravity: 125,
		//gravity: 0.25, // Gravity force (constant)
		numIter: 2500,// Maximum number of iterations to perform
		initialTemp: 100,// Initial temperature (maximum node displacement)
		coolingFactor: 0.99,// Cooling factor (how the temperature is reduced between consecutive iterations
		minTemp: 1.0,// Lower temperature threshold (below this point the layout will end)
		tile: true, // Whether to tile disconnected nodes
		weaver: true, // Pass a reference to weaver to use threads for calculations
		tilingPaddingVertical: 10,// Amount of vertical space to put between degree zero nodes during tiling (can also be a function)
		tilingPaddingHorizontal: 10,// Amount of horizontal space to put between degree zero nodes during tiling (can also be a function)
		gravityRangeCompound: 1.5,// Cose-Bilkent: Gravity range (constant) for compounds
		gravityCompound: 1.0,// Cose-Bilkent: Gravity force (constant) for compounds
		gravityRange: 3.8,// Cose-Bilkent: Gravity range (constant)
		initialEnergyOnIncremental: 0.5, // Initial cooling factor for incremental layout
		maxSimulationTime: 10000, // max length in ms to run the layout
		ungrabifyWhileSimulating: false, // so you can't drag nodes during layout
		repulsion: 400,// Arbor: forces used by arbor (use arbor default on undefined), springy forces and config
		stiffness: 400,// Arbor, springy forces and config
		friction: undefined,// Arbor
		fps: undefined,// Arbor
		precision: undefined,// Arbor
		nodeMass: undefined,// Arbor
		damping: 0.5, //springy
		edgeLength: function( edge ){//springy, NOT arbor, cola
			var length = edge.data('length');
			if( length !== undefined && !isNaN(length) ){
				return length;
			}
		},
		//edgeLength: undefined, // Arbor, cola, make sure it is undefined
		stepSize: 0.1, /// Arbor smoothing of arbor bounding box
		stableEnergy: function( energy ){// Arbor
			var e = energy;
			return (e.max <= 0.5) || (e.mean <= 0.3);
		},
		avoidOverlap: true, // cola: if true, prevents overlap of node bounding boxes
		handleDisconnected: true, // cola: if true, avoids disconnected components from overlapping
		nodeSpacing: function( node ){ return 10; }, // extra spacing around nodes
		flow: undefined, // cola: use DAG/tree flow layout if specified, e.g. { axis: 'y', minSeparation: 30 }
		alignment: undefined, // cola: relative alignment constraints on nodes, e.g. function( node ){ return { x: 0, y: 1 } }
		edgeSymDiffLength: undefined, // cola: symmetric diff edge length in simulation
		edgeJaccardLength: undefined, // cola: jaccard edge length in simulation
		unconstrIter: undefined, // cola: unconstrained initial layout iterations
		userConstIter: undefined, // cola: initial layout iterations with user-specified constraints
		allConstIter: undefined, // cola: initial layout iterations with all constraints including non-overlap
		nodeSep: undefined, // dagre: the separation between adjacent nodes in the same rank
		edgeSep: undefined, // dagre: the separation between adjacent edges in the same rank
		rankSep: undefined, // dagre: the separation between adjacent nodes in the same rank
		rankDir: 'LR', // dagre: 'TB' for top to bottom flow, 'LR' for left to right,
		ranker: 'longest-path', // dagre: Type of algorithm to assign a rank to each node in the input graph. Possible values: 'network-simplex', 'tight-tree' or 'longest-path'
		minLen: function( edge ){ return 1; }, // dagre: number of ranks to keep between the source and target of the edge
		edgeWeight: function( edge ){ return 1; }, // dagre: higher weight edges are generally made shorter and straighter than lower weight edges
		spacingFactor: undefined, // Applies a multiplicative factor (>0) to expand or compress the overall area that the nodes take up
		springLength: function(edge) { return 80 }, // Euler: The ideal length of a spring
		springCoeff: function(edge) { return 0.0088 }, // Euler: Hooke's law coefficient
		mass: function(node) { return 4 },// Euler: The mass of the node in the physics simulation
		gravity: -1.2,// Euler: Coulomb's law coefficient
		pull: 0.001,// Euler: A force that pulls nodes towards the origin (0, 0)
		theta: 0.666,// Euler: Theta coefficient from Barnes-Hut simulation
		dragCoeff: 0.02,// Euler: Friction / drag coefficient to make the system stabilise over time
		movementThreshold: 1,// Euler: When the total of the squared position deltas is less than this value, the simulation ends
		timeStep: 20,// Euler: The amount of time passed per tick
		maxIterations: 1000, //Euler, ngraph
		klay: {
			addUnnecessaryBendpoints: false, // Adds bend points even if an edge does not change direction.
			aspectRatio: 1.6, // The aimed aspect ratio of the drawing, that is the quotient of width by height
			borderSpacing: 20, // Minimal amount of space to be left to the border
			compactComponents: false, // Tries to further compact components (disconnected sub-graphs).
			crossingMinimization: 'LAYER_SWEEP', // Strategy for crossing minimization.
			cycleBreaking: 'GREEDY', // Strategy for cycle breaking. Cycle breaking looks for cycles in the graph and determines which edges to reverse to break the cycles. Reversed edges will end up pointing to the opposite direction of regular edges (that is, reversed edges will point left if edges usually point right).
			direction: 'DOWN', // Overall direction of edges: horizontal (right / left) or vertical (down / up)
			/* UNDEFINED, RIGHT, LEFT, DOWN, UP */
			edgeRouting: 'ORTHOGONAL', // Defines how edges are routed (POLYLINE, ORTHOGONAL, SPLINES)
			edgeSpacingFactor: 0.5, // Factor by which the object spacing is multiplied to arrive at the minimal spacing between edges.
			feedbackEdges: true, // Whether feedback edges should be highlighted by routing around the nodes.
			fixedAlignment: 'NONE', // Tells the BK node placer to use a certain alignment instead of taking the optimal result.  This option should usually be left alone.
			/* NONE Chooses the smallest layout from the four possible candidates.
			LEFTUP Chooses the left-up candidate from the four possible candidates.
			RIGHTUP Chooses the right-up candidate from the four possible candidates.
			LEFTDOWN Chooses the left-down candidate from the four possible candidates.
			RIGHTDOWN Chooses the right-down candidate from the four possible candidates.
			BALANCED Creates a balanced layout from the four possible candidates. */
			inLayerSpacingFactor: 1.0, // Factor by which the usual spacing is multiplied to determine the in-layer spacing between objects.
			layoutHierarchy: true, // Whether the selected layouter should consider the full hierarchy
			linearSegmentsDeflectionDampening: 0.3, // Dampens the movement of nodes to keep the diagram from getting too large.
			mergeEdges: false, // Edges that have no ports are merged so they touch the connected nodes at the same points.
			mergeHierarchyCrossingEdges: true, // If hierarchical layout is active, hierarchy-crossing edges use as few hierarchical ports as possible.
			nodeLayering:'NETWORK_SIMPLEX', // Strategy for node layering. 'NETWORK_SIMPLEX', 'LONGEST_PATH', 'INTERACTIVE'
			nodePlacement:'BRANDES_KOEPF', 	// BRANDES_KOEPF Minimizes the number of edge bends at the expense of diagram size
											// LINEAR_SEGMENTS Computes a balanced placement.
											// INTERACTIVE Tries to keep the preset y coordinates of nodes from the original layout. For dummy nodes, a guess is made to infer their coordinates. Requires the other interactive phase implementations to have run as well.
											// SIMPLE Minimizes the area at the expense of... well, pretty much everything else. */
			randomizationSeed: 1, // Seed used for pseudo-random number generators to control the layout algorithm; 0 means a new seed is generated
			routeSelfLoopInside: false, // Whether a self-loop is routed around or inside its node.
			separateConnectedComponents: true, // Whether each connected component should be processed separately
			spacing: 20, // Overall setting for the minimal amount of space to be left between objects
			thoroughness: 7 // How much effort should be spent to produce a nice layout..
		},
		priority: function( edge ){ return null; }, // Klay: Edges with a non-nil value are skipped when greedy edge cycle breaking is enabled
		async: { // ngraph
			 maxIterations: 1000,// ngraph: tell layout that we want to compute all at once:
			 stepsPerCycle: 30,// ngraph: 
			 waitForStep: false // ngraph: Run it till the end:
		},
		physics: {// ngraph: 
			springLength: 100, // ngraph: Ideal length for links (springs in physical model).
			springCoeff: 0.0008,// ngraph: Hook's law coefficient. 1 - solid spring.
			gravity: -1.2, // ngraph: Coulomb's law coefficient.
			theta: 0.8, // ngraph: Theta coefficient from Barnes Hut simulation
			dragCoeff: 0.02, // ngraph: Drag force coefficient. Used to slow down system
			timeStep: 20, // ngraph: Default time step (dt) for forces integration
			iterations: 10000,// ngraph
			fit: true,// ngraph
			stableThreshold: 0.000009// ngraph
		},
		iterations: 10000,// ngraph
		refreshInterval: 16, // ngraph: in ms
		refreshIterations: 10, // ngraph: iterations until thread sends an update
		stableThreshold: 2,// ngraph
		infinite: false // overrides all other options for a forces-all-the-time mode
	};
	
	var optionsSpringy = {
		name: "springy",
		animate: true, // whether to show the layout as it's running
		maxSimulationTime: 4000, // max length in ms to run the layout
		ungrabifyWhileSimulating: false, // so you can't drag nodes during layout
		fit: true, // whether to fit the viewport to the graph
		padding: 30, // padding on fit
		boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
		randomize: false, // whether to use random initial positions
		infinite: false, // overrides all other options for a forces-all-the-time mode
		ready: undefined, // callback on layoutready
		stop: undefined, // callback on layoutstop

		// springy forces and config
		stiffness: 400,
		repulsion: 400,
		damping: 0.5,
		edgeLength: function( edge ){
			var length = edge.data('length');
			if( length !== undefined && !isNaN(length) ){
				return length;
			}
		}
	};
	
	var optionsD3Force = {
		name: "d3-force",
        animate: true, // whether to show the layout as it's running; special 'end' value makes the layout animate like a discrete layout
        maxIterations: 0, // max iterations before the layout will bail out
        maxSimulationTime: 0, // max length in ms to run the layout
        ungrabifyWhileSimulating: false, // so you can't drag nodes during layout
        fixedAfterDragging: false, // fixed node after dragging (user specified by expandslider)
        fit: false, // on every layout reposition of nodes, fit the viewport
        padding: 30, // padding around the simulation
        boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
        /**d3-force API**/
        alpha: 0.5, // sets the current alpha to the specified number in the range [0,1] (user specified by 
        alphaMin: undefined, // sets the minimum alpha to the specified number in the range [0,1]
        alphaDecay: undefined, // sets the alpha decay rate to the specified number in the range [0,1]
        alphaTarget: undefined, // sets the current target alpha to the specified number in the range [0,1]
        velocityDecay: undefined, // sets the velocity decay factor to the specified number in the range [0,1]
        collideRadius: 100, // sets the radius accessor to the specified number or function (user specified by nestingslider)
        collideStrength: undefined, // sets the force strength to the specified number in the range [0,1] (user specified by overlapslider)
        collideIterations: undefined, // sets the number of iterations per application to the specified number
        linkId: undefined, // sets the node id accessor to the specified function
        linkDistance: 10, // sets the distance accessor to the specified number or function (user specified by edgelengthslider) 
        linkStrength: undefined, // -0.01, // sets the strength accessor to the specified number or function (user specified by overlapslider)
        linkIterations: undefined, // sets the number of iterations per application to the specified number
        manyBodyStrength: -600, // sets the strength accessor to the specified number or function (user specified by elasticityslider)
        manyBodyTheta: undefined, // sets the Barnes–Hut approximation criterion to the specified number
        manyBodyDistanceMin: undefined, // sets the minimum distance between nodes over which this force is considered
        manyBodyDistanceMax: undefined, // sets the maximum distance between nodes over which this force is considered
        xStrength: undefined, // sets the strength accessor to the specified number or function
        xX: undefined, // sets the x-coordinate accessor to the specified number or function
        yStrength: undefined, // sets the strength accessor to the specified number or function
        yY: undefined, // sets the y-coordinate accessor to the specified number or function
        radialStrength: undefined, // sets the strength accessor to the specified number or function (user specified by edgelengthslider) 
        radialRadius: 10, // sets the circle radius to the specified number or function (user specified by spreadslider)
        radialX: undefined, // sets the x-coordinate of the circle center to the specified number
        radialY: undefined, // sets the y-coordinate of the circle center to the specified number
        // layout event callbacks
        ready: function(){}, // on layoutready
        stop: function(){}, // on layoutstop
        tick: function() {}, // on every iteration
        // positioning options
        randomize: false, // use random node positions at beginning of layout
        // infinite layout options
        infinite: false // overrides all other options for a forces-all-the-time mode
	};
	
	function doLayout() {
		var checkedalgo = document.querySelector('input[name="algo"]:checked');
		console.log("layout options ");
		if (checkedalgo) {
			var algo = checkedalgo.value;
			if (algo) {
				if (algo == "springy") {
					console.log(optionsSpringy);
					console.log("layouting with " + algo);
					cy.layout( optionsSpringy ).run();
				} else if (algo == "d3-force") {
					console.log(optionsD3Force);
					console.log("layouting with " + algo);
					cy.layout( optionsD3Force ).run();
				} else {
					console.log(options);
					options.name = algo;
					console.log("layouting with " + algo);
					if(document.getElementById('excludeparentlinks').checked){
						var edgesToParents = getEdgesToParents(cy);
						cy = removeEdges(cy, edgesToParents);
						cy.layout( options ).run();
						cy = addEdges(cy, edgesToParents);
					} else {
						cy.layout( options ).run();
					}
				}
			}
		} else {
			if(document.getElementById('excludeparentlinks').checked){
				var edgesToParents = getEdgesToParents(cy);
				cy = removeEdges(cy, edgesToParents);
				cy.layout( options ).run();
				cy = addEdges(cy, edgesToParents);
			} else {
				cy.layout( options ).run();
			}
		}
	};

	var spreadslider = document.getElementById("spreadslider");
	var output = document.getElementById("spreadvalue");
	output.innerHTML = spreadslider.value;
	spreadslider.oninput = function() {
	  output.innerHTML = this.value;
	  options.minDist = parseInt(this.value);
	  optionsD3Force.radialRadius = parseInt(this.value);
	}
	spreadslider.onmouseup = function() {
		if (options.klay.nodePlacement === 'BRANDES_KOEPF') {
			options.klay.nodePlacement = 'LINEAR_SEGMENTS';
		} else if (options.klay.nodePlacement === 'LINEAR_SEGMENTS') {
			options.klay.nodePlacement = 'SIMPLE';
		} else {
			options.klay.nodePlacement = 'BRANDES_KOEPF';
		} 

		doLayout();
	};
	
	var expandslider = document.getElementById("expandslider");
	var outputexpandslider = document.getElementById("expandvalue");
	outputexpandslider.innerHTML = expandslider.value;
	expandslider.oninput = function() {
	  outputexpandslider.innerHTML = this.value;
	  options.expandingFactor = parseInt(this.value);
	  options.klay.spacing = parseInt(this.value);
	  options.pull = parseInt(this.value)/parseFloat(1000);
	}
	expandslider.onmouseup = function() {
		optionsD3Force.fixedAfterDragging = !optionsD3Force.fixedAfterDragging;
		doLayout();
	};
	
	var repulsionslider = document.getElementById("repulsionslider");
	var outputrepulsionslider = document.getElementById("repulsionvalue");
	outputrepulsionslider.innerHTML = repulsionslider.value;
	repulsionslider.oninput = function() {
	  outputrepulsionslider.innerHTML = this.value;
	  options.nodeRepulsion = parseInt(this.value);
	  options.repulsion = parseInt(this.value);
	  optionsSpringy.repulsion = parseInt(this.value);
	  options.klay.thoroughness = parseInt(this.value);
	  options.theta = parseInt(this.value)/parseFloat(1000);
	  optionsD3Force.alpha = parseInt(this.value)/parseFloat(1000);
	}
	repulsionslider.onmouseup = function() {
		doLayout();
	};
	
	var overlapslider = document.getElementById("overlapslider");
	var outputoverlapslider = document.getElementById("overlapvalue");
	outputoverlapslider.innerHTML = overlapslider.value;
	overlapslider.oninput = function() {
	  outputoverlapslider.innerHTML = this.value;
	  options.nodeOverlap = parseInt(this.value);
	  options.stiffness = parseInt(this.value);
	  optionsSpringy.stiffness = parseInt(this.value);
	  options.dragCoeff = parseInt(this.value)/parseFloat(100);
	  //optionsD3Force.collideStrength = -parseInt(this.value)/parseFloat(1000);
	  //optionsD3Force.linkStrength = -parseInt(this.value)/parseFloat(10000);
	  //optionsD3Force.radialStrength = -parseInt(this.value)*5/parseFloat(10000);
	}
	overlapslider.onmouseup = function() {
		doLayout();
	};
	
	var edgelengthslider = document.getElementById("edgelengthslider");
	var outputedgelengthslider = document.getElementById("edgelengthvalue");
	outputedgelengthslider.innerHTML = edgelengthslider.value;
	edgelengthslider.oninput = function() {
	  outputedgelengthslider.innerHTML = this.value;
	  options.idealEdgeLength = parseInt(this.value);
	  options.edgeLength = parseInt(this.value);
	  optionsSpringy.edgeLength = parseInt(this.value);
	  optionsD3Force.linkDistance = parseInt(this.value)/10;
	}
	edgelengthslider.onmouseup = function() {
		if (options.klay.nodeLayering === 'NETWORK_SIMPLEX') {
			options.klay.nodeLayering = 'LONGEST_PATH';
		} else {
			options.klay.nodeLayering = 'NETWORK_SIMPLEX';
		} 

		doLayout();
	};
	
	var elasticityslider = document.getElementById("elasticityslider");
	var outputelasticityslider = document.getElementById("elasticityvalue");
	outputelasticityslider.innerHTML = elasticityslider.value;
	elasticityslider.oninput = function() {
	  outputelasticityslider.innerHTML = this.value;
	  options.edgeElelasticity = parseInt(this.value)/parseFloat(10);
	  options.damping = parseInt(this.value)/parseFloat(10);
	  optionsSpringy.damping = parseInt(this.value)/parseFloat(10);
	  options.klay.linearSegmentsDeflectionDampening = parseInt(this.value)/parseFloat(10);
	  optionsD3Force.manyBodyStrength = -parseInt(this.value); 
	}
	elasticityslider.onmouseup = function() {
		doLayout();
	};
	
	var nestingslider = document.getElementById("nestingslider");
	var outputnestingslider = document.getElementById("nestingvalue");
	outputnestingslider.innerHTML = nestingslider.value;
	nestingslider.oninput = function() {
	  outputnestingslider.innerHTML = this.value;
	  options.nestingFactor = parseInt(this.value)/parseFloat(10);
	  optionsD3Force.collideRadius = parseInt(this.value);
	}
	nestingslider.onmouseup = function() {
		if (options.klay.randomizationSeed === 0) {
		  options.klay.randomizationSeed = 1;
		} else {
		  options.klay.randomizationSeed = 0;
		} 
		doLayout();
	};
	
	var nodesizeslider = document.getElementById("nodesizeslider");
	var outputnodesizeslider = document.getElementById("nodesizevalue");
	outputnodesizeslider.innerHTML = nodesizeslider.value;
	nodesizeslider.oninput = function() {
	  outputnodesizeslider.innerHTML = this.value;
	  var nodesize = parseInt(this.value);
	}
	nodesizeslider.onmouseup = function() {
		var nodesize = parseInt(this.value);
		var nodes = cy.nodes();
		for(var i = 0; i < nodes.length; i++){
			var curNode = nodes[i];
			curNode.style("font-size", nodesize);
		}
	}
	
	var nodeborderslider = document.getElementById("nodeborderslider");
	var outputnodeborderslider = document.getElementById("nodebordervalue");
	outputnodeborderslider.innerHTML = nodeborderslider.value;
	nodeborderslider.oninput = function() {
	  outputnodeborderslider.innerHTML = this.value;
	  var nodeborder = parseInt(this.value);
	}
	nodeborderslider.onmouseup = function() {
		var nodeborder = parseInt(this.value);
		var nodes = cy.nodes();
		for(var i = 0; i < nodes.length; i++){
			var curNode = nodes[i];
			curNode.style("border-width", nodeborder);
		}
	}
	
	var edgesizeslider = document.getElementById("edgesizeslider");
	var outputedgesizeslider = document.getElementById("edgesizevalue");
	outputedgesizeslider.innerHTML = edgesizeslider.value;
	edgesizeslider.oninput = function() {
	  outputedgesizeslider.innerHTML = this.value;
	  var edgesize = parseInt(this.value);
	}
	edgesizeslider.onmouseup = function() {
		var edgesize = parseInt(this.value);
		var edges = cy.edges();
		for(var i = 0; i < edges.length; i++){
			var curEdge = edges[i];
			curEdge.style("font-size", edgesize);
		}
	}
	
	var edgewidthslider = document.getElementById("edgewidthslider");
	var outputedgewidthslider = document.getElementById("edgewidthvalue");
	outputedgewidthslider.innerHTML = edgewidthslider.value;
	edgewidthslider.oninput = function() {
	  outputedgewidthslider.innerHTML = this.value;
	  var edgewidth = parseInt(this.value);
	}
	edgewidthslider.onmouseup = function() {
		var edgewidth = parseInt(this.value);
		var edges = cy.edges();
		for(var i = 0; i < edges.length; i++){
			var curEdge = edges[i];
			curEdge.style("width", edgewidth);
		}
	}
	
	var loading = document.getElementById('loading');
	loading.classList.add('loaded');
	
	var hasPresetPositions = determinePresetPositions();
	if (hasPresetPositions) {
		options.name = 'preset';
	}
	
	if(document.getElementById('excludeparentlinks').checked){
		var edgesToParents = getEdgesToParents(cy);
		cy = removeEdges(cy, edgesToParents);
		cy.layout( options ).run();
		cy = addEdges(cy, edgesToParents);
	} else {
		cy.layout( options ).run();
	}

	var defaultContextSettings = {
		menuRadius: 50, // the radius of the circular menu in pixels
		selector: 'node', // elements matching this Cytoscape.js selector will trigger cxtmenus
		commands: [ // an array of commands to list in the menu or a function that returns the array
			{ // example command
				fillColor: 'rgba(200, 200, 200, 0.75)', // optional: custom background color for item
				content: 'log id', // html/text content to be displayed in the menu
				contentStyle: {}, // css key:value pairs to set the command's css in js if you want
				select: function(ele) { // a function to execute when the command is selected
				console.log( ele.id() ) // `ele` holds the reference to the active element
				},
				enabled: true // whether the command is selectable
			}, 
			{ // example command
				fillColor: 'rgba(200, 200, 200, 0.75)', // optional: custom background color for item
				content: 'log xy', // html/text content to be displayed in the menu
				contentStyle: {}, // css key:value pairs to set the command's css in js if you want
				select: function(ele) { // a function to execute when the command is selected
					console.log( "x=" + ele.position('x') + " y=" + ele.position('y') ) // `ele` holds the reference to the active element
				},
				enabled: true // whether the command is selectable
			}
		],
		fillColor: 'rgba(0, 0, 0, 0.75)', // the background color of the menu
		activeFillColor: 'rgba(1, 105, 217, 0.75)', // the color used to indicate the selected command
		activePadding: 20, // additional size in pixels for the active command
		indicatorSize: 24, // the size in pixels of the pointer to the active command
		separatorWidth: 3, // the empty spacing in pixels between successive commands
		spotlightPadding: 4, // extra spacing in pixels between the element and the spotlight
		minSpotlightRadius: 24, // the minimum radius in pixels of the spotlight
		maxSpotlightRadius: 38, // the maximum radius in pixels of the spotlight
		openMenuEvents: 'cxttapstart taphold', // space-separated cytoscape events that will open the menu; only `cxttapstart` and/or `taphold` work here
		itemColor: 'white', // the colour of text in the command's content
		itemTextShadowColor: 'transparent', // the text shadow colour of the command's content
		zIndex: 9999, // the z-index of the ui div
		atMouse: false // draw menu at mouse position
	};

	var menu = cy.cxtmenu( defaultContextSettings );
	
	var navigationOptions = {
		container: false, // can be a HTML or jQuery element or jQuery selector
		viewLiveFramerate: 0, // set false to update graph pan only on drag end; set 0 to do it instantly; set a number (frames per second) to update not more than N times per second
		thumbnailEventFramerate: 30, // max thumbnail's updates per second triggered by graph updates
		thumbnailLiveFramerate: false, // max thumbnail's updates per second. Set false to disable
		dblClickDelay: 200, // milliseconds
		removeCustomContainer: true, // destroy the container specified by user on plugin destroy
		rerenderDelay: 100 // ms to throttle rerender updates to the panzoom for performance
	};

	var nav = cy.navigator( navigationOptions ); // get navigator instance, nav
	
	var collapseOptions = {
		layoutBy: {
			name: 'klay',
			animate: true,
			animationEasing: 'ease-out',// Easing of the animation for animate:'end'
			animationDuration: 250,// The duration of the animation for animate:'end'
			randomize: false,
			fit: true
		}, // to rearrange after expand/collapse. It's just layout options or whole layout function. Choose your side!
		// recommended usage: use cose-bilkent layout with randomize: false to preserve mental map upon expand/collapse
		fisheye: true, // whether to perform fisheye view after expand/collapse you can specify a function too
		animate: false, // whether to animate on drawing changes you can specify a function too
		animationEasing: 'ease-out',// Easing of the animation for animate:'end'
		animationDuration: 250,// The duration of the animation for animate:'end'
		ready: function () { }, // callback when expand/collapse initialized
		undoable: true, // and if undoRedoExtension exists,
		cueEnabled: true, // Whether cues are enabled
		expandCollapseCuePosition: 'top-left', // default cue position is top left you can specify a function per node too
		expandCollapseCueSize: 12, // size of expand-collapse cue
		expandCollapseCueLineSize: 8, // size of lines used for drawing plus-minus icons
		expandCueImage: undefined, // image of expand icon if undefined draw regular expand cue
		collapseCueImage: undefined, // image of collapse icon if undefined draw regular collapse cue
		expandCollapseCueSensitivity: 1 // sensitivity of expand-collapse cues
	};
	
	var ur = cy.undoRedo(); // external (optional) extension which enables undo/redo of expand/collapse
	//document.getElementById('collapseAll').style['opacity'] = "1.0";
	//document.getElementById('expandAll').style['opacity'] = "1.0";
	//document.getElementById('collapseRecursively').style['opacity'] = "1.0";
	//document.getElementById('expandRecursively').style['opacity'] = "1.0";
	
	cy.expandCollapse( collapseOptions );
	
	//if (!hasPresetPositions) {
	//	ur.do("collapseAll"); // start collapsed by default
	//}
	
	var exportOptions = {
		output: 'base64', 
		bg: undefined,
		full: true,
		scale: 1.0,
		maxWidth: undefined,
		maxHeight: undefined,
		quality: 1.0
	};
	
	var ranking = cy.elements().pageRank();

	var panZoomOptions = {
		zoomFactor: 0.05, // zoom factor per zoom tick
		zoomDelay: 45, // how many ms between zoom ticks
		minZoom: 0.1, // min zoom level
		maxZoom: 10, // max zoom level
		fitPadding: 50, // padding when fitting
		panSpeed: 10, // how many ms in between pan ticks
		panDistance: 10, // max pan distance per tick
		panDragAreaSize: 75, // the length of the pan drag box in which the vector for panning is calculated (bigger = finer control of pan speed and direction)
		panMinPercentSpeed: 0.25, // the slowest speed we can pan by (as a percent of panSpeed)
		panInactiveArea: 8, // radius of inactive area in pan drag box
		panIndicatorMinOpacity: 0.5, // min opacity of pan indicator (the draggable nib); scales from this to 1.0
		zoomOnly: false, // a minimal version of the ui only with zooming (useful on systems with bad mousewheel resolution)
		fitSelector: undefined, // selector of elements to fit
		animateOnFit: function(){ // whether to animate on fit
			return false;
		},
		fitAnimationDuration: 1000, // duration of animation on fit
		sliderHandleIcon: 'fa fa-minus',
		zoomInIcon: 'fa fa-plus',
		zoomOutIcon: 'fa fa-minus',
		resetIcon: 'fa fa-expand'
	};

	cy.panzoom( panZoomOptions );

	var api = cy.viewUtilities({
		neighbor: function(node){
			return node.closedNeighborhood();
		},
		neighborSelectTime: 1000
	});

	function determinePresetPositions() {
		var isPreset = false;
		var elements = cy.elements();
		for(var i = 0; i < elements.length; i++){
			var curElement = elements[i];
			var position = curElement._private.position;
			if (position && position.x && position.y) {
				if (position.x === 0 && position.y === 0) {
					isPreset = false;
				} else if (position.x != 0 || position.y != 0) {
					isPrset = true;
					break;
				}
			}
		}
		return isPreset;
	}

	function thickenBorder(eles){
		eles.forEach(function( ele ){
			var defaultBorderWidth = Number(ele.css("border-width").substring(0,ele.css("border-width").length-2));
			ele.css("border-width", defaultBorderWidth + 2);
		});
		eles.data("thickBorder", true);
		return eles;
	}
	
	function thinBorder(eles){
		eles.forEach(function( ele ){
			var defaultBorderWidth = Number(ele.css("border-width").substring(0,ele.css("border-width").length-2));
			ele.css("border-width", defaultBorderWidth - 2);
		});
		eles.removeData("thickBorder");
		return eles;
	}

	ur.action("thickenBorder", thickenBorder, thinBorder);
	ur.action("thinBorder", thinBorder, thickenBorder);

	$("#hide").click(function (){
		var actions = [];                    
		var nodesToHide = cy.$(":selected").add(cy.$(":selected").nodes().descendants());                  
		var nodesWithHiddenNeighbor = cy.edges(":hidden").connectedNodes().intersection(nodesToHide);
		actions.push({name: "thinBorder", param: nodesWithHiddenNeighbor});
		actions.push({name: "hide", param: nodesToHide});                    
		nodesWithHiddenNeighbor = nodesToHide.neighborhood(":visible")
			.nodes().difference(nodesToHide).difference(cy.nodes("[thickBorder]"));
		actions.push({name: "thickenBorder", param: nodesWithHiddenNeighbor}); 
		cy.undoRedo().do("batch", actions);
	});
	$("#showAll").click(function () {
		var actions = [];
		var nodesWithHiddenNeighbor = cy.nodes("[thickBorder]");
		actions.push({name: "thinBorder", param: nodesWithHiddenNeighbor});
		actions.push({name: "show", param: cy.elements()});
		ur.do("batch", actions);                    
	});

	$("#showHiddenNeighbors").click(function () {
		var hiddenEles = cy.$(":selected").neighborhood().filter(':hidden');
		var actions = [];
		var nodesWithHiddenNeighbor = (hiddenEles.neighborhood(":visible").nodes("[thickBorder]"))
			.difference(cy.edges(":hidden").difference(hiddenEles.edges().union(hiddenEles.nodes().connectedEdges())).connectedNodes());
		actions.push({name: "thinBorder", param: nodesWithHiddenNeighbor});
		actions.push({name: "show", param: hiddenEles.union(hiddenEles.parent())});
		nodesWithHiddenNeighbor = hiddenEles.nodes().edgesWith(cy.nodes(":hidden").difference(hiddenEles.nodes()))
			.connectedNodes().intersection(hiddenEles.nodes());
		actions.push({name: "thickenBorder", param: nodesWithHiddenNeighbor}); 
		cy.undoRedo().do("batch", actions);                
	});

	var tappedBefore;
	cy.on('tap', 'node', function (event) {
		var node = this;
		var tappedNow = node;
		setTimeout(function () {
			tappedBefore = null;
		}, 300);
		if (tappedBefore && tappedBefore.id() === tappedNow.id()) {
			tappedNow.trigger('doubleTap');
			tappedBefore = null;
		} else {
			tappedBefore = tappedNow;

			if(document.getElementById('taptoshow').checked){
				//Hide/Show on tap
				if (this.connectedEdges().targets()[1]) {
					if (this.connectedEdges().targets()[1].style("display") == "none"){
						//show the nodes and edges
						this.connectedEdges().targets().style("display", "element");
					} else {
						//hide the children nodes and edges recursively
						this.successors().targets().style("display", "none");
					}
				}
			}						
		}
	});

	cy.on('doubleTap', 'node', function (event) {
		var hiddenEles = cy.$(":selected").neighborhood().filter(':hidden');
		var actions = [];
		var nodesWithHiddenNeighbor = (hiddenEles.neighborhood(":visible").nodes("[thickBorder]"))
			.difference(cy.edges(":hidden").difference(hiddenEles.edges().union(hiddenEles.nodes().connectedEdges())).connectedNodes());
		actions.push({name: "thinBorder", param: nodesWithHiddenNeighbor});
		actions.push({name: "show", param: hiddenEles.union(hiddenEles.parent())});
		nodesWithHiddenNeighbor = hiddenEles.nodes().edgesWith(cy.nodes(":hidden").difference(hiddenEles.nodes()))
			.connectedNodes().intersection(hiddenEles.nodes());
		actions.push({name: "thickenBorder", param: nodesWithHiddenNeighbor}); 
		cy.undoRedo().do("batch", actions);
		if(document.getElementById('layout').checked){
			if(document.getElementById('excludeparentlinks').checked){
				var edgesToParents = getEdgesToParents(cy);
				cy = removeEdges(cy, edgesToParents);
				cy.layout( options ).run();
				cy = addEdges(cy, edgesToParents);
			} else {
				cy.layout( options ).run();
			}
		}  
	});
	
	$("#highlightNeighbors").click(function () {
		if(cy.$(":selected").length > 0)
			ur.do("highlightNeighbors", cy.$(":selected"));
	});
	
	$("#removeHighlights").click(function () {
		ur.do("removeHighlights");
	});
	
	function panIn(target) {
		cy.animate({
			fit: {
				eles: target,
				padding: 200
			},
			duration: 700,
			easing: 'ease',
			queue: true
		});
	}; 
	
	$("#search").click(function () {
		var text = $("#tosearch").val();
		var filteredElements = cy.filter(function(element, i){
			if( element.data("label") && element.data("label").toString().indexOf(text) !== -1){
				return true;
			}
				return false;
		});
		if (filteredElements) {
			var actions = [];
			actions.push({name: "thickenBorder", param: filteredElements});
			actions.push({name: "show", param: filteredElements});
			ur.do("batch", actions);
			panIn(filteredElements[0]);
		}
	});

	/*document.getElementById('collapseRecursively').addEventListener('click', function () {
		ur.do("collapseRecursively", {
			nodes: cy.$(":selected")
		});
		console.log("collapsed recursively");
	});
	document.getElementById('expandRecursively').addEventListener('click', function () {
		ur.do("expandRecursively", {
			nodes: cy.$(":selected")
		});
		console.log("expanded recursively");
	});
	document.getElementById('collapseAll').addEventListener('click', function () {
		ur.do("collapseAll"); // cy.collapseAll(options);
		console.log("collapsed all");
	});
	document.getElementById('expandAll').addEventListener('click', function () {
		ur.do("expandAll");
		console.log("expanded all");
	});*/
    document.addEventListener('keydown', function (e){
		if (e.ctrlKey && e.which == '90') {
			cy.undoRedo().undo();
			console.log("undo");
		}
		else if (e.ctrlKey && e.which == '89') {
			cy.undoRedo().redo();
			console.log("redo");
		}
	}, true );
	document.getElementById('exportpng').addEventListener('click', function () {
		console.log("export png");
		var pngExport = cy.png( exportOptions );
		writefile("graph.png", pngExport.toString(), {type:"image/png"});
	});
	document.getElementById('exportjpg').addEventListener('click', function () {
		console.log("export jpg");
		var jpgExport = cy.jpg( exportOptions );
		writefile("graph.jpg", jpgExport.toString(), {type:"image/jpg"});
	});
	document.getElementById('exportjson').addEventListener('click', function () {
		console.log("export json");
		var jsonExport = cy.json();
		writefile("graph.json", JSON.stringify(jsonExport, null, 4), {type:"text/plain"});
	});
	document.getElementById('exportsvg').addEventListener('click', function () {
		console.log("export svg");
		var svgExport = cy.svgConvertor();
		writefile("graph.svg", svgExport.toString(), {type:"image/svg"});
	});
	
	function getInitialPositions() {
		if (!initialPositions && !initialPositions[0]) {
			selectAllFromInitialPositions()
			.then(function(results){
				if (results && results[0] && results[0].data) {
					initialPositions = JSON.parse(results[0].data);
					console.log("loading initial positions from local database #entities = " + data.length);
				} else {
					console.log("No persisted position data found.");
					return Promise.reject();
				}
				return Promise.resolve(initialPositions);
			})
		}
		return Promise.resolve(initialPositions);
	}

	cy.on('layoutstop', function() {
		cy.maxZoom(1.0);
		cy.animate({
			fit: {
				eles: cy.elements(),
				padding: 50
			}
		}, {
			duration: 200,
		}, {
		easing: 'ease'
		});
	    cy.center();
	    cy.maxZoom(100);
	});

	function changeToPresetPositions() {
		getInitialPositions()
		.then(function(initialPos){
			try {
				var arrayLength = initialPos.length;
				var pan = cy.pan();
				var zoom = cy.zoom(); 
				for (var i = 0; i < arrayLength; i++) {
				    var storedId = initialPos[i].id;
				    var storedX = initialPos[i].x;
				    var storedY = initialPos[i].y;
				    var node = getNodeByDataId(storedId);
				    var newX = (parseInt(storedX) - pan['x'])/zoom;
				    var newY =  (parseInt(storedY) - pan['y'])/zoom;
					node.animation({
						renderedPosition: { x: newX, y: newY },
						duration: 200,
						easing: 'ease'
					}).play();
					//node.renderedPosition('x', (parseInt(storedX) - pan['x'])/zoom);
				    //node.renderedPosition('y', (parseInt(storedY) - pan['y'])/zoom);
				}
			}
			catch (error){
				console.log("Error updating graph positions: " + error);
				return Promise.reject();
			}
			return Promise.resolve(cy.elements());
		})
		.then(function() {
			cy.layout( options ).run();
			return Promise.resolve(cy.elements());
		});
	}
	
	function fitGraphInView(elements) {
		//fireMouseEvents("div.cy-panzoom-reset.cy-panzoom-zoom-button",['mousedown']);
		//document.getElementsByClassName('cy-panzoom-reset cy-panzoom-zoom-button')[0].click();	
		if( elements.size() === 0 ){
			cy.reset();
		} else {
			cy.resize();
			cy.animate({
				fit: {
					eles: elements,
					padding: 50
				}
			}, {
					duration: 200
			});
		}
		return Promise.resolve(cy.elements());
	}

	function getNodeByDataId(id) {
		var nodes = cy.nodes();
	    for (var i = 0; i < nodes.length; ++i) {
			if (nodes[i].data('id') && nodes[i].data('id') == id) {
				return nodes[i];
			}
	    }
	}

	document.getElementById('layoutalgo').addEventListener('click', function () {
		var algo = document.querySelector('input[name="algo"]:checked').value;
		options.name = algo;
		collapseOptions.layoutBy.name = algo; 

		var textualtree = document.getElementById('textualtree');
		var cyDiv = document.getElementById('cy');

		console.log("layouting with " + algo);
		var thisOptions;
		if (algo === 'dagre') {
			if (options.rankDir === 'LR') {
				options.rankDir = 'TB';
				thisOptions = options;
			} else {
				options.rankDir = 'LR';
				thisOptions = options;
			}
		} else if (algo === 'klay') {
			if (options.klay.direction === 'RIGHT') {
				options.klay.direction = 'DOWN';
				thisOptions = options;
			} else {
				options.klay.direction = 'RIGHT';
				thisOptions = options;
			}
		} else if (algo == "springy") {
			thisOptions = optionsSpringy;
		} else if (algo == "d3-force") {
			thisOptions = optionsD3Force;
		} else {
			thisOptions = options;
		}
		
		console.log("layouting options: ");
		console.log(thisOptions);
		document.getElementById('loading').classList.remove('loaded');
		if(document.getElementById('excludeparentlinks').checked){
			var edgesToParents = getEdgesToParents(cy);
			cy = removeEdges(cy, edgesToParents);
			if (algo === "preset") {
				changeToPresetPositions();
			} else {
				cy.layout( thisOptions ).run();
			}
			cy = addEdges(cy, edgesToParents);
		} else {
			if (algo === "preset") {
				changeToPresetPositions();
			} else {
				cy.layout( thisOptions ).run();
			}
		}
		document.getElementById('loading').classList.add('loaded');
	});
	/*document.getElementById('collapsible').addEventListener('click', function () {
		if(document.getElementById('collapsible').checked){
			document.getElementById('collapseAll').style['opacity'] = "1.0";
			document.getElementById('expandAll').style['opacity'] = "1.0";
			document.getElementById('collapseRecursively').style['opacity'] = "1.0";
			document.getElementById('expandRecursively').style['opacity'] = "1.0";
			cy.expandCollapse( collapseOptions );
			ur.do("collapseAll"); // start collapsed by default
        } else {
			window.location.reload(true);
		}					
	});*/
	document.getElementById('taptoshow').addEventListener('click', function () {
		var nodes = cy.nodes();
		if(document.getElementById('taptoshow').checked){
			//recursively hide all children of the root node
			cy.$("#" + nodes[0].data.id).successors().targets().style("display", "none");
        } else {
			window.location.reload(true);
		}					
	});
	document.getElementById('excludeparentlinks').addEventListener('click', function () {
		if(document.getElementById('excludeparentlinks').checked){
			var edgesToParents = getEdgesToParents(cy);
			cy = removeEdges(cy, edgesToParents);
			cy.layout( options ).run();
			cy = addEdges(cy, edgesToParents);
        } else {
			var edgesToParents = getEdgesToParents(cy);
			cy = addEdges(cy, edgesToParents);
			cy.layout( options ).run();
		}					
	});
	document.getElementById('tooltips').addEventListener('click', function () {
		if(document.getElementById('tooltips').checked){
			cy.elements().qtip({
				content: function(){ 
					 return this._private.data.popup.toString();
				},
				position: {
					my: 'top center',
					at: 'bottom center'
				},
				style: {
					classes: 'qtip-bootstrap',
					tip: {
						width: 16,
						height: 8
					}
				}
			});
		} else {
			window.location.reload(true);
		}
	});					
	document.getElementById('graphdblook').addEventListener('click', function () {
		if(document.getElementById('graphdblook').checked){
			var nodes = cy.nodes();
			for(var i = 0; i < nodes.length; i++){
				var curNode = nodes[i];
				curNode.style("shape", "ellipse");
				curNode.style("width", "100");
				curNode.style("height", "100");
				curNode.style("font-size", "32pt");
				curNode.style("border-width", "10");
			}
			var edges = cy.edges();
			for(var i = 0; i < edges.length; i++){
				var curEdge = edges[i];
				curEdge.style("width", "30");
				curEdge.style("line-color", "#888");
				curEdge.style("target-arrow-color", "#888");
				curEdge.style("text-outline-color", "#000");
				curEdge.style("text-max-width", "400px");
				curEdge.style("color", "#fff");
				curEdge.style("font-size", "24pt");
				curEdge.style("text-rotation", "autorotate");
			}
		} else {
			window.location.reload(true);
		}
	});
	document.getElementById('fisheye').addEventListener('click', function () {
		if(document.getElementById('fisheye').checked){
			console.log("fisheye view");
			var edges = cy.edges();
			for(var i = 0; i < edges.length; i++){
				var curEdge = edges[i];
				curEdge.style("curve-style", "bezier");
			}
			const lodash = _.noConflict();
			cy.fit({padding: 50});
			cy.nodes().forEach(node => {
				node.scratch('_fisheye', {});
				node.scratch('_fisheye').x = node.position('x');
				node.scratch('_fisheye').y = node.position('y');
			});
			let fisheyeFlag = false;
			cy.on('mouseover', 'node', lodash.debounce(function (evt) {
				const position = evt.target.position();
				if (!fisheyeFlag) {
					fisheyeFlag = true;
					var node = this;
					var normalsize = node.numericStyle("font-size");
					var largesize = Math.max(normalsize*10, 100);
// animation not working with cy > v3.3.0
//					var sizeAni = node.animation({
//						  style: { 
//							  "font-size": largesize
//						  }
//						}, {
//						  duration: 200
//						});
//					sizeAni.play();
					node.style("font-size", largesize);
					cy.nodes().layout({
						name: 'fisheye',
						focus: position,
						animate: true
					}).run()
				}
			}, 250));
			cy.on('mouseout', 'node', lodash.debounce(function (evt) {
				if (fisheyeFlag) {
					var node = this;
					var largesize = node.numericStyle("font-size");
					var normalsize = Math.max(parseInt(largesize/10), 12);
// animation not working with cy > v3.3.0
//					var sizeAni = node.animation({
//						  style: { 
//							  "font-size": normalsize
//						  }
//						}, {
//						  duration: 200
//						});
//					sizeAni.play();
					node.style("font-size", normalsize);
					cy.nodes().forEach(node => {
						node.animate({
							position: node.scratch('_fisheye')
						});
					});
					fisheyeFlag = false;
				}
			}, 250));
		} else {
			window.location.reload(true);
		}
	});
	document.getElementById('scalebydegree').addEventListener('click', function () {
		if(document.getElementById('scalebydegree').checked){
			console.log("scale node size by number of connections");
			cy.nodes().forEach(node => {
				var numLinks = node.degree(false);
				var normalsize = node.numericStyle("font-size");
				var largesize = Math.max(parseInt(normalsize*numLinks), 12);
				node.style("font-size", largesize);
			});
		} else {
			cy.nodes().forEach(node => {
				var numLinks = node.degree(false);
				var largesize = node.numericStyle("font-size");
				var normalsize = Math.max(parseInt(largesize/(numLinks)), 12);
				node.style("font-size", normalsize);
			});
		}
	});
}