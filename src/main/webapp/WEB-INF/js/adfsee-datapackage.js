/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
var data;
var treeData;
var datapackage;
var tabledata;
var loadafo = true;
var loadaft = true;
var dolayout = true;
var multigraph = false;
var skolemize = false;
var loadexample = false;
var includeadf = false;
var columnWidths;
var columnHeaders;

initGraphStore();

document.addEventListener('DOMContentLoaded', function() {
		var loading = document.getElementById('loading');
		loading.classList.remove('loaded');

		if (localStorage.getItem('datapackageStatus') && localStorage.getItem('datapackageStatus') == freshStartStatus) {
			resetLocalDbAndDataOfDatapackage().then(createDatapackage);
		} else if (localStorage.getItem('datapackageStatus') && localStorage.getItem('datapackageStatus') == datapackageAvailableStatus) {
			selectAllFromDatapackage()
				.then(updateDatapackageStateWithSelectedResults)
				.then(createDatapackage);
		} else {
			localStorage.setItem('datapackageStatus', freshStartStatus);
			resetLocalDbAndDataOfDatapackage()
				.then(function() {
					console	.log("No state information found in local datapackage store. Restarting from scratch.");
					return Promise.resolve();
				})
				.then(createDatapackage);
		}
});

function createDatapackage() {
	var loading = document.getElementById('loading');
	loading.classList.remove('loaded');
	var tempTree = [];
	tempTree.push(datapackage);
	try {
		if ($('#datapackage').tree('getTree')) {
			$('#datapackage').tree('loadData', tempTree);
		} else {
			$(function() {
				$('#datapackage').tree({
					data: tempTree,
					dragAndDrop: false,
					autoOpen: 0, 
					saveState: true,
					animationSpeed: 100,
					useContextMenu: false,
					autoEscape: false,
					onCreateLi: function(node, $li) {
						if (node.children.length <= 0 || node.name === "/") {
							$li.find('.jqtree-element').append('<a href="' + node.url +'" class="download" download><span style="font-weight:900;">&nbsp;&nbsp;&#x21B7;</span></a>');
						}
			        },
					closedIcon: $('<i class="fa fa-arrow-circle-right"></i>'),
					openedIcon: $('<i class="fa fa-arrow-circle-down"></i>')
				});
			});
			
			var $tree = $('#datapackage');
			$tree.on(
			    'tree.click',
			    function(event) {
			        event.preventDefault();
			        var node = event.node;
			        $("#details").html("");
			        var htmlString = "";
			        var keys = Object.keys(node);
			        for (var key in keys) {
			        	var value = node[keys[key]];
			        	if (isObject(value)) {
			        		continue;
			        	}
			        	htmlString = htmlString + "<span class=\"key\">" + keys[key] + ":</span>&nbsp;&nbsp;<span class=\"value\">" + value + "</span><br/>";
			        }
			        $("#details").html(htmlString);
			    }
			);
		}
	} catch (error) {
		console.log("Error during drawing jqtree of datapackage: " + error);
		loading.classList.add('loaded');
		return Promise.reject();
	}
	finally {
		loading.classList.add('loaded');

		$('#collapse').click(function() {
			var tree = $('#datapackage').tree('getTree');
			tree.iterate(function(node) {
				if (node.hasChildren()) {
					$('#datapackage').tree('closeNode', node, true);
				}
				return true;
			});
		});

		$('#expand').click(function() {
			var tree = $('#datapackage').tree('getTree');
			tree.iterate(function(node) {
				if (node.hasChildren()) {
					$('#datapackage').tree('openNode', node, true);
				}
				return true;
			});
		});    	
	}
	return Promise.resolve();
}

var getKeys = function(obj){
   var keys = [];
   for(var key in obj){
      keys.push(key);
   }
   return keys;
}