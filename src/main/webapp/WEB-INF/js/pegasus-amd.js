define('pegasus', [], function() {

// a   url (naming it a, because it will be reused to store callbacks)
// e   timeout error placeholder to avoid using var, not to be used
// xhr placeholder to avoid using var, not to be used
function pegasus(a, e, xhr) {
  xhr = new XMLHttpRequest();

  // Set URL
  xhr.open('GET', a);

  // Don't need a to store URL anymore
  // Reuse it to store callbacks
  a = [];

  pegasus.timeout && (xhr.timeout = pegasus.timeout);

  xhr.ontimeout = function (event) {
    e = event
  }

  xhr.onreadystatechange = xhr.then = function(onSuccess, onError, cb, data) {
    // Test if onSuccess is a function
    // Means that the user called xhr.then
    if (onSuccess && onSuccess.call) {
      a = [,onSuccess, onError];
    }

    // Test if there's a timeout error
    e && a[2] && a[2](e, xhr)

    // Test if request is complete
    if (xhr.readyState == 4) {
      // index will be:
      // 0 if undefined
      // 1 if status is between 200 and 399
      // 2 if status is over
      cb = a[0|xhr.status / 200];
      if (cb) {
        try {
          data = JSON.parse(xhr.responseText)
        } catch (e) {
          data = null
        }
        cb(data, xhr);
      }
    }
  };

  // Send the GET request
  xhr.send();

  // Return request
  return xhr;
}


return pegasus;

});
//The MIT License (MIT)
//
//Copyright (c) 2014 typicode
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.
// source https://github.com/typicode/pegasus