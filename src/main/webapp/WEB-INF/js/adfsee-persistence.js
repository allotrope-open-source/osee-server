/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
var Connection = new JsStore.Instance();
var DbName ='GraphStore';

function getDbSchema() {
	var TblGraphs = {
			Name: 'Graphs',
			Columns: [
				{
					Name: 'name',
					PrimaryKey: true,
					NotNull: true
				}, 
				{
					Name: 'data',
					NotNull: true
				} 
			]
		};
	var TblInitialPositions = {
			Name: 'Positions',
			Columns: [
				{
					Name: 'name',
					PrimaryKey: true,
					NotNull: true
				}, 
				{
					Name: 'data',
					NotNull: true
				} 
			]
		};
	var TblTrees = {
			Name: 'Trees',
			Columns: [
				{
					Name: 'name',
					PrimaryKey: true,
					NotNull: true
				}, 
				{
					Name: 'data',
					NotNull: true
				} 
			]
		};
	var TblAudittrails = {
			Name: 'Audittrails',
			Columns: [
				{
					Name: 'name',
					PrimaryKey: true,
					NotNull: true
				}, 
				{
					Name: 'data',
					NotNull: true
				} 
			]
		};
	var TblAdfInfos = {
			Name: 'AdfInfos',
			Columns: [
				{
					Name: 'name',
					PrimaryKey: true,
					NotNull: true
				}, 
				{
					Name: 'data',
					NotNull: true
				} 
			]
		};
	var TblDatapackages = {
			Name: 'Datapackages',
			Columns: [
				{
					Name: 'name',
					PrimaryKey: true,
					NotNull: true
				}, 
				{
					Name: 'data',
					NotNull: true
				} 
			]
		};
	var TblDatadescriptions = {
			Name: 'Datadescriptions',
			Columns: [
				{
					Name: 'name',
					PrimaryKey: true,
					NotNull: true
				}, 
				{
					Name: 'data',
					NotNull: true
				} 
			]
		};
	var TblQueryResults = {
			Name: 'QueryResults',
			Columns: [
				{
					Name: 'name',
					PrimaryKey: true,
					NotNull: true
				}, 
				{
					Name: 'data',
					NotNull: true
				} 
			]
		};
	var TblDatacubes = {
			Name: 'Datacubes',
			Columns: [
				{
					Name: 'name',
					PrimaryKey: true,
					NotNull: true
				}, 
				{
					Name: 'data',
					NotNull: true
				} 
			]
		};
	var TblInput = {
			Name: 'Inputs',
			Columns: [
				{
					Name: 'name',
					PrimaryKey: true,
					NotNull: true
				}, 
				{
					Name: 'data',
					NotNull: true
				} 
			]
		};
	var Db = {
		Name: DbName,
		Tables: [TblGraphs, TblTrees, TblInitialPositions, TblQueryResults, TblDatacubes, TblAudittrails, TblAdfInfos, TblDatapackages, TblDatadescriptions, TblInput]
	}
	return Db;
}

function initGraphStore() {
	JsStore.isDbExist(DbName, function(isExist) {
		if (isExist) {
			console.log("Reusing existing local database with name: " + DbName)
			Connection.openDb(DbName);
		} else {
			var Database = getDbSchema();
			console.log("Creating new local database with name: " + DbName)
			console.log(Database);
			Connection.createDb(Database);
		}
	}, function(error) {
		console.log("Error during init of local database.")
		console.error(error);
	})
}	

async function execSelectGraphs() {
	var res = await Connection.select({
		From: 'Graphs'
	});
	
	return Promise.resolve(res);
}

async function selectAllFromGraphs() {
	var results = await execSelectGraphs()
	.then(function(results) {
		console.log("Total number of query results in graph table = " + ((results) ? results.length : "0"));
		return Promise.resolve(results);
	})
	.catch(function(error) {
		console.log("Error during reading graph data from local storage: ");
		console.log(JSON.stringify(error));
		return Promise.reject();
	});
	return Promise.resolve(results);
}

async function execSelectInitialPositions() {
	var res = await Connection.select({
		From: 'Positions'
	});
	
	return Promise.resolve(res);
}

async function selectAllFromInitialPositions() {
	var results = await execSelectInitialPositions()
	.then(function(results) {
		console.log("Total number of query results in positions table = " + ((results) ? results.length : "0"));
		return Promise.resolve(results);
	})
	.catch(function(error) {
		console.log("Error during reading positions data from local storage: ");
		console.log(JSON.stringify(error));
		return Promise.reject();
	});
	return Promise.resolve(results);
}

async function execSelectTrees() {
	var res = await Connection.select({
		From: 'Trees'
	});
	
	return Promise.resolve(res);
}

async function selectAllFromTrees() {
	var results = await execSelectTrees()
	.then(function(results) {
		console.log("Total number of query results in tree table = " + ((results) ? results.length : "0"));
		return Promise.resolve(results);
	})
	.catch(function(error) {
		console.log("Error during reading tree data from local storage: ");
		console.log(JSON.stringify(error));
		return Promise.reject();
	});
	return Promise.resolve(results);
}

async function execSelectAudittrails() {
	var res = await Connection.select({
		From: 'Audittrails'
	});
	
	return Promise.resolve(res);
}

async function selectAllFromAudittrails() {
	var results = await execSelectAudittrails()
	.then(function(results) {
		console.log("Total number of query results in audittrail table = " + ((results) ? results.length : "0"));
		return Promise.resolve(results);
	})
	.catch(function(error) {
		console.log("Error during reading audittrail data from local storage: ");
		console.log(JSON.stringify(error));
		return Promise.reject();
	});
	return Promise.resolve(results);
}

async function execSelectAdfInfos() {
	var res = await Connection.select({
		From: 'AdfInfos'
	});
	
	return Promise.resolve(res);
}

async function selectAllFromAdfInfos() {
	var results = await execSelectAdfInfos()
	.then(function(results) {
		console.log("Total number of query results in adfInfo table = " + ((results) ? results.length : "0"));
		return Promise.resolve(results);
	})
	.catch(function(error) {
		console.log("Error during reading adfInfo data from local storage: ");
		console.log(JSON.stringify(error));
		return Promise.reject();
	});
	return Promise.resolve(results);
}

async function execSelectDatapackages() {
	var res = await Connection.select({
		From: 'Datapackages'
	});
	
	return Promise.resolve(res);
}

async function selectAllFromDatapackage() {
	var results = await execSelectDatapackages()
	.then(function(results) {
		console.log("Total number of query results in datapackage table = " + ((results) ? results.length : "0"));
		return Promise.resolve(results);
	})
	.catch(function(error) {
		console.log("Error during reading datapackage data from local storage: ");
		console.log(JSON.stringify(error));
		return Promise.reject();
	});
	return Promise.resolve(results);
}

async function execSelectDatadescriptions() {
	var res = await Connection.select({
		From: 'Datadescriptions'
	});
	
	return Promise.resolve(res);
}

async function selectAllFromDatadescription() {
	var results = await execSelectDatadescriptions()
	.then(function(results) {
		console.log("Total number of query results in datadescription table = " + ((results) ? results.length : "0"));
		return Promise.resolve(results);
	})
	.catch(function(error) {
		console.log("Error during reading datadescription data from local storage: ");
		console.log(JSON.stringify(error));
		return Promise.reject();
	});
	return Promise.resolve(results);
}

async function execSelectQueryResults() {
	var res = await Connection.select({
		From: 'QueryResults'
	});
	
	return Promise.resolve(res);
}

async function selectAllFromQueryResults() {
	var results = await execSelectQueryResults()
	.then(function(results) {
		console.log("Total number of query results in query results table = " + ((results) ? results.length : "0"));
		return Promise.resolve(results);
	})
	.catch(function(error) {
		console.log("Error during reading query results data from local storage: ");
		console.log(JSON.stringify(error));
		return Promise.reject();
	});
	return Promise.resolve(results);
}

async function execSelectDatacubes() {
	var res = await Connection.select({
		From: 'Datacubes'
	});
	
	return Promise.resolve(res);
}

async function selectAllFromDatacubes() {
	var results = await execSelectDatacubes()
	.then(function(results) {
		console.log("Total number of query results in datacubes table = " + ((results) ? results.length : "0"));
		return Promise.resolve(results);
	})
	.catch(function(error) {
		console.log("Error during reading datacubes from local storage: ");
		console.log(JSON.stringify(error));
		return Promise.reject();
	});
	return Promise.resolve(results);
}

async function execSelectInputs() {
	var res = await Connection.select({
		From: 'Inputs'
	});
	
	return Promise.resolve(res);
}

async function selectAllFromInputs() {
	var results = await execSelectInputs()
	.then(function(results) {
		console.log("Total number of entries in inputs table = " + ((results) ? results.length : "0"));
		return Promise.resolve(results);
	})
	.catch(function(error) {
		console.log("Error during reading inputs from local storage: ");
		console.log(JSON.stringify(error));
		return Promise.reject();
	});
	return Promise.resolve(results);
}

function willDrawGraph() {
	return new Promise(function(resolve, reject) {
		drawGraph();
		var statusOk = true;
		if (statusOk) {
			resolve();
		} else {
			reject();
		}
	});
}

async function checkIfGraphDataExist() {
	var res = await Connection.select({
		From: "Graphs",
		Where: {
			"name": "graphdata",
		}
	});
	return Promise.resolve(res);
}

async function updateGraphs(e) {
	var res = await Connection.update({
		In: "Graphs",
		Set: {
			"name": "graphdata",
			"data" : JSON.stringify(e)
		},
		Where: {
			"name": "graphdata"
		}
	});
	return Promise.resolve(res);
}

async function insertGraphs(e) {
	var Values = { 
			"name" : "graphdata",
			"data" : JSON.stringify(e)
	};
	var res = await Connection.insert({
		Into: 'Graphs',
		Values: [Values],
		Return: true
	});
	
	return Promise.resolve(res);
}

async function addGraphDataToLocalStorage(e) {
	await checkIfGraphDataExist()	
	.then(function (results){
		if (results && results[0]) {
			return Promise.resolve(true);
		} else {
			return Promise.resolve(false)
		}
	})
	.catch(function (error) {
		console.log("Error during querying graph data from local storage: " + JSON.stringify(error));
		return Promise.reject();
	})
	.then(async function updateOrInsert(isUpdate) {
		if (isUpdate) {
			console.log("Found graph data in local storage. Updating...");
			await updateGraphs(e)
			.then(function (rowUpdated){
				console.log("Updated graph data in local storage.");
				return Promise.resolve();
			})
			.catch(function (error) {
				console.log("Error during updating graph data in local storage:" + JSON.stringify(error));
				return Promise.reject();
			});
		} else {
			console.log("Inserting graph data to local storage.");
			await insertGraphs(e)
			.then(function(values) {
				console.log("Added graph data to local storage #entities = " + ((values) ? values.length : "0"));
				return Promise.resolve();
			})
			.catch(function(error) {
				console.log("Error during inserting graph data to local storage: " + JSON.stringify(error));
				return Promise.reject();
			});
		}
		return Promise.resolve();
	})
	.catch(function(error) {
		console.log("Error during creating or updating graph data in local storage: " + JSON.stringify(error));
		return Promise.reject();
	});
}

async function checkIfPositionDataExist() {
	var res = await Connection.select({
		From: "Positions",
		Where: {
			"name": "positiondata",
		}
	});
	return Promise.resolve(res);
}

async function updatePositions(e) {
	var res = await Connection.update({
		In: "Positions",
		Set: {
			"name": "positiondata",
			"data" : JSON.stringify(e)
		},
		Where: {
			"name": "positiondata"
		}
	});
	return Promise.resolve(res);
}

async function insertPositions(e) {
	var Values = { 
			"name" : "positiondata",
			"data" : JSON.stringify(e)
	};
	var res = await Connection.insert({
		Into: 'Positions',
		Values: [Values],
		Return: true
	});
	
	return Promise.resolve(res);
}

async function addPositionsDataToLocalStorage(e) {
	await checkIfPositionDataExist()	
	.then(function (results){
		if (results && results[0]) {
			return Promise.resolve(true);
		} else {
			return Promise.resolve(false)
		}
	})
	.catch(function (error) {
		console.log("Error during querying position data from local storage: " + JSON.stringify(error));
		return Promise.reject();
	})
	.then(async function updateOrInsert(isUpdate) {
		if (isUpdate) {
			console.log("Found position data in local storage. Updating...");
			await updatePositions(e)
			.then(function (rowUpdated){
				console.log("Updated position data in local storage.");
				return Promise.resolve();
			})
			.catch(function (error) {
				console.log("Error during updating position data in local storage:" + JSON.stringify(error));
				return Promise.reject();
			});
		} else {
			console.log("Inserting position data to local storage.");
			await insertPositions(e)
			.then(function(values) {
				console.log("Added position data to local storage #entities = " + ((values) ? values.length : "0"));
				return Promise.resolve();
			})
			.catch(function(error) {
				console.log("Error during inserting position data to local storage: " + JSON.stringify(error));
				return Promise.reject();
			});
		}
		return Promise.resolve();
	})
	.catch(function(error) {
		console.log("Error during creating or updating position data in local storage: " + JSON.stringify(error));
		return Promise.reject();
	});
}

async function checkIfTreeDataExist() {
	var res = await Connection.select({
		From: "Trees",
		Where: {
			"name": "treedata",
		}
	});
	return Promise.resolve(res);
}

async function updateTrees(e) {
	var res = await Connection.update({
		In: "Trees",
		Set: {
			"name": "treedata",
			"data" : JSON.stringify(e)
		},
		Where: {
			"name": "treedata"
		}
	});
	return Promise.resolve(res);
}

async function insertTrees(e) {
	var Values = { 
			"name" : "treedata",
			"data" : JSON.stringify(e)
	};
	var res = await Connection.insert({
		Into: 'Trees',
		Values: [Values],
		Return: true
	});
	
	return Promise.resolve(res);
}

async function addTreeDataToLocalStorage(e) {
	await checkIfTreeDataExist()	
	.then(function (results){
		if (results && results[0]) {
			return Promise.resolve(true);
		} else {
			return Promise.resolve(false)
		}
	})
	.catch(function (error) {
		console.log("Error during querying tree data from local storage: " + JSON.stringify(error));
		return Promise.reject();
	})
	.then(async function updateOrInsert(isUpdate) {
		if (isUpdate) {
			console.log("Found tree data in local storage. Updating...");
			await updateTrees(e)
			.then(function (rowUpdated){
				console.log("Updated tree data in local storage.");
				return Promise.resolve();
			})
			.catch(function (error) {
				console.log("Error during updating tree data in local storage:" + JSON.stringify(error));
				return Promise.reject();
			});
		} else {
			console.log("Inserting tree data to local storage.");
			await insertTrees(e)
			.then(function(values) {
				console.log("Added tree data to local storage #entities = " + ((values) ? values.length : "0"));
				return Promise.resolve();
			})
			.catch(function(error) {
				console.log("Error during inserting tree data to local storage: " + JSON.stringify(error));
				return Promise.reject();
			});
		}
		return Promise.resolve();
	})
	.catch(function(error) {
		console.log("Error during creating or updating tree data in local storage: " + JSON.stringify(error));
		return Promise.reject();
	});
}

async function checkIfAudittrailDataExist() {
	var res = await Connection.select({
		From: "Audittrails",
		Where: {
			"name": "audittraildata",
		}
	});
	return Promise.resolve(res);
}

async function updateAudittrails(e) {
	var res = await Connection.update({
		In: "Audittrails",
		Set: {
			"name": "audittraildata",
			"data" : JSON.stringify(e)
		},
		Where: {
			"name": "audittraildata"
		}
	});
	return Promise.resolve(res);
}

async function insertAudittrails(e) {
	var Values = { 
			"name" : "audittraildata",
			"data" : JSON.stringify(e)
	};
	var res = await Connection.insert({
		Into: 'Audittrails',
		Values: [Values],
		Return: true
	});
	
	return Promise.resolve(res);
}

async function addAudittrailDataToLocalStorage(e) {
	await checkIfAudittrailDataExist()	
	.then(function (results){
		if (results && results[0]) {
			return Promise.resolve(true);
		} else {
			return Promise.resolve(false)
		}
	})
	.catch(function (error) {
		console.log("Error during querying audittrail data from local storage: " + JSON.stringify(error));
		return Promise.reject();
	})
	.then(async function updateOrInsert(isUpdate) {
		if (isUpdate) {
			console.log("Found audittrail data in local storage. Updating...");
			await updateAudittrails(e)
			.then(function (rowUpdated){
				console.log("Updated audittrail data in local storage.");
				return Promise.resolve();
			})
			.catch(function (error) {
				console.log("Error during updating audittrail data in local storage:" + JSON.stringify(error));
				return Promise.reject();
			});
		} else {
			console.log("Inserting audittrail data to local storage.");
			await insertAudittrails(e)
			.then(function(values) {
				console.log("Added audittrail data to local storage #entities = " + ((values) ? values.length : "0"));
				return Promise.resolve();
			})
			.catch(function(error) {
				console.log("Error during inserting audittrail data to local storage: " + JSON.stringify(error));
				return Promise.reject();
			});
		}
		return Promise.resolve();
	})
	.catch(function(error) {
		console.log("Error during creating or updating audittrail data in local storage: " + JSON.stringify(error));
		return Promise.reject();
	});
}

async function checkIfAdfInfoDataExist() {
	var res = await Connection.select({
		From: "AdfInfos",
		Where: {
			"name": "adfinfodata",
		}
	});
	return Promise.resolve(res);
}

async function updateAdfInfos(e) {
	var res = await Connection.update({
		In: "AdfInfos",
		Set: {
			"name": "adfinfodata",
			"data" : JSON.stringify(e)
		},
		Where: {
			"name": "adfinfodata"
		}
	});
	return Promise.resolve(res);
}

async function insertAdfInfos(e) {
	var Values = { 
			"name" : "adfinfodata",
			"data" : JSON.stringify(e)
	};
	var res = await Connection.insert({
		Into: 'AdfInfos',
		Values: [Values],
		Return: true
	});
	
	return Promise.resolve(res);
}

async function addAdfInfoDataToLocalStorage(e) {
	await checkIfAdfInfoDataExist()	
	.then(function (results){
		if (results && results[0]) {
			return Promise.resolve(true);
		} else {
			return Promise.resolve(false)
		}
	})
	.catch(function (error) {
		console.log("Error during querying adfInfo data from local storage: " + JSON.stringify(error));
		return Promise.reject();
	})
	.then(async function updateOrInsert(isUpdate) {
		if (isUpdate) {
			console.log("Found adfInfo data in local storage. Updating...");
			await updateAdfInfos(e)
			.then(function (rowUpdated){
				console.log("Updated adfInfo data in local storage.");
				return Promise.resolve();
			})
			.catch(function (error) {
				console.log("Error during updating adfInfo data in local storage:" + JSON.stringify(error));
				return Promise.reject();
			});
		} else {
			console.log("Inserting adfInfo data to local storage.");
			await insertAdfInfos(e)
			.then(function(values) {
				console.log("Added adfInfo data to local storage #entities = " + ((values) ? values.length : "0"));
				return Promise.resolve();
			})
			.catch(function(error) {
				console.log("Error during inserting adfInfo data to local storage: " + JSON.stringify(error));
				return Promise.reject();
			});
		}
		return Promise.resolve();
	})
	.catch(function(error) {
		console.log("Error during creating or updating adfInfo data in local storage: " + JSON.stringify(error));
		return Promise.reject();
	});
}

async function checkIfDatapackageDataExist() {
	var res = await Connection.select({
		From: "Datapackages",
		Where: {
			"name": "datapackage",
		}
	});
	return Promise.resolve(res);
}

async function updateDatapackages(e) {
	var res = await Connection.update({
		In: "Datapackages",
		Set: {
			"name": "datapackage",
			"data" : JSON.stringify(e)
		},
		Where: {
			"name": "datapackage"
		}
	});
	return Promise.resolve(res);
}

async function insertDatapackages(e) {
	var Values = { 
			"name" : "datapackage",
			"data" : JSON.stringify(e)
	};
	var res = await Connection.insert({
		Into: 'Datapackages',
		Values: [Values],
		Return: true
	});
	
	return Promise.resolve(res);
}

async function addDatapackageDataToLocalStorage(e) {
	await checkIfDatapackageDataExist()	
	.then(function (results){
		if (results && results[0]) {
			return Promise.resolve(true);
		} else {
			return Promise.resolve(false)
		}
	})
	.catch(function (error) {
		console.log("Error during querying datapackage data from local storage: " + JSON.stringify(error));
		return Promise.reject();
	})
	.then(async function updateOrInsert(isUpdate) {
		if (isUpdate) {
			console.log("Found datapackage data in local storage. Updating...");
			await updateDatapackages(e)
			.then(function (rowUpdated){
				console.log("Updated datapackage data in local storage.");
				return Promise.resolve();
			})
			.catch(function (error) {
				console.log("Error during updating datapackage data in local storage:" + JSON.stringify(error));
				return Promise.reject();
			});
		} else {
			console.log("Inserting datapackage data to local storage.");
			await insertDatapackages(e)
			.then(function(values) {
				console.log("Added datapackage data to local storage #entities = " + ((values) ? values.length : "0"));
				return Promise.resolve();
			})
			.catch(function(error) {
				console.log("Error during inserting datapackage data to local storage: " + JSON.stringify(error));
				return Promise.reject();
			});
		}
		return Promise.resolve();
	})
	.catch(function(error) {
		console.log("Error during creating or updating datapackage data in local storage: " + JSON.stringify(error));
		return Promise.reject();
	});
}

async function checkIfDatadescriptionDataExist() {
	var res = await Connection.select({
		From: "Datadescriptions",
		Where: {
			"name": "datadescription",
		}
	});
	return Promise.resolve(res);
}

async function updateDatadescriptions(e) {
	var res = await Connection.update({
		In: "Datadescriptions",
		Set: {
			"name": "datadescription",
			"data" : JSON.stringify(e)
		},
		Where: {
			"name": "datadescription"
		}
	});
	return Promise.resolve(res);
}

async function insertDatadescriptions(e) {
	var Values = { 
			"name" : "datadescription",
			"data" : JSON.stringify(e)
	};
	var res = await Connection.insert({
		Into: 'Datadescriptions',
		Values: [Values],
		Return: true
	});
	
	return Promise.resolve(res);
}

async function addDatadescriptionDataToLocalStorage(e) {
	await checkIfDatadescriptionDataExist()	
	.then(function (results){
		if (results && results[0]) {
			return Promise.resolve(true);
		} else {
			return Promise.resolve(false)
		}
	})
	.catch(function (error) {
		console.log("Error during querying datadescription data from local storage: " + JSON.stringify(error));
		return Promise.reject();
	})
	.then(async function updateOrInsert(isUpdate) {
		if (isUpdate) {
			console.log("Found datadescription data in local storage. Updating...");
			await updateDatadescriptions(e)
			.then(function (rowUpdated){
				console.log("Updated datadescription data in local storage.");
				return Promise.resolve();
			})
			.catch(function (error) {
				console.log("Error during updating datadescription data in local storage:" + JSON.stringify(error));
				return Promise.reject();
			});
		} else {
			console.log("Inserting datadescription data to local storage.");
			await insertDatadescriptions(e)
			.then(function(values) {
				console.log("Added datadescription data to local storage #entities = " + ((values) ? values.length : "0"));
				return Promise.resolve();
			})
			.catch(function(error) {
				console.log("Error during inserting datadescription data to local storage: " + JSON.stringify(error));
				return Promise.reject();
			});
		}
		return Promise.resolve();
	})
	.catch(function(error) {
		console.log("Error during creating or updating datadescription data in local storage: " + JSON.stringify(error));
		return Promise.reject();
	});
}

async function checkIfQueryResultsExist() {
	var res = await Connection.select({
		From: "QueryResults",
		Where: {
			"name": "queryresultsdata",
		}
	});
	return Promise.resolve(res);
}

async function updateQueryResults(e) {
	var res = await Connection.update({
		In: "QueryResults",
		Set: {
			"name": "queryresultsdata",
			"data" : JSON.stringify(e)
		},
		Where: {
			"name": "queryresultsdata"
		}
	});
	return Promise.resolve(res);
}

async function insertQueryResults(e) {
	var Values = { 
			"name" : "queryresultsdata",
			"data" : JSON.stringify(e)
	};
	var res = await Connection.insert({
		Into: 'QueryResults',
		Values: [Values],
		Return: true
	});
	
	return Promise.resolve(res);
}

async function addQueryResultsDataToLocalStorage(e) {
	await checkIfQueryResultsExist()
	.then(function (results){
		if (results && results[0]) {
			return Promise.resolve(true);
		} else {
			return Promise.resolve(false)
		}
	})
	.catch(function (error) {
		console.log("Error during querying query results data from local storage: " + JSON.stringify(error));
		return Promise.reject();
	})
	.then(async function updateOrInsert(isUpdate) {
		if (isUpdate) {
			console.log("Found query results data in local storage. Updating...");
			await updateQueryResults(e)
			.then(function (rowUpdated){
				console.log("Updated query results data in local storage.");
				return Promise.resolve();
			})
			.catch(function (error) {
				console.log("Error during updating query results data in local storage:" + JSON.stringify(error));
				return Promise.reject();
			});
		} else {
			console.log("Inserting query results data to local storage.");
			await insertQueryResults(e)
			.then(function(values) {
				console.log("Added query results data to local storage #entities = " + ((values) ? values.length : "0"));
				return Promise.resolve();
			})
			.catch(function(error) {
				console.log("Error during inserting query results data to local storage: " + JSON.stringify(error));
				return Promise.reject();
			});
		}
		return Promise.resolve();
	})
	.catch(function(error) {
		console.log("Error during creating or updating query results data in local storage: " + JSON.stringify(error));
		return Promise.reject();
	});
}

async function checkIfDatacubesExist() {
	var res = await Connection.select({
		From: "Datacubes",
		Where: {
			"name": "datacubesdata",
		}
	});
	return Promise.resolve(res);
}

async function updateDatacubes(e) {
	var res = await Connection.update({
		In: "Datacubes",
		Set: {
			"name": "datacubesdata",
			"data" : JSON.stringify(e)
		},
		Where: {
			"name": "datacubesdata"
		}
	});
	return Promise.resolve(res);
}

async function insertDatacubes(e) {
	var Values = { 
			"name" : "datacubesdata",
			"data" : JSON.stringify(e)
	};
	var res = await Connection.insert({
		Into: 'Datacubes',
		Values: [Values],
		Return: true
	});
	
	return Promise.resolve(res);
}

async function addDatacubesToLocalStorage(e) {
	await checkIfDatacubesExist()
	.then(function (results){
		if (results && results[0]) {
			return Promise.resolve(true);
		} else {
			return Promise.resolve(false)
		}
	})
	.catch(function (error) {
		console.log("Error during querying datacubes from local storage: " + JSON.stringify(error));
		return Promise.reject();
	})
	.then(async function updateOrInsert(isUpdate) {
		if (isUpdate) {
			console.log("Found datacubes in local storage. Updating...");
			await updateDatacubes(e)
			.then(function (rowUpdated){
				console.log("Updated datacubes in local storage.");
				return Promise.resolve();
			})
			.catch(function (error) {
				console.log("Error during updating datacubes in local storage:" + JSON.stringify(error));
				return Promise.reject();
			});
		} else {
			console.log("Inserting datacubes to local storage.");
			await insertDatacubes(e)
			.then(function(values) {
				console.log("Added datacubes to local storage #entities = " + ((values) ? values.length : "0"));
				return Promise.resolve();
			})
			.catch(function(error) {
				console.log("Error during inserting datacubes to local storage: " + JSON.stringify(error));
				return Promise.reject();
			});
		}
		return Promise.resolve();
	})
	.catch(function(error) {
		console.log("Error during creating or updating datacubes in local storage: " + JSON.stringify(error));
		return Promise.reject();
	});
}

async function checkIfInputsExist() {
	var res = await Connection.select({
		From: "Inputs",
		Where: {
			"name": "constructdata",
		}
	});
	return Promise.resolve(res);
}

async function updateInputs(e) {
	var res = await Connection.update({
		In: "Inputs",
		Set: {
			"name": "constructdata",
			"data" : JSON.stringify(e)
		},
		Where: {
			"name": "constructdata"
		}
	});
	return Promise.resolve(res);
}

async function insertInputs(e) {
	var Values = { 
			"name" : "constructdata",
			"data" : JSON.stringify(e)
	};
	var res = await Connection.insert({
		Into: 'Inputs',
		Values: [Values],
		Return: true
	});
	
	return Promise.resolve(res);
}

async function addInputsToLocalStorage(e) {
	await checkIfInputsExist()
	.then(function (results){
		if (results && results[0]) {
			return Promise.resolve(true);
		} else {
			return Promise.resolve(false)
		}
	})
	.catch(function (error) {
		console.log("Error during querying inputs from local storage: " + JSON.stringify(error));
		return Promise.reject();
	})
	.then(async function updateOrInsert(isUpdate) {
		if (isUpdate) {
			console.log("Found inputs in local storage. Updating...");
			await updateInputs(e)
			.then(function (rowUpdated){
				console.log("Updated inputs in local storage.");
				return Promise.resolve();
			})
			.catch(function (error) {
				console.log("Error during updating inputs in local storage:" + JSON.stringify(error));
				return Promise.reject();
			});
		} else {
			console.log("Inserting inputs to local storage.");
			await insertInputs(e)
			.then(function(values) {
				console.log("Added inputs to local storage #entities = " + ((values) ? values.length : "0"));
				return Promise.resolve();
			})
			.catch(function(error) {
				console.log("Error during inserting inputs to local storage: " + JSON.stringify(error));
				return Promise.reject();
			});
		}
		return Promise.resolve();
	})
	.catch(function(error) {
		console.log("Error during creating or updating inputs in local storage: " + JSON.stringify(error));
		return Promise.reject();
	});
}

function deleteGraphsFromLocalDb() {
	return Connection.clear("Graphs")
	.then(function () {
		console.log('Graphs deleted from local store.');
		return Promise.resolve();	
	})
	.catch(function (error) {
		console.log('Error during cleaning up local graph store.');
		return Promise.reject();
	});
}			

function deletePositionsFromLocalDb() {
	return Connection.clear("Positions")
	.then(function () {
		console.log('Positions deleted from local store.');
		return Promise.resolve();	
	})
	.catch(function (error) {
		console.log('Error during cleaning up local position store.');
		return Promise.reject();
	});
}			

function deleteTreesFromLocalDb() {
	return Connection.clear("Trees")
	.then(function () {
		console.log('Trees deleted from local store.');
		return Promise.resolve();	
	})
	.catch(function (error) {
		console.log('Error during cleaning up local tree store.');
		return Promise.reject();
	});
}			

function deleteAudittrailsFromLocalDb() {
	return Connection.clear("Audittrails")
	.then(function () {
		console.log('Audittrails deleted from local store.');
		return Promise.resolve();	
	})
	.catch(function (error) {
		console.log('Error during cleaning up local audittrail store.');
		return Promise.reject();
	});
}			

function deleteAdfInfosFromLocalDb() {
	return Connection.clear("AdfInfos")
	.then(function () {
		console.log('AdfInfos deleted from local store.');
		return Promise.resolve();	
	})
	.catch(function (error) {
		console.log('Error during cleaning up local adfInfo store.');
		return Promise.reject();
	});
}			

function deleteDatapackagesFromLocalDb() {
	return Connection.clear("Datapackages")
	.then(function () {
		console.log('Datapackages deleted from local store.');
		return Promise.resolve();	
	})
	.catch(function (error) {
		console.log('Error during cleaning up local datapackage store.');
		return Promise.reject();
	});
}			

function deleteDatadescriptionsFromLocalDb() {
	return Connection.clear("Datadescriptions")
	.then(function () {
		console.log('Datadescriptions deleted from local store.');
		return Promise.resolve();	
	})
	.catch(function (error) {
		console.log('Error during cleaning up local datadescription store.');
		return Promise.reject();
	});
}			

function deleteQueryResultsFromLocalDb() {
	return Connection.clear("QueryResults")
	.then(function () {
		console.log('Query results deleted from local store.');
		return Promise.resolve();	
	})
	.catch(function (error) {
		console.log('Error during cleaning up local query results store.');
		return Promise.reject();
	});
}			

function deleteDatacubesFromLocalDb() {
	return Connection.clear("Datacubes")
	.then(function () {
		console.log('Datacubes deleted from local store.');
		return Promise.resolve();	
	})
	.catch(function (error) {
		console.log('Error during cleaning up local datacubes store.');
		return Promise.reject();
	});
}			

function deleteInputsFromLocalDb() {
	return Connection.clear("Inputs")
	.then(function () {
		console.log('Inputs deleted from local store.');
		return Promise.resolve();	
	})
	.catch(function (error) {
		console.log('Error during cleaning up local inputs store.');
		return Promise.reject();
	});
}			

function resetLocalDbAndDataOfGraphs() {
	return deleteGraphsFromLocalDb()
	.then(deletePositionsFromLocalDb)
	.then(function() {
		data = initData;
		return Promise.resolve();
	});				
}

function resetLocalDbAndDataOfTrees() {
	return deleteTreesFromLocalDb()
	.then(function() {
		treeData = initTreeData;
		return Promise.resolve();
	});				
}

function resetLocalDbAndDataOfAudittrails() {
	return deleteAudittrailsFromLocalDb()
	.then(function() {
		audittrail = initTreeData;
		return Promise.resolve();
	});				
}

function resetLocalDbAndDataOfAdfInfos() {
	return deleteAdfInfosFromLocalDb()
	.then(function() {
		adfInfo = initTreeData;
		return Promise.resolve();
	});				
}

function resetLocalDbAndDataOfDatapackage() {
	return deleteDatapackagesFromLocalDb()
	.then(function() {
		datapackage = initTreeData;
		return Promise.resolve();
	});				
}

function resetLocalDbAndDataOfDatadescription() {
	return deleteDatadescriptionsFromLocalDb()
	.then(function() {
		datadescription = initTreeData;
		return Promise.resolve();
	});				
}

function resetLocalDbAndDataOfQueryResults() {
	return deleteQueryResultsFromLocalDb()
	.then(function() {
		queryResultsData = {};
		return Promise.resolve();
	});				
}

function resetLocalDbAndDataOfDatacubes() {
	return deleteDatacubesFromLocalDb()
	.then(function() {
		datacubes = {};
		return Promise.resolve();
	});				
}
function resetLocalDbAndDataOfInputs() {
	return deleteInputsFromLocalDb()
	.then(function() {
		inputs = {};
		return Promise.resolve();
	});				
}

async function initQuery() {
	var construct = await selectAllFromInputs();
	if (construct && construct[0] && construct[0].data && construct[0].data != "") {
		var query = JSON.parse(construct[0].data);
		if (document.getElementById('construct')){
			document.getElementById('construct').value = query;
			console.log("query: " + query);
			await updatInputsStateWithSelectedResults(construct);
		} else {
			console.log("Skipping init of construct query.");
		}
	} else {
		if (document.getElementById('construct')){
			document.getElementById('construct').value = "construct query";
			console.log("query: construct query");
			await updatInputsStateWithSelectedResults(construct);
		} else {
			console.log("Skipping init of construct query.");
		}
	}
}

document.addEventListener('DOMContentLoaded', async function() {
	if (localStorage.getItem('inputStatus') && localStorage.getItem('inputStatus') == freshStartStatus) {
		await initQuery();
	} else if (localStorage.getItem('inputStatus') && localStorage.getItem('inputStatus') == inputAvailableStatus) {
		await initQuery();
	} else {
		localStorage.setItem('inputStatus', freshStartStatus);
		resetLocalDbAndDataOfInputs()
			.then(function() {
				console.log("No state information found in local input store. Restarting from scratch.");
				return Promise.resolve();
			})
			.then(initQuery);
	}
});
