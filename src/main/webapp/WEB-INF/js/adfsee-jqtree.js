/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
var data;
var treeData;
var loadafo = true;
var loadaft = true;
var dolayout = true;
var multigraph = false;
var skolemize = false;
var loadexample = false;
var includeadf = false;
var tree;

initGraphStore();

document.addEventListener('DOMContentLoaded',function(){
	if (localStorage.getItem('treeStatus') && localStorage.getItem('treeStatus') == freshStartStatus) {
		resetLocalDbAndDataOfTrees()
		.then(drawTree);
	} else if (localStorage.getItem('treeStatus') && localStorage.getItem('treeStatus') == treeAvailableStatus) {
		selectAllFromTrees()
		.then(updateTreeStateWithSelectedResults)
		.then(drawTree);
	} else {
		localStorage.setItem('treeStatus', freshStartStatus);
		resetLocalDbAndDataOfTrees()
		.then(function() {
			console.log("No state information found in local tree store. Restarting from scratch.");
			return Promise.resolve();
		})
		.then(drawTree);
	}
});

function drawTree() {
	var loading = document.getElementById('loading');
	loading.classList.remove('loaded');
	try {
		if ($('#textualtree').tree('getTree')) {
			$('#textualtree').tree('loadData', treeData);
		} else {
			$(function() {
				$('#textualtree').tree({
					data: treeData,
					dragAndDrop: true,
					autoOpen: 0, 
					saveState: true,
					autoEscape: false,
					closedIcon: $('<i class="fa fa-arrow-circle-right"></i>'),
					openedIcon: $('<i class="fa fa-arrow-circle-down"></i>')
				});
			});
			
			var $tree = $('#textualtree');
		}
	} catch (error) {
		console.log("Error during drawing jqtree: " + error);
		loading.classList.add('loaded');
		return Promise.reject();
	}
	finally {
		loading.classList.add('loaded');

		$('#collapse').click(function() {
			var tree = $('#textualtree').tree('getTree');
			tree.iterate(function(node) {
				if (node.hasChildren()) {
					$('#textualtree').tree('closeNode', node, true);
				}
				return true;
			});
		});

		$('#expand').click(function() {
			var tree = $('#textualtree').tree('getTree');
			tree.iterate(function(node) {
				if (node.hasChildren()) {
					$('#textualtree').tree('openNode', node, true);
				}
				return true;
			});
		});    	
	}
	return Promise.resolve();
}