/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
var data;
var treeData;
var tree;
var nodeData;
var biofabricGraph;
var hiddenNodes = [];
var hiddenLinks = [];
var nodes = null;
var links = null;

var nodeColors = {};
var labels = {};
var counter = 0;
var needToRemoveSelfLinks = true;
var showlabels = false;

var numNodes;
var numClusters = 0;

var colorValues = {
	"INFORMATION": "#FFCC00", 
	'INSTANCE': '#98BDF9', 
	'ALLOTROPE': '#DD6C07', 
	'COMMON': '#FFFFFF',
	'MATERIAL': '#C2E7C2',
	'PROCESS': '#CAA3F1',
	'RESULT': '#FFCC00',
	'EQUIPMENT': '#AAD5FF',
	'PROPERTY': '#FFFFFF',
	'QUALITY': '#FFFF99',
	'CONTEXTUAL_ROLE': '#FFC896',
	'REALIZABLE_ENTITY': '#FF99CC',
	'OBO': '#D5ECF7',
	'BFO': '#236099',
	'IAO': '#FFCC00',
	'OBI': '#449d44',
	'PATO': '#234979',
	'RO': '#FFFFFF',
	'CHMO': '#8EC1C5',
	'ENVO': '#A5FF4B',
	'NCBI': '#326597',
	'CHEBI': '#DAD734',
	'EXAMPLE': '#FFDFF7',
	'OTHER': '#FFFFFF',
	'LITERAL' : "#FFF0B3"
};

initGraphStore();

document.addEventListener('DOMContentLoaded',function(){
	loading.classList.remove('loaded');
	if (localStorage.getItem('graphStatus') && localStorage.getItem('graphStatus') == freshStartStatus) {
		resetLocalDbAndDataOfGraphs()
			.then(drawBiofabricGraph);
	} else if (localStorage.getItem('graphStatus') && localStorage.getItem('graphStatus') == graphAvailableStatus) {
		selectAllFromGraphs()
			.then(updateGraphStateWithSelectedResults)
			.then(selectAllFromInitialPositions)
			.then(updatePositionsWithSelectedResults)
			.then(drawBiofabricGraph);
	} else {
		localStorage.setItem('graphStatus', freshStartStatus);
		resetLocalDbAndDataOfGraphs()
			.then(drawBiofabricGraph);
		console.log("No state information found in local graph store. Restarting from scratch.");
	}
});

async function drawBiofabricGraph() {
	let promise = await new Promise(
		function (resolve, reject) {
			try {
				let width = window.screen.width;
				let height = window.screen.height;
				let mouse = [width / 2, height / 2];
				const biofabricData = {
				  sprites: [],
				  links: []
				};
				
				var graphdata = {
						nodes : [],
						links : []
				};
				var theNodeData = data;
				graphdata = getNodesAndLinksFromCytoscapeGraph(theNodeData, graphdata, 0);

				bfab(1800, 3000, d3);
				drawFabric(graphdata); 

			} catch(error) {
				loading.classList.add('loaded');
				console.log("Error during creation of biofabric graph: " + error);
				reject();
			}
			loading.classList.add('loaded');
			resolve(biofabricGraph);
		}
	)
	.catch(error => { 
		loading.classList.add('loaded');
		console.log("Error during creation of biofabric graph: " + error);
	});
	return promise;
}

function redrawBiofabricGraph() {
	drawBiofabricGraph()
		.then(reload);
}

function reload() {
	var reloadAllowed = localStorage.getItem('reloadBiofabricAllowed');
	if (reloadAllowed && reloadAllowed == "1") {
		localStorage.setItem('reloadBiofabricAllowed',"0");
		window.location.reload();
	} else if (!reloadAllowed) {
		localStorage.setItem('reloadBiofabricAllowed',"0");
		window.location.reload();
	} else if (reloadAllowed && reloadAllowed == "0") {
		localStorage.setItem('reloadBiofabricAllowed',"1");
	}
	return Promise.resolve();
}