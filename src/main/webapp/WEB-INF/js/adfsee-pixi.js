/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
var data;
var treeData;
var tree;
var nodeData;
var pixiGraph;
var hiddenNodes = [];
var hiddenLinks = [];
var nodes = null;
var links = null;

var nodeColors = {};
var labels = {};
var counter = 0;
var needToRemoveSelfLinks = true;
var showlabels = false;

var numNodes;
var numClusters = 0;

var colorValues = {
	"INFORMATION": "#FFCC00", 
	'INSTANCE': '#98BDF9', 
	'ALLOTROPE': '#DD6C07', 
	'COMMON': '#FFFFFF',
	'MATERIAL': '#C2E7C2',
	'PROCESS': '#CAA3F1',
	'RESULT': '#FFCC00',
	'EQUIPMENT': '#AAD5FF',
	'PROPERTY': '#FFFFFF',
	'QUALITY': '#FFFF99',
	'CONTEXTUAL_ROLE': '#FFC896',
	'REALIZABLE_ENTITY': '#FF99CC',
	'OBO': '#D5ECF7',
	'BFO': '#236099',
	'IAO': '#FFCC00',
	'OBI': '#449d44',
	'PATO': '#234979',
	'RO': '#FFFFFF',
	'CHMO': '#8EC1C5',
	'ENVO': '#A5FF4B',
	'NCBI': '#326597',
	'CHEBI': '#DAD734',
	'EXAMPLE': '#FFDFF7',
	'OTHER': '#FFFFFF',
	'LITERAL' : "#FFF0B3"
};

initGraphStore();

document.addEventListener('DOMContentLoaded',function(){
	loading.classList.remove('loaded');
	if (localStorage.getItem('graphStatus') && localStorage.getItem('graphStatus') == freshStartStatus) {
		resetLocalDbAndDataOfGraphs()
			.then(drawPixiGraph);
	} else if (localStorage.getItem('graphStatus') && localStorage.getItem('graphStatus') == graphAvailableStatus) {
		selectAllFromGraphs()
			.then(updateGraphStateWithSelectedResults)
			.then(selectAllFromInitialPositions)
			.then(updatePositionsWithSelectedResults)
			.then(drawPixiGraph);
	} else {
		localStorage.setItem('graphStatus', freshStartStatus);
		resetLocalDbAndDataOfGraphs()
			.then(drawPixiGraph);
		console.log("No state information found in local graph store. Restarting from scratch.");
	}
});

async function drawPixiGraph() {
	let promise = await new Promise(
		function (resolve, reject) {
			try {
				let width = window.screen.width;
				let height = window.screen.height;
				let mouse = [width / 2, height / 2];
				let time = 0.0;
				const pixiData = {
				  sprites: [],
				  links: []
				};
				
				const style = new PIXI.TextStyle({
				    breakWords: true,
				    fill: "#0000a0",
				    fontSize: 12,
				    lineJoin: "round",
				    stroke: "white",
				    strokeThickness: 2,
				    wordWrap: true,
				    wordWrapWidth: 200
				});
				
				var graphdata = {
						nodes : [],
						links : []
				};
				var theNodeData = data;
				graphdata = getNodesAndLinksFromCytoscapeGraph(theNodeData, graphdata, 0);

				counter = 0;
			
				const isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/.test(navigator.userAgent);
			
				const numberOfItems = isMobile ? 500 : 3000;
				let globalDragging = false;
			
				const app = new PIXI.Application(width, height, { antialias: true });
				app.renderer.backgroundColor = 0xffffff;
				document.body.appendChild(app.view);
			
				window.addEventListener('resize', resize);
			
			    function resize() {
			    	app.renderer.resize(window.innerWidth, window.innerHeight);
			    }
			
			    resize();
			
			    const viewport = new Viewport.Viewport({
				  screenWidth: width,
				  screenHeight: height,
				  worldWidth: width,
				  worldHeight: height
				});
			
				viewport.wheel().drag().decelerate();
				app.stage.addChild(viewport);
			
				function makeParticleTexture(props) {
				  const gfx = new PIXI.Graphics();
				  gfx.beginFill(props.fill);
				  gfx.lineStyle(props.strokeWidth, props.stroke);
				  gfx.moveTo(props.strokeWidth, props.strokeWidth);
				  gfx.lineTo(props.size - props.strokeWidth, props.strokeWidth);
				  gfx.lineTo(props.size - props.strokeWidth, props.size - props.strokeWidth);
				  gfx.lineTo(props.strokeWidth, props.size - props.strokeWidth);
				  gfx.lineTo(props.strokeWidth, props.strokeWidth);
				  gfx.endFill();
			
				  const texture = app.renderer.generateTexture(gfx, PIXI.SCALE_MODES.LINEAR, 2);
			
				  return texture;
				}
			
				const texture = makeParticleTexture({
				  fill: 0x009009,
				  stroke: 0x000000,
				  strokeWidth: 1,
				  size: isMobile ? 16 : 8
				});
				
				function makeTexture(color) {
					var textureStyle = {
						"fill" : color,
						  stroke: 0x000000,
						  strokeWidth: 1,
						  size: isMobile ? 16 : 8
					};
					return makeParticleTexture(textureStyle);
				}
			
				const textureHover = makeParticleTexture({
				  fill: 0xffffff,
				  stroke: 0x000000,
				  strokeWidth: 1,
				  size: isMobile ? 20 : 10
				});
			
				const linksGraphics = new PIXI.Graphics();
				viewport.addChild(linksGraphics);
				
				function makeSprites(numberOfItems) {
				  const sprites = [];
				  for (let i = 0; i < numberOfItems; i++) {
				    const sprite = new PIXI.Sprite(texture);
				    sprite.x = Math.random() * width;
				    sprite.y = Math.random() * height;
				    sprite.radius = 10;
				    sprite.index = i;
				    sprite.peers = d3
				      .range(Math.floor(Math.random() * 10))
				      .map(() => Math.floor(Math.random() * 100));
				    sprite.anchor.x = 0.5;
				    sprite.anchor.y = 0.5;
				    sprite.rotation = i * 10;
				    sprite.interactive = true;
				    sprite.buttonMode = true; // cursor change
				    sprite.scale.set(Math.random() * 2 + 1);
				    sprite
				      .on("pointerdown", onDragStart)
				      .on("pointerup", onDragEnd)
				      .on("pointerupoutside", onDragEnd)
				      .on("pointerover", onMouseOver)
				      .on("pointerout", onMouseOut)
				      .on("pointermove", onDragMove);
				    sprites.push(sprite);
				    viewport.addChild(sprite);
				  }
				  return sprites;
				}
				
				function makeSpritesForGraphData(g) {
				  const sprites = [];
				  var arrayLength = g.nodes.length;
				  for (var i = 0; i < arrayLength; i++) {
					var spriteTexture = makeTexture(g.nodes[i].color);
				    const sprite = new PIXI.Sprite(spriteTexture);
				    sprite.x = Math.random() * width;
				    sprite.y = Math.random() * height;
				    sprite.radius = 10;
				    sprite.index = i;
				    sprite.id = g.nodes[i].id; 
				    sprite.label = g.nodes[i].name;
					sprite.text = new PIXI.Text(sprite.label, style);
					sprite.text.visible = false;
					sprite.basetexture = spriteTexture;
				    sprite.peers = d3
				      .range(Math.floor(Math.random() * 10))
				      .map(() => Math.floor(Math.random() * 100));
				    sprite.anchor.x = 0.5;
				    sprite.anchor.y = 0.5;
				    sprite.rotation = i * 10;
				    sprite.interactive = true;
				    sprite.buttonMode = true; // cursor change
				    sprite.scale.set(Math.random() * 2 + 1);
				    sprite
				      .on("pointerdown", onDragStart)
				      .on("pointerup", onDragEnd)
				      .on("pointerupoutside", onDragEnd)
				      .on("pointerover", onMouseOver)
				      .on("pointerout", onMouseOut)
				      .on("pointermove", onDragMove);
				    sprites.push(sprite);
				    viewport.addChild(sprite);
				    viewport.addChild(sprite.text);
				  }
				  return sprites;
				}
				
				function makeLinks(nodes) {
				  const links = d3.range(nodes.length - 1).map(i => ({
				    source: Math.floor(Math.sqrt(i)),
				    target: i + 1,
				    value: Math.random() + 0.5
				  }));
				  return links;
				}
				
				function makeLinksForGraphData(sprites, g) {
					var links = [];
					var spritesLength = sprites.length;
					var arrayLength = g.links.length;
					for (var i = 0; i < arrayLength; i++) {
						var sourceId = g.links[i].source;
						var targetId = g.links[i].target;
						var linkSourceIndex = -1;
						var linkTargetIndex = -1;
						for (var j = 0; j < spritesLength; j++) {
							var spriteId = sprites[j].id;
							var index = sprites[j].index;
							if (sourceId == spriteId) {
								linkSourceIndex = index;
							}
							if (targetId == spriteId) {
								linkTargetIndex = index;
							}
							
							if (linkSourceIndex > -1 && linkTargetIndex > -1) {
								break;
							}
						}
						var text = new PIXI.Text(g.links[i].label,style);
						text.visible = false;
							
						var newLink = {
							"source" : linkSourceIndex,
							"target" : linkTargetIndex,
							"value" : g.links[i].value,
							"label" : g.links[i].text,
							"text" : text
						};
						
						links.push(newLink);
					}
					return links;
				}
				
				let tick = 0;
				function makeSimulation(pixiData, manualLooping) {
				  const simulation = d3
				    .forceSimulation(pixiData.sprites)
				    .velocityDecay(0.8)
				    .force(
				      "charge",
				      d3
				        .forceManyBody()
				        .strength(-100)
				        .distanceMax(250)
				        .distanceMin(80)
				    )
				    .force(
				      "center",
				      d3
				        .forceCenter()
				        .x(width * 0.5)
				        .y(height * 0.5)
				    )
				    .force(
				      "link",
				      d3
				        .forceLink(pixiData.links)
				        .id(d => d.index)
				        .distance(80)
				        .strength(d => d.value)
				    )
				    .force(
				      "collision",
				      d3
				        .forceCollide()
				        .radius(d => d.radius)
				        .iterations(2)
				    )
				    .on("tick", function() {});
				  if (manualLooping) simulation.stop();
				  return simulation;
				}
			
				//pixiData.sprites = makeSprites(numberOfItems);
				pixiData.sprites = makeSpritesForGraphData(graphdata);
				//pixiData.links = makeLinks(pixiData.sprites);
				pixiData.links = makeLinksForGraphData(pixiData.sprites, graphdata);
			
				simulation = makeSimulation(pixiData, true);
			
				app.ticker.add(function updateGraphLinks(delta) {
				  simulation.tick();
				  updateLinks(pixiData, linksGraphics);
				  updatelabels(pixiData);
				});
			
				function updatelabels(pixiData) {
					var spritesLength = pixiData.sprites.length;
					if (showlabels) {
						for (var i = 0; i < spritesLength; i++) {
							pixiData.sprites[i].text.x = pixiData.sprites[i].x;
							pixiData.sprites[i].text.y = pixiData.sprites[i].y;
						}
					}
				}

				function onMouseOver() {
				  this.isOver = true;
				  this.texture = textureHover;
				  
				  if (!showlabels) {
					  this.text.x = this.x;
					  this.text.y = this.y + 10;
					  this.text.visible = true;
				  }
				}
			
				function onMouseOut() {
				  if (this.dragging) {
				    return;
				  }
				  this.isOver = false;
				  this.texture = this.basetexture;
				  if (!showlabels) {
					  this.text.visible = false;
				  }
				}
			
				function onDragStart(event) {
				  viewport.plugins.pause('drag');
				  simulation.alphaTarget(0.3).restart();
				  this.isDown = true;
				  this.eventData = event.data;
				  this.alpha = 0.5;
				  this.dragging = true;
				  globalDragging = true;
				  
				}
			
				function onDragEnd(event) {
				  if(!event.active) simulation.alphaTarget(0);
				  this.alpha = 1;
				  this.dragging = false;
				  this.isOver = false;
				  this.data = null;
				  this.fx = null;
				  this.fy = null;
				  this.texture = this.basetexture;
				  globalDragging = false;
				  viewport.plugins.resume('drag');
				}
			
				function onDragMove(event) {
				  if (this.dragging) {
				    const newPosition = this.eventData.getLocalPosition(this.parent);
				    this.fx = newPosition.x;
				    this.fy = newPosition.y;
				  }
				  if (this.isOver) {
				    this.texture = textureHover;
				  }
				}

				document.getElementById("showlabels").addEventListener('click', function() {
					var spritesLength = pixiData.sprites.length;
					if (showlabels) {
						showlabels = false;
						for (var i = 0; i < spritesLength; i++) {
							pixiData.sprites[i].text.visible = false;
						}
					} else {
						showlabels = true;
						for (var i = 0; i < spritesLength; i++) {
							pixiData.sprites[i].text.x = pixiData.sprites[i].x;
							pixiData.sprites[i].text.y = pixiData.sprites[i].y;
							pixiData.sprites[i].text.visible = true;
						}
					}
				});
			
				function updateLinks(_data, _links) {
				  _links.clear();
				  _links.alpha = 0.27;
				  _data.links.forEach(link => {
				    let { source, target } = link;
				    _links.lineStyle(link.value, 0x000000);
				    _links.moveTo(source.x, source.y);
				    _links.lineTo(target.x, target.y);
				  });
				  _links.endFill();
				}

			} catch(error) {
				loading.classList.add('loaded');
				console.log("Error during creation of pixi graph: " + error);
				reject();
			}
			loading.classList.add('loaded');
			resolve(pixiGraph);
		}
	)
	.catch(error => { 
		loading.classList.add('loaded');
		console.log("Error during creation of pixi graph: " + error);
	});
	return promise;
}

function redrawPixiGraph() {
	drawPixiGraph()
		.then(reload);
}

function reload() {
	var reloadAllowed = localStorage.getItem('reloadPixiAllowed');
	if (reloadAllowed && reloadAllowed == "1") {
		localStorage.setItem('reloadPixiAllowed',"0");
		window.location.reload();
	} else if (!reloadAllowed) {
		localStorage.setItem('reloadPixiAllowed',"0");
		window.location.reload();
	} else if (reloadAllowed && reloadAllowed == "0") {
		localStorage.setItem('reloadPixiAllowed',"1");
	}
	return Promise.resolve();
}