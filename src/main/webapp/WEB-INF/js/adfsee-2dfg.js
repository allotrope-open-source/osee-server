/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
var data;
var treeData;
var loadafo = true;
var loadaft = true;
var dolayout = true;
var loadexample = false;
var multigraph = false;
var skolemize = false;
var includeadf = false;
var tree;
var nodeData;
var twodfg;
var hiddenNodes = [];
var hiddenLinks = [];
var nodes = null;
var links = null;

var nodeColors = {};
var labels = {};
var counter = 0;
var needToRemoveSelfLinks = true;

var colorValues = {
	"INFORMATION": "#FFCC00", 
	'INSTANCE': '#98BDF9', 
	'ALLOTROPE': '#DD6C07', 
	'COMMON': '#FFFFFF',
	'MATERIAL': '#C2E7C2',
	'PROCESS': '#CAA3F1',
	'RESULT': '#FFCC00',
	'EQUIPMENT': '#AAD5FF',
	'PROPERTY': '#FFFFFF',
	'QUALITY': '#FFFF99',
	'CONTEXTUAL_ROLE': '#FFC896',
	'REALIZABLE_ENTITY': '#FF99CC',
	'OBO': '#D5ECF7',
	'BFO': '#236099',
	'IAO': '#FFCC00',
	'OBI': '#449d44',
	'PATO': '#234979',
	'RO': '#FFFFFF',
	'CHMO': '#8EC1C5',
	'ENVO': '#A5FF4B',
	'NCBI': '#326597',
	'CHEBI': '#DAD734',
	'EXAMPLE': '#FFDFF7',
	'OTHER': '#FFFFFF',
	'LITERAL' : "#FFF0B3"
};

initGraphStore();

document.addEventListener('DOMContentLoaded',function(){
	var loading = document.getElementById('loading');
	loading.classList.remove('loaded');
	if (localStorage.getItem('treeStatus') && localStorage.getItem('treeStatus') == freshStartStatus) {
		resetLocalDbAndDataOfTrees()
		.then(draw2dfg);
	} else if (localStorage.getItem('treeStatus') && localStorage.getItem('treeStatus') == treeAvailableStatus) {
		selectAllFromTrees()
		.then(updateTreeStateWithSelectedResults)
		.then(draw2dfg);
	} else {
		localStorage.setItem('treeStatus', freshStartStatus);
		resetLocalDbAndDataOfTrees()
		.then(function() {
			console.log("No state information found in local tree store. Restarting from scratch.");
			return Promise.resolve();
		})
		.then(draw2dfg);
	}
	loading.classList.add('loaded');
});

async function create2dfg() {
	let promise = await new Promise(
		function (resolve, reject) {
			try {
				var nodeData = treeData;
				var data = {
						nodes : [],
						links : []
				};
				counter = 0;

				var g = getNodesAndLinks(nodeData, data, 0);

				twodfg = null;

			    var elem = document.getElementById('2dgraph');
			    twodfg = ForceGraph()(elem)
			    .graphData(g)
				.nodeLabel(node => `${node.name}`)
				.nodeColor(node => `${node.color}`)
				.linkColor("0,0,0")
//				.linkWidth(0.3) // incompatible with curvature
				.linkCurvature('none')
				.onNodeHover(node => elem.style.cursor = node ? 'pointer' : null)
				.onNodeClick(node => {
					twodfg.centerAt(node.x, node.y, 1000);
					twodfg.zoom(8, 2000);
				});			 
				const linkForce = twodfg
					.d3Force('link')
					.distance(link => 20.0);
				
			    //Define GUI
			    const Settings = function() {
			      this.linkDistance = 20;
			      this.curvature = false;
			      this.nodeResolution=8;
			      this.nodeOpacity=1.0;
			      this.showParticles = false;
			      this.particles = 1;
			      this.arrowLength = 1.5;
			      this.arrowPosition = 1;
			      this.changeLinkWidth = false;
			      this.linkWidth = 0.1;
			      this.changeNodeSize = false;
			      this.nodeSize = 4.0;
			      this.particleWidth = 1.0;
			      this.linkOpacity=1.0;
			      this.particleSpeed = 0.01;
			      this.forceEngine = true;
			      this.showLabels = false;
			      this.textHeight = 8.0;
			      this.showDag=false;
			      this.dagLevel = 200;
			      this.linkColor = 'rgb(0,0,0)';
			      this.changeNodeColor = false;
			      this.nodeColor = 'rgb(0,0,0)';
			      this.textColor = 'rgb(0,0,0)';
			      this.changeTextColor = false;
			      this.inverseScale = 1.0;
			      this.clickToHide = false;
			      this.showAll = function() {
					    let { nodes, links } = twodfg.graphData();
					    if (hiddenLinks && hiddenLinks.length > 0) {
					    	for (var i = 0; i < hiddenLinks.length; i++) {
					    		links.push(hiddenLinks[i]);
					    	}
					    }
					    if (hiddenNodes && hiddenNodes.length > 0) {
					    	for (var i = 0; i < hiddenNodes.length; i++) {
					    		nodes.push(hiddenNodes[i]);
					    	}
					    }
					    twodfg.graphData({ nodes, links });
					    hiddenLinks = [];
					    hiddenNodes = [];
			      };
			    };
			    
			    const settings = new Settings();
				
			    const gui = new dat.GUI();
			    const controllerSeventeen = gui.add(settings, 'showLabels');
			    const controllerEighteen = gui.add(settings, 'inverseScale', 0.001, 4.0);
			    const controllerEleven = gui.add(settings, 'changeNodeSize');
			    const controllerTwelve = gui.add(settings, 'nodeSize', 0.0, 100.0);
			    const controllerNine = gui.add(settings, 'changeLinkWidth');
			    const controllerTen = gui.add(settings, 'linkWidth', 0.0, 5.0);
			    const controllerTwo = gui.add(settings, 'curvature');
			    const controllerSeven = gui.add(settings, 'arrowLength', 0.1, 5.0);
			    const controllerEight = gui.add(settings, 'arrowPosition', 0.0, 2.0);
			    const controllerFive = gui.add(settings, 'showParticles');
			    const controllerSix = gui.add(settings, 'particles', 1, 10);
			    const controllerThirteen = gui.add(settings, 'particleWidth', 0.0, 5.0);
			    const controllerFifteen = gui.add(settings, 'particleSpeed', 0.0, 0.2);
			    const controllerSixteen = gui.add(settings, 'forceEngine');
			    const controllerNineteen = gui.add(settings, 'showDag');
			    const controllerTwentyTwo = gui.add(settings,'changeNodeColor');
			    const controllerTwentyFive = gui.add(settings,'changeTextColor');
			    const controllerTwentyThree = gui.addColor(settings,'nodeColor');
			    const controllerTwentyFour = gui.addColor(settings,'textColor');
			    const controllerTwentyOne = gui.addColor(settings,'linkColor');
			    const controllerTwentySix = gui.add(settings,'clickToHide');
			    const controllerTwentySeven = gui.add(settings,'showAll');
			    
			    controllerTwo.onChange(updateCurvature);
			    function updateCurvature() {
			    	if (settings.curvature) {
			    		settings.linkWidth = 0.0;
			    		updateLinkWidth();
			    	}
			    	twodfg.linkCurvature((settings.curvature ? 'curvature' : 'none'));
			    }

			    controllerFive.onChange(updateShowParticles);
			    function updateShowParticles() {
			    	if (settings.showParticles) {
			    		twodfg.linkDirectionalParticles(settings.particles);
			    	} else {
			    		twodfg.linkDirectionalParticles(0);
			    	}
			    }

			    controllerSix.onChange(updateParticles);
			    function updateParticles() {
			    	if (settings.showParticles) {
			    		twodfg.linkDirectionalParticles(settings.particles);
			    	} 
			    }

			    controllerSeven.onChange(updateArrowLength);
			    function updateArrowLength() {
		    		twodfg.linkDirectionalArrowLength(settings.arrowLength) // 3d arrow looks weird
			    }

			    controllerEight.onChange(updateArrowPosition);
			    function updateArrowPosition() {
		    		twodfg.linkDirectionalArrowRelPos(settings.arrowPosition)
			    }

			    controllerNine.onChange(updateChangeLinkWidth);
			    function updateChangeLinkWidth() {
			    	if (settings.changeLinkWidth) {
			    		twodfg.linkWidth(settings.linkWidth);
			    	} else {
			    		twodfg.linkWidth(0.0);
			    	}
			    }

			    controllerTen.onChange(updateLinkWidth);
			    function updateLinkWidth() {
			    	if (settings.changeLinkWidth) {
			    		twodfg.linkWidth(settings.linkWidth);
			    	} 
			    }

			    controllerEleven.onChange(updateChangeNodeSize);
			    function updateChangeNodeSize() {
			    	if (settings.changeNodeSize) {
			    		twodfg.nodeRelSize(settings.nodeSize);
			    	} else {
			    		twodfg.nodeRelSize(4.0);
			    	}
			    }

			    controllerTwelve.onChange(updateNodeSize);
			    function updateNodeSize() {
			    	if (settings.changeNodeSize) {
			    		twodfg.nodeRelSize(settings.nodeSize);
			    	} 
			    }

			    controllerThirteen.onChange(updateParticleWidth);
			    function updateParticleWidth() {
		    		twodfg.linkDirectionalParticleWidth(settings.particleWidth);
			    }

			    controllerFifteen.onChange(updateParticleSpeed);
			    function updateParticleSpeed() {
		    		twodfg.linkDirectionalParticleSpeed(settings.particleSpeed);
			    }

			    controllerSixteen.onChange(updateForceEngine);
			    function updateForceEngine() {
		    		twodfg.forceEngine((settings.forceEngine == true) ? "d3" : "ngraph");
			    }

			    controllerSeventeen.onChange(updateShowLabels);
			    function updateShowLabels() {
			    	if (settings.showLabels) {
			    		let _settings = settings;
			    		twodfg.nodeCanvasObject((node, ctx, globalScale) => {
							var label = node.id;
							if (node.name) {
								label = node.name;
							}
			                const fontSize = 12/_settings.inverseScale;
			                ctx.font = `${fontSize}px Sans-Serif`;
			                const textWidth = ctx.measureText(label).width;
			                const bckgDimensions = [textWidth, fontSize].map(n => n + fontSize * 0.2); // some padding
			                ctx.fillStyle = 'rgba(255, 255, 255, 0.8)';
			                ctx.fillRect(node.x - bckgDimensions[0] / 2, node.y - bckgDimensions[1] / 2, ...bckgDimensions);
			                ctx.textAlign = 'center';
			                ctx.textBaseline = 'middle';
			                ctx.fillStyle = node.color;
							if (_settings && _settings.textColor) {
								ctx.fillStyle = _settings.textColor;
							}
			                ctx.fillText(label, node.x, node.y);
			              })
			              .nodeLabel(node => (`${node.detail}` ? `${node.detail}` : `${node.name}`));
			    	} else {
					    twodfg = ForceGraph()(elem)
					    .graphData(g)
						.nodeLabel(node => `${node.name}`)
						.nodeColor(node => `${node.color}`)
						.linkColor(link => settings.linkColor)
						.linkWidth(settings.linkWidth) // incompatible with curvature
						.linkCurvature((settings.curvature ? 'curvature' : 'none'))
						.onNodeHover(node => elem.style.cursor = node ? 'pointer' : null)
						.onNodeClick(node => {
							if (settings.clickToHide) {
								removeNode(node);
							} else {
								twodfg.centerAt(node.x, node.y, 1000);
								twodfg.zoom(8, 2000);
							}
						});			 
			    	}
			    }

			    controllerEighteen.onChange(updateTextSize);
			    function updateTextSize() {
			    	if (settings.showLabels) {
			    		let _settings = settings;
			    		twodfg.nodeCanvasObject((node, ctx, globalScale) => {
							var label = node.id;
							if (node.name) {
								label = node.name;
							}
			                const fontSize = 12/_settings.inverseScale;
			                ctx.font = `${fontSize}px Sans-Serif`;
			                const textWidth = ctx.measureText(label).width;
			                const bckgDimensions = [textWidth, fontSize].map(n => n + fontSize * 0.2); // some padding
			                ctx.fillStyle = 'rgba(255, 255, 255, 0.8)';
			                ctx.fillRect(node.x - bckgDimensions[0] / 2, node.y - bckgDimensions[1] / 2, ...bckgDimensions);
			                ctx.textAlign = 'center';
			                ctx.textBaseline = 'middle';
			                ctx.fillStyle = node.color;
							if (_settings && _settings.textColor) {
								ctx.fillStyle = _settings.textColor;
							}
			                ctx.fillText(label, node.x, node.y);
			              })
			    	} else {
					    twodfg = ForceGraph()(elem)
					    .graphData(g)
						.nodeLabel(node => `${node.name}`)
						.nodeColor(node => `${node.color}`)
						.linkColor(link => settings.linkColor)
						.linkWidth(settings.linkWidth) // incompatible with curvature
						.linkCurvature((settings.curvature ? 'curvature' : 'none'))
						.onNodeHover(node => elem.style.cursor = node ? 'pointer' : null)
						.onNodeClick(node => {
							if (settings.clickToHide) {
								removeNode(node);
							} else {
								twodfg.centerAt(node.x, node.y, 1000);
								twodfg.zoom(8, 2000);
							}
						});			 
			    	}
			    }

			    controllerTwentyFive.onChange(updateChangeTextColor);
			    function updateChangeTextColor() {
			    	if (settings.showLabels) {
			    		let _settings = settings;
			    		twodfg.nodeCanvasObject((node, ctx, globalScale) => {
							var label = node.id;
							if (node.name) {
								label = node.name;
							}
			                const fontSize = 12/_settings.inverseScale;
			                ctx.font = `${fontSize}px Sans-Serif`;
			                const textWidth = ctx.measureText(label).width;
			                const bckgDimensions = [textWidth, fontSize].map(n => n + fontSize * 0.2); // some padding
			                ctx.fillStyle = 'rgba(255, 255, 255, 0.8)';
			                ctx.fillRect(node.x - bckgDimensions[0] / 2, node.y - bckgDimensions[1] / 2, ...bckgDimensions);
			                ctx.textAlign = 'center';
			                ctx.textBaseline = 'middle';
			                ctx.fillStyle = node.color;
							if (_settings && _settings.textColor) {
								ctx.fillStyle = _settings.textColor;
							}
			                ctx.fillText(label, node.x, node.y);
			              });
			    	} else {
					    twodfg = ForceGraph()(elem)
					    .graphData(g)
						.nodeLabel(node => `${node.name}`)
						.nodeColor(node => `${node.color}`)
						.linkColor(link => settings.linkColor)
						.linkWidth(settings.linkWidth) 
						.linkCurvature((settings.curvature ? 'curvature' : 'none'))
						.onNodeHover(node => elem.style.cursor = node ? 'pointer' : null)
						.onNodeClick(node => {
							if (settings.clickToHide) {
								removeNode(node);
							} else {
								twodfg.centerAt(node.x, node.y, 1000);
								twodfg.zoom(8, 2000);
							}
						});			 
			    	}
			    }

			    const daggui = new dat.GUI();
			    controllerNineteen.onChange(updateShowDag);
			    const controls = { 'DAG Orientation': 'td'};
			    daggui.add(controls, 'DAG Orientation', ['td', 'bu', 'lr', 'rl', 'zout', 'zin', 'radialout', 'radialin', null])
			    	.onChange(orientation => twodfg && twodfg.dagMode(orientation));
			    
			    const controllerTwenty = daggui.add(settings, 'dagLevel', 0, 500);
			    controllerTwenty.onChange(updateDagLevel);
			    function updateDagLevel() {
			    	if (settings.showDag) {
						twodfg.dagLevelDistance(settings.dagLevel);
			    	}			    	
			    }

			    $(daggui.domElement).attr("hidden", true);
			    
			    function updateShowDag() {
			    	if (settings.showDag) {
			    		$(daggui.domElement).attr("hidden", false);
						let _settings = settings;
						twodfg = ForceGraph()
						.dagMode('td')
						.dagLevelDistance(_settings.dagLevel)
						.backgroundColor('#FFFFFF')
						.linkColor(_settings.linkColor)
						.nodeColor(node => `${node.color}`)
						.nodeId('id')
						.nodeVal('size')
						.nodeLabel('name')
						.d3Force('collision', d3.forceCollide(node => Math.sqrt(100 / (node.level + 1)) * _settings.nodeSize))
						.d3VelocityDecay(0.3)
						.onNodeHover(node => elem.style.cursor = node ? 'pointer' : null)
						.onNodeClick(node => {
							if (settings.clickToHide) {
								removeNode(node);
							} else {
								twodfg.centerAt(node.x, node.y, 1000);
								twodfg.zoom(8, 2000);
							}
						});			 
						
						if (needToRemoveSelfLinks) {
							tree = removeSelfLinks(g);
						}
						twodfg(document.getElementById('2dgraph')).graphData(tree);
			    	} else {
			    		$(daggui.domElement).attr("hidden", true);
					    twodfg = ForceGraph()(document.getElementById('2dgraph'))
					    .graphData(g)
						.nodeLabel(node => `${node.name}`)
						.nodeColor(node => `${node.color}`)
						.linkColor(settings.linkColor)
						.linkWidth(settings.linkWidth)
						.linkCurvature((settings.curvature ? 'curvature' : 'none'))
						.onNodeHover(node => elem.style.cursor = node ? 'pointer' : null)
						.onNodeClick(node => {
							if (settings.clickToHide) {
								removeNode(node);
							} else {
								twodfg.centerAt(node.x, node.y, 1000);
								twodfg.zoom(8, 2000);
							}
						});			 
						
						twodfg(document.getElementById('2dgraph')).graphData(g);
			    	}
			    }
			    
			    controllerTwentyOne.onChange(updateLinkColor);
			    function updateLinkColor() {
			    	var colorObj = new THREE.Color( settings.linkColor );
			    	var hex = colorObj.getHexString();
			    	var css = colorObj.getStyle();
			    	var display = "#"+ hex + " or " + css;
			    	$("#colors").append(display+"<br>");
			    	twodfg.linkColor(link => settings.linkColor);
			    };
			    
			    controllerTwentyTwo.onChange(updateChangeNodeColor);
			    function updateChangeNodeColor() {
			    	if (settings.changeNodeColor) {
			    		twodfg.nodeColor(node => settings.nodeColor);
			    	} else {
						twodfg.nodeColor(node => `${node.color}`);
			    	}
			    };
			    
			    controllerTwentyThree.onChange(updateNodeColor);
			    function updateNodeColor() {
			    	if (settings.changeNodeColor) {
				    	var colorObj = new THREE.Color( settings.nodeColor );
				    	var hex = colorObj.getHexString();
				    	var css = colorObj.getStyle();
				    	var display = "#"+ hex + " or " + css;
				    	$("#colors").append(display+"<br>");
				    	twodfg.nodeColor(node => settings.nodeColor);
			    	}
			    };
			    
			    controllerTwentySix.onChange(updateClickToHide);
			    function updateClickToHide() {
			    	if (settings.clickToHide) {
			    		console.log("click a node to hide it.");
						twodfg.onNodeClick(node => removeNode(node));
					} else {
						twodfg.onNodeClick(node => {
								twodfg.centerAt(node.x, node.y, 1000);
								twodfg.zoom(8, 2000);
							});			 
					}
			    };
			    
			    controllerTwentySeven.onChange(updateShowAll);
			    function updateShowAll() {
		    		console.log("showing all nodes.");
			    };

			} catch(error) {
				loading.classList.add('loaded');
				console.log("Error during creation of 3d force graph: " + error);
				reject();
			}
			loading.classList.add('loaded');
			resolve(twodfg);
		}
	)
	.catch(error => { 
		loading.classList.add('loaded');
		console.log("Error during creation of 2d force graph: " + error);
	});
	return promise;
}

function extractRGB(css) {
	var rgb = css.replace(/^(rgb|rgba)\(/,'').replace(/\)$/,'').replace(/\s/g,'');
	return rgb;
}

function draw2dfg() {
	var loading = document.getElementById('loading');
	loading.classList.remove('loaded');

	try {
		create2dfg();
	} catch(error) {
		console.log("Error during creation of 2d force graph: " + error);
		loading.classList.add('loaded');
		return Promise.reject();
	}
	
	return Promise.resolve();
}

function redraw2dfg() {
	draw2dfg()
		.then(reload);
}

function reload() {
	var reloadAllowed = localStorage.getItem('reload2dfgAllowed');
	if (reloadAllowed && reloadAllowed == "1") {
		localStorage.setItem('reload2dfgAllowed',"0");
		window.location.reload();
	} else if (!reloadAllowed) {
		localStorage.setItem('reloadAllowed',"0");
		window.location.reload();
	} else if (reloadAllowed && reloadAllowed == "0") {
		localStorage.setItem('reloadAllowed',"1");
	}
	return Promise.resolve();
}
	
function getNodesAndLinks(nodeData, nodeslinks, parentId) {
	var arrayLength = nodeData.length;
	for (var i = 0; i < arrayLength; i++) {
		var curNode = nodeData[i]
		var label;
		if (curNode.label.indexOf("<") !== -1) {
			label = curNode.label.split('<')[1];
			label = label.split('>')[0];
		}
		else if (curNode.label.indexOf("[") !== -1) {
			label = "[]";
		} else {
			label = curNode.label;
		}
		
		var detail = "";
		if (curNode.detail) {
			detail = curNode.detail;
		}

		var domains = [];
		if (curNode.domains) {
			domains = curNode.domains;
			detail = detail + "</br></br>" + domains;
		}

		var newNodeColor = "#FFFFFF";
		
		if (curNode.domains && curNode.domains[0]) {
			if (colorValues[curNode.domains[0]]) {
				newNodeColor = colorValues[curNode.domains[0]];
			}
		} else if (curNode.data && curNode.data.bg) {
			newNodeColor = curNode.data.bg;
		} else if (curNode.color) {
			newNodeColor = curNode.color;
		}
		
		var newNode = {
			"id" : counter++,
			"name" : label,
			"color" : hexToRgbA(newNodeColor),
			"radius" : 5, 
			"detail" : detail,
			"categories" : domains
		}; 
		
		nodeslinks.nodes.push(newNode);

		nodeColors[newNode.id] = newNode.color;
		labels[newNode.id] = label;

		if (nodeslinks.nodes.length > 0) {
			var label;
			if (curNode.label.indexOf("<") !== -1) {
				label = curNode.label.split('<')[0];
			}
			else if (curNode.label.indexOf("[") !== -1) {
				label = curNode.label.split('[')[0];
			} else {
				label = curNode.label;
			}

			if (label.indexOf("\"") > -1) {
				label = label.substring(0, label.indexOf("\"") - 1);
			}

			var detail = "";
			if (curNode.detail) {
				detail = curNode.detail;
			}

			var domains = [];
			if (curNode.domains) {
				domains = curNode.domains;
			}

			var newLink = {
				"id" : counter++,
				"source" : parentId,
				"target" : newNode.id,
				"text" : label,
				"curvature" : 0.5,
				"rotation" : Math.PI * 1 / 6,
				"detail" : detail,
   				"categories" : domains
			}
			nodeslinks.links.push(newLink);   
			labels[newLink.id] = label; 				
		}
		var children = curNode.children;
		if (children) {
			nodeslinks = getNodesAndLinks(children, nodeslinks, newNode.id);
		}
	}
	return nodeslinks;
}

function hexToRgbA(hex){
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+',1)';
    }
    throw new Error('Bad Hex');
}

function removeSelfLinks(g) {
	tree = {
			nodes : [],
			links : []
	};
	
	var arrayLength = g.links.length;
	var noSelfLinks = [];
	for (var i = 0; i < arrayLength; i++) {
		var source = g.links[i].source;
		var target = g.links[i].target;
		if (source != target) {
			noSelfLinks.push(g.links[i]);
		} else {
			console.log("removing selflink for node id: " + source);
		}
	}
	
	tree.nodes = g.nodes;
	tree.links = noSelfLinks;
	needToRemoveSelfLinks = false;
	return tree;
}

function removeNode(node) {
    let { nodes, links } = twodfg.graphData();
    var linksToHide = links.filter(l => l.source == node || l.target == node);
    links = links.filter(l => l.source !== node && l.target !== node); // Remove links attached to node
	var spliceId;
    for (var i = 0; i < nodes.length; i++) {
		if ( nodes[i].id == node.id ){
			spliceId = i;
			break;
		}
	}
	nodes.splice(spliceId, 1);
    
    hiddenNodes.push(node);
    if (linksToHide && linksToHide.length > 0) {
    	for (var i = 0; i < linksToHide.length; i++) {
    		hiddenLinks.push(linksToHide[i]);
    	}
    }
//    nodes.forEach((n, idx) => { n.id = idx; }); // Reset node ids to array index
    twodfg.graphData({ nodes, links });
}