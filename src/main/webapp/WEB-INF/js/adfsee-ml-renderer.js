var margin = {
  top: 20,
  right: 20,
  bottom: 20,
  left: 20
};

var width = $(window).width() - margin.left - margin.right; 
var height = $(window).height() - margin.top - margin.bottom;
var scale = d3.scaleOrdinal(d3.schemeCategory10);

let svg;
let node;
let link;
let text;

function setup(data) {
  var getColor = d => scale(d.group);

  function zoomed () {
   	let x = d3.event.transform.x + margin.left 
    let y = d3.event.transform.y + margin.top
  	svg.attr("transform", "translate(" + x + "," + y + ") scale(1)");
  }

  var zoom = d3.zoom()
    .extent([[margin.left, margin.top], [width, height]])
    .scaleExtent([1, 1])
    .on("zoom", zoomed);

  svg = d3
    .select("body")
    .append("svg")
    .attr("width", width)
    .attr("height", height);
  
  link = svg
	.selectAll("line")
    .data(data.links)
    .enter().append("line")
    .attr("class", "link")
    .attr("stroke", "#999")
    .attr("stroke-opacity", 0.6)
    .style('pointer-events', 'all')
    .attr("stroke-width", 1);

  node = svg
    .selectAll("circle")
    .data(data.nodes)
    .enter().append("circle")
    .attr("class", "node")
    .attr("stroke", "#fff")
    .attr("stroke-width", 1.5)
    .style('pointer-events', 'all')
    .attr("r", 5)
    .attr("fill", getColor);
 
  text = svg.selectAll("text")
	.data(data.nodes)
	.enter()
	.append("text")
	.attr("x", function(d) { return d.x; })
	.attr("y", function(d) { return d.y; })
	.text( function (d) { return d.label })
	.attr("font-family", "sans-serif")
	.attr("font-size", "10px")
	.attr("fill", "black")
	.style("display", "none");
};

function draw(data) {
  node.attr("transform", function(d) {
    var dx = d.x + width / 2;
    var dy = d.y + height / 2;
    return "translate(" + dx + "," + dy + ")";
  });

  if (showlabels) {
	  text.attr("transform", function(d) {
		    var dx = d.x + width / 2;
		    var dy = d.y + height / 2;
		    return "translate(" + dx + "," + dy + ")";
		  });
  }

  function getNodePosition(nodeId) {
    var node = data.nodes.find(n => n.id === nodeId);
    return {
      x: node.x + width / 2,
      y: node.y + height / 2
    };
  };

  link
    .attr("x1", d => getNodePosition(d.source).x)
    .attr("y1", d => getNodePosition(d.source).y)
    .attr("x2", d => getNodePosition(d.target).x)
    .attr("y2", d => getNodePosition(d.target).y);
};
