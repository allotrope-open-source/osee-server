/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
var data;
var treeData;
var loadafo = true;
var loadaft = true;
var dolayout = true;
var loadexample = false;
var multigraph = false;
var skolemize = false;
var includeadf = false;
var tree;
var nodeData;
var threedfg;

var nodes = null;
var links = null;

var nodeColors = {};
var labels = {};
var counter = 0;
var needToRemoveSelfLinks = true;

var colorValues = {
	"INFORMATION": "#FFCC00", 
	'INSTANCE': '#98BDF9', 
	'ALLOTROPE': '#DD6C07', 
	'COMMON': '#FFFFFF',
	'MATERIAL': '#C2E7C2',
	'PROCESS': '#CAA3F1',
	'RESULT': '#FFCC00',
	'EQUIPMENT': '#AAD5FF',
	'PROPERTY': '#FFFFFF',
	'QUALITY': '#FFFF99',
	'CONTEXTUAL_ROLE': '#FFC896',
	'REALIZABLE_ENTITY': '#FF99CC',
	'OBO': '#D5ECF7',
	'BFO': '#236099',
	'IAO': '#FFCC00',
	'OBI': '#449d44',
	'PATO': '#234979',
	'RO': '#FFFFFF',
	'CHMO': '#8EC1C5',
	'ENVO': '#A5FF4B',
	'NCBI': '#326597',
	'CHEBI': '#DAD734',
	'EXAMPLE': '#FFDFF7',
	'OTHER': '#FFFFFF',
	'LITERAL' : "#FFF0B3"
};

initGraphStore();

document.addEventListener('DOMContentLoaded',function(){
	var loading = document.getElementById('loading');
	loading.classList.remove('loaded');
	if (localStorage.getItem('treeStatus') && localStorage.getItem('treeStatus') == freshStartStatus) {
		resetLocalDbAndDataOfTrees()
		.then(draw3dfg);
	} else if (localStorage.getItem('treeStatus') && localStorage.getItem('treeStatus') == treeAvailableStatus) {
		selectAllFromTrees()
		.then(updateTreeStateWithSelectedResults)
		.then(draw3dfg);
	} else {
		localStorage.setItem('treeStatus', freshStartStatus);
		resetLocalDbAndDataOfTrees()
		.then(function() {
			console.log("No state information found in local tree store. Restarting from scratch.");
			return Promise.resolve();
		})
		.then(draw3dfg);
	}
	loading.classList.add('loaded');
});

async function create3dfg() {
	let promise = await new Promise(
		function (resolve, reject) {
			try {
				var nodeData = treeData;
				var data = {
						nodes : [],
						links : []
				};
				counter = 0;

				var g = getNodesAndLinks(nodeData, data, 0);

				threedfg = null;

			    //Define GUI
			    const Settings = function() {
			      this.linkDistance = 20;
			      this.curvature = false;
			      this.nodeResolution=8;
			      this.nodeOpacity=1.0;
			      this.showParticles = false;
			      this.particles = 1;
			      this.arrowLength = 1.5;
			      this.arrowPosition = 1;
			      this.changeLinkWidth = false;
			      this.linkWidth = 0.1;
			      this.changeNodeSize = false;
			      this.nodeSize = 4.0;
			      this.particleWidth = 1.0;
			      this.linkOpacity=1.0;
			      this.particleSpeed = 0.01;
			      this.forceEngine = true;
			      this.showLabels = false;
			      this.textHeight = 8.0;
			      this.showDag=false;
			      this.dagLevel = 200;
			      this.linkColor = 'rgb(255,255,255)';
			      this.changeNodeColor = false;
			      this.nodeColor = 'rgb(255,255,255)';
			      this.textColor = 'rgb(255,255,255)';
			      this.changeTextColor = false;
			    };
			    
			    const settings = new Settings();
				
			    var elem = document.getElementById('3dgraph');
//			    threedfg = ForceGraph3D({ controlType: 'fly' })(elem) // flying camera control
			    threedfg = ForceGraph3D()(elem)
			    .graphData(g)
				.nodeLabel(node => `${node.name}`)
				.nodeColor(
						node => `${node.color}`
				 )
				.nodeResolution(16)
				.nodeOpacity(1.0)
				.linkColor("255,255,255")
//				.linkWidth(0.3) // incompatible with curvature
				.linkOpacity(1.0)
				.linkCurvature((settings.curvature ? 'curvature' : 'none'))
				.linkCurveRotation('rotation')
				.enableNavigationControls(true)
				.showNavInfo(true)
				.onNodeHover(node => elem.style.cursor = node ? 'pointer' : null)
				.onNodeClick(node => {
					const distance = 40;
					const distRatio = 1 + distance/Math.hypot(node.x, node.y, node.z);
					threedfg.cameraPosition({ x: node.x * distRatio, y: node.y * distRatio, z: node.z * distRatio }, node, 3000);
				});
			 
				const linkForce = threedfg
					.d3Force('link')
					.distance(link => settings.linkDistance);
  
			    const gui = new dat.GUI();
			    const controllerSeventeen = gui.add(settings, 'showLabels');
			    const controllerEighteen = gui.add(settings, 'textHeight', 0.0, 100.0);
			    const controllerEleven = gui.add(settings, 'changeNodeSize');
			    const controllerTwelve = gui.add(settings, 'nodeSize', 0.0, 100.0);
			    const controllerThree = gui.add(settings, 'nodeResolution', 4, 64);
			    const controllerFour = gui.add(settings, 'nodeOpacity', 0.0, 1.0);
			    const controllerOne = gui.add(settings, 'linkDistance', 0, 100);
			    const controllerNine = gui.add(settings, 'changeLinkWidth');
			    const controllerTen = gui.add(settings, 'linkWidth', 0.0, 5.0);
			    const controllerFourteen = gui.add(settings, 'linkOpacity', 0.0, 1.0);
			    const controllerTwo = gui.add(settings, 'curvature');
			    const controllerSeven = gui.add(settings, 'arrowLength', 0.1, 5.0);
			    const controllerEight = gui.add(settings, 'arrowPosition', 0.0, 2.0);
			    const controllerFive = gui.add(settings, 'showParticles');
			    const controllerSix = gui.add(settings, 'particles', 1, 10);
			    const controllerThirteen = gui.add(settings, 'particleWidth', 0.0, 5.0);
			    const controllerFifteen = gui.add(settings, 'particleSpeed', 0.0, 0.2);
			    const controllerSixteen = gui.add(settings, 'forceEngine');
			    const controllerNineteen = gui.add(settings, 'showDag');
			    const controllerTwentyTwo = gui.add(settings,'changeNodeColor');
			    const controllerTwentyFive = gui.add(settings,'changeTextColor');
			    const controllerTwentyThree = gui.addColor(settings,'nodeColor');
			    const controllerTwentyFour = gui.addColor(settings,'textColor');
			    const controllerTwentyOne = gui.addColor(settings,'linkColor');
			    
			    controllerOne.onChange(updateLinkDistance);
			    function updateLinkDistance() {
			    	linkForce.distance(link => settings.linkDistance);
			    	threedfg.numDimensions(3); // Re-heat simulation
			    }

			    controllerTwo.onChange(updateCurvature);
			    function updateCurvature() {
			    	if (settings.curvature) {
			    		settings.linkWidth = 0.0;
			    		updateLinkWidth();
			    	}
			    	threedfg.linkCurvature((settings.curvature ? 'curvature' : 'none'));
			    	threedfg.numDimensions(3); // Re-heat simulation
			    }

			    controllerThree.onChange(updateNodeResolution);
			    function updateNodeResolution() {
			    	threedfg.nodeResolution(settings.nodeResolution);
			    	threedfg.numDimensions(3); // Re-heat simulation
			    }

			    controllerFour.onChange(updateNodeOpacity);
			    function updateNodeOpacity() {
			    	threedfg.nodeOpacity(settings.nodeOpacity);
			    	threedfg.numDimensions(3); // Re-heat simulation
			    }

			    controllerFive.onChange(updateShowParticles);
			    function updateShowParticles() {
			    	if (settings.showParticles) {
			    		threedfg.linkDirectionalParticles(settings.particles);
			    	} else {
			    		threedfg.linkDirectionalParticles(0);
			    	}
			    	threedfg.numDimensions(3); // Re-heat simulation
			    }

			    controllerSix.onChange(updateParticles);
			    function updateParticles() {
			    	if (settings.showParticles) {
			    		threedfg.linkDirectionalParticles(settings.particles);
			    	} 
			    	threedfg.numDimensions(3); // Re-heat simulation
			    }

			    controllerSeven.onChange(updateArrowLength);
			    function updateArrowLength() {
		    		threedfg.linkDirectionalArrowLength(settings.arrowLength) // 3d arrow looks weird
			    	threedfg.numDimensions(3); // Re-heat simulation
			    }

			    controllerEight.onChange(updateArrowPosition);
			    function updateArrowPosition() {
		    		threedfg.linkDirectionalArrowRelPos(settings.arrowPosition)
			    	threedfg.numDimensions(3); // Re-heat simulation
			    }

			    controllerNine.onChange(updateChangeLinkWidth);
			    function updateChangeLinkWidth() {
			    	if (settings.changeLinkWidth) {
			    		threedfg.linkWidth(settings.linkWidth);
			    	} else {
			    		threedfg.linkWidth(0.0);
			    	}
			    	threedfg.numDimensions(3); // Re-heat simulation
			    }

			    controllerTen.onChange(updateLinkWidth);
			    function updateLinkWidth() {
			    	if (settings.changeLinkWidth) {
			    		threedfg.linkWidth(settings.linkWidth);
			    	} 
			    	threedfg.numDimensions(3); // Re-heat simulation
			    }

			    controllerEleven.onChange(updateChangeNodeSize);
			    function updateChangeNodeSize() {
			    	if (settings.changeNodeSize) {
			    		threedfg.nodeRelSize(settings.nodeSize);
			    	} else {
			    		threedfg.nodeRelSize(4.0);
			    	}
			    	threedfg.numDimensions(3); // Re-heat simulation
			    }

			    controllerTwelve.onChange(updateNodeSize);
			    function updateNodeSize() {
			    	if (settings.changeNodeSize) {
			    		threedfg.nodeRelSize(settings.nodeSize);
			    	} 
			    	threedfg.numDimensions(3); // Re-heat simulation
			    }

			    controllerThirteen.onChange(updateParticleWidth);
			    function updateParticleWidth() {
		    		threedfg.linkDirectionalParticleWidth(settings.particleWidth);
			    	threedfg.numDimensions(3); // Re-heat simulation
			    }

			    controllerFourteen.onChange(updateLinkOpacity);
			    function updateLinkOpacity() {
		    		threedfg.linkOpacity(settings.linkOpacity);
			    	threedfg.numDimensions(3); // Re-heat simulation
			    }

			    controllerFifteen.onChange(updateParticleSpeed);
			    function updateParticleSpeed() {
		    		threedfg.linkDirectionalParticleSpeed(settings.particleSpeed);
			    	threedfg.numDimensions(3); // Re-heat simulation
			    }

			    controllerSixteen.onChange(updateForceEngine);
			    function updateForceEngine() {
		    		threedfg.forceEngine((settings.forceEngine == true) ? "d3" : "ngraph");
			    	threedfg.numDimensions(3); // Re-heat simulation
			    }

			    controllerSeventeen.onChange(updateShowLabels);
			    function updateShowLabels() {
			    	if (settings.showLabels) {
			    		let _settings = settings;
						threedfg.nodeThreeObject((node) => {
							const obj = new THREE.Mesh(
								new THREE.SphereGeometry(10),
								new THREE.MeshBasicMaterial({ depthWrite: false, transparent: true, opacity: 0 })
							);
							// add text sprite as child
							var textString = node.id;
							if (node.name) {
								textString = node.name;
							}
							const sprite = new SpriteText(textString);
							sprite.color = node.color;
							if (_settings && _settings.textHeight) {
								sprite.textHeight = _settings.textHeight;
							} else {
								sprite.textHeight = 8;
							}
							obj.add(sprite);
							return obj;
						});
						// Spread nodes a little wider
						threedfg.d3Force('charge').strength(-150);
			    	} else {
						threedfg.nodeThreeObject(node => {
							const obj = new THREE.Mesh(
								new THREE.SphereGeometry(10),
								new THREE.MeshBasicMaterial({ depthWrite: false, transparent: false, opacity: 1.0 })
							);
							threedfg.d3Force('charge').strength(-100);
						});
						threedfg.numDimensions(3); // Re-heat simulation
			    	}
			    }

			    controllerEighteen.onChange(updateTextHeight);
			    function updateTextHeight() {
			    	if (settings.showLabels) {
			    		let _settings = settings;
						threedfg.nodeThreeObject((node) => {
							const obj = new THREE.Mesh(
								new THREE.SphereGeometry(10),
								new THREE.MeshBasicMaterial({ depthWrite: false, transparent: true, opacity: 0 })
							);
							// add text sprite as child
							var textString = node.id;
							if (node.name) {
								textString = node.name;
							}
							const sprite = new SpriteText(textString);
							sprite.color = node.color;
							if (_settings && _settings.textHeight) {
								sprite.textHeight = _settings.textHeight;
							} else {
								sprite.textHeight = 8;
							}
							obj.add(sprite);
							return obj;
						});
						// Spread nodes a little wider
						threedfg.d3Force('charge').strength(-150);
			    	}
			    }

			    controllerTwentyFive.onChange(updateChangeTextColor);
			    function updateChangeTextColor() {
			    	if (settings.showLabels && settings.changeTextColor) {
			    		updateTextColor();
			    	} else {
						updateShowLabels();
			    	}
			    };

			    controllerTwentyFour.onChange(updateTextColor);
			    function updateTextColor() {
			    	if (settings.showLabels && settings.changeTextColor) {
			    		let _settings = settings;
			    		var colorObj = new THREE.Color( _settings.textColor );
			    		var hex = colorObj.getHexString();
			    		var css = colorObj.getStyle();
			    		var display = "#"+ hex + " or " + css;
			    		$("#colors").append(display+"<br>");
						threedfg.nodeThreeObject((node) => {
							const obj = new THREE.Mesh(
								new THREE.SphereGeometry(10),
								new THREE.MeshBasicMaterial({ depthWrite: false, transparent: true, opacity: 0 })
							);
							// add text sprite as child
							var textString = node.id;
							if (node.name) {
								textString = node.name;
							}
							const sprite = new SpriteText(textString);
							sprite.color = node.color;
							if (_settings && _settings.textColor) {
								sprite.color = _settings.textColor;
							}
							if (_settings && _settings.textHeight) {
								sprite.textHeight = _settings.textHeight;
							} else {
								sprite.textHeight = 8;
							}
							obj.add(sprite);
							return obj;
						});
						// Spread nodes a little wider
						threedfg.d3Force('charge').strength(-150);
			    	}
			    }

			    const daggui = new dat.GUI();
			    controllerNineteen.onChange(updateShowDag);
			    const controls = { 'DAG Orientation': 'td'};
			    daggui.add(controls, 'DAG Orientation', ['td', 'bu', 'lr', 'rl', 'zout', 'zin', 'radialout', 'radialin', null])
			    	.onChange(orientation => threedfg && threedfg.dagMode(orientation));
			    
			    const controllerTwenty = daggui.add(settings, 'dagLevel', 0, 500);
			    controllerTwenty.onChange(updateDagLevel);
			    function updateDagLevel() {
			    	if (settings.showDag) {
						threedfg.dagLevelDistance(settings.dagLevel);
			    	}			    	
			    }

			    $(daggui.domElement).attr("hidden", true);
			    
			    function updateShowDag() {
			    	if (settings.showDag) {
			    		$(daggui.domElement).attr("hidden", false);
						let _settings = settings;
						threedfg = ForceGraph3D()
						.dagMode('td')
						.dagLevelDistance(_settings.dagLevel)
						.backgroundColor('#101020')
						.linkColor(_settings.linkColor)
						.nodeColor(node => `${node.color}`)
						.nodeRelSize(_settings.nodeSize)
						.nodeId('id')
						.nodeVal('size')
						.nodeLabel('name')
						.nodeOpacity(_settings.nodeOpacity)
						.d3Force('collision', d3.forceCollide(node => Math.cbrt(node.size) * _settings.nodeSize))
						.d3VelocityDecay(0.3)
						.onNodeHover(node => elem.style.cursor = node ? 'pointer' : null)
						.onNodeClick(node => {
							const distance = 40;
							const distRatio = 1 + distance/Math.hypot(node.x, node.y, node.z);
							threedfg.cameraPosition({ x: node.x * distRatio, y: node.y * distRatio, z: node.z * distRatio }, node, 3000);
						});
						
						if (needToRemoveSelfLinks) {
							tree = removeSelfLinks(g);
						}
						threedfg(document.getElementById('3dgraph')).graphData(tree);
			    	} else {
			    		$(daggui.domElement).attr("hidden", true);
					    threedfg = ForceGraph3D()(document.getElementById('3dgraph'))
					    .graphData(g)
						.nodeLabel(node => `${node.name}`)
						.nodeColor(node => `${node.color}`)
						.nodeResolution(16)
						.nodeOpacity(1.0)
						.linkColor("255,255,255")
//						.linkWidth(0.3) // incompatible with curvature
						.linkOpacity(1.0)
						.linkCurvature((settings.curvature ? 'curvature' : 'none'))
						.linkCurveRotation('rotation')
						.enableNavigationControls(true)
						.showNavInfo(true)
						.onNodeHover(node => elem.style.cursor = node ? 'pointer' : null)
						.onNodeClick(node => {
							const distance = 40;
							const distRatio = 1 + distance/Math.hypot(node.x, node.y, node.z);
							threedfg.cameraPosition({ x: node.x * distRatio, y: node.y * distRatio, z: node.z * distRatio }, node, 3000);
						});
						
						threedfg(document.getElementById('3dgraph')).graphData(g);
			    	}
			    }
			    
			    controllerTwentyOne.onChange(updateLinkColor);
			    function updateLinkColor() {
			    	var colorObj = new THREE.Color( settings.linkColor );
			    	var hex = colorObj.getHexString();
			    	var css = colorObj.getStyle();
			    	var display = "#"+ hex + " or " + css;
			    	$("#colors").append(display+"<br>");
			    	threedfg.linkColor(link => settings.linkColor);
			    	threedfg.numDimensions(3); // Re-heat simulation
			    };
			    
			    controllerTwentyTwo.onChange(updateChangeNodeColor);
			    function updateChangeNodeColor() {
			    	if (settings.changeNodeColor) {
			    		threedfg.nodeColor(node => settings.nodeColor);
			    	} else {
						threedfg.nodeColor(node => `${node.color}`);
			    	}
			    	threedfg.numDimensions(3); // Re-heat simulation
			    };
			    
			    controllerTwentyThree.onChange(updateNodeColor);
			    function updateNodeColor() {
			    	if (settings.changeNodeColor) {
				    	var colorObj = new THREE.Color( settings.nodeColor );
				    	var hex = colorObj.getHexString();
				    	var css = colorObj.getStyle();
				    	var display = "#"+ hex + " or " + css;
				    	$("#colors").append(display+"<br>");
				    	threedfg.nodeColor(node => settings.nodeColor);
				    	threedfg.numDimensions(3); // Re-heat simulation
			    	}
			    };

			} catch(error) {
				loading.classList.add('loaded');
				console.log("Error during creation of 3d force graph: " + error);
				reject();
			}
			loading.classList.add('loaded');
			resolve(threedfg);
		}
	)
	.catch(error => { 
		loading.classList.add('loaded');
		console.log("Error during creation of 3d force graph: " + error);
	});
	return promise;
}

function extractRGB(css) {
	var rgb = css.replace(/^(rgb|rgba)\(/,'').replace(/\)$/,'').replace(/\s/g,'');
	return rgb;
}

function draw3dfg() {
	var loading = document.getElementById('loading');
	loading.classList.remove('loaded');

	try {
		create3dfg();
	} catch(error) {
		console.log("Error during creation of 3d force graph: " + error);
		loading.classList.add('loaded');
		return Promise.reject();
	}
	
	return Promise.resolve();
}

function redraw3dfg() {
	draw3dfg()
		.then(reload);
}

function reload() {
	var reloadAllowed = localStorage.getItem('reload3dfgAllowed');
	if (reloadAllowed && reloadAllowed == "1") {
		localStorage.setItem('reload3dfgAllowed',"0");
		window.location.reload();
	} else if (!reloadAllowed) {
		localStorage.setItem('reloadAllowed',"0");
		window.location.reload();
	} else if (reloadAllowed && reloadAllowed == "0") {
		localStorage.setItem('reloadAllowed',"1");
	}
	return Promise.resolve();
}
	
function getNodesAndLinks(nodeData, nodeslinks, parentId) {
	var arrayLength = nodeData.length;
	for (var i = 0; i < arrayLength; i++) {
		var curNode = nodeData[i]
		var label;
		if (curNode.label.indexOf("<") !== -1) {
			label = curNode.label.split('<')[1];
			label = label.split('>')[0];
		}
		else if (curNode.label.indexOf("[") !== -1) {
			label = "[]";
		} else {
			label = curNode.label;
		}
		
		var detail = "";
		if (curNode.detail) {
			detail = curNode.detail;
		}

		var domains = [];
		if (curNode.domains) {
			domains = curNode.domains;
			detail = detail + "</br></br>" + domains;
		}

		var newNodeColor = "#FFFFFF";
		
		if (curNode.domains && curNode.domains[0]) {
			if (colorValues[curNode.domains[0]]) {
				newNodeColor = colorValues[curNode.domains[0]];
			}
		} else if (curNode.data && curNode.data.bg) {
			newNodeColor = curNode.data.bg;
		} else if (curNode.color) {
			newNodeColor = curNode.color;
		}
		
		var newNode = {
			"id" : counter++,
			"name" : label,
			"color" : hexToRgbA(newNodeColor),
			"radius" : 5, 
			"detail" : detail,
			"categories" : domains
		}; 
		
		nodeslinks.nodes.push(newNode);

		nodeColors[newNode.id] = newNode.color;
		labels[newNode.id] = label;

		if (nodeslinks.nodes.length > 0) {
			var label;
			if (curNode.label.indexOf("<") !== -1) {
				label = curNode.label.split('<')[0];
			}
			else if (curNode.label.indexOf("[") !== -1) {
				label = curNode.label.split('[')[0];
			} else {
				label = curNode.label;
			}

			if (label.indexOf("\"") > -1) {
				label = label.substring(0, label.indexOf("\"") - 1);
			}

			var detail = "";
			if (curNode.detail) {
				detail = curNode.detail;
			}

			var domains = [];
			if (curNode.domains) {
				domains = curNode.domains;
			}

			var newLink = {
				"id" : counter++,
				"source" : parentId,
				"target" : newNode.id,
				"text" : label,
				"curvature" : 0.5,
				"rotation" : Math.PI * 1 / 6,
				"detail" : detail,
   				"categories" : domains
			}
			nodeslinks.links.push(newLink);   
			labels[newLink.id] = label; 				
		}
		var children = curNode.children;
		if (children) {
			nodeslinks = getNodesAndLinks(children, nodeslinks, newNode.id);
		}
	}
	return nodeslinks;
}

function hexToRgbA(hex){
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+',1)';
    }
    throw new Error('Bad Hex');
}

function removeSelfLinks(g) {
	tree = {
			nodes : [],
			links : []
	};
	
	var arrayLength = g.links.length;
	var noSelfLinks = [];
	for (var i = 0; i < arrayLength; i++) {
		var source = g.links[i].source;
		var target = g.links[i].target;
		if (source != target) {
			noSelfLinks.push(g.links[i]);
		} else {
			console.log("removing selflink for node id: " + source);
		}
	}
	
	tree.nodes = g.nodes;
	tree.links = noSelfLinks;
	needToRemoveSelfLinks = false;
	return tree;
}
