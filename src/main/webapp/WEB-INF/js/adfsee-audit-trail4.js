/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
var data;
var treeData;
var audittrailData;
var audittrail;
var tabledata;
var loadafo = true;
var loadaft = true;
var dolayout = true;
var loadexample = false;
var multigraph = false;
var skolemize = false;
var includeadf = false;
var columnWidths;
var columnHeaders;

initGraphStore();

document.addEventListener('DOMContentLoaded', function() {
		var loading = document.getElementById('loading');
		loading.classList.remove('loaded');

		if (localStorage.getItem('audittrailStatus') && localStorage.getItem('audittrailStatus') == freshStartStatus) {
			resetLocalDbAndDataOfAudittrails().then(createTreeMap);
		} else if (localStorage.getItem('audittrailStatus') && localStorage.getItem('audittrailStatus') == audittrailAvailableStatus) {
			selectAllFromAudittrails()
				.then(updateAudittrailStateWithSelectedResults)
				.then(createTreeMap);
		} else {
			localStorage.setItem('audittrailStatus', freshStartStatus);
			resetLocalDbAndDataOfAudittrails()
				.then(function() {
					console	.log("No state information found in local audittrail store. Restarting from scratch.");
					return Promise.resolve();
				})
				.then(createTreeMap);
		}

		loading.classList.add('loaded');
});

async function createTreeMap() {
	var data = {};
	data.name = "data";
	data.children = await createJsonTreeMapData(audittrail);
	
	try {
		var svg = d3.select("svg"),
		width = +svg.attr("width"),
		height = +svg.attr("height"),
	    radius = Math.min(width, height) / 2,
	    color = d3.scale.category20c();
		
		svg = d3.select("svg")
			.attr("width", width)
			.attr("height", height)
			.append("g")
			.attr("transform", "translate(" + width / 2 + "," + height * .52 + ")");
		
		var partition = d3.layout.partition()
		    .sort(null)
		    .size([2 * Math.PI, radius * radius])
		    .value(function(d) { return 1; });
		
		var arc = d3.svg.arc()
		    .startAngle(function(d) { return d.x; })
		    .endAngle(function(d) { return d.x + d.dx; })
		    .innerRadius(function(d) { return Math.sqrt(d.y); })
		    .outerRadius(function(d) { return Math.sqrt(d.y + d.dy); });
		
		var path = svg.datum(data.children).selectAll("path")
		  	.data(partition.nodes)
		  	.enter().append("path")
		  	.attr("display", function(d) { return d.depth ? null : "none"; }) // hide inner ring
		  	.attr("d", arc)
		  	.style("stroke", "#fff")
		  	.style("fill", function(d) { return color((d.children ? d : d.parent).name); })
		  	.style("fill-rule", "evenodd")
		  	.text(function(d) { return d.name; })
		  	.each(stash)
		  	.on("mouseover", mouseover)
		    .on("mouseout", mouseout);
			
		function mouseover(d) {
			var label = d.name;
		  d3.select(this) 
		  svg.append("text")
			.attr("id", "tooltip")
			.attr("x", 10)
			.attr("y", 50)
			.attr("text-anchor", "middle")
			.attr("font-family", "sans-serif")
			.attr("font-size", "11px")
			.attr("font-weight", "bold")
			.attr("fill", "black")
			.text(label);
		};
		 
		function mouseout(d) {
			  d3.select(this) 
			  svg.selectAll("text").remove();
		};
					
		d3.selectAll("input")
		  	.on("change", function change() {
				var value = this.value === "count"
				    ? function() { return 1; }
				    : function(d) { return d.size; };
				
				path.data(partition.value(value).nodes)
				  	.transition()
				    .duration(1500)
				    .attrTween("d", arcTween);
			  });
		
		// Stash the old values for transition.
		function stash(d) {
			d.x0 = d.x;
			d.dx0 = d.dx;
		}
		
		// Interpolate the arcs in data space.
		function arcTween(a) {
		  var i = d3.interpolate({x: a.x0, dx: a.dx0}, a);
		  return function(t) {
		    var b = i(t);
		    a.x0 = b.x;
		    a.dx0 = b.dx;
		    return arc(b);
		  };
		}
		
		d3.select(self.frameElement).style("height", height + "px");
	} catch(error) {
		console.log("Error during creation of treemap: " + error);
		loading.classList.add('loaded');
		return Promise.reject();
	}
	return Promise.resolve();
}

async function createJsonTreeMapData(jsonData) {
	loading.classList.remove('loaded');
	var tmptree = {};
	
	try {
		if (!jsonData) {
			tmptree = initTreeMapData;
		} else {
			tmptree = audittrail;
		}
		if (localStorage.getItem('audittrailStatus') && localStorage.getItem('audittrailStatus') == audittrailAvailableStatus) {
			tmptree = await anyJson2NormalizedView(tmptree, null);
		}

		tmptree = await anyJson2NormalizedView(tmptree, null);
	} catch(error) {
		console.log("Error during transformation from audittrail to treemap: " + error);
		loading.classList.add('loaded');
		return Promise.reject();
	}
	loading.classList.add('loaded');
	return Promise.resolve(tmptree);
}