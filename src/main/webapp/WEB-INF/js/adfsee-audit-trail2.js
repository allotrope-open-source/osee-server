/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
var data;
var treeData;
var audittrailData;
var audittrail;
var tabledata;
var loadafo = true;
var loadaft = true;
var dolayout = true;
var loadexample = false;
var multigraph = false;
var skolemize = false;
var includeadf = false;
var columnWidths;
var columnHeaders;

initGraphStore();

document.addEventListener('DOMContentLoaded', function() {
		var loading = document.getElementById('loading');
		loading.classList.remove('loaded');

		if (localStorage.getItem('audittrailStatus') && localStorage.getItem('audittrailStatus') == freshStartStatus) {
			resetLocalDbAndDataOfAudittrails().then(doParse2);
		} else if (localStorage.getItem('audittrailStatus') && localStorage.getItem('audittrailStatus') == audittrailAvailableStatus) {
			selectAllFromAudittrails()
				.then(updateAudittrailStateWithSelectedResults)
				.then(doParse2);
		} else {
			localStorage.setItem('audittrailStatus', freshStartStatus);
			resetLocalDbAndDataOfAudittrails()
				.then(function() {
					console	.log("No state information found in local audittrail store. Restarting from scratch.");
					return Promise.resolve();
				})
				.then(doParse2);
		}

		loading.classList.add('loaded');
});