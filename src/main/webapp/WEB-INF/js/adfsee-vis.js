/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
var data;
var treeData;
var loadafo = true;
var loadaft = true;
var dolayout = true;
var multigraph = false;
var skolemize = false;
var loadexample = false;
var includeadf = false;
var tree;
var nodes = null;
var edges = null;
var network = null;
var layoutMethod = "directed";
var directionInput = "UD";
var nodeData;
var visData = {
	nodes : [],
	edges : []
};

var options = {
    layout: {
    	randomSeed: undefined,
		improvedLayout:false,
        hierarchical: {
			enabled:true,
			levelSeparation: 200,
			nodeSpacing: 200,
			treeSpacing: 200,
			blockShifting: true,
			edgeMinimization: true,
			parentCentralization: true,
            direction: directionInput,
            sortMethod: layoutMethod
        }
    },
    edges: {
    	smooth: {
    		enabled: true,
			type: 'cubicBezier',
            roundness: 0.4,
            forceDirection: (directionInput.value == "UD" || directionInput.value == "DU") ? 'vertical' : 'horizontal',
		},
		arrows: {
			to : {
				enabled : true ,
				scaleFactor : 1
			}
		},
		font : {
			size : 14, 
			color: "#000000",
			strokeWidth: 2,
			strokeColor: "#ffffff"
		},
        value : 1,
        widthConstraint : 300,
        length: 10,
        scaling : {
        	min : 1,
        	max : 10,
        	label : {
        		enabled : true
        	},
        	customScalingFunction : function (min,max,total,value) {
				if (max === min) {
					return 0.5;
				}
				else {
					var scale = 1 / (max - min);
					return Math.max(0,(value - min)*scale);
				}
			}
        },
        selfReferenceSize:20,
		shadow:{
			enabled: false,
			color: 'rgba(0,0,0,0.5)',
			size:10,
			x:5,
			y:5
		},
		color : {
			inherit: false,
			color: "#848484"
		}
	},
	nodes: {
		borderWidth : 1,
		font : {
			size : 24
		},
        shape : 'box',
        size : 25,
        value : 1,
        widthConstraint : 150,
       	mass : 1,
        scaling : {
        	min : 1,
        	max : 500,
        	label : {
        		enabled : false,
        		min: 4,
				max: 500,
				maxVisible: 500,
				drawThreshold: 5
        	},
        	customScalingFunction : function (min,max,total,value) {
				if (max === min) {
					return 0.5;
				}
				else {
					var scale = 1 / (max - min);
					return Math.max(0,(value - min)*scale);
				}
			}
        }
	},
	physics:{
		enabled: false,
		barnesHut: {
			gravitationalConstant: -2000,
			centralGravity: 0.3,
			springLength: 95,
			springConstant: 0.04,
			damping: 0.09,
			avoidOverlap: 0
		},
		forceAtlas2Based: {
			gravitationalConstant: -50,
			centralGravity: 0.01,
			springConstant: 0.08,
			springLength: 100,
			damping: 0.4,
			avoidOverlap: 0
		},
		repulsion: {
			centralGravity: 0.2,
			springLength: 200,
			springConstant: 0.05,
			nodeDistance: 100,
			damping: 0.09
		},
		hierarchicalRepulsion: {
			centralGravity: 0.0,
			springLength: 100,
			springConstant: 0.01,
			nodeDistance: 120,
			damping: 0.09
		},
		maxVelocity: 50,
		minVelocity: 0.1,
		solver: 'forceAtlas2Based',
		stabilization: {
			enabled: true,
			iterations: 1000,
			updateInterval: 100,
			onlyDynamicEdges: false,
			fit: true
		},
		timestep: 0.5,
		adaptiveTimestep: true
	}            
};

initGraphStore();

document.addEventListener('DOMContentLoaded',function(){
	var loading = document.getElementById('loading');
	loading.classList.remove('loaded');
	if (localStorage.getItem('treeStatus') && localStorage.getItem('treeStatus') == freshStartStatus) {
		resetLocalDbAndDataOfTrees()
		.then(drawVisTree);
	} else if (localStorage.getItem('treeStatus') && localStorage.getItem('treeStatus') == treeAvailableStatus) {
		selectAllFromTrees()
		.then(updateTreeStateWithSelectedResults)
		.then(drawVisTree);
	} else {
		localStorage.setItem('treeStatus', freshStartStatus);
		resetLocalDbAndDataOfTrees()
		.then(function() {
			console.log("No state information found in local tree store. Restarting from scratch.");
			return Promise.resolve();
		})
		.then(drawVisTree);
	}
});

async function drawVisTree() {
	let promise = await new Promise(
		function (resolve, reject) {
			try {
				var loading = document.getElementById('loading');
				loading.classList.remove('loaded');
				visData = parseTreeData()
					.then(determineGraphFromNodesAndEdges)
					.then(destroy)
					.then(createVisNetwork);
			} catch(error) {
				console.log("Error during drawing visJs tree: " + error);
				reject();
			}
			resolve(visData);
		}
	)
	.catch(error => { 
		loading.classList.add('loaded');
		console.log("Error during drawing visJs tree: " + error);
	});
	return promise;
}

function parseTreeData() {
	nodeData = treeData;
	var promiseStruct = [ nodeData, 0 ];
	return Promise.resolve(promiseStruct);
}

function redraw() {
	if (visData) {
		console.log("Redrawing visJs tree.");
		return destroy(visData)
			.then(createVisNetwork);
	} else {
		console.log("No input data found for visJs. Trying to restart.");
		return drawVisTree();
	}
}

function createVisNetwork(visData) {
	try {
		var loading = document.getElementById('loading');
		loading.classList.remove('loaded');

		var container = document.getElementById('mynetwork');
	    network = new vis.Network(container, visData, options);
	
	    network.on('select', function (params) {
	        document.getElementById('selection').innerHTML = 'Selection: ' + params.nodes;
	    });
	
	    network.on("afterDrawing", function (ctx) {
			var dataURL = ctx.canvas.toDataURL();
			document.getElementById('canvasImg').src = dataURL;
		});
		loading.classList.add('loaded');
	} catch(error) {
		console.log("Error during creation of visJs tree: " + error);
		loading.classList.add('loaded');
		return Promise.reject();
	}
    
    return Promise.resolve(visData);
}

function destroy(nodesedges) {
    if (network !== null) {
        network.destroy();
        network = null;
    }
    return Promise.resolve(nodesedges);
}

var counter;
var nodesedges;

async function determineGraphFromNodesAndEdges(promiseStruct) {
	let promise = await new Promise(
		function (resolve, reject) {
			try {
				counter = 0;
				nodesedges = {
					nodes : [],
					edges : []
				};
				console.log("Transforming tree to visJs input.")
				updateNodesAndEdges(promiseStruct[0], promiseStruct[1]);
			} catch(error) {
				console.log("Error during transformation of tree data to visJs required input format: " + error);
				reject();
			}
			resolve(nodesedges);
		}
	)
	.catch(error => { 
		console.log("Error during preparation of visJs input data: " + error);
	});
	return promise;
}

async function updateNodesAndEdges(nodeData, parentId) {
	let promise = await new Promise(
		function (resolve, reject) {
			try {
				var arrayLength = nodeData.length;
				for (var i = 0; i < arrayLength; i++) {
					var curNode = nodeData[i]
					var label;
					if (curNode.label && curNode.label.indexOf("<") !== -1) {
						label = curNode.label.split('<')[1];
						label = label.split('>')[0];
					}
					else if (curNode.label && curNode.label.indexOf("[") !== -1) {
						label = "[]";
					} else if (curNode.label) {
						label = curNode.label;
					} else {
						label = "empty";
					}
					var newNode = {
						"id" : counter++,
						"label" : label,
						"color" : { 
							background : curNode.color, 
							border : "#000000"
						}
					}; 
					nodesedges.nodes.push(newNode);
					if (nodesedges.nodes.length > 0) {
						var label;
						if (curNode.label && curNode.label.indexOf("<") !== -1) {
							label = curNode.label.split('<')[0];
						}
						else if (curNode.label && curNode.label.indexOf("[") !== -1) {
							label = curNode.label.split('[')[0];
						} else if (curNode.label) {
							label = curNode.label;
						} else {
							label = "empty";
						}
						var newEdge = {
							"from" : parentId,
							"to" : newNode.id,
							font: {align: 'middle'},
							"label" : label
						}
						nodesedges.edges.push(newEdge);    				
					}
					var children = curNode.children;
					if (children) {
						updateNodesAndEdges(children, newNode.id);
					}
				}
				resolve();
			} catch(error) {
				console.log("Error during transformation of node with parentId " + parentId + ": " + error);
				reject();
			}
		}
	)
	.catch(error => { 
		console.log("Error during transformation of node with parentId " + parentId + " to visJs: " + error);
	});
	return promise;
}

var levelseparationslider = document.getElementById("levelseparationslider");
var outputlevelseparationslider = document.getElementById("levelseparationvalue");
outputlevelseparationslider.innerHTML = levelseparationslider.value;
levelseparationslider.oninput = function() {
	outputlevelseparationslider.innerHTML = this.value;
	options.layout.hierarchical.levelSeparation = parseInt(this.value);
}
levelseparationslider.onmouseup = function() {
	redraw();
};

var nodespacingslider = document.getElementById("nodespacingslider");
var outputnodespacingslider = document.getElementById("nodespacingvalue");
outputnodespacingslider.innerHTML = nodespacingslider.value;
nodespacingslider.oninput = function() {
	outputnodespacingslider.innerHTML = this.value;
	options.layout.hierarchical.nodeSpacing = parseInt(this.value);
}
nodespacingslider.onmouseup = function() {
	redraw();
};

var treespacingslider = document.getElementById("treespacingslider");
var outputtreespacingslider = document.getElementById("treespacingvalue");
outputtreespacingslider.innerHTML = treespacingslider.value;
treespacingslider.oninput = function() {
	outputtreespacingslider.innerHTML = this.value;
	options.layout.hierarchical.treeSpacing = parseInt(this.value);
}
treespacingslider.onmouseup = function() {
	redraw();
};

var arrowsizeslider = document.getElementById("arrowsizeslider");
var outputarrowsizeslider = document.getElementById("arrowsizevalue");
outputarrowsizeslider.innerHTML = arrowsizeslider.value;
arrowsizeslider.oninput = function() {
	outputarrowsizeslider.innerHTML = this.value;
	options.edges.arrows.to.scaleFactor = parseFloat(this.value/10.0);
}
arrowsizeslider.onmouseup = function() {
	redraw();
};

var edgefontsizeslider = document.getElementById("edgefontsizeslider");
var outputedgefontsizeslider = document.getElementById("edgefontsizevalue");
outputedgefontsizeslider.innerHTML = edgefontsizeslider.value;
edgefontsizeslider.oninput = function() {
	outputedgefontsizeslider.innerHTML = this.value;
	options.edges.font.size = parseInt(this.value);
}
edgefontsizeslider.onmouseup = function() {
	redraw();
};

var roundnessslider = document.getElementById("roundnessslider");
var outputroundnessslider = document.getElementById("roundnessvalue");
outputroundnessslider.innerHTML = roundnessslider.value;
roundnessslider.oninput = function() {
	outputroundnessslider.innerHTML = this.value;
	options.edges.smooth.roundness = parseFloat(this.value/100.0);
}
roundnessslider.onmouseup = function() {
	redraw();
};

var edgewidthslider = document.getElementById("edgewidthslider");
var outputedgewidthslider = document.getElementById("edgewidthvalue");
outputedgewidthslider.innerHTML = edgewidthslider.value;
edgewidthslider.oninput = function() {
	outputedgewidthslider.innerHTML = this.value;
	options.edges.scaling.max = parseInt(this.value);
}
edgewidthslider.onmouseup = function() {
	redraw();
};

var edgelengthslider = document.getElementById("edgelengthslider");
var outputedgelengthslider = document.getElementById("edgelengthvalue");
outputedgelengthslider.innerHTML = edgelengthslider.value;
edgelengthslider.oninput = function() {
	outputedgelengthslider.innerHTML = this.value;
	options.edges.length = parseInt(this.value);
}
edgelengthslider.onmouseup = function() {
	redraw();
};

var edgelabelwidthslider = document.getElementById("edgelabelwidthslider");
var outputedgelabelwidthslider = document.getElementById("edgelabelwidthvalue");
outputedgelabelwidthslider.innerHTML = edgelabelwidthslider.value;
edgelabelwidthslider.oninput = function() {
	outputedgelabelwidthslider.innerHTML = this.value;
	options.edges.widthConstraint = parseInt(this.value);
}
edgelabelwidthslider.onmouseup = function() {
	redraw();
};

var nodeborderslider = document.getElementById("nodeborderslider");
var outputnodeborderslider = document.getElementById("nodebordervalue");
outputnodeborderslider.innerHTML = nodeborderslider.value;
nodeborderslider.oninput = function() {
	outputnodeborderslider.innerHTML = this.value;
	options.nodes.borderWidth = parseInt(this.value);
};

var nodefontsizeslider = document.getElementById("nodefontsizeslider");
var outputnodefontsizeslider = document.getElementById("nodefontsizevalue");
outputnodefontsizeslider.innerHTML = nodefontsizeslider.value;
nodefontsizeslider.oninput = function() {
	outputnodefontsizeslider.innerHTML = this.value;
	options.nodes.font.size = parseInt(this.value);
}
nodefontsizeslider.onmouseup = function() {
	redraw();
};

var nodemassslider = document.getElementById("nodemassslider");
var outputnodemassslider = document.getElementById("nodemassvalue");
outputnodemassslider.innerHTML = nodemassslider.value;
nodemassslider.oninput = function() {
	outputnodemassslider.innerHTML = this.value;
	options.nodes.mass = parseInt(this.value);
}
nodemassslider.onmouseup = function() {
	redraw();
};

var nodescalingslider = document.getElementById("nodescalingslider");
var outputnodescalingslider = document.getElementById("nodescalingvalue");
outputnodescalingslider.innerHTML = nodescalingslider.value;
nodescalingslider.oninput = function() {
	outputnodescalingslider.innerHTML = this.value;
	options.nodes.scaling.max = parseInt(this.value);
}
nodescalingslider.onmouseup = function() {
	redraw();
};

var nodesizeslider = document.getElementById("nodesizeslider");
var outputnodesizeslider = document.getElementById("nodesizevalue");
outputnodesizeslider.innerHTML = nodesizeslider.value;
nodesizeslider.oninput = function() {
	outputnodesizeslider.innerHTML = this.value;
	options.nodes.size = parseInt(this.value);
}
nodesizeslider.onmouseup = function() {
	redraw();
};

var nodelabelwidthslider = document.getElementById("nodelabelwidthslider");
var outputnodelabelwidthslider = document.getElementById("nodelabelwidthvalue");
outputnodelabelwidthslider.innerHTML = nodelabelwidthslider.value;
nodelabelwidthslider.oninput = function() {
	outputnodelabelwidthslider.innerHTML = this.value;
	options.nodes.widthConstraint = parseInt(this.value);
}
nodelabelwidthslider.onmouseup = function() {
	redraw();
};

var dropdown = document.getElementById("layout");
dropdown.onchange = function() {
	options.layout.hierarchical.sortMethod = dropdown.value;
	redraw();
}
var dropdownEdgeType = document.getElementById("edgetype");
dropdownEdgeType.onchange = function() {
	options.edges.smooth.type = dropdownEdgeType.value;
	redraw();
}
var dropdownNodeType = document.getElementById("nodetype");
dropdownNodeType.onchange = function() {
	options.nodes.shape = dropdownNodeType.value;
	redraw();
}
var dropDownSolverType = document.getElementById("solvertype");
dropDownSolverType.onchange = function() {
	options.physics.solver = dropDownSolverType.value;
	redraw();
}
var btnUD = document.getElementById("updown");
btnUD.onclick = function () {
	options.layout.hierarchical.direction = "UD";
	redraw();
}
var btnDU = document.getElementById("downup");
	btnDU.onclick = function () {
	options.layout.hierarchical.direction = "DU";
	redraw();
};
var btnLR = document.getElementById("leftright");
	btnLR.onclick = function () {
	options.layout.hierarchical.direction = "LR";
	redraw();
};
var btnRL = document.getElementById("rightleft");
	btnRL.onclick = function () {
	options.layout.hierarchical.direction = "RL";
	redraw();
};
document.getElementById('hierarchicalswitch').addEventListener('click', function () {
	if(document.getElementById('hierarchicalswitch').checked){
		console.log("hierarchical layout");
		options.layout.hierarchical.enabled = true;
	} else {
		options.layout.hierarchical.enabled = false;
		console.log("not hierarchical layout");
	}
	redraw();
});
document.getElementById('blockshiftingswitch').addEventListener('click', function () {
	if(document.getElementById('blockshiftingswitch').checked){
		console.log("block switching layout");
		options.layout.hierarchical.blockShifting = true;
	} else {
		options.layout.hierarchical.blockShifting = false;
		console.log("no blockswitching layout");
	}
	redraw();
});
document.getElementById('edgeminswitch').addEventListener('click', function () {
	if(document.getElementById('edgeminswitch').checked){
		console.log("edge minimization layout");
		options.layout.hierarchical.edgeMinimization = true;
	} else {
		options.layout.hierarchical.edgeMinimization = false;
		console.log("no edge minimization layout");
	}
	redraw();
});
document.getElementById('parentcentralswitch').addEventListener('click', function () {
	if(document.getElementById('parentcentralswitch').checked){
		console.log("parent centralization layout");
		options.layout.hierarchical.parentCentralization = true;
	} else {
		options.layout.hierarchical.parentCentralization = false;
		console.log("no parent centralization hierarchical layout");
	}
	redraw();
});
document.getElementById('improvedlayoutswitch').addEventListener('click', function () {
	if(document.getElementById('improvedlayoutswitch').checked){
		console.log("improved layout");
		options.layout.improvedLayout = true;
	} else {
		options.layout.improvedLayout = false;
		console.log("no improved layout");
	}
	redraw();
});
document.getElementById('randomseedswitch').addEventListener('click', function () {
	if(document.getElementById('randomseedswitch').checked){
		console.log("keep layout");
		options.layout.randomSeed = network.getSeed();
	} else {
		options.layout.improvedLayout = undefined;
		console.log("new random layout");
	}
	redraw();
});
document.getElementById('smoothswitch').addEventListener('click', function () {
	if(document.getElementById('smoothswitch').checked){
		console.log("smooth edges");
		options.edges.smooth.enabled = true;
	} else {
		options.edges.smooth.enabled = false;
		console.log("no smooth edges");
	}
	redraw();
});
document.getElementById('physicswitch').addEventListener('click', function () {
	if(document.getElementById('physicswitch').checked){
		console.log("use physics");
		options.physics.enabled = true;
	} else {
		options.physics.enabled = false;
		console.log("no physics");
	}
	redraw();
});

function destroyClickedElement(event) {
	document.body.removeChild(event.target);
};

document.getElementById('exportpng').addEventListener('click', function () {
	console.log("export png");
	var dataURL = document.getElementById("canvasImg").src;
	var downloadLink = document.createElement("a");
	downloadLink.download = "graph.png";
	downloadLink.innerHTML = "Download File";
	downloadLink.href = dataURL;
	downloadLink.onclick = destroyClickedElement;
	downloadLink.style.display = "none";
	document.body.appendChild(downloadLink);
	downloadLink.click();
});

