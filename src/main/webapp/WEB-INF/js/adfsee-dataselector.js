/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
Dropzone.options.fileUploadForm = {
		dictDefaultMessage: "Drop data file here (.TTL or .ADF)",
		maxFilesize: 2000,
		thumbnailWidth: 40,
		thumbnailHeight: 40,
		init: function() {
            this.on("success", function(files, serverResponse) {
            	$("#fileUploadForm div.dz-default.dz-message").css("display","none");
                showServerResponse(serverResponse);
            });
            this.on("successmultiple", function(files, serverResponse) {
            	$("#fileUploadForm div.dz-default.dz-message").css("display","none");
                showServerResponse(serverResponse);
            });
			this.on("error", function(file, response) {
                showServerResponse(response);
			});
			this.on("errormultiple", function(file, response) {
                showServerResponse(response);
			});
        },
		addRemoveLinks: false
};
	
Dropzone.options.additionalFileUploadForm = {
		dictDefaultMessage: "Drop additional turtle files here<br/>(e.g. vocabulary)",
		maxFileSize: 2000,
		thumbnailWidth: 40,
		thumbnailHeight: 40,
		init: function() {
            this.on("success", function(files, serverResponse) {
            	$("#additionalFileUploadForm div.dz-default.dz-message").css("display","none");
                showServerResponse(serverResponse);
            });
            this.on("successmultiple", function(files, serverResponse) {
            	$("#additionalFileUploadForm div.dz-default.dz-message").css("display","none");
                showServerResponse(serverResponse);
            });
			this.on("error", function(file, response) {
                showServerResponse(response);
			});
			this.on("errormultiple", function(file, response) {
                showServerResponse(response);
			});
        },
		addRemoveLinks: false
};

function resetDivProcessingResult(){
	$("#processingresult").html("");
	$("#processingresult").removeClass("ok");
	$("#processingresult").removeClass("error");
	$("#processingresult").removeClass("unknown");
}

function showServerResponse(response) {
	if (response) {
		if (response.success) {
			resetDivProcessingResult();
			$("#processingresult").addClass("ok");
			$("#processingresult").html(response.message);
		} else {
			resetDivProcessingResult();
			$("#processingresult").addClass("error");
			$("#processingresult").html(response.message);
			removeUploadedFileDivs();		    			
		}
	} else {
		showServerError();
	}
};

function showServerError() {
	resetDivProcessingResult();
		$("#processingresult").addClass("unknown");
        $("#processingresult").html("Unknown server state. Please try again later.");
		removeUploadedFileDivs();
};

var isGeneratedClickToFadeOutAndRemove = false;

function removeUploadedFileDivs() {
	var dataDropZone = document.getElementById("fileUploadForm");
	var deleted = false;
	var childNodes = dataDropZone.childNodes;
	for(var i = childNodes.length - 1; 0 <= i; i--) {
		if (childNodes[i].localName 
				&& childNodes[i].localName.toLowerCase() == "div" 
				&& childNodes[i].className 
				&& (childNodes[i].className.includes("dz-preview") || childNodes[i].className.includes("dz-file-preview"))) {
				isGeneratedClickToFadeOutAndRemove = true;
				dataDropZone.childNodes[i].style.display = "block";
				dataDropZone.childNodes[i].id = "data-preview-" + i;
			dataDropZone.childNodes[i].click();	    				
			deleted = true;
		}
	}
	
	if (deleted) {
		childNodes = dataDropZone.childNodes;
		for(var i = childNodes.length - 1; 0 <= i; i--) {
			if (childNodes[i].localName 
					&& childNodes[i].localName.toLowerCase() == "div" 
					&& childNodes[i].className 
					&& (childNodes[i].className.includes("dz-default") || (childNodes[i].className.includes("dz-message")))) {
			dataDropZone.removeChild(childNodes[i]);
			}
		}
		var divtest = document.createElement("div");
	    var resetDiv = document.createElement("div");
	    resetDiv.classList.add('dz-default');
	    resetDiv.classList.add('dz-message');
	    var resetSpan = document.createElement("span");
	    resetSpan.textContent = "Data file(s) removed. Please upload again (.TTL or .ADF)";
		resetDiv.appendChild(resetSpan);
	    dataDropZone.appendChild(resetDiv);
	    dataDropZone.classList.remove("dz-started");
	}

	var additionalDropZone = document.getElementById("additionalFileUploadForm");
	deleted = false;
	childNodes = additionalDropZone.childNodes;
	for(var i = childNodes.length - 1; 0 <= i; i--) {
		if (childNodes[i].localName 
				&& childNodes[i].localName.toLowerCase() == "div" 
				&& childNodes[i].className 
				&& (childNodes[i].className.includes("dz-preview") || childNodes[i].className.includes("dz-file-preview"))) {
				isGeneratedClickToFadeOutAndRemove = true;
			additionalDropZone.childNodes[i].style.display = "block";
			additionalDropZone.childNodes[i].id = "additional-preview-" + i;
			additionalDropZone.childNodes[i].click();	    				
			deleted = true;
		}
	}

	if (deleted) {
			childNodes = additionalDropZone.childNodes;
		for(var i = childNodes.length - 1; 0 <= i; i--) {
				if (childNodes[i].localName 
						&& childNodes[i].localName.toLowerCase() == "div" 
						&& childNodes[i].className 
						&& (childNodes[i].className.includes("dz-default") || (childNodes[i].className.includes("dz-message")))) {
				additionalDropZone.removeChild(childNodes[i]);
				}
			}
		var divtest = document.createElement("div");
	    var resetDiv = document.createElement("div");
	    resetDiv.classList.add('dz-default');
	    resetDiv.classList.add('dz-message');
	    var resetSpan = document.createElement("span");
	    resetSpan.textContent = "Additional file(s) removed. Please upload again.";
		resetDiv.appendChild(resetSpan);
		additionalDropZone.appendChild(resetDiv);
		additionalDropZone.classList.remove("dz-started");
	}
	isGeneratedClickToFadeOutAndRemove = false;
}

document.addEventListener( "click", dropzoneCleanupListener );

function dropzoneCleanupListener(event){
    var element = event.target;
    if(element.tagName.toLowerCase() == 'div' && (element.classList.contains("dz-preview") || element.classList.contains("dz-file-preview"))) {
		if(isGeneratedClickToFadeOutAndRemove){
			var id = "#" + element.id;
			$(id).fadeOut(1000, function() { 
				$(this).remove(); 
			});
		};
    };
};

$(function() {
    $('.content').hide();
    $('#uploaddiv').hover(function() {
        $(this).children('.content').stop().slideDown();
    }, function() {
        $(this).children('.content').stop().slideUp();
    })
});		

document.getElementById('includeafo').addEventListener('click', function () {
	if(document.getElementById('includeafo').checked){
		loadafo = true;
	} else {
		loadafo = false;
	}
});

document.getElementById('includeaft').addEventListener('click', function () {
	if(document.getElementById('includeaft').checked){
		loadaft = true;
	} else {
		loadaft = false;
	}
});

document.getElementById('dolayout').addEventListener('click', function () {
	if(document.getElementById('dolayout').checked){
		dolayout = true;
	} else {
		dolayout = false;
	}
});

document.getElementById('literalnodes').addEventListener('click', function () {
	if(document.getElementById('literalnodes').checked){
		excludeliterals = true;
	} else {
		excludeliterals = false;
	}
});

document.getElementById('includeadf').addEventListener('click', function () {
	if(document.getElementById('includeadf').checked){
		includeadf = true;
	} else {
		includeadf = false;
	}
});

document.getElementById('example').addEventListener('click', function () {
	if(document.getElementById('example').checked){
		loadexample = true;
	} else {
		loadexample = false;
	}
});

document.getElementById('multigraph').addEventListener('click', function () {
	if(document.getElementById('multigraph').checked){
		multigraph = true;
	} else {
		multigraph = false;
	}
});

document.getElementById('skolemize').addEventListener('click', function () {
	if(document.getElementById('skolemize').checked){
		skolemize = true;
	} else {
		skolemize = false;
	}
});

document.getElementById('endpoint').addEventListener('click', function () {
	if(document.getElementById('endpoint').checked){
		endpoint = true;
	} else {
		endpoint = false;
	}
});

document.getElementById('construct').addEventListener('change', async function () {
	var construct = document.getElementById('construct').value;
	if(construct == ""){
		construct = "construct query";
		document.getElementById('construct').value = construct;
	}
	await addInputsToLocalStorage(construct);
});

document.getElementById('instancecheckmark').addEventListener('click', function () {
	if (!document.getElementById('instanceinput').checked) {
		document.getElementById('instanceinput').click();
	}
	console.log("see instances = " + document.getElementById('instanceinput').checked);
	console.log("see classes = " + document.getElementById('classinput').checked);
});					

document.getElementById('classcheckmark').addEventListener('click', function () {
	if (!document.getElementById('classinput').checked) {
		document.getElementById('classinput').click();
	}
	console.log("see instances = " + document.getElementById('instanceinput').checked);
	console.log("see classes = " + document.getElementById('classinput').checked);
});					

document.getElementById('processbutton').addEventListener('click', function () {
	resetDivProcessingResult();
	$("#processingresult").addClass("ok");
	$("#processingresult").html("Waiting for server response...");
	document.getElementById('adfseeimage').classList.add('adfseeloading');
	var seeinstances = document.getElementById('instanceinput').checked;
	var seeclasses = document.getElementById('classinput').checked;
	var includeadfapi = document.getElementById('includeadf').checked;
	var excludeliterals = document.getElementById('literalnodes').checked;
	var endpoint = document.getElementById('endpoint').checked;
	var baseUrl = "http://localhost:8181/generate";
	var theurl;
	if (loadafo && loadaft) {
		theurl = baseUrl + "?afo=1&aft=1";
	} else if (loadafo && !loadaft) {
		theurl = baseUrl + "?afo=1";
	} else if  (!loadafo && loadaft) {
		theurl = baseUrl + "?aft=1";
	}
	
	if (dolayout) {
		if (loadafo || loadaft) {
			theurl = theurl + "&layout=1";
		} else {
			theurl = baseUrl + "?layout=1";
		}
	}

	if (loadexample) {
		theurl = baseUrl + "?example=1";
		
		if (multigraph) {
			theurl = theurl + "&multigraph=1";
		}
		
		if (skolemize) {
			theurl = theurl + "&skolemize=1";
		}
		
		if (dolayout) {
			theurl = theurl + "&layout=1";
		}
	} else {
		if (multigraph) {
			if (loadafo || loadaft || dolayout) {
				theurl = theurl + "&multigraph=1";
				if (skolemize) {
					theurl = theurl + "&skolemize=1";
				}
			} else {
				theurl = theurl + "?multigraph=1";
				if (skolemize) {
					theurl = theurl + "&skolemize=1";
				}
				
				if (dolayout) {
					theurl = theurl + "&layout=1";
				}
			}
		} else if (skolemize) {
			if (loadafo || loadaft || dolayout) {
				theurl = theurl + "&skolemize=1";
			} else {
				theurl = theurl + "?skolemize=1";
			}
		}
	}
	
	if (!loadafo && !loadaft && !dolayout && !loadexample) {
		theurl = baseUrl;
		
		if (multigraph) {
			theurl = theurl + "&multigraph=1";
		}

		if (skolemize) {
			theurl = theurl + "&skolemize=1";
		}
	}
	
	if (seeinstances) {
		if (loadafo || loadaft || loadexample || multigraph || skolemize || dolayout) {
			theurl = theurl + "&instances=1";
		} else {
			theurl = theurl + "?instances=1";
		}
	}
	
	if (seeclasses) {
		if (loadafo || loadaft || loadexample || multigraph || skolemize || dolayout) {
			theurl = theurl + "&classes=1";
		} else {
			theurl = theurl + "?classes=1";
		}
	}
	
	if (includeadfapi) {
		if (loadafo || loadaft || loadexample || seeinstances || seeclasses || multigraph || skolemize || dolayout) {
			theurl = theurl + "&adfapi=1";
		} else {
			theurl = theurl + "?adfapi=1";
		}
	}
	
	if (excludeliterals) {
		if (loadafo || loadaft || loadexample || seeinstances || seeclasses || includeadfapi || multigraph || skolemize || dolayout) {
			theurl = theurl + "&excludeliterals=1";
		} else {
			theurl = theurl + "?excludeliterals=1";
		}
	}
	
	if (endpoint) {
		var endpointUrl = encodeURI(document.getElementById('endpointurl').value);
		var graphUrl = encodeURI(document.getElementById('graph').value);
		var construct = encodeURIComponent(document.getElementById('construct').value);
		if (loadafo || loadaft || loadexample || seeinstances || seeclasses || includeadfapi || excludeliterals || multigraph || skolemize || dolayout) {
			theurl = theurl + "&endpoint=" + endpointUrl + "&graph=" + graphUrl + "&query=" + construct;
		} else {
			theurl = theurl + "?endpoint=" + endpointUrl + "&graph=" + graphUrl + "&query=" + construct;
		}
	} else {
		var construct = encodeURIComponent(document.getElementById('construct').value);
		if (construct) {
			if (loadafo || loadaft || loadexample || seeinstances || seeclasses || includeadfapi || excludeliterals || multigraph || skolemize || dolayout) {
				theurl = theurl + "&query=" + construct;
			} else {
				theurl = theurl + "?&query=" + construct;
			}
		}
	}
	
	var retVal = $.ajax({
		headers:{  
	    	"key":"key",
	    	"Accept":"application/json",
	     	"Content-type":"application/json"
		},
		url: theurl,
		contentType: 'application/json',
		timeout: 3000000,
		success:function(response){
	    	console.log(response);
			document.getElementById('adfseeimage').classList.remove('adfseeloading');
			document.getElementById('loading').classList.remove('loaded');
			try {
				showServerResponse(response);
				var errorMsg;
				if (response && response.graphString) {
				    try {
				        var elements = JSON.parse(response.graphString);
						updateGraphStatusInLocalStorage(graphAvailableStatus);
				        updateCytoscapeGraph(elements)
				        	.then(extractInitialPositions)
				        	.then(addPositionsDataToLocalStorage)
				        	.then(selectAllFromInitialPositions);
				        addGraphDataToLocalStorage(elements)
				        	.then(selectAllFromGraphs)
				        	.then(updateGraphDataInMemory);
				    } catch(e) {
				    	console.log(e);
		    			errorMsg = "Error during parsing of graph data in server response.";
		    			resetDivProcessingResult();
		    			$("#processingresult").addClass("error");
		    			$("#processingresult").html(errorMsg);
				    }
				}
				if (response && response.treeString) {
				    try {
				        var trees = JSON.parse(response.treeString);
				        updateTreeStatusInLocalStorage(treeAvailableStatus);
				        addTreeDataToLocalStorage(trees)
				        	.then(selectAllFromTrees)
				        	.then(updateTreeDataInMemory)
				        	.then(updateTree);
				    } catch(e) {
				    	console.log(e);
		    			errorMsg += " Error during parsing of tree data in server response."
	    				resetDivProcessingResult();
		    			$("#processingresult").addClass("error");
		    			$("#processingresult").html(errorMsg);
				    }
				}
				if (response && response.queryResultsString) {
				    try {
				        var queryResults = JSON.parse(response.queryResultsString);
				        updateQueryResultsStatusInLocalStorage(queryResultsAvailableStatus);
				        addQueryResultsDataToLocalStorage(queryResults)
				        	.then(selectAllFromQueryResults)
				        	.then(updateQueryResultsDataInMemory)
				        	.then(updateQueryResultsPage);
				    } catch(e) {
				    	console.log(e);
		    			errorMsg += " Error during parsing of query results data in server response."
	    				resetDivProcessingResult();
		    			$("#processingresult").addClass("error");
		    			$("#processingresult").html(errorMsg);
				    }
				}
				if (response && response.datacubesString) {
				    try {
				        var datacubes = JSON.parse(response.datacubesString);
				        updateDatacubesStatusInLocalStorage(datacubesAvailableStatus);
				        addDatacubesToLocalStorage(datacubes)
				        	.then(selectAllFromDatacubes)
				        	.then(updateDatacubesInMemory)
				        	.then(updateDatacubesPage);
				    } catch(e) {
				    	console.log(e);
		    			errorMsg += " Error during parsing of datacubes in server response."
	    				resetDivProcessingResult();
		    			$("#processingresult").addClass("error");
		    			$("#processingresult").html(errorMsg);
				    }
				}
				if (response && response.audittrailString) {
				    try {
				        var audittrails = JSON.parse(response.audittrailString);
				        audittrails = htmlEscape(JSON.stringify(audittrails));
				        audittrails = JSON.parse(audittrails);

				        updateAudittrailStatusInLocalStorage(audittrailAvailableStatus);
				        addAudittrailDataToLocalStorage(audittrails)
				        	.then(selectAllFromAudittrails)
				        	.then(updateAudittrailDataInMemory)
				        	.then(updateAudittrail);
				    } catch(e) {
				    	console.log(e);
		    			errorMsg += " Error during parsing of audittrail data in server response."
	    				resetDivProcessingResult();
		    			$("#processingresult").addClass("error");
		    			$("#processingresult").html(errorMsg);
				    }
				}
				if (response && response.adfInfoString) {
				    try {
				        var adfInfos = JSON.parse(response.adfInfoString);
				        adfInfos = htmlEscape(JSON.stringify(adfInfos));
				        adfInfos = JSON.parse(adfInfos);

				        updateAdfInfoStatusInLocalStorage(adfInfoAvailableStatus);
				        addAdfInfoDataToLocalStorage(adfInfos)
				        	.then(selectAllFromAdfInfos)
				        	.then(updateAdfInfoDataInMemory)
				        	.then(updateAdfInfo);
				    } catch(e) {
				    	console.log(e);
		    			errorMsg += " Error during parsing of adfInfo data in server response."
	    				resetDivProcessingResult();
		    			$("#processingresult").addClass("error");
		    			$("#processingresult").html(errorMsg);
				    }
				}
				if (response && response.datapackageString) {
				    try {
				        var datapackage = JSON.parse(response.datapackageString);
				        datapackage = htmlEscape(JSON.stringify(datapackage));
				        datapackage = JSON.parse(datapackage);

				        updateDatapackageStatusInLocalStorage(datapackageAvailableStatus);
				        addDatapackageDataToLocalStorage(datapackage)
				        	.then(selectAllFromDatapackage)
				        	.then(updateDatapackageDataInMemory)
				        	.then(updateDatapackage);
				    } catch(e) {
				    	console.log(e);
		    			errorMsg += " Error during parsing of datapackage data in server response."
	    				resetDivProcessingResult();
		    			$("#processingresult").addClass("error");
		    			$("#processingresult").html(errorMsg);
				    }
				}
				if (response && response.datadescriptionString) {
				    try {
				        var datadescription = JSON.parse(response.datadescriptionString);
				        datadescription = htmlEscape(JSON.stringify(datadescription));
				        datadescription = JSON.parse(datadescription);

				        updateDatadescriptionStatusInLocalStorage(datadescriptionAvailableStatus);
				        addDatadescriptionDataToLocalStorage(datadescription)
				        	.then(selectAllFromDatadescription)
				        	.then(updateDatadescriptionDataInMemory)
				        	.then(updateDatadescription);
				    } catch(e) {
				    	console.log(e);
		    			errorMsg += " Error during parsing of datadescription data in server response."
	    				resetDivProcessingResult();
		    			$("#processingresult").addClass("error");
		    			$("#processingresult").html(errorMsg);
				    }
				}
				if (response && response.layoutString) {
				    try {
				        var elements = JSON.parse(response.layoutString);
						updateGraphStatusInLocalStorage(graphAvailableStatus);
				        updateCytoscapeLayout(elements);
				    } catch(e) {
				    	console.log(e);
		    			errorMsg = "Error during parsing of layout data in server response.";
		    			resetDivProcessingResult();
		    			$("#processingresult").addClass("error");
		    			$("#processingresult").html(errorMsg);
				    }
				}
				removeUploadedFileDivs();	
			} catch(error) {
				console.log("Error during processing server response: " + e);
			} finally {
				document.getElementById('adfseeimage').classList.remove('adfseeloading');
				document.getElementById('loading').classList.add('loaded');
			}
		},
		error:function(response){
	    	console.log(response);
			document.getElementById('adfseeimage').classList.remove('adfseeloading');
			document.getElementById('loading').classList.add('loaded');
			showServerError();
	    }
	});
	
	document.getElementById('loading').classList.add('loaded');
});

/** To stop XSS attacks by using JSON with HTML nodes **/
function htmlEscape(str) {
	const tagsToReplace = {
		"&": "&amp;",
		"<": "&lt;",
		">": "&gt;"
	};
	return str.replace(/[&<>]/g, function(tag) {
		return tagsToReplace[tag] || tag;
	});
}

function updateGraphDataInMemory(results) {
	try {
		if (results && results[0]) {
			data = JSON.parse(results[0].data);
			console.log("Updated graph data in memory.");
		}
	} catch(error) {
		console.log("Error during parsing response string: " + error)
		return Promise.reject();
	}
	return Promise.resolve();
}

function updateTreeDataInMemory(results) {
	try {
		if (results && results[0]) {
			treeData = JSON.parse(results[0].data);
			console.log("Updated tree data in memory.");
		}
	} catch(error) {
		console.log("Error during parsing response string: " + error)
		return Promise.reject();
	}
	return Promise.resolve();
}

function updateAudittrailDataInMemory(results) {
	try {
		if (results && results[0]) {
			audittrail = JSON.parse(results[0].data);
			console.log("Updated audittrail data in memory.");
		}
	} catch(error) {
		console.log("Error during parsing response string: " + error)
		return Promise.reject();
	}
	return Promise.resolve();
}

function updateAdfInfoDataInMemory(results) {
	try {
		if (results && results[0]) {
			adfInfo = JSON.parse(results[0].data);
			console.log("Updated adfInfo data in memory.");
		}
	} catch(error) {
		console.log("Error during parsing response string: " + error)
		return Promise.reject();
	}
	return Promise.resolve();
}

function updateDatapackageDataInMemory(results) {
	try {
		if (results && results[0]) {
			datapackage = JSON.parse(results[0].data);
			console.log("Updated datapackage data in memory.");
		}
	} catch(error) {
		console.log("Error during parsing response string: " + error)
		return Promise.reject();
	}
	return Promise.resolve();
}

function updateDatadescriptionDataInMemory(results) {
	try {
		if (results && results[0]) {
			datadescription = JSON.parse(results[0].data);
			console.log("Updated datadescription data in memory.");
		}
	} catch(error) {
		console.log("Error during parsing response string: " + error)
		return Promise.reject();
	}
	return Promise.resolve();
}

function updateQueryResultsDataInMemory(results) {
	try {
		if (results && results[0]) {
			queryResultsData = JSON.parse(results[0].data);
			console.log("Updated query results data in memory.");
		}
	} catch(error) {
		console.log("Error during parsing response string: " + error)
		return Promise.reject();
	}
	return Promise.resolve();
}

function updateDatacubesInMemory(results) {
	try {
		if (results && results[0]) {
			datacubes = JSON.parse(results[0].data);
			console.log("Updated datacubes in memory.");
		}
	} catch(error) {
		console.log("Error during parsing response string: " + error)
		return Promise.reject();
	}
	return Promise.resolve();
}

function extractInitialPositions(e) {
	try {
		initialPositions = [];
		var arrayLength = e.length;
		for (var i = 0; i < arrayLength; i++) {
			var element = e[i];
			if (element.group == "edges") {
				continue;
			}
		    var nodeId = element.data.id;
		    var nodeX = element.position.x;
		    var nodeY = element.position.y;
		    initialPositions.push({
		    	"id" : nodeId,
		    	"x" : nodeX,
		    	"y" : nodeY
		    });
		}
	} catch (error) {
		console.log("Error during storing initial node positions: " + error);
		return Promise.reject();
	}
	return Promise.resolve(initialPositions);
}

function updateCytoscapeGraph(e) {
	var pageUrl = window.location.href;
	if (!pageUrl) {
		console.log("Error missing page URL. Trying to reload...");
		location.reload();
		return Promise.reject();
	}

	if (pageUrl && (pageUrl == "http://localhost:8181/" || pageUrl == "http://localhost:8181" || pageUrl.includes("index.html") || pageUrl.includes("html-label.html"))) {
		cy.elements().remove();
		cy.add(e);
		cy.resize();
		cy.fit();
	} else {
		console.log("Updating cytoscape not required.");
	}
	return Promise.resolve(e);
}

function updateCytoscapeLayout(e) {
	var pageUrl = window.location.href;
	if (!pageUrl) {
		console.log("Error missing page URL. Trying to reload...");
		location.reload();
		return Promise.reject();
	}

	if (pageUrl && (pageUrl == "http://localhost:8181/" || pageUrl == "http://localhost:8181" || pageUrl.includes("index.html") || pageUrl.includes("html-label.html"))) {
		console.log("Loading cytoscape layout from json.");
		cy.json(e);
		cy.resize();
		cy.fit();
	} else {
		console.log("Updating cytoscape not required.");
	}
	return Promise.resolve(e);
}

function updateTree() {
	var pageUrl = window.location.href;
	if (!pageUrl) {
		console.log("Error missing page URL. Trying to reload...");
		location.reload();
		return Promise.reject();
	}
	
	if (pageUrl.includes("inspire-tree")) {
		return drawTree();
	} else if (pageUrl.includes("jquery-tree")) {
		return drawTree();
	} else if (pageUrl.includes("vue-tree")) {
		mainTree().then(reloadTree);
	} else if (pageUrl.includes("vis-tree")) {
		drawVisTree();
	} else if (pageUrl.includes("3dfg.html")) {
		redraw3dfg();
	} else if (pageUrl.includes("3dfgVR.html")) {
		redraw3dfgVR();
	} else if (pageUrl.includes("2dfg.html")) {
		redraw2dfg();
	} else if (pageUrl.includes("pixi.html")) {
		redrawPixiGraph();
	} else if (pageUrl.includes("stardust.html")) {
		redrawStardustGraph();
	} else if (pageUrl.includes("d3")) {
		drawD3();
	} else {
		console.log("Updating trees not required.");
	}
	return Promise.resolve();
}

function updateAudittrail() {
	var pageUrl = window.location.href;
	if (!pageUrl) {
		console.log("Error missing page URL. Trying to reload...");
		location.reload();
		return Promise.reject();
	}
	
	if (pageUrl.includes("audit-trail.html")) {
		return createJsonTree();
	} else if (pageUrl.includes("audit-trail2.html")) {
		return doParse2();
	} else if (pageUrl.includes("audit-trail3.html")) {
		return createTreeMap();
	} else if (pageUrl.includes("audit-trail4.html")) {
		return createTreeMap();
	} else if (pageUrl.includes("audit-trail5.html")) {
		return createTreeMap();
	} else {
		console.log("Updating audit-trail not required.");
	}
	return Promise.resolve();
}

function updateAdfInfo() {
	var pageUrl = window.location.href;
	if (!pageUrl) {
		console.log("Error missing page URL. Trying to reload...");
		location.reload();
		return Promise.reject();
	}
	
	if (pageUrl.includes("adf-info.html")) {
		return createAdfInfo();
	} else {
		console.log("Updating adf-Info not required.");
	}
	return Promise.resolve();
}

function updateDatapackage() {
	var pageUrl = window.location.href;
	if (!pageUrl) {
		console.log("Error missing page URL. Trying to reload...");
		location.reload();
		return Promise.reject();
	}
	
	if (pageUrl.includes("datapackage.html")) {
		return createDatapackage();
	} else {
		console.log("Updating datapackage not required.");
	}
	return Promise.resolve();
}

function updateDatadescription() {
	var pageUrl = window.location.href;
	if (!pageUrl) {
		console.log("Error missing page URL. Trying to reload...");
		location.reload();
		return Promise.reject();
	}
	
	if (pageUrl.includes("datadescription.html")) {
		return createDatadescription();
	} else {
		console.log("Updating datadescription not required.");
	}
	return Promise.resolve();
}

function updateQueryResultsPage() {
	var pageUrl = window.location.href;
	if (!pageUrl) {
		console.log("Error missing page URL. Trying to reload...");
		location.reload();
		return Promise.reject();
	}
	
	if (pageUrl.includes("peak-table")) {
		return createTable();
	} else {
		console.log("Updating table not required.");
	}
	return Promise.resolve();
}

function updateDatacubesPage() {
	var pageUrl = window.location.href;
	if (!pageUrl) {
		console.log("Error missing page URL. Trying to reload...");
		location.reload();
		return Promise.reject();
	}
	
	if (pageUrl.includes("peak-table")) {
		return createCharts();
	} else {
		console.log("Updating datacube charts not required.");
	}
	return Promise.resolve();
}

document.getElementById('instancecheckmark').click();	 