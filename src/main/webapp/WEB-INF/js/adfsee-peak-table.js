/**
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
var data;
var treeData;
var queryResultsData;
var datacubes;
var tabledata;
var loadafo = true;
var loadaft = true;
var dolayout = true;
var multigraph = false;
var skolemize = false;
var loadexample = false;
var includeadf = false;
var columnWidths;
var columnHeaders;

initGraphStore();

document.addEventListener('DOMContentLoaded', function() {
		var loading = document.getElementById('loading');
		loading.classList.remove('loaded');

		if (localStorage.getItem('queryResultsStatus')	&& localStorage.getItem('queryResultsStatus') == freshStartStatus) {
			resetLocalDbAndDataOfQueryResults().then(createTable);
		} else if (localStorage.getItem('queryResultsStatus') && localStorage.getItem('queryResultsStatus') == queryResultsAvailableStatus) {
			selectAllFromQueryResults()
				.then(updateQueryResultsStateWithSelectedResults)
				.then(createTable);
		} else {
			localStorage.setItem('queryResultsStatus', freshStartStatus);
			resetLocalDbAndDataOfQueryResults()
				.then(function() {
					console	.log("No state information found in local query results store. Restarting from scratch.");
					return Promise.resolve();
				})
				.then(createTable);
		}

		if (localStorage.getItem('datacubesStatus')	&& localStorage.getItem('datacubesStatus') == freshStartStatus) {
			resetLocalDbAndDataOfDatacubes().then(createCharts);
		} else if (localStorage.getItem('datacubesStatus') && localStorage.getItem('datacubesStatus') == datacubesAvailableStatus) {
			selectAllFromDatacubes()
				.then(updateDatacubesStateWithSelectedResults)
				.then(createCharts);
		} else {
			localStorage.setItem('datacubesStatus',	freshStartStatus);
			resetLocalDbAndDataOfDatacubes()
				.then(function() {
					console.log("No state information found in local datacubes store. Restarting from scratch.");
					return Promise.resolve();
				})
				.then(createCharts);
		}
		loading.classList.add('loaded');
});

async function createTable() {
	var loading = document.getElementById('loading');
	loading.classList.remove('loaded');
	try {
		tabledata = await prepareTableData();
		columnWidths = await prepareColWidths(tabledata);

		$('#peaktable').jexcel({
			data : tabledata,
			colWidths : columnWidths,
			colHeaders : columnHeaders
		});
	} catch (error) {
		console.log("Error during creation of table: " + error);
		loading.classList.add('loaded');
	}
	loading.classList.add('loaded');
	return Promise.resolve();
}

async function prepareTableData() {
	if (!queryResultsData["peak-table-super-domain"]) {
		var initTable = [ [ 'How to get started', '', '' ],
				[ 'click on', 'button', 'Select data' ],
				[ 'that is', 'located at', 'right border' ],
				[ 'then', 'select', 'data files' ],
				[ 'or', 'select', 'Try example' ],
				[ 'then', 'click', 'See!' ], ];

		return Promise.resolve(initTable);
	}

	var rows = [];
	try {
		var headerRow = queryResultsData["peak-table-super-domain"].head.vars;
		if (!headerRow) {
			console.log("No header data found.");
			return Promise.reject();
		}
		columnHeaders = headerRow;

		var results = queryResultsData["peak-table-super-domain"].results.bindings;
		if (!results) {
			console.log("No results data found.");
			return Promise.resolve(rows);
		}

		var resultsCount = results.length;

		var peaktableCounter = 1;
		var peakCounter = 1;
		var currentPeaktableId;
		// results are ordered by peak table ID
		for (var i = 0; i < resultsCount; i++) {
			var rowLength = await countProperties(results[i]);

			if (rowLength <= 3) {
				console.log("Skipped empty row: " + JSON.stringify(results[i]));
				continue;
			}

			var row = [];

			if (results[i].peaktable && results[i].peaktable.value) {
				if (!currentPeaktableId) {
					currentPeaktableId = results[i].peaktable.value;
				}

				if (currentPeaktableId != results[i].peaktable.value) {
					currentPeaktableId = results[i].peaktable.value;
					peaktableCounter++;
					peakCounter = 1;
				}
				row.push(peaktableCounter + " (" + results[i].peaktable.value + ")");
			} else {
				row.push('');
			}

			if (results[i].peak && results[i].peak.value) {
				row.push(peakCounter++ + " (" + results[i].peak.value + ")");
			} else {
				row.push(peakCounter++);
			}

			if (results[i].index && results[i].index.value) {
				row.push(results[i].index.value);
			} else {
				row.push('');
			}

			if (results[i].name && results[i].name.value) {
				row.push(results[i].name.value);
			} else {
				row.push('');
			}

			if (results[i].position && results[i].position.value) {
				row.push(results[i].position.value);
			} else {
				row.push('');
			}

			if (results[i].positionunit && results[i].positionunit.value) {
				row.push(results[i].positionunit.value);
			} else {
				row.push('');
			}

			if (results[i].height && results[i].height.value) {
				row.push(results[i].height.value);
			} else {
				row.push('');
			}

			if (results[i].heightunit && results[i].heightunit.value) {
				row.push(results[i].heightunit.value);
			} else {
				row.push('');
			}

			if (results[i].width && results[i].width.value) {
				row.push(results[i].width.value);
			} else {
				row.push('');
			}

			if (results[i].widthunit && results[i].widthunit.value) {
				row.push(results[i].widthunit.value);
			} else {
				row.push('');
			}

			if (results[i].area && results[i].area.value) {
				row.push(results[i].area.value);
			} else {
				row.push('');
			}

			if (results[i].areaunit && results[i].areaunit.value) {
				row.push(results[i].areaunit.value);
			} else {
				row.push('');
			}

			if (results[i].datacube && results[i].datacube.value) {
				row.push(results[i].datacube.value);
			} else {
				row.push('');
			}

			if (results[i].datacubetitle && results[i].datacubetitle.value) {
				row.push(results[i].datacubetitle.value);
			} else {
				row.push('');
			}

			rows.push(row);
			console.log("Parsed row: " + JSON.stringify(row));
		}
	} catch (error) {
		console.log("Error during preparation of rows: " + error);
		return Promise.reject();
	}
	return Promise.resolve(rows);
}

async function prepareColWidths(tabledata) {
	var columnWidths = [];
	try {
		var columnCount = 0;
		if (tabledata && tabledata[0]) {
			columnCount = tabledata[0].length;
		}

		var totalWidth = $(document).width() - 300;
		if (totalWidth <= 0) {
			totalWidth = $(document).width();
		}

		if (columnCount == 0) {
			return Promise.resolve([ totalWidth ]);
		}

		var columnWidth = parseInt(totalWidth / columnCount) - 5;
		if (columnWidth <= 5) {
			columnWidth = 5;
		}

		for (var i = 0; i < columnCount; i++) {
			columnWidths.push(columnWidth);
		}
	} catch (error) {
		console.log("Error during preparation of columns: " + error);
		return Promise.reject();
	}
	return Promise.resolve(columnWidths);
}

async function countProperties(o) {
	var c = 0;
	var property;
	for (property in o) {
		if (o.hasOwnProperty(property)) {
			c += 1;
		}
	}

	return Promise.resolve(c);
}

async function createCharts() {

	if (!datacubes && !datacubes[0]) {
		console.log("No datacubes found.");
		document.getElementById("chartdiv").style.display = "none";
		return Promise.resolve();
	}

	//reset charts
	var chartdiv = document.getElementById("chartdiv");
	while (chartdiv.firstChild) {
		chartdiv.removeChild(chartdiv.firstChild);
	}
	chartdiv.style.display = "block";
	
	try {
		for (var i = 0; i < datacubes.length; i++) {
			var chartNode = document.createElement("canvas");
			chartNode.classList.add("cubechart");
			var chartId = "cube " + i
			chartNode.id = chartId;
			document.getElementById("chartdiv").appendChild(chartNode);

			var ctx = document.getElementById(chartId).getContext('2d');

			var datasetLabel = datacubes[i].title
			if (datacubes[i].title != datacubes[i].id) {
				datasetLabel += " (" + datacubes[i].id + ")";
			}

			var cubeChart = new Chart(ctx, {
				type : 'line',
				data : {
					labels : datacubes[i].dimensionValues[0],
					datasets : [ {
						label : datasetLabel,
						fill : false,
						data : datacubes[i].measureValues[0],
						// lineTension: 0,
						pointRadius: 0,
						borderWidth: 1,
						borderColor: 'rgba(0,0,0,1.0)',
						cubicInterpolationMode : 'monotone'
					} ]
				},
				options : {
					responsive : true,
					maintainAspectRatio : false,
					tooltips : {
						mode : 'index',
						intersect : false,
						callbacks: {
			                label: function(tooltipItem, data) {
			                    var label = data.datasets[tooltipItem.datasetIndex].label || '';
			                    if (label) {
			                        label += ': ';
			                    }
			                    label += tooltipItem.yLabel;
			                    return label;
			                }
			            }
					},
					hover : {
						mode : 'nearest',
						intersect : true
					},
					scales : {
						xAxes : [ {
							display : true,
							scaleLabel : {
								display : true,
								labelString : "[ "	+ (datacubes[i].dimensionUnits[0] ? datacubes[i].dimensionUnits[0] : "undefined") + " ]"
							},
							ticks: {
			                    callback: function(value, index, values) {
			                        return Math.round(value * 100) / 100;
			                    }
			                }
						} ],
						yAxes : [ {
							ticks : {
								beginAtZero : true,
								callback: function(value, index, values) {
			                        return Math.round(value * 100) / 100;
			                    }
							},
							display : true,
							scaleLabel : {
								display : true,
								labelString : "[ "	+ (datacubes[i].measureUnits[0] ? datacubes[i].measureUnits[0] : "undefined") + " ]"
							}
						} ],
					},
					legend : {
						display : false,
						position : 'top'
					},
					title : {
						display : true,
						position : 'top',
						text : datasetLabel
					}
				}
			});
		}
	} catch (error) {
		console.log("Error during creation of charts: " + error);
		return Promise.reject();
	}

	return Promise.resolve();
}