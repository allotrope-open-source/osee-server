var initTreeData = [{
		"id": "1",
		"label": "How to get started",
		"text": "How to get started",
		"name": "<span class=\"nodelabel\">How to get started</span>",
		"color": "#FFCC00",
		"detail": "How to get started",
		"domains": ["INFORMATION", "INSTANCE"],
		"children": [{
				"id": "2",
				"label": "click on <Select data>",
				"text": "click on <Select data>",
				"name": "<span class=\"relationlabel\"><span class=\"relationpart\">click on </span><span class=\"objectpart\"><i class=\"fa fa-angle-left\"></i>Select data<i class=\"fa fa-angle-right\"></i></span></span>",
				"color": "#FFCC00",
				"detail": "click on <Select data>",
				"domains": ["INFORMATION", "INSTANCE"],
				"children": [{
						"id": "3",
						"label": "located at right border",
						"text": "located at right border",
						"name": "<span class=\"literallabel\">located at \"<span class=\"literalpart\">right border</span>\"^^xsd:string</span>",
						"color": "#ffffff",
						"detail": "located at right border",
						"domains": ["INFORMATION", "INSTANCE"],
						"children": [{
								"id": "4",
								"label": "select data files",
								"text": "select data files",
								"name": "<span class=\"literallabel\">select \"<span class=\"literalpart\">data files</span>\"^^xsd:string</span>",
								"color": "#ffffff",
								"detail": "Select data files",
								"domains": ["INFORMATION", "INSTANCE"],
								"children": [{
										"id": "6",
										"label": "can be turtle files (.ttl)",
										"text": "can be turtle files (.ttl)",
										"name": "<span class=\"literallabel\">can be \"<span class=\"literalpart\">turtle files (.ttl)</span>\"^^xsd:string</span>",
										"color": "#ffffff",
										"detail": "can be turtle files (.ttl)",
										"domains": ["INFORMATION", "INSTANCE"],
										"children": [],
										"literals": null,
										"relations": null
									}, {
										"id": "7",
										"label": "can be Allotrope data format (.adf)",
										"text": "can be Allotrope data format (.adf)",
										"name": "<span class=\"literallabel\">can be \"<span class=\"literalpart\">Allotrope data format (.adf)</span>\"^^xsd:string</span>",
										"color": "#ffffff",
										"detail": "can be Allotrope data format (.adf)",
										"domains": ["INFORMATION", "INSTANCE"],
										"children": [],
										"literals": null,
										"relations": null
									}, {
										"id": "9",
										"label": "push button See!",
										"text": "push button See!",
										"name": "<span class=\"literallabel\">push \"<span class=\"literalpart\">button See!</span>\"^^xsd:string</span>",
										"color": "#ffffff",
										"detail": "push button See!",
										"domains": ["INFORMATION", "INSTANCE"],
										"children": [{
												"id": "11",
												"label": "to create visualization",
												"text": "to create visualization",
												"name": "<span class=\"literallabel\">to create \"<span class=\"literalpart\">visualization</span>\"^^xsd:string</span>",
												"color": "#ffffff",
												"detail": "to create visualization",
												"domains": ["INFORMATION", "INSTANCE"],
												"children": [],
												"literals": null,
												"relations": null
											}
										],
										"literals": null,
										"relations": null
									}, {
										"id": "8",
										"label": "if needed select further files",
										"text": "if needed select further files",
										"name": "<span class=\"literallabel\">if needed select \"<span class=\"literalpart\">further files</span>\"^^xsd:string</span>",
										"color": "#ffffff",
										"detail": "if needed select further files",
										"domains": ["INFORMATION", "INSTANCE"],
										"children": [{
											"id": "15",
											"label": "loaded as vocabulary",
											"text": "loaded as vocabulary",
											"name": "<span class=\"literallabel\">loaded as \"<span class=\"literalpart\">vocabulary</span>\"^^xsd:string</span>",
											"color": "#ffffff",
											"detail": "loaded as vocabulary",
											"domains": ["INFORMATION", "INSTANCE"],
											"children": [{
													"id": "16",
													"label": "must be turtle files (.ttl)",
													"text": "must be turtle files (.ttl)",
													"name": "<span class=\"literallabel\">must be \"<span class=\"literalpart\">turtle files (.ttl)</span>\"^^xsd:string</span>",
													"color": "#ffffff",
													"detail": "must be turtle files (.ttl)",
													"domains": ["INFORMATION", "INSTANCE"],
													"children": [],
													"literals": null,
													"relations": null
												}
											],
											"literals": null,
											"relations": null
										}, {
												"id": "13",
												"label": "push button See!",
												"text": "push button See!",
												"name": "<span class=\"literallabel\">push \"<span class=\"literalpart\">button See!</span>\"^^xsd:string</span>",
												"color": "#ffffff",
												"detail": "push button See!",
												"domains": ["INFORMATION", "INSTANCE"],
												"children": [{
														"id": "14",
														"label": "to create visualization",
														"text": "to create visualization",
														"name": "<span class=\"literallabel\">to create \"<span class=\"literalpart\">visualization</span>\"^^xsd:string</span>",
														"color": "#ffffff",
														"detail": "to create visualization",
														"domains": ["INFORMATION", "INSTANCE"],
														"children": [],
														"literals": null,
														"relations": null
													}
												],
												"literals": null,
												"relations": null
											} ],
									"literals": null,
									"relations": null
									} 
								],
								"literals": null,
								"relations": null
							}, {
								"id": "5",
								"label": "select <Try example>",
								"text": "select <Try example>",
								"name": "<span class=\"literallabel\">select \"<span class=\"literalpart\">Try example</span>\"^^xsd:string</span>",
								"color": "#ffffff",
								"detail": "select <Try example>",
								"domains": ["INFORMATION", "INSTANCE"],
								"children": [{
										"id": "10",
										"label": "push button See!",
										"text": "push button See!",
										"name": "<span class=\"literallabel\">push \"<span class=\"literalpart\">button See!</span>\"^^xsd:string</span>",
										"color": "#ffffff",
										"detail": "push button See!",
										"domains": ["INFORMATION", "INSTANCE"],
										"children": [{
												"id": "12",
												"label": "to create visualization",
												"text": "to create visualization",
												"name": "<span class=\"literallabel\">to create \"<span class=\"literalpart\">visualization</span>\"^^xsd:string</span>",
												"color": "#ffffff",
												"detail": "to create visualization",
												"domains": ["INFORMATION", "INSTANCE"],
												"children": [],
												"literals": null,
												"relations": null
											}
										],
										"literals": null,
										"relations": null
									}
								],
								"literals": null,
								"relations": null
							}
						]
					}
				],
				"literals": null,
				"relations": null
			}
		],
		"literals": null,
		"relations": null
	}
];
