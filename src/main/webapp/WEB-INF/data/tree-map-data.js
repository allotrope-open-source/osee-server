var initTreeData = {
	"name": "How to get started ",
	"children": [
		{
			"name": "click on Select data ",
			"children": [
				{
					"name": "located at right border ",
				}
			]
		}, 
		{
			"name": "select data files ",
			"children": [
				{
					"name": "can be turtle files (.ttl) ",
				}, 
				{
					"name": "can be Allotrope data format (.adf) ",
				}
			]
		},
		{
			"name": "push button See! ",
			"children": [
				{
					"name": "to create visualization ",
				}
			]
		}, 
		{
			"name": "if needed select further files ",
			"children": [
				{
					"name": "loaded as vocabulary ",
					"children": [
						{
							"name": "must be turtle files (.ttl) ",
						}
					]
				}
			]
		},
		{
			"name": "push button See! ",
			"children": [
				{
					"name": "to create visualization ",
				}
			]
		}, 
		{
			"name": "select <Try example> ",
		}
	]
};	