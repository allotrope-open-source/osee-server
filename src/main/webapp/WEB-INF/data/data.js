var	initData =
			[ {
		                "data": {
		                    "id": "1",
		                    "label": "How to get started",
		                    "detail": "How to get started",
		                    "visibility": "visible",
		                    "parent": null,
		                    "bg": "#d78df4",
		                    "shape": "rectangle",
		                    "popup": "<br/>How to get started<br/><br/>",
		                    "borderwidth": "2px"
		                },
		                "position": {
		                    "x": 287.09999976158144,
		                    "y": 20
		                },
		                "group": "nodes",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            },
		            {
		                "data": {
		                    "id": "2",
		                    "label": "\"Select data\"",
		                    "detail": "\"Select data\"",
		                    "visibility": "visible",
		                    "parent": null,
		                    "bg": "#968df4",
		                    "shape": "rectangle",
		                    "popup": "<br/>Select data<br/><br/>",
		                    "borderwidth": "2px"
		                },
		                "position": {
		                    "x": 294.59999976158144,
		                    "y": 65.99999999999999
		                },
		                "group": "nodes",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            },
		            {
		                "data": {
		                    "id": "3",
		                    "label": "right border",
		                    "detail": "right border",
		                    "visibility": "visible",
		                    "parent": null,
		                    "bg": "#8dabf4",
		                    "shape": "rectangle",
		                    "popup": "<br/>right border<br/><br/>",
		                    "borderwidth": "2px"
		                },
		                "position": {
		                    "x": 305.09999976158144,
		                    "y": 111.99999999999997
		                },
		                "group": "nodes",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            },
		            {
		                "data": {
		                    "id": "4",
		                    "label": "data file",
		                    "detail": "data file",
		                    "visibility": "visible",
		                    "parent": null,
		                    "bg": "#8dc1f4",
		                    "shape": "rectangle",
		                    "popup": "<br/>data file<br/><br/>",
		                    "borderwidth": "2px"
		                },
		                "position": {
		                    "x": 242.79855355129098,
		                    "y": 167.14794167475117
		                },
		                "group": "nodes",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            },
		            {
		                "data": {
		                    "id": "5",
		                    "label": "Turtle format (.ttl)",
		                    "detail": "Turtle format (.ttl)",
		                    "visibility": "visible",
		                    "parent": null,
		                    "bg": "#8dd2f4",
		                    "shape": "rectangle",
		                    "popup": "<br/>Turtle format (.ttl)<br/><br/>",
		                    "borderwidth": "2px"
		                },
		                "position": {
		                    "x": -9.924145585820133,
		                    "y": 264.54613090190685
		                },
		                "group": "nodes",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            },
		            {
		                "data": {
		                    "id": "6",
		                    "label": "Allotrope data format (.adf)",
		                    "detail": "Allotrope data format (.adf)",
		                    "visibility": "visible",
		                    "parent": null,
		                    "bg": "#8de0f4",
		                    "shape": "rectangle",
		                    "popup": "<br/>Allotrope data format (.adf)<br/><br/>",
		                    "borderwidth": "2px"
		                },
		                "position": {
		                    "x": -52.384773275683486,
		                    "y": 221.97917446802595
		                },
		                "group": "nodes",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            },
		            {
		                "data": {
		                    "id": "7",
		                    "label": "further files",
		                    "detail": "further files",
		                    "visibility": "visible",
		                    "parent": null,
		                    "bg": "#8df4f4",
		                    "shape": "rectangle",
		                    "popup": "<br/>3. further files<br/><br/>",
		                    "borderwidth": "2px"
		                },
		                "position": {
		                    "x": 174.4287688421777,
		                    "y": 260.81685137347705
		                },
		                "group": "nodes",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            },
		            {
		                "data": {
		                    "id": "8",
		                    "label": "vocabulary",
		                    "detail": "vocabulary",
		                    "visibility": "visible",
		                    "parent": null,
		                    "bg": "#8df4e0",
		                    "shape": "rectangle",
		                    "popup": "<br/>vocabulary<br/><br/>",
		                    "borderwidth": "2px"
		                },
		                "position": {
		                    "x": 78.77754981886541,
		                    "y": 335.50213533541273
		                },
		                "group": "nodes",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            },
		            {
		                "data": {
		                    "id": "9",
		                    "label": "Try example",
		                    "detail": "Try exmple",
		                    "visibility": "visible",
		                    "parent": null,
		                    "bg": "#8df4d0",
		                    "shape": "rectangle",
		                    "popup": "<br/>Try example<br/><br/>",
		                    "borderwidth": "2px"
		                },
		                "position": {
		                    "x": 432.43481550720566,
		                    "y": 172.1223576649675
		                },
		                "group": "nodes",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            },
		            {
		                "data": {
		                    "id": "10",
		                    "label": "See!",
		                    "detail": "See!",
		                    "visibility": "visible",
		                    "parent": null,
		                    "bg": "#8df4c1",
		                    "shape": "rectangle",
		                    "popup": "<br/>See!<br/><br/>",
		                    "borderwidth": "2px"
		                },
		                "position": {
		                    "x": 362.15916328719027,
		                    "y": 311.43966160051366
		                },
		                "group": "nodes",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            },
		            {
		                "data": {
		                    "id": "11",
		                    "label": "visualization",
		                    "detail": "visualization",
		                    "visibility": "visible",
		                    "parent": null,
		                    "bg": "#8df4b1",
		                    "shape": "rectangle",
		                    "popup": "<br/>visualization<br/><br/>",
		                    "borderwidth": "2px"
		                },
		                "position": {
		                    "x": 365.6558530779,
		                    "y": 376.4155263360866
		                },
		                "group": "nodes",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            },
		            {
		                "data": {
		                    "id": "1-2",
		                    "label": "click on",
		                    "detail": "click on",
		                    "visibility": "visible",
		                    "source": "1",
		                    "target": "2",
		                    "curveStyle": "bezier",
		                    "lineWidth": "2",
		                    "lineColor": "#000000",
		                    "color": "#a6a6a8"
		                },
		                "position": {},
		                "group": "edges",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            },
		            {
		                "data": {
		                    "id": "2-3",
		                    "label": "located at",
		                    "detail": "located at",
		                    "visibility": "visible",
		                    "source": "2",
		                    "target": "3",
		                    "curveStyle": "bezier",
		                    "lineWidth": "2",
		                    "lineColor": "#000000",
		                    "color": "#a6a6a8"
		                },
		                "position": {},
		                "group": "edges",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            },
		            {
		                "data": {
		                    "id": "3-4",
		                    "label": "select",
		                    "detail": "select",
		                    "visibility": "visible",
		                    "source": "3",
		                    "target": "4",
		                    "curveStyle": "bezier",
		                    "lineWidth": "2",
		                    "lineColor": "#000000",
		                    "color": "#a6a6a8"
		                },
		                "position": {},
		                "group": "edges",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            },
		            {
		                "data": {
		                    "id": "4-5",
		                    "label": "can be",
		                    "detail": "can be",
		                    "visibility": "visible",
		                    "source": "4",
		                    "target": "5",
		                    "curveStyle": "bezier",
		                    "lineWidth": "2",
		                    "lineColor": "#000000",
		                    "color": "#a6a6a8"
		                },
		                "position": {},
		                "group": "edges",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            },
		            {
		                "data": {
		                    "id": "4-6",
		                    "label": "can be",
		                    "detail": "can be",
		                    "visibility": "visible",
		                    "source": "4",
		                    "target": "6",
		                    "curveStyle": "bezier",
		                    "lineWidth": "2",
		                    "lineColor": "#000000",
		                    "color": "#a6a6a8"
		                },
		                "position": {},
		                "group": "edges",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            },
		            {
		                "data": {
		                    "id": "4-7",
		                    "label": "select if needed",
		                    "detail": "select if needed",
		                    "visibility": "visible",
		                    "source": "4",
		                    "target": "7",
		                    "curveStyle": "bezier",
		                    "lineWidth": "2",
		                    "lineColor": "#000000",
		                    "color": "#a6a6a8"
		                },
		                "position": {},
		                "group": "edges",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            },
		            {
		                "data": {
		                    "id": "7-8",
		                    "label": "loaded as",
		                    "detail": "loaded as",
		                    "visibility": "visible",
		                    "source": "7",
		                    "target": "8",
		                    "curveStyle": "bezier",
		                    "lineWidth": "2",
		                    "lineColor": "#000000",
		                    "color": "#a6a6a8"
		                },
		                "position": {},
		                "group": "edges",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            },
		            {
		                "data": {
		                    "id": "8-5",
		                    "label": "must be",
		                    "detail": "must be",
		                    "visibility": "visible",
		                    "source": "8",
		                    "target": "5",
		                    "curveStyle": "bezier",
		                    "lineWidth": "2",
		                    "lineColor": "#000000",
		                    "color": "#a6a6a8"
		                },
		                "position": {},
		                "group": "edges",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            },
		            {
		                "data": {
		                    "id": "3-9",
		                    "label": "or select",
		                    "detail": "or select",
		                    "visibility": "visible",
		                    "source": "3",
		                    "target": "9",
		                    "curveStyle": "bezier",
		                    "lineWidth": "2",
		                    "lineColor": "#000000",
		                    "color": "#a6a6a8"
		                },
		                "position": {},
		                "group": "edges",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            },
		            {
		                "data": {
		                    "id": "9-10",
		                    "label": "push",
		                    "detail": "push",
		                    "visibility": "visible",
		                    "source": "9",
		                    "target": "10",
		                    "curveStyle": "bezier",
		                    "lineWidth": "2",
		                    "lineColor": "#000000",
		                    "color": "#a6a6a8"
		                },
		                "position": {},
		                "group": "edges",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            },
		            {
		                "data": {
		                    "id": "4-10",
		                    "label": "push",
		                    "detail": "push",
		                    "visibility": "visible",
		                    "source": "4",
		                    "target": "10",
		                    "curveStyle": "bezier",
		                    "lineWidth": "2",
		                    "lineColor": "#000000",
		                    "color": "#a6a6a8"
		                },
		                "position": {},
		                "group": "edges",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            },
		            {
		                "data": {
		                    "id": "7-10",
		                    "label": "push",
		                    "detail": "push",
		                    "visibility": "visible",
		                    "source": "7",
		                    "target": "10",
		                    "curveStyle": "bezier",
		                    "lineWidth": "2",
		                    "lineColor": "#000000",
		                    "color": "#a6a6a8"
		                },
		                "position": {},
		                "group": "edges",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            },
		            {
		                "data": {
		                    "id": "10-11",
		                    "label": "to create",
		                    "detail": "to create",
		                    "visibility": "visible",
		                    "source": "10",
		                    "target": "11",
		                    "curveStyle": "bezier",
		                    "lineWidth": "2",
		                    "lineColor": "#000000",
		                    "color": "#a6a6a8"
		                },
		                "position": {},
		                "group": "edges",
		                "removed": false,
		                "selected": false,
		                "selectable": true,
		                "locked": false,
		                "grabbable": true,
		                "classes": "l1"
		            }
		        ];