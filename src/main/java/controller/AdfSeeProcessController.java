/*******************************************************************
*               Notice
*
* Copyright Osthus GmbH, All rights reserved.
*
* This software is part of the Allotrope ADM project
*
* Address: Osthus GmbH
*        : Eisenbahnweg 9
*        : 52068 Aachen
*        : Germany
*
*******************************************************************/

package controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jfree.util.Log;
import org.jutils.jprocesses.JProcesses;
import org.jutils.jprocesses.model.ProcessInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeroturnaround.exec.ProcessExecutor;
import org.zeroturnaround.exec.StartedProcess;

import adfsee.TempFolderCreator;
import processor.util.RunUtil;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 */
public class AdfSeeProcessController
{
	public static Logger logger = LoggerFactory.getLogger("log");

	public static void run()
	{
		keepControllerRunningInNewThread();
		startAdfSeeStatusMessageThread();
	}

	private static void startUp()
	{
		Log.info("Starting up AdfSee process controller service.");

		try
		{
			boolean isDebug = java.lang.management.ManagementFactory.getRuntimeMXBean().getInputArguments().toString().indexOf("-agentlib:jdwp") > 0;

			ProcessExecutor processExecutor = new ProcessExecutor();
			if (isDebug)
			{
				processExecutor.command("java", "-jar", //
						"-Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5006", //
						TempFolderCreator.CONTROLLER_FOLDER_PATH_NAME + "/adfsee-controller.jar", TempFolderCreator.CONTROLLER_FOLDER_PATH_NAME, "fuseki-server.jar", "1000");
			}
			else
			{
				processExecutor.command("java", "-jar", //
						TempFolderCreator.CONTROLLER_FOLDER_PATH_NAME + "/adfsee-controller.jar", TempFolderCreator.CONTROLLER_FOLDER_PATH_NAME, "fuseki-server.jar", "1000");
			}

			StartedProcess adfSeeControllerProcess = processExecutor //
					.redirectOutput(System.out) //
					.redirectError(System.err) //
					.directory(TempFolderCreator.CONTROLLER_FOLDER_PATH.toFile()) //
					.start();
		}
		catch (IOException e)
		{
			logger.info("Error during startup of ADFSee controller service.");
			e.printStackTrace();
		}
	}

	public static void keepControllerRunningInNewThread()
	{
		try
		{
			class ADFSeeControllerLoaderThread extends Thread
			{
				public static final String controllerFile = "controller.status.txt";
				public static final String controllerMessage = "controller has stopped";

				@Override
				public void run()
				{
					AdfSeeProcessController adfSeeProcessController = new AdfSeeProcessController();
					AdfSeeProcessController.startUp();
					try
					{
						Thread.sleep(1000);
					}
					catch (InterruptedException e2)
					{
						e2.printStackTrace();
						logger.info("Error during first start of adfsee controller. Interrupting loader thread " + Thread.currentThread().getName());
						Thread.currentThread().interrupt();
					}

					while (!Thread.currentThread().isInterrupted())
					{
						int counter = 1;
						int maxCounter = 5;
						Path controllerStatusPath = Paths.get(TempFolderCreator.CONTROLLER_FOLDER_PATH_NAME, controllerFile);
						while (counter <= maxCounter)
						{
							try
							{
								Thread.sleep(1234);
							}
							catch (InterruptedException e1)
							{
								e1.printStackTrace();
								logger.info("Error during monitoring adfsee controller. Interrupting monitoring thread " + Thread.currentThread().getName());
								Thread.currentThread().interrupt();
							}
							logger.debug("Monitoring adfsee controller service...");
							if (Files.exists(controllerStatusPath))
							{
								try
								{
									List<String> lines = Files.readAllLines(controllerStatusPath);
									if (!lines.isEmpty())
									{
										String currentControllerStatus = lines.get(0);
										if (currentControllerStatus.trim().equals(controllerMessage))
										{
											logger.info("Status file says adfsee controller has stopped: " + controllerStatusPath.toAbsolutePath().toString() + " Check " + counter + "/" + maxCounter);
											counter++;
											continue;
										}
										else
										{
											counter = 1;
										}
									}
									else
									{
										logger.info(
												"Error reading adfsee controller status from file: " + controllerStatusPath.toAbsolutePath().toString() + " Retrying..." + counter + "/" + maxCounter);
										counter++;
										continue;
									}
								}
								catch (Exception innerException)
								{
									logger.info("Error reading adfsee controller status from file: " + controllerStatusPath.toAbsolutePath().toString() + " Retrying..." + counter + "/" + maxCounter);
									counter++;
									continue;
								}
							}
							try
							{
								Files.write(controllerStatusPath, controllerMessage.getBytes());
								logger.debug(controllerMessage + "? " + controllerStatusPath.toAbsolutePath().toString());
							}
							catch (Exception e)
							{
								logger.info("Error writing controller status message to file: " + controllerStatusPath.toAbsolutePath().toString() + " " + counter + "/" + maxCounter);
								e.printStackTrace();
								counter++;
							}
						}

						logger.info("ADFSee controller service is not responding. Stopping and restarting controller service.");

						logger.info("Stopping controller service...");
						boolean hasStopped = true;
						ProcessInfo controllerProcessInfo = RunUtil.getProcessInfo("adfsee-controller.jar");

						if (controllerProcessInfo != null && StringUtils.isNotEmpty(controllerProcessInfo.getPid()))
						{
							int pid = Integer.valueOf(controllerProcessInfo.getPid());
							boolean success = JProcesses.killProcessGracefully(pid).isSuccess();
							if (!success)
							{
								logger.info("Could not stop controller process " + pid + " gracefully.");
								success = JProcesses.killProcess(pid).isSuccess();
								if (!success)
								{
									logger.info("Could not kill controller process " + pid + ". Manual stop is required (e.g. via task manager)");
									hasStopped = false;
								}
								else
								{
									logger.info("Controller process " + pid + " was killed.");
								}
							}
							else
							{
								logger.info("Controller process " + pid + " was stopped gracefully.");
							}
						}
						else
						{
							logger.info("No running controller process found.");
						}

						if (hasStopped)
						{
							logger.info("Restarting controller service...");
							AdfSeeProcessController.startUp();
						}
					}
				}
			}

			ADFSeeControllerLoaderThread loaderThread = new ADFSeeControllerLoaderThread();
			loaderThread.start();
		}
		catch (Exception e)
		{
			logger.info("Error during loader thread for ADFSee controller service.");
			e.printStackTrace();
		}

	}

	private static void startAdfSeeStatusMessageThread()
	{
		try
		{
			class AdfSeeStatusMessageThread extends Thread
			{
				public static final String adfSeeFile = "adfsee.status.txt";
				public static final String adfSeeMessage = "adfsee is alive";

				@Override
				public void run()
				{
					Path adfSeeStatusPath = Paths.get(TempFolderCreator.CONTROLLER_FOLDER_PATH_NAME, adfSeeFile);
					logger.debug("Writing adfsee status messages to file: " + adfSeeStatusPath.toAbsolutePath().toString() + " in thread " + Thread.currentThread().getId());
					try
					{
						Files.deleteIfExists(adfSeeStatusPath);
					}
					catch (IOException e)
					{
						logger.info("Cannot delete adfsee log file: " + adfSeeStatusPath.toAbsolutePath().toString());
						e.printStackTrace();
					}

					while (!Thread.currentThread().isInterrupted())
					{
						try
						{
							Files.write(adfSeeStatusPath, adfSeeMessage.getBytes());
							logger.debug(adfSeeMessage + " " + adfSeeStatusPath.toAbsolutePath().toString());
						}
						catch (Exception e)
						{
							logger.info("Error writing status message to file: " + adfSeeStatusPath.toAbsolutePath().toString());
							e.printStackTrace();
						}
						try
						{
							Thread.sleep(1200);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
							logger.info("Error waiting for writing next status message to file: " + adfSeeStatusPath.toAbsolutePath().toString());
						}
					}

					logger.info("Thread for writing adfsee status messages has stopped.");

				}
			}

			AdfSeeStatusMessageThread adfSeeStatusMessageThread = new AdfSeeStatusMessageThread();
			adfSeeStatusMessageThread.start();
		}
		catch (Exception e)
		{
			logger.info("Error during thread for printing ADFSee status messages.");
			e.printStackTrace();
		}

	}

}
