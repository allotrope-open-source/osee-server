package query;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.query.Syntax;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.logging.log4j.Logger;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class QueryManager
{
	public static Map<String, String> execute(Model model, Model externalResources)
	{
		Model fullModel = createFullModel(model, externalResources);
		Map<String, String> queryResults = new HashMap<String, String>();
		String queryForPeakTableInSuperDomainResult = query(SparqlQuery.peakTableSuperDomainQueryV01, fullModel);
		if (queryForPeakTableInSuperDomainResult.isEmpty() || queryForPeakTableInSuperDomainResult.length() <= 261)
		{
			queryForPeakTableInSuperDomainResult = query(SparqlQuery.peakTableSuperDomainQueryV02, fullModel);
		}
		if (queryForPeakTableInSuperDomainResult.isEmpty() || queryForPeakTableInSuperDomainResult.length() <= 261)
		{
			queryForPeakTableInSuperDomainResult = query(SparqlQuery.peakTableSuperDomainQueryV03, fullModel);
		}
		if (queryForPeakTableInSuperDomainResult.isEmpty() || queryForPeakTableInSuperDomainResult.length() <= 261)
		{
			queryForPeakTableInSuperDomainResult = query(SparqlQuery.peakTableQueryV01, fullModel);
		}
		queryResults.put("peak-table-super-domain", queryForPeakTableInSuperDomainResult);
		return queryResults;
	}

	private static Model createFullModel(Model model, Model externalResources)
	{
		Model fullModel = ModelFactory.createDefaultModel();
		fullModel.add(model);

		if (externalResources != null && !externalResources.isEmpty())
		{
			fullModel.add(externalResources);
		}
		return fullModel;
	}

	public static String query(String queryString, Model model)
	{
		String resultString = StringUtils.EMPTY;
		Query query = QueryFactory.create(queryString, Syntax.syntaxSPARQL_11);
		try (QueryExecution queryExecution = QueryExecutionFactory.create(query, model))
		{
			ResultSet results = queryExecution.execSelect();
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			ResultSetFormatter.outputAsJSON(outputStream, results);
			resultString = new String(outputStream.toByteArray());
			System.out.println(resultString);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return resultString;
	}

	public static Model constructGraph(String constructQuery, Model model, Model externalResources, Logger log)
	{
		try
		{
			Model fullModel = null;
			if (externalResources != null && !externalResources.isEmpty())
			{
				fullModel = ModelFactory.createDefaultModel();
				fullModel.add(model);
				fullModel.add(externalResources);
			}
			else
			{
				fullModel = model;
			}

			Query query = QueryFactory.create(constructQuery);
			QueryExecution qe = QueryExecutionFactory.create(query, fullModel);
			qe.setTimeout(60000);
			log.info("Executing construct query: " + ((constructQuery != null && !constructQuery.isEmpty()) ? "\"" + constructQuery + "\"" : ""));
			Model constructedModel = qe.execConstruct();
			if (constructedModel != null && constructedModel.size() > 0)
			{
				model = constructedModel.difference(externalResources);
				log.info("Constructed " + model.size() + " triples.");
			}
			else
			{
				log.info("No triples constructed. Using default model.");
			}
		}
		catch (Exception e)
		{
			log.error("Error during construct query: " + ((constructQuery != null && !constructQuery.isEmpty()) ? "\"" + constructQuery + "\"" : ""));
			throw e;
		}
		return model;
	}

}
