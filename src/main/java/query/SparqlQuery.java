package query;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class SparqlQuery
{
	public static final String injectionQuery = read("injection.rq");

	public static final String hplcQuery = read("hplc.rq");

	public static final String processQuery = read("process.rq");

	public static final String deviceQuery = read("device.rq");

	public static final String materialQuery = read("material.rq");

	public static final String injectionInstancesQuery = read("injection-instances.rq");

	public static final String hplcInstancesQuery = read("hplc-instances.rq");

	public static final String processInstancesQuery = read("process-instances.rq");

	public static final String deviceInstancesQuery = read("device-instances.rq");

	public static final String materialInstancesQuery = read("material-instances.rq");

	public static final String peakTableSuperDomainQueryV01 = read("peak-table-super-domain-v01.rq");

	public static final String peakTableSuperDomainQueryV02 = read("peak-table-super-domain-v02.rq");

	public static final String peakTableSuperDomainQueryV03 = read("peak-table-super-domain-v03.rq");

	public static final String peakTableQueryV01 = read("peak-table-v01.rq");

	public static final String constructModelFromGraphTemplate = read("construct-model-from-named-graph.rq");

	private static String read(String filename)
	{
		InputStream inStream = SparqlQuery.class.getResourceAsStream(filename);
		Reader reader = new InputStreamReader(inStream, StandardCharsets.UTF_8);
		BufferedReader bf = new BufferedReader(reader);
		StringBuilder sb = new StringBuilder();
		String line;
		try
		{
			while ((line = bf.readLine()) != null)
			{
				sb.append(line + "\n");
			}
			bf.close();
			reader.close();
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
		return sb.toString();
	}
}
