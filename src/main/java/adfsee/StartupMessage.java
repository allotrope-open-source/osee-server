package adfsee;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.stereotype.Component;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
@Component
@PropertySources({ //
		@PropertySource(value = "classpath:application.properties"), //
		@PropertySource(value = "file:src/main/resources/application.properties", ignoreResourceNotFound = true) //
})
public class StartupMessage
{
	@Value("${server.port}")
	String serverPort;

	private Logger logger = LoggerFactory.getLogger("log");

	public void out()
	{
		message();
	}

	private void message()
	{
		//@formatter:off
		logger.info("\r\n" //
				+ "    ___    ____  ___________              _         ____  __  ___   ___   _______   ________\r\n" +
				"   /   |  / __ \\/ ____/ ___/___  ___     (_)____   / __ \\/ / / / | / / | / /  _/ | / / ____/\r\n" +
				"  / /| | / / / / /_   \\__ \\/ _ \\/ _ \\   / / ___/  / /_/ / / / /  |/ /  |/ // //  |/ / / __  \r\n" +
				" / ___ |/ /_/ / __/  ___/ /  __/  __/  / (__  )  / _, _/ /_/ / /|  / /|  // // /|  / /_/ /  \r\n" +
				"/_/  |_/_____/_/    /____/\\___/\\___/  /_/____/  /_/ |_|\\____/_/ |_/_/ |_/___/_/ |_/\\____/   \r\n" +
				"                                                                                            \r\n" +
				"");
		logger.info("\r\nNow open up http://localhost:" + serverPort + " in your CHROME internet browser.\n");
		//@formatter:on
	}
}
