package adfsee;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import controller.AdfSeeProcessController;
import fuseki.FusekiTripleStore;
import storage.IStorageService;
import storage.StorageProperties;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
@PropertySources({ //
		@PropertySource(value = "classpath:application.properties"), //
		@PropertySource(value = "classpath:processor.properties") //
})
@ComponentScan({ "adfsee", "storage", "restcontroller", "data" })
@EntityScan({ "adfsee", "storage", "restcontroller", "data" })
public class AdfSeeMain
{
	private static StartupMessage startupMessage;
	private static TempFolderCreator tempFolderCreator;
	public static boolean useTripleStore = false;
	public static Logger logger = LoggerFactory.getLogger("log");

	@Autowired
	public AdfSeeMain(StartupMessage startupMessage, TempFolderCreator tempFolderCreator)
	{
		AdfSeeMain.startupMessage = startupMessage;
		AdfSeeMain.tempFolderCreator = tempFolderCreator;
	}

	public static void main(String[] args) throws IOException
	{
		parseArgs(args);
		SpringApplication.run(AdfSeeMain.class, args);
		tempFolderCreator.create();
		if (useTripleStore)
		{
			logger.info("Starting services for triple store.");
			FusekiTripleStore.startInNewThread();
			AdfSeeProcessController.run();
		}
		startupMessage.out();
	}

	private static void parseArgs(String[] args)
	{
		for (int i = 0; i < args.length; i++)
		{
			String string = args[i];
			if (string.equalsIgnoreCase("triplestore"))
			{
				useTripleStore = true;
			}
		}
	}

	@Bean
	CommandLineRunner init(IStorageService storageService)
	{
		return (args) -> {
			storageService.deleteAll();
			storageService.init();
		};
	}
}
