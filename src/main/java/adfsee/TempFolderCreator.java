package adfsee;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import processor.util.ResourceExtractor;
import storage.StorageProperties;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
@Component
public class TempFolderCreator
{
	public static String UPLOAD_FOLDER = StorageProperties.location;
	public static String DOWNLOAD_FOLDER = StorageProperties.downloadLocation;

	public static Path UPLOAD_FOLDER_PATH = null;
	public static Path DOWNLOAD_FOLDER_PATH = null;

	public static String UPLOAD_FOLDER_PATH_NAME = null;
	public static String DOWNLOAD_FOLDER_PATH_NAME = null;

	public static Path DOT_FOLDER_PATH = null;
	public static String DOT_FOLDER_PATH_NAME = null;

	public static Path TRIPLE_STORE_FOLDER_PATH = null;
	public static String TRIPLE_STORE_FOLDER_PATH_NAME = null;

	public static Path CONTROLLER_FOLDER_PATH = null;
	public static String CONTROLLER_FOLDER_PATH_NAME = null;

	public static String SPACE_REPLACE = "_SPACE_";

	private Logger logger = LoggerFactory.getLogger("log");
	private static final String TRIPLE_STORE = "triplestore";
	private static final String CONTROLLER = "controller";

	public void create() throws IOException
	{
		UPLOAD_FOLDER_PATH = Files.createTempDirectory(UPLOAD_FOLDER);
		UPLOAD_FOLDER_PATH_NAME = UPLOAD_FOLDER_PATH.toAbsolutePath().toString() + File.separator;
		logger.info("Created temporary file upload folder at " + UPLOAD_FOLDER_PATH_NAME);

		DOWNLOAD_FOLDER_PATH = Files.createTempDirectory(DOWNLOAD_FOLDER);
		DOWNLOAD_FOLDER_PATH_NAME = DOWNLOAD_FOLDER_PATH.toAbsolutePath().toString() + File.separator;
		logger.info("Created temporary file download folder at " + DOWNLOAD_FOLDER_PATH_NAME);

		DOT_FOLDER_PATH = Files.createTempDirectory("dot");
		DOT_FOLDER_PATH_NAME = DOT_FOLDER_PATH.toAbsolutePath().toString() + File.separator;

		if (SystemUtils.IS_OS_WINDOWS)
		{
			ResourceExtractor.extract("graphviz", DOT_FOLDER_PATH_NAME);
		}
		else if (SystemUtils.IS_OS_MAC)
		{
			ResourceExtractor.extract("graphviz-2.14-macos", DOT_FOLDER_PATH_NAME);
		}
		else
		{
			logger.info("Could not detect operating system. Using default windows os.");
			ResourceExtractor.extract("graphviz", DOT_FOLDER_PATH_NAME);
		}
		logger.info("Created graphviz dot binary folder at " + DOT_FOLDER_PATH_NAME);

		if (AdfSeeMain.useTripleStore)
		{
			createDefinedTempFolder(TRIPLE_STORE);
			createDefinedTempFolder(CONTROLLER);
		}
	}

	private void createDefinedTempFolder(String name) throws IOException
	{
		Path tempFolder = Files.createTempDirectory(name);
		Path definedPath = Paths.get(tempFolder.toAbsolutePath().toString().replaceAll(name + ".*$", name));
		Path path = null;
		String pathName = StringUtils.EMPTY;
		if (!Files.exists(definedPath, LinkOption.NOFOLLOW_LINKS))
		{
			path = Files.createDirectory(definedPath);
			pathName = path.toAbsolutePath().toString() + File.separator;
			ResourceExtractor.extract(name, pathName);
			logger.info("Created " + name + " folder at " + pathName);
		}
		else
		{
			path = definedPath;
			pathName = path.toAbsolutePath().toString() + File.separator;
			logger.info("Reusing " + name + " folder found at " + pathName);
		}
		setStaticVariables(name, path, pathName);
	}

	private void setStaticVariables(String name, Path path, String pathName)
	{
		if (name.equals(TRIPLE_STORE))
		{
			TRIPLE_STORE_FOLDER_PATH = path;
			TRIPLE_STORE_FOLDER_PATH_NAME = pathName;
		}
		else if (name.equals(CONTROLLER))
		{
			CONTROLLER_FOLDER_PATH = path;
			CONTROLLER_FOLDER_PATH_NAME = pathName;
		}
		else
		{
			Log.error("Unhandled temp folder type: " + name);
		}
	}
}
