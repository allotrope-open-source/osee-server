package adfsee;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;

/**
 * Webconfig based on https://www.thymeleaf.org/doc/tutorials/2.1/thymeleafspring.html
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
@Configuration
@EnableWebMvc
@ComponentScan
public class WebConfig extends WebMvcConfigurerAdapter implements ApplicationContextAware
{

	private ApplicationContext applicationContext;

	public WebConfig()
	{
		super();
	}

	@Override
	public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException
	{
		this.applicationContext = applicationContext;
	}

	@Bean
	public ResourceBundleMessageSource messageSource()
	{
		ResourceBundleMessageSource resourceBundleMessageSource = new ResourceBundleMessageSource();
		resourceBundleMessageSource.setBasename("Messages");
		return resourceBundleMessageSource;
	}

	/* **************************************************************** */
	/* THYMELEAF-SPECIFIC ARTIFACTS */
	/* TemplateResolver <- TemplateEngine <- ViewResolver */
	/* **************************************************************** */

	@Bean
	public SpringResourceTemplateResolver templateResolver()
	{
		SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
		templateResolver.setApplicationContext(applicationContext);
		templateResolver.setPrefix("/WEB-INF/adfsee/");
		templateResolver.setSuffix(".html");
		templateResolver.setTemplateMode("LEGACYHTML5");
		// Template cache is true by default. Set to false if you want
		// templates to be automatically updated when modified.
		templateResolver.setCacheable(true);
		return templateResolver;
	}

	@Bean
	public SpringTemplateEngine templateEngine()
	{
		SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		templateEngine.setTemplateResolver(templateResolver());
		return templateEngine;
	}

	@Bean
	public ThymeleafViewResolver thymeleafViewResolver()
	{
		ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
		viewResolver.setContentType("text/html;encoding=utf-8");
		viewResolver.setTemplateEngine(templateEngine());
		viewResolver.setViewNames(
				new String[] { "index", "*th", "html-label", "inspire-tree", "jquery-tree", "vis-tree", "vue-tree", "peak-table", "d3", "audit-trail",
						"audit-trail2", "audit-trail3", "audit-trail4", "audit-trail5", "adf-info", "datapackage", "datadescription", "3dfg", "2dfg", "3dfgVR", "ontodia", "pixi", "stardust", "ml", "biofabric", "yasgui" });
		viewResolver.setOrder(Integer.valueOf(1));
		return viewResolver;
	}

	/* **************************************************************** */
	/* JSP-SPECIFIC ARTIFACTS */
	/* **************************************************************** */

	@Bean
	public InternalResourceViewResolver jspViewResolver()
	{
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/adfsee/");
		viewResolver.setSuffix(".jsp");
		viewResolver.setViewNames(new String[] { "*jsp" });
		viewResolver.setOrder(Integer.valueOf(2));
		return viewResolver;
	}

	/* ******************************************************************* */
	/* Defines callback methods to customize the Java-based configuration */
	/* for Spring MVC enabled via {@code @EnableWebMvc} */
	/* ******************************************************************* */

	/*
	 * Dispatcher configuration for serving static resources
	 */
	@Override
	public void addResourceHandlers(final ResourceHandlerRegistry registry)
	{
		super.addResourceHandlers(registry);
		registry.addResourceHandler("/image/**").addResourceLocations("/WEB-INF/image/");
		registry.addResourceHandler("/css/**").addResourceLocations("/WEB-INF/css/");
		registry.addResourceHandler("/js/**").addResourceLocations("/WEB-INF/js/");
		registry.addResourceHandler("/data/**").addResourceLocations("/WEB-INF/data/");
		registry.addResourceHandler("/etc/**").addResourceLocations("/WEB-INF/etc/");
	}

	@Override
	public void configureViewResolvers(final ViewResolverRegistry registry)
	{
		registry.jsp("/WEB-INF/adfsee/", ".jsp");
	}
}