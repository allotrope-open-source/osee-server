package restcontroller;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
@Component
public class UploadFileVisitor implements FileVisitor<Path>
{
	public static Set<File> uploadedFiles = new HashSet<File>();
	public static Map<String, Set<File>> categorizedfiles = new HashMap<String, Set<File>>()
	{
		private static final long serialVersionUID = -3491100707802817616L;
		{
			put(RequestController.DATA_PREFIX, new HashSet<File>());
			put(RequestController.ADDITIONAL_PREFIX, new HashSet<File>());
		}
	};
	public static Set<File> excludedFiles = new HashSet<File>();

	private Logger logger = LoggerFactory.getLogger("log");

	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException
	{
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException
	{
		if (file.toString() != null && !file.toString().toUpperCase().endsWith("TTL") && !file.toString().toUpperCase().endsWith("ADF") && !file.toString().toUpperCase().endsWith("JSON") && !file.toString().toUpperCase().endsWith("XML"))
		{
			logger.info("Skipped: " + file.toString());
			uploadedFiles.add(file.toFile());
			excludedFiles.add(file.toFile());
			return FileVisitResult.CONTINUE;
		}
		logger.info("Including in visualization: " + file.toString());
		uploadedFiles.add(file.toFile());
		if (file.getFileName().toString().toLowerCase().startsWith(RequestController.DATA_PREFIX))
		{
			categorizedfiles.get(RequestController.DATA_PREFIX).add(file.toFile());
		}
		else if (file.getFileName().toString().toLowerCase().startsWith(RequestController.ADDITIONAL_PREFIX))
		{

			categorizedfiles.get(RequestController.ADDITIONAL_PREFIX).add(file.toFile());
		}
		else
		{
			excludedFiles.add(file.toFile());
		}
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException
	{
		return null;
	}

	@Override
	public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException
	{
		return FileVisitResult.CONTINUE;
	}
};
