package restcontroller;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public enum RequestParameters
{
	EXAMPLE, AFO, AFT, ADFAPI, INSTANCES, CLASSES, ENDPOINT, GRAPH, EXCLUDELITERALS, QUERY, MULTIGRAPH, SKOLEMIZE, LAYOUT
}
