package restcontroller;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import adfsee.AdfSeeMain;
import adfsee.TempFolderCreator;

/**
 * Handling REST calls without further processing
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
@Controller
public class RequestController
{
	public static final String DATA_PREFIX = "data_";
	public static final String ADDITIONAL_PREFIX = "additional_";

	private Logger logger = LoggerFactory.getLogger("log");
	@Value("${spring.application.name}")
	String appName;

	@PostMapping(value = "/upload")
	public @ResponseBody Object upload(@RequestParam("file") MultipartFile file, HttpServletRequest request)
	{
		Date startDate = Calendar.getInstance().getTime();
		try
		{
			logger.info("upload() called");
			if (file.isEmpty())
			{
				return new ResponseMessage(true, "Please select a file to upload", null, startDate);
			}

			try
			{
				Path path = Paths.get(TempFolderCreator.UPLOAD_FOLDER_PATH_NAME + DATA_PREFIX + RandomStringUtils.randomAlphanumeric(5) + "_" + file.getOriginalFilename());
				if (Files.exists(path))
				{
					return new ResponseMessage(true, "Uploaded file already exists on server. Please reload page.", null, startDate);
				}

				byte[] bytes = file.getBytes();
				Files.write(path, bytes);
			}
			catch (IOException e)
			{
				e.printStackTrace();
				String responseMessage = ProcessController.createMessageStringAfterCleanup("Error during upload of file " + file.getOriginalFilename() + ". Please try again.");
				return new ResponseMessage(false, responseMessage, null, startDate);
			}

			return new ResponseMessage(true, "You have successfully uploaded " + file.getOriginalFilename(), null, startDate);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			String responseMessage = ProcessController.createMessageStringAfterCleanup("Error during upload of file upload. Please try again.");
			return new ResponseMessage(false, responseMessage, null, startDate);
		}
	}

	@PostMapping(value = "/uploadadditional")
	public @ResponseBody Object uploadadditional(@RequestParam("file") MultipartFile file, HttpServletRequest request)
	{
		Date startDate = Calendar.getInstance().getTime();
		try
		{
			logger.info("uploadadditional() called");

			if (file.isEmpty())
			{
				return new ResponseMessage(true, "Please select a file to upload", null, startDate);
			}

			try
			{
				Path path = Paths.get(TempFolderCreator.UPLOAD_FOLDER_PATH_NAME + ADDITIONAL_PREFIX +  RandomStringUtils.randomAlphanumeric(5) + "_" + file.getOriginalFilename());
				if (Files.exists(path))
				{
					return new ResponseMessage(true, "Uploaded file already exists on server: " + file.getOriginalFilename(), null, startDate);
				}

				byte[] bytes = file.getBytes();
				Files.write(path, bytes);
			}
			catch (IOException e)
			{
				e.printStackTrace();
				String responseMessage = ProcessController.createMessageStringAfterCleanup("Error during upload of file " + file.getOriginalFilename() + ". Please try again.");
				return new ResponseMessage(false, responseMessage, null, startDate);
			}

			return new ResponseMessage(true, "You have successfully uploaded " + file.getOriginalFilename(), null, startDate);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			String responseMessage = ProcessController.createMessageStringAfterCleanup("Error during upload of file upload. Please try again.");
			return new ResponseMessage(false, responseMessage, null, startDate);
		}
	}

	@GetMapping("/")
	public String index(Model model)
	{
		logger.info("index called");
		model.addAttribute("appName", appName);
		model.addAttribute("useTripleStore", AdfSeeMain.useTripleStore);
		return "index";
	}

	@GetMapping("/index.html")
	public String indexHtml(Model model)
	{
		logger.info("index() called");
		model.addAttribute("appName", appName);
		model.addAttribute("useTripleStore", AdfSeeMain.useTripleStore);
		return "index";
	}

	@GetMapping("/Dropzone")
	public String dropzone(Map<String, Object> model)
	{
		logger.info("Dropzone() called");
		return "Dropzone";
	}

	@GetMapping("/html-label.html")
	public String cytoHtml(Map<String, Object> model)
	{
		logger.info("cytoHtml() called");
		model.put("useTripleStore", AdfSeeMain.useTripleStore);
		return "html-label";
	}

	@GetMapping("/inspire-tree.html")
	public String inspireTree(Map<String, Object> model)
	{
		logger.info("inspireTree() called");
		return "inspire-tree";
	}

	@GetMapping("/jquery-tree.html")
	public String jqueryTree(Map<String, Object> model)
	{
		logger.info("jqueryTree() called");
		return "jquery-tree";
	}

	@GetMapping("/vis-tree.html")
	public String visTree(Map<String, Object> model)
	{
		logger.info("visTree() called");
		return "vis-tree";
	}

	@GetMapping("/vue-tree.html")
	public String vueTree(Map<String, Object> model)
	{
		logger.info("vueTree() called");
		return "vue-tree";
	}

	@GetMapping("/peak-table.html")
	public String peakTable(Map<String, Object> model)
	{
		logger.info("peakTable() called");
		return "peak-table";
	}

	@GetMapping("/d3.html")
	public String d3(Map<String, Object> model)
	{
		logger.info("d3() called");
		return "d3";
	}

	@GetMapping("/audit-trail.html")
	public String audittrail(Map<String, Object> model)
	{
		logger.info("audittrail() called");
		return "audit-trail";
	}

	@GetMapping("/audit-trail2.html")
	public String audittrail2(Map<String, Object> model)
	{
		logger.info("audittrail2() called");
		return "audit-trail2";
	}

	@GetMapping("/audit-trail3.html")
	public String audittrail3(Map<String, Object> model)
	{
		logger.info("audittrail3() called");
		return "audit-trail3";
	}

	@GetMapping("/audit-trail4.html")
	public String audittrail4(Map<String, Object> model)
	{
		logger.info("audittrail4() called");
		return "audit-trail4";
	}

	@GetMapping("/audit-trail5.html")
	public String audittrail5(Map<String, Object> model)
	{
		logger.info("audittrail5() called");
		return "audit-trail5";
	}

	@GetMapping("/adf-info.html")
	public String adfInfo(Map<String, Object> model)
	{
		logger.info("adfInfo() called");
		return "adf-info";
	}

	@GetMapping("/datapackage.html")
	public String datapackage(Map<String, Object> model)
	{
		logger.info("datapackage() called");
		return "datapackage";
	}

	@GetMapping("/datadescription.html")
	public String datadescription(Map<String, Object> model)
	{
		logger.info("datadescription() called");
		return "datadescription";
	}

	@GetMapping("/3dfg.html")
	public String threedfg(Map<String, Object> model)
	{
		logger.info("three3dfg() called");
		return "3dfg";
	}

	@GetMapping("/2dfg.html")
	public String twodfg(Map<String, Object> model)
	{
		logger.info("2dfg() called");
		return "2dfg";
	}

	@GetMapping("/3dfgVR.html")
	public String threedfgVR(Map<String, Object> model)
	{
		logger.info("3dfgVR() called");
		return "3dfgVR";
	}

	@GetMapping("/ontodia.html")
	public String ontodia(Map<String, Object> model)
	{
		logger.info("ontodia() called");
		return "ontodia";
	}

	@GetMapping("/pixi.html")
	public String pixi(Map<String, Object> model)
	{
		logger.info("pixi() called");
		return "pixi";
	}

	@GetMapping("/stardust.html")
	public String stardust(Map<String, Object> model)
	{
		logger.info("stardust() called");
		return "stardust";
	}

	@GetMapping("/ml.html")
	public String ml(Map<String, Object> model)
	{
		logger.info("ml() called");
		return "ml";
	}

	@GetMapping("/biofabric.html")
	public String biofabric(Map<String, Object> model)
	{
		logger.info("biofabric() called");
		return "biofabric";
	}

	@GetMapping("/yasgui.html")
	public String yasgui(Map<String, Object> model)
	{
		logger.info("yasgui() called");
		return "yasgui";
	}

	@GetMapping(value = { "/download/**/{file_name}", "/download/{file_name}" })
	@ResponseBody
	public ResponseEntity<Object> getFile(@PathVariable("file_name") String fileName, HttpServletRequest request)
	{
		logger.info("getFile() called to download: " + fileName);
		String targetFileName = request.getRequestURI();
		try {
			targetFileName = targetFileName.replaceAll(TempFolderCreator.SPACE_REPLACE, " ");
		} catch (Exception e) {
			logger.error("Error processing download uri: " + targetFileName);
			e.printStackTrace();
			String responseMessage = ProcessController.createMessageStringAfterCleanup("Error during download of file.");
			return new ResponseEntity<>(responseMessage, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		targetFileName = targetFileName.replaceAll("\\.\\.", "").replaceAll("/download/", "").replaceAll("/", Matcher.quoteReplacement(File.separator));
		HttpHeaders headers = new HttpHeaders();
		FileSystemResource fileSystemResource = new FileSystemResource(getFileFor(targetFileName));
		headers.set("content-disposition", "inline; filename=\"" + fileSystemResource.getFilename() + "\"");
		headers.set("content-length", String.valueOf(fileSystemResource.getFile().length()));
		return new ResponseEntity<>(fileSystemResource, headers, HttpStatus.OK);
	}

	private File getFileFor(String fileName)
	{
		logger.info("Fetching file from download folder: " + TempFolderCreator.DOWNLOAD_FOLDER_PATH_NAME);
		Path filePath = Paths.get(TempFolderCreator.DOWNLOAD_FOLDER_PATH_NAME + fileName);
		if (!Files.exists(filePath) || !Files.isRegularFile(filePath, LinkOption.NOFOLLOW_LINKS) || Files.isDirectory(filePath))
		{
			try
			{
				Path tempFile = Files.createTempFile(Paths.get(TempFolderCreator.DOWNLOAD_FOLDER_PATH_NAME), null, ".txt");
				Files.write(tempFile, Arrays.asList("file not found!"), Charset.defaultCharset(), StandardOpenOption.WRITE);
				tempFile.toFile().deleteOnExit();
				return tempFile.toFile();
			}
			catch (IOException e)
			{
				e.printStackTrace();
				return null;
			}
		}
		return filePath.toFile();
	}

}