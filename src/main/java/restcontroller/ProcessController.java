package restcontroller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import adfsee.TempFolderCreator;
import data.ProcessingFlags;
import processor.Processor;
import processor.util.ResourceExtractor;

/**
 * Handling REST calls for processing uploaded data
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
@RestController
public class ProcessController
{
	private static UploadFileVisitor uploadFileVisitor;
	private static ProcessingFlags processingFlags;

	public static Logger logger = LoggerFactory.getLogger("log");

	public static String baseFileUrl = "";

	@Autowired
	public ProcessController(UploadFileVisitor uploadFileVisitor, ProcessingFlags processingFlags, @Value("${server.port}") String serverPort)
	{
		ProcessController.uploadFileVisitor = uploadFileVisitor;
		ProcessController.processingFlags = processingFlags;
		baseFileUrl = "http://localhost:" + serverPort + "/download/";
	}

	@GetMapping(value = "/generate", produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody ResponseMessage process(HttpServletRequest request)
	{
		processingFlags.reset();
		Date startDate = Calendar.getInstance().getTime();
		try
		{
			logger.info("process() called");

			Map<String, String[]> requestParameterMap = request.getParameterMap();
			for (String key : requestParameterMap.keySet())
			{
				logger.info("Handling parameter with key : " + key + " and value: " + requestParameterMap.get(key)[0]);
				if (key.toUpperCase().equals(RequestParameters.EXAMPLE.name()))
				{
					processingFlags.setUseExample(true);
				}

				if (key.toUpperCase().equals(RequestParameters.AFO.name()))
				{
					processingFlags.setLoadAfo(true);
				}

				if (key.toUpperCase().equals(RequestParameters.AFT.name()))
				{
					processingFlags.setLoadAft(true);
				}

				if (key.toUpperCase().equals(RequestParameters.LAYOUT.name()))
				{
					processingFlags.setDoLayout(true);
				}

				if (key.toUpperCase().equals(RequestParameters.ADFAPI.name()))
				{
					processingFlags.setIncludeAdfApi(true);
				}

				if (key.toUpperCase().equals(RequestParameters.INSTANCES.name()))
				{
					processingFlags.setVisualizeInstances(true);
				}

				if (key.toUpperCase().equals(RequestParameters.CLASSES.name()))
				{
					processingFlags.setVisualizeOntology(true);
				}

				if (key.toUpperCase().equals(RequestParameters.EXCLUDELITERALS.name()))
				{
					processingFlags.setExcludeLiterals(true);
				}

				if (key.toUpperCase().equals(RequestParameters.ENDPOINT.name()))
				{
					processingFlags.setEndpoint(true);
					processingFlags.setEndpointUrl(requestParameterMap.get(key)[0]);
				}

				if (key.toUpperCase().equals(RequestParameters.GRAPH.name()))
				{
					processingFlags.setGraphUrl(requestParameterMap.get(key)[0]);
				}

				if (key.toUpperCase().equals(RequestParameters.MULTIGRAPH.name()))
				{
					processingFlags.setMultiGraph(true);
				}

				if (key.toUpperCase().equals(RequestParameters.SKOLEMIZE.name()))
				{
					processingFlags.setSkolemize(true);
				}

				if (key.toUpperCase().equals(RequestParameters.QUERY.name()))
				{
					String queryDecoded = URLDecoder.decode(requestParameterMap.get(key)[0].replace("+", "%2B"), "UTF-8").replace("%2B", "+");
					processingFlags.setConstructQuery(queryDecoded);
				}
			}

			try
			{
				prepareDownloadFolder();
			}
			catch (Exception e)
			{
				e.printStackTrace();
				return new ResponseMessage(false, createMessageStringAfterCleanup("Exception during preparation of download folder."), processingFlags, startDate);
			}

			if (processingFlags.isUseExample())
			{
				ResponseMessage responseMessage = executeExample(startDate);
				return responseMessage;
			}

			Path uploadPath = TempFolderCreator.UPLOAD_FOLDER_PATH;
			if (uploadPath.toFile().isDirectory())
			{
				try
				{
					Files.walkFileTree(uploadPath, uploadFileVisitor);
				}
				catch (IOException e)
				{
					e.printStackTrace();
					return new ResponseMessage(false, createMessageStringAfterCleanup("Error during file processing."), processingFlags, startDate);
				}
			}
			else
			{
				return new ResponseMessage(false, createMessageStringAfterCleanup("There is no upload folder on the server available."), processingFlags, startDate);
			}

			if (UploadFileVisitor.uploadedFiles.isEmpty() && !processingFlags.isEndpoint())
			{
				return new ResponseMessage(false, createMessageStringAfterCleanup("No uploaded files found."), processingFlags, startDate);
			}

			if (UploadFileVisitor.uploadedFiles.size() > 0 && UploadFileVisitor.uploadedFiles.size() == UploadFileVisitor.excludedFiles.size()
					&& UploadFileVisitor.uploadedFiles.containsAll(UploadFileVisitor.excludedFiles))
			{
				return new ResponseMessage(false, createMessageStringAfterCleanup("Uploaded files could not be processed. Only TTL or ADF format supported."), processingFlags, startDate);
			}

			Set<File> dataFiles = UploadFileVisitor.categorizedfiles.get(RequestController.DATA_PREFIX);
			Set<File> additionalFiles = UploadFileVisitor.categorizedfiles.get(RequestController.ADDITIONAL_PREFIX);
			if (!UploadFileVisitor.categorizedfiles.get(RequestController.DATA_PREFIX).isEmpty() && !UploadFileVisitor.categorizedfiles.get(RequestController.ADDITIONAL_PREFIX).isEmpty())
			{
				Set<String> dataFileNames = new HashSet<String>();
				Set<String> additionalFileNames = new HashSet<String>();
				for (Iterator<File> iterator = additionalFiles.iterator(); iterator.hasNext();)
				{
					File file = iterator.next();
					String fileName = file.getName().replaceFirst(RequestController.ADDITIONAL_PREFIX, "");

					if (!file.getName().toLowerCase().endsWith("ttl"))
					{
						return new ResponseMessage(false, createMessageStringAfterCleanup("File " + fileName.substring(7) + " was uploaded as additional vocabulary but only TTL files are supported."),
								processingFlags, startDate);
					}

					additionalFileNames.add(fileName);
				}
				for (Iterator<File> iterator = dataFiles.iterator(); iterator.hasNext();)
				{
					File file = iterator.next();
					String fileName = file.getName().replaceFirst(RequestController.DATA_PREFIX, "");
					if (file.getName().toLowerCase().endsWith("adf"))
					{
						processingFlags.setAdfInput(true);
					}
					dataFileNames.add(fileName);
				}
			}
			else if (!UploadFileVisitor.categorizedfiles.get(RequestController.DATA_PREFIX).isEmpty() && UploadFileVisitor.categorizedfiles.get(RequestController.DATA_PREFIX).size() == 1
					&& UploadFileVisitor.categorizedfiles.get(RequestController.ADDITIONAL_PREFIX).isEmpty())
			{
				for (Iterator<File> iterator = UploadFileVisitor.categorizedfiles.get(RequestController.DATA_PREFIX).iterator(); iterator.hasNext();)
				{
					File file = iterator.next();
					if (file.getName().toLowerCase().endsWith("json"))
					{
						logger.info("Loading layout from json: " + file.getAbsolutePath());
						String layoutString = StringUtils.join(Files.readAllLines(file.toPath()), "\n");
						Map<String, String> resultMap = new HashMap<String, String>();
						try
						{
							resultMap.put("layout", layoutString);
							resultMap.put("layoutsize", String.valueOf(layoutString.length()));
						}
						catch (Exception e)
						{
							logger.error("Error during parsing precomputed layout from json file: " + file.getAbsolutePath());
							e.printStackTrace();
							return new ResponseMessage(false, createMessageStringAfterCleanup("Uploaded json file could not be parsed."), processingFlags, startDate);
						}
						ResponseMessage responseMessage = cleanUpAndPrepareResponse(resultMap, startDate);
						return responseMessage;
					}
				}
			}

			if (processingFlags.isEndpoint())
			{
				ResponseMessage responseMessage = executeQueryEndpoint(startDate, additionalFiles);
				return responseMessage;
			}

			Map<String, String> results = null;
			try
			{
				logger.info("processing");
				List<String> parameterList = new ArrayList<String>();
				parameterList.add("--input");
				
				if (dataFiles.size() == 1) {
					// process single TTL or ADF 
					parameterList.add(TempFolderCreator.UPLOAD_FOLDER_PATH_NAME + dataFiles.iterator().next().getName());
				} else {
					// process multiple files
					// rdfcyto expects exactly 1 input file so we merge our data files to one artifact 
					logger.info("Merging input files for rdfcyto...");
					Path mergedFile = TempFolderCreator.UPLOAD_FOLDER_PATH.resolve("merged-" + RandomStringUtils.randomAlphanumeric(5) + ".ttl");
					Model mergedModel = ModelFactory.createDefaultModel();
					for (File file : dataFiles)
					{
						try{
							Model fileModel = RDFDataMgr.loadModel(file.getAbsolutePath().toString());
							mergedModel.add(fileModel);
						} catch(Exception e) {
							logger.error("Error reading model from file: " + file.getAbsolutePath().toString());
							e.printStackTrace();
						}
					}
					
					try(FileOutputStream fos = new FileOutputStream(mergedFile.toFile())) {
						RDFDataMgr.write(fos, mergedModel, Lang.TURTLE);
					} catch(Exception e) {
						logger.error("Error writing merged model to file: " + mergedFile.toFile().getAbsolutePath().toString());
						e.printStackTrace();
					}
					
					parameterList.add(TempFolderCreator.UPLOAD_FOLDER_PATH_NAME + mergedFile.toFile().getName());
					
				}

				if (!additionalFiles.isEmpty() || processingFlags.isLoadAfo())
				{
					parameterList.add("--read");
				}

				if (!additionalFiles.isEmpty())
				{
					for (File file : additionalFiles)
					{
						parameterList.add(TempFolderCreator.UPLOAD_FOLDER_PATH_NAME + file.getName());
					}
				}

				if (processingFlags.isLoadAfo())
				{
					ResourceExtractor.extractSingleResource("vocabulary/afo-suite-snapshot.ttl", TempFolderCreator.UPLOAD_FOLDER_PATH_NAME);
					parameterList.add(TempFolderCreator.UPLOAD_FOLDER_PATH_NAME + "afo-suite-snapshot.ttl");
					ResourceExtractor.extractSingleResource("vocabulary/afo-merged-WD-2019-01-25.ttl", TempFolderCreator.UPLOAD_FOLDER_PATH_NAME);
					parameterList.add(TempFolderCreator.UPLOAD_FOLDER_PATH_NAME + "afo-merged-WD-2019-01-25.ttl");
				}

				if (processingFlags.isLoadAft())
				{
					ResourceExtractor.extractSingleResource("vocabulary/aft-merged-1.1.5-dev.ttl", TempFolderCreator.UPLOAD_FOLDER_PATH_NAME);
					parameterList.add(TempFolderCreator.UPLOAD_FOLDER_PATH_NAME + "aft-merged-1.1.5-dev.ttl");
				}

				if (processingFlags.isVisualizeInstances())
				{
					parameterList.add("--instances");
				}

				if (processingFlags.isVisualizeOntology())
				{
					parameterList.add("--ontology");
				}

				parameterList.add("--output");
				parameterList.add("data.json");

				if (!processingFlags.isExcludeLiterals())
				{
					parameterList.add("--showliteralnodes");
				}

				parameterList.add("--optimize");
				parameterList.add("--layout");
				parameterList.add("graphviz");
				parameterList.add("--dot");
				parameterList.add(TempFolderCreator.DOT_FOLDER_PATH_NAME + "dot.exe");
				parameterList.add("--nodesize");
				parameterList.add("2.0");
				parameterList.add("--graphvizalgo");
				parameterList.add("sfdp");

				if (processingFlags.isIncludeAdfApi())
				{
					parameterList.add("--includeadfapi");
				}

				if (processingFlags.getConstructQuery() != null && !processingFlags.getConstructQuery().isEmpty()
						&& !processingFlags.getConstructQuery().trim().toLowerCase().equals("construct query"))
				{
					parameterList.add("--construct");
					parameterList.add(processingFlags.getConstructQuery());
				}

				if (processingFlags.isMultiGraph())
				{
					parameterList.add("--multigraph");
				}

				if (processingFlags.isSkolemize())
				{
					parameterList.add("--skolemize");
				}

				if (!processingFlags.isDoLayout())
				{
					parameterList.add("--skiplayout");
				}

				String[] parameters = new String[parameterList.size()];
				int i = 0;
				for (Iterator<String> iterator = parameterList.iterator(); iterator.hasNext();)
				{
					String string = iterator.next();
					parameters[i++] = string;
				}

				results = Processor.processData(parameters);
			}
			catch (Exception e1)
			{
				e1.printStackTrace();
				return new ResponseMessage(false, createMessageStringAfterCleanup("Exception during processing of uploaded files."), processingFlags, startDate);
			}

			ResponseMessage responseMessage = cleanUpAndPrepareResponse(results, startDate);
			return responseMessage;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return new ResponseMessage(false, createMessageStringAfterCleanup("Exception during processing of uploaded files."), processingFlags, startDate);
		}
	}

	private ResponseMessage cleanUpAndPrepareResponse(Map<String, String> results, Date startDate)
	{
		String cleanupMessage = removeUploadedFiles();

		String processedFiles = "";
		for (File file : UploadFileVisitor.categorizedfiles.get(RequestController.DATA_PREFIX))
		{
			String fileName = file.getName().replaceFirst(RequestController.DATA_PREFIX, "");
			processedFiles = processedFiles + " " + fileName;
		}
		for (File file : UploadFileVisitor.categorizedfiles.get(RequestController.ADDITIONAL_PREFIX))
		{
			String fileName = file.getName().replaceFirst(RequestController.ADDITIONAL_PREFIX, "");
			processedFiles = processedFiles + " " + fileName;
		}
		String excludedFiles = "";
		for (File file : UploadFileVisitor.excludedFiles)
		{
			excludedFiles = excludedFiles + " " + file.getName().replaceFirst(RequestController.DATA_PREFIX, "").replaceFirst(RequestController.ADDITIONAL_PREFIX, "");
		}
		if (!processedFiles.isEmpty())
		{
			processedFiles = processedFiles.trim();
		}

		if (!excludedFiles.isEmpty())
		{
			excludedFiles = excludedFiles.trim();
		}

		String treeString = results.get("tree");
		String graphString = results.get("graph");

		String statusResponse = StringUtils.EMPTY;
		if (!processingFlags.isUseExample() && !processingFlags.isEndpoint())
		{
			statusResponse = (processedFiles.isEmpty() ? "No data files were found." : "Processed files: " + processedFiles) + (excludedFiles.isEmpty() ? "" : " Excluded files: " + excludedFiles);
		}
		else if (processingFlags.isUseExample())
		{
			statusResponse = "Generated graph for example data of LC-UV super domain. ";
		}
		else if (processingFlags.isEndpoint())
		{
			statusResponse = "Generated graph from SPARQL endpoint. ";
		}
		else
		{
			statusResponse = "Generated graph. ";
		}

		String responseString = "Request was executed. " + statusResponse + (cleanupMessage.isEmpty() ? "" : cleanupMessage);

		ResponseMessage responseMessage = new ResponseMessage(true, responseString, processingFlags, treeString, graphString, startDate);

		responseMessage.setGraphSize(results.get("graphSize"));
		responseMessage.setTreeSize(results.get("treeSize"));

		responseMessage.setQueryResultsString(results.get("queryresults"));
		responseMessage.setQueryResultsSize(results.get("queryresultssize"));

		responseMessage.setDatacubesString(results.get("datacubes"));
		responseMessage.setDatacubesSize(results.get("datacubessize"));

		responseMessage.setAudittrailString(results.get("audittrail"));
		responseMessage.setAudittrailSize(results.get("audittrailsize"));

		responseMessage.setAdfInfoString(results.get("adfinfo"));
		responseMessage.setAdfInfoSize(results.get("adfinfosize"));

		responseMessage.setDatapackageString(results.get("datapackage"));
		responseMessage.setDatapackageSize(results.get("datapackagesize"));

		responseMessage.setDatadescriptionString(results.get("datadescription"));
		responseMessage.setDatadescriptionSize(results.get("datadescriptionsize"));

		responseMessage.setLayoutString(results.get("layout"));
		responseMessage.setDatapackageSize(results.get("layoutsize"));

		clearFileSets();
		return responseMessage;
	}

	private ResponseMessage executeExample(Date startDate)
	{
		// [--debug, --input, test/lc-instances.ttl, --read, afo-suite-snapshot.ttl, adm-lc-super-domain.ttl, shacl.ttl, --instances, --output,
		// data.json, --showliteralnodes, --optimize, --layout, graphviz, --dot, C:\dev\tools\GraphViz2.38\bin\dot.exe, --nodesize, 2.0,
		// --graphvizalgo, sfdp]

		ResourceExtractor.extractSingleResource("instances/example.adf", TempFolderCreator.UPLOAD_FOLDER_PATH_NAME);
		ResourceExtractor.extractSingleResource("vocabulary/afo-suite-snapshot.ttl", TempFolderCreator.UPLOAD_FOLDER_PATH_NAME);
		ResourceExtractor.extractSingleResource("vocabulary/adm-lc-super-domain.ttl", TempFolderCreator.UPLOAD_FOLDER_PATH_NAME);
		ResourceExtractor.extractSingleResource("vocabulary/afo-merged-WD-2019-01-25.ttl", TempFolderCreator.UPLOAD_FOLDER_PATH_NAME);
		ResourceExtractor.extractSingleResource("vocabulary/shacl.ttl", TempFolderCreator.UPLOAD_FOLDER_PATH_NAME);

		List<String> parameterList = new ArrayList<String>();
		parameterList.add("--input");
		parameterList.add(TempFolderCreator.UPLOAD_FOLDER_PATH_NAME + "example.adf");
		parameterList.add("--read");
		parameterList.add(TempFolderCreator.UPLOAD_FOLDER_PATH_NAME + "afo-suite-snapshot.ttl");
		parameterList.add(TempFolderCreator.UPLOAD_FOLDER_PATH_NAME + "adm-lc-super-domain.ttl");
		parameterList.add(TempFolderCreator.UPLOAD_FOLDER_PATH_NAME + "afo-merged-WD-2019-01-25.ttl");
		parameterList.add(TempFolderCreator.UPLOAD_FOLDER_PATH_NAME + "shacl.ttl");
		parameterList.add("--instances");
		parameterList.add("--output");
		parameterList.add("data.json");
		parameterList.add("--optimize");
		parameterList.add("--layout");
		parameterList.add("graphviz");
		parameterList.add("--dot");
		parameterList.add(TempFolderCreator.DOT_FOLDER_PATH_NAME + "dot.exe");
		parameterList.add("--nodesize");
		parameterList.add("2.0");

		if (processingFlags.isIncludeAdfApi())
		{
			parameterList.add("--includeadfapi");
		}

		if (!processingFlags.isExcludeLiterals())
		{
			parameterList.add("--showliteralnodes");
		}

		if (processingFlags.isMultiGraph())
		{
			parameterList.add("--multigraph");
		}

		if (processingFlags.isSkolemize())
		{
			parameterList.add("--skolemize");
		}

		if (!processingFlags.isDoLayout())
		{
			parameterList.add("--skiplayout");
		}

		parameterList.add("--graphvizalgo");
		parameterList.add("sfdp");

		if (processingFlags.getConstructQuery() != null && !processingFlags.getConstructQuery().isEmpty() && !processingFlags.getConstructQuery().trim().toLowerCase().equals("construct query"))
		{
			parameterList.add("--construct");
			parameterList.add(processingFlags.getConstructQuery());
		}

		String[] parameters = new String[parameterList.size()];
		int i = 0;
		for (Iterator<String> iterator = parameterList.iterator(); iterator.hasNext();)
		{
			String string = iterator.next();
			parameters[i++] = string;
		}

		Map<String, String> results = null;
		try
		{
			results = Processor.processData(parameters);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return new ResponseMessage(false, createMessageStringAfterCleanup("Exception during processing of uploaded files."), processingFlags, startDate);
		}

		ResponseMessage responseMessage = cleanUpAndPrepareResponse(results, startDate);
		return responseMessage;
	}

	private ResponseMessage executeQueryEndpoint(Date startDate, Set<File> additionalFiles)
	{
		List<String> parameterList = new ArrayList<String>();
		parameterList.add("--input");
		parameterList.add("endpoint.sparql");

		if (!additionalFiles.isEmpty() || processingFlags.isLoadAfo())
		{
			parameterList.add("--read");
		}

		if (!additionalFiles.isEmpty())
		{
			for (File file : additionalFiles)
			{
				parameterList.add(TempFolderCreator.UPLOAD_FOLDER_PATH_NAME + file.getName());
			}
		}

		if (processingFlags.isLoadAfo())
		{
			ResourceExtractor.extractSingleResource("vocabulary/afo-suite-snapshot.ttl", TempFolderCreator.UPLOAD_FOLDER_PATH_NAME);
			parameterList.add(TempFolderCreator.UPLOAD_FOLDER_PATH_NAME + "afo-suite-snapshot.ttl");
			ResourceExtractor.extractSingleResource("vocabulary/afo-merged-WD-2019-01-25.ttl", TempFolderCreator.UPLOAD_FOLDER_PATH_NAME);
			parameterList.add(TempFolderCreator.UPLOAD_FOLDER_PATH_NAME + "afo-merged-WD-2019-01-25.ttl");
		}

		if (processingFlags.isLoadAft())
		{
			ResourceExtractor.extractSingleResource("vocabulary/aft-merged-1.1.5-dev.ttl", TempFolderCreator.UPLOAD_FOLDER_PATH_NAME);
			parameterList.add(TempFolderCreator.UPLOAD_FOLDER_PATH_NAME + "aft-merged-1.1.5-dev.ttl");
		}

		if (!processingFlags.isVisualizeInstances())
		{
			parameterList.add("--instances");
		}

		if (processingFlags.isVisualizeOntology())
		{
			parameterList.add("--ontology");
		}

		parameterList.add("--output");
		parameterList.add("data.json");

		if (!processingFlags.isExcludeLiterals())
		{
			parameterList.add("--showliteralnodes");
		}

		parameterList.add("--optimize");
		parameterList.add("--layout");
		parameterList.add("graphviz");
		parameterList.add("--dot");
		parameterList.add(TempFolderCreator.DOT_FOLDER_PATH_NAME + "dot.exe");
		parameterList.add("--nodesize");
		parameterList.add("2.0");
		parameterList.add("--graphvizalgo");
		parameterList.add("sfdp");
		parameterList.add("--endpointurl");
		parameterList.add(processingFlags.getEndpointUrl());

		if (processingFlags.getGraphUrl() != null && !processingFlags.getGraphUrl().isEmpty())
		{
			parameterList.add("--graphurl");
			parameterList.add(processingFlags.getGraphUrl());
		}

		if (processingFlags.getConstructQuery() != null && !processingFlags.getConstructQuery().isEmpty())
		{
			parameterList.add("--construct");
			parameterList.add(processingFlags.getConstructQuery());
		}

		if (processingFlags.isIncludeAdfApi())
		{
			parameterList.add("--includeadfapi");
		}

		if (processingFlags.isMultiGraph())
		{
			parameterList.add("--multigraph");
		}

		if (processingFlags.isSkolemize())
		{
			parameterList.add("--skolemize");
		}

		if (!processingFlags.isDoLayout())
		{
			parameterList.add("--skiplayout");
		}

		String[] parameters = new String[parameterList.size()];
		int i = 0;
		for (Iterator<String> iterator = parameterList.iterator(); iterator.hasNext();)
		{
			String string = iterator.next();
			parameters[i++] = string;
		}

		Map<String, String> results = null;
		try
		{
			results = Processor.processData(parameters);
			if (results == null)
			{
				return new ResponseMessage(false,
						createMessageStringAfterCleanup("Error querying SPARQL endpoint: \"" + processingFlags.getEndpointUrl() + "\" named graph: \"" + processingFlags.getGraphUrl() + "\""),
						processingFlags, startDate);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return new ResponseMessage(false, createMessageStringAfterCleanup("Exception during processing SPARQL endpoint."), processingFlags, startDate);
		}

		ResponseMessage responseMessage = cleanUpAndPrepareResponse(results, startDate);
		return responseMessage;
	}

	public static String createMessageStringAfterCleanup(String responseMessage)
	{
		String cleanupMessage = removeUploadedFiles();
		clearFileSets();
		processingFlags.reset();
		if (!cleanupMessage.isEmpty())
		{
			responseMessage = responseMessage + cleanupMessage;
		}
		return responseMessage;
	}

	public static void clearFileSets()
	{
		UploadFileVisitor.uploadedFiles.clear();
		UploadFileVisitor.categorizedfiles.get(RequestController.DATA_PREFIX).clear();
		UploadFileVisitor.categorizedfiles.get(RequestController.ADDITIONAL_PREFIX).clear();
		UploadFileVisitor.excludedFiles.clear();
	}

	public static String removeUploadedFiles()
	{
		try
		{
			for (Iterator<File> iterator = UploadFileVisitor.uploadedFiles.iterator(); iterator.hasNext();)
			{
				File file = iterator.next();

				try
				{
					Files.deleteIfExists(file.toPath());
				}
				catch (IOException e)
				{
					e.printStackTrace();
					return " Exception during cleanup of file: " + file.getName();
				}
			}

			Path uploadFolder = Paths.get(TempFolderCreator.UPLOAD_FOLDER_PATH_NAME);
			if (Files.exists(uploadFolder))
			{
				FileUtils.cleanDirectory(uploadFolder.toFile());
			}
			else
			{
				Files.createDirectory(uploadFolder);
			}

			return "";
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return " Exception during cleanup of upload folder.";
		}
	}

	public static String prepareDownloadFolder()
	{
		try
		{
			Path downloadFolder = Paths.get(TempFolderCreator.DOWNLOAD_FOLDER_PATH_NAME);
			if (Files.exists(downloadFolder))
			{
				FileUtils.cleanDirectory(downloadFolder.toFile());
			}
			else
			{
				Files.createDirectory(downloadFolder);
			}

			return "";
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return " Exception during preparation of download folder.";
		}
	}
}