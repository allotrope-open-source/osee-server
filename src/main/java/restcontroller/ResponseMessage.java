package restcontroller;

import java.util.Calendar;
import java.util.Date;

import data.ProcessingFlags;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class ResponseMessage
{

	public boolean success = true;
	public String message;
	public ProcessingFlags processingFlags;
	public String treeString;
	public String graphString;
	public String layoutString;
	public String queryResultsString;
	public String datacubesString;
	public String audittrailString;
	public String adfInfoString;
	public String datapackageString;
	public String datadescriptionString;
	public String treeSize;
	public String graphSize;
	public String layoutSize;
	public String queryResultsSize;
	public String datacubesSize;
	public String audittrailSize;
	public String adfInfoSize;
	public String datadescriptionSize;
	public String datapackageSize;
	public Date startTime;

	public ResponseMessage(boolean success, String message, ProcessingFlags processingFlags, Date startDate)
	{
		super();
		this.success = success;
		this.message = createMessage(message, startDate);
		this.processingFlags = processingFlags;
		startTime = startDate;
	}

	public ResponseMessage(boolean success, String message, ProcessingFlags processingFlags, String treeString, String graphString, Date startDate)
	{
		super();
		this.success = success;
		this.message = createMessage(message, startDate);
		this.processingFlags = processingFlags;
		this.treeString = treeString;
		this.graphString = graphString;
		startTime = startDate;
	}

	private String createMessage(String messageInternal, Date startDate)
	{
		Date endDate = Calendar.getInstance().getTime();
		return "Server response: " + messageInternal + "<br/><span style=\"font-size:6pt;\">" + endDate.toString() + ", " + (endDate.getTime() - startDate.getTime()) + " ms</span>";
	}

	public boolean getSuccess()
	{
		return success;
	}

	public void setSuccess(boolean success)
	{
		this.success = success;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = createMessage(message, startTime);
	}

	public ProcessingFlags getProcessingFlags()
	{
		return processingFlags;
	}

	public void setProcessingFlags(ProcessingFlags processingFlags)
	{
		this.processingFlags = processingFlags;
	}

	public String getTreeString()
	{
		return treeString;
	}

	public void setTreeString(String treeString)
	{
		this.treeString = treeString;
	}

	public String getLayoutString()
	{
		return layoutString;
	}

	public void setLayoutString(String layoutString)
	{
		this.layoutString = layoutString;
	}

	public String getGraphString()
	{
		return graphString;
	}

	public void setGraphString(String graphString)
	{
		this.graphString = graphString;
	}

	public Date getStartTime()
	{
		return startTime;
	}

	public void setStartTime(Date startTime)
	{
		this.startTime = startTime;
	}

	public String getGraphSize()
	{
		return graphSize;
	}

	public void setGraphSize(String graphSize)
	{
		this.graphSize = graphSize;
	}

	public String getTreeSize()
	{
		return treeSize;
	}

	public void setTreeSize(String treeSize)
	{
		this.treeSize = treeSize;
	}

	public String getLayoutSize()
	{
		return layoutSize;
	}

	public void setLayoutSize(String layoutSize)
	{
		this.layoutSize = layoutSize;
	}

	public String getQueryResultsString()
	{
		return queryResultsString;
	}

	public void setQueryResultsString(String queryResultsString)
	{
		this.queryResultsString = queryResultsString;
	}

	public String getQueryResultsSize()
	{
		return queryResultsSize;
	}

	public void setQueryResultsSize(String queryResultsSize)
	{
		this.queryResultsSize = queryResultsSize;
	}

	public String getDatacubesString()
	{
		return datacubesString;
	}

	public void setDatacubesString(String datacubesString)
	{
		this.datacubesString = datacubesString;
	}

	public String getDatacubesSize()
	{
		return datacubesSize;
	}

	public void setDatacubesSize(String datacubesSize)
	{
		this.datacubesSize = datacubesSize;
	}

	public String getAudittrailString()
	{
		return audittrailString;
	}

	public void setAudittrailString(String audittrailString)
	{
		this.audittrailString = audittrailString;
	}

	public String getAudittrailSize()
	{
		return audittrailSize;
	}

	public void setAudittrailSize(String audittrailSize)
	{
		this.audittrailSize = audittrailSize;
	}

	public String getAdfInfoString()
	{
		return adfInfoString;
	}

	public void setAdfInfoString(String adfInfoString)
	{
		this.adfInfoString = adfInfoString;
	}

	public String getAdfInfoSize()
	{
		return adfInfoSize;
	}

	public void setAdfInfoSize(String adfInfoSize)
	{
		this.adfInfoSize = adfInfoSize;
	}

	public String getDatapackageString()
	{
		return datapackageString;
	}

	public void setDatapackageString(String datapackageString)
	{
		this.datapackageString = datapackageString;
	}

	public String getDatapackageSize()
	{
		return datapackageSize;
	}

	public void setDatapackageSize(String datapackageSize)
	{
		this.datapackageSize = datapackageSize;
	}

	public String getDatadescriptionString()
	{
		return datadescriptionString;
	}

	public void setDatadescriptionString(String datadescriptionString)
	{
		this.datadescriptionString = datadescriptionString;
	}

	public String getDatadescriptionSize()
	{
		return datadescriptionSize;
	}

	public void setDatadescriptionSize(String datadescriptionSize)
	{
		this.datadescriptionSize = datadescriptionSize;
	}
}
