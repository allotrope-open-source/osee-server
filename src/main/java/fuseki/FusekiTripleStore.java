package fuseki;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdfconnection.RDFConnection;
import org.apache.jena.rdfconnection.RDFConnectionFuseki;
import org.apache.jena.update.UpdateAction;
import org.jutils.jprocesses.JProcesses;
import org.jutils.jprocesses.model.ProcessInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import adfsee.TempFolderCreator;
import processor.AdfReader;
import processor.util.RdfUtil;
import processor.util.RunUtil;
import query.Queries;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 */
public class FusekiTripleStore {

	public static final String FusekiDataFolder = TempFolderCreator.TRIPLE_STORE_FOLDER_PATH_NAME + "run";

	public static Logger logger = LoggerFactory.getLogger("log");

	public static final String MERGED = "http://localhost/adfsee/merged";
	public static final String INSTANCES = "http://localhost/adfsee/instances";
	public static final String VOCABULARY = "http://localhost/adfsee/vocabulary";
	private static final String fusekiPort = "9999";
	private static final String datasetName = "adfsee";
	private static final String endpoint = "http://localhost:" + fusekiPort + "/" + datasetName;
	public static RDFConnection connection = null;
	private static Dataset dataset;

	public static void initializeTripleStore() {
		initConnectionsAndDataset();

		try {
			logger.info("Initialized Fuseki with dataset: " + datasetName);
			try (QueryExecution qe = connection.query(Queries.initMessage)) {
				logger.info(qe.execSelect().next().get("s").toString());
			}

			dropGraphs();
		} catch (Exception e) {
			logger.error("Error initializing Fuseki triple store");
			e.printStackTrace();
		}
	}

	private static void initConnectionsAndDataset() {
		if (connection != null) {
			connection.close();
		}
		connection = RDFConnectionFuseki.create().destination(endpoint).build();

		if (dataset != null) {
			dataset.end();
		}
		dataset = connection.fetchDataset();
	}

	private static void dropGraphs() {
		List<String> graphNames = collectGraphNames();

		if (!graphNames.isEmpty()) {
			for (Iterator<String> iterator = graphNames.iterator(); iterator.hasNext();) {
				String graphName = (String) iterator.next();
				logger.info("Drop graph <" + graphName + ">");
				try {
					dataset.begin(ReadWrite.WRITE);
					UpdateAction.parseExecute("DROP GRAPH <" + graphName + ">", dataset);
					dataset.commit();
				} catch (Exception e) {
					logger.error("Error dropping graph: " + graphName);
					e.printStackTrace();
				}
			}
		}

		clearDefaultGraph();
	}

	private static void clearDefaultGraph() {
		try {
			logger.info("Clear default graph...");
			dataset.begin(ReadWrite.WRITE);
			UpdateAction.parseExecute(Queries.deleteAllTriples, dataset);
			dataset.commit();
			logger.info("Cleared default graph.");
		} catch (Exception e) {
			logger.error("Error clearing default graph.");
			e.printStackTrace();
		}
	}

	private static List<String> collectGraphNames() {
		List<String> graphNames = new ArrayList<String>();
		try (QueryExecution qe = connection.query(Queries.queryForGraphs)) {
			ResultSet rs = qe.execSelect();
			while (rs.hasNext()) {
				QuerySolution querySolution = (QuerySolution) rs.next();
				String graphName = querySolution.get("g").toString();
				graphNames.add(graphName);
			}
		}
		return graphNames;
	}

	private static void clearGraph(String graphName) {
		try {
			dataset.begin(ReadWrite.WRITE);
			UpdateAction.parseExecute("DROP GRAPH <" + graphName + ">", dataset);
			dataset.commit();
			logger.info("Drop graph <" + graphName + ">");
		} catch (Exception e) {
			logger.error("Error dropping graph: " + graphName);
			e.printStackTrace();
		}
	}

	public static void cleanupTripleStore() {
		try {
			logger.info("Clear all graphs in dataset " + datasetName);
			dropGraphs();
		} catch (Exception e) {
			logger.error("Exception during clearing existing graphs in namespace " + datasetName);
			e.printStackTrace();
		}
	}

	public static void loadTriples(Model model, Model externalResources) {
		try {
			FusekiTripleStore.loadToTripleStore(model, externalResources);
		} catch (Exception e) {
			logger.error("Error during loading triples to triple store.");
			e.printStackTrace();
		}

	}

	public static void loadAdfToTripleStore(Path adfPath) {
		Map<String, Model> graphMap = AdfReader.extractDataDescription(adfPath, logger);
		if (graphMap.isEmpty()) {
			logger.error("No graphs were extracted from ADF: " + adfPath.toAbsolutePath().toString());
			return;
		}
		
		initConnectionsAndDataset();
		doQueryForNumberOfAllTriples();

		// first clear graphs
		try {
			for (Entry<String, Model> entry : graphMap.entrySet()) {
				String graphName = entry.getKey();
				if (!graphName.equals(RdfUtil.defaultGraph)) {
					if (namedGraphExists(graphName)) {
						logger.info("Graph " + graphName + " exists. Dropping now...");
						try {
							dataset.begin(ReadWrite.WRITE);
							UpdateAction.parseExecute("DROP GRAPH <" + graphName + ">", dataset);
							dataset.commit();
							logger.info("Dropped graph " + graphName);
						} catch (Exception e) {
							logger.error("Error dropping graph: " + graphName);
							e.printStackTrace();
						}
					}
				} else {
					logger.info("Clearing default graph...");
					try {
						dataset.begin(ReadWrite.WRITE);
						UpdateAction.parseExecute(Queries.deleteAllTriples, dataset);
						dataset.commit();
						logger.info("Cleared default graph.");
					} catch (Exception e) {
						logger.error("Error clearing default graph.");
						e.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error clearing graphs.");
			e.printStackTrace();
		}

		doQueryForNumberOfAllTriples();

		// Load triples to dataset
		try {
			for (Entry<String, Model> entry : graphMap.entrySet()) {
				String graphName = entry.getKey();
				Model model = entry.getValue();
				if (!graphName.equals(RdfUtil.defaultGraph)) {
					try {
						connection.load(graphName, model);
						logger.info("Loaded triples to graph " + graphName + ". #triples = " + model.size());
					} catch (Exception e) {
						logger.error("Error loading triples to graph: " + graphName);
					}
				} else {
					try {
						connection.load(null, model);
						logger.info("Loaded triples to default graph. #triples = " + model.size());
					} catch (Exception e) {
						logger.error("Error loading triples to default graph.");
						e.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error during loading triples to database.");
			e.printStackTrace();
		}

		doQueryForNumberOfAllTriples();
	}

	private static void doQueryForNumberOfAllTriples() {
		logger.info("Counting all triples...");
		try (QueryExecution qe = connection.query(Queries.queryForNumberOfAllTriples)) {
			ResultSet rs = qe.execSelect();
			if (rs.hasNext()) {
				while (rs.hasNext()) {
					QuerySolution querySolution = (QuerySolution) rs.next();
					String totalTriples = querySolution.get("num").toString();
					logger.info("Total number of triples in triplestore: " + totalTriples);
				}
			} else {
				logger.info("Query for total number of all triples returned no result.");
			}
		} catch (Exception e) {
			logger.error("Error querying for all triples after clearing graphs.");
			e.printStackTrace();
		}
	}

	public static void loadToGraphInSameThread(String graphName, Model model) {
		try {
			FusekiTripleStore.loadToGraph(graphName, model);
		} catch (Exception e) {
			logger.error("Error during loading triples to graph: " + graphName);
			e.printStackTrace();
		}

	}

	public static void loadToGraph(String graphName, Model model) {
		logger.info("Loading triples to graph: " + graphName);

		try {
			// first clear
			if (!graphName.equals(RdfUtil.defaultGraph)) {
				if (namedGraphExists(graphName)) {
					logger.info("Graph " + graphName + " exists. Clearing now...");
					clearGraph(graphName);
				}
			} else {
				clearDefaultGraph();
			}
		} catch (Exception e) {
			logger.error("Error clearing graph: " + graphName);
			e.printStackTrace();
		}

		// then load
		try {
			if (!graphName.equals(RdfUtil.defaultGraph)) {
				connection.load(graphName, model);
				logger.info("Loaded triples to graph " + graphName + ". #triples = " + model.size());
			} else {
				connection.load(null, model);
				logger.info("Loaded triples to default graph. #triples = " + model.size());
			}
		} catch (Exception e) {
			logger.error("Error loading triples to graph: " + graphName);
			e.printStackTrace();
		}
	}

	private static boolean namedGraphExists(String graphUri) throws Exception {
		List<String> graphNames = collectGraphNames();
		if (graphNames.contains(graphUri)) {
			return true;
		}

		return false;
	}

	public static void loadToTripleStore(Model model, Model externalResources) {
		logger.info("Loading triples to triple store.");

		clearGraphSafe(INSTANCES);
		clearGraphSafe(VOCABULARY);
		clearGraphSafe(MERGED);

		try {
			logger.info("Loading instance data to triple store...");
			connection.load(INSTANCES, model);
			logger.info("Loaded instance data to graph " + INSTANCES + ". #triples = " + model.size());
			logger.info("Loading vocabulary to triple store...");
			connection.load(VOCABULARY, externalResources);
			logger.info("Loaded vocabulary data to graph " + VOCABULARY + ". #triples = " + externalResources.size());
			logger.info("Loading merged instance data and vocabulary to triple store...");
			connection.load(MERGED, model);
			connection.load(MERGED, externalResources);
			logger.info("Loaded instance data and vocabulary to graph " + MERGED + " #triples = " + String.valueOf(model.size()
					+ externalResources.size()));
			doQueryForNumberOfAllTriples();
		} catch (Exception e) {
			logger.error("Exception during loading triples to triple store.");
			e.printStackTrace();
		}
	}

	private static void clearGraphSafe(String graphName) {
		try {
			if (namedGraphExists(graphName)) {
					logger.info("Graph " + graphName + " exists. Clearing now...");
					clearGraph(graphName);
				}
		} catch (Exception e) {
			logger.error("Error clearing graph: " + graphName);
			e.printStackTrace();
		}
	}

	public void run() {
		if (RunUtil.available(9999)) {
			removeFusekiFiles();

			startUp();
		} else {
			logger.info("Fuseki is already running on port 9999. Trying to stop the orphan process. Please wait...");

			ProcessInfo fusekiProcessInfo = RunUtil.getProcessInfo("fuseki-server.jar");

			if (fusekiProcessInfo != null && StringUtils.isNotEmpty(fusekiProcessInfo.getPid())) {
				int pid = Integer.valueOf(fusekiProcessInfo.getPid());
				boolean success = JProcesses.killProcessGracefully(pid).isSuccess();
				if (!success) {
					logger.info("Could not stop process " + pid + " gracefully.");
					success = JProcesses.killProcess(pid).isSuccess();
					if (!success) {
						logger.info("Could not kill process " + pid);
						throw new IllegalStateException(
								"Unable to start triple store because there is already a process of Fuseki running with process ID "
										+ pid);
					} else {
						logger.info("Fuseki process " + pid + " was killed.");
					}
				} else {
					logger.info("Fuseki process " + pid + " was stopped gracefully.");
				}

				if (RunUtil.available(9999)) {
					removeFusekiFiles();
					startUp();
				} else {
					throw new IllegalStateException("Process with id " + pid
							+ " was stopped but port 9999 is still blocked. Please try a fresh startup.");
				}
			} else {
				throw new IllegalStateException("Unable to start triple store because port 9999 is blocked!");
			}
		}
	}

	private void removeFusekiFiles() {
		try {
			if (Files.exists(Paths.get(FusekiDataFolder), LinkOption.NOFOLLOW_LINKS)) {
				FileUtils.cleanDirectory(Paths.get(FusekiDataFolder).toFile());
			}
		} catch (IOException e) {
			logger.error("Exception caught during removal of existing Fuseki data folder: " + FusekiDataFolder);
			e.printStackTrace();
		}
	}

	private void startUp() {
		Thread thread = new Thread(() -> {
			try {
				boolean isDebug = java.lang.management.ManagementFactory.getRuntimeMXBean().getInputArguments()
						.toString().indexOf("-agentlib:jdwp") > 0;

				ProcessBuilder processBuilder = new ProcessBuilder();

				if (isDebug) {
					processBuilder.command("java", //
							"-Xmx4g", //
							"-jar", //
							"-Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005", //
							TempFolderCreator.TRIPLE_STORE_FOLDER_PATH_NAME + "/fuseki-server.jar", //
							"--update", //
							"--config=config.ttl", //
							"--port=9999", //
							"--verbose");
				} else {
					processBuilder.command("java", //
							"-Xmx4g", //
							"-jar", //
							TempFolderCreator.TRIPLE_STORE_FOLDER_PATH_NAME + "/fuseki-server.jar", //
							"--update", //
							"--config=config.ttl", //
							"--port=9999", //
							"--verbose");
				}

				processBuilder.directory(TempFolderCreator.TRIPLE_STORE_FOLDER_PATH.toFile());

				Process fusekiProcess = null;

				try {
					fusekiProcess = processBuilder.start();
				} catch (Exception e) {
					logger.error("ADFSee exits, shutting down Fuseki as well. ", e);
					if (fusekiProcess != null) {
						fusekiProcess.destroyForcibly();
					}
				}
			} catch (Exception e) {
				logger.error("Error during startup of Fuseki.");
				e.printStackTrace();
			}
		});
		logger.info("Launching Fuseki triple store in separate thread " + thread.getName());

		thread.start();

		logger.info("Waiting 2 secs to let Fuseki start up then initializing.");
		final ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1);
		executor.schedule(new Runnable() {
			@Override
			public void run() {
				logger.info("Initializing triple store.");
				FusekiTripleStore.initializeTripleStore();
			}
		}, 2000, TimeUnit.MILLISECONDS);
	}

	public static void startInNewThread() {
		try {
			class FusekiLoaderThread extends Thread {
				@Override
				public void run() {
					FusekiTripleStore fusekiTripleStore = new FusekiTripleStore();
					fusekiTripleStore.run();
				}
			}

			FusekiLoaderThread loaderThread = new FusekiLoaderThread();
			loaderThread.start();
		} catch (Exception e) {
			logger.error("Error during starting up triple store.");
			e.printStackTrace();
		}
	}
}
