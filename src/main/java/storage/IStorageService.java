package storage;

import java.nio.file.Path;
import java.util.stream.Stream;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

/**
 * based on https://github.com/b1nnar/examples-spring-boot/blob/master/src/main/java/ro/binnar/projects/alfa/storage/FileSystemStorageService.java
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public interface IStorageService
{

	void init();

	void store(MultipartFile file);

	Stream<Path> loadAll();

	Path load(String filename);

	Resource loadAsResource(String filename);

	void deleteAll();

}