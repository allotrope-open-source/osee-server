package storage;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("storage")
/**
 * based on https://github.com/b1nnar/examples-spring-boot/blob/master/src/main/java/ro/binnar/projects/alfa/storage/FileSystemStorageService.java
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class StorageProperties
{
	public static final String location = "upload";
	public static final String downloadLocation = "download";

	public String getLocation()
	{
		return location;
	}

	public String getDownloadLocation()
	{
		return downloadLocation;
	}
}
