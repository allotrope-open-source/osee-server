package skolemizer;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Objects;

import org.allotrope.adf.audit.service.ChangeCapture;
import org.allotrope.adf.model.AdfFile;
import org.allotrope.adf.service.AdfService;
import org.allotrope.adf.service.AdfServiceFactory;
import org.allotrope.datashapes.foaf.model.FoafModel;
import org.allotrope.datashapes.foaf.model.Person;
import org.allotrope.datashapes.prov.model.Entity;
import org.allotrope.datashapes.prov.model.ProvenanceModel;

import com.hp.hpl.jena.graph.Graph;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.sparql.core.DatasetGraph;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

import processor.Processor;

public class ADFUnskolemizer
{
	public static void createAdfWithUnskolemizedGraph(String adfFileName, String skolemizedGraphIRI) throws IOException
	{
		Processor.log.info("Unskolemizing ADF " + adfFileName);
		Objects.requireNonNull(adfFileName);
		AdfService adfService = AdfServiceFactory.create();
		try (AdfFile adfFile = adfService.openFile(Paths.get(adfFileName)))
		{
			Node skolemMapGraphIri = NodeFactory.createURI("adf://dd/skolemMap");
			Node skolemizedSourceGraphIri = NodeFactory.createURI(skolemizedGraphIRI != null ? skolemizedGraphIRI : "urn:x-arq:DefaultGraph");
			DatasetGraph ds = adfFile.getDataset().asDatasetGraph();
			Node unskolemizedGraphIri = NodeFactory.createURI(skolemizedSourceGraphIri + "-unskolemized");
			Graph skolemMapGraph = null;
			ChangeCapture audit = null;
			if (adfFile.isAuditTrailActive())
			{
				Person person = FoafModel.person("noreply@allotrope.org").description("Allotrope API").build();
				Entity software = ProvenanceModel.entity("https://gitlab.com/allotrope-open-source/adfsee").build();
				audit = adfFile.getAuditTrailService().startCuration(person, "unskolemized graph of data description", software);
			}
			skolemMapGraph = Util.openOrCreateNamedGraph(ds, skolemMapGraphIri);
			skolemMapGraph.add(new Triple(skolemMapGraphIri, Util.PAV_DERIVED_FROM, skolemizedSourceGraphIri));

			Skolemizer skolemizer = new Skolemizer();
			Graph skolemizedSourceGraph = Util.openOrCreateNamedGraph(ds, skolemizedSourceGraphIri);
			Graph targetUnskolemizedGraph = Util.openOrCreateNamedGraph(ds, unskolemizedGraphIri);
			skolemizer.unskolemize(skolemizedSourceGraph, targetUnskolemizedGraph, skolemMapGraph);

			skolemizedSourceGraph.clear();

			ExtendedIterator<Triple> tripleIterator = targetUnskolemizedGraph.find(Triple.ANY);
			while (tripleIterator.hasNext())
			{
				Triple triple = tripleIterator.next();
				skolemizedSourceGraph.add(triple);
			}

			ds.removeGraph(unskolemizedGraphIri);

			if (audit != null)
			{
				audit.commit();
			}

		}
		catch (Exception e)
		{
			Processor.log.error("Error opening ADF for unskolemizing.");
			e.printStackTrace();
		}

	}
}
