package skolemizer;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Objects;

import org.allotrope.adf.audit.service.ChangeCapture;
import org.allotrope.adf.model.AdfFile;
import org.allotrope.adf.service.AdfService;
import org.allotrope.adf.service.AdfServiceFactory;
import org.allotrope.datashapes.foaf.model.FoafModel;
import org.allotrope.datashapes.foaf.model.Person;
import org.allotrope.datashapes.prov.model.Entity;
import org.allotrope.datashapes.prov.model.ProvenanceModel;

import com.hp.hpl.jena.graph.Graph;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.sparql.core.DatasetGraph;
import com.hp.hpl.jena.sparql.graph.GraphFactory;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

import processor.Processor;

public class ADFSkolemizer
{
	public static Model skolemizeGraph(String adfFileName, String graphIRI, boolean isExport) throws IOException
	{
		Processor.log.info("Skolemizing ADF " + adfFileName);
		Objects.requireNonNull(adfFileName);
		graphIRI = (graphIRI != null) ? graphIRI : "urn:x-arq:DefaultGraph";
		AdfService adfService = AdfServiceFactory.create();
		try (AdfFile adfFile = adfService.openFile(Paths.get(adfFileName)))
		{
			Node skolemMapGraphIRI = NodeFactory.createURI("adf://dd/skolemMap");
			Node unskolemizedGraphIri = NodeFactory.createURI(graphIRI);
			DatasetGraph ds = adfFile.getDataset().asDatasetGraph();
			Node skolemizedGraphIri = NodeFactory.createURI(graphIRI + "-skolemized");
			Graph sourceUnskolemizedGraph = null;
			Graph skolemMapGraph = null;
			ChangeCapture audit = null;
			if (adfFile.isAuditTrailActive())
			{
				Person person = FoafModel.person("noreply@allotrope.org").description("Allotrope API").build();
				Entity software = ProvenanceModel.entity("https://gitlab.com/allotrope-open-source/adfsee").build();
				audit = adfFile.getAuditTrailService().startCuration(person, "skolemization graph stored in ADF", software);
			}
			sourceUnskolemizedGraph = Util.openOrCreateNamedGraph(ds, unskolemizedGraphIri);
			skolemMapGraph = Util.openOrCreateNamedGraph(ds, skolemMapGraphIRI);
			skolemMapGraph.add(new Triple(skolemMapGraphIRI, Util.PAV_DERIVED_FROM, unskolemizedGraphIri));

			Graph targetSkolemizedGraph = Util.openOrCreateNamedGraph(ds, skolemizedGraphIri);

			Skolemizer skolemizer = new Skolemizer();
			skolemizer.skolemize(targetSkolemizedGraph, sourceUnskolemizedGraph, skolemMapGraph);

			if (isExport)
			{
				// default behaviour, export skolemized graphs from the data description
				if (audit != null)
				{
					audit.commit();
				}

				Model model = ModelFactory.createModelForGraph(targetSkolemizedGraph);

				return model;
			}
			else
			{
				// for testing purposes (use checkbox skolemize)
				// remove the unskolemized graph and replace it with the skolemized one within the data description
				sourceUnskolemizedGraph.clear();
				ExtendedIterator<Triple> tripleIterator = targetSkolemizedGraph.find(Triple.ANY);
				while (tripleIterator.hasNext())
				{
					Triple triple = tripleIterator.next();
					sourceUnskolemizedGraph.add(triple);
				}
				ds.removeGraph(skolemizedGraphIri);

				if (audit != null)
				{
					audit.commit();
				}

				return null;
			}
		}
		catch (Exception e)
		{
			Processor.log.error("Error processing ADF for skolemization.");
			e.printStackTrace();
		}

		return null;
	}

	public static Model skolemizeModelForExport(String graphIRI, DatasetGraph datasetGraph, AdfFile adfFile, String rdfFormat)
	{
		try
		{
			Processor.log.info("Skolemizing for export: " + graphIRI + " (" + rdfFormat + ")");
			Objects.requireNonNull(graphIRI);
			Objects.requireNonNull(datasetGraph);
			Objects.requireNonNull(adfFile);

			Node skolemMapGraphIRI = NodeFactory.createURI("adf://dd/skolemMap");
			Node unskolemizedGraphIri = NodeFactory.createURI(graphIRI);

			Graph sourceUnskolemizedGraph = null;
			Graph skolemMapGraph = null;

			ChangeCapture audit = null;
			if (adfFile.isAuditTrailActive())
			{
				Person person = FoafModel.person("noreply@allotrope.org").description("Allotrope API").build();
				Entity software = ProvenanceModel.entity("https://gitlab.com/allotrope-open-source/adfsee").build();
				audit = adfFile.getAuditTrailService().startCuration(person, "skolemization graph stored in ADF", software);
			}

			sourceUnskolemizedGraph = Util.openOrCreateNamedGraph(datasetGraph, unskolemizedGraphIri);
			skolemMapGraph = Util.openOrCreateNamedGraph(datasetGraph, skolemMapGraphIRI);
			skolemMapGraph.add(new Triple(skolemMapGraphIRI, Util.PAV_DERIVED_FROM, unskolemizedGraphIri));

			Graph targetSkolemizedGraph = GraphFactory.createDefaultGraph();

			Skolemizer skolemizer = new Skolemizer();
			skolemizer.skolemize(targetSkolemizedGraph, sourceUnskolemizedGraph, skolemMapGraph);

			if (audit != null)
			{
				audit.commit();
			}

			Model model = ModelFactory.createModelForGraph(targetSkolemizedGraph);

			return model;
		}
		catch (Exception e)
		{
			Processor.log.info("Error during skolemization of graph " + graphIRI + " for export.");
			e.printStackTrace();
		}

		return null;
	}
}
