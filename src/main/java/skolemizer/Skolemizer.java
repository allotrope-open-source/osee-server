package skolemizer;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import com.hp.hpl.jena.graph.Graph;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * Helper to produce skolemized blank nodes from existing graphs while maintaining a mapping graph that associates blank nodes with the skolem IRIs
 *
 */
public class Skolemizer
{
	private String skolemIRIPrefix = "http://allotrope.org/";
	private Node hasSkolemIRI = NodeFactory.createURI("http://www.w3.org/2002/07/owl#sameAs");
	private int maxCacheSize = 5000;

	public Node getHasSkolemIRI()
	{
		return hasSkolemIRI;
	}

	public void setHasSkolemIRI(Node hasSkolemIRI)
	{
		this.hasSkolemIRI = hasSkolemIRI;
	}

	public int getMaxCacheSize()
	{
		return maxCacheSize;
	}

	public void setMaxCacheSize(int maxCacheSize)
	{
		this.maxCacheSize = maxCacheSize;
	}

	private Map<String, Node> cache = new LinkedHashMap<String, Node>(maxCacheSize + 1, .75F, true)
	{
		private static final long serialVersionUID = -2503258166378264600L;

		@Override
		public boolean removeEldestEntry(Map.Entry<String, Node> eldest)
		{
			return size() > maxCacheSize;
		}
	};

	public String getSkolemIRIPrefix()
	{
		return skolemIRIPrefix;
	}

	public void setSkolemIRIPrefix(String skolemIRIPrefix)
	{
		this.skolemIRIPrefix = skolemIRIPrefix;
	}

	private String newSkolemIRI()
	{
		String newUUID = UUID.randomUUID().toString().replace("-", "");
		return skolemIRIPrefix + ".well-known/genid/" + newUUID;
	}

	private Node getOrCreateSkolemizedNode(Node blankNode, Graph skolemIRIMappingGraph)
	{
		Node skolemized = null;
		String anon = blankNode.getBlankNodeLabel();
		skolemized = cache.get(anon);
		if (skolemized == null)
		{
			ExtendedIterator<Triple> it = skolemIRIMappingGraph.find(blankNode, hasSkolemIRI, Node.ANY);
			if (it.hasNext())
			{
				Triple t = it.next();
				skolemized = t.getObject();
				cache.put(anon, skolemized);
			}
			else
			{
				skolemized = NodeFactory.createURI(newSkolemIRI());
				skolemIRIMappingGraph.add(new Triple(blankNode, hasSkolemIRI, skolemized));
				cache.put(anon, skolemized);
			}
		}
		return skolemized;
	}

	private Node getOrCreateBNode(Node skolemizedNode, Graph skolemIRIMappingGraph)
	{
		Node anon = null;
		String skolemizedUri = skolemizedNode.getURI();
		Node skolemized = NodeFactory.createURI(skolemizedUri);
		anon = cache.get(skolemizedUri);
		if (anon == null)
		{
			ExtendedIterator<Triple> it = skolemIRIMappingGraph.find(Node.ANY, hasSkolemIRI, skolemized);
			if (it.hasNext())
			{
				Triple t = it.next();
				anon = t.getSubject();
				if (anon.isBlank())
				{
					cache.put(skolemizedUri, anon);
				}
				else
				{
					cache.put(skolemizedUri, skolemized); // same as applied to non blank node, should be an error
				}
			}
			else
			{
				anon = NodeFactory.createAnon();
				skolemIRIMappingGraph.add(new Triple(anon, hasSkolemIRI, skolemized));
				cache.put(skolemizedUri, anon);
			}
		}
		return anon;
	}

	public void skolemize(Graph skolemized, Graph graph, Graph skolemIRIMappingGraph)
	{
		graph.find(Node.ANY, Node.ANY, Node.ANY).forEachRemaining(t -> {
			Node s = t.getSubject();
			Node p = t.getPredicate();
			Node o = t.getObject();
			Node sk = null;
			Node ok = null;
			if (s.isBlank())
			{
				sk = getOrCreateSkolemizedNode(s, skolemIRIMappingGraph);
			}
			else
			{
				sk = s;
			}
			if (o.isBlank())
			{
				ok = getOrCreateSkolemizedNode(o, skolemIRIMappingGraph);
			}
			else
			{
				ok = o;
			}
			Triple outTriple = new Triple(sk, p, ok);
			skolemized.add(outTriple);
		});
	}

	public void unskolemize(Graph skolemized, Graph graph, Graph skolemIRIMappingGraph)
	{
		skolemized.find(Node.ANY, Node.ANY, Node.ANY).forEachRemaining(t -> {
			Node s = t.getSubject();
			Node p = t.getPredicate();
			Node o = t.getObject();
			Node sbn = null;
			Node obn = null;
			if (isSkolemized(s))
			{
				sbn = getOrCreateBNode(s, skolemIRIMappingGraph);
			}
			else
			{
				sbn = s;
			}
			if (isSkolemized(o))
			{
				obn = getOrCreateBNode(o, skolemIRIMappingGraph);
			}
			else
			{
				obn = o;
			}
			Triple outTriple = new Triple(sbn, p, obn);
			graph.add(outTriple);
		});
	}

	private boolean isSkolemized(Node node)
	{
		boolean isSkolemized = node.isURI() && node.getURI().matches("http://(.*)/.well-known/genid/(.*)");
		return isSkolemized;
	}
}
