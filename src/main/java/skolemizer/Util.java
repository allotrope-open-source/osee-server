package skolemizer;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import com.hp.hpl.jena.graph.Graph;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.sparql.core.DatasetGraph;
import com.hp.hpl.jena.sparql.graph.GraphFactory;

public class Util
{
	public static final Node PAV_DERIVED_FROM = NodeFactory.createURI("http://purl.org/pav/derivedFrom");
	public static final Node PAV_CREATED_ON = NodeFactory.createURI("http://purl.org/pav/createdOn");

	public static Graph openOrCreateNamedGraph(DatasetGraph ds, Node graphIRI)
	{
		Graph graph = ds.getGraph(graphIRI);
		if (graph == null)
		{
			graph = GraphFactory.createGraphMem();
			graph.add(new Triple(graphIRI, PAV_CREATED_ON, NodeFactory.createLiteral(ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ISO_INSTANT))));
			ds.addGraph(graphIRI, graph);
			graph = ds.getGraph(graphIRI);
		}
		return graph;

	}
}
