package processor.audit;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class Datatype extends AbstractAuditEntity
{
	public String order;
	public String size;

	public String getOrder()
	{
		return order;
	}

	public void setOrder(String order)
	{
		this.order = order;
	}

	public String getSize()
	{
		return size;
	}

	public void setSize(String size)
	{
		this.size = size;
	}
}
