package processor.audit;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class Component extends AbstractAuditEntity
{
	public String attachment;
	public String datatypeId;
	public String required;
	public String property;
	public String order;

	public String getAttachment()
	{
		return attachment;
	}

	public void setAttachment(String attachment)
	{
		this.attachment = attachment;
	}

	public String getDatatypeId()
	{
		return datatypeId;
	}

	public void setDatatypeId(String datatypeId)
	{
		this.datatypeId = datatypeId;
	}

	public String getRequired()
	{
		return required;
	}

	public void setRequired(String required)
	{
		this.required = required;
	}

	public String getProperty()
	{
		return property;
	}

	public void setProperty(String property)
	{
		this.property = property;
	}

	public String getOrder()
	{
		return order;
	}

	public void setOrder(String order)
	{
		this.order = order;
	}
}
