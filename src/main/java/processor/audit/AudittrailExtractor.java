package processor.audit;

import java.io.InputStream;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.allotrope.adf.audit.service.AdfAuditTrailService;
import org.allotrope.adf.dc.service.DataCubeService;
import org.allotrope.adf.dd.model.DataDescription;
import org.allotrope.adf.dp.model.DpFile;
import org.allotrope.adf.model.AdfFile;
import org.allotrope.adf.service.AdfService;
import org.allotrope.adf.service.AdfServiceFactory;
import org.allotrope.datashapes.audit.model.AuditRecord;
import org.allotrope.datashapes.audit.model.AuditTrail;
import org.allotrope.datashapes.audit.model.ChangeSet;
import org.allotrope.datashapes.audit.model.DataUpdate;
import org.allotrope.datashapes.base.model.Resource;
import org.allotrope.datashapes.datacube.model.ComponentSpecification;
import org.allotrope.datashapes.datacube.model.DataSet;
import org.allotrope.datashapes.datacube.model.DataStructureDefinition;
import org.allotrope.datashapes.datacube.model.Measure;
import org.allotrope.datashapes.datacube.model.SelectionStructureDefinition;
import org.allotrope.datashapes.datapackage.model.File;
import org.allotrope.datashapes.datapackage.model.Folder;
import org.allotrope.datashapes.datapackage.model.Segment;
import org.allotrope.datashapes.foaf.model.Person;
import org.allotrope.datashapes.hdf.enums.FillTime;
import org.allotrope.datashapes.hdf.enums.FillValueStatus;
import org.allotrope.datashapes.hdf.enums.ScaleType;
import org.allotrope.datashapes.hdf.enums.StorageAllocation;
import org.allotrope.datashapes.hdf.model.Dataspace;
import org.allotrope.datashapes.hdf.model.DataspaceDimension;
import org.allotrope.datashapes.hdf.model.Datatype;
import org.allotrope.datashapes.hdf.model.DimensionSelection;
import org.allotrope.datashapes.prov.model.Activity;
import org.allotrope.datashapes.prov.model.Agent;
import org.allotrope.datashapes.prov.model.Attribution;
import org.allotrope.datashapes.prov.model.Entity;
import org.allotrope.datashapes.prov.model.Revision;
import org.allotrope.datashapes.prov.model.Role;
import org.allotrope.datashapes.prov.model.Usage;
import org.allotrope.datashapes.rdf.Object2RDF;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.iri.IRIFactory;
import org.apache.logging.log4j.Logger;

import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;

import processor.Rdf2Cyto;
import processor.util.RdfUtil;
import processor.util.Vocabulary;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class AudittrailExtractor
{
	private static Object2RDF object2rdf = new Object2RDF();
	private static com.osthus.jena.repackaged.iri.IRIFactory iriFactory = com.osthus.jena.repackaged.iri.IRIFactory.iriImplementation();

	public static List<ExtractedAudittrail> extractFrom(Path path, org.apache.jena.rdf.model.Model modelWithAdf, org.apache.jena.rdf.model.Model externalResources, Logger log)
	{
		log.info("Extracting audit trail...");
		List<ExtractedAudittrail> extractedAudittrails = new ArrayList<ExtractedAudittrail>();
		try
		{
			AdfService adfService = AdfServiceFactory.create();
			try (AdfFile adfFile = adfService.openFile(path, true))
			{
				DataCubeService dataCubeService = adfFile.getDataCubeService();
				AdfAuditTrailService auditTrailService = adfFile.getAuditTrailService();
				if (auditTrailService == null || !auditTrailService.hasAuditTrail())
				{
					log.info("ADF file does not contain any audit trails.");
					return extractedAudittrails;
				}

				AuditTrail[] auditTrails = auditTrailService.getAuditTrails();
				for (int i = 0; i < auditTrails.length; i++)
				{
					AuditTrail trail = auditTrails[i];
					log.info("audit trail " + i + " with id: \"" + trail.id().get() + "\" title: \"" + trail.title() + "\" label: \"" + trail.label() + "\" description: \"" + trail.description()
							+ "\" of: \"" + trail.auditTrailOf().id().get() + "\"");
					ExtractedAudittrail extractedAudittrail = new ExtractedAudittrail();
					extractedAudittrail.setId(trail.id().get());
					extractedAudittrail.setLabel(trail.label());
					extractedAudittrail.setTitle(trail.title());
					extractedAudittrail.setDescription(trail.description());
					extractedAudittrail.setParentId(trail.auditTrailOf().id().get());

					Set<AuditRecord> auditRecords = trail.auditRecords();
					List<processor.audit.AuditRecord> extractedAuditRecords = new ArrayList<processor.audit.AuditRecord>();
					for (Iterator<AuditRecord> iterator = auditRecords.iterator(); iterator.hasNext();)
					{
						AuditRecord record = iterator.next();
						record = auditTrailService.getAuditRecord(record.id().get()); // make sure object has initialized properties
						log.info(" --> audit record with id: \"" + record.id().get() + "\" title: \"" + record.title() + "\" label: \"" + record.label() + "\" description: \"" + record.description()
								+ "\" of: \"" + record.auditTrail().id().get() + "\" with status: \"" + record.status().name() + "\"");
						processor.audit.AuditRecord extractedAuditRecord = new processor.audit.AuditRecord();
						extractedAuditRecord.setId(record.id().get());
						extractedAuditRecord.setTitle(record.title());
						extractedAuditRecord.setLabel(record.label());
						extractedAuditRecord.setDescription(record.description());
						extractedAuditRecord.setParentId(record.auditTrail().id().get());
						extractedAuditRecord.setStatus(record.status().name());

						String auditRecordIri = record.id().get();
						Revision auditRecordRevision = auditTrailService.getAuditRecordRevision(auditRecordIri);
						log.info("     --> revision id: \"" + auditRecordRevision.id().get() + "\" title: \"" + auditRecordRevision.title() + "\" label: \"" + auditRecordRevision.label()
								+ "\" description: \"" + auditRecordRevision.description() + "\"");
						processor.audit.Revision extractedRevision = new processor.audit.Revision();
						extractedRevision.setId(auditRecordRevision.id().get());
						extractedRevision.setTitle(auditRecordRevision.title());
						extractedRevision.setLabel(auditRecordRevision.label());
						extractedRevision.setDescription(auditRecordRevision.description());

						Activity auditRecordActivity = auditTrailService.getAuditRecordActivity(auditRecordIri);
						String toolTitle = StringUtils.EMPTY;
						if (auditRecordActivity != null)
						{
							Set<Usage> qualifiedUsages = auditRecordActivity.qualifiedUsages();
							for (Iterator<Usage> qualifiedUsagesIterator = qualifiedUsages.iterator(); qualifiedUsagesIterator.hasNext();)
							{
								Usage usage = qualifiedUsagesIterator.next();
								if (usage.hadRole() != null && usage.hadRole().types().contains(IRIFactory.iriImplementation().create("http://purl.allotrope.org/ontologies/audit#Tool")))
								{
									Entity entity = usage.entity();
									toolTitle = entity.title();
								}
							}
							String startDate = StringUtils.EMPTY;
							if (auditRecordActivity.startedAtTime() != null)
							{
								SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
								startDate = sdf.format(auditRecordActivity.startedAtTime().getTime());
							}
							String endDate = StringUtils.EMPTY;
							if (auditRecordActivity.endedAtTime() != null)
							{
								SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
								endDate = sdf.format(auditRecordActivity.endedAtTime().getTime());
							}
							log.info("     --> activity id: \"" + auditRecordActivity.id().get() + "\" title: \"" + auditRecordActivity.title() + "\" label: \"" + auditRecordActivity.label()
									+ "\" description: \"" + auditRecordActivity.description() + "\" start time: \"" + startDate + "\" end time: \"" + endDate + "\" tool: \"" + toolTitle + "\"");

							processor.audit.Activity extractedActivity = new processor.audit.Activity();
							extractedActivity.setId(auditRecordActivity.id().get());
							extractedActivity.setTitle(auditRecordActivity.title());
							extractedActivity.setLabel(auditRecordActivity.label());
							extractedActivity.setStartDate(startDate);
							extractedActivity.setEndDate(endDate);
							extractedActivity.setTool(toolTitle);
							extractedAuditRecord.setActivity(extractedActivity);
						}

						Set<Attribution> auditRecordAttributions = auditTrailService.getAuditRecordAttributions(auditRecordIri);
						List<processor.audit.Attribution> extractedAttributions = new ArrayList<processor.audit.Attribution>();
						Dataset auditRecordDataset = auditTrailService.getAuditRecordDataset(auditRecordIri);
						if (auditRecordAttributions != null && !auditRecordAttributions.isEmpty())
						{
							for (Iterator<Attribution> auditRecordAttributionIterator = auditRecordAttributions.iterator(); auditRecordAttributionIterator.hasNext();)
							{
								Attribution attribution = auditRecordAttributionIterator.next();
								log.info("     --> attribution id: \"" + attribution.id().get() + "\" title: \"" + attribution.title() + "\" label: \"" + attribution.label() + "\" description: \""
										+ attribution.description() + "\"");
								processor.audit.Attribution extractedAttribution = new processor.audit.Attribution();
								extractedAttribution.setId(attribution.id().get());
								extractedAttribution.setTitle(attribution.title());
								extractedAttribution.setLabel(attribution.label());
								extractedAttribution.setDescription(attribution.description());

								Agent agent = attribution.agent();
								String agentIri = agent.id().get();
								Model auditModel = auditRecordDataset.getNamedModel(record.dataset().id().get());// auditModel.write(System.out, "TTL")

								Person person = (Person) object2rdf.read(auditModel.getGraph(), NodeFactory.createURI(agentIri), org.allotrope.datashapes.foaf.model.Person.class);
								if (person != null)
								{
									log.info("         --> person id: \"" + person.id().get() + "\" title: \"" + person.title() + "\" label: \"" + person.label() + "\" description: \""
											+ person.description() + "\" mail: \"" + (person.mbox() == null ? "" : person.mbox()) + "\" first name: \""
											+ (person.firstName() == null ? "" : person.firstName()) + "\" family name: \"" + (person.familyName() == null ? "" : person.familyName()) + "\"");

									processor.audit.Person extractedPerson = new processor.audit.Person();
									extractedPerson.setId(person.id().get());
									extractedPerson.setTitle(person.title());
									extractedPerson.setLabel(person.label());
									extractedPerson.setDescription(person.description());
									extractedPerson.setFirstName(person.firstName());
									extractedPerson.setFamilyName(person.familyName());
									extractedPerson.setMail((person.mbox() == null ? "" : person.mbox().toString()));

									Role role = attribution.hadRole();
									if (role != null)
									{
										log.info("         --> role id: \"" + role.id().get() + "\" title: \"" + role.title() + "\" label: \"" + role.label() + "\" description: \""
												+ role.description() + "\"");
										processor.audit.Role extractedRole = new processor.audit.Role();
										extractedRole.setId(role.id().get());
										extractedRole.setTitle(role.title());
										extractedRole.setLabel(role.label());
										extractedRole.setDescription(role.description());
										extractedPerson.setRole(extractedRole);
									}
									extractedAttribution.setPerson(extractedPerson);
								}
								extractedAttributions.add(extractedAttribution);
							}
							extractedAuditRecord.setAttributions(extractedAttributions);
						}

						Entity whatWasChanged = auditRecordRevision.revised();

						Model recordModel = auditRecordDataset.getNamedModel(record.dataset().id().get());
						org.allotrope.datashapes.prov.model.Entity changedEntity = (org.allotrope.datashapes.prov.model.Entity) object2rdf.read(recordModel.getGraph(),
								NodeFactory.createURI(whatWasChanged.id().get()), org.allotrope.datashapes.prov.model.Entity.class);

						List<String> agentNames = new ArrayList<String>(changedEntity.attributedTo().size());
						List<Agent> agents = new ArrayList<Agent>(changedEntity.attributedTo().size());
						Iterator<Agent> agentIterator = changedEntity.attributedTo().iterator();
						while (agentIterator.hasNext())
						{
							Agent agent = agentIterator.next();
							agentNames.add(agent.title());
							agents.add(agent);
						}
						log.info("     --> CHANGE id: \"" + changedEntity.id().get() + "\" title: \"" + changedEntity.title() + "\" label: \"" + changedEntity.label() + "\" description: \""
								+ changedEntity.description() + "\" attributed to: \"" + StringUtils.join(agentNames, ", ") + "\" date: \""
								+ (changedEntity.generatedAtTime() == null ? "" : changedEntity.generatedAtTime().getTime().toString()) + "\" by: \""
								+ (changedEntity.generatedBy() == null ? "" : changedEntity.generatedBy().title()) + "\"");

						Change extractedWhatWasChanged = new Change();
						extractedWhatWasChanged.setId(whatWasChanged.id().get());
						extractedWhatWasChanged.setTitle(whatWasChanged.title());
						extractedWhatWasChanged.setLabel(whatWasChanged.label());
						extractedWhatWasChanged.setDescription(whatWasChanged.description());
						extractedWhatWasChanged.setAttributedTo(agents);
						extractedRevision.setWhatWasChanged(extractedWhatWasChanged);
						extractedAuditRecord.setRevision(extractedRevision);

						// Data description changes
						ChangeSet ddChanges = auditTrailService.getAuditRecordDataDescriptionChanges(auditRecordIri);
						log.info("     --> DD changes id: \"" + ddChanges.id().get() + "\" title: \"" + ddChanges.title() + "\" label: \"" + ddChanges.label() + "\" description: \""
								+ ddChanges.description() + "\" motivated by: \"" + ddChanges.motivatedBy() + "\" subject: \""
								+ (ddChanges.subjectOfChange() == null ? "" : ddChanges.subjectOfChange().id().get()) + "\"");
						DataDescriptionChanges extractedDdChanges = new DataDescriptionChanges();
						extractedDdChanges.setId(ddChanges.id().get());
						extractedDdChanges.setTitle(ddChanges.title());
						extractedDdChanges.setLabel(ddChanges.label());
						extractedDdChanges.setDescription(ddChanges.description());
						extractedDdChanges.setMotivatedBy(ddChanges.motivatedBy());
						extractedDdChanges.setSubjectOfChange((ddChanges.subjectOfChange() == null ? "" : ddChanges.subjectOfChange().id().get()));

						List<processor.audit.Update> extractedUpdates = new ArrayList<processor.audit.Update>();
						Iterator<DataUpdate> updatesIterator = ddChanges.updates().iterator();
						while (updatesIterator.hasNext())
						{
							DataUpdate dataUpdate = updatesIterator.next();
							String oldValue = (dataUpdate.oldDataValue() == null ? "" : dataUpdate.oldDataValue().lexicalValue());
							String newValue = (dataUpdate.newDataValue() == null ? "" : dataUpdate.newDataValue().lexicalValue());
							String previous = (dataUpdate.previousUpdate() == null ? "" : dataUpdate.previousUpdate().id().get());
							log.info("         --> DD update id: \"" + dataUpdate.id().get() + "\" title: \"" + dataUpdate.title() + "\" label: \"" + dataUpdate.label() + "\" description: \""
									+ dataUpdate.description() + "\" old value: \"" + oldValue + "\" new value: \"" + newValue + "\" previous: \""
									+ (dataUpdate.target() == null ? "" : dataUpdate.target().id().get()) + "\" previous: \"" + previous + "\"");

							processor.audit.Update extractedUpdate = new processor.audit.Update();
							extractedUpdate.setId(dataUpdate.id().get());
							extractedUpdate.setTitle(dataUpdate.title());
							extractedUpdate.setLabel(dataUpdate.label());
							extractedUpdate.setDescription(dataUpdate.description());
							extractedUpdate.setNewValue(newValue);
							extractedUpdate.setOldValue(oldValue);
							extractedUpdate.setPrevious(previous);
							extractedUpdate.setTarget((dataUpdate.target() == null ? "" : dataUpdate.target().id().get()));

							Resource oldStatements = dataUpdate.oldData(); // this is the named graph in the audit record dataset capturing the old statements
							List<processor.audit.Statement> extractedOldStatements = new ArrayList<processor.audit.Statement>();
							Model oldStatementModel = auditRecordDataset.getNamedModel(oldStatements.id().get());
							StmtIterator stmtIterator = oldStatementModel.listStatements();
							while (stmtIterator.hasNext())
							{
								Statement statement = stmtIterator.next();
								processor.audit.Statement extractedStatement = determineExtractedStatement(statement, modelWithAdf, externalResources, log, true, dataUpdate.target().id().get());
								extractedOldStatements.add(extractedStatement);
							}
							extractedUpdate.setOldData(extractedOldStatements);

							Resource newStatements = dataUpdate.newData(); // this is the named graph in the audit record dataset capturing the added statements
							List<processor.audit.Statement> extractedNewStatements = new ArrayList<processor.audit.Statement>();
							Model addedStatementModel = auditRecordDataset.getNamedModel(newStatements.id().get());
							stmtIterator = addedStatementModel.listStatements();
							while (stmtIterator.hasNext())
							{
								Statement statement = stmtIterator.next();
								processor.audit.Statement extractedStatement = determineExtractedStatement(statement, modelWithAdf, externalResources, log, false, dataUpdate.target().id().get());
								extractedNewStatements.add(extractedStatement);
							}
							extractedUpdate.setNewData(extractedNewStatements);
							extractedUpdates.add(extractedUpdate);
						}
						extractedDdChanges.setUpdates(extractedUpdates);

						// Named graph changes
						List<processor.audit.DataDescriptionChange> extractedDdAdditions = new ArrayList<processor.audit.DataDescriptionChange>();
						Iterator<Resource> additionsIterator = ddChanges.additions().iterator();
						while (additionsIterator.hasNext())
						{
							Resource addition = additionsIterator.next();

							org.allotrope.datashapes.graph.model.Graph graph = (org.allotrope.datashapes.graph.model.Graph) object2rdf.read(recordModel.getGraph(),
									NodeFactory.createURI(addition.id().get()), org.allotrope.datashapes.graph.model.Graph.class);
							if (graph != null)
							{
								String graphId = graph.id().get();
								String graphTitle = graph.title();
								String graphLabel = graph.label();
								String graphDescription = graph.description();
								log.info("         --> DD named graph addition id: \"" + graphId + "\" title: \"" + graphTitle + "\" label: \"" + graphLabel + "\" description: \"" + graphDescription
										+ "\"");
								DataDescriptionChange extractedDdAddition = new DataDescriptionChange();
								extractedDdAddition.setId(graphId);
								extractedDdAddition.setTitle(graphTitle);
								extractedDdAddition.setLabel(graphLabel);
								extractedDdAddition.setDescription(graphDescription);
								extractedDdAdditions.add(extractedDdAddition);
							}
						}
						extractedDdChanges.setAdditions(extractedDdAdditions);

						List<processor.audit.DataDescriptionChange> extractedDdRemovals = new ArrayList<processor.audit.DataDescriptionChange>();
						Iterator<Resource> removalsIterator = ddChanges.removals().iterator();
						while (removalsIterator.hasNext())
						{
							Resource removal = removalsIterator.next();

							org.allotrope.datashapes.graph.model.Graph graph = (org.allotrope.datashapes.graph.model.Graph) object2rdf.read(recordModel.getGraph(),
									NodeFactory.createURI(removal.id().get()), org.allotrope.datashapes.graph.model.Graph.class);
							if (graph != null)
							{
								String graphId = graph.id().get();
								String graphTitle = graph.title();
								String graphLabel = graph.label();
								String graphDescription = graph.description();
								log.info("         --> DD named graph removal id: \"" + graphId + "\" title: \"" + graphTitle + "\" label: \"" + graphLabel + "\" description: \"" + graphDescription
										+ "\"");
								DataDescriptionChange extractedDdRemoval = new DataDescriptionChange();
								extractedDdRemoval.setId(graphId);
								extractedDdRemoval.setTitle(graphTitle);
								extractedDdRemoval.setLabel(graphLabel);
								extractedDdRemoval.setDescription(graphDescription);
								extractedDdRemovals.add(extractedDdRemoval);
							}
						}
						extractedDdChanges.setRemovals(extractedDdRemovals);
						extractedAuditRecord.setDdChanges(extractedDdChanges);

						// Datacube changes
						ChangeSet dcChanges = auditTrailService.getAuditRecordDataCubeChanges(auditRecordIri);
						log.info("     --> DC changes id: \"" + dcChanges.id().get() + "\" title: \"" + dcChanges.title() + "\" label: \"" + dcChanges.label() + "\" description: \""
								+ dcChanges.description() + "\" motivated by: \"" + dcChanges.motivatedBy() + "\" subject: \""
								+ (dcChanges.subjectOfChange() == null ? "" : dcChanges.subjectOfChange().id().get()) + "\"");

						DatacubeChanges extractedDcChanges = new DatacubeChanges();
						extractedDcChanges.setId(dcChanges.id().get());
						extractedDcChanges.setTitle(dcChanges.title());
						extractedDcChanges.setLabel(dcChanges.label());
						extractedDcChanges.setDescription(dcChanges.description());
						extractedDcChanges.setMotivatedBy(dcChanges.motivatedBy());
						extractedDcChanges.setSubjectOfChange((dcChanges.subjectOfChange() == null ? "" : dcChanges.subjectOfChange().id().get()));

						List<DatacubeChange> extractedDcAdditions = new ArrayList<DatacubeChange>();
						for (Resource dcAddition : dcChanges.additions())
						{
							String label = StringUtils.EMPTY;
							if (dataCubeService.openDataCube(iriFactory.create(dcAddition.id().get())) != null)
							{
								label = dataCubeService.openDataCube(iriFactory.create(dcAddition.id().get())).getLabel();
							}
							log.info("         --> DC addition id: \"" + dcAddition.id().get() + "\" label: \"" + label + "\"");

							DatacubeChange extractedDcAdditon = new DatacubeChange();
							extractedDcAdditon.setId(dcAddition.id().get());
							extractedDcAdditon.setDatacubeLabel(label);

							org.allotrope.datashapes.datacube.model.DataSet dataset = (org.allotrope.datashapes.datacube.model.DataSet) object2rdf.read(recordModel.getGraph(),
									NodeFactory.createURI(dcAddition.id().get()), org.allotrope.datashapes.datacube.model.DataSet.class);

							if (dataset != null)
							{
								String datasetId = dataset.id().get();
								String datasetTitle = dataset.title();
								String datasetLabel = dataset.label();
								String datasetDescription = dataset.description();
								log.info("             --> dataset id: \"" + datasetId + "\" title: \"" + datasetTitle + "\" label: \"" + datasetLabel + "\" description: \"" + datasetDescription
										+ "\"");
								processor.audit.Dataset extractedDcChangedDataset = new processor.audit.Dataset();
								extractedDcChangedDataset.setId(datasetId);
								extractedDcChangedDataset.setTitle(datasetTitle);
								extractedDcChangedDataset.setLabel(datasetLabel);
								extractedDcChangedDataset.setDescription(datasetDescription);

								DataStructureDefinition datasetStructure = dataset.structure();
								if (datasetStructure != null)
								{
									String datasetStructureId = datasetStructure.id().get();
									String datasetStructureTitle = datasetStructure.title();
									String datasetStructureLabel = datasetStructure.label();
									String datasetStructureDescription = datasetStructure.description();
									log.info("                 --> structure id: \"" + datasetStructureId + "\" title: \"" + datasetStructureTitle + "\" label: \"" + datasetStructureLabel
											+ "\" description: \"" + datasetStructureDescription + "\"");
									DataStructure extractedDataStructure = new DataStructure();
									extractedDataStructure.setId(datasetStructureId);
									extractedDataStructure.setTitle(datasetStructureTitle);
									extractedDataStructure.setLabel(datasetStructureLabel);
									extractedDataStructure.setDescription(datasetStructureDescription);

									Set<org.allotrope.datashapes.datacube.model.Attribute> attributes = datasetStructure.attributes();
									List<Attribute> extractedAttributes = new ArrayList<Attribute>();
									for (Iterator<org.allotrope.datashapes.datacube.model.Attribute> attributesIterator = attributes.iterator(); attributesIterator.hasNext();)
									{
										org.allotrope.datashapes.hdf.model.Attribute attribute = (org.allotrope.datashapes.hdf.model.Attribute) attributesIterator.next();
										String attributeId = attribute.id().get();
										String attributeTitle = attribute.title();
										String attributeLabel = attribute.label();
										String attributeDescription = attribute.description();
										log.info("                     --> attribute id: \"" + attributeId + "\" title: \"" + attributeTitle + "\" label: \"" + attributeLabel + "\" description: \""
												+ attributeDescription + "\"");
										Attribute extractedDcChangedDatasetStructureAttribute = new Attribute();
										extractedDcChangedDatasetStructureAttribute.setId(attributeId);
										extractedDcChangedDatasetStructureAttribute.setTitle(attributeTitle);
										extractedDcChangedDatasetStructureAttribute.setDescription(attributeDescription);
										extractedDcChangedDatasetStructureAttribute.setLabel(attributeLabel);

										Datatype datatype = attribute.datatype();
										String datatypeId = datatype.id().get();
										String datatypeTitle = datatype.title();
										String datatypeLabel = datatype.label();
										String datatypeDescription = datatype.description();
										String datatypeOrder = (datatype.order() != null ? datatype.order().name() : "");
										String datatypeSize = String.valueOf(datatype.size());
										log.info("                         --> datatype  id: \"" + datatypeId + "\" title: \"" + datatypeTitle + "\" label: \"" + datatypeLabel + "\" description: \""
												+ datatypeDescription + "\" order: \"" + datatypeOrder + "\" size: \"" + datatypeSize + "\"");

										processor.audit.Datatype extractedDcChangedDatasetStructureAttributeDatatype = new processor.audit.Datatype();
										extractedDcChangedDatasetStructureAttributeDatatype.setId(datatypeId);
										extractedDcChangedDatasetStructureAttributeDatatype.setTitle(datatypeTitle);
										extractedDcChangedDatasetStructureAttributeDatatype.setLabel(datatypeLabel);
										extractedDcChangedDatasetStructureAttributeDatatype.setDescription(datatypeDescription);
										extractedDcChangedDatasetStructureAttributeDatatype.setOrder(datatypeOrder);
										extractedDcChangedDatasetStructureAttributeDatatype.setSize(datatypeSize);
										extractedDcChangedDatasetStructureAttribute.setDatatype(extractedDcChangedDatasetStructureAttributeDatatype);

										Dataspace dataspace = attribute.dataspace();
										String dataspaceId = dataspace.id().get();
										String dataspaceTitle = dataspace.title();
										String dataspaceLabel = dataspace.label();
										String dataspaceDescription = dataspace.description();
										String dataspaceRank = String.valueOf(dataspace.rank());
										log.info("                         --> dataspace  id: \"" + dataspaceId + "\" title: \"" + dataspaceTitle + "\" label: \"" + dataspaceLabel
												+ "\" description: \"" + dataspaceDescription + "\" rank: \"" + dataspaceRank + "\"");
										processor.audit.Dataspace extractedDcChangedDatasetStructureAttributeDataspace = new processor.audit.Dataspace();
										extractedDcChangedDatasetStructureAttributeDataspace.setId(dataspaceId);
										extractedDcChangedDatasetStructureAttributeDataspace.setTitle(dataspaceTitle);
										extractedDcChangedDatasetStructureAttributeDataspace.setLabel(dataspaceLabel);
										extractedDcChangedDatasetStructureAttributeDataspace.setDescription(dataspaceDescription);

										Set<DataspaceDimension> dimensions = dataspace.dimensions();
										List<processor.audit.Dimension> extractedDcChangedDatasetStructureAttributeDataspaceDimension = new ArrayList<processor.audit.Dimension>();
										for (Iterator<DataspaceDimension> dimensionsIterator = dimensions.iterator(); dimensionsIterator.hasNext();)
										{
											DataspaceDimension dataspaceDimension = dimensionsIterator.next();
											String dataspaceDimensionId = dataspaceDimension.id().get();
											String dataspaceDimensionTitle = dataspaceDimension.title();
											String dataspaceDimensionLabel = dataspaceDimension.label();
											String dataspaceDimensionDescription = dataspaceDimension.description();
											String dataspaceDimensionIndex = String.valueOf(dataspaceDimension.index());
											String dataspaceDimensionSize = String.valueOf(dataspaceDimension.size());
											String dataspaceDimensionCurrentSize = String.valueOf(dataspaceDimension.currentSize());
											String dataspaceDimensionInitialSize = String.valueOf(dataspaceDimension.initialSize());
											String dataspaceDimensionMaxSize = String.valueOf(dataspaceDimension.maximumSize());
											log.info("                             --> dimension id: \"" + dataspaceDimensionId + "\" title: \"" + dataspaceDimensionTitle + "\" label: \""
													+ dataspaceDimensionLabel + "\" description: \"" + dataspaceDimensionDescription + "\" index: \"" + dataspaceDimensionIndex + "\" size: \""
													+ dataspaceDimensionSize + "\" current size: \"" + dataspaceDimensionCurrentSize + "\" initial size: \"" + dataspaceDimensionInitialSize
													+ "\" maximum size: \"" + dataspaceDimensionMaxSize + "\"");
											processor.audit.Dimension extractedDimension = new Dimension();
											extractedDimension.setId(dataspaceDimensionId);
											extractedDimension.setTitle(dataspaceDimensionTitle);
											extractedDimension.setLabel(dataspaceDimensionLabel);
											extractedDimension.setDescription(dataspaceDimensionDescription);
											extractedDimension.setIndex(dataspaceDimensionIndex);
											extractedDimension.setSize(dataspaceDimensionSize);
											extractedDimension.setCurrentSize(dataspaceDimensionCurrentSize);
											extractedDimension.setInitialSize(dataspaceDimensionInitialSize);
											extractedDimension.setMaxSize(dataspaceDimensionMaxSize);
											extractedDcChangedDatasetStructureAttributeDataspaceDimension.add(extractedDimension);
										}
										extractedDcChangedDatasetStructureAttributeDataspace.setDimensions(extractedDcChangedDatasetStructureAttributeDataspaceDimension);
										extractedDcChangedDatasetStructureAttribute.setDataspace(extractedDcChangedDatasetStructureAttributeDataspace);
										extractedAttributes.add(extractedDcChangedDatasetStructureAttribute);
									}

									extractedDataStructure.setAttributes(extractedAttributes);

									Set<ComponentSpecification> components = datasetStructure.components();
									List<processor.audit.Component> extractedComponents = new ArrayList<processor.audit.Component>();
									for (Iterator<ComponentSpecification> componentsIterator = components.iterator(); componentsIterator.hasNext();)
									{
										ComponentSpecification componentSpecification = componentsIterator.next();
										String componentSpecificationId = componentSpecification.id().get();
										String componentSpecificationTitle = componentSpecification.title();
										String componentSpecificationLabel = componentSpecification.label();
										String componentSpecificationDescription = componentSpecification.description();
										String componentSpecificationAttachmentId = (componentSpecification.componentAttachment() != null ? componentSpecification.componentAttachment().toString()
												: "");
										String componentSpecificationDataTypeId = (componentSpecification.componentDataType() != null ? componentSpecification.componentDataType().toString() : "");
										String componentSpecificationRequired = String.valueOf(componentSpecification.componentRequired());
										log.info("                     --> component specification id: \"" + componentSpecificationId + "\" title: \"" + componentSpecificationTitle + "\" label: \""
												+ componentSpecificationLabel + "\" description: \"" + componentSpecificationDescription + "\" attachment id: \"" + componentSpecificationAttachmentId
												+ "\" datatype id: \"" + componentSpecificationDataTypeId + "\" required: \"" + componentSpecificationRequired + "\"");
										processor.audit.Component extractedComponent = new processor.audit.Component();
										extractedComponent.setId(componentSpecificationId);
										extractedComponent.setTitle(componentSpecificationTitle);
										extractedComponent.setLabel(componentSpecificationLabel);
										extractedComponent.setDescription(componentSpecificationDescription);
										extractedComponent.setAttachment(componentSpecificationAttachmentId);
										extractedComponent.setDatatypeId(componentSpecificationDataTypeId);
										extractedComponent.setRequired(componentSpecificationRequired);
										extractedComponents.add(extractedComponent);
									}
									extractedDataStructure.setComponents(extractedComponents);

									List<org.allotrope.datashapes.datacube.model.Dimension> dimensions = datasetStructure.dimensions();
									List<processor.audit.Dimension> extractedDimensions = new ArrayList<processor.audit.Dimension>();
									for (Iterator<org.allotrope.datashapes.datacube.model.Dimension> dimensionsIterator = dimensions.iterator(); dimensionsIterator.hasNext();)
									{
										org.allotrope.datashapes.datacube.model.Dimension dimension = dimensionsIterator.next();
										String dimensionId = dimension.id().get();
										String dimensionTitle = dimension.title();
										String dimensionLabel = dimension.label();
										String dimensionDescription = dimension.description();
										String dimensionProperty = (dimension.dimensionProperty() != null ? dimension.dimensionProperty().toString() : "");
										String dimensionOrder = String.valueOf(dimension.order());
										String dimensionAttachmentId = (dimension.componentAttachment() != null ? dimension.componentAttachment().toString() : "");
										String dimensionDataTypeId = (dimension.componentDataType() != null ? dimension.componentDataType().toString() : "");
										String dimensionRequired = String.valueOf(dimension.componentRequired());
										log.info("                     --> dimension  id: \"" + dimensionId + "\" title: \"" + dimensionTitle + "\" label: \"" + dimensionLabel + "\" description: \""
												+ dimensionDescription + "\" property: \"" + dimensionProperty + "\" order: \"" + dimensionOrder + "\" attachment id: \"" + dimensionAttachmentId
												+ "\" datatype id: \"" + dimensionDataTypeId + "\" required: \"" + dimensionRequired + "\"");
										processor.audit.Dimension extractedDimension = new processor.audit.Dimension();
										extractedDimension.setId(dimensionId);
										extractedDimension.setTitle(dimensionTitle);
										extractedDimension.setLabel(dimensionLabel);
										extractedDimension.setDescription(dimensionDescription);
										extractedDimension.setProperty(dimensionProperty);
										extractedDimension.setOrder(dimensionOrder);
										extractedDimension.setAttachment(dimensionAttachmentId);
										extractedDimension.setDatatypeId(dimensionDataTypeId);
										extractedDimension.setRequired(dimensionRequired);
										extractedDimensions.add(extractedDimension);
									}

									extractedDataStructure.setDimensions(extractedDimensions);

									List<processor.audit.Measure> extractedMeasures = new ArrayList<processor.audit.Measure>();
									Set<Measure> measures = datasetStructure.measures();
									for (Iterator<Measure> measuresIterator = measures.iterator(); measuresIterator.hasNext();)
									{
										Measure measure = measuresIterator.next();
										String measureId = measure.id().get();
										String measureTitle = measure.title();
										String measureLabel = measure.label();
										String measureDescription = measure.description();
										String measureProperty = (measure.measureProperty() != null ? measure.measureProperty().toString() : "");
										String measureOrder = String.valueOf(measure.order());
										String measureAttachmentId = (measure.componentAttachment() != null ? measure.componentAttachment().toString() : "");
										String measureDataTypeId = (measure.componentDataType() != null ? measure.componentDataType().toString() : "");
										String measureRequired = String.valueOf(measure.componentRequired());
										log.info("                     --> measure  id: \"" + measureId + "\" title: \"" + measureTitle + "\" label: \"" + measureLabel + "\" description: \""
												+ measureDescription + "\" property: \"" + measureProperty + "\" order: \"" + measureOrder + "\" attachment id: \"" + measureAttachmentId
												+ "\" datatype id: \"" + measureDataTypeId + "\" required: \"" + measureRequired + "\"");
										processor.audit.Measure extractedMeasure = new processor.audit.Measure();
										extractedMeasure.setId(measureId);
										extractedMeasure.setTitle(measureTitle);
										extractedMeasure.setLabel(measureLabel);
										extractedMeasure.setDescription(measureDescription);
										extractedMeasure.setAttachment(measureAttachmentId);
										extractedMeasure.setDatatypeId(measureDataTypeId);
										extractedMeasure.setRequired(measureRequired);
										extractedMeasure.setProperty(measureProperty);
										extractedMeasure.setOrder(measureOrder);
										extractedMeasures.add(extractedMeasure);
									}
									extractedDataStructure.setMeasures(extractedMeasures);
									extractedDcChangedDataset.setDataStructure(extractedDataStructure);
								}
								extractedDcAdditon.setDataSet(extractedDcChangedDataset);
							}
							extractedDcAdditions.add(extractedDcAdditon);
						}
						extractedDcChanges.setAdditions(extractedDcAdditions);

						List<DatacubeChange> extractedDcRemovals = new ArrayList<DatacubeChange>(); // currently api does not support removals
						for (Resource dcRemoval : dcChanges.removals())
						{
							String label = StringUtils.EMPTY;
							if (dataCubeService.openDataCube(iriFactory.create(dcRemoval.id().get())) != null)
							{
								label = dataCubeService.openDataCube(iriFactory.create(dcRemoval.id().get())).getLabel();
							}
							log.info("         --> DC removal id: \"" + dcRemoval.id().get() + "\" label: \"" + label + "\"");

							DatacubeChange extractedDcRemoval = new DatacubeChange();
							extractedDcRemoval.setId(dcRemoval.id().get());
							extractedDcRemoval.setDatacubeLabel(label);

							org.allotrope.datashapes.datacube.model.DataSet dataset = (org.allotrope.datashapes.datacube.model.DataSet) object2rdf.read(recordModel.getGraph(),
									NodeFactory.createURI(dcRemoval.id().get()), org.allotrope.datashapes.datacube.model.DataSet.class);

							if (dataset != null)
							{
								String datasetId = dataset.id().get();
								String datasetTitle = dataset.title();
								String datasetLabel = dataset.label();
								String datasetDescription = dataset.description();
								log.info("             --> dataset id: \"" + datasetId + "\" title: \"" + datasetTitle + "\" label: \"" + datasetLabel + "\" description: \"" + datasetDescription
										+ "\"");
								processor.audit.Dataset extractedDcChangedDataset = new processor.audit.Dataset();
								extractedDcChangedDataset.setId(datasetId);
								extractedDcChangedDataset.setTitle(datasetTitle);
								extractedDcChangedDataset.setLabel(datasetLabel);
								extractedDcChangedDataset.setDescription(datasetDescription);

								DataStructureDefinition datasetStructure = dataset.structure();
								if (datasetStructure != null)
								{
									String datasetStructureId = datasetStructure.id().get();
									String datasetStructureTitle = datasetStructure.title();
									String datasetStructureLabel = datasetStructure.label();
									String datasetStructureDescription = datasetStructure.description();
									log.info("                 --> structure id: \"" + datasetStructureId + "\" title: \"" + datasetStructureTitle + "\" label: \"" + datasetStructureLabel
											+ "\" description: \"" + datasetStructureDescription + "\"");
									DataStructure extractedDataStructure = new DataStructure();
									extractedDataStructure.setId(datasetStructureId);
									extractedDataStructure.setTitle(datasetStructureTitle);
									extractedDataStructure.setLabel(datasetStructureLabel);
									extractedDataStructure.setDescription(datasetStructureDescription);

									Set<org.allotrope.datashapes.datacube.model.Attribute> attributes = datasetStructure.attributes();
									List<Attribute> extractedAttributes = new ArrayList<Attribute>();
									for (Iterator<org.allotrope.datashapes.datacube.model.Attribute> attributesIterator = attributes.iterator(); attributesIterator.hasNext();)
									{
										org.allotrope.datashapes.hdf.model.Attribute attribute = (org.allotrope.datashapes.hdf.model.Attribute) attributesIterator.next();
										String attributeId = attribute.id().get();
										String attributeTitle = attribute.title();
										String attributeLabel = attribute.label();
										String attributeDescription = attribute.description();
										log.info("                     --> attribute id: \"" + attributeId + "\" title: \"" + attributeTitle + "\" label: \"" + attributeLabel + "\" description: \""
												+ attributeDescription + "\"");
										Attribute extractedDcChangedDatasetStructureAttribute = new Attribute();
										extractedDcChangedDatasetStructureAttribute.setId(attributeId);
										extractedDcChangedDatasetStructureAttribute.setTitle(attributeTitle);
										extractedDcChangedDatasetStructureAttribute.setDescription(attributeDescription);
										extractedDcChangedDatasetStructureAttribute.setLabel(attributeLabel);

										Datatype datatype = attribute.datatype();
										String datatypeId = datatype.id().get();
										String datatypeTitle = datatype.title();
										String datatypeLabel = datatype.label();
										String datatypeDescription = datatype.description();
										String datatypeOrder = (datatype.order() != null ? datatype.order().name() : "");
										String datatypeSize = String.valueOf(datatype.size());
										log.info("                         --> datatype  id: \"" + datatypeId + "\" title: \"" + datatypeTitle + "\" label: \"" + datatypeLabel + "\" description: \""
												+ datatypeDescription + "\" order: \"" + datatypeOrder + "\" size: \"" + datatypeSize + "\"");

										processor.audit.Datatype extractedDcChangedDatasetStructureAttributeDatatype = new processor.audit.Datatype();
										extractedDcChangedDatasetStructureAttributeDatatype.setId(datatypeId);
										extractedDcChangedDatasetStructureAttributeDatatype.setTitle(datatypeTitle);
										extractedDcChangedDatasetStructureAttributeDatatype.setLabel(datatypeLabel);
										extractedDcChangedDatasetStructureAttributeDatatype.setDescription(datatypeDescription);
										extractedDcChangedDatasetStructureAttributeDatatype.setOrder(datatypeOrder);
										extractedDcChangedDatasetStructureAttributeDatatype.setSize(datatypeSize);
										extractedDcChangedDatasetStructureAttribute.setDatatype(extractedDcChangedDatasetStructureAttributeDatatype);

										Dataspace dataspace = attribute.dataspace();
										String dataspaceId = dataspace.id().get();
										String dataspaceTitle = dataspace.title();
										String dataspaceLabel = dataspace.label();
										String dataspaceDescription = dataspace.description();
										String dataspaceRank = String.valueOf(dataspace.rank());
										log.info("                         --> dataspace  id: \"" + dataspaceId + "\" title: \"" + dataspaceTitle + "\" label: \"" + dataspaceLabel
												+ "\" description: \"" + dataspaceDescription + "\" rank: \"" + dataspaceRank + "\"");
										processor.audit.Dataspace extractedDcChangedDatasetStructureAttributeDataspace = new processor.audit.Dataspace();
										extractedDcChangedDatasetStructureAttributeDataspace.setId(dataspaceId);
										extractedDcChangedDatasetStructureAttributeDataspace.setTitle(dataspaceTitle);
										extractedDcChangedDatasetStructureAttributeDataspace.setLabel(dataspaceLabel);
										extractedDcChangedDatasetStructureAttributeDataspace.setDescription(dataspaceDescription);

										Set<DataspaceDimension> dimensions = dataspace.dimensions();
										List<processor.audit.Dimension> extractedDcChangedDatasetStructureAttributeDataspaceDimension = new ArrayList<processor.audit.Dimension>();
										for (Iterator<DataspaceDimension> dimensionsIterator = dimensions.iterator(); dimensionsIterator.hasNext();)
										{
											DataspaceDimension dataspaceDimension = dimensionsIterator.next();
											String dataspaceDimensionId = dataspaceDimension.id().get();
											String dataspaceDimensionTitle = dataspaceDimension.title();
											String dataspaceDimensionLabel = dataspaceDimension.label();
											String dataspaceDimensionDescription = dataspaceDimension.description();
											String dataspaceDimensionIndex = String.valueOf(dataspaceDimension.index());
											String dataspaceDimensionSize = String.valueOf(dataspaceDimension.size());
											String dataspaceDimensionCurrentSize = String.valueOf(dataspaceDimension.currentSize());
											String dataspaceDimensionInitialSize = String.valueOf(dataspaceDimension.initialSize());
											String dataspaceDimensionMaxSize = String.valueOf(dataspaceDimension.maximumSize());
											log.info("                             --> dimension id: \"" + dataspaceDimensionId + "\" title: \"" + dataspaceDimensionTitle + "\" label: \""
													+ dataspaceDimensionLabel + "\" description: \"" + dataspaceDimensionDescription + "\" index: \"" + dataspaceDimensionIndex + "\" size: \""
													+ dataspaceDimensionSize + "\" current size: \"" + dataspaceDimensionCurrentSize + "\" initial size: \"" + dataspaceDimensionInitialSize
													+ "\" maximum size: \"" + dataspaceDimensionMaxSize + "\"");
											processor.audit.Dimension extractedDimension = new Dimension();
											extractedDimension.setId(dataspaceDimensionId);
											extractedDimension.setTitle(dataspaceDimensionTitle);
											extractedDimension.setLabel(dataspaceDimensionLabel);
											extractedDimension.setDescription(dataspaceDimensionDescription);
											extractedDimension.setIndex(dataspaceDimensionIndex);
											extractedDimension.setSize(dataspaceDimensionSize);
											extractedDimension.setCurrentSize(dataspaceDimensionCurrentSize);
											extractedDimension.setInitialSize(dataspaceDimensionInitialSize);
											extractedDimension.setMaxSize(dataspaceDimensionMaxSize);
											extractedDcChangedDatasetStructureAttributeDataspaceDimension.add(extractedDimension);
										}
										extractedDcChangedDatasetStructureAttributeDataspace.setDimensions(extractedDcChangedDatasetStructureAttributeDataspaceDimension);
										extractedDcChangedDatasetStructureAttribute.setDataspace(extractedDcChangedDatasetStructureAttributeDataspace);
										extractedAttributes.add(extractedDcChangedDatasetStructureAttribute);
									}

									extractedDataStructure.setAttributes(extractedAttributes);

									Set<ComponentSpecification> components = datasetStructure.components();
									List<processor.audit.Component> extractedComponents = new ArrayList<processor.audit.Component>();
									for (Iterator<ComponentSpecification> componentsIterator = components.iterator(); componentsIterator.hasNext();)
									{
										ComponentSpecification componentSpecification = componentsIterator.next();
										String componentSpecificationId = componentSpecification.id().get();
										String componentSpecificationTitle = componentSpecification.title();
										String componentSpecificationLabel = componentSpecification.label();
										String componentSpecificationDescription = componentSpecification.description();
										String componentSpecificationAttachmentId = (componentSpecification.componentAttachment() != null ? componentSpecification.componentAttachment().toString()
												: "");
										String componentSpecificationDataTypeId = (componentSpecification.componentDataType() != null ? componentSpecification.componentDataType().toString() : "");
										String componentSpecificationRequired = String.valueOf(componentSpecification.componentRequired());
										log.info("                     --> component specification id: \"" + componentSpecificationId + "\" title: \"" + componentSpecificationTitle + "\" label: \""
												+ componentSpecificationLabel + "\" description: \"" + componentSpecificationDescription + "\" attachment id: \"" + componentSpecificationAttachmentId
												+ "\" datatype id: \"" + componentSpecificationDataTypeId + "\" required: \"" + componentSpecificationRequired + "\"");
										processor.audit.Component extractedComponent = new processor.audit.Component();
										extractedComponent.setId(componentSpecificationId);
										extractedComponent.setTitle(componentSpecificationTitle);
										extractedComponent.setLabel(componentSpecificationLabel);
										extractedComponent.setDescription(componentSpecificationDescription);
										extractedComponent.setAttachment(componentSpecificationAttachmentId);
										extractedComponent.setDatatypeId(componentSpecificationDataTypeId);
										extractedComponent.setRequired(componentSpecificationRequired);
										extractedComponents.add(extractedComponent);
									}
									extractedDataStructure.setComponents(extractedComponents);

									List<org.allotrope.datashapes.datacube.model.Dimension> dimensions = datasetStructure.dimensions();
									List<processor.audit.Dimension> extractedDimensions = new ArrayList<processor.audit.Dimension>();
									for (Iterator<org.allotrope.datashapes.datacube.model.Dimension> dimensionsIterator = dimensions.iterator(); dimensionsIterator.hasNext();)
									{
										org.allotrope.datashapes.datacube.model.Dimension dimension = dimensionsIterator.next();
										String dimensionId = dimension.id().get();
										String dimensionTitle = dimension.title();
										String dimensionLabel = dimension.label();
										String dimensionDescription = dimension.description();
										String dimensionProperty = (dimension.dimensionProperty() != null ? dimension.dimensionProperty().toString() : "");
										String dimensionOrder = String.valueOf(dimension.order());
										String dimensionAttachmentId = (dimension.componentAttachment() != null ? dimension.componentAttachment().toString() : "");
										String dimensionDataTypeId = (dimension.componentDataType() != null ? dimension.componentDataType().toString() : "");
										String dimensionRequired = String.valueOf(dimension.componentRequired());
										log.info("                     --> dimension  id: \"" + dimensionId + "\" title: \"" + dimensionTitle + "\" label: \"" + dimensionLabel + "\" description: \""
												+ dimensionDescription + "\" property: \"" + dimensionProperty + "\" order: \"" + dimensionOrder + "\" attachment id: \"" + dimensionAttachmentId
												+ "\" datatype id: \"" + dimensionDataTypeId + "\" required: \"" + dimensionRequired + "\"");
										processor.audit.Dimension extractedDimension = new processor.audit.Dimension();
										extractedDimension.setId(dimensionId);
										extractedDimension.setTitle(dimensionTitle);
										extractedDimension.setLabel(dimensionLabel);
										extractedDimension.setDescription(dimensionDescription);
										extractedDimension.setProperty(dimensionProperty);
										extractedDimension.setOrder(dimensionOrder);
										extractedDimension.setAttachment(dimensionAttachmentId);
										extractedDimension.setDatatypeId(dimensionDataTypeId);
										extractedDimension.setRequired(dimensionRequired);
										extractedDimensions.add(extractedDimension);
									}

									extractedDataStructure.setDimensions(extractedDimensions);

									List<processor.audit.Measure> extractedMeasures = new ArrayList<processor.audit.Measure>();
									Set<Measure> measures = datasetStructure.measures();
									for (Iterator<Measure> measuresIterator = measures.iterator(); measuresIterator.hasNext();)
									{
										Measure measure = measuresIterator.next();
										String measureId = measure.id().get();
										String measureTitle = measure.title();
										String measureLabel = measure.label();
										String measureDescription = measure.description();
										String measureProperty = (measure.measureProperty() != null ? measure.measureProperty().toString() : "");
										String measureOrder = String.valueOf(measure.order());
										String measureAttachmentId = (measure.componentAttachment() != null ? measure.componentAttachment().toString() : "");
										String measureDataTypeId = (measure.componentDataType() != null ? measure.componentDataType().toString() : "");
										String measureRequired = String.valueOf(measure.componentRequired());
										log.info("                     --> measure  id: \"" + measureId + "\" title: \"" + measureTitle + "\" label: \"" + measureLabel + "\" description: \""
												+ measureDescription + "\" property: \"" + measureProperty + "\" order: \"" + measureOrder + "\" attachment id: \"" + measureAttachmentId
												+ "\" datatype id: \"" + measureDataTypeId + "\" required: \"" + measureRequired + "\"");
										processor.audit.Measure extractedMeasure = new processor.audit.Measure();
										extractedMeasure.setId(measureId);
										extractedMeasure.setTitle(measureTitle);
										extractedMeasure.setLabel(measureLabel);
										extractedMeasure.setDescription(measureDescription);
										extractedMeasure.setAttachment(measureAttachmentId);
										extractedMeasure.setDatatypeId(measureDataTypeId);
										extractedMeasure.setRequired(measureRequired);
										extractedMeasure.setProperty(measureProperty);
										extractedMeasure.setOrder(measureOrder);
										extractedMeasures.add(extractedMeasure);
									}
									extractedDataStructure.setMeasures(extractedMeasures);
									extractedDcChangedDataset.setDataStructure(extractedDataStructure);
								}
								extractedDcRemoval.setDataSet(extractedDcChangedDataset);
							}
							extractedDcRemovals.add(extractedDcRemoval);
						}
						extractedDcChanges.setRemovals(extractedDcRemovals);

						List<processor.audit.Update> extractedDcUpdates = new ArrayList<processor.audit.Update>();
						for (DataUpdate dcUpdate : dcChanges.updates())
						{
							log.info("         --> DC update id: \"" + dcUpdate.id().get() + "\" title: \"" + dcUpdate.title() + "\" label: \"" + dcUpdate.label() + "\" description: \""
									+ dcUpdate.description() + "\" target: \"" + (dcUpdate.target() == null ? "" : dcUpdate.target().id().get()) + "\"");
							processor.audit.Update extractedDcUpdate = new processor.audit.Update();
							extractedDcUpdate.setId(dcUpdate.id().get());
							extractedDcUpdate.setTitle(dcUpdate.title());
							extractedDcUpdate.setLabel(dcUpdate.label());
							extractedDcUpdate.setDescription(dcUpdate.description());
							extractedDcUpdate.setTarget((dcUpdate.target() == null ? "" : dcUpdate.target().id().get()));

							if (dcUpdate.oldDataReference() != null)
							{
								org.allotrope.datashapes.datacube.model.DataSelection dataSelection = (org.allotrope.datashapes.datacube.model.DataSelection) object2rdf.read(recordModel.getGraph(),
										NodeFactory.createURI(dcUpdate.oldDataReference().id().get()), org.allotrope.datashapes.datacube.model.DataSelection.class);

								if (dataSelection != null)
								{
									String dataSelectionId = dataSelection.id().get();
									String dataSelectionTitle = dataSelection.title();
									String dataSelectionLabel = dataSelection.label();
									String dataSelectionDescription = dataSelection.description();
									log.info("             --> OLD selection id: \"" + dataSelectionId + "\" title: \"" + dataSelectionTitle + "\" label: \"" + dataSelectionLabel
											+ "\" description: \"" + dataSelectionDescription + "\"");
									processor.audit.DataSelection extractedDataSelection = new processor.audit.DataSelection();
									extractedDataSelection.setId(dataSelectionId);
									extractedDataSelection.setTitle(dataSelectionTitle);
									extractedDataSelection.setLabel(dataSelectionLabel);
									extractedDataSelection.setDescription(dataSelectionDescription);

									// TODO read arbitrary dataytypes not just double[]
									org.allotrope.datashapes.datacube.model.DataSelection oldDataSelection = (org.allotrope.datashapes.datacube.model.DataSelection) dcUpdate.oldDataReference();
									try
									{
										Object archivedDatacubeData = auditTrailService.getArchivedDatacubeData(oldDataSelection);
										double[] archivedDatacubeDataValues = (archivedDatacubeData == null ? null : (double[]) archivedDatacubeData);
										log.info("             --> OLD values: \"" + Arrays.toString(archivedDatacubeDataValues) + "\"");
										List<String> extractedValues = new ArrayList<String>(archivedDatacubeDataValues.length);
										for (int j = 0; j < archivedDatacubeDataValues.length; j++)
										{
											double d = archivedDatacubeDataValues[j];
											extractedValues.add(String.valueOf(d));
										}
										extractedDcUpdate.setOldDataValues(extractedValues);
									}
									catch (Exception e)
									{
										// log.info(" --> skipped spurious
										// entry");
									}

									SelectionStructureDefinition selectionStructure = dataSelection.selectionStructure();
									String selectionStructureId = selectionStructure.id().get();
									String selectionStructureTitle = selectionStructure.title();
									String selectionStructureLabel = selectionStructure.label();
									String selectionStructureDescription = selectionStructure.description();
									log.info("                 --> structure id: \"" + selectionStructureId + "\" title: \"" + selectionStructureTitle + "\" label: \"" + selectionStructureLabel
											+ "\" description: \"" + selectionStructureDescription + "\"");
									processor.audit.DataSelectionStructure extractedDataSelectionStructure = new DataSelectionStructure();
									extractedDataSelectionStructure.setId(selectionStructureId);
									extractedDataSelectionStructure.setTitle(selectionStructureTitle);
									extractedDataSelectionStructure.setLabel(selectionStructureLabel);
									extractedDataSelectionStructure.setDescription(selectionStructureDescription);

									List<processor.audit.ComponentSelection> extractedComponentSelections = new ArrayList<processor.audit.ComponentSelection>();
									Set<org.allotrope.datashapes.datacube.model.ComponentSelection> selectedComponents = selectionStructure.selectedComponents();
									for (Iterator<org.allotrope.datashapes.datacube.model.ComponentSelection> selectedComponentsIterator = selectedComponents.iterator(); selectedComponentsIterator
											.hasNext();)
									{
										org.allotrope.datashapes.datacube.model.ComponentSelection componentSelection = selectedComponentsIterator.next();
										String componentSelectionId = componentSelection.id().get();
										String componentSelectionTitle = componentSelection.title();
										String componentSelectionLabel = componentSelection.label();
										String componentSelectionDescription = componentSelection.description();
										ComponentSpecification selectionOn = componentSelection.selectionOn();
										String componentSelectionOn = (selectionOn != null ? selectionOn.id().get() : "");
										log.info("                     --> selected component id: \"" + componentSelectionId + "\" title: \"" + componentSelectionTitle + "\" label: \""
												+ componentSelectionLabel + "\" description: \"" + componentSelectionDescription + "\" selection on: \"" + componentSelectionOn + "\"");
										processor.audit.ComponentSelection extractedComponentSelection = new processor.audit.ComponentSelection();
										extractedComponentSelection.setId(componentSelectionId);
										extractedComponentSelection.setTitle(componentSelectionTitle);
										extractedComponentSelection.setLabel(componentSelectionLabel);
										extractedComponentSelection.setDescription(componentSelectionDescription);

										if (selectionOn != null)
										{
											String componentSpecificationId = selectionOn.id().get();
											String componentSpecificationTitle = selectionOn.title();
											String componentSpecificationLabel = selectionOn.label();
											String componentSpecificationDescription = selectionOn.description();
											String componentSpecificationAttachmentId = (selectionOn.componentAttachment() != null ? selectionOn.componentAttachment().toString() : "");
											String componentSpecificationDataTypeId = (selectionOn.componentDataType() != null ? selectionOn.componentDataType().toString() : "");
											String componentSpecificationRequired = String.valueOf(selectionOn.componentRequired());
											log.info("                         --> selected component specification id: \"" + componentSpecificationId + "\" title: \"" + componentSpecificationTitle
													+ "\" label: \"" + componentSpecificationLabel + "\" description: \"" + componentSpecificationDescription + "\" attachment id: \""
													+ componentSpecificationAttachmentId + "\" datatype id: \"" + componentSpecificationDataTypeId + "\" required: \"" + componentSpecificationRequired
													+ "\"");
											processor.audit.Component extractedComponentSpecification = new processor.audit.Component();
											extractedComponentSpecification.setId(componentSpecificationId);
											extractedComponentSpecification.setTitle(componentSpecificationTitle);
											extractedComponentSpecification.setLabel(componentSpecificationLabel);
											extractedComponentSpecification.setDescription(componentSpecificationDescription);
											extractedComponentSpecification.setAttachment(componentSpecificationAttachmentId);
											extractedComponentSpecification.setDatatypeId(componentSpecificationDataTypeId);
											extractedComponentSpecification.setRequired(componentSpecificationRequired);
											extractedComponentSelection.setSelectionOn(extractedComponentSpecification);
										}
										extractedComponentSelections.add(extractedComponentSelection);
									}
									extractedDataSelectionStructure.setSelectedComponents(extractedComponentSelections);
									extractedDataSelection.setDataSelectionStructure(extractedDataSelectionStructure);

									DataSet dataset = dataSelection.dataSelectionOf();
									if (dataset != null)
									{
										String datasetId = dataset.id().get();
										String datasetTitle = dataset.title();
										String datasetLabel = dataset.label();
										String datasetDescription = dataset.description();
										log.info("                 --> dataset id: \"" + datasetId + "\" title: \"" + datasetTitle + "\" label: \"" + datasetLabel + "\" description: \""
												+ datasetDescription + "\"");
										processor.audit.Dataset extractedDataSet = new processor.audit.Dataset();
										extractedDataSet.setId(datasetId);
										extractedDataSet.setTitle(datasetTitle);
										extractedDataSet.setLabel(datasetLabel);
										extractedDataSet.setDescription(datasetDescription);

										DataStructureDefinition datasetStructure = dataset.structure();
										if (datasetStructure != null)
										{
											String datasetStructureId = datasetStructure.id().get();
											String datasetStructureTitle = datasetStructure.title();
											String datasetStructureLabel = datasetStructure.label();
											String datasetStructureDescription = datasetStructure.description();
											log.info("                     --> structure id: \"" + datasetStructureId + "\" title: \"" + datasetStructureTitle + "\" label: \"" + datasetStructureLabel
													+ "\" description: \"" + datasetStructureDescription + "\"");
											DataStructure extractedDataStructure = new DataStructure();
											extractedDataStructure.setId(datasetStructureId);
											extractedDataStructure.setTitle(datasetStructureTitle);
											extractedDataStructure.setLabel(datasetStructureLabel);
											extractedDataStructure.setDescription(datasetStructureDescription);

											List<Attribute> extractedDatasetstructureAttributes = new ArrayList<Attribute>();
											Set<org.allotrope.datashapes.datacube.model.Attribute> attributes = datasetStructure.attributes();
											for (Iterator<org.allotrope.datashapes.datacube.model.Attribute> attributesIterator = attributes.iterator(); attributesIterator.hasNext();)
											{
												org.allotrope.datashapes.hdf.model.Attribute attribute = (org.allotrope.datashapes.hdf.model.Attribute) attributesIterator.next();
												String attributeId = attribute.id().get();
												String attributeTitle = attribute.title();
												String attributeLabel = attribute.label();
												String attributeDescription = attribute.description();
												log.info("                         --> attribute id: \"" + attributeId + "\" title: \"" + attributeTitle + "\" label: \"" + attributeLabel
														+ "\" description: \"" + attributeDescription + "\"");
												Attribute extractedDatasetstructureAttribute = new Attribute();
												extractedDatasetstructureAttribute.setId(attributeId);
												extractedDatasetstructureAttribute.setLabel(attributeLabel);
												extractedDatasetstructureAttribute.setTitle(datasetStructureTitle);
												extractedDatasetstructureAttribute.setDescription(attributeDescription);

												Datatype datatype = attribute.datatype();
												String datatypeId = datatype.id().get();
												String datatypeTitle = datatype.title();
												String datatypeLabel = datatype.label();
												String datatypeDescription = datatype.description();
												String datatypeOrder = (datatype.order() != null ? datatype.order().name() : "");
												String datatypeSize = String.valueOf(datatype.size());
												log.info("                             --> datatype  id: \"" + datatypeId + "\" title: \"" + datatypeTitle + "\" label: \"" + datatypeLabel
														+ "\" description: \"" + datatypeDescription + "\" order: \"" + datatypeOrder + "\" size: \"" + datatypeSize + "\"");
												processor.audit.Datatype extractedDatatype = new processor.audit.Datatype();
												extractedDatatype.setId(datatypeId);
												extractedDatatype.setTitle(datatypeTitle);
												extractedDatatype.setLabel(datatypeLabel);
												extractedDatatype.setDescription(datatypeDescription);
												extractedDatatype.setOrder(datatypeOrder);
												extractedDatatype.setSize(datatypeSize);
												extractedDatasetstructureAttribute.setDatatype(extractedDatatype);

												Dataspace dataspace = attribute.dataspace();
												String dataspaceId = dataspace.id().get();
												String dataspaceTitle = dataspace.title();
												String dataspaceLabel = dataspace.label();
												String dataspaceDescription = dataspace.description();
												String dataspaceRank = String.valueOf(dataspace.rank());
												log.info("                             --> dataspace  id: \"" + dataspaceId + "\" title: \"" + dataspaceTitle + "\" label: \"" + dataspaceLabel
														+ "\" description: \"" + dataspaceDescription + "\" rank: \"" + dataspaceRank + "\"");
												processor.audit.Dataspace extractedDataspace = new processor.audit.Dataspace();
												extractedDataspace.setId(dataspaceId);
												extractedDataspace.setTitle(dataspaceTitle);
												extractedDataspace.setLabel(dataspaceLabel);
												extractedDataspace.setDescription(dataspaceDescription);
												extractedDataspace.setRank(dataspaceRank);

												List<Dimension> extractedDimensions = new ArrayList<Dimension>();
												Set<DataspaceDimension> dimensions = dataspace.dimensions();
												for (Iterator<DataspaceDimension> dimensionsIterator = dimensions.iterator(); dimensionsIterator.hasNext();)
												{
													DataspaceDimension dataspaceDimension = dimensionsIterator.next();
													String dataspaceDimensionId = dataspaceDimension.id().get();
													String dataspaceDimensionTitle = dataspaceDimension.title();
													String dataspaceDimensionLabel = dataspaceDimension.label();
													String dataspaceDimensionDescription = dataspaceDimension.description();
													String dataspaceDimensionIndex = String.valueOf(dataspaceDimension.index());
													String dataspaceDimensionSize = String.valueOf(dataspaceDimension.size());
													String dataspaceDimensionCurrentSize = String.valueOf(dataspaceDimension.currentSize());
													String dataspaceDimensionInitialSize = String.valueOf(dataspaceDimension.initialSize());
													String dataspaceDimensionMaxSize = String.valueOf(dataspaceDimension.maximumSize());
													log.info("                                 --> dimension id: \"" + dataspaceDimensionId + "\" title: \"" + dataspaceDimensionTitle + "\" label: \""
															+ dataspaceDimensionLabel + "\" description: \"" + dataspaceDimensionDescription + "\" index: \"" + dataspaceDimensionIndex + "\" size: \""
															+ dataspaceDimensionSize + "\" current size: \"" + dataspaceDimensionCurrentSize + "\" initial size: \"" + dataspaceDimensionInitialSize
															+ "\" maximum size: \"" + dataspaceDimensionMaxSize + "\"");
													Dimension extractedDimension = new Dimension();
													extractedDimension.setId(dataspaceDimensionId);
													extractedDimension.setTitle(dataspaceDimensionTitle);
													extractedDimension.setLabel(dataspaceDimensionLabel);
													extractedDimension.setDescription(dataspaceDimensionDescription);
													extractedDimension.setIndex(dataspaceDimensionIndex);
													extractedDimension.setSize(dataspaceDimensionSize);
													extractedDimension.setCurrentSize(dataspaceDimensionCurrentSize);
													extractedDimension.setInitialSize(dataspaceDimensionInitialSize);
													extractedDimension.setInitialSize(dataspaceDimensionInitialSize);
													extractedDimension.setMaxSize(dataspaceDimensionMaxSize);
													extractedDimensions.add(extractedDimension);
												}
												extractedDataspace.setDimensions(extractedDimensions);
												extractedDatasetstructureAttribute.setDataspace(extractedDataspace);
												extractedDatasetstructureAttributes.add(extractedDatasetstructureAttribute);
											}
											extractedDataStructure.setAttributes(extractedDatasetstructureAttributes);

											Set<ComponentSpecification> components = datasetStructure.components();
											List<Component> extractedComponents = new ArrayList<Component>();
											for (Iterator<ComponentSpecification> componentsIterator = components.iterator(); componentsIterator.hasNext();)
											{
												ComponentSpecification componentSpecification = componentsIterator.next();
												String componentSpecificationId = componentSpecification.id().get();
												String componentSpecificationTitle = componentSpecification.title();
												String componentSpecificationLabel = componentSpecification.label();
												String componentSpecificationDescription = componentSpecification.description();
												String componentSpecificationAttachmentId = (componentSpecification.componentAttachment() != null
														? componentSpecification.componentAttachment().toString() : "");
												String componentSpecificationDataTypeId = (componentSpecification.componentDataType() != null ? componentSpecification.componentDataType().toString()
														: "");
												String componentSpecificationRequired = String.valueOf(componentSpecification.componentRequired());
												log.info("                         --> component specification id: \"" + componentSpecificationId + "\" title: \"" + componentSpecificationTitle
														+ "\" label: \"" + componentSpecificationLabel + "\" description: \"" + componentSpecificationDescription + "\" attachment id: \""
														+ componentSpecificationAttachmentId + "\" datatype id: \"" + componentSpecificationDataTypeId + "\" required: \""
														+ componentSpecificationRequired + "\"");
												Component extractedComponent = new Component();
												extractedComponent.setId(componentSpecificationId);
												extractedComponent.setTitle(componentSpecificationTitle);
												extractedComponent.setLabel(componentSpecificationLabel);
												extractedComponent.setDescription(componentSpecificationDescription);
												extractedComponent.setAttachment(componentSpecificationAttachmentId);
												extractedComponent.setDatatypeId(componentSpecificationDataTypeId);
												extractedComponent.setRequired(componentSpecificationRequired);
												extractedComponents.add(extractedComponent);
											}
											extractedDataStructure.setComponents(extractedComponents);

											List<Dimension> extractedDimensions = new ArrayList<Dimension>();
											List<org.allotrope.datashapes.datacube.model.Dimension> dimensions = datasetStructure.dimensions();
											for (Iterator<org.allotrope.datashapes.datacube.model.Dimension> dimensionsIterator = dimensions.iterator(); dimensionsIterator.hasNext();)
											{
												org.allotrope.datashapes.datacube.model.Dimension dimension = dimensionsIterator.next();
												String dimensionId = dimension.id().get();
												String dimensionTitle = dimension.title();
												String dimensionLabel = dimension.label();
												String dimensionDescription = dimension.description();
												String dimensionProperty = (dimension.dimensionProperty() != null ? dimension.dimensionProperty().toString() : "");
												String dimensionOrder = String.valueOf(dimension.order());
												String dimensionAttachmentId = (dimension.componentAttachment() != null ? dimension.componentAttachment().toString() : "");
												String dimensionDataTypeId = (dimension.componentDataType() != null ? dimension.componentDataType().toString() : "");
												String dimensionRequired = String.valueOf(dimension.componentRequired());
												log.info("                          --> dimension  id: \"" + dimensionId + "\" title: \"" + dimensionTitle + "\" label: \"" + dimensionLabel
														+ "\" description: \"" + dimensionDescription + "\" property: \"" + dimensionProperty + "\" order: \"" + dimensionOrder + "\" attachment id: \""
														+ dimensionAttachmentId + "\" datatype id: \"" + dimensionDataTypeId + "\" required: \"" + dimensionRequired + "\"");
												Dimension extractedDimension = new Dimension();
												extractedDimension.setId(dimensionId);
												extractedDimension.setTitle(dimensionTitle);
												extractedDimension.setLabel(dimensionLabel);
												extractedDimension.setDescription(dimensionDescription);
												extractedDimension.setProperty(dimensionProperty);
												extractedDimension.setOrder(dimensionOrder);
												extractedDimension.setAttachment(dimensionAttachmentId);
												extractedDimension.setDatatypeId(dimensionDataTypeId);
												extractedDimension.setRequired(dimensionRequired);
												extractedDimensions.add(extractedDimension);
											}
											extractedDataStructure.setDimensions(extractedDimensions);

											List<processor.audit.Measure> extractedMeasures = new ArrayList<processor.audit.Measure>();
											Set<Measure> measures = datasetStructure.measures();
											for (Iterator<Measure> measuresIterator = measures.iterator(); measuresIterator.hasNext();)
											{
												Measure measure = measuresIterator.next();
												String measureId = measure.id().get();
												String measureTitle = measure.title();
												String measureLabel = measure.label();
												String measureDescription = measure.description();
												String measureProperty = (measure.measureProperty() != null ? measure.measureProperty().toString() : "");
												String measureOrder = String.valueOf(measure.order());
												String measureAttachmentId = (measure.componentAttachment() != null ? measure.componentAttachment().toString() : "");
												String measureDataTypeId = (measure.componentDataType() != null ? measure.componentDataType().toString() : "");
												String measureRequired = String.valueOf(measure.componentRequired());
												log.info("                         --> measure  id: \"" + measureId + "\" title: \"" + measureTitle + "\" label: \"" + measureLabel
														+ "\" description: \"" + measureDescription + "\" property: \"" + measureProperty + "\" order: \"" + measureOrder + "\" attachment id: \""
														+ measureAttachmentId + "\" datatype id: \"" + measureDataTypeId + "\" required: \"" + measureRequired + "\"");
												processor.audit.Measure extractedMeasure = new processor.audit.Measure();
												extractedMeasure.setId(measureId);
												extractedMeasure.setTitle(measureTitle);
												extractedMeasure.setLabel(measureLabel);
												extractedMeasure.setDescription(measureDescription);
												extractedMeasure.setProperty(measureProperty);
												extractedMeasure.setOrder(measureOrder);
												extractedMeasure.setAttachment(measureAttachmentId);
												extractedMeasure.setDatatypeId(measureDataTypeId);
												extractedMeasure.setRequired(measureRequired);
												extractedMeasures.add(extractedMeasure);
											}
											extractedDataStructure.setMeasures(extractedMeasures);
											extractedDataSet.setDataStructure(extractedDataStructure);
										}
										extractedDataSelection.setDataset(extractedDataSet);
									}
									extractedDcUpdate.setOldSelection(extractedDataSelection);
								}
							}

							if (dcUpdate.newDataReference() != null)
							{
								org.allotrope.datashapes.datacube.model.DataSelection dataSelection = (org.allotrope.datashapes.datacube.model.DataSelection) object2rdf.read(recordModel.getGraph(),
										NodeFactory.createURI(dcUpdate.newDataReference().id().get()), org.allotrope.datashapes.datacube.model.DataSelection.class);

								if (dataSelection != null)
								{
									String dataSelectionId = dataSelection.id().get();
									String dataSelectionTitle = dataSelection.title();
									String dataSelectionLabel = dataSelection.label();
									String dataSelectionDescription = dataSelection.description();
									log.info("             --> NEW selection id: \"" + dataSelectionId + "\" title: \"" + dataSelectionTitle + "\" label: \"" + dataSelectionLabel
											+ "\" description: \"" + dataSelectionDescription + "\"");
									processor.audit.DataSelection extractedDataSelection = new processor.audit.DataSelection();
									extractedDataSelection.setId(dataSelectionId);
									extractedDataSelection.setTitle(dataSelectionTitle);
									extractedDataSelection.setLabel(dataSelectionLabel);
									extractedDataSelection.setDescription(dataSelectionDescription);

									SelectionStructureDefinition selectionStructure = dataSelection.selectionStructure();
									String selectionStructureId = selectionStructure.id().get();
									String selectionStructureTitle = selectionStructure.title();
									String selectionStructureLabel = selectionStructure.label();
									String selectionStructureDescription = selectionStructure.description();
									log.info("                 --> structure id: \"" + selectionStructureId + "\" title: \"" + selectionStructureTitle + "\" label: \"" + selectionStructureLabel
											+ "\" description: \"" + selectionStructureDescription + "\"");
									processor.audit.DataSelectionStructure extractedDataSelectionStructure = new DataSelectionStructure();
									extractedDataSelectionStructure.setId(selectionStructureId);
									extractedDataSelectionStructure.setTitle(selectionStructureTitle);
									extractedDataSelectionStructure.setLabel(selectionStructureLabel);
									extractedDataSelectionStructure.setDescription(selectionStructureDescription);

									List<processor.audit.ComponentSelection> extractedComponentSelections = new ArrayList<processor.audit.ComponentSelection>();
									Set<org.allotrope.datashapes.datacube.model.ComponentSelection> selectedComponents = selectionStructure.selectedComponents();
									for (Iterator<org.allotrope.datashapes.datacube.model.ComponentSelection> selectedComponentsIterator = selectedComponents.iterator(); selectedComponentsIterator
											.hasNext();)
									{
										org.allotrope.datashapes.datacube.model.ComponentSelection componentSelection = selectedComponentsIterator.next();
										String componentSelectionId = componentSelection.id().get();
										String componentSelectionTitle = componentSelection.title();
										String componentSelectionLabel = componentSelection.label();
										String componentSelectionDescription = componentSelection.description();
										ComponentSpecification selectionOn = componentSelection.selectionOn();
										String componentSelectionOn = (selectionOn != null ? selectionOn.id().get() : "");
										log.info("                     --> selected component id: \"" + componentSelectionId + "\" title: \"" + componentSelectionTitle + "\" label: \""
												+ componentSelectionLabel + "\" description: \"" + componentSelectionDescription + "\" selection on: \"" + componentSelectionOn + "\"");
										processor.audit.ComponentSelection extractedComponentSelection = new processor.audit.ComponentSelection();
										extractedComponentSelection.setId(componentSelectionId);
										extractedComponentSelection.setTitle(componentSelectionTitle);
										extractedComponentSelection.setLabel(componentSelectionLabel);
										extractedComponentSelection.setDescription(componentSelectionDescription);

										if (selectionOn != null)
										{
											String componentSpecificationId = selectionOn.id().get();
											String componentSpecificationTitle = selectionOn.title();
											String componentSpecificationLabel = selectionOn.label();
											String componentSpecificationDescription = selectionOn.description();
											String componentSpecificationAttachmentId = (selectionOn.componentAttachment() != null ? selectionOn.componentAttachment().toString() : "");
											String componentSpecificationDataTypeId = (selectionOn.componentDataType() != null ? selectionOn.componentDataType().toString() : "");
											String componentSpecificationRequired = String.valueOf(selectionOn.componentRequired());
											log.info("                         --> selected component specification id: \"" + componentSpecificationId + "\" title: \"" + componentSpecificationTitle
													+ "\" label: \"" + componentSpecificationLabel + "\" description: \"" + componentSpecificationDescription + "\" attachment id: \""
													+ componentSpecificationAttachmentId + "\" datatype id: \"" + componentSpecificationDataTypeId + "\" required: \"" + componentSpecificationRequired
													+ "\"");
											processor.audit.Component extractedComponentSpecification = new processor.audit.Component();
											extractedComponentSpecification.setId(componentSpecificationId);
											extractedComponentSpecification.setTitle(componentSpecificationTitle);
											extractedComponentSpecification.setLabel(componentSpecificationLabel);
											extractedComponentSpecification.setDescription(componentSpecificationDescription);
											extractedComponentSpecification.setAttachment(componentSpecificationAttachmentId);
											extractedComponentSpecification.setDatatypeId(componentSpecificationDataTypeId);
											extractedComponentSpecification.setRequired(componentSpecificationRequired);
											extractedComponentSelection.setSelectionOn(extractedComponentSpecification);
										}
										extractedComponentSelections.add(extractedComponentSelection);
									}
									extractedDataSelectionStructure.setSelectedComponents(extractedComponentSelections);
									extractedDataSelection.setDataSelectionStructure(extractedDataSelectionStructure);

									DataSet dataset = dataSelection.dataSelectionOf();
									if (dataset != null)
									{
										String datasetId = dataset.id().get();
										String datasetTitle = dataset.title();
										String datasetLabel = dataset.label();
										String datasetDescription = dataset.description();
										log.info("                 --> dataset id: \"" + datasetId + "\" title: \"" + datasetTitle + "\" label: \"" + datasetLabel + "\" description: \""
												+ datasetDescription + "\"");
										processor.audit.Dataset extractedDataSet = new processor.audit.Dataset();
										extractedDataSet.setId(datasetId);
										extractedDataSet.setTitle(datasetTitle);
										extractedDataSet.setLabel(datasetLabel);
										extractedDataSet.setDescription(datasetDescription);

										DataStructureDefinition datasetStructure = dataset.structure();
										if (datasetStructure != null)
										{
											String datasetStructureId = datasetStructure.id().get();
											String datasetStructureTitle = datasetStructure.title();
											String datasetStructureLabel = datasetStructure.label();
											String datasetStructureDescription = datasetStructure.description();
											log.info("                     --> structure id: \"" + datasetStructureId + "\" title: \"" + datasetStructureTitle + "\" label: \"" + datasetStructureLabel
													+ "\" description: \"" + datasetStructureDescription + "\"");
											DataStructure extractedDataStructure = new DataStructure();
											extractedDataStructure.setId(datasetStructureId);
											extractedDataStructure.setTitle(datasetStructureTitle);
											extractedDataStructure.setLabel(datasetStructureLabel);
											extractedDataStructure.setDescription(datasetStructureDescription);

											List<Attribute> extractedDatasetstructureAttributes = new ArrayList<Attribute>();
											Set<org.allotrope.datashapes.datacube.model.Attribute> attributes = datasetStructure.attributes();
											for (Iterator<org.allotrope.datashapes.datacube.model.Attribute> attributesIterator = attributes.iterator(); attributesIterator.hasNext();)
											{
												org.allotrope.datashapes.hdf.model.Attribute attribute = (org.allotrope.datashapes.hdf.model.Attribute) attributesIterator.next();
												String attributeId = attribute.id().get();
												String attributeTitle = attribute.title();
												String attributeLabel = attribute.label();
												String attributeDescription = attribute.description();
												log.info("                         --> attribute id: \"" + attributeId + "\" title: \"" + attributeTitle + "\" label: \"" + attributeLabel
														+ "\" description: \"" + attributeDescription + "\"");
												Attribute extractedDatasetstructureAttribute = new Attribute();
												extractedDatasetstructureAttribute.setId(attributeId);
												extractedDatasetstructureAttribute.setLabel(attributeLabel);
												extractedDatasetstructureAttribute.setTitle(datasetStructureTitle);
												extractedDatasetstructureAttribute.setDescription(attributeDescription);

												Datatype datatype = attribute.datatype();
												String datatypeId = datatype.id().get();
												String datatypeTitle = datatype.title();
												String datatypeLabel = datatype.label();
												String datatypeDescription = datatype.description();
												String datatypeOrder = (datatype.order() != null ? datatype.order().name() : "");
												String datatypeSize = String.valueOf(datatype.size());
												log.info("                             --> datatype  id: \"" + datatypeId + "\" title: \"" + datatypeTitle + "\" label: \"" + datatypeLabel
														+ "\" description: \"" + datatypeDescription + "\" order: \"" + datatypeOrder + "\" size: \"" + datatypeSize + "\"");
												processor.audit.Datatype extractedDatatype = new processor.audit.Datatype();
												extractedDatatype.setId(datatypeId);
												extractedDatatype.setTitle(datatypeTitle);
												extractedDatatype.setLabel(datatypeLabel);
												extractedDatatype.setDescription(datatypeDescription);
												extractedDatatype.setOrder(datatypeOrder);
												extractedDatatype.setSize(datatypeSize);
												extractedDatasetstructureAttribute.setDatatype(extractedDatatype);

												Dataspace dataspace = attribute.dataspace();
												String dataspaceId = dataspace.id().get();
												String dataspaceTitle = dataspace.title();
												String dataspaceLabel = dataspace.label();
												String dataspaceDescription = dataspace.description();
												String dataspaceRank = String.valueOf(dataspace.rank());
												log.info("                             --> dataspace  id: \"" + dataspaceId + "\" title: \"" + dataspaceTitle + "\" label: \"" + dataspaceLabel
														+ "\" description: \"" + dataspaceDescription + "\" rank: \"" + dataspaceRank + "\"");
												processor.audit.Dataspace extractedDataspace = new processor.audit.Dataspace();
												extractedDataspace.setId(dataspaceId);
												extractedDataspace.setTitle(dataspaceTitle);
												extractedDataspace.setLabel(dataspaceLabel);
												extractedDataspace.setDescription(dataspaceDescription);
												extractedDataspace.setRank(dataspaceRank);

												List<Dimension> extractedDimensions = new ArrayList<Dimension>();
												Set<DataspaceDimension> dimensions = dataspace.dimensions();
												for (Iterator<DataspaceDimension> dimensionsIterator = dimensions.iterator(); dimensionsIterator.hasNext();)
												{
													DataspaceDimension dataspaceDimension = dimensionsIterator.next();
													String dataspaceDimensionId = dataspaceDimension.id().get();
													String dataspaceDimensionTitle = dataspaceDimension.title();
													String dataspaceDimensionLabel = dataspaceDimension.label();
													String dataspaceDimensionDescription = dataspaceDimension.description();
													String dataspaceDimensionIndex = String.valueOf(dataspaceDimension.index());
													String dataspaceDimensionSize = String.valueOf(dataspaceDimension.size());
													String dataspaceDimensionCurrentSize = String.valueOf(dataspaceDimension.currentSize());
													String dataspaceDimensionInitialSize = String.valueOf(dataspaceDimension.initialSize());
													String dataspaceDimensionMaxSize = String.valueOf(dataspaceDimension.maximumSize());
													log.info("                                 --> dimension id: \"" + dataspaceDimensionId + "\" title: \"" + dataspaceDimensionTitle + "\" label: \""
															+ dataspaceDimensionLabel + "\" description: \"" + dataspaceDimensionDescription + "\" index: \"" + dataspaceDimensionIndex + "\" size: \""
															+ dataspaceDimensionSize + "\" current size: \"" + dataspaceDimensionCurrentSize + "\" initial size: \"" + dataspaceDimensionInitialSize
															+ "\" maximum size: \"" + dataspaceDimensionMaxSize + "\"");
													Dimension extractedDimension = new Dimension();
													extractedDimension.setId(dataspaceDimensionId);
													extractedDimension.setTitle(dataspaceDimensionTitle);
													extractedDimension.setLabel(dataspaceDimensionLabel);
													extractedDimension.setDescription(dataspaceDimensionDescription);
													extractedDimension.setIndex(dataspaceDimensionIndex);
													extractedDimension.setSize(dataspaceDimensionSize);
													extractedDimension.setCurrentSize(dataspaceDimensionCurrentSize);
													extractedDimension.setInitialSize(dataspaceDimensionInitialSize);
													extractedDimension.setInitialSize(dataspaceDimensionInitialSize);
													extractedDimension.setMaxSize(dataspaceDimensionMaxSize);
													extractedDimensions.add(extractedDimension);
												}
												extractedDataspace.setDimensions(extractedDimensions);
												extractedDatasetstructureAttribute.setDataspace(extractedDataspace);
												extractedDatasetstructureAttributes.add(extractedDatasetstructureAttribute);
											}
											extractedDataStructure.setAttributes(extractedDatasetstructureAttributes);

											Set<ComponentSpecification> components = datasetStructure.components();
											List<Component> extractedComponents = new ArrayList<Component>();
											for (Iterator<ComponentSpecification> componentsIterator = components.iterator(); componentsIterator.hasNext();)
											{
												ComponentSpecification componentSpecification = componentsIterator.next();
												String componentSpecificationId = componentSpecification.id().get();
												String componentSpecificationTitle = componentSpecification.title();
												String componentSpecificationLabel = componentSpecification.label();
												String componentSpecificationDescription = componentSpecification.description();
												String componentSpecificationAttachmentId = (componentSpecification.componentAttachment() != null
														? componentSpecification.componentAttachment().toString() : "");
												String componentSpecificationDataTypeId = (componentSpecification.componentDataType() != null ? componentSpecification.componentDataType().toString()
														: "");
												String componentSpecificationRequired = String.valueOf(componentSpecification.componentRequired());
												log.info("                         --> component specification id: \"" + componentSpecificationId + "\" title: \"" + componentSpecificationTitle
														+ "\" label: \"" + componentSpecificationLabel + "\" description: \"" + componentSpecificationDescription + "\" attachment id: \""
														+ componentSpecificationAttachmentId + "\" datatype id: \"" + componentSpecificationDataTypeId + "\" required: \""
														+ componentSpecificationRequired + "\"");
												Component extractedComponent = new Component();
												extractedComponent.setId(componentSpecificationId);
												extractedComponent.setTitle(componentSpecificationTitle);
												extractedComponent.setLabel(componentSpecificationLabel);
												extractedComponent.setDescription(componentSpecificationDescription);
												extractedComponent.setAttachment(componentSpecificationAttachmentId);
												extractedComponent.setDatatypeId(componentSpecificationDataTypeId);
												extractedComponent.setRequired(componentSpecificationRequired);
												extractedComponents.add(extractedComponent);
											}
											extractedDataStructure.setComponents(extractedComponents);

											List<Dimension> extractedDimensions = new ArrayList<Dimension>();
											List<org.allotrope.datashapes.datacube.model.Dimension> dimensions = datasetStructure.dimensions();
											for (Iterator<org.allotrope.datashapes.datacube.model.Dimension> dimensionsIterator = dimensions.iterator(); dimensionsIterator.hasNext();)
											{
												org.allotrope.datashapes.datacube.model.Dimension dimension = dimensionsIterator.next();
												String dimensionId = dimension.id().get();
												String dimensionTitle = dimension.title();
												String dimensionLabel = dimension.label();
												String dimensionDescription = dimension.description();
												String dimensionProperty = (dimension.dimensionProperty() != null ? dimension.dimensionProperty().toString() : "");
												String dimensionOrder = String.valueOf(dimension.order());
												String dimensionAttachmentId = (dimension.componentAttachment() != null ? dimension.componentAttachment().toString() : "");
												String dimensionDataTypeId = (dimension.componentDataType() != null ? dimension.componentDataType().toString() : "");
												String dimensionRequired = String.valueOf(dimension.componentRequired());
												log.info("                          --> dimension  id: \"" + dimensionId + "\" title: \"" + dimensionTitle + "\" label: \"" + dimensionLabel
														+ "\" description: \"" + dimensionDescription + "\" property: \"" + dimensionProperty + "\" order: \"" + dimensionOrder + "\" attachment id: \""
														+ dimensionAttachmentId + "\" datatype id: \"" + dimensionDataTypeId + "\" required: \"" + dimensionRequired + "\"");
												Dimension extractedDimension = new Dimension();
												extractedDimension.setId(dimensionId);
												extractedDimension.setTitle(dimensionTitle);
												extractedDimension.setLabel(dimensionLabel);
												extractedDimension.setDescription(dimensionDescription);
												extractedDimension.setProperty(dimensionProperty);
												extractedDimension.setOrder(dimensionOrder);
												extractedDimension.setAttachment(dimensionAttachmentId);
												extractedDimension.setDatatypeId(dimensionDataTypeId);
												extractedDimension.setRequired(dimensionRequired);
												extractedDimensions.add(extractedDimension);
											}
											extractedDataStructure.setDimensions(extractedDimensions);

											List<processor.audit.Measure> extractedMeasures = new ArrayList<processor.audit.Measure>();
											Set<Measure> measures = datasetStructure.measures();
											for (Iterator<Measure> measuresIterator = measures.iterator(); measuresIterator.hasNext();)
											{
												Measure measure = measuresIterator.next();
												String measureId = measure.id().get();
												String measureTitle = measure.title();
												String measureLabel = measure.label();
												String measureDescription = measure.description();
												String measureProperty = (measure.measureProperty() != null ? measure.measureProperty().toString() : "");
												String measureOrder = String.valueOf(measure.order());
												String measureAttachmentId = (measure.componentAttachment() != null ? measure.componentAttachment().toString() : "");
												String measureDataTypeId = (measure.componentDataType() != null ? measure.componentDataType().toString() : "");
												String measureRequired = String.valueOf(measure.componentRequired());
												log.info("                         --> measure  id: \"" + measureId + "\" title: \"" + measureTitle + "\" label: \"" + measureLabel
														+ "\" description: \"" + measureDescription + "\" property: \"" + measureProperty + "\" order: \"" + measureOrder + "\" attachment id: \""
														+ measureAttachmentId + "\" datatype id: \"" + measureDataTypeId + "\" required: \"" + measureRequired + "\"");
												processor.audit.Measure extractedMeasure = new processor.audit.Measure();
												extractedMeasure.setId(measureId);
												extractedMeasure.setTitle(measureTitle);
												extractedMeasure.setLabel(measureLabel);
												extractedMeasure.setDescription(measureDescription);
												extractedMeasure.setProperty(measureProperty);
												extractedMeasure.setOrder(measureOrder);
												extractedMeasure.setAttachment(measureAttachmentId);
												extractedMeasure.setDatatypeId(measureDataTypeId);
												extractedMeasure.setRequired(measureRequired);
												extractedMeasures.add(extractedMeasure);
											}
											extractedDataStructure.setMeasures(extractedMeasures);
											extractedDataSet.setDataStructure(extractedDataStructure);
										}
										extractedDataSelection.setDataset(extractedDataSet);

										// TODO extract new data from cube
										org.allotrope.datashapes.datacube.model.DataSelection newDataSelection = (org.allotrope.datashapes.datacube.model.DataSelection) dcUpdate.newDataReference();
										try
										{
											Object archivedDatacubeData = auditTrailService.getArchivedDatacubeData(newDataSelection);
											double[] archivedDatacubeDataValues = (archivedDatacubeData == null ? null : (double[]) archivedDatacubeData);
											log.info("             --> NEW values: \"" + Arrays.toString(archivedDatacubeDataValues) + "\"");
											List<String> extractedValues = new ArrayList<String>(archivedDatacubeDataValues.length);
											for (int j = 0; j < archivedDatacubeDataValues.length; j++)
											{
												double d = archivedDatacubeDataValues[j];
												extractedValues.add(String.valueOf(d));
											}
											extractedDcUpdate.setNewDataValues(extractedValues);
										}
										catch (Exception e)
										{
											// log.info(" --> skipped spurious
											// entry");
										}

									}
									extractedDcUpdate.setNewSelection(extractedDataSelection);
								}
							}
						}
						extractedDcChanges.setUpdates(extractedDcUpdates);
						extractedAuditRecord.setDcChanges(extractedDcChanges);

						// data package changes
						ChangeSet dpChanges = auditTrailService.getAuditRecordDataPackageChanges(auditRecordIri);
						log.info("     --> DP changes id: \"" + dpChanges.id().get() + "\" title: \"" + dpChanges.title() + "\" label: \"" + dpChanges.label() + "\" description: \""
								+ dpChanges.description() + "\" motivated by: \"" + dpChanges.motivatedBy() + "\" subject: \""
								+ (dpChanges.subjectOfChange() == null ? "" : dpChanges.subjectOfChange().id().get()) + "\"");
						DatapackageChanges extractedDatapackageChanges = new DatapackageChanges();
						extractedDatapackageChanges.setId(dpChanges.id().get());
						extractedDatapackageChanges.setTitle(dpChanges.title());
						extractedDatapackageChanges.setLabel(dpChanges.label());
						extractedDatapackageChanges.setDescription(dpChanges.description());
						extractedDatapackageChanges.setMotivatedBy(dpChanges.motivatedBy());
						extractedDatapackageChanges.setSubjectOfChange((dpChanges.subjectOfChange() == null ? "" : dpChanges.subjectOfChange().id().get()));

						com.hp.hpl.jena.rdf.model.Property dctTitle = ResourceFactory.createProperty(Vocabulary.DCT_TITLE.getURI());
						com.hp.hpl.jena.rdf.model.Property dctIdentifier = ResourceFactory.createProperty(Vocabulary.DCT_IDENTIFIER.getURI());
						com.hp.hpl.jena.rdf.model.Property dctCreator = ResourceFactory.createProperty(Vocabulary.DCT_CREATOR.getURI());
						com.hp.hpl.jena.rdf.model.Property dctCreated = ResourceFactory.createProperty(Vocabulary.DCT_CREATED.getURI());
						com.hp.hpl.jena.rdf.model.Property dctModified = ResourceFactory.createProperty(Vocabulary.DCT_MODIFIED.getURI());
						com.hp.hpl.jena.rdf.model.Property dctIsPartOf = ResourceFactory.createProperty(Vocabulary.DCT_IS_PART_OF.getURI());
						com.hp.hpl.jena.rdf.model.Property dctFormat = ResourceFactory.createProperty(Vocabulary.DCT_FORMAT.getURI());
						com.hp.hpl.jena.rdf.model.Property dctDescription = ResourceFactory.createProperty(Vocabulary.DCT_DESCRIPTION.getURI());
						com.hp.hpl.jena.rdf.model.Property modifiedBy = ResourceFactory.createProperty(Vocabulary.ADF_DP_MODIFIED_BY.getURI());
						com.hp.hpl.jena.rdf.model.Property archivedTo = ResourceFactory.createProperty(Vocabulary.AFA_ARCHIVED_TO.getURI());
						com.hp.hpl.jena.rdf.model.Property rdfsLabel = ResourceFactory.createProperty(Vocabulary.RDFS_LABEL.getURI());
						com.hp.hpl.jena.rdf.model.Property representedBy = ResourceFactory.createProperty(Vocabulary.ADF_DP_REPRESENTED_BY.getURI());
						com.hp.hpl.jena.rdf.model.Property fileSize = ResourceFactory.createProperty(Vocabulary.ADF_DP_FILESIZE.getURI());
						com.hp.hpl.jena.rdf.model.Property charSet = ResourceFactory.createProperty(Vocabulary.ADF_DP_CHARSET.getURI());
						com.hp.hpl.jena.rdf.model.Property isCompressed = ResourceFactory.createProperty(Vocabulary.ADF_DP_IS_COMPRESSED.getURI());
						com.hp.hpl.jena.rdf.model.Property lineSeparator = ResourceFactory.createProperty(Vocabulary.ADF_DP_LINESEPARATOR.getURI());
						com.hp.hpl.jena.rdf.model.Property hasMessageDigest = ResourceFactory.createProperty(Vocabulary.ADF_DP_HAS_MESSAGE_DIGEST.getURI());
						com.hp.hpl.jena.rdf.model.Property hasMessageDigestAlgorithm = ResourceFactory.createProperty(Vocabulary.ADF_DP_HAS_MESSAGE_DIGEST_ALGORITHM.getURI());

						List<DatapackageChange> dpAdditions = new ArrayList<DatapackageChange>();
						for (Resource dpAddition : dpChanges.additions())
						{
							String preview = StringUtils.EMPTY;
							String nodeId = StringUtils.EMPTY;
							String nodeUrl = StringUtils.EMPTY;
							String nodeTitle = StringUtils.EMPTY;
							String nodeLabel = StringUtils.EMPTY;
							String nodePath = StringUtils.EMPTY;
							String nodeCreator = StringUtils.EMPTY;
							String nodeCreationTime = StringUtils.EMPTY;
							String nodeModifier = StringUtils.EMPTY;
							String nodeModificationTime = StringUtils.EMPTY;
							String nodeDescription = StringUtils.EMPTY;
							String nodeHash = StringUtils.EMPTY;
							String nodeRepresentedBy = StringUtils.EMPTY;
							String nodeFormat = StringUtils.EMPTY;
							String nodeSize = StringUtils.EMPTY;
							String nodeParent = StringUtils.EMPTY;
							String nodeParentId = StringUtils.EMPTY;
							String nodeCharset = StringUtils.EMPTY;
							String nodeCompressed = StringUtils.EMPTY;
							String nodeLineSeparator = StringUtils.EMPTY;
							String nodeType = StringUtils.EMPTY;
							DataDescription dd = adfFile.getDataDescription();
							if (dpAddition instanceof Folder)
							{
								nodeType = "folder";
								Folder addedFolder = (Folder) dpAddition;
								nodeId = addedFolder.id().get();
								nodeUrl = addedFolder.URL().toString();
								com.hp.hpl.jena.rdf.model.Resource folderResource = dd.createResource(nodeId);
								nodeTitle = addedFolder.title();
								if (nodeTitle == null || nodeTitle.isEmpty())
								{
									nodeTitle = (folderResource.getProperty(dctTitle) != null ? folderResource.getProperty(dctTitle).getString() : "");
								}
								nodeLabel = addedFolder.label();
								if (nodeLabel == null || nodeLabel.isEmpty())
								{
									nodeLabel = (folderResource.getProperty(rdfsLabel) != null ? folderResource.getProperty(rdfsLabel).getString() : "");
								}
								nodePath = addedFolder.path();
								if (nodePath == null || nodePath.isEmpty())
								{
									nodePath = ((folderResource.getProperty(representedBy) != null && folderResource.getProperty(representedBy).getResource() != null)
											? folderResource.getProperty(representedBy).getResource().getURI() : "");
								}
								nodeCreator = (addedFolder.createdBy() != null ? addedFolder.createdBy().id().get() : "");
								if (nodeCreator == null || nodeCreator.isEmpty())
								{
									nodeCreator = ((folderResource.getProperty(dctCreator) != null && folderResource.getProperty(dctCreator).getResource() != null)
											? folderResource.getProperty(dctCreator).getResource().getProperty(dctIdentifier).getString() : "");
								}
								nodeCreationTime = (addedFolder.createdOn() != null ? addedFolder.createdOn().getTime().toString() : "");
								if (nodeCreationTime == null || nodeCreationTime.isEmpty())
								{
									nodeCreationTime = (folderResource.getProperty(dctCreated) != null ? folderResource.getProperty(dctCreated).getString() : "");
								}
								nodeModifier = (addedFolder.modifiedBy() != null ? addedFolder.modifiedBy().id().get() : "");
								if (nodeModifier == null || nodeModifier.isEmpty())
								{
									nodeModifier = ((folderResource.getProperty(modifiedBy) != null && folderResource.getProperty(modifiedBy).getResource() != null)
											? folderResource.getProperty(modifiedBy).getResource().getProperty(dctIdentifier).getString() : "");
								}
								nodeModificationTime = (addedFolder.modifiedOn() != null ? addedFolder.modifiedOn().getTime().toString() : "");
								if (nodeModificationTime == null || nodeModificationTime.isEmpty())
								{
									nodeModificationTime = (folderResource.getProperty(dctModified) != null ? folderResource.getProperty(dctModified).getString() : "");
								}
								nodeDescription = addedFolder.description();
								if (nodeDescription == null || nodeDescription.isEmpty())
								{
									nodeDescription = (folderResource.getProperty(dctDescription) != null ? folderResource.getProperty(dctDescription).getString() : "");
								}
								nodeHash = String.valueOf(addedFolder.hashCode());
								if (nodeHash == null || nodeHash.isEmpty())
								{
									nodeHash = (folderResource.getProperty(hasMessageDigest) != null ? folderResource.getProperty(hasMessageDigest).getString() : "");
									nodeHash = nodeHash
											+ ((folderResource.getProperty(hasMessageDigestAlgorithm) != null && folderResource.getProperty(hasMessageDigestAlgorithm).getResource() != null)
													? " (" + folderResource.getProperty(hasMessageDigest).getResource().getLocalName() + ")" : "");
								}
								nodeRepresentedBy = (addedFolder.representedBy() != null ? addedFolder.representedBy().id().get() : "");
								if (nodeRepresentedBy == null || nodeRepresentedBy.isEmpty())
								{
									nodeRepresentedBy = ((folderResource.getProperty(representedBy) != null && folderResource.getProperty(representedBy).getResource() != null)
											? folderResource.getProperty(representedBy).getResource().getURI() : "");
								}
								nodeParent = (addedFolder.partOf() != null ? addedFolder.partOf().id().get() : "");
								if (nodeParent == null || nodeParent.isEmpty() || nodeParent.startsWith(Vocabulary.URN_UUID))
								{
									nodeParent = ((folderResource.getProperty(dctIsPartOf) != null && folderResource.getProperty(dctIsPartOf).getResource() != null
											&& folderResource.getProperty(dctIsPartOf).getResource().getProperty(representedBy) != null
											&& folderResource.getProperty(dctIsPartOf).getResource().getProperty(representedBy).getResource() != null)
													? folderResource.getProperty(dctIsPartOf).getResource().getProperty(representedBy).getResource().getURI() : "");
								}
								nodeParentId = (addedFolder.partOf() != null ? addedFolder.partOf().id().get() : "");

							}
							if (dpAddition instanceof File)
							{
								nodeType = "file";
								File addedFile = (File) dpAddition;
								nodeId = addedFile.id().get();
								nodeUrl = addedFile.URL().toString();
								com.hp.hpl.jena.rdf.model.Resource fileResource = dd.createResource(nodeId);
								nodeTitle = addedFile.title();
								if (nodeTitle == null || nodeTitle.isEmpty())
								{
									nodeTitle = (fileResource.getProperty(dctTitle) != null ? fileResource.getProperty(dctTitle).getString() : "");
								}
								nodeLabel = addedFile.label();
								if (nodeLabel == null || nodeLabel.isEmpty())
								{
									nodeLabel = (fileResource.getProperty(rdfsLabel) != null ? fileResource.getProperty(rdfsLabel).getString() : "");
								}
								nodePath = addedFile.path();
								if (nodePath == null || nodePath.isEmpty())
								{
									nodePath = ((fileResource.getProperty(representedBy) != null && fileResource.getProperty(representedBy).getResource() != null)
											? fileResource.getProperty(representedBy).getResource().getURI() : "");
								}
								nodeCreator = (addedFile.createdBy() != null ? addedFile.createdBy().id().get() : "");
								if (nodeCreator == null || nodeCreator.isEmpty())
								{
									nodeCreator = ((fileResource.getProperty(dctCreator) != null && fileResource.getProperty(dctCreator).getResource() != null)
											? fileResource.getProperty(dctCreator).getResource().getProperty(dctIdentifier).getString() : "");
								}
								nodeCreationTime = (addedFile.createdOn() != null ? addedFile.createdOn().getTime().toString() : "");
								if (nodeCreationTime == null || nodeCreationTime.isEmpty())
								{
									nodeCreationTime = (fileResource.getProperty(dctCreated) != null ? fileResource.getProperty(dctCreated).getString() : "");
								}
								nodeModifier = (addedFile.modifiedBy() != null ? addedFile.modifiedBy().id().get() : "");
								if (nodeModifier == null || nodeModifier.isEmpty())
								{
									nodeModifier = ((fileResource.getProperty(modifiedBy) != null && fileResource.getProperty(modifiedBy).getResource() != null)
											? fileResource.getProperty(modifiedBy).getResource().getProperty(dctIdentifier).getString() : "");
								}
								nodeModificationTime = (addedFile.modifiedOn() != null ? addedFile.modifiedOn().getTime().toString() : "");
								if (nodeModificationTime == null || nodeModificationTime.isEmpty())
								{
									nodeModificationTime = (fileResource.getProperty(dctModified) != null ? fileResource.getProperty(dctModified).getString() : "");
								}
								nodeDescription = addedFile.description();
								if (nodeDescription == null || nodeDescription.isEmpty())
								{
									nodeDescription = (fileResource.getProperty(dctDescription) != null ? fileResource.getProperty(dctDescription).getString() : "");
								}
								nodeHash = String.valueOf(addedFile.hashCode());
								if (nodeHash == null || nodeHash.isEmpty())
								{
									nodeHash = (fileResource.getProperty(hasMessageDigest) != null ? fileResource.getProperty(hasMessageDigest).getString() : "");
									nodeHash = nodeHash + ((fileResource.getProperty(hasMessageDigestAlgorithm) != null && fileResource.getProperty(hasMessageDigestAlgorithm).getResource() != null)
											? " (" + fileResource.getProperty(hasMessageDigest).getResource().getLocalName() + ")" : "");
								}
								nodeRepresentedBy = (addedFile.representedBy() != null ? addedFile.representedBy().id().get() : "");
								if (nodeRepresentedBy == null || nodeRepresentedBy.isEmpty())
								{
									nodeRepresentedBy = ((fileResource.getProperty(representedBy) != null && fileResource.getProperty(representedBy).getResource() != null)
											? fileResource.getProperty(representedBy).getResource().getURI() : "");
								}
								nodeSize = (addedFile.fileSize() != null ? addedFile.fileSize().toString() : "");
								if (nodeSize == null || nodeSize.isEmpty() || nodeSize.equals("0"))
								{
									nodeSize = (fileResource.getProperty(fileSize) != null ? String.valueOf(fileResource.getProperty(fileSize).getLong()) : "");
								}
								nodeFormat = (addedFile.format() != null ? addedFile.format().toDisplayString() : "");
								if (nodeFormat == null || nodeFormat.isEmpty())
								{
									nodeFormat = ((fileResource.getProperty(dctFormat) != null && fileResource.getProperty(dctFormat).getResource() != null)
											? fileResource.getProperty(dctFormat).getResource().getLocalName() : "");
								}
								nodeParent = (addedFile.partOf() != null ? addedFile.partOf().id().get() : "");
								if (nodeParent == null || nodeParent.isEmpty() || nodeParent.startsWith(Vocabulary.URN_UUID))
								{
									nodeParent = ((fileResource.getProperty(dctIsPartOf) != null && fileResource.getProperty(dctIsPartOf).getResource() != null
											&& fileResource.getProperty(dctIsPartOf).getResource().getProperty(representedBy) != null
											&& fileResource.getProperty(dctIsPartOf).getResource().getProperty(representedBy).getResource() != null)
													? fileResource.getProperty(dctIsPartOf).getResource().getProperty(representedBy).getResource().getURI() : "");
								}
								nodeParentId = (addedFile.partOf() != null ? addedFile.partOf().id().get() : "");
								if (nodeCharset == null || nodeCharset.isEmpty())
								{
									nodeCharset = (fileResource.getProperty(charSet) != null ? fileResource.getProperty(charSet).getString() : "");
								}
								if (nodeCompressed == null || nodeCompressed.isEmpty())
								{
									nodeCompressed = (fileResource.getProperty(isCompressed) != null ? String.valueOf(fileResource.getProperty(isCompressed).getBoolean()) : "");
								}
								if (nodeLineSeparator == null || nodeLineSeparator.isEmpty())
								{
									nodeLineSeparator = ((fileResource.getProperty(lineSeparator) != null && fileResource.getProperty(lineSeparator).getResource() != null)
											? fileResource.getProperty(lineSeparator).getResource().getLocalName() : "");
								}
								if (adfFile.getDataPackage().openFileByUri(dpAddition.id().get()) != null)
								{
									preview = StringUtils.replace(StringUtils.abbreviate(new String(adfFile.getDataPackage().openFileByUri(dpAddition.id().get()).read()), 100), "\r\n", ", ");
								}
							}
							log.info("         --> DP addition id: \"" + nodeId + "\" url: \"" + nodeUrl + "\" title: \"" + nodeTitle + "\" label: \"" + nodeLabel + "\" description: \""
									+ nodeDescription + "\" path: \"" + nodePath + "\" creator: \"" + nodeCreator + "\" created on: \"" + nodeCreationTime + "\" modifier: \"" + nodeModifier
									+ "\" modified on: \"" + nodeModificationTime + "\" hash: \"" + nodeHash + "\" represented by: \"" + nodeRepresentedBy + "\"");
							DatapackageChange datapackageChange = new DatapackageChange();
							datapackageChange.setId(nodeId);
							datapackageChange.setTitle(nodeTitle);
							datapackageChange.setLabel(nodeLabel);
							datapackageChange.setDescription(nodeDescription);
							datapackageChange.setPath(nodePath);
							datapackageChange.setCreator(nodeCreator);
							datapackageChange.setCreatedOn(nodeCreationTime);
							datapackageChange.setModifiedBy(nodeModifier);
							datapackageChange.setModifiedOn(nodeModificationTime);
							datapackageChange.setHash(nodeHash);
							datapackageChange.setRepresentedBy(nodeRepresentedBy);
							datapackageChange.setType(nodeType);

							if (dpAddition instanceof Folder)
							{
								log.info("             --> FOLDER parent: \"" + nodeParent + "\" parent id: \"" + nodeParentId + "\"");
								datapackageChange.setParent(nodeParent);
								datapackageChange.setParentId(nodeParentId);
							}
							if (dpAddition instanceof File)
							{
								log.info("             --> FILE parent: \"" + nodeParent + "\" parent id: \"" + nodeParentId + "\" size: \"" + nodeSize + "\" format: \"" + nodeFormat
										+ "\" charSet: \"" + nodeCharset + "\" compressed: \"" + nodeCompressed + "\" lineseparator: \"" + nodeLineSeparator + "\" preview: \"" + preview + "\"");
								datapackageChange.setParent(nodeParent);
								datapackageChange.setParentId(nodeParentId);
								datapackageChange.setSize(nodeSize);
								datapackageChange.setFormat(nodeFormat);
								datapackageChange.setCharSet(nodeCharset);
								datapackageChange.setCompressed(nodeCompressed);
								datapackageChange.setLineSeparator(nodeLineSeparator);
								datapackageChange.setPreview(preview);
							}

							dpAdditions.add(datapackageChange);
						}
						extractedDatapackageChanges.setAdditions(dpAdditions);

						List<DatapackageChange> dpRemovals = new ArrayList<DatapackageChange>();
						for (Resource dpRemoval : dpChanges.removals())
						{
							String preview = StringUtils.EMPTY;
							String nodeId = StringUtils.EMPTY;
							String nodeUrl = StringUtils.EMPTY;
							String nodeTitle = StringUtils.EMPTY;
							String nodeLabel = StringUtils.EMPTY;
							String nodePath = StringUtils.EMPTY;
							String nodeCreator = StringUtils.EMPTY;
							String nodeCreationTime = StringUtils.EMPTY;
							String nodeModifier = StringUtils.EMPTY;
							String nodeModificationTime = StringUtils.EMPTY;
							String nodeDescription = StringUtils.EMPTY;
							String nodeHash = StringUtils.EMPTY;
							String nodeRepresentedBy = StringUtils.EMPTY;
							String nodeFormat = StringUtils.EMPTY;
							String nodeSize = StringUtils.EMPTY;
							String nodeParent = StringUtils.EMPTY;
							String nodeParentId = StringUtils.EMPTY;
							String nodeCharset = StringUtils.EMPTY;
							String nodeCompressed = StringUtils.EMPTY;
							String nodeLineSeparator = StringUtils.EMPTY;
							String nodeLengthString = StringUtils.EMPTY;
							String nodeType = StringUtils.EMPTY;

							DataDescription dd = adfFile.getDataDescription();
							if (dpRemoval instanceof Folder)
							{
								nodeType = "folder";
								Folder removedFolder = (Folder) dpRemoval;
								nodeId = removedFolder.id().get();
								nodeUrl = removedFolder.URL().toString();
								com.hp.hpl.jena.rdf.model.Resource folderResource = dd.createResource(nodeId);
								nodeTitle = removedFolder.title();
								if (nodeTitle == null || nodeTitle.isEmpty())
								{
									nodeTitle = (folderResource.getProperty(dctTitle) != null ? folderResource.getProperty(dctTitle).getString() : "");
								}
								nodeLabel = removedFolder.label();
								if (nodeLabel == null || nodeLabel.isEmpty())
								{
									nodeLabel = (folderResource.getProperty(rdfsLabel) != null ? folderResource.getProperty(rdfsLabel).getString() : "");
								}
								nodePath = removedFolder.path();
								if (nodePath == null || nodePath.isEmpty())
								{
									nodePath = ((folderResource.getProperty(representedBy) != null && folderResource.getProperty(representedBy).getResource() != null)
											? folderResource.getProperty(representedBy).getResource().getURI() : "");
								}
								nodeCreator = (removedFolder.createdBy() != null ? removedFolder.createdBy().id().get() : "");
								if (nodeCreator == null || nodeCreator.isEmpty())
								{
									nodeCreator = ((folderResource.getProperty(dctCreator) != null && folderResource.getProperty(dctCreator).getResource() != null)
											? folderResource.getProperty(dctCreator).getResource().getProperty(dctIdentifier).getString() : "");
								}
								nodeCreationTime = (removedFolder.createdOn() != null ? removedFolder.createdOn().getTime().toString() : "");
								if (nodeCreationTime == null || nodeCreationTime.isEmpty())
								{
									nodeCreationTime = (folderResource.getProperty(dctCreated) != null ? folderResource.getProperty(dctCreated).getString() : "");
								}
								nodeModifier = (removedFolder.modifiedBy() != null ? removedFolder.modifiedBy().id().get() : "");
								if (nodeModifier == null || nodeModifier.isEmpty())
								{
									nodeModifier = ((folderResource.getProperty(modifiedBy) != null && folderResource.getProperty(modifiedBy).getResource() != null)
											? folderResource.getProperty(modifiedBy).getResource().getProperty(dctIdentifier).getString() : "");
								}
								nodeModificationTime = (removedFolder.modifiedOn() != null ? removedFolder.modifiedOn().getTime().toString() : "");
								if (nodeModificationTime == null || nodeModificationTime.isEmpty())
								{
									nodeModificationTime = (folderResource.getProperty(dctModified) != null ? folderResource.getProperty(dctModified).getString() : "");
								}
								nodeDescription = removedFolder.description();
								if (nodeDescription == null || nodeDescription.isEmpty())
								{
									nodeDescription = (folderResource.getProperty(dctDescription) != null ? folderResource.getProperty(dctDescription).getString() : "");
								}
								nodeHash = String.valueOf(removedFolder.hashCode());
								if (nodeHash == null || nodeHash.isEmpty())
								{
									nodeHash = (folderResource.getProperty(hasMessageDigest) != null ? folderResource.getProperty(hasMessageDigest).getString() : "");
									nodeHash = nodeHash
											+ ((folderResource.getProperty(hasMessageDigestAlgorithm) != null && folderResource.getProperty(hasMessageDigestAlgorithm).getResource() != null)
													? " (" + folderResource.getProperty(hasMessageDigest).getResource().getLocalName() + ")" : "");
								}
								nodeRepresentedBy = (removedFolder.representedBy() != null ? removedFolder.representedBy().id().get() : "");
								if (nodeRepresentedBy == null || nodeRepresentedBy.isEmpty())
								{
									nodeRepresentedBy = ((folderResource.getProperty(representedBy) != null && folderResource.getProperty(representedBy).getResource() != null)
											? folderResource.getProperty(representedBy).getResource().getURI() : "");
								}
								nodeParent = (removedFolder.partOf() != null ? removedFolder.partOf().id().get() : "");
								if (nodeParent != null && !nodeParent.isEmpty())
								{
									com.hp.hpl.jena.rdf.model.Resource parentfolderResource = adfFile.getDataDescription().createResource(nodeParent);
									nodeParent = ((parentfolderResource != null && parentfolderResource.getProperty(representedBy) != null
											&& parentfolderResource.getProperty(representedBy).getResource() != null) ? parentfolderResource.getProperty(representedBy).getResource().getURI() : "");
								}
								nodeParentId = (removedFolder.partOf() != null ? removedFolder.partOf().id().get() : "");
							}
							if (dpRemoval instanceof File)
							{
								nodeType = "file";
								File removedFile = (File) dpRemoval;
								nodeId = removedFile.id().get();
								nodeUrl = removedFile.URL().toString();
								com.hp.hpl.jena.rdf.model.Resource fileResource = dd.createResource(nodeId);
								nodeTitle = removedFile.title();
								if (nodeTitle == null || nodeTitle.isEmpty())
								{
									nodeTitle = (fileResource.getProperty(dctTitle) != null ? fileResource.getProperty(dctTitle).getString() : "");
								}
								nodeLabel = removedFile.label();
								if (nodeLabel == null || nodeLabel.isEmpty())
								{
									nodeLabel = (fileResource.getProperty(rdfsLabel) != null ? fileResource.getProperty(rdfsLabel).getString() : "");
								}
								nodePath = removedFile.path();
								if (nodePath == null || nodePath.isEmpty())
								{
									nodePath = ((fileResource.getProperty(representedBy) != null && fileResource.getProperty(representedBy).getResource() != null)
											? fileResource.getProperty(representedBy).getResource().getURI() : "");
								}
								nodeCreator = (removedFile.createdBy() != null ? removedFile.createdBy().id().get() : "");
								if (nodeCreator == null || nodeCreator.isEmpty())
								{
									nodeCreator = ((fileResource.getProperty(dctCreator) != null && fileResource.getProperty(dctCreator).getResource() != null)
											? fileResource.getProperty(dctCreator).getResource().getProperty(dctIdentifier).getString() : "");
								}
								nodeCreationTime = (removedFile.createdOn() != null ? removedFile.createdOn().getTime().toString() : "");
								if (nodeCreationTime == null || nodeCreationTime.isEmpty())
								{
									nodeCreationTime = (fileResource.getProperty(dctCreated) != null ? fileResource.getProperty(dctCreated).getString() : "");
								}
								nodeModifier = (removedFile.modifiedBy() != null ? removedFile.modifiedBy().id().get() : "");
								if (nodeModifier == null || nodeModifier.isEmpty())
								{
									nodeModifier = ((fileResource.getProperty(modifiedBy) != null && fileResource.getProperty(modifiedBy).getResource() != null)
											? fileResource.getProperty(modifiedBy).getResource().getProperty(dctIdentifier).getString() : "");
								}
								nodeModificationTime = (removedFile.modifiedOn() != null ? removedFile.modifiedOn().getTime().toString() : "");
								if (nodeModificationTime == null || nodeModificationTime.isEmpty())
								{
									nodeModificationTime = (fileResource.getProperty(dctModified) != null ? fileResource.getProperty(dctModified).getString() : "");
								}
								nodeDescription = removedFile.description();
								if (nodeDescription == null || nodeDescription.isEmpty())
								{
									nodeDescription = (fileResource.getProperty(dctDescription) != null ? fileResource.getProperty(dctDescription).getString() : "");
								}
								nodeHash = String.valueOf(removedFile.hashCode());
								if (nodeHash == null || nodeHash.isEmpty())
								{
									nodeHash = (fileResource.getProperty(hasMessageDigest) != null ? fileResource.getProperty(hasMessageDigest).getString() : "");
									nodeHash = nodeHash + ((fileResource.getProperty(hasMessageDigestAlgorithm) != null && fileResource.getProperty(hasMessageDigestAlgorithm).getResource() != null)
											? " (" + fileResource.getProperty(hasMessageDigest).getResource().getLocalName() + ")" : "");
								}
								nodeRepresentedBy = (removedFile.representedBy() != null ? removedFile.representedBy().id().get() : "");
								if (nodeRepresentedBy == null || nodeRepresentedBy.isEmpty())
								{
									nodeRepresentedBy = ((fileResource.getProperty(representedBy) != null && fileResource.getProperty(representedBy).getResource() != null)
											? fileResource.getProperty(representedBy).getResource().getURI() : "");
								}
								nodeSize = (removedFile.fileSize() != null ? removedFile.fileSize().toString() : "");
								if (nodeSize == null || nodeSize.isEmpty() || nodeSize.equals("0"))
								{
									nodeSize = (fileResource.getProperty(fileSize) != null ? String.valueOf(fileResource.getProperty(fileSize).getLong()) : "");
								}
								nodeFormat = (removedFile.format() != null ? removedFile.format().toDisplayString() : "");
								if (nodeFormat == null || nodeFormat.isEmpty())
								{
									nodeFormat = ((fileResource.getProperty(dctFormat) != null && fileResource.getProperty(dctFormat).getResource() != null)
											? fileResource.getProperty(dctFormat).getResource().getLocalName() : "");
								}
								nodeParent = (removedFile.partOf() != null ? removedFile.partOf().id().get() : "");
								if (nodeParent != null && !nodeParent.isEmpty())
								{
									com.hp.hpl.jena.rdf.model.Resource parentfolderResource = adfFile.getDataDescription().createResource(nodeParent);
									nodeParent = ((parentfolderResource != null && parentfolderResource.getProperty(representedBy) != null
											&& parentfolderResource.getProperty(representedBy).getResource() != null) ? parentfolderResource.getProperty(representedBy).getResource().getURI() : "");
								}
								nodeParentId = (removedFile.partOf() != null ? removedFile.partOf().id().get() : "");
								if (nodeCharset == null || nodeCharset.isEmpty())
								{
									nodeCharset = (fileResource.getProperty(charSet) != null ? fileResource.getProperty(charSet).getString() : "");
								}
								if (nodeCompressed == null || nodeCompressed.isEmpty())
								{
									nodeCompressed = (fileResource.getProperty(isCompressed) != null ? String.valueOf(fileResource.getProperty(isCompressed).getBoolean()) : "");
								}
								if (nodeLineSeparator == null || nodeLineSeparator.isEmpty())
								{
									nodeLineSeparator = ((fileResource.getProperty(lineSeparator) != null && fileResource.getProperty(lineSeparator).getResource() != null)
											? fileResource.getProperty(lineSeparator).getResource().getLocalName() : "");
								}
								try (InputStream archivedContent = auditTrailService.openArchivedFile(removedFile))
								{
									byte[] buffer = new byte[removedFile.fileSize().intValue()]; //
									nodeLengthString = String.valueOf(archivedContent.read(buffer, 0, buffer.length));
									preview = StringUtils.replace(StringUtils.abbreviate(new String(buffer), 200), "\r\n", ", ");
								}
							}
							log.info("         --> DP removal id: \"" + nodeId + "\" url: \"" + nodeUrl + "\" title: \"" + nodeTitle + "\" label: \"" + nodeLabel + "\" description: \""
									+ nodeDescription + "\" path: \"" + nodePath + "\" creator: \"" + nodeCreator + "\" created on: \"" + nodeCreationTime + "\" modifier: \"" + nodeModifier
									+ "\" modified on: \"" + nodeModificationTime + "\" hash: \"" + nodeHash + "\" represented by: \"" + nodeRepresentedBy + "\"");
							DatapackageChange datapackageChange = new DatapackageChange();
							datapackageChange.setId(nodeId);
							datapackageChange.setTitle(nodeTitle);
							datapackageChange.setLabel(nodeLabel);
							datapackageChange.setDescription(nodeDescription);
							datapackageChange.setPath(nodePath);
							datapackageChange.setCreator(nodeCreator);
							datapackageChange.setCreatedOn(nodeCreationTime);
							datapackageChange.setModifiedBy(nodeModifier);
							datapackageChange.setModifiedOn(nodeModificationTime);
							datapackageChange.setHash(nodeHash);
							datapackageChange.setRepresentedBy(nodeRepresentedBy);
							datapackageChange.setType(nodeType);

							if (dpRemoval instanceof Folder)
							{
								log.info("             --> FOLDER parent: \"" + nodeParent + "\" parent id: \"" + nodeParentId + "\"");
								datapackageChange.setParent(nodeParent);
								datapackageChange.setParentId(nodeParentId);
							}
							if (dpRemoval instanceof File)
							{
								log.info("             --> FILE parent: \"" + nodeParent + "\" parent id: \"" + nodeParentId + "\" size: \"" + nodeSize + "\" format: \"" + nodeFormat
										+ "\" charSet: \"" + nodeCharset + "\" compressed: \"" + nodeCompressed + "\" lineseparator: \"" + nodeLineSeparator + "\" preview: \"" + preview
										+ "\" total length: \"" + nodeLengthString + "\"");
								datapackageChange.setParent(nodeParent);
								datapackageChange.setParentId(nodeParentId);
								datapackageChange.setSize(nodeSize);
								datapackageChange.setFormat(nodeFormat);
								datapackageChange.setCharSet(nodeCharset);
								datapackageChange.setCompressed(nodeCompressed);
								datapackageChange.setLineSeparator(nodeLineSeparator);
								datapackageChange.setPreview(preview);
								datapackageChange.setPreviewLength(nodeLengthString);
							}
							dpRemovals.add(datapackageChange);
						}
						extractedDatapackageChanges.setRemovals(dpRemovals);

						List<Update> dpUpdates = new ArrayList<Update>();
						updatesIterator = dpChanges.updates().iterator();
						while (updatesIterator.hasNext())
						{
							DataUpdate dataUpdate = updatesIterator.next();
							log.info("         --> DP update id: \"" + dataUpdate.id().get() + "\" title: \"" + dataUpdate.title() + "\" label: \"" + dataUpdate.label() + "\" description: \""
									+ dataUpdate.description() + "\" old file segment: \"" + (dataUpdate.oldDataReference() == null ? "" : dataUpdate.oldDataReference().id().get())
									+ "\" new file segment: \"" + (dataUpdate.newDataReference() == null ? "" : dataUpdate.newDataReference().id().get()) + "\" target: \""
									+ (dataUpdate.target() == null ? "" : dataUpdate.target().id().get()) + "\"");
							Update extractedUpdate = new Update();
							extractedUpdate.setId(dataUpdate.id().get());
							extractedUpdate.setTitle(dataUpdate.title());
							extractedUpdate.setLabel(dataUpdate.label());
							extractedUpdate.setDescription(dataUpdate.description());
							extractedUpdate.setOldDataReference((dataUpdate.oldDataReference() == null ? "" : dataUpdate.oldDataReference().id().get()));
							extractedUpdate.setNewDataReference((dataUpdate.newDataReference() == null ? "" : dataUpdate.newDataReference().id().get()));
							extractedUpdate.setTarget((dataUpdate.target() == null ? "" : dataUpdate.target().id().get()));

							Resource oldSegment = dataUpdate.oldDataReference();
							if (oldSegment != null && oldSegment instanceof Segment)
							{
								Segment oldDpSegment = (Segment) oldSegment;
								File oldFileResource = (File) oldDpSegment.bytestream();
								String segmentId = oldDpSegment.id().get();
								String segmentTitle = oldDpSegment.title();
								String segmentLabel = oldDpSegment.label();
								String segmentDescription = oldDpSegment.description();
								String segmentPosition = String.valueOf(oldDpSegment.position());
								String segmentSize = String.valueOf(oldDpSegment.size());

								byte[] buffer = null;
								try (InputStream archivedContent = auditTrailService.openArchivedFile(oldFileResource))
								{
									buffer = new byte[oldFileResource.fileSize().intValue()]; //
									archivedContent.read(buffer, 0, buffer.length);
								}

								byte[] fileSegment = Arrays.copyOfRange(buffer, oldDpSegment.position().intValue(), oldDpSegment.position().intValue() + oldDpSegment.size().intValue());
								String preview = StringUtils.replace(StringUtils.abbreviate(new String(fileSegment), 200), "\r\n", ", ");
								log.info("             --> OLD segment id: \"" + segmentId + "\" title: \"" + segmentTitle + "\" label: \"" + segmentLabel + "\" description: \"" + segmentDescription
										+ "\" position: \"" + segmentPosition + "\" size: \"" + segmentSize + "\" preview: \"" + preview + "\"");
								processor.audit.Segment extractedSegment = new processor.audit.Segment();
								extractedSegment.setId(segmentId);
								extractedSegment.setTitle(segmentTitle);
								extractedSegment.setLabel(segmentLabel);
								extractedSegment.setDescription(segmentDescription);
								extractedSegment.setPosition(segmentPosition);
								extractedSegment.setSize(segmentSize);
								extractedSegment.setPreview(preview);
								extractedUpdate.setOldSegment(extractedSegment);
							}

							Resource newSegment = dataUpdate.newDataReference();
							if (newSegment != null && newSegment instanceof Segment)
							{
								Segment newDpSegment = (Segment) newSegment;
								File newFileResource = (File) newDpSegment.bytestream();
								DpFile newFile = adfFile.getDataPackage().openFileByUri(newFileResource.id().get());
								String segmentId = newDpSegment.id().get();
								String segmentTitle = newDpSegment.title();
								String segmentLabel = newDpSegment.label();
								String segmentDescription = newDpSegment.description();
								String segmentPosition = String.valueOf(newDpSegment.position());
								String segmentSize = String.valueOf(newDpSegment.size());
								byte[] buffer = new byte[(int) newFile.getSize()]; //
								buffer = newFile.read();
								byte[] fileSegment = Arrays.copyOfRange(buffer, newDpSegment.position().intValue(), newDpSegment.position().intValue() + newDpSegment.size().intValue());
								String preview = StringUtils.replace(StringUtils.abbreviate(new String(fileSegment), 200), "\r\n", ", ");
								log.info("             --> NEW segment id: \"" + segmentId + "\" title: \"" + segmentTitle + "\" label: \"" + segmentLabel + "\" description: \"" + segmentDescription
										+ "\" position: \"" + segmentPosition + "\" size: \"" + segmentSize + "\" preview: \"" + preview + "\"");
								processor.audit.Segment extractedSegment = new processor.audit.Segment();
								extractedSegment.setId(segmentId);
								extractedSegment.setTitle(segmentTitle);
								extractedSegment.setLabel(segmentLabel);
								extractedSegment.setDescription(segmentDescription);
								extractedSegment.setPosition(segmentPosition);
								extractedSegment.setSize(segmentSize);
								extractedSegment.setPreview(preview);
								extractedUpdate.setNewSegment(extractedSegment);
							}
							dpUpdates.add(extractedUpdate);
						}
						extractedDatapackageChanges.setUpdates(dpUpdates);
						extractedAuditRecord.setDpChanges(extractedDatapackageChanges);

						// HDF changes
						ChangeSet hdfChanges = auditTrailService.getAuditRecordHDFChanges(auditRecordIri);
						log.info("     --> HDF changes id: \"" + hdfChanges.id().get() + "\" title: \"" + hdfChanges.title() + "\" label: \"" + hdfChanges.label() + "\" description: \""
								+ hdfChanges.description() + "\" motivated by: \"" + hdfChanges.motivatedBy() + "\" subject: \""
								+ (hdfChanges.subjectOfChange() == null ? "" : hdfChanges.subjectOfChange().id().get()) + "\"");
						HdfChanges extractedHdfChanges = new HdfChanges();
						extractedHdfChanges.setId(hdfChanges.id().get());
						extractedHdfChanges.setTitle(hdfChanges.title());
						extractedHdfChanges.setLabel(hdfChanges.label());
						extractedHdfChanges.setDescription(hdfChanges.description());
						extractedHdfChanges.setMotivatedBy(hdfChanges.motivatedBy());
						extractedHdfChanges.setSubjectOfChange((hdfChanges.subjectOfChange() == null ? "" : hdfChanges.subjectOfChange().id().get()));

						List<HdfChange> hdfAdditions = new ArrayList<HdfChange>();
						for (Resource hdfAddition : hdfChanges.additions())
						{
							HdfChange extractedHdfChange = new HdfChange();
							if (hdfAddition instanceof org.allotrope.datashapes.hdf.model.Dataset)
							{
								org.allotrope.datashapes.hdf.model.Dataset dataset = (org.allotrope.datashapes.hdf.model.Dataset) hdfAddition;
								String datasetId = dataset.id().get();
								String datasetTitle = dataset.title();
								String datasetLabel = dataset.label();
								String datasetName = dataset.name();
								String datasetDescription = dataset.description();
								String datasetTime = (dataset.allocationTime() != null ? dataset.allocationTime().name() : "");
								String datasetAtomicFillValue = (dataset.atomicFillValue() != null ? dataset.atomicFillValue().lexicalValue() : "");
								String datasetCompressionLevel = String.valueOf(dataset.compressionLevel());
								String datasetDatatype = (dataset.datatype() != null ? dataset.datatype().id().get() : "");
								log.info("         --> added dataset id: \"" + datasetId + "\" title: \"" + datasetTitle + "\" label: \"" + datasetLabel + "\" description: \"" + datasetDescription
										+ "\" name: \"" + datasetName + "\" time: \"" + datasetTime + "\" fill value: \"" + datasetAtomicFillValue + "\" compression: \"" + datasetCompressionLevel
										+ "\" datatype: \"" + datasetDatatype + "\"");
								extractedHdfChange.setId(datasetId);
								extractedHdfChange.setLabel(datasetLabel);
								extractedHdfChange.setTitle(datasetTitle);
								extractedHdfChange.setDescription(datasetDescription);
								extractedHdfChange.setName(datasetName);
								extractedHdfChange.setTime(datasetTime);
								extractedHdfChange.setAtomicFillValue(datasetAtomicFillValue);
								extractedHdfChange.setCompressionLevel(datasetCompressionLevel);
								extractedHdfChange.setDataType(datasetDatatype);
								extractedHdfChange.setChangeType("dataset");
							}
							else if (hdfAddition instanceof org.allotrope.datashapes.hdf.model.Group)
							{
								org.allotrope.datashapes.hdf.model.Group group = (org.allotrope.datashapes.hdf.model.Group) hdfAddition;
								String groupId = group.id().get();
								String groupTitle = group.title();
								String groupLabel = group.label();
								String groupName = group.name();
								String groupDescription = group.description();
								log.info("         --> added group id: \"" + groupId + "\" title: \"" + groupTitle + "\" label: \"" + groupLabel + "\" description: \"" + groupDescription
										+ "\" name: \"" + groupName + "\"");
								extractedHdfChange.setId(groupId);
								extractedHdfChange.setLabel(groupLabel);
								extractedHdfChange.setTitle(groupTitle);
								extractedHdfChange.setDescription(groupDescription);
								extractedHdfChange.setName(groupName);
								extractedHdfChange.setChangeType("group");
							}
							hdfAdditions.add(extractedHdfChange);
						}
						extractedHdfChanges.setAdditions(hdfAdditions);

						List<HdfChange> hdfRemovals = new ArrayList<HdfChange>();
						for (Resource hdfRemoval : hdfChanges.removals())
						{
							HdfChange extractedHdfChange = new HdfChange();
							if (hdfRemoval instanceof org.allotrope.datashapes.hdf.model.Dataset)
							{
								org.allotrope.datashapes.hdf.model.Dataset dataset = (org.allotrope.datashapes.hdf.model.Dataset) hdfRemoval;
								String datasetId = dataset.id().get();
								String datasetTitle = dataset.title();
								String datasetLabel = dataset.label();
								String datasetName = dataset.name();
								String datasetDescription = dataset.description();
								String datasetTime = (dataset.allocationTime() != null ? dataset.allocationTime().name() : "");
								String datasetAtomicFillValue = (dataset.atomicFillValue() != null ? dataset.atomicFillValue().lexicalValue() : "");
								String datasetCompressionLevel = String.valueOf(dataset.compressionLevel());
								String datasetDatatype = (dataset.datatype() != null ? dataset.datatype().id().get() : "");

								String datasetArchivedTo = StringUtils.EMPTY;
								com.hp.hpl.jena.rdf.model.Resource datasetResource = adfFile.getDataDescription().createResource(datasetId);
								if (datasetResource.hasProperty(archivedTo))
								{
									datasetArchivedTo = datasetResource.getPropertyResourceValue(archivedTo).getURI();
								}
								log.info("         --> removed dataset id: \"" + datasetId + "\" title: \"" + datasetTitle + "\" label: \"" + datasetLabel + "\" description: \"" + datasetDescription
										+ "\" name: \"" + datasetName + "\" time: \"" + datasetTime + "\" fill value: \"" + datasetAtomicFillValue + "\" compression: \"" + datasetCompressionLevel
										+ "\" datatype: \"" + datasetDatatype + "\" archived to: \"" + datasetArchivedTo + "\"");
								extractedHdfChange.setId(datasetId);
								extractedHdfChange.setLabel(datasetLabel);
								extractedHdfChange.setTitle(datasetTitle);
								extractedHdfChange.setDescription(datasetDescription);
								extractedHdfChange.setName(datasetName);
								extractedHdfChange.setTime(datasetTime);
								extractedHdfChange.setAtomicFillValue(datasetAtomicFillValue);
								extractedHdfChange.setCompressionLevel(datasetCompressionLevel);
								extractedHdfChange.setDataType(datasetDatatype);
								extractedHdfChange.setArchivedTo(datasetArchivedTo);
								extractedHdfChange.setChangeType("dataset");
							}
							else if (hdfRemoval instanceof org.allotrope.datashapes.hdf.model.Group)
							{
								org.allotrope.datashapes.hdf.model.Group group = (org.allotrope.datashapes.hdf.model.Group) hdfRemoval;
								String groupId = group.id().get();
								String groupTitle = group.title();
								String groupLabel = group.label();
								String groupName = group.name();
								String groupDescription = group.description();
								log.info("         --> removed group id: \"" + groupId + "\" title: \"" + groupTitle + "\" label: \"" + groupLabel + "\" description: \"" + groupDescription
										+ "\" name: \"" + groupName + "\"");
								extractedHdfChange.setId(groupId);
								extractedHdfChange.setLabel(groupLabel);
								extractedHdfChange.setTitle(groupTitle);
								extractedHdfChange.setDescription(groupDescription);
								extractedHdfChange.setName(groupName);
								extractedHdfChange.setChangeType("group");
							}
							hdfRemovals.add(extractedHdfChange);
						}
						extractedHdfChanges.setRemovals(hdfRemovals);

						List<Update> hdfUpdates = new ArrayList<Update>();
						for (DataUpdate hdfUpdate : hdfChanges.updates())
						{
							String hdfUpdateId = hdfUpdate.id().get();
							String hdfUpdateTitle = hdfUpdate.title();
							String hdfUpdateLabel = hdfUpdate.label();
							String hdfUpdateDescription = hdfUpdate.description();
							DataUpdate previousUpdate = hdfUpdate.previousUpdate();
							String previousUpdateString = (previousUpdate != null ? previousUpdate.id().get() : "");
							Resource newDataReference = hdfUpdate.newDataReference();
							String newDataReferenceUri = (newDataReference != null ? newDataReference.id().get() : "");
							Resource oldDataReference = hdfUpdate.oldDataReference();
							String oldDataReferenceUri = (oldDataReference != null ? oldDataReference.id().get() : "");
							Resource targetReference = hdfUpdate.target();
							String targetReferenceUri = (targetReference != null ? targetReference.id().get() : "");
							log.info("         --> update id: \"" + hdfUpdateId + "\" title: \"" + hdfUpdateTitle + "\" label: \"" + hdfUpdateLabel + "\" description: \"" + hdfUpdateDescription
									+ "\" previous update id: \"" + previousUpdateString + "\" new data reference: \"" + newDataReferenceUri + "\" old data reference: \"" + oldDataReferenceUri
									+ "\" target: \"" + targetReferenceUri + "\"");
							processor.audit.Update extractedHdfUpdate = new Update();
							extractedHdfUpdate.setId(hdfUpdateId);
							extractedHdfUpdate.setTitle(hdfUpdateTitle);
							extractedHdfUpdate.setLabel(hdfUpdateLabel);
							extractedHdfUpdate.setDescription(hdfUpdateDescription);
							extractedHdfUpdate.setPrevious(previousUpdateString);
							extractedHdfUpdate.setNewDataReference(newDataReferenceUri);
							extractedHdfUpdate.setOldDataReference(oldDataReferenceUri);
							extractedHdfUpdate.setTarget(targetReferenceUri);

							if (newDataReference != null)
							{
								org.allotrope.datashapes.hdf.model.Selection newSelection = (org.allotrope.datashapes.hdf.model.Selection) hdfUpdate.newDataReference();
								String selectionId = newSelection.id().get();
								String selectionTitle = newSelection.title();
								String selectionLabel = newSelection.label();
								String selectionDescription = newSelection.description();
								log.info("             --> NEW selection id: \"" + selectionId + "\" title: \"" + selectionTitle + "\" label: \"" + selectionLabel + "\" description: \""
										+ selectionDescription + "\"");
								processor.audit.DataSelection extractedSelection = new DataSelection();
								extractedSelection.setId(selectionId);
								extractedSelection.setTitle(selectionTitle);
								extractedSelection.setLabel(selectionLabel);
								extractedSelection.setDescription(selectionDescription);

								org.allotrope.datashapes.hdf.model.Attribute selectionAttribute = newSelection.selectedAttribute();
								if (selectionAttribute != null)
								{
									String selectionAttributeId = selectionAttribute.id().get();
									String selectionAttributeTitle = selectionAttribute.title();
									String selectionAttributeLabel = selectionAttribute.label();
									String selectionAttributeName = selectionAttribute.name();
									log.info("                 --> NEW attribute id: \"" + selectionAttributeId + "\" title: \"" + selectionAttributeTitle + "\" label: \"" + selectionAttributeLabel
											+ "\" name: \"" + selectionAttributeName + "\"");
									Attribute extractedAttribute = new Attribute();
									extractedAttribute.setId(selectionAttributeId);
									extractedAttribute.setTitle(selectionAttributeTitle);
									extractedAttribute.setLabel(selectionAttributeLabel);
									extractedAttribute.setName(selectionAttributeName);
									extractedSelection.setAttribute(extractedAttribute);
								}

								org.allotrope.datashapes.hdf.model.Dataset selectionDataset = newSelection.selectedDataset();
								String selectionDatasetId = StringUtils.EMPTY;
								String selectionDatasetLabel = StringUtils.EMPTY;
								String selectionDatasetTitle = StringUtils.EMPTY;
								String selectionDatasetName = StringUtils.EMPTY;
								String selectionDatasetDescription = StringUtils.EMPTY;
								String selectionDatasetAllocTimeString = StringUtils.EMPTY;
								String selectionDatasetFillTimeString = StringUtils.EMPTY;
								String selectionDatasetFillStateString = StringUtils.EMPTY;
								String selectionDatasetScaleTypeString = StringUtils.EMPTY;
								if (selectionDataset != null)
								{
									selectionDatasetId = selectionDataset.id().get();
									selectionDatasetLabel = selectionDataset.label();
									selectionDatasetTitle = selectionDataset.title();
									selectionDatasetName = selectionDataset.name();
									selectionDatasetDescription = selectionDataset.description();
									StorageAllocation selectionDatasetAllocTime = selectionDataset.allocationTime();
									selectionDatasetAllocTimeString = (selectionDatasetAllocTime != null ? selectionDatasetAllocTime.name() : "");
									FillTime selectionDatasetFillTime = selectionDataset.fillTime();
									selectionDatasetFillTimeString = (selectionDatasetFillTime != null ? selectionDatasetFillTime.name() : "");
									FillValueStatus selectionDatasetFillState = selectionDataset.fillValueStatus();
									selectionDatasetFillStateString = (selectionDatasetFillState != null ? selectionDatasetFillState.name() : "");
									ScaleType selectionDatasetScale = selectionDataset.scaleType();
									selectionDatasetScaleTypeString = (selectionDatasetScale != null ? selectionDatasetScale.name() : "");
									log.info("                 --> NEW dataset id: \"" + selectionDatasetId + "\" title: \"" + selectionDatasetTitle + "\" label: \"" + selectionDatasetLabel
											+ "\" name: \"" + selectionDatasetName + "\" description: \"" + selectionDatasetDescription + "\" alloc time: \"" + selectionDatasetAllocTimeString
											+ "\" fill time: \"" + selectionDatasetFillTimeString + "\" fill state: \"" + selectionDatasetFillStateString + "\" scale type: \""
											+ selectionDatasetScaleTypeString + "\"");
									processor.audit.Dataset extractedDataset = new processor.audit.Dataset();
									extractedDataset.setId(selectionDatasetId);
									extractedDataset.setTitle(selectionDatasetTitle);
									extractedDataset.setLabel(selectionDatasetLabel);
									extractedDataset.setDescription(selectionDatasetDescription);
									extractedDataset.setAllocTime(selectionDatasetAllocTimeString);
									extractedDataset.setFillTime(selectionDatasetFillTimeString);
									extractedDataset.setFillState(selectionDatasetFillStateString);
									extractedDataset.setScaleType(selectionDatasetScaleTypeString);
									extractedSelection.setDataset(extractedDataset);
								}

								Dataspace selectionDataspace = newSelection.selectionOn();
								String selectionDataspaceId = selectionDataspace.id().get();
								String selectionDataspaceTitle = selectionDataspace.title();
								String selectionDataspaceLabel = selectionDataspace.label();
								String selectionDataspaceDescription = selectionDataspace.description();
								String selectionDataspaceRank = String.valueOf(selectionDataspace.rank());
								log.info("                 --> NEW dataspace id: \"" + selectionDataspaceId + "\" title: \"" + selectionDataspaceTitle + "\" label: \"" + selectionDataspaceLabel
										+ "\" description: \"" + selectionDataspaceDescription + "\" rank: \"" + selectionDataspaceRank + "\"");
								processor.audit.Dataspace extractedDataspace = new processor.audit.Dataspace();
								extractedDataspace.setId(selectionDataspaceId);
								extractedDataspace.setTitle(selectionDataspaceTitle);
								extractedDataspace.setLabel(selectionDataspaceLabel);
								extractedDataspace.setDescription(selectionDataspaceDescription);
								extractedDataspace.setRank(selectionDataspaceRank);

								List<Dimension> extractedDimensions = new ArrayList<Dimension>();
								Set<DataspaceDimension> dimensions = selectionDataspace.dimensions();
								if (dimensions != null && !dimensions.isEmpty())
								{
									for (Iterator<DataspaceDimension> dimensionIterator = dimensions.iterator(); dimensionIterator.hasNext();)
									{
										DataspaceDimension dataspaceDimension = dimensionIterator.next();
										String dataspaceDimensionId = dataspaceDimension.id().get();
										String dataspaceDimensionTitle = dataspaceDimension.title();
										String dataspaceDimensionLabel = dataspaceDimension.label();
										String dataspaceDimensionDescription = dataspaceDimension.description();
										String dataspaceDimensionIndex = String.valueOf(dataspaceDimension.index());
										String dataspaceDimensionSize = String.valueOf(dataspaceDimension.size());
										String dataspaceDimensionCurrentSize = String.valueOf(dataspaceDimension.currentSize());
										String dataspaceDimensionInitialSize = String.valueOf(dataspaceDimension.initialSize());
										String dataspaceDimensionMaxSize = String.valueOf(dataspaceDimension.maximumSize());
										log.info("                     --> NEW dimension id: \"" + dataspaceDimensionId + "\" title: \"" + dataspaceDimensionTitle + "\" label: \""
												+ dataspaceDimensionLabel + "\" description: \"" + dataspaceDimensionDescription + "\" index: \"" + dataspaceDimensionIndex + "\" size: \""
												+ dataspaceDimensionSize + "\" current size: \"" + dataspaceDimensionCurrentSize + "\" initial size: \"" + dataspaceDimensionInitialSize
												+ "\" maximum size: \"" + dataspaceDimensionMaxSize + "\"");
										Dimension extractedDimension = new Dimension();
										extractedDimension.setId(dataspaceDimensionId);
										extractedDimension.setTitle(dataspaceDimensionTitle);
										extractedDimension.setLabel(dataspaceDimensionLabel);
										extractedDimension.setDescription(dataspaceDimensionDescription);
										extractedDimension.setIndex(dataspaceDimensionIndex);
										extractedDimension.setSize(dataspaceDimensionSize);
										extractedDimension.setCurrentSize(dataspaceDimensionCurrentSize);
										extractedDimension.setInitialSize(dataspaceDimensionInitialSize);
										extractedDimension.setMaxSize(dataspaceDimensionMaxSize);
										extractedDimensions.add(extractedDimension);
									}
									extractedDataspace.setDimensions(extractedDimensions);
								}
								extractedSelection.setSelectionOn(extractedDataspace);
								extractedHdfUpdate.setNewSelection(extractedSelection);
							}

							if (oldDataReference != null)
							{
								org.allotrope.datashapes.hdf.model.Selection oldSelection = (org.allotrope.datashapes.hdf.model.Selection) hdfUpdate.oldDataReference();
								String selectionId = oldSelection.id().get();
								String selectionTitle = oldSelection.title();
								String selectionLabel = oldSelection.label();
								String selectionDescription = oldSelection.description();
								log.info("             --> OLD selection id: \"" + selectionId + "\" title: \"" + selectionTitle + "\" label: \"" + selectionLabel + "\" description: \""
										+ selectionDescription + "\"");
								processor.audit.DataSelection extractedSelection = new DataSelection();
								extractedSelection.setId(selectionId);
								extractedSelection.setTitle(selectionTitle);
								extractedSelection.setLabel(selectionLabel);
								extractedSelection.setDescription(selectionDescription);

								org.allotrope.datashapes.hdf.model.Attribute selectionAttribute = oldSelection.selectedAttribute();
								if (selectionAttribute != null)
								{
									String selectionAttributeId = selectionAttribute.id().get();
									String selectionAttributeTitle = selectionAttribute.title();
									String selectionAttributeLabel = selectionAttribute.label();
									String selectionAttributeName = selectionAttribute.name();
									log.info("                 --> OLD attribute id: \"" + selectionAttributeId + "\" title: \"" + selectionAttributeTitle + "\" label: \"" + selectionAttributeLabel
											+ "\" name: \"" + selectionAttributeName + "\"");
									Attribute extractedAttribute = new Attribute();
									extractedAttribute.setId(selectionAttributeId);
									extractedAttribute.setTitle(selectionAttributeTitle);
									extractedAttribute.setLabel(selectionAttributeLabel);
									extractedAttribute.setName(selectionAttributeName);
									extractedSelection.setAttribute(extractedAttribute);
								}

								org.allotrope.datashapes.hdf.model.Dataset selectionDataset = oldSelection.selectedDataset();
								String selectionDatasetId = StringUtils.EMPTY;
								String selectionDatasetLabel = StringUtils.EMPTY;
								String selectionDatasetTitle = StringUtils.EMPTY;
								String selectionDatasetName = StringUtils.EMPTY;
								String selectionDatasetDescription = StringUtils.EMPTY;
								String selectionDatasetAllocTimeString = StringUtils.EMPTY;
								String selectionDatasetFillTimeString = StringUtils.EMPTY;
								String selectionDatasetFillStateString = StringUtils.EMPTY;
								String selectionDatasetScaleTypeString = StringUtils.EMPTY;
								if (selectionDataset != null)
								{
									selectionDatasetId = selectionDataset.id().get();
									selectionDatasetLabel = selectionDataset.label();
									selectionDatasetTitle = selectionDataset.title();
									selectionDatasetName = selectionDataset.name();
									selectionDatasetDescription = selectionDataset.description();
									StorageAllocation selectionDatasetAllocTime = selectionDataset.allocationTime();
									selectionDatasetAllocTimeString = (selectionDatasetAllocTime != null ? selectionDatasetAllocTime.name() : "");
									FillTime selectionDatasetFillTime = selectionDataset.fillTime();
									selectionDatasetFillTimeString = (selectionDatasetFillTime != null ? selectionDatasetFillTime.name() : "");
									FillValueStatus selectionDatasetFillState = selectionDataset.fillValueStatus();
									selectionDatasetFillStateString = (selectionDatasetFillState != null ? selectionDatasetFillState.name() : "");
									ScaleType selectionDatasetScale = selectionDataset.scaleType();
									selectionDatasetScaleTypeString = (selectionDatasetScale != null ? selectionDatasetScale.name() : "");
									log.info("                 --> OLD dataset id: \"" + selectionDatasetId + "\" title: \"" + selectionDatasetTitle + "\" label: \"" + selectionDatasetLabel
											+ "\" name: \"" + selectionDatasetName + "\" description: \"" + selectionDatasetDescription + "\" alloc time: \"" + selectionDatasetAllocTimeString
											+ "\" fill time: \"" + selectionDatasetFillTimeString + "\" fill state: \"" + selectionDatasetFillStateString + "\" scale type: \""
											+ selectionDatasetScaleTypeString + "\"");
									processor.audit.Dataset extractedDataset = new processor.audit.Dataset();
									extractedDataset.setId(selectionDatasetId);
									extractedDataset.setTitle(selectionDatasetTitle);
									extractedDataset.setLabel(selectionDatasetLabel);
									extractedDataset.setDescription(selectionDatasetDescription);
									extractedDataset.setAllocTime(selectionDatasetAllocTimeString);
									extractedDataset.setFillTime(selectionDatasetFillTimeString);
									extractedDataset.setFillState(selectionDatasetFillStateString);
									extractedDataset.setScaleType(selectionDatasetScaleTypeString);
									extractedSelection.setDataset(extractedDataset);
								}

								Dataspace selectionDataspace = oldSelection.selectionOn();
								String selectionDataspaceId = selectionDataspace.id().get();
								String selectionDataspaceTitle = selectionDataspace.title();
								String selectionDataspaceLabel = selectionDataspace.label();
								String selectionDataspaceDescription = selectionDataspace.description();
								String selectionDataspaceRank = String.valueOf(selectionDataspace.rank());
								log.info("                 --> OLD dataspace id: \"" + selectionDataspaceId + "\" title: \"" + selectionDataspaceTitle + "\" label: \"" + selectionDataspaceLabel
										+ "\" description: \"" + selectionDataspaceDescription + "\" rank: \"" + selectionDataspaceRank + "\"");
								processor.audit.Dataspace extractedDataspace = new processor.audit.Dataspace();
								extractedDataspace.setId(selectionDataspaceId);
								extractedDataspace.setTitle(selectionDataspaceTitle);
								extractedDataspace.setLabel(selectionDataspaceLabel);
								extractedDataspace.setDescription(selectionDataspaceDescription);
								extractedDataspace.setRank(selectionDataspaceRank);

								List<Dimension> extractedDimensions = new ArrayList<Dimension>();
								Set<DataspaceDimension> dimensions = selectionDataspace.dimensions();
								if (dimensions != null && !dimensions.isEmpty())
								{
									for (Iterator<DataspaceDimension> dimensionIterator = dimensions.iterator(); dimensionIterator.hasNext();)
									{
										DataspaceDimension dataspaceDimension = dimensionIterator.next();
										String dataspaceDimensionId = dataspaceDimension.id().get();
										String dataspaceDimensionTitle = dataspaceDimension.title();
										String dataspaceDimensionLabel = dataspaceDimension.label();
										String dataspaceDimensionDescription = dataspaceDimension.description();
										String dataspaceDimensionIndex = String.valueOf(dataspaceDimension.index());
										String dataspaceDimensionSize = String.valueOf(dataspaceDimension.size());
										String dataspaceDimensionCurrentSize = String.valueOf(dataspaceDimension.currentSize());
										String dataspaceDimensionInitialSize = String.valueOf(dataspaceDimension.initialSize());
										String dataspaceDimensionMaxSize = String.valueOf(dataspaceDimension.maximumSize());
										log.info("                     --> OLD dimension id: \"" + dataspaceDimensionId + "\" title: \"" + dataspaceDimensionTitle + "\" label: \""
												+ dataspaceDimensionLabel + "\" description: \"" + dataspaceDimensionDescription + "\" index: \"" + dataspaceDimensionIndex + "\" size: \""
												+ dataspaceDimensionSize + "\" current size: \"" + dataspaceDimensionCurrentSize + "\" initial size: \"" + dataspaceDimensionInitialSize
												+ "\" maximum size: \"" + dataspaceDimensionMaxSize + "\"");
										Dimension extractedDimension = new Dimension();
										extractedDimension.setId(dataspaceDimensionId);
										extractedDimension.setTitle(dataspaceDimensionTitle);
										extractedDimension.setLabel(dataspaceDimensionLabel);
										extractedDimension.setDescription(dataspaceDimensionDescription);
										extractedDimension.setIndex(dataspaceDimensionIndex);
										extractedDimension.setSize(dataspaceDimensionSize);
										extractedDimension.setCurrentSize(dataspaceDimensionCurrentSize);
										extractedDimension.setInitialSize(dataspaceDimensionInitialSize);
										extractedDimension.setMaxSize(dataspaceDimensionMaxSize);
										extractedDimensions.add(extractedDimension);
									}
									extractedDataspace.setDimensions(extractedDimensions);
								}
								extractedSelection.setSelectionOn(extractedDataspace);

								com.hp.hpl.jena.rdf.model.Resource oldSelectionResource = recordModel.getResource(selectionId);
								if (recordModel.listStatements(oldSelectionResource, archivedTo, (RDFNode) null).hasNext())
								{
									DataSelection extractedArchivedSelection = new DataSelection();
									com.hp.hpl.jena.rdf.model.Resource archivedSelection = recordModel.listStatements(oldSelectionResource, archivedTo, (RDFNode) null).next().getResource();
									String archivedSelectionId = archivedSelection.getURI();

									org.allotrope.datashapes.hdf.model.HyperSlab hyperSlab = (org.allotrope.datashapes.hdf.model.HyperSlab) object2rdf.read(recordModel.getGraph(),
											NodeFactory.createURI(archivedSelectionId), org.allotrope.datashapes.hdf.model.HyperSlab.class);
									String archivedSelectionTitle = StringUtils.EMPTY;
									String archivedSelectionLabel = StringUtils.EMPTY;
									String archivedSelectionDescription = StringUtils.EMPTY;
									String archivedSelectionAttribute = StringUtils.EMPTY;
									String archivedSelectionOn = StringUtils.EMPTY;
									String archivedSelectionDataset = StringUtils.EMPTY;
									if (hyperSlab != null)
									{
										archivedSelectionTitle = hyperSlab.title();
										archivedSelectionLabel = hyperSlab.label();
										archivedSelectionDescription = hyperSlab.description();
										archivedSelectionAttribute = (hyperSlab.selectedAttribute() != null ? hyperSlab.selectedAttribute().id().get() : "");
										archivedSelectionOn = (hyperSlab.selectionOn() != null ? hyperSlab.selectionOn().id().get() : "");
										archivedSelectionDataset = (hyperSlab.selectedDataset() != null ? hyperSlab.selectedDataset().id().get() : "");
									}
									log.info("                     --> OLD archived to: \"" + archivedSelectionId + "\" title: \"" + archivedSelectionTitle + "\" label: \"" + archivedSelectionLabel
											+ "\" description: \"" + archivedSelectionDescription + "\" attribute: \"" + archivedSelectionAttribute + "\" selection on: \"" + archivedSelectionOn
											+ "\" selected dataset: \"" + archivedSelectionDataset + "\"");
									extractedArchivedSelection.setId(archivedSelectionId);
									extractedArchivedSelection.setTitle(archivedSelectionTitle);
									extractedArchivedSelection.setLabel(archivedSelectionLabel);
									extractedArchivedSelection.setDescription(archivedSelectionDescription);

									if (!archivedSelectionAttribute.isEmpty())
									{
										String archivedSelectionAttributeId = hyperSlab.selectedAttribute().id().get();
										String archivedSelectionAttributeTitle = hyperSlab.selectedAttribute().title();
										String archivedSelectionAttributeLabel = hyperSlab.selectedAttribute().label();
										String archivedSelectionAttributeName = hyperSlab.selectedAttribute().name();
										log.info("                 --> OLD attribute id: \"" + archivedSelectionAttributeId + "\" title: \"" + archivedSelectionAttributeTitle + "\" label: \""
												+ archivedSelectionAttributeLabel + "\" name: \"" + archivedSelectionAttributeName + "\"");
										Attribute extractedAttribute = new Attribute();
										extractedAttribute.setId(archivedSelectionAttributeId);
										extractedAttribute.setTitle(archivedSelectionAttributeTitle);
										extractedAttribute.setLabel(archivedSelectionAttributeLabel);
										extractedAttribute.setName(archivedSelectionAttributeName);
										extractedArchivedSelection.setAttribute(extractedAttribute);
									}

									if (!archivedSelectionOn.isEmpty())
									{
										Dataspace archivedSelectionDataspace = hyperSlab.selectionOn();
										String archivedSelectionDataspaceId = archivedSelectionDataspace.id().get();
										String archivedSelectionDataspaceTitle = archivedSelectionDataspace.title();
										String archivedSelectionDataspaceLabel = archivedSelectionDataspace.label();
										String archivedSelectionDataspaceDescription = archivedSelectionDataspace.description();
										String archivedSelectionDataspaceRank = String.valueOf(archivedSelectionDataspace.rank());
										log.info("                 --> OLD dataspace id: \"" + archivedSelectionDataspaceId + "\" title: \"" + archivedSelectionDataspaceTitle + "\" label: \""
												+ archivedSelectionDataspaceLabel + "\" description: \"" + archivedSelectionDataspaceDescription + "\" rank: \"" + archivedSelectionDataspaceRank
												+ "\"");
										processor.audit.Dataspace archivedExtractedDataspace = new processor.audit.Dataspace();
										archivedExtractedDataspace.setId(archivedSelectionDataspaceId);
										archivedExtractedDataspace.setTitle(archivedSelectionDataspaceTitle);
										archivedExtractedDataspace.setLabel(archivedSelectionDataspaceLabel);
										archivedExtractedDataspace.setDescription(archivedSelectionDataspaceDescription);
										archivedExtractedDataspace.setRank(archivedSelectionDataspaceRank);

										List<Dimension> archivedExtractedDimensions = new ArrayList<Dimension>();
										Set<DataspaceDimension> selectionDimensions = selectionDataspace.dimensions();
										if (selectionDimensions != null && !selectionDimensions.isEmpty())
										{
											for (Iterator<DataspaceDimension> dimensionIterator = selectionDimensions.iterator(); dimensionIterator.hasNext();)
											{
												DataspaceDimension dataspaceDimension = dimensionIterator.next();
												String dataspaceDimensionId = dataspaceDimension.id().get();
												String dataspaceDimensionTitle = dataspaceDimension.title();
												String dataspaceDimensionLabel = dataspaceDimension.label();
												String dataspaceDimensionDescription = dataspaceDimension.description();
												String dataspaceDimensionIndex = String.valueOf(dataspaceDimension.index());
												String dataspaceDimensionSize = String.valueOf(dataspaceDimension.size());
												String dataspaceDimensionCurrentSize = String.valueOf(dataspaceDimension.currentSize());
												String dataspaceDimensionInitialSize = String.valueOf(dataspaceDimension.initialSize());
												String dataspaceDimensionMaxSize = String.valueOf(dataspaceDimension.maximumSize());
												log.info("                     --> OLD dimension id: \"" + dataspaceDimensionId + "\" title: \"" + dataspaceDimensionTitle + "\" label: \""
														+ dataspaceDimensionLabel + "\" description: \"" + dataspaceDimensionDescription + "\" index: \"" + dataspaceDimensionIndex + "\" size: \""
														+ dataspaceDimensionSize + "\" current size: \"" + dataspaceDimensionCurrentSize + "\" initial size: \"" + dataspaceDimensionInitialSize
														+ "\" maximum size: \"" + dataspaceDimensionMaxSize + "\"");
												Dimension extractedDimension = new Dimension();
												extractedDimension.setId(dataspaceDimensionId);
												extractedDimension.setTitle(dataspaceDimensionTitle);
												extractedDimension.setLabel(dataspaceDimensionLabel);
												extractedDimension.setDescription(dataspaceDimensionDescription);
												extractedDimension.setIndex(dataspaceDimensionIndex);
												extractedDimension.setSize(dataspaceDimensionSize);
												extractedDimension.setCurrentSize(dataspaceDimensionCurrentSize);
												extractedDimension.setInitialSize(dataspaceDimensionInitialSize);
												extractedDimension.setMaxSize(dataspaceDimensionMaxSize);
												archivedExtractedDimensions.add(extractedDimension);
											}
											archivedExtractedDataspace.setDimensions(archivedExtractedDimensions);
										}
										extractedArchivedSelection.setSelectionOn(archivedExtractedDataspace);
									}

									if (!archivedSelectionDataset.isEmpty())
									{
										String archivedSelectionDatasetId = hyperSlab.selectedDataset().id().get();
										String archivedSelectionDatasetLabel = hyperSlab.selectedDataset().label();
										String archivedSelectionDatasetTitle = hyperSlab.selectedDataset().title();
										String archivedSelectionDatasetName = hyperSlab.selectedDataset().name();
										String archivedSelectionDatasetDescription = hyperSlab.selectedDataset().description();
										StorageAllocation archivedSelectionDatasetAllocTime = hyperSlab.selectedDataset().allocationTime();
										String archivedSelectionDatasetAllocTimeString = (archivedSelectionDatasetAllocTime != null ? hyperSlab.selectedDataset().name() : "");
										FillTime archivedSelectionDatasetFillTime = hyperSlab.selectedDataset().fillTime();
										String archivedSelectionDatasetFillTimeString = (archivedSelectionDatasetFillTime != null ? archivedSelectionDatasetFillTime.name() : "");
										FillValueStatus archivedSelectionDatasetFillState = hyperSlab.selectedDataset().fillValueStatus();
										String archivedSelectionDatasetFillStateString = (archivedSelectionDatasetFillState != null ? archivedSelectionDatasetFillState.name() : "");
										ScaleType archivedSelectionDatasetScale = hyperSlab.selectedDataset().scaleType();
										String archivedSelectionDatasetScaleTypeString = (archivedSelectionDatasetScale != null ? archivedSelectionDatasetScale.name() : "");
										log.info("                 --> OLD dataset id: \"" + archivedSelectionDatasetId + "\" title: \"" + archivedSelectionDatasetTitle + "\" label: \""
												+ archivedSelectionDatasetLabel + "\" name: \"" + archivedSelectionDatasetName + "\" description: \"" + archivedSelectionDatasetDescription
												+ "\" alloc time: \"" + archivedSelectionDatasetAllocTimeString + "\" fill time: \"" + archivedSelectionDatasetFillTimeString + "\" fill state: \""
												+ archivedSelectionDatasetFillStateString + "\" scale type: \"" + archivedSelectionDatasetScaleTypeString + "\"");
										processor.audit.Dataset extractedDataset = new processor.audit.Dataset();
										extractedDataset.setId(archivedSelectionDatasetId);
										extractedDataset.setTitle(archivedSelectionDatasetTitle);
										extractedDataset.setLabel(archivedSelectionDatasetLabel);
										extractedDataset.setDescription(archivedSelectionDatasetDescription);
										extractedDataset.setAllocTime(archivedSelectionDatasetAllocTimeString);
										extractedDataset.setFillTime(archivedSelectionDatasetFillTimeString);
										extractedDataset.setFillState(archivedSelectionDatasetFillStateString);
										extractedDataset.setScaleType(archivedSelectionDatasetScaleTypeString);
										extractedArchivedSelection.setDataset(extractedDataset);
									}

									if (hyperSlab != null)
									{
										List<processor.audit.DimensionSelection> extractedDimensionSelections = new ArrayList<processor.audit.DimensionSelection>();
										Set<DimensionSelection> archivedSelectionDimensionSelections = hyperSlab.dimensionSelections();
										if (archivedSelectionDimensionSelections != null)
										{
											for (Iterator<DimensionSelection> dimensionIterator = archivedSelectionDimensionSelections.iterator(); dimensionIterator.hasNext();)
											{
												DimensionSelection dimensionSelection = dimensionIterator.next();
												String archivedSelectionDimensionSelectionId = dimensionSelection.id().get();
												String archivedSelectionDimensionSelectionTitle = dimensionSelection.title();
												String archivedSelectionDimensionSelectionLabel = dimensionSelection.label();
												String archivedSelectionDimensionSelectionDescription = dimensionSelection.description();
												String archivedSelectionDimensionSelectionIndex = String.valueOf(dimensionSelection.index());
												String archivedSelectionDimensionSelectionStart = String.valueOf(dimensionSelection.start());
												String archivedSelectionDimensionSelectionCount = String.valueOf(dimensionSelection.count());
												String archivedSelectionDimensionSelectionBlock = String.valueOf(dimensionSelection.block());
												String archivedSelectionDimensionSelectionStride = String.valueOf(dimensionSelection.stride());
												log.info("                         --> OLD archived dimension selection: \"" + archivedSelectionDimensionSelectionId + "\" title: \""
														+ archivedSelectionDimensionSelectionTitle + "\" label: \"" + archivedSelectionDimensionSelectionLabel + "\" description: \""
														+ archivedSelectionDimensionSelectionDescription + "\" index: \"" + archivedSelectionDimensionSelectionIndex + "\" start: \""
														+ archivedSelectionDimensionSelectionStart + "\" count: \"" + archivedSelectionDimensionSelectionCount + "\" block: \""
														+ archivedSelectionDimensionSelectionBlock + "\" stride: \"" + archivedSelectionDimensionSelectionStride + "\"");
												processor.audit.DimensionSelection extractedDimensionSelection = new processor.audit.DimensionSelection();
												extractedDimensionSelection.setId(archivedSelectionDimensionSelectionId);
												extractedDimensionSelection.setTitle(archivedSelectionDimensionSelectionTitle);
												extractedDimensionSelection.setLabel(archivedSelectionDimensionSelectionLabel);
												extractedDimensionSelection.setDescription(archivedSelectionDimensionSelectionDescription);
												extractedDimensionSelection.setIndex(archivedSelectionDimensionSelectionIndex);
												extractedDimensionSelection.setStart(archivedSelectionDimensionSelectionStart);
												extractedDimensionSelection.setCount(archivedSelectionDimensionSelectionCount);
												extractedDimensionSelection.setBlock(archivedSelectionDimensionSelectionBlock);
												extractedDimensionSelection.setStride(archivedSelectionDimensionSelectionStride);
												extractedDimensionSelections.add(extractedDimensionSelection);
											}
											extractedArchivedSelection.setDimensionSelections(extractedDimensionSelections);
										}
									}
									extractedSelection.setArchivedTo(extractedArchivedSelection);
								}
								extractedHdfUpdate.setOldSelection(extractedSelection);
							}

							if (targetReference != null)
							{
								if (targetReference instanceof org.allotrope.datashapes.hdf.model.Dataset)
								{
									org.allotrope.datashapes.hdf.model.Dataset dataset = (org.allotrope.datashapes.hdf.model.Dataset) targetReference;
									String datasetId = dataset.id().get();
									String datasetTitle = dataset.title();
									String datasetLabel = dataset.label();
									String datasetName = dataset.name();
									String datasetDescription = dataset.description();
									String datasetTime = (dataset.allocationTime() != null ? dataset.allocationTime().name() : "");
									String datasetAtomicFillValue = (dataset.atomicFillValue() != null ? dataset.atomicFillValue().lexicalValue() : "");
									String datasetCompressionLevel = String.valueOf(dataset.compressionLevel());
									String datasetDatatype = (dataset.datatype() != null ? dataset.datatype().id().get() : "");
									log.info("         --> target id: \"" + datasetId + "\" title: \"" + datasetTitle + "\" label: \"" + datasetLabel + "\" description: \"" + datasetDescription
											+ "\" name: \"" + datasetName + "\" time: \"" + datasetTime + "\" fill value: \"" + datasetAtomicFillValue + "\" compression: \"" + datasetCompressionLevel
											+ "\" datatype: \"" + datasetDatatype + "\"");

									processor.audit.Dataset extractedDataset = new processor.audit.Dataset();
									extractedDataset.setId(datasetId);
									extractedDataset.setTitle(datasetTitle);
									extractedDataset.setLabel(datasetLabel);
									extractedDataset.setDescription(datasetDescription);
									extractedDataset.setAllocTime(datasetTime);
									extractedDataset.setName(datasetName);
									extractedDataset.setCompressionLevel(datasetCompressionLevel);
									extractedDataset.setFillValue(datasetAtomicFillValue);
									extractedDataset.setDataType(datasetDatatype);

									com.hp.hpl.jena.rdf.model.Property selectedDataset = com.hp.hpl.jena.rdf.model.ResourceFactory.createProperty(Vocabulary.HDF_SELECTED_DATASET.getURI());
									com.hp.hpl.jena.rdf.model.Resource targetResource = recordModel.getResource(datasetId);
									if (recordModel.listStatements((com.hp.hpl.jena.rdf.model.Resource) null, selectedDataset, targetResource).hasNext())
									{
										com.hp.hpl.jena.rdf.model.Resource hyperSlabResource = recordModel.listStatements((com.hp.hpl.jena.rdf.model.Resource) null, selectedDataset, targetResource)
												.next().getResource();

										org.allotrope.datashapes.hdf.model.HyperSlab hyperSlab = (org.allotrope.datashapes.hdf.model.HyperSlab) object2rdf.read(recordModel.getGraph(),
												NodeFactory.createURI(hyperSlabResource.getURI()), org.allotrope.datashapes.hdf.model.HyperSlab.class);
										String hyperSlabTitle = StringUtils.EMPTY;
										String hyperSlabLabel = StringUtils.EMPTY;
										String hyperSlabDescription = StringUtils.EMPTY;
										String hyperSlabAttribute = StringUtils.EMPTY;
										String hyperSlabOn = StringUtils.EMPTY;
										String hyperSlabDataset = StringUtils.EMPTY;
										if (hyperSlab != null)
										{
											hyperSlabTitle = hyperSlab.title();
											hyperSlabLabel = hyperSlab.label();
											hyperSlabDescription = hyperSlab.description();
											hyperSlabAttribute = (hyperSlab.selectedAttribute() != null ? hyperSlab.selectedAttribute().id().get() : "");
											hyperSlabOn = (hyperSlab.selectionOn() != null ? hyperSlab.selectionOn().id().get() : "");
											hyperSlabDataset = (hyperSlab.selectedDataset() != null ? hyperSlab.selectedDataset().id().get() : "");
										}
										log.info("             --> hyperslab id: \"" + hyperSlabResource.getURI() + "\" title: \"" + hyperSlabTitle + "\" label: \"" + hyperSlabLabel
												+ "\" description: \"" + hyperSlabDescription + "\" attribute: \"" + hyperSlabAttribute + "\" selection on: \"" + hyperSlabOn
												+ "\" selected dataset: \"" + hyperSlabDataset + "\"");

										processor.audit.DataSelection extractedDataSelection = new processor.audit.DataSelection();
										extractedDataSelection.setId(hyperSlabResource.getURI());
										extractedDataSelection.setTitle(hyperSlabTitle);
										extractedDataSelection.setLabel(hyperSlabLabel);
										extractedDataSelection.setDescription(hyperSlabDescription);

										if (!hyperSlabAttribute.isEmpty())
										{
											String hyperSlabAttributeId = hyperSlab.selectedAttribute().id().get();
											String hyperSlabAttributeTitle = hyperSlab.selectedAttribute().title();
											String hyperSlabAttributeLabel = hyperSlab.selectedAttribute().label();
											String hyperSlabAttributeName = hyperSlab.selectedAttribute().name();
											log.info("                 --> attribute id: \"" + hyperSlabAttributeId + "\" title: \"" + hyperSlabAttributeTitle + "\" label: \""
													+ hyperSlabAttributeLabel + "\" name: \"" + hyperSlabAttributeName + "\"");
											Attribute extractedAttribute = new Attribute();
											extractedAttribute.setId(hyperSlabAttributeId);
											extractedAttribute.setTitle(hyperSlabAttributeTitle);
											extractedAttribute.setLabel(hyperSlabAttributeLabel);
											extractedAttribute.setName(hyperSlabAttributeName);
											extractedDataSelection.setAttribute(extractedAttribute);
										}

										if (!hyperSlabOn.isEmpty())
										{
											Dataspace selectionDataspace = hyperSlab.selectionOn();
											String selectionDataspaceId = selectionDataspace.id().get();
											String selectionDataspaceTitle = selectionDataspace.title();
											String selectionDataspaceLabel = selectionDataspace.label();
											String selectionDataspaceDescription = selectionDataspace.description();
											String selectionDataspaceRank = String.valueOf(selectionDataspace.rank());
											log.info("                 --> dataspace id: \"" + selectionDataspaceId + "\" title: \"" + selectionDataspaceTitle + "\" label: \""
													+ selectionDataspaceLabel + "\" description: \"" + selectionDataspaceDescription + "\" rank: \"" + selectionDataspaceRank + "\"");
											processor.audit.Dataspace extractedDataspace = new processor.audit.Dataspace();
											extractedDataspace.setId(selectionDataspaceId);
											extractedDataspace.setTitle(selectionDataspaceTitle);
											extractedDataspace.setLabel(selectionDataspaceLabel);
											extractedDataspace.setDescription(selectionDataspaceDescription);
											extractedDataspace.setRank(selectionDataspaceRank);

											List<Dimension> extractedDimensions = new ArrayList<Dimension>();
											Set<DataspaceDimension> selectionDimensions = selectionDataspace.dimensions();
											if (selectionDimensions != null && !selectionDimensions.isEmpty())
											{
												for (Iterator<DataspaceDimension> dimensionIterator = selectionDimensions.iterator(); dimensionIterator.hasNext();)
												{
													DataspaceDimension dataspaceDimension = dimensionIterator.next();
													String dataspaceDimensionId = dataspaceDimension.id().get();
													String dataspaceDimensionTitle = dataspaceDimension.title();
													String dataspaceDimensionLabel = dataspaceDimension.label();
													String dataspaceDimensionDescription = dataspaceDimension.description();
													String dataspaceDimensionIndex = String.valueOf(dataspaceDimension.index());
													String dataspaceDimensionSize = String.valueOf(dataspaceDimension.size());
													String dataspaceDimensionCurrentSize = String.valueOf(dataspaceDimension.currentSize());
													String dataspaceDimensionInitialSize = String.valueOf(dataspaceDimension.initialSize());
													String dataspaceDimensionMaxSize = String.valueOf(dataspaceDimension.maximumSize());
													log.info("                     --> dimension id: \"" + dataspaceDimensionId + "\" title: \"" + dataspaceDimensionTitle + "\" label: \""
															+ dataspaceDimensionLabel + "\" description: \"" + dataspaceDimensionDescription + "\" index: \"" + dataspaceDimensionIndex + "\" size: \""
															+ dataspaceDimensionSize + "\" current size: \"" + dataspaceDimensionCurrentSize + "\" initial size: \"" + dataspaceDimensionInitialSize
															+ "\" maximum size: \"" + dataspaceDimensionMaxSize + "\"");
													Dimension extractedDimension = new Dimension();
													extractedDimension.setId(dataspaceDimensionId);
													extractedDimension.setTitle(dataspaceDimensionTitle);
													extractedDimension.setLabel(dataspaceDimensionLabel);
													extractedDimension.setDescription(dataspaceDimensionDescription);
													extractedDimension.setIndex(dataspaceDimensionIndex);
													extractedDimension.setSize(dataspaceDimensionSize);
													extractedDimension.setCurrentSize(dataspaceDimensionCurrentSize);
													extractedDimension.setInitialSize(dataspaceDimensionInitialSize);
													extractedDimension.setMaxSize(dataspaceDimensionMaxSize);
													extractedDimensions.add(extractedDimension);
												}
												extractedDataspace.setDimensions(extractedDimensions);
											}
											extractedDataSelection.setSelectionOn(extractedDataspace);
										}

										if (hyperSlab != null)
										{
											List<processor.audit.DimensionSelection> extractedDimensionSelections = new ArrayList<processor.audit.DimensionSelection>();
											Set<DimensionSelection> hyperSlabDimensionSelections = hyperSlab.dimensionSelections();
											if (hyperSlabDimensionSelections != null)
											{
												for (Iterator<DimensionSelection> dimensionIterator = hyperSlabDimensionSelections.iterator(); dimensionIterator.hasNext();)
												{
													DimensionSelection dimensionSelection = dimensionIterator.next();
													String hyperSlabDimensionSelectionId = dimensionSelection.id().get();
													String hyperSlabDimensionSelectionTitle = dimensionSelection.title();
													String hyperSlabDimensionSelectionLabel = dimensionSelection.label();
													String hyperSlabDimensionSelectionDescription = dimensionSelection.description();
													String hyperSlabDimensionSelectionIndex = String.valueOf(dimensionSelection.index());
													String hyperSlabDimensionSelectionStart = String.valueOf(dimensionSelection.start());
													String hyperSlabDimensionSelectionCount = String.valueOf(dimensionSelection.count());
													String hyperSlabDimensionSelectionBlock = String.valueOf(dimensionSelection.block());
													String hyperSlabDimensionSelectionStride = String.valueOf(dimensionSelection.stride());
													log.info("                 --> dimension selection: \"" + hyperSlabDimensionSelectionId + "\" title: \"" + hyperSlabDimensionSelectionTitle
															+ "\" label: \"" + hyperSlabDimensionSelectionLabel + "\" description: \"" + hyperSlabDimensionSelectionDescription + "\" index: \""
															+ hyperSlabDimensionSelectionIndex + "\" start: \"" + hyperSlabDimensionSelectionStart + "\" count: \"" + hyperSlabDimensionSelectionCount
															+ "\" block: \"" + hyperSlabDimensionSelectionBlock + "\" stride: \"" + hyperSlabDimensionSelectionStride + "\"");
													processor.audit.DimensionSelection extractedDimensionSelection = new processor.audit.DimensionSelection();
													extractedDimensionSelection.setId(hyperSlabDimensionSelectionId);
													extractedDimensionSelection.setTitle(hyperSlabDimensionSelectionTitle);
													extractedDimensionSelection.setLabel(hyperSlabDimensionSelectionLabel);
													extractedDimensionSelection.setDescription(hyperSlabDimensionSelectionDescription);
													extractedDimensionSelection.setIndex(hyperSlabDimensionSelectionIndex);
													extractedDimensionSelection.setStart(hyperSlabDimensionSelectionStart);
													extractedDimensionSelection.setCount(hyperSlabDimensionSelectionCount);
													extractedDimensionSelection.setBlock(hyperSlabDimensionSelectionBlock);
													extractedDimensionSelection.setStride(hyperSlabDimensionSelectionStride);
													extractedDimensionSelections.add(extractedDimensionSelection);
												}
												extractedDataSelection.setDimensionSelections(extractedDimensionSelections);
											}
										}
										extractedDataset.setInverseSelectedDataset(extractedDataSelection);
									}
									extractedHdfUpdate.setTargetDataset(extractedDataset);
								}
							}
							hdfUpdates.add(extractedHdfUpdate);
						}
						extractedHdfChanges.setUpdates(hdfUpdates);
						extractedAuditRecord.setHdfChanges(extractedHdfChanges);

						extractedAuditRecords.add(extractedAuditRecord);
					}
					extractedAudittrail.setAuditRecords(extractedAuditRecords);
					extractedAudittrails.add(extractedAudittrail);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return extractedAudittrails;
	}

	private static processor.audit.Statement determineExtractedStatement(Statement statement, org.apache.jena.rdf.model.Model modelWithAdf, org.apache.jena.rdf.model.Model externalResources,
			Logger log, boolean isOld, String graphUri)
	{
		com.hp.hpl.jena.rdf.model.Resource subject = statement.getSubject();

		String predicateString = statement.getPredicate().asResource().getURI();
		org.apache.jena.rdf.model.Resource predicateResource = modelWithAdf.getResource(predicateString);
		predicateString = Rdf2Cyto.determineLabel(modelWithAdf, externalResources, predicateResource);

		RDFNode object = statement.getObject();

		String subjectString = StringUtils.EMPTY;
		if (subject.isURIResource())
		{
			subjectString = subject.getURI();
			org.apache.jena.rdf.model.Resource subjectResource = modelWithAdf.getResource(subjectString);
			subjectString = Rdf2Cyto.determineLabel(modelWithAdf, externalResources, subjectResource);
			if (!subjectString.contains(subject.getLocalName()))
			{
				subjectString = subjectString + " <" + RdfUtil.addPrefix(subject.getLocalName(), subjectResource) + ">";
			}
		}
		else
		{
			subjectString = subject.asNode().getBlankNodeId().getLabelString();
		}

		String objectString = StringUtils.EMPTY;
		if (object.isLiteral())
		{
			objectString = object.asLiteral().getLexicalForm();
		}
		else if (object.isURIResource())
		{
			objectString = object.asResource().getURI();
			org.apache.jena.rdf.model.Resource objectResource = modelWithAdf.getResource(objectString);
			objectString = Rdf2Cyto.determineLabel(modelWithAdf, externalResources, objectResource);
			if (!objectString.contains(object.asResource().getLocalName()))
			{
				objectString = objectString + " <" + RdfUtil.addPrefix(object.asResource().getLocalName(), objectResource) + ">";
			}
		}
		else
		{
			objectString = object.asNode().getBlankNodeId().getLabelString();
		}

		log.info("             --> " + (isOld ? "OLD" : "NEW") + " graph: \"" + graphUri + "\" subject: \"" + subjectString + "\" predicate: \"" + predicateString + "\" object: \"" + objectString
				+ "\"");
		processor.audit.Statement extractedStatement = new processor.audit.Statement();
		extractedStatement.setSubject(subjectString);
		extractedStatement.setPredicate(predicateString);
		extractedStatement.setObject(objectString);
		return extractedStatement;
	}
}
