package processor.audit;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public abstract class AbstractAuditEntity
{
	public String id;
	public String title;
	public String label;
	public String description;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getLabel()
	{
		return label;
	}

	public void setLabel(String label)
	{
		this.label = label;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}
}
