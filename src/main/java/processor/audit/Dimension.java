package processor.audit;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class Dimension extends Component
{
	public String index;
	public String size;
	public String currentSize;
	public String initialSize;
	public String maxSize;

	public String getIndex()
	{
		return index;
	}

	public void setIndex(String index)
	{
		this.index = index;
	}

	public String getSize()
	{
		return size;
	}

	public void setSize(String size)
	{
		this.size = size;
	}

	public String getCurrentSize()
	{
		return currentSize;
	}

	public void setCurrentSize(String currentSize)
	{
		this.currentSize = currentSize;
	}

	public String getInitialSize()
	{
		return initialSize;
	}

	public void setInitialSize(String initialSize)
	{
		this.initialSize = initialSize;
	}

	public String getMaxSize()
	{
		return maxSize;
	}

	public void setMaxSize(String maxSize)
	{
		this.maxSize = maxSize;
	}
}
