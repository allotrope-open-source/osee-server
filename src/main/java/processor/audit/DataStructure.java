package processor.audit;

import java.util.List;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class DataStructure extends AbstractAuditEntity
{
	List<Attribute> attributes;
	List<processor.audit.Component> components;
	List<processor.audit.Dimension> dimensions;
	List<processor.audit.Measure> measures;

	public List<Attribute> getAttributes()
	{
		return attributes;
	}

	public void setAttributes(List<Attribute> attributes)
	{
		this.attributes = attributes;
	}

	public List<processor.audit.Component> getComponents()
	{
		return components;
	}

	public void setComponents(List<processor.audit.Component> components)
	{
		this.components = components;
	}

	public List<processor.audit.Dimension> getDimensions()
	{
		return dimensions;
	}

	public void setDimensions(List<processor.audit.Dimension> dimensions)
	{
		this.dimensions = dimensions;
	}

	public List<processor.audit.Measure> getMeasures()
	{
		return measures;
	}

	public void setMeasures(List<processor.audit.Measure> measures)
	{
		this.measures = measures;
	}
}
