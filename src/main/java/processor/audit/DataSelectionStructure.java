package processor.audit;

import java.util.List;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class DataSelectionStructure extends AbstractAuditEntity
{
	public List<ComponentSelection> selectedComponents;

	public List<ComponentSelection> getSelectedComponents()
	{
		return selectedComponents;
	}

	public void setSelectedComponents(List<ComponentSelection> selectedComponents)
	{
		this.selectedComponents = selectedComponents;
	}
}
