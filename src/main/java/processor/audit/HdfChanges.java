package processor.audit;

import java.util.List;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class HdfChanges extends AbstractAuditEntity
{
	public List<HdfChange> additions;
	public List<HdfChange> removals;
	public List<Update> updates;
	String subjectOfChange;
	String motivatedBy;

	public List<HdfChange> getAdditions()
	{
		return additions;
	}

	public void setAdditions(List<HdfChange> additions)
	{
		this.additions = additions;
	}

	public List<HdfChange> getRemovals()
	{
		return removals;
	}

	public void setRemovals(List<HdfChange> removals)
	{
		this.removals = removals;
	}

	public List<Update> getUpdates()
	{
		return updates;
	}

	public void setUpdates(List<Update> updates)
	{
		this.updates = updates;
	}

	public String getSubjectOfChange()
	{
		return subjectOfChange;
	}

	public void setSubjectOfChange(String subjectOfChange)
	{
		this.subjectOfChange = subjectOfChange;
	}

	public String getMotivatedBy()
	{
		return motivatedBy;
	}

	public void setMotivatedBy(String motivatedBy)
	{
		this.motivatedBy = motivatedBy;
	}
}
