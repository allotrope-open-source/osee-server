package processor.audit;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class Activity extends AbstractAuditEntity
{
	public String startDate;
	public String endDate;
	public String tool;

	public String getStartDate()
	{
		return startDate;
	}

	public void setStartDate(String startDate)
	{
		this.startDate = startDate;
	}

	public String getEndDate()
	{
		return endDate;
	}

	public void setEndDate(String endDate)
	{
		this.endDate = endDate;
	}

	public String getTool()
	{
		return tool;
	}

	public void setTool(String tool)
	{
		this.tool = tool;
	}
}
