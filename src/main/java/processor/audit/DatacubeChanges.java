package processor.audit;

import java.util.List;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class DatacubeChanges extends AbstractAuditEntity
{
	public List<DatacubeChange> additions;
	public List<DatacubeChange> removals;
	public List<Update> updates;
	String subjectOfChange;
	String motivatedBy;

	public List<DatacubeChange> getAdditions()
	{
		return additions;
	}

	public void setAdditions(List<DatacubeChange> dcAdditions)
	{
		additions = dcAdditions;
	}

	public List<DatacubeChange> getRemovals()
	{
		return removals;
	}

	public void setRemovals(List<DatacubeChange> dcRemovals)
	{
		removals = dcRemovals;
	}

	public String getSubjectOfChange()
	{
		return subjectOfChange;
	}

	public void setSubjectOfChange(String subjectOfChange)
	{
		this.subjectOfChange = subjectOfChange;
	}

	public String getMotivatedBy()
	{
		return motivatedBy;
	}

	public void setMotivatedBy(String motivatedBy)
	{
		this.motivatedBy = motivatedBy;
	}

	public List<Update> getUpdates()
	{
		return updates;
	}

	public void setUpdates(List<Update> updates)
	{
		this.updates = updates;
	}
}
