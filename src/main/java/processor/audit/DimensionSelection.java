package processor.audit;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class DimensionSelection extends AbstractAuditEntity
{
	public String start;
	public String index;
	public String count;
	public String block;
	public String stride;

	public String getStart()
	{
		return start;
	}

	public void setStart(String start)
	{
		this.start = start;
	}

	public String getIndex()
	{
		return index;
	}

	public void setIndex(String index)
	{
		this.index = index;
	}

	public String getCount()
	{
		return count;
	}

	public void setCount(String count)
	{
		this.count = count;
	}

	public String getBlock()
	{
		return block;
	}

	public void setBlock(String block)
	{
		this.block = block;
	}

	public String getStride()
	{
		return stride;
	}

	public void setStride(String stride)
	{
		this.stride = stride;
	}
}
