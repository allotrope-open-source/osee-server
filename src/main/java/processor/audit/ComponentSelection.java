package processor.audit;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class ComponentSelection extends AbstractAuditEntity
{
	public Component selectionOn;

	public Component getSelectionOn()
	{
		return selectionOn;
	}

	public void setSelectionOn(Component selectionOn)
	{
		this.selectionOn = selectionOn;
	}
}
