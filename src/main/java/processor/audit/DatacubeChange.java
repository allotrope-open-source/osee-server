package processor.audit;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class DatacubeChange extends AbstractAuditEntity
{
	public String datacubeLabel;
	public Dataset dataSet;

	public String getDatacubeLabel()
	{
		return datacubeLabel;
	}

	public void setDatacubeLabel(String dcLabel)
	{
		datacubeLabel = dcLabel;
	}

	public Dataset getDataSet()
	{
		return dataSet;
	}

	public void setDataSet(Dataset dataSet)
	{
		this.dataSet = dataSet;
	}
}
