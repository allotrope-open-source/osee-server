package processor.audit;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class Dataset extends AbstractAuditEntity
{
	public DataStructure dataStructure;
	public String allocTime;
	public String fillTime;
	public String fillState;
	public String fillValue;
	public String scaleType;
	public String name;
	public String compressionLevel;
	public String dataType;
	public DataSelection inverseSelectedDataset;

	public DataStructure getDataStructure()
	{
		return dataStructure;
	}

	public void setDataStructure(DataStructure dataStructure)
	{
		this.dataStructure = dataStructure;
	}

	public String getAllocTime()
	{
		return allocTime;
	}

	public void setAllocTime(String allocTime)
	{
		this.allocTime = allocTime;
	}

	public String getFillTime()
	{
		return fillTime;
	}

	public void setFillTime(String fillTime)
	{
		this.fillTime = fillTime;
	}

	public String getFillState()
	{
		return fillState;
	}

	public void setFillState(String fillState)
	{
		this.fillState = fillState;
	}

	public String getScaleType()
	{
		return scaleType;
	}

	public void setScaleType(String scaleType)
	{
		this.scaleType = scaleType;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getCompressionLevel()
	{
		return compressionLevel;
	}

	public void setCompressionLevel(String compressionLevel)
	{
		this.compressionLevel = compressionLevel;
	}

	public String getDataType()
	{
		return dataType;
	}

	public void setDataType(String dataType)
	{
		this.dataType = dataType;
	}

	public String getFillValue()
	{
		return fillValue;
	}

	public void setFillValue(String fillValue)
	{
		this.fillValue = fillValue;
	}

	public DataSelection getInverseSelectedDataset()
	{
		return inverseSelectedDataset;
	}

	public void setInverseSelectedDataset(DataSelection inverseSelectedDataset)
	{
		this.inverseSelectedDataset = inverseSelectedDataset;
	}
}
