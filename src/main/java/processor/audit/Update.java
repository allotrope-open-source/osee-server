package processor.audit;

import java.util.List;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class Update extends AbstractAuditEntity
{
	public String oldValue;
	public String newValue;
	public String target;
	public String previous;
	public String oldDataReference;
	public String newDataReference;
	public List<Statement> oldData;
	public List<Statement> newData;
	public List<String> oldDataValues;
	public List<String> newDataValues;
	public DataSelection oldSelection;
	public DataSelection newSelection;
	public Dataset targetDataset;
	public Segment oldSegment;
	public Segment newSegment;

	public String getOldValue()
	{
		return oldValue;
	}

	public void setOldValue(String oldValue)
	{
		this.oldValue = oldValue;
	}

	public String getNewValue()
	{
		return newValue;
	}

	public void setNewValue(String newValue)
	{
		this.newValue = newValue;
	}

	public String getTarget()
	{
		return target;
	}

	public void setTarget(String target)
	{
		this.target = target;
	}

	public List<Statement> getOldData()
	{
		return oldData;
	}

	public void setOldData(List<Statement> oldData)
	{
		this.oldData = oldData;
	}

	public List<Statement> getNewData()
	{
		return newData;
	}

	public void setNewData(List<Statement> newData)
	{
		this.newData = newData;
	}

	public String getOldDataReference()
	{
		return oldDataReference;
	}

	public void setOldDataReference(String oldDataReference)
	{
		this.oldDataReference = oldDataReference;
	}

	public String getNewDataReference()
	{
		return newDataReference;
	}

	public void setNewDataReference(String newDataReference)
	{
		this.newDataReference = newDataReference;
	}

	public String getPrevious()
	{
		return previous;
	}

	public void setPrevious(String previous)
	{
		this.previous = previous;
	}

	public List<String> getOldDataValues()
	{
		return oldDataValues;
	}

	public void setOldDataValues(List<String> oldDataValues)
	{
		this.oldDataValues = oldDataValues;
	}

	public List<String> getNewDataValues()
	{
		return newDataValues;
	}

	public void setNewDataValues(List<String> newDataValues)
	{
		this.newDataValues = newDataValues;
	}

	public DataSelection getOldSelection()
	{
		return oldSelection;
	}

	public void setOldSelection(DataSelection oldSelection)
	{
		this.oldSelection = oldSelection;
	}

	public DataSelection getNewSelection()
	{
		return newSelection;
	}

	public void setNewSelection(DataSelection newSelection)
	{
		this.newSelection = newSelection;
	}

	public Segment getOldSegment()
	{
		return oldSegment;
	}

	public void setOldSegment(Segment oldSegment)
	{
		this.oldSegment = oldSegment;
	}

	public Segment getNewSegment()
	{
		return newSegment;
	}

	public void setNewSegment(Segment newSegment)
	{
		this.newSegment = newSegment;
	}

	public Dataset getTargetDataset()
	{
		return targetDataset;
	}

	public void setTargetDataset(Dataset targetDataset)
	{
		this.targetDataset = targetDataset;
	}
}
