package processor.audit;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class Revision extends AbstractAuditEntity
{
	public Change whatWasChanged;

	public Change getWhatWasChanged()
	{
		return whatWasChanged;
	}

	public void setWhatWasChanged(Change whatWasChanged)
	{
		this.whatWasChanged = whatWasChanged;
	}
}
