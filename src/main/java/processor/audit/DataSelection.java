package processor.audit;

import java.util.List;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class DataSelection extends AbstractAuditEntity
{
	public DataSelectionStructure dataSelectionStructure;
	public Dataset dataset;
	public Attribute attribute;
	public Dataspace selectionOn;
	public List<DimensionSelection> dimensionSelections;
	public DataSelection archivedTo;

	public DataSelectionStructure getDataSelectionStructure()
	{
		return dataSelectionStructure;
	}

	public void setDataSelectionStructure(DataSelectionStructure dataSelectionStructure)
	{
		this.dataSelectionStructure = dataSelectionStructure;
	}

	public Dataset getDataset()
	{
		return dataset;
	}

	public void setDataset(Dataset dataset)
	{
		this.dataset = dataset;
	}

	public Attribute getAttribute()
	{
		return attribute;
	}

	public void setAttribute(Attribute attribute)
	{
		this.attribute = attribute;
	}

	public Dataspace getSelectionOn()
	{
		return selectionOn;
	}

	public void setSelectionOn(Dataspace selectionOn)
	{
		this.selectionOn = selectionOn;
	}

	public List<DimensionSelection> getDimensionSelections()
	{
		return dimensionSelections;
	}

	public void setDimensionSelections(List<DimensionSelection> dimensionSelections)
	{
		this.dimensionSelections = dimensionSelections;
	}

	public DataSelection getArchivedTo()
	{
		return archivedTo;
	}

	public void setArchivedTo(DataSelection archivedTo)
	{
		this.archivedTo = archivedTo;
	}
}
