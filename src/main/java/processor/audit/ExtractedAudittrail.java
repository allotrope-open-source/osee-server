package processor.audit;

import java.util.List;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class ExtractedAudittrail extends AbstractAuditEntity
{
	public String parentId;
	public List<AuditRecord> auditRecords;

	public String getParentId()
	{
		return parentId;
	}

	public void setParentId(String parentId)
	{
		this.parentId = parentId;
	}

	public List<AuditRecord> getAuditRecords()
	{
		return auditRecords;
	}

	public void setAuditRecords(List<AuditRecord> auditRecords)
	{
		this.auditRecords = auditRecords;
	}
}
