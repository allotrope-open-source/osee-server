package processor.audit;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class DatapackageChange extends AbstractAuditEntity
{
	public String path;
	public String creator;
	public String createdOn;
	public String modifiedBy;
	public String modifiedOn;
	public String hash;
	public String representedBy;
	public String parent;
	public String parentId;
	public String size;
	public String format;
	public String charSet;
	public String compressed;
	public String lineSeparator;
	public String preview;
	public String previewLength;
	public String type;

	public String getPath()
	{
		return path;
	}

	public void setPath(String path)
	{
		this.path = path;
	}

	public String getCreator()
	{
		return creator;
	}

	public void setCreator(String creator)
	{
		this.creator = creator;
	}

	public String getCreatedOn()
	{
		return createdOn;
	}

	public void setCreatedOn(String createdOn)
	{
		this.createdOn = createdOn;
	}

	public String getModifiedBy()
	{
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedOn()
	{
		return modifiedOn;
	}

	public void setModifiedOn(String modifiedOn)
	{
		this.modifiedOn = modifiedOn;
	}

	public String getHash()
	{
		return hash;
	}

	public void setHash(String hash)
	{
		this.hash = hash;
	}

	public String getRepresentedBy()
	{
		return representedBy;
	}

	public void setRepresentedBy(String representedBy)
	{
		this.representedBy = representedBy;
	}

	public String getParent()
	{
		return parent;
	}

	public void setParent(String parent)
	{
		this.parent = parent;
	}

	public String getParentId()
	{
		return parentId;
	}

	public void setParentId(String parentId)
	{
		this.parentId = parentId;
	}

	public String getSize()
	{
		return size;
	}

	public void setSize(String size)
	{
		this.size = size;
	}

	public String getFormat()
	{
		return format;
	}

	public void setFormat(String format)
	{
		this.format = format;
	}

	public String getCharSet()
	{
		return charSet;
	}

	public void setCharSet(String charSet)
	{
		this.charSet = charSet;
	}

	public String getCompressed()
	{
		return compressed;
	}

	public void setCompressed(String compressed)
	{
		this.compressed = compressed;
	}

	public String getLineSeparator()
	{
		return lineSeparator;
	}

	public void setLineSeparator(String lineSeparator)
	{
		this.lineSeparator = lineSeparator;
	}

	public String getPreview()
	{
		return preview;
	}

	public void setPreview(String preview)
	{
		this.preview = preview;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public String getPreviewLength()
	{
		return previewLength;
	}

	public void setPreviewLength(String previewLength)
	{
		this.previewLength = previewLength;
	}
}
