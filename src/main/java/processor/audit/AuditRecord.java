package processor.audit;

import java.util.List;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class AuditRecord extends AbstractAuditEntity
{
	public String parentId;
	public String status;
	public Revision revision;
	public DataDescriptionChanges ddChanges;
	public DatacubeChanges dcChanges;
	public Activity activity;
	public List<Attribution> attributions;
	public DatapackageChanges dpChanges;
	public HdfChanges hdfChanges;

	public String getParentId()
	{
		return parentId;
	}

	public void setParentId(String parentId)
	{
		this.parentId = parentId;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public Revision getRevision()
	{
		return revision;
	}

	public void setRevision(Revision revision)
	{
		this.revision = revision;
	}

	public DataDescriptionChanges getDdChanges()
	{
		return ddChanges;
	}

	public void setDdChanges(DataDescriptionChanges ddChanges)
	{
		this.ddChanges = ddChanges;
	}

	public Activity getActivity()
	{
		return activity;
	}

	public void setActivity(Activity activity)
	{
		this.activity = activity;
	}

	public List<Attribution> getAttributions()
	{
		return attributions;
	}

	public void setAttributions(List<Attribution> attributions)
	{
		this.attributions = attributions;
	}

	public DatacubeChanges getDcChanges()
	{
		return dcChanges;
	}

	public void setDcChanges(DatacubeChanges dcChanges)
	{
		this.dcChanges = dcChanges;
	}

	public DatapackageChanges getDpChanges()
	{
		return dpChanges;
	}

	public void setDpChanges(DatapackageChanges dpChanges)
	{
		this.dpChanges = dpChanges;
	}

	public HdfChanges getHdfChanges()
	{
		return hdfChanges;
	}

	public void setHdfChanges(HdfChanges hdfChanges)
	{
		this.hdfChanges = hdfChanges;
	}
}
