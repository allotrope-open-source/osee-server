package processor.audit;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class HdfChange extends AbstractAuditEntity
{
	public String name;
	public String time;
	public String atomicFillValue;
	public String compressionLevel;
	public String dataType;
	public String changeType;
	public String archivedTo;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getTime()
	{
		return time;
	}

	public void setTime(String time)
	{
		this.time = time;
	}

	public String getAtomicFillValue()
	{
		return atomicFillValue;
	}

	public void setAtomicFillValue(String atomicFillValue)
	{
		this.atomicFillValue = atomicFillValue;
	}

	public String getCompressionLevel()
	{
		return compressionLevel;
	}

	public void setCompressionLevel(String compressionLevel)
	{
		this.compressionLevel = compressionLevel;
	}

	public String getDataType()
	{
		return dataType;
	}

	public void setDataType(String dataType)
	{
		this.dataType = dataType;
	}

	public String getChangeType()
	{
		return changeType;
	}

	public void setChangeType(String changeType)
	{
		this.changeType = changeType;
	}

	public String getArchivedTo()
	{
		return archivedTo;
	}

	public void setArchivedTo(String archivedTo)
	{
		this.archivedTo = archivedTo;
	}
}
