package processor.audit;

import java.util.List;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class Dataspace extends AbstractAuditEntity
{
	List<Dimension> dimensions;
	String rank;

	public List<Dimension> getDimensions()
	{
		return dimensions;
	}

	public void setDimensions(List<Dimension> dimensions)
	{
		this.dimensions = dimensions;
	}

	public String getRank()
	{
		return rank;
	}

	public void setRank(String rank)
	{
		this.rank = rank;
	}
}
