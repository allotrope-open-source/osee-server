package processor.audit;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class Attribute extends AbstractAuditEntity
{
	processor.audit.Datatype datatype;
	processor.audit.Dataspace dataspace;
	public String name;

	public void setDatatype(processor.audit.Datatype datatype)
	{
		this.datatype = datatype;
	}

	public processor.audit.Datatype getDatatype()
	{
		return datatype;
	}

	public processor.audit.Dataspace getDataspace()
	{
		return dataspace;
	}

	public void setDataspace(processor.audit.Dataspace dataspace)
	{
		this.dataspace = dataspace;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}
