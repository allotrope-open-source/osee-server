package processor.audit;

import java.util.List;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class DatapackageChanges extends AbstractAuditEntity
{
	public List<DatapackageChange> additions;
	public List<DatapackageChange> removals;
	public List<Update> updates;
	String subjectOfChange;
	String motivatedBy;

	public List<DatapackageChange> getAdditions()
	{
		return additions;
	}

	public void setAdditions(List<DatapackageChange> additions)
	{
		this.additions = additions;
	}

	public List<DatapackageChange> getRemovals()
	{
		return removals;
	}

	public void setRemovals(List<DatapackageChange> removals)
	{
		this.removals = removals;
	}

	public String getSubjectOfChange()
	{
		return subjectOfChange;
	}

	public void setSubjectOfChange(String subjectOfChange)
	{
		this.subjectOfChange = subjectOfChange;
	}

	public String getMotivatedBy()
	{
		return motivatedBy;
	}

	public void setMotivatedBy(String motivatedBy)
	{
		this.motivatedBy = motivatedBy;
	}

	public List<Update> getUpdates()
	{
		return updates;
	}

	public void setUpdates(List<Update> updates)
	{
		this.updates = updates;
	}
}
