package processor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;

import processor.util.AfCytoUtil;
import processor.util.Vocabulary;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class AdfTripleExtractor
{
	private static Set<Statement> extractedStatements = new HashSet<Statement>();
	private static Set<Resource> extractedResources = new HashSet<Resource>();

	public static Set<Statement> extractFrom(Model model)
	{
		extractedStatements.clear();
		extractedResources.clear();

		StmtIterator stmtIterator = model.listStatements();
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			if (AfCytoUtil.isAdfApiTripleToExclude(statement))
			{
				extractedStatements.add(statement);
				extractedResources.add(statement.getSubject());
				if (statement.getObject().isResource())
				{
					extractedResources.add(statement.getResource());
				}
				continue;
			}

			if (statement.getPredicate().equals(Vocabulary.RDF_TYPE))
			{
				Resource typeResource = statement.getResource();
				if (typeResource.isURIResource() && AfCytoUtil.hasNamespaceOfAdfApi(typeResource))
				{
					extractedStatements.add(statement);
					extractedResources.add(statement.getSubject());
				}
			}
		}

		Iterator<Resource> resourceIterator = extractedResources.iterator();
		while (resourceIterator.hasNext())
		{
			Resource resource = resourceIterator.next();
			StmtIterator extractedResourceStmtIterator = model.listStatements(resource, (Property) null, (RDFNode) null);
			while (extractedResourceStmtIterator.hasNext())
			{
				Statement statement = extractedResourceStmtIterator.next();
				extractedStatements.add(statement);
			}
			extractedResourceStmtIterator = model.listStatements((Resource) null, (Property) null, resource);
			while (extractedResourceStmtIterator.hasNext())
			{
				Statement statement = extractedResourceStmtIterator.next();
				extractedStatements.add(statement);
			}
		}

		List<String> statementAsStrings = new ArrayList<String>(extractedStatements.size());
		for (Statement statement : extractedStatements)
		{
			statementAsStrings.add(statement.toString());
		}
		Collections.sort(statementAsStrings);
		for (String statementString : statementAsStrings)
		{
			Processor.log.info("Statement of ADF API: " + statementString);
		}

		return extractedStatements;
	}
}
