package processor.datadescription;

import com.hp.hpl.jena.datatypes.BaseDatatype;
import com.hp.hpl.jena.datatypes.RDFDatatype;

class XSDDateTimeStampType extends BaseDatatype
{
	public static String theTypeURI = "http://www.w3.org/2001/XMLSchema#dateTimeStamp";
	public static RDFDatatype theDateTimeStampType = new XSDDateTimeStampType(theTypeURI);

	public XSDDateTimeStampType(String uri)
	{
		super(uri);
	}

}
