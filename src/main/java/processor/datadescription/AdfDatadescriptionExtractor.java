package processor.datadescription;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.allotrope.adf.model.AdfFile;
import org.allotrope.adf.service.AdfService;
import org.allotrope.adf.service.AdfServiceFactory;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.hp.hpl.jena.datatypes.TypeMapper;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.DatasetFactory;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.sparql.core.DatasetGraph;
import com.osthus.jena.repackaged.riot.RDFDataMgr;
import com.osthus.jena.repackaged.riot.RDFFormat;
import com.osthus.jena.repackaged.riot.system.PrefixMapFactory;

import adfsee.TempFolderCreator;
import processor.Processor;
import processor.util.RdfUtil;
import processor.util.Zipper;
import restcontroller.ProcessController;
import skolemizer.ADFSkolemizer;

@Component
/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class AdfDatadescriptionExtractor
{
	private static final String defaultGraphName = "urn:x-arq:DefaultGraph";

	public static DdEntity extractFrom(Path path, Logger log)
	{
		log.info("Extracting datadescription...");
		String targetFolder = TempFolderCreator.DOWNLOAD_FOLDER_PATH_NAME + "datadescription";

		DdEntity ddWithZip = null;
		DdEntity extractedDatadescription = null;
		try
		{
			Path targetPath = Paths.get(targetFolder);
			if (Files.isRegularFile(targetPath))
			{
				Files.deleteIfExists(targetPath);
			}
			else if (Files.isDirectory(targetPath))
			{
				FileUtils.cleanDirectory(targetPath.toFile());
				Files.delete(targetPath);
			}
			Files.createDirectory(targetPath);
			registerXsdDateTimeStamp();
			AdfService adfService = AdfServiceFactory.create();
			try (AdfFile adfFile = adfService.openFile(path, false))
			{
				extractedDatadescription = extractDataDescription(log, targetFolder, adfFile);
			}
			catch (Exception e)
			{
				Processor.log.info("Error opening ADF in write mode for export of datadescription. " //
						+ "For first skolemized export, ADF files must be opened in write mode " //
						+ "because a graph of skolem IRIs needs to be written to the ADF.");
				e.printStackTrace();
				Processor.log.info("Retrying in read-only mode...");
				try (AdfFile adfFile = adfService.openFile(path, true))
				{
					extractedDatadescription = extractDataDescription(log, targetFolder, adfFile);
				}
				catch (Exception innerException)
				{
					Processor.log.info("Error opening ADF in read-only mode for export of datadescription.");
					innerException.printStackTrace();
				}
			}
			Zipper.compressZipfile(targetFolder, targetFolder + ".zip");
			ddWithZip = createDdWithZip();
			ddWithZip.setChildren(Arrays.asList(extractedDatadescription));
		}
		catch (Exception e)
		{
			Processor.log.info("Error during exporting data description.");
			e.printStackTrace();
		}

		return ddWithZip;
	}

	private static DdEntity extractDataDescription(Logger log, String targetFolder, AdfFile adfFile) throws IOException
	{
		DdEntity extractedDatadescription;
		Dataset dataset = adfFile.getDataset();
		Model model = dataset.getDefaultModel();
		Model modelCopy = ModelFactory.createDefaultModel();
		modelCopy.add(model);
		Dataset datasetCopy = DatasetFactory.create(modelCopy);

		List<DdEntity> children = new ArrayList<DdEntity>();

		// write out default model
		model.setNsPrefixes(RdfUtil.prefixMap);
		Path defaultModelAsTtl = Paths.get(targetFolder + "/default-graph.ttl");
		writeModelToPath(model, defaultModelAsTtl, "TTL");

		Path defaultModelAsN3 = Paths.get(targetFolder + "/default-graph.n3");
		writeModelToPath(model, defaultModelAsN3, "NT");

		Path defaultModelAsTtlSkolemized = Paths.get(targetFolder + "/default-graph-skolemized.ttl");
		writeSkolemizedGraphToPath(defaultGraphName, dataset.asDatasetGraph(), defaultModelAsTtlSkolemized, "TTL", adfFile);

		Path defaultModelAsN3Skolemized = Paths.get(targetFolder + "/default-graph-skolemized.n3");
		writeSkolemizedGraphToPath(defaultGraphName, dataset.asDatasetGraph(), defaultModelAsN3Skolemized, "NT", adfFile);

		// create ddEntity
		DdEntity child = extractGraphMetadata(model, null, defaultModelAsTtl.toFile().getName(), defaultModelAsN3.toFile().getName(), defaultModelAsTtlSkolemized.toFile().getName(),
				defaultModelAsN3Skolemized.toFile().getName());
		children.add(child);

		Iterator<String> listNames = dataset.listNames();
		Long numTriples = 0l;
		while (listNames.hasNext())
		{
			String graphName = listNames.next();
			model = dataset.getNamedModel(graphName);

			// write out separate graphs
			model.setNsPrefixes(RdfUtil.prefixMap);
			Path namedModelPathTtl = Paths.get(targetFolder + "/named-" + ResourceFactory.createResource(graphName).getLocalName() + ".ttl");
			writeModelToPath(model, namedModelPathTtl, "TTL");

			Path namedModelPathAsN3 = Paths.get(targetFolder + "/named-" + ResourceFactory.createResource(graphName).getLocalName() + ".n3");
			writeModelToPath(model, namedModelPathAsN3, "NT");

			Path namedModelPathTtlSkolemized = Paths.get(targetFolder + "/named-" + ResourceFactory.createResource(graphName).getLocalName() + "-skolemized.ttl");
			writeSkolemizedGraphToPath(graphName, dataset.asDatasetGraph(), namedModelPathTtlSkolemized, "TTL", adfFile);

			Path namedModelPathAsN3Skolemized = Paths.get(targetFolder + "/named-" + ResourceFactory.createResource(graphName).getLocalName() + "-skolemized.n3");
			writeSkolemizedGraphToPath(graphName, dataset.asDatasetGraph(), namedModelPathAsN3Skolemized, "NT", adfFile);

			numTriples += model.listStatements().toList().size();
			// create ddEntity
			child = extractGraphMetadata(model, graphName, namedModelPathTtl.toFile().getName(), namedModelPathAsN3.toFile().getName(), namedModelPathTtlSkolemized.toFile().getName(),
					namedModelPathAsN3Skolemized.toFile().getName());
			children.add(child);

			datasetCopy.addNamedModel(graphName, model);
		}

		// write trig
		Path fullDd = Paths.get(targetFolder + "/dataset.trig");
		Files.deleteIfExists(fullDd);

		try (FileOutputStream fos = new FileOutputStream(fullDd.toFile()))
		{
			RDFDataMgr.createDatasetWriter(RDFFormat.TRIG_PRETTY).write(fos, datasetCopy.asDatasetGraph(), PrefixMapFactory.create(RdfUtil.prefixMap), null, null);
		}
		catch (Exception e)
		{
			log.info("Error writing trig file from ADF.");
			e.printStackTrace();
		}

		// create ddEntity
		extractedDatadescription = extractDatasetGraphMetadata(numTriples);
		extractedDatadescription.setChildren(children);
		return extractedDatadescription;
	}

	private static void writeSkolemizedGraphToPath(String graphName, DatasetGraph datasetGraph, Path namedModelPathTtlSkolemized, String rdfFormat, AdfFile adfFile)
	{
		Model skolemized = ADFSkolemizer.skolemizeModelForExport(graphName, datasetGraph, adfFile, rdfFormat);
		writeModelToPath(skolemized, namedModelPathTtlSkolemized, rdfFormat);
	}

	private static void writeModelToPath(Model model, Path path, String rdfFormat)
	{
		try
		{
			Files.deleteIfExists(path);
			try (FileOutputStream fos = new FileOutputStream(path.toFile()))
			{
				Processor.log.info("Exporting to file: " + path.toAbsolutePath().toString());
				model.write(fos, rdfFormat);
			}
			catch (Exception e)
			{
				Processor.log.info("Error writing model as " + rdfFormat + " to path: " + path.toAbsolutePath().toString());
				e.printStackTrace();
			}
		}
		catch (Exception e)
		{
			Processor.log.info("Error deleting existing file for export: " + path.toAbsolutePath().toString());
		}
	}

	private static DdEntity extractDatasetGraphMetadata(Long numTriples)
	{
		DdEntity child = new DdEntity();
		child.setName("dataset");
		child.setLabel("dataset");
		child.setText("dataset");
		child.setColor("#000000");
		child.setUuid("dataset");
		child.setId("dataset");
		child.setNumberOfTriples(String.valueOf(numTriples));
		URI uri;
		try
		{
			uri = new URI(ProcessController.baseFileUrl + "datadescription/dataset.trig");
			child.setUrl(uri.toASCIIString());
		}
		catch (URISyntaxException e)
		{
			e.printStackTrace();
		}
		return child;
	}

	private static DdEntity createDdWithZip()
	{
		DdEntity child = new DdEntity();
		child.setName("complete");
		child.setLabel("zipped");
		child.setText("All graphs in one zip archive.");
		child.setColor("#000000");
		child.setUuid("complete");
		child.setId("complete");
		URI uri;
		try
		{
			uri = new URI(ProcessController.baseFileUrl + "datadescription.zip");
			child.setUrl(uri.toASCIIString());
		}
		catch (URISyntaxException e)
		{
			e.printStackTrace();
		}
		return child;
	}

	private static DdEntity extractGraphMetadata(Model model, String name, String fileNameTtl, String fileNameN3, String fileNameTtlSkolemized, String fileNameN3Skolemized)
	{
		DdEntity child = new DdEntity();
		if (name == null)
		{
			child.setName("urn:x-arq:DefaultGraph");
			child.setLabel("default graph");
			child.setText("urn:x-arq:DefaultGraph");
			child.setUuid("urn:x-arq:DefaultGraph");
			child.setId("urn:x-arq:DefaultGraph");
		}
		else
		{
			child.setName(name);
			child.setLabel(name);
			child.setText(name);
			child.setUuid(name);
			child.setId(name);
		}
		child.setColor("#AAD5FF");
		child.setNumberOfTriples(String.valueOf(model.size()));
		URI uriTtl;
		try
		{
			uriTtl = new URI(ProcessController.baseFileUrl + "datadescription/" + fileNameTtl);
			child.setUrl(uriTtl.toASCIIString());
		}
		catch (URISyntaxException e)
		{
			e.printStackTrace();
		}
		URI uriN3;
		try
		{
			uriN3 = new URI(ProcessController.baseFileUrl + "datadescription/" + fileNameN3);
			child.setUrlN3(uriN3.toASCIIString());
		}
		catch (URISyntaxException e)
		{
			e.printStackTrace();
		}
		URI uriTtlSkolemized;
		try
		{
			uriTtlSkolemized = new URI(ProcessController.baseFileUrl + "datadescription/" + fileNameTtlSkolemized);
			child.setUrlSkolemized(uriTtlSkolemized.toASCIIString());
		}
		catch (URISyntaxException e)
		{
			e.printStackTrace();
		}
		URI uriN3Skolemized;
		try
		{
			uriN3Skolemized = new URI(ProcessController.baseFileUrl + "datadescription/" + fileNameN3Skolemized);
			child.setUrlN3Skolemized(uriN3Skolemized.toASCIIString());
		}
		catch (URISyntaxException e)
		{
			e.printStackTrace();
		}
		return child;
	}

	private static void registerXsdDateTimeStamp()
	{
		TypeMapper mapper = TypeMapper.getInstance();
		if (mapper.getTypeByName(XSDDateTimeStampType.theTypeURI) == null)
		{
			mapper.registerDatatype(XSDDateTimeStampType.theDateTimeStampType);
		}
	}
}
