package processor.datadescription;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class DdEntity
{
	@JsonInclude(Include.NON_NULL)
	public String name;

	@JsonInclude(Include.NON_NULL)
	public String uuid;

	@JsonInclude(Include.NON_NULL)
	public String id;

	@JsonInclude(Include.NON_NULL)
	public String label;

	@JsonInclude(Include.NON_NULL)
	public String text;

	@JsonInclude(Include.NON_NULL)
	public String numberOfTriples;

	@JsonInclude(Include.NON_NULL)
	public String url;

	@JsonInclude(Include.NON_NULL)
	public String urlN3;

	@JsonInclude(Include.NON_NULL)
	public String urlSkolemized;

	@JsonInclude(Include.NON_NULL)
	public String urlN3Skolemized;

	@JsonInclude(Include.NON_NULL)
	public String color;

	@JsonInclude(Include.NON_NULL)
	public List<DdEntity> children;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getUuid()
	{
		return uuid;
	}

	public void setUuid(String uuid)
	{
		this.uuid = uuid;
	}

	public List<DdEntity> getChildren()
	{
		return children;
	}

	public void setChildren(List<DdEntity> children)
	{
		this.children = children;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getLabel()
	{
		return label;
	}

	public void setLabel(String label)
	{
		this.label = label;
	}

	public String getText()
	{
		return text;
	}

	public void setText(String text)
	{
		this.text = text;
	}

	public String getColor()
	{
		return color;
	}

	public void setColor(String color)
	{
		this.color = color;
	}

	public String getUrl()
	{
		return url;
	}

	public String getUrlSkolemized()
	{
		return urlSkolemized;
	}

	public String getUrlN3()
	{
		return urlN3;
	}

	public String getUrlN3Skolemized()
	{
		return urlN3Skolemized;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public void setUrlSkolemized(String urlSkolemized)
	{
		this.urlSkolemized = urlSkolemized;
	}

	public void setUrlN3(String urlN3)
	{
		this.urlN3 = urlN3;
	}

	public void setUrlN3Skolemized(String urlN3Skolemized)
	{
		this.urlN3Skolemized = urlN3Skolemized;
	}

	public String getNumberOfTriples()
	{
		return numberOfTriples;
	}

	public void setNumberOfTriples(String numberOfTriples)
	{
		this.numberOfTriples = numberOfTriples;
	}
}
