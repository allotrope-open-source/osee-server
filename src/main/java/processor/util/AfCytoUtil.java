package processor.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.xml.sax.SAXException;

import processor.Processor;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class AfCytoUtil
{
	/**
	 * Add triples from the files in the given array of filenames to the given model.
	 *
	 * @param additionalFiles
	 * @param model
	 * @throws FileNotFoundException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public static Model addTriples(String[] additionalFiles) throws FileNotFoundException, ParserConfigurationException, SAXException, IOException
	{
		if (additionalFiles != null && additionalFiles.length > 0)
		{
			Model tempModel = ModelFactory.createDefaultModel();

			for (int i = 0; i < additionalFiles.length; i++)
			{
				Path pathToAdditionalFile = Paths.get(additionalFiles[i]);
				Processor.log.info("reading triples from file: " + pathToAdditionalFile.toString());
				Model additionalModel = null;
				if (pathToAdditionalFile.toString().toUpperCase().endsWith("TTL"))
				{
					additionalModel = ModelFactory.createDefaultModel();
					additionalModel.read(new FileInputStream(pathToAdditionalFile.toFile()), null, "TTL");
				}
				else if (pathToAdditionalFile.toString().toUpperCase().endsWith("OWL"))
				{
					additionalModel = ModelFactory.createDefaultModel();
					additionalModel.read(new FileInputStream(pathToAdditionalFile.toFile()), null, "RDF/XML");
				}
				else
				{
					throw new IllegalStateException("unsupported input file: " + pathToAdditionalFile.toString());
				}

				if (additionalModel != null && !additionalModel.isEmpty())
				{
					int numTriplesToAdd = additionalModel.listStatements().toList().size();
					int numTriples = tempModel.listStatements().toList().size();
					tempModel = tempModel.add(additionalModel);
					int numTriplesNew = tempModel.listStatements().toList().size();
					Processor.log.info(numTriplesToAdd + " triples found. " + (numTriplesNew - numTriples) + " triples added.");
				}
				else
				{
					Processor.log.info("no triples added from file: " + pathToAdditionalFile.toString());
				}
			}

			return tempModel;
		}

		return null;
	}

	public static boolean isAdfApiTripleToExclude(Statement statement)
	{
		if (!Processor.excludeAdf)
		{
			return false;
		}

		if (statement.getSubject().isURIResource() && hasNamespaceOfAdfApi(statement.getSubject()))
		{
			return true;
		}

		if (hasNamespaceOfAdfApi(statement.getPredicate().asResource()))
		{
			return true;
		}

		if (statement.getObject().isURIResource() && hasNamespaceOfAdfApi(statement.getResource()))
		{
			return true;
		}

		return false;
	}

	public static boolean hasNamespaceOfAdfApi(Resource resource)
	{

		if (resource.getURI().startsWith(Vocabulary.ADF_DC_HDF_PREFIX))
		{
			return true;
		}

		if (resource.getURI().startsWith(Vocabulary.ADF_DC_PREFIX))
		{
			return true;
		}

		if (resource.getURI().startsWith(Vocabulary.ADF_DP_PREFIX))
		{
			return true;
		}

		if (resource.getURI().startsWith(Vocabulary.HDF_PREFIX))
		{
			return true;
		}

		if (resource.getURI().startsWith(Vocabulary.QB_PREFIX))
		{
			return true;
		}

		return false;
	}
}
