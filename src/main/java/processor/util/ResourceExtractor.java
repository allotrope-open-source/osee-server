package processor.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class ResourceExtractor {
	public static Logger logger = LoggerFactory.getLogger("log");

	public static void extract(String resourceFolder, String destinationFolder) {
		try {
			ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
			Resource[] resources = resolver
					.getResources(ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX + resourceFolder + "/**");
			URI inJarUri = new DefaultResourceLoader().getResource("classpath:" + resourceFolder).getURI();
			logger.info("Resource URI: " + inJarUri);
			for (Resource resource : resources) {
				String relativePath = resource.getURI().getRawSchemeSpecificPart()
						.replace(inJarUri.getRawSchemeSpecificPart(), "");
				if (relativePath.isEmpty()) {
					continue;
				}

				if (inJarUri.toString().toLowerCase().startsWith("jar")) {
					// packaged JAR
					Map<String, String> emptyPropertiesMap = new HashMap<>();
					String[] pieces = inJarUri.toString().split("!");
					try (FileSystem fs = FileSystems.newFileSystem(URI.create(pieces[0]), emptyPropertiesMap)) {
						Path inJarUriPath = fs.getPath(pieces[1]);
						logger.info("Extracting resource: " + inJarUriPath.toAbsolutePath().toString() + relativePath + " from jar with internal path: " + fs.getPath(relativePath).toAbsolutePath().toString());
						if (relativePath.endsWith(File.separator) || relativePath.endsWith("/")) {
							logger.info("Creating directory: " + destinationFolder + relativePath);
							File dirFile = new File(destinationFolder + relativePath);
							if (!dirFile.exists()) {
								dirFile.mkdir();
							}
						} else {
							copyResourceToFilePath(resource, destinationFolder + relativePath);
						}
					} catch (Exception e) {
						logger.error("Error extracting resources from jar with URI: " + inJarUri.toString());
						e.printStackTrace();
					}
				} else {
					// IDE
					String inJarUriPathString = Paths.get(inJarUri).toAbsolutePath().toString();
					logger.info(inJarUriPathString);
					Path inJarUriPath = Paths.get(inJarUriPathString, relativePath);
					logger.info("Extracting resource: " + inJarUriPath.toAbsolutePath().toString());
					if (relativePath.endsWith(File.separator) || Files.isDirectory(inJarUriPath)) {
						File dirFile = new File(destinationFolder + relativePath);
						if (!dirFile.exists()) {
							dirFile.mkdir();
						}
					} else {
						copyResourceToFilePath(resource, destinationFolder + relativePath);
					}
				}
			}
		} catch (IOException e) {
			logger.error("Extraction of resource failed: ", resourceFolder);
			e.printStackTrace();
		}
	}

	public static void extractSingleResource(String resourceName, String destinationFolder) {
		try {
			Resource resource = new DefaultResourceLoader().getResource("classpath:" + resourceName);
			copySingleResourceToFilePath(resource, destinationFolder);
		} catch (IOException e) {
			logger.debug("Extraction of resource failed: ", e);
		}
	}

	private static void copySingleResourceToFilePath(Resource resource, String filePath) throws IOException {
		InputStream resourceInputStream = resource.getInputStream();
		File file = new File(filePath + "/" + resource.getFilename());
		if (!file.exists()) {
			FileUtils.copyInputStreamToFile(resourceInputStream, file);
			logger.info("Extracting " + resource.getFilename() + " to " + file.getAbsolutePath().toString());
		}
	}

	private static void copyResourceToFilePath(Resource resource, String filePath) throws IOException {
		InputStream resourceInputStream = resource.getInputStream();
		File file = new File(filePath);
		if (!file.exists()) {
			FileUtils.copyInputStreamToFile(resourceInputStream, file);
			logger.info("Extracting " + resource.getFilename() + " to " + file.getAbsolutePath().toString());
		}
	}
}