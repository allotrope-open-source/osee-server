package processor.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class ColorScheme
{
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Map<String, String> domain2scheme = new HashMap()
	{
		private static final long serialVersionUID = 4079928218691037228L;

		{
			put(DomainEnum.COMMON.name(), "#FFFFFF");
			put(DomainEnum.MATERIAL.name(), "#C2E7C2");
			put(DomainEnum.PROCESS.name(), "#CAA3F1");
			put(DomainEnum.RESULT.name(), "#FFCC00");
			put(DomainEnum.INFORMATION.name(), "#FFCC00");
			put(DomainEnum.EQUIPMENT.name(), "#AAD5FF");
			put(DomainEnum.PROPERTY.name(), "#FFFFFF");
			put(DomainEnum.QUALITY.name(), "#FFFF99");
			put(DomainEnum.CONTEXTUAL_ROLE.name(), "#FFC896");
			put(DomainEnum.REALIZABLE_ENTITY.name(), "#FF99CC");
			put(DomainEnum.OTHER.name(), "#FFFFFF");
		}
	};
}
