package processor.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;

/**
 * @author Helge Krieg, OSTHUS GmbH
 */
public class Zipper
{
	public static void compressZipfile(String sourceDir, String outputFile) throws IOException, FileNotFoundException
	{
		ZipOutputStream zipFile = new ZipOutputStream(new FileOutputStream(outputFile));
		compressDirectoryToZipfile(sourceDir, sourceDir, zipFile);
		IOUtils.closeQuietly(zipFile);
	}

	private static void compressDirectoryToZipfile(String rootDir, String sourceDir, ZipOutputStream out) throws IOException, FileNotFoundException
	{
		for (File file : new File(sourceDir).listFiles())
		{
			if (file.isDirectory())
			{
				compressDirectoryToZipfile(rootDir, sourceDir + File.separator + file.getName(), out);
			}
			else
			{
				ZipEntry entry = new ZipEntry(sourceDir.replace(rootDir, "") + file.getName());
				out.putNextEntry(entry);
				String fileName = (sourceDir.isEmpty() ? file.getName() : sourceDir + "/" + file.getName());
				FileInputStream in = new FileInputStream(fileName);
				IOUtils.copy(in, out);
				IOUtils.closeQuietly(in);
			}
		}
	}

}
