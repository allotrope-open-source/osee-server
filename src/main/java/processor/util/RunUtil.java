/*******************************************************************
*               Notice
*
* Copyright Osthus GmbH, All rights reserved.
*
* This software is part of the Allotrope ADM project
*
* Address: Osthus GmbH
*        : Eisenbahnweg 9
*        : 52068 Aachen
*        : Germany
*
*******************************************************************/

package processor.util;

import java.io.IOException;
import java.net.Socket;
import java.util.List;

import org.jutils.jprocesses.JProcesses;
import org.jutils.jprocesses.model.ProcessInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 */
public class RunUtil
{
	public static Logger logger = LoggerFactory.getLogger("log");

	public static boolean available(int port)
	{
		try (Socket ignored = new Socket("localhost", port))
		{
			return false;
		}
		catch (IOException ignored)
		{
			return true;
		}
	}

	public static ProcessInfo getProcessInfo(String name)
	{
		List<ProcessInfo> processesList = JProcesses.getProcessList();

		ProcessInfo proxyServiceInfo = null;
		for (final ProcessInfo processInfo : processesList)
		{
			if (processInfo.getCommand() != null && processInfo.getCommand().contains(name) && !processInfo.getCommand().contains("adfsee-controller.jar"))
			{
				logger.info("Process PID: " + processInfo.getPid());
				logger.info("Process Name: " + processInfo.getName());
				logger.info("Process Time: " + processInfo.getTime());
				logger.info("User: " + processInfo.getUser());
				logger.info("Virtual Memory: " + processInfo.getVirtualMemory());
				logger.info("Physical Memory: " + processInfo.getPhysicalMemory());
				logger.info("CPU usage: " + processInfo.getCpuUsage());
				logger.info("Start Time: " + processInfo.getStartTime());
				logger.info("Priority: " + processInfo.getPriority());
				logger.info("Full command: " + processInfo.getCommand());
				proxyServiceInfo = processInfo;
//				break;
			}
		}
		return proxyServiceInfo;
	}
}
