package processor.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Helge Krieg, OSTHUS GmbH
 */
@SuppressWarnings("deprecation")
public class RdfUtil
{
	public static final String defaultGraph = "urn:x-arq:DefaultGraph";

	private static final Logger log = LogManager.getLogger("Logger");

	private static final Map<String, String> aftPrefixMap = new HashMap<String, String>()
	{
		/**
		 *
		 */
		private static final long serialVersionUID = -7254536606416869104L;

		{
			put("af-c", Vocabulary.AFC_PREFIX);
			put("af-cq", Vocabulary.AFCQ_PREFIX);
			put("af-dt", Vocabulary.AFDT_PREFIX);
			put("af-e", Vocabulary.AFE_PREFIX);
			put("af-m", Vocabulary.AFM_PREFIX);
			put("af-r", Vocabulary.AFR_PREFIX);
			put("af-x", Vocabulary.AFX_PREFIX);
			put("af-p", Vocabulary.AFP_PREFIX);
			put("af-rl", Vocabulary.AFRL_PREFIX);
			put("af-fn", Vocabulary.AFFN_PREFIX);
			put("af-re", Vocabulary.AFRE_PREFIX);
			put("af-q", Vocabulary.AFQ_PREFIX);
			put("af-cur", Vocabulary.AFCUR_PREFIX);
			put("af-ec-001", Vocabulary.AFEC_001_PREFIX);
			put("af-ec-002", Vocabulary.AFEC_002_PREFIX);
			put("af-ec-003", Vocabulary.AFEC_003_PREFIX);
			put("af-ec-004", Vocabulary.AFEC_004_PREFIX);
			put("af-ec-005", Vocabulary.AFEC_005_PREFIX);
			put("af-ec-006", Vocabulary.AFEC_006_PREFIX);
		}
	};

	private static final Map<String, String> oboPrefixMap = new HashMap<String, String>()
	{
		/**
		 *
		 */
		private static final long serialVersionUID = -8647061365759245053L;

		{
			put("obo", Vocabulary.OBO_PREFIX);
			put("cl", Vocabulary.CL_PREFIX);
			put("go", Vocabulary.GO_PREFIX);
			put("ro", Vocabulary.RO_PREFIX);
			put("uo", Vocabulary.UO_PREFIX);
			put("iao", Vocabulary.IAO_PREFIX);
			put("bfo", Vocabulary.BFO_PREFIX);
			put("gaz", Vocabulary.GAZ_PREFIX);
			put("mop", Vocabulary.MOP_PREFIX);
			put("obi", Vocabulary.OBI_PREFIX);
			put("chmo", Vocabulary.CHMO_PREFIX);
			put("envo", Vocabulary.ENVO_PREFIX);
			put("ncbi", Vocabulary.NCBI_PREFIX);
			put("pato", Vocabulary.PATO_PREFIX);
			put("chebi", Vocabulary.CHEBI_PREFIX);
			put("uberon", Vocabulary.UBERON_PREFIX);
		}
	};

	public static Map<String, String> prefixMap = new HashMap<String, String>()
	{
		/**
		 *
		 */
		private static final long serialVersionUID = -1924514529193036734L;

		{
			putAll(aftPrefixMap);
			putAll(oboPrefixMap);
			put("m", Vocabulary.MATHML_PREFIX);
			put("co", Vocabulary.COLLECTION_ONTOLOGY_PREFIX);
			put("ex", Vocabulary.EXAMPLE_PREFIX);
			put("lc", Vocabulary.LC_PREFIX);
			put("qb", Vocabulary.QB_PREFIX);
			put("dct", Vocabulary.DCT_PREFIX);
			put("ex2", Vocabulary.EXAMPLE2_PREFIX);
			put("hdf", Vocabulary.HDF_PREFIX);
			put("ldp", Vocabulary.LDP_PREFIX);
			put("map", Vocabulary.MAP_PREFIX);
			put("ops", Vocabulary.OPS_PREFIX);
			put("ore", Vocabulary.ORE_PREFIX);
			put("org", Vocabulary.ORG_PREFIX);
			put("owl", Vocabulary.OWL_PREFIX);
			put("pav", Vocabulary.PAV_PREFIX);
			put("rdf", Vocabulary.RDF_PREFIX);
			put("xml", Vocabulary.XML_PREFIX);
			put("xsd", Vocabulary.XSD_PREFIX);
			put("foaf", Vocabulary.FOAF_PREFIX);
			put("gear", Vocabulary.GEAR_PREFIX);
			put("omcd", Vocabulary.OPENMATH_PREFIX);
			put("prov", Vocabulary.PROV_PREFIX);
			put("qudt", Vocabulary.QUDT_SCHEMA_PREFIX);
			put("rdfs", Vocabulary.RDFS_PREFIX);
			put("skos", Vocabulary.SKOS_PREFIX);
			put("time", Vocabulary.TIME_PREFIX);
			put("unit", Vocabulary.QUDT_UNIT_PREFIX);
			put("void", Vocabulary.VOID_PREFIX);
			put("afs-c", Vocabulary.AFS_C_PREFIX);
			put("afs-q", Vocabulary.AFS_Q_PREFIX);
			put("rdfg", Vocabulary.RDFG_PREFIX);
			put("shacl", Vocabulary.SHACL_PREFIX);
			put("vcard", Vocabulary.VCARD_PREFIX);
			put("adf-dc", Vocabulary.ADF_DC_PREFIX);
			put("adf-dd", Vocabulary.ADF_DD_PREFIX);
			put("adf-dp", Vocabulary.ADF_DP_PREFIX);
			put("adf-md", Vocabulary.ADF_MD_PREFIX);
			put("af-map", Vocabulary.AFMAP_PREFIX);
			put("afs-dc", Vocabulary.AFS_DC_PREFIX);
			put("afs-hr", Vocabulary.AFS_HR_PREFIX);
			put("dctype", Vocabulary.DCTYPE_PREFIX);
			put("premis", Vocabulary.PREMIS_PREFIX);
			put("af-math", Vocabulary.AFMATH_PREFIX);
			put("qudt-ext", Vocabulary.QUDT_SCHEMA_EXT_PREFIX);
			put("unit-ext", Vocabulary.QUDT_UNIT_EXT_PREFIX);
			put("af-audit", Vocabulary.AFAUDIT_PREFIX);
			put("adf-dc-hdf", Vocabulary.ADF_DC_HDF_PREFIX);
			put("quantity-ext", Vocabulary.QUDT_QUANTITY_EXT_PREFIX);
		}
	};

	private static String prefixes = "";

	private static Map<String, String> namespaceMap = new HashMap<String, String>();

	public static String getPrefixes()
	{
		if (!prefixes.equals(""))
		{
			return prefixes;
		}

		createPrefixes();

		return prefixes;
	}

	public static Map<String, String> getNamespaceMap()
	{
		if (!namespaceMap.isEmpty())
		{
			return namespaceMap;
		}

		createNamespaceMap();

		return namespaceMap;
	}

	private static void createPrefixes()
	{
		StringBuilder sb = new StringBuilder();
		for (Entry<String, String> entry : prefixMap.entrySet())
		{
			sb.append("PREFIX ");
			sb.append(entry.getKey());
			sb.append(": <");
			sb.append(entry.getValue());
			sb.append(">\n");
		}

		prefixes = sb.toString();
		List<String> lines = Arrays.asList(prefixes.split("\\n"));
		Collections.sort(lines);
		prefixes = StringUtils.join(lines, "\n");
	}

	private static void createNamespaceMap()
	{
		for (Entry<String, String> entry : prefixMap.entrySet())
		{
			namespaceMap.put(entry.getValue(), entry.getKey());
		}
	}

	public static void updatePrefixes(String[] newPrefixes)
	{
		for (int i = 0; i < newPrefixes.length; i = i + 2)
		{
			prefixMap.put(newPrefixes[i], newPrefixes[i + 1]);
		}

		createNamespaceMap();
	}

	public static void listPrefixes()
	{
		log.info("Prefixes: ");
		List<String> prefixes = new ArrayList<String>(prefixMap.keySet());
		Collections.sort(prefixes);
		for (Iterator<String> iterator = prefixes.iterator(); iterator.hasNext();)
		{
			String prefix = iterator.next();
			log.info(String.format("%1$15s %2$s", prefix, prefixMap.get(prefix)));
		}
		log.info("");
	}

	public static boolean isAFTNamespace(String namespace)
	{
		if (aftPrefixMap.containsValue(namespace))
		{
			return true;
		}

		return false;
	}

	public static Model convertBlankNodesToNamedResources(Model model)
	{
		log.info("removing blank nodes from data description");
		Map<String, Resource> id2blankNode = new HashMap<String, Resource>();
		Map<Resource, String> blankNode2id = new HashMap<Resource, String>();
		StmtIterator blankStatementIterator = model.listStatements();
		while (blankStatementIterator.hasNext())
		{
			Statement statement = blankStatementIterator.next();
			Resource subject = statement.getSubject();
			if (!subject.isAnon())
			{
				continue;
			}
			String uuid = Vocabulary.URN_UUID + "bnode:" + UUID.randomUUID();
			id2blankNode.put(uuid, subject);
			blankNode2id.put(subject, uuid);
		}

		if (id2blankNode.isEmpty())
		{
			return model;
		}

		List<Statement> statementsToRemove = new ArrayList<Statement>();
		List<Statement> statementsToAdd = new ArrayList<Statement>();

		for (Entry<Resource, String> entry : blankNode2id.entrySet())
		{
			Resource bnode = entry.getKey();
			String uuid = entry.getValue();
			Resource newResource = model.createResource(uuid);
			StmtIterator stmtIterator = model.listStatements(bnode, (Property) null, (RDFNode) null);
			while (stmtIterator.hasNext())
			{
				Statement statement = stmtIterator.next();
				statementsToRemove.add(statement);

				Statement newStatement = null;
				if (!statement.getObject().isAnon())
				{
					newStatement = ResourceFactory.createStatement(newResource, statement.getPredicate(), statement.getObject());
				}
				else
				{
					Resource newResourceForBlankObject = model.createResource(blankNode2id.get(statement.getObject().asResource()));
					newStatement = ResourceFactory.createStatement(newResource, statement.getPredicate(), newResourceForBlankObject);
				}
				statementsToAdd.add(newStatement);
			}

			stmtIterator = model.listStatements((Resource) null, (Property) null, bnode);
			while (stmtIterator.hasNext())
			{
				Statement statement = stmtIterator.next();
				statementsToRemove.add(statement);

				Statement newStatement = null;
				if (!statement.getSubject().isAnon())
				{
					newStatement = ResourceFactory.createStatement(statement.getSubject(), statement.getPredicate(), newResource);
				}
				else
				{
					Resource newResourceForBlankSubject = model.createResource(blankNode2id.get(statement.getSubject()));
					newStatement = ResourceFactory.createStatement(newResourceForBlankSubject, statement.getPredicate(), newResource);
				}
				statementsToAdd.add(newStatement);
			}
		}

		model.remove(statementsToRemove);
		model.add(statementsToAdd);

		return model;
	}

	public static Resource getResourceByLabel(Model model, String label, boolean includeProperties, boolean isResourceExpected)
	{
		if (label == null || label.isEmpty())
		{
			return null;
		}

		if (label.contains("&#10;"))
		{
			label = label.replaceAll("&#10;", "").trim();
		}

		if (label.contains("["))
		{
			label = label.replaceAll("\\[", "").trim();
		}

		if (label.contains("]"))
		{
			label = label.replaceAll("\\]", "").trim();
		}

		if (label.contains("instance of"))
		{
			label = label.replaceAll("instance of", "").trim();
		}

		if (label.contains("NamedIndividual"))
		{
			label = label.replaceAll("NamedIndividual", "").trim();
		}

		if (label.contains("^^"))
		{
			return null;
		}

		if (label.contains(","))
		{
			String[] types = label.split(",");
			for (int i = 0; i < types.length; i++)
			{
				label = types[i].trim();
				if (label.toLowerCase().startsWith("af"))
				{
					break;
				}
			}
		}

		String[] segments = label.split(":");
		String namespace;
		String filterLabel;
		if (segments.length > 1)
		{
			namespace = RdfUtil.namespaceMap.get(segments[0]);
			if (namespace == null || namespace.isEmpty())
			{
				log.info("Missing prefix for term: " + label);
				return null;
			}
			filterLabel = segments[1];
		}
		else
		{
			namespace = Vocabulary.AFO_PREFIX;
			filterLabel = label;
		}

		if (namespace.equals(Vocabulary.RDF_PREFIX))
		{
			return model.getResource(namespace + filterLabel);
		}

		if (namespace.contains(Vocabulary.QUDT_SCHEMA_PREFIX))
		{
			filterLabel = filterLabel.replaceAll("\\s", "-");
			filterLabel = WordUtils.capitalizeFully(filterLabel, '-').replaceAll("\\-", "");
			if (!isResourceExpected)
			{
				// property expected that should have label starting with lowercase
				String firstLetterLowercase = filterLabel.substring(0, 1).toLowerCase();
				filterLabel = firstLetterLowercase + filterLabel.substring(1, filterLabel.length());
			}
			return model.getResource(namespace + filterLabel);
		}

		Set<String> partialHits = new HashSet<String>();

		Set<Resource> allClassesAndIndividuals = new HashSet<Resource>();
		StmtIterator classIterator = model.listStatements((Resource) null, (Property) null, Vocabulary.OWL_CLASS);
		while (classIterator.hasNext())
		{
			Statement statement = classIterator.next();
			Resource subject = statement.getSubject();
			allClassesAndIndividuals.add(subject);
		}

		StmtIterator individualIterator = model.listStatements((Resource) null, (Property) null, Vocabulary.OWL_NAMED_INDIVIDUAL);
		while (individualIterator.hasNext())
		{
			Statement statement = individualIterator.next();
			Resource subject = statement.getSubject();
			allClassesAndIndividuals.add(subject);
		}

		if (includeProperties)
		{
			StmtIterator propertiesIterator = model.listStatements((Resource) null, (Property) null, Vocabulary.OWL_OBJECT_PROPERTY);
			while (propertiesIterator.hasNext())
			{
				Statement statement = propertiesIterator.next();
				Resource subject = statement.getSubject();
				allClassesAndIndividuals.add(subject);
			}

			propertiesIterator = model.listStatements((Resource) null, (Property) null, Vocabulary.OWL_DATATYPE_PROPERTY);
			while (propertiesIterator.hasNext())
			{
				Statement statement = propertiesIterator.next();
				Resource subject = statement.getSubject();
				allClassesAndIndividuals.add(subject);
			}

			propertiesIterator = model.listStatements((Resource) null, (Property) null, Vocabulary.OWL_ANNOTATION_PROPERTY);
			while (propertiesIterator.hasNext())
			{
				Statement statement = propertiesIterator.next();
				Resource subject = statement.getSubject();
				allClassesAndIndividuals.add(subject);
			}

		}

		Iterator<Resource> nodeIterator = allClassesAndIndividuals.iterator();
		while (nodeIterator.hasNext())
		{
			Resource subject = nodeIterator.next();

			StmtIterator subjectStmtIterator = model.listStatements(subject, (Property) null, (RDFNode) null);
			while (subjectStmtIterator.hasNext())
			{
				Statement subjectStatement = subjectStmtIterator.next();
				if (subjectStatement.getPredicate().equals(Vocabulary.SKOS_PREF_LABEL) || subjectStatement.getPredicate().equals(Vocabulary.RDFS_LABEL))
				{
					String currentLabel = subjectStatement.getString();
					if (!filterLabel.equals(currentLabel))
					{
						continue;
					}

					if (subject.getURI().startsWith(namespace))
					{
						return subject;
					}
					else
					{
						String currentNamespace = RdfUtil.getNamespaceMap().get(subject.getNameSpace());
						if (currentNamespace != null)
						{
							partialHits.add(currentNamespace + ":" + currentLabel);
						}
						else
						{
							partialHits.add(subject.getNameSpace() + currentLabel);
						}
					}
				}
			}
		}

		if (!partialHits.isEmpty())
		{
			log.info("No term found for label \"" + label + "\" but found possible hit: " + StringUtils.join(partialHits, ", "));
		}

		return null;
	}

	public static String addPrefix(String labelString, Resource resource)
	{
		if (!resource.isURIResource())
		{
			return labelString;
		}

		String prefix = StringUtils.EMPTY;
		if (resource.getURI().startsWith(Vocabulary.URN_UUID))
		{
			prefix = Vocabulary.URN_UUID.substring(0, Vocabulary.URN_UUID.length() - 1);
		}
		else
		{
			prefix = getNamespaceMap().get(resource.getNameSpace());
		}
		if (prefix != null && !prefix.isEmpty())
		{
			if (prefix.equals("obo"))
			{
				prefix = getPrefixForOboTermLabel(resource.getLocalName());
			}
			labelString = prefix.trim() + ":" + labelString.trim();
		}
		return labelString;
	}

	public static String getPrefixForOboTermLabel(String name)
	{
		String prefix;
		if (name.startsWith("BFO"))
		{
			prefix = "bfo";
		}
		else if (name.startsWith("IAO"))
		{
			prefix = "iao";
		}
		else if (name.startsWith("OBI"))
		{
			prefix = "obi";
		}
		else if (name.startsWith("RO"))
		{
			prefix = "ro";
		}
		else if (name.startsWith("CHEBI"))
		{
			prefix = "chebi";
		}
		else if (name.startsWith("PATO"))
		{
			prefix = "pato";
		}
		else if (name.contains("_"))
		{
			prefix = name.split("_")[0].toLowerCase();
		}
		else
		{
			prefix = "obo";
		}
		return prefix;
	}
}
