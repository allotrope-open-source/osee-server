package processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import processor.cytojo.CytoEdge;
import processor.cytojo.CytoEdgeData;
import processor.cytojo.CytoEntity;
import processor.cytojo.CytoNode;
import processor.cytojo.CytoNodeData;
import processor.util.ColorScheme;
import processor.util.DomainEnum;
import processor.util.RdfUtil;
import processor.util.Vocabulary;
import query.SparqlQuery;

/**
 * Transformation from RDF to cytoscape input
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class Rdf2Cyto
{
	private static Set<Resource> resourcesAsNodes = new HashSet<>();
	private static Set<String> visited = new HashSet<String>();

	public static Set<CytoEntity> create(Model model, Model externalResources)
	{
		resourcesAsNodes.clear();

		if (Processor.extractInstances)
		{
			Model instanceModel = AfInstanceExtractor.createInstanceModel(model, externalResources);
			model = instanceModel;
		}
		else if (Processor.visualizeOntology && externalResources != null && !externalResources.isEmpty())
		{
			Processor.log.info("Visualizing ontology. Merging input file with additional resources.");
			model.add(externalResources);
			Processor.log.info("Total number of triples after merging: " + model.size());
		}

		model = splitCrowdedResources(model, externalResources);

		Set<CytoEntity> entities = transformModelToCytoEntitiesByIteration(model, externalResources);

		if (!Processor.visualizeOntology) {
			entities = determineClusters(entities, model, externalResources);
		}

		outputAmount(entities);
		checkForLonelyNodes(entities);
		return entities;
	}

	private static Model splitCrowdedResources(Model model, Model externalResources)
	{
		List<Statement> statementsToRemove = new ArrayList<Statement>();
		List<Statement> statementsToAdd = new ArrayList<Statement>();
		Set<Resource> visited = new HashSet<Resource>();
		StmtIterator stmtIterator = model.listStatements((Resource) null, (Property) null, (RDFNode) null);
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			if (statement.getObject().isLiteral())
			{
				continue;
			}

			if (statement.getObject().isAnon())
			{
				continue;
			}

			Resource object = statement.getResource();
			if (visited.contains(object))
			{
				continue;
			}

			visited.add(object);
			if (isCrowdedResource(object, model, externalResources))
			{
				Processor.log.info("untangling crowded resource: " + object.getURI());
				StmtIterator incomingIterator = model.listStatements((Resource) null, (Property) null, object);
				while (incomingIterator.hasNext())
				{
					Statement incomingStatement = incomingIterator.next();
					Resource surrogate = model.createResource(Vocabulary.URN_UUID + UUID.randomUUID());
					StmtIterator objectStatements = model.listStatements(object, (Property) null, (RDFNode) null);
					while (objectStatements.hasNext())
					{
						Statement objectStatement = objectStatements.next();
						if (objectStatement.getObject().isLiteral())
						{
							statementsToAdd.add(model.createStatement(surrogate, objectStatement.getPredicate(), objectStatement.getObject()));
						}

						if (Vocabulary.RDF_TYPE.getURI().equals(objectStatement.getPredicate().getURI()))
						{
							statementsToAdd.add(model.createStatement(surrogate, objectStatement.getPredicate(), objectStatement.getObject()));
							// TODO handle blank nodes of rdf types
						}
					}
					statementsToAdd.add(model.createStatement(incomingStatement.getSubject(), incomingStatement.getPredicate(), surrogate));
					statementsToAdd.add(model.createStatement(surrogate, Vocabulary.RDF_TYPE, object));
					statementsToRemove.add(incomingStatement);
				}
			}
			else if (isCrowdedOntologyResourceInShaclShape(object, model, externalResources))
			{
				Processor.log.info("untangling crowded resource in shape: " + object.getURI());
				StmtIterator incomingIterator = model.listStatements((Resource) null, (Property) null, object);
				while (incomingIterator.hasNext())
				{
					Statement incomingStatement = incomingIterator.next();
					if (!incomingStatement.getPredicate().getURI().toString().startsWith(Vocabulary.SHACL_PREFIX))
					{
						continue;
					}

					Resource surrogate = model.createResource(Vocabulary.URN_UUID + UUID.randomUUID());
					StmtIterator objectStatements = model.listStatements(object, (Property) null, (RDFNode) null);
					while (objectStatements.hasNext())
					{
						Statement objectStatement = objectStatements.next();
						if (objectStatement.getObject().isLiteral())
						{
							statementsToAdd.add(model.createStatement(surrogate, objectStatement.getPredicate(), objectStatement.getObject()));
						}

						if (Vocabulary.RDF_TYPE.getURI().equals(objectStatement.getPredicate().getURI()))
						{
							statementsToAdd.add(model.createStatement(surrogate, objectStatement.getPredicate(), objectStatement.getObject()));
							// TODO handle blank nodes of rdf types
						}
					}
					statementsToAdd.add(model.createStatement(incomingStatement.getSubject(), incomingStatement.getPredicate(), surrogate));
					statementsToAdd.add(model.createStatement(surrogate, Vocabulary.RDF_TYPE, object));
					statementsToRemove.add(incomingStatement);
				}
			}
		}

		if (!statementsToRemove.isEmpty())
		{
			model.remove(statementsToRemove);
		}

		if (!statementsToAdd.isEmpty())
		{
			model.add(statementsToAdd);
		}

		return model;
	}

	private static boolean isCrowdedResource(Resource resource, Model model, Model externalResources)
	{
		if (!hasRelevantType(resource, model, externalResources))
		{
			return false;
		}

		Set<Resource> froms = new HashSet<Resource>();
		StmtIterator incomingStatementIterator = model.listStatements((Resource) null, (Property) null, resource);
		while (incomingStatementIterator.hasNext())
		{
			Statement incomingStatement = incomingStatementIterator.next();
			froms.add(incomingStatement.getSubject());
		}

		if (froms.size() > 1)
		{
			return true;
		}

		return false;
	}

	private static boolean hasRelevantType(Resource resource, Model model, Model externalResources)
	{
		if (resource.getURI().toLowerCase().contains("qudt"))
		{
			return true;
		}

		if (resource.getURI().toLowerCase().contains("chebi"))
		{
			return true;
		}

		if (Vocabulary.XSD_DOUBLE.getURI().equals(resource.getURI()))
		{
			return true;
		}

		if (Vocabulary.XSD_DATETIME.getURI().equals(resource.getURI()))
		{
			return true;
		}

		if (Vocabulary.XSD_STRING.getURI().equals(resource.getURI()))
		{
			return true;
		}

		if (Vocabulary.XSD_BOOLEAN.getURI().equals(resource.getURI()))
		{
			return true;
		}

		if (Vocabulary.XSD_INTEGER.getURI().equals(resource.getURI()))
		{
			return true;
		}

		if (Vocabulary.RDF_NIL.getURI().equals(resource.getURI()))
		{
			return true;
		}

		if (Processor.visualizeOntology)
		{
			if (Vocabulary.OWL_CLASS.getURI().equals(resource.getURI()))
			{
				return true;
			}

			if (Vocabulary.OWL_AXIOM.getURI().equals(resource.getURI()))
			{
				return true;
			}

			if (Vocabulary.OBO_ELUCIDATION.getURI().equals(resource.getURI()))
			{
				return true;
			}

			if (resource.getURI().endsWith(".owl"))
			{
				return true;
			}
		}
		return false;
	}

	private static boolean isCrowdedOntologyResourceInShaclShape(Resource resource, Model model, Model externalResources)
	{
		boolean isObjectOfShaclProperty = false;

		StmtIterator stmtIterator = model.listStatements((Resource) null, (Property) null, resource);
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			if (statement.getPredicate().getURI().toString().startsWith(Vocabulary.SHACL_PREFIX))
			{
				isObjectOfShaclProperty = true;
				break;
			}
		}

		if (externalResources != null && !isObjectOfShaclProperty)
		{
			StmtIterator stmtIterator2 = externalResources.listStatements((Resource) null, (Property) null, resource);
			while (stmtIterator2.hasNext())
			{
				Statement statement = stmtIterator2.next();
				if (statement.getPredicate().getURI().toString().startsWith(Vocabulary.SHACL_PREFIX))
				{
					isObjectOfShaclProperty = true;
					break;
				}
			}
		}

		if (!isObjectOfShaclProperty)
		{
			return false;
		}

		// is it crowded?
		Set<Resource> froms = new HashSet<Resource>();
		StmtIterator incomingStatementIterator = model.listStatements((Resource) null, (Property) null, resource);
		while (incomingStatementIterator.hasNext())
		{
			Statement incomingStatement = incomingStatementIterator.next();
			froms.add(incomingStatement.getSubject());
		}

		if (froms.size() < 2)
		{
			return false;
		}

		if (model.listStatements(resource, RDF.type, OWL.Class).hasNext())
		{
			return true;
		}

		if (externalResources != null && externalResources.listStatements(resource, RDF.type, OWL.Class).hasNext())
		{
			return true;
		}

		if (model.listStatements(resource, RDF.type, OWL.ObjectProperty).hasNext())
		{
			return true;
		}

		if (externalResources != null && externalResources.listStatements(resource, RDF.type, OWL.ObjectProperty).hasNext())
		{
			return true;
		}

		if (model.listStatements(resource, RDF.type, OWL.DatatypeProperty).hasNext())
		{
			return true;
		}

		if (externalResources != null && externalResources.listStatements(resource, RDF.type, OWL.DatatypeProperty).hasNext())
		{
			return true;
		}

		return false;
	}

	private static Set<CytoEntity> transformModelToCytoEntitiesByIteration(Model model, Model externalResources)
	{
		Processor.log.info("Transforming RDF model to cytoscape entity model...");
		Model mergedModel = ModelFactory.createDefaultModel();
		mergedModel.add(model);
		if (externalResources != null && !externalResources.isEmpty())
		{
			mergedModel.add(externalResources);
		}

		Set<CytoEntity> entities = new HashSet<CytoEntity>();
		Set<String> visited = new HashSet<String>();
		
		long numTriples = 0;
		long count  = 0;
		StmtIterator stmtIterator = null;
		if (!Processor.visualizeOntology) {
			stmtIterator = model.listStatements();
			numTriples = model.size();
		} else {
			stmtIterator = mergedModel.listStatements();
			numTriples = mergedModel.size();
		}
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();

			count++;
			if (count%1000 == 0 || (count == numTriples)) {
				Processor.log.info("transformed: " + count + " / " + numTriples);
			} 
			if (statement.getPredicate().equals(Vocabulary.RDF_TYPE))
			{
				continue;
			}

			Resource subject = statement.getSubject();
			String subjectId = StringUtils.EMPTY;
			if (subject.isURIResource())
			{
				subjectId = subject.getURI();
			}
			else
			{
				subjectId = subject.asNode().getBlankNodeId().getLabelString();
			}

			if (visited.contains(subjectId) && statement.getObject().isLiteral())
			{
				continue;
			}

			visited.add(subjectId);

			if (isNodeToBeSkipped(statement.getSubject(), model))
			{
				Processor.log.debug("Skipping statement: " + statement.toString());
				continue;
			}

			if (isNodeToBeSkipped(statement.getObject(), model))
			{
				Processor.log.debug("Skipping statement: " + statement.toString());
				continue;
			}

			CytoNode subjectNode = getCytoNode(subjectId, entities);
			if (subjectNode == null)
			{
				subjectNode = determineCytoEntityForResource(subject, model, externalResources, mergedModel);
				entities.add(subjectNode);
				entities = addLiteralPropertiesForResource(subject, subjectNode, model, externalResources, entities);
			}

			if (statement.getObject().isLiteral())
			{
				continue;
			}

			String objectId = StringUtils.EMPTY;
			if (statement.getObject().isURIResource())
			{
				objectId = statement.getObject().asResource().getURI();
			}
			else
			{
				objectId = statement.getObject().asNode().getBlankNodeId().getLabelString();
			}

			CytoNode objectNode = getCytoNode(objectId, entities);
			if (objectNode == null)
			{
				objectNode = determineCytoEntityForResource(statement.getResource(), model, externalResources, mergedModel);
				entities.add(objectNode);
				entities = addLiteralPropertiesForResource(statement.getResource(), objectNode, model, externalResources, entities);
			}

			CytoEdge cytoEdge = createCytoEdgeForLink(statement.getPredicate(), subjectNode, objectNode, model, externalResources, false);
			entities.add(cytoEdge);
		}
		return entities;
	}

	private static boolean isNodeToBeSkipped(RDFNode node, Model model)
	{
		if (!Processor.visualizeOntology)
		{

			if (node.isURIResource() && Vocabulary.SHACL_NODE_SHAPE.getURI().equals(node.asResource().getURI()))
			{
				return true;
			}

			if (node.isURIResource() && Vocabulary.SHACL_PROPERTY_SHAPE.getURI().equals(node.asResource().getURI()))
			{
				return true;
			}

			if (node.isURIResource() && Vocabulary.OWL_CLASS.getURI().equals(node.asResource().getURI()))
			{
				return true;
			}

			if (node.isURIResource() && Vocabulary.OWL_ANNOTATION_PROPERTY.getURI().equals(node.asResource().getURI()))
			{
				return true;
			}

			if (node.isURIResource() && Vocabulary.OWL_OBJECT_PROPERTY.getURI().equals(node.asResource().getURI()))
			{
				return true;
			}

			if (node.isURIResource() && Vocabulary.OWL_DATATYPE_PROPERTY.getURI().equals(node.asResource().getURI()))
			{
				return true;
			}

			if (node.isURIResource() && Vocabulary.OWL_NAMED_INDIVIDUAL.getURI().equals(node.asResource().getURI()))
			{
				return true;
			}

			return false;
		}

		if (node.isURIResource())
		{
			if (Vocabulary.OWL_CLASS.getURI().equals(node.asResource().getURI()))
			{
				return true;
			}

			if (Vocabulary.OWL_NAMED_INDIVIDUAL.getURI().equals(node.asResource().getURI()))
			{
				return true;
			}

			if (Vocabulary.OWL_RESTRICTION.getURI().equals(node.asResource().getURI()))
			{
				return true;
			}

			if (Vocabulary.OWL_AXIOM.getURI().equals(node.asResource().getURI()))
			{
				return true;
			}

			if (Vocabulary.OWL_ANNOTATION_PROPERTY.getURI().equals(node.asResource().getURI()))
			{
				return true;
			}

			if (Vocabulary.OWL_OBJECT_PROPERTY.getURI().equals(node.asResource().getURI()))
			{
				return true;
			}

			if (Vocabulary.OWL_DATATYPE_PROPERTY.getURI().equals(node.asResource().getURI()))
			{
				return true;
			}

			if (Vocabulary.IAO_HAS_ASSOCIATED_AXIOM_NL.getURI().equals(node.asResource().getURI()))
			{
				return true;
			}

			if (Vocabulary.IAO_HAS_ASSOCIATED_AXIOM_FOL.getURI().equals(node.asResource().getURI()))
			{
				return true;
			}

			if (Vocabulary.OBO_ELUCIDATION.getURI().equals(node.asResource().getURI()))
			{
				return true;
			}

			if (node.asResource().getURI().endsWith(".owl"))
			{
				return true;
			}

			if (node.asResource().hasProperty(Vocabulary.OWL_ANNOTATEDSOURCE))
			{
				return true;
			}

			if (node.asResource().hasProperty(Vocabulary.OWL_ANNOTATEDTARGET))
			{
				return true;
			}

			if (node.asResource().hasProperty(Vocabulary.OWL_ANNOTATEDPROPERTY))
			{
				return true;
			}

			if (model.listStatements(null, Vocabulary.RDFS_IS_DEFINED_BY, node).hasNext())
			{
				return true;
			}

		}

		if ((node.isURIResource() && node.asResource().getURI().startsWith(Vocabulary.URN_UUID)) || node.isAnon())
		{
			if (node.asResource().hasProperty(Vocabulary.RDF_TYPE, Vocabulary.OWL_CLASS) && !Processor.visualizeOntology)
			{
				return true;
			}
			if (node.asResource().hasProperty(Vocabulary.RDF_TYPE, Vocabulary.OWL_RESTRICTION))
			{
				return true;
			}
			if (node.asResource().hasProperty(Vocabulary.RDF_TYPE, Vocabulary.OWL_AXIOM))
			{
				return true;
			}
			if (node.asResource().hasProperty(Vocabulary.RDF_TYPE, Vocabulary.OWL_ANNOTATION_PROPERTY) && !Processor.visualizeOntology)
			{
				return true;
			}
			if (node.asResource().hasProperty(Vocabulary.RDF_TYPE, Vocabulary.OWL_OBJECT_PROPERTY) && !Processor.visualizeOntology)
			{
				return true;
			}
			if (node.asResource().hasProperty(Vocabulary.RDF_TYPE, Vocabulary.OWL_DATATYPE_PROPERTY) && !Processor.visualizeOntology)
			{
				return true;
			}
			if (node.asResource().hasProperty(Vocabulary.RDF_TYPE, Vocabulary.IAO_HAS_ASSOCIATED_AXIOM_NL))
			{
				return true;
			}
			if (node.asResource().hasProperty(Vocabulary.RDF_TYPE, Vocabulary.IAO_HAS_ASSOCIATED_AXIOM_FOL))
			{
				return true;
			}
			if (node.asResource().hasProperty(Vocabulary.RDF_TYPE, Vocabulary.OBO_ELUCIDATION))
			{
				return true;
			}

			if (node.asResource().hasProperty(Vocabulary.OWL_ANNOTATEDSOURCE))
			{
				return true;
			}

			if (node.asResource().hasProperty(Vocabulary.OWL_ANNOTATEDTARGET))
			{
				return true;
			}

			if (node.asResource().hasProperty(Vocabulary.OWL_ANNOTATEDPROPERTY))
			{
				return true;
			}

			if (model.listStatements(null, Vocabulary.RDFS_IS_DEFINED_BY, node).hasNext())
			{
				return true;
			}

		}

		return false;
	}

	public static Set<CytoNode> checkForLonelyNodes(Set<CytoEntity> entities)
	{
		Set<String> branchNodes = new HashSet<String>();
		for (Iterator<CytoEntity> iterator = entities.iterator(); iterator.hasNext();)
		{
			CytoEntity cytoEntity = iterator.next();
			if (cytoEntity instanceof CytoEdge)
			{
				CytoEdge cytoEdge = (CytoEdge) cytoEntity;
				branchNodes.add(cytoEdge.getData().getSource());
				branchNodes.add(cytoEdge.getData().getTarget());
			}
		}

		Set<CytoNode> lonelyNodes = new HashSet<CytoNode>();
		for (Iterator<CytoEntity> iterator = entities.iterator(); iterator.hasNext();)
		{
			CytoEntity cytoEntity = iterator.next();
			if (cytoEntity instanceof CytoNode)
			{
				CytoNode cytoNode = (CytoNode) cytoEntity;
				if (!branchNodes.contains(cytoNode.getData().getId()))
				{
					lonelyNodes.add(cytoNode);
				}
			}
		}

		if (!lonelyNodes.isEmpty())
		{
			Processor.log.info("found " + lonelyNodes.size() + " lonely nodes!");
			for (Iterator<CytoNode> iterator = lonelyNodes.iterator(); iterator.hasNext();)
			{
				CytoNode cytoNode = iterator.next();
				Processor.log.info("lonely node: " + cytoNode.getData().getId() + " " + cytoNode.getData().getLabel());
			}
		}
		return lonelyNodes;
	}

	public static void outputAmount(Set<CytoEntity> entities)
	{
		long nodes = 0;
		long edges = 0;
		long total = 0;
		for (Iterator<CytoEntity> iterator = entities.iterator(); iterator.hasNext();)
		{
			CytoEntity cytoEntity = iterator.next();
			if (cytoEntity instanceof CytoNode)
			{
				nodes++;
			}
			else if (cytoEntity instanceof CytoEdge)
			{
				edges++;
			}
			else
			{
				throw new IllegalStateException("found unknown cyto entity");
			}
			total++;
		}
		Processor.log.info("nodes= " + nodes + " edges= " + edges + " total= " + total);
	}

	private static Set<CytoEntity> transformModelToCytoEntitiesByRecursivePathFollowing(Model model, Model externalResources)
	{
		Processor.log.info("creating cytoscape entities");
		Model mergedModel = ModelFactory.createDefaultModel();
		mergedModel.add(model);
		if (externalResources != null && !externalResources.isEmpty())
		{
			mergedModel.add(externalResources);
		}

		Model tempModel = ModelFactory.createDefaultModel();
		tempModel.add(model);

		Set<CytoEntity> cytoEntities = new HashSet<>();

		long delta = 1;
		int numIter = 1;
		long numTriples = tempModel.size();
		long numTriplesNext = tempModel.size();
		while (delta > 0 && numTriples > 0 && numTriplesNext > 0 && numIter < 20)
		{
			numTriples = tempModel.size();
			Processor.log.info("Graph iteration: " + numIter + " #triples: " + numTriples);

			Resource rootResource = null;
			if (Processor.rootNode == null || numIter > 1)
			{
				rootResource = Rdf2Cyto.findRoot(tempModel, externalResources);
			}
			else
			{
				rootResource = Processor.rootNode;
			}

			if (rootResource == null && numIter == 1)
			{
				throw new IllegalStateException("No root node found");
			}
			else if (rootResource == null && numIter > 1)
			{
				Processor.log.info("No further root node found.");
				break;
			}
			else
			{
				Processor.log.info("found root node: " + (rootResource.isAnon() ? rootResource.asNode().getBlankNodeLabel() : rootResource.getURI()));
			}

			CytoNode cytoNode = determineCytoEntityForResource(rootResource, tempModel, externalResources, mergedModel);
			cytoEntities.add(cytoNode);

			Set<Statement> visited = new HashSet<Statement>();
			Set<String> entities = new HashSet<String>();
			if (rootResource.isURIResource())
			{
				entities.add(rootResource.getURI());
			}
			else
			{
				entities.add(rootResource.asNode().getBlankNodeId().getLabelString());
			}
			resourcesAsNodes.add(rootResource);

			VisitNeighbourStruct visitNeighbourStruct = new VisitNeighbourStruct(cytoEntities, visited, entities);

			if (Processor.visualizeOntology)
			{
				Model fullModel = ModelFactory.createDefaultModel();
				fullModel.add(tempModel);
				if (externalResources != null)
				{
					fullModel.add(externalResources);
				}
				visitNeighbourStruct = addEntitiesForNeighbours(rootResource, cytoNode, fullModel, externalResources, mergedModel, visitNeighbourStruct);
			}
			else
			{
				visitNeighbourStruct = addEntitiesForNeighbours(rootResource, cytoNode, tempModel, externalResources, mergedModel, visitNeighbourStruct);
			}

			if (Processor.useNestedNodes)
			{
				visitNeighbourStruct = addParentNodesForScalarValuePattern(visitNeighbourStruct, tempModel);
			}

			cytoEntities.addAll(visitNeighbourStruct.getCytoEntities());

			if (Processor.singleGraph)
			{
				break;
			}

			Set<Resource> resourcesToRemove = new HashSet<Resource>();
			for (Resource resource : resourcesAsNodes)
			{
				if (tempModel.containsResource(resource))
				{
					resourcesToRemove.add(resource);
				}
			}

			List<Statement> statementsToRemove = new ArrayList<Statement>();
			for (Resource resource : resourcesToRemove)
			{
				statementsToRemove.addAll(tempModel.listStatements(resource, (Property) null, (RDFNode) null).toList());
				statementsToRemove.addAll(tempModel.listStatements((Resource) null, (Property) null, resource).toList());
			}
			tempModel.remove(statementsToRemove);

			numTriplesNext = tempModel.size();
			delta = numTriples - numTriplesNext;
			numIter++;
			rootResource = null;
		}

		return cytoEntities;
	}

	private static VisitNeighbourStruct addParentNodesForScalarValuePattern(VisitNeighbourStruct visitNeighbourStruct, Model model)
	{
		Set<String> resourceIds = visitNeighbourStruct.getResources();
		Set<CytoEntity> cytoEntities = visitNeighbourStruct.getCytoEntities();

		Set<Resource> scalarValues = new HashSet<Resource>();
		for (Iterator<Resource> iterator = resourcesAsNodes.iterator(); iterator.hasNext();)
		{
			Resource resource = iterator.next();
			if (model.listStatements(resource, Vocabulary.RDF_TYPE, Vocabulary.AFR_SCALAR_QUANTITY_DATUM).hasNext())
			{
				scalarValues.add(resource);
			}
		}

		for (Iterator<Resource> iterator = scalarValues.iterator(); iterator.hasNext();)
		{
			Resource scalarValue = iterator.next();
			Resource quantityValue = null;
			StmtIterator stmtIterator = model.listStatements(scalarValue, Vocabulary.AFX_QUANTIFIED_BY, (RDFNode) null);
			while (stmtIterator.hasNext())
			{
				Statement statement = stmtIterator.next();
				Resource resource = statement.getResource();
				if (resource.hasProperty(Vocabulary.RDF_TYPE, Vocabulary.QUDT_QUANTITY_VALUE))
				{
					quantityValue = resource;
					break;
				}
			}

			if (quantityValue == null)
			{
				continue;
			}

			CytoNode scalarValueNode = getNode(cytoEntities, scalarValue);

			String parentId = scalarValueNode.getData().getId() + "pattern";
			String label = scalarValueNode.getData().getLabel();
			String popup = scalarValueNode.getData().getPopup();
			CytoNode parentNode = new CytoNode(new CytoNodeData(parentId, label));
			parentNode.getData().setPopup(popup);
			parentNode.getData().setShape("rectangle");
			parentNode.getData().setBg(scalarValueNode.getData().getBg());
			cytoEntities.add(parentNode);

			scalarValueNode.getData().setParent(parentId);
			cytoEntities.add(scalarValueNode);
			CytoNode quantityValueNode = getNode(cytoEntities, quantityValue);
			quantityValueNode.getData().setParent(parentId);
			cytoEntities.add(quantityValueNode);

			Resource unit = null;
			StmtIterator qvStmtIterator = model.listStatements(quantityValue, Vocabulary.QUDT_UNIT, (RDFNode) null);
			while (qvStmtIterator.hasNext())
			{
				Statement statement = qvStmtIterator.next();
				Resource resource = statement.getResource();
				unit = resource;
				break;
			}

			if (unit != null)
			{
				CytoNode unitNode = getNode(cytoEntities, unit);
				unitNode.getData().setParent(parentId);
				cytoEntities.add(unitNode);
			}
		}

		visitNeighbourStruct.setResources(resourceIds);
		visitNeighbourStruct.setCytoEntities(cytoEntities);
		return visitNeighbourStruct;
	}

	private static CytoNode getNode(Set<CytoEntity> cytoEntities, Resource resource)
	{
		CytoNode cytoNode;
		if (existsNode(cytoEntities, resource.getURI() + "parent"))
		{
			cytoNode = getCytoNode(resource.getURI() + "parent", cytoEntities);
		}
		else
		{
			cytoNode = getCytoNode(resource, cytoEntities);
		}
		return cytoNode;
	}

	public static CytoNode getCytoNode(String string, Set<CytoEntity> cytoEntities)
	{
		for (Iterator<CytoEntity> iterator = cytoEntities.iterator(); iterator.hasNext();)
		{
			CytoEntity cytoEntity = iterator.next();
			if (cytoEntity instanceof CytoEdge)
			{
				continue;
			}

			CytoNode cytoNode = (CytoNode) cytoEntity;
			if (cytoNode.getData().getId().equals(string))
			{
				return cytoNode;
			}
		}
		return null;
	}

	private static VisitNeighbourStruct addEntitiesForNeighbours(Resource resource, CytoNode cytoNode, Model model, Model externalResources, Model mergedModel,
			VisitNeighbourStruct visitNeighbourStruct)
	{
		if (Processor.visualizeOntology)
		{
			if (resource.isURIResource() && resource.getURI().startsWith(Vocabulary.OWL_PREFIX))
			{
				return visitNeighbourStruct;
			}

			if (resource.isAnon())
			{
				StmtIterator stmtIterator = model.listStatements(resource, Vocabulary.RDF_TYPE, (RDFNode) null);
				while (stmtIterator.hasNext())
				{
					Statement statement = stmtIterator.next();
					if (statement.getObject().isURIResource() && statement.getResource().getURI().startsWith(Vocabulary.OWL_PREFIX))
					{
						return visitNeighbourStruct;
					}
				}
				return visitNeighbourStruct;
			}
		}

		Processor.log.info("visiting: " + (resource.isAnon() ? resource.asNode().getBlankNodeLabel() : resource.getURI()));
		Set<Statement> visited = visitNeighbourStruct.getVisited();
		Set<String> resources = visitNeighbourStruct.getResources();
		Set<CytoEntity> cytoEntities = visitNeighbourStruct.getCytoEntities();

		StmtIterator stmtIterator = model.listStatements(resource, (Property) null, (RDFNode) null);
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			if (statement.getObject().isLiteral())
			{
				continue;
			}

			if (visited.contains(statement))
			{
				continue;
			}

			if (Processor.excludeRdfTypeNodes && !Processor.visualizeOntology && statement.getPredicate().equals(Vocabulary.RDF_TYPE))
			{
				continue;
			}

			if (Processor.visualizeOntology && statement.getPredicate().equals(Vocabulary.RDF_TYPE))
			{
				if (statement.getObject().isURIResource() && (statement.getResource().equals(Vocabulary.OWL_CLASS) || statement.getResource().equals(Vocabulary.OWL_OBJECT_PROPERTY)
						|| statement.getResource().equals(Vocabulary.OWL_DATATYPE_PROPERTY) || statement.getResource().equals(Vocabulary.OWL_ANNOTATION_PROPERTY)
						|| statement.getResource().equals(Vocabulary.OWL_NAMED_INDIVIDUAL)))
				{
					continue;
				}
			}

			CytoNode neighbour = null;
			if (statement.getResource().isURIResource())
			{
				if (!resources.contains(statement.getResource().getURI()))
				{
					neighbour = determineCytoEntityForResource(statement.getResource(), model, externalResources, mergedModel);
					cytoEntities.add(neighbour);
					resources.add(statement.getResource().getURI());
					resourcesAsNodes.add(statement.getResource());
				}
				else
				{
					neighbour = getCytoNode(statement.getResource(), cytoEntities);
				}
			}
			else
			{
				String id = statement.getResource().asNode().getBlankNodeId().getLabelString();
				if (!resources.contains(id))
				{
					neighbour = determineCytoEntityForResource(statement.getResource(), model, externalResources, mergedModel);
					cytoEntities.add(neighbour);
					resources.add(id);
					resourcesAsNodes.add(statement.getResource());
				}
				else
				{
					neighbour = getCytoNode(statement.getResource(), cytoEntities);
				}
			}

			CytoEdge cytoEdge = createCytoEdgeForLink(statement.getPredicate(), cytoNode, neighbour, model, externalResources, false);
			cytoEntities.add(cytoEdge);

			visited.add(statement);
			visitNeighbourStruct.setVisited(visited);
			visitNeighbourStruct.setResources(resources);
			visitNeighbourStruct.setCytoEntities(cytoEntities);
			visitNeighbourStruct = addEntitiesForNeighbours(statement.getResource(), neighbour, model, externalResources, mergedModel, visitNeighbourStruct);
		}

		visited = visitNeighbourStruct.getVisited();
		resources = visitNeighbourStruct.getResources();
		cytoEntities = visitNeighbourStruct.getCytoEntities();

		stmtIterator = model.listStatements((Resource) null, (Property) null, resource);
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();

			if (visited.contains(statement))
			{
				continue;
			}

			if (Processor.excludeRdfTypeNodes && !Processor.visualizeOntology && statement.getPredicate().equals(Vocabulary.RDF_TYPE))
			{
				continue;
			}

			CytoNode neighbour = null;
			if (statement.getSubject().isURIResource())
			{
				if (!resources.contains(statement.getSubject().getURI()))
				{
					neighbour = determineCytoEntityForResource(statement.getSubject(), model, externalResources, mergedModel);
					cytoEntities.add(neighbour);
					resources.add(statement.getSubject().getURI());
					resourcesAsNodes.add(statement.getSubject());
				}
				else
				{
					neighbour = getCytoNode(statement.getSubject(), cytoEntities);
				}
			}
			else
			{
				String id = statement.getSubject().asNode().getBlankNodeId().getLabelString();
				if (!resources.contains(id))
				{
					neighbour = determineCytoEntityForResource(statement.getSubject(), model, externalResources, mergedModel);
					cytoEntities.add(neighbour);
					resources.add(id);
					resourcesAsNodes.add(statement.getSubject());
				}
				else
				{
					neighbour = getCytoNode(statement.getSubject(), cytoEntities);
				}
			}

			CytoEdge cytoEdge = createCytoEdgeForLink(statement.getPredicate(), neighbour, cytoNode, model, externalResources, false);
			cytoEntities.add(cytoEdge);
			visited.add(statement);
			visitNeighbourStruct.setVisited(visited);
			visitNeighbourStruct.setResources(resources);
			visitNeighbourStruct.setCytoEntities(cytoEntities);

			visitNeighbourStruct = addEntitiesForNeighbours(statement.getSubject(), neighbour, model, externalResources, mergedModel, visitNeighbourStruct);
		}

		visitNeighbourStruct = addLiteralPropertiesForResource(resource, cytoNode, model, externalResources, visitNeighbourStruct);

		return visitNeighbourStruct;
	}

	private static CytoNode getCytoNode(Resource resource, Set<CytoEntity> cytoEntities)
	{
		for (Iterator<CytoEntity> iterator = cytoEntities.iterator(); iterator.hasNext();)
		{
			CytoEntity cytoEntity = iterator.next();
			if (cytoEntity instanceof CytoEdge)
			{
				continue;
			}

			CytoNode cytoNode = (CytoNode) cytoEntity;
			if (resource.isURIResource() && cytoNode.getData().getId().equals(resource.getURI()))
			{
				return cytoNode;
			}

			if (resource.isAnon() && cytoNode.getData().getId().equals(resource.asNode().getBlankNodeId().getLabelString()))
			{
				return cytoNode;
			}
		}

		return null;
	}

	private static Set<CytoEntity> addLiteralPropertiesForResource(Resource resource, CytoNode source, Model model, Model externalResources, Set<CytoEntity> entities)
	{
		CytoNode parentNode = null;

		StmtIterator stmtIterator = model.listStatements(resource, (Property) null, (RDFNode) null);

		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			if (!statement.getObject().isLiteral())
			{
				continue;
			}

			CytoNode cytoNode = null;
			if (Processor.useNestedNodes && parentNode == null)
			{
				String parentId = source.getData().getId() + "parent";
				if (!existsNode(entities, parentId))
				{
					String label = source.getData().getLabel();
					String popup = source.getData().getPopup();
					parentNode = new CytoNode(new CytoNodeData(parentId, label));
					parentNode.getData().setPopup(popup);
					parentNode.getData().setShape("rectangle");
					parentNode.getData().setBg(source.getData().getBg());
					entities.add(parentNode);
				}
				else
				{
					parentNode = getCytoNode(parentId, entities);
				}
				cytoNode = createCytoEntityForLiteral(statement.getObject().asLiteral(), parentNode.getData().getId());
			}
			else
			{
				cytoNode = createCytoEntityForLiteral(statement.getObject().asLiteral(), null);
			}

			CytoEdge cytoEdge = createCytoEdgeForLink(statement.getPredicate(), source, cytoNode, model, externalResources, true);
			entities.add(cytoNode);
			entities.add(cytoEdge);

			addDetailForLiteralProperty(source, entities, cytoNode, cytoEdge);
		}
		return entities;
	}

	private static VisitNeighbourStruct addLiteralPropertiesForResource(Resource resource, CytoNode source, Model model, Model externalResources, VisitNeighbourStruct visitNeighbourStruct)
	{
		Set<Statement> visited = visitNeighbourStruct.getVisited();
		Set<String> resources = visitNeighbourStruct.getResources();
		Set<CytoEntity> cytoEntities = visitNeighbourStruct.getCytoEntities();

		CytoNode parentNode = null;

		StmtIterator stmtIterator = model.listStatements(resource, (Property) null, (RDFNode) null);

		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			if (!statement.getObject().isLiteral())
			{
				continue;
			}

			if (visited.contains(statement))
			{
				continue;
			}

			CytoNode cytoNode = null;
			if (Processor.useNestedNodes && parentNode == null)
			{
				String parentId = source.getData().getId() + "parent";
				if (!existsNode(cytoEntities, parentId))
				{
					String label = source.getData().getLabel();
					String popup = source.getData().getPopup();
					parentNode = new CytoNode(new CytoNodeData(parentId, label));
					parentNode.getData().setPopup(popup);
					parentNode.getData().setShape("rectangle");
					parentNode.getData().setBg(source.getData().getBg());
					cytoEntities.add(parentNode);
				}
				else
				{
					parentNode = getCytoNode(parentId, cytoEntities);
				}
				cytoNode = createCytoEntityForLiteral(statement.getObject().asLiteral(), parentNode.getData().getId());
			}
			else
			{
				cytoNode = createCytoEntityForLiteral(statement.getObject().asLiteral(), null);
				Processor.log.info("creating node for literal statement: " + statement);
			}

			CytoEdge cytoEdge = createCytoEdgeForLink(statement.getPredicate(), source, cytoNode, model, externalResources, true);
			if (Processor.hideLiteralNodes)
			{
				String edgeLabel = cytoEdge.getData().getLabel();
				String sourcePopup = source.getData().getPopup();
				String targetPopup = cytoNode.getData().getLabel();
				if (edgeLabel != null && !edgeLabel.isEmpty() && sourcePopup != null && !sourcePopup.isEmpty() && targetPopup != null && !targetPopup.isEmpty())
				{
					sourcePopup = sourcePopup + "<br/><br/>" + edgeLabel + " " + targetPopup;
					source.getData().setPopup(sourcePopup);
					cytoEntities.remove(source);
					cytoEntities.add(source);
				}
			}
			cytoEntities.add(cytoNode);
			cytoEntities.add(cytoEdge);

			addDetailForLiteralProperty(source, cytoEntities, cytoNode, cytoEdge);

			visited.add(statement);
			visitNeighbourStruct.setVisited(visited);
			visitNeighbourStruct.setCytoEntities(cytoEntities);
			visitNeighbourStruct.setResources(resources);
		}
		return visitNeighbourStruct;
	}

	public static void addDetailForLiteralProperty(CytoNode source, Set<CytoEntity> cytoEntities, CytoNode cytoNode, CytoEdge cytoEdge)
	{
		String detail = source.getData().getDetail();
		detail = detail + "<br/><span class=\"literal\">" + cytoEdge.getData().getLabel() + "</span> " + cytoNode.getData().getLabel();
		source.getData().setDetail(detail);
		cytoEntities.add(source);
		return;
	}

	private static boolean existsNode(Set<CytoEntity> cytoEntities, String parentId)
	{
		for (Iterator<CytoEntity> iterator = cytoEntities.iterator(); iterator.hasNext();)
		{
			CytoEntity cytoEntity = iterator.next();
			if (cytoEntity instanceof CytoEdge)
			{
				continue;
			}

			CytoNode cytoNode = (CytoNode) cytoEntity;
			if (cytoNode.getData().getId().equals(parentId))
			{
				return true;
			}
		}
		return false;
	}

	private static CytoEdge createCytoEdgeForLink(Property predicate, CytoNode source, CytoNode target, Model model, Model externalResources, boolean isLinkToLiteral)
	{
		String label = createLinkLabel(model, externalResources, predicate);
		CytoEdge cytoEdge = new CytoEdge(new CytoEdgeData(Vocabulary.URN_UUID + UUID.randomUUID(), label, source.getData().getId(), target.getData().getId()));
		if (isLinkToLiteral && Processor.hideLiteralNodes)
		{
			cytoEdge.getData().setVisibility("hidden");
		}
		cytoEdge.getData().setDetail(label);
		cytoEdge.setPointsToLiteral(isLinkToLiteral);
		cytoEdge.getData().setIRI(predicate.getURI());
		cytoEdge = determineSuperType(cytoEdge, predicate, model, externalResources);
		return cytoEdge;
	}

	private static CytoEdge determineSuperType(CytoEdge cytoEdge, Property predicate, Model model, Model externalResources)
	{
		String propertySuperType = "TOP";
		if (predicate.getLocalName().equals(Vocabulary.BFO_HAS_PART.getLocalName()))
		{
			propertySuperType = Vocabulary.BFO_HAS_PART.getLocalName();
			cytoEdge.getData().setSupertype(propertySuperType);
			return cytoEdge;
		}
		else if (predicate.getLocalName().equals(Vocabulary.RO_HAS_PARTICIPANT.getLocalName()))
		{
			propertySuperType = Vocabulary.RO_HAS_PARTICIPANT.getLocalName();
			cytoEdge.getData().setSupertype(propertySuperType);
			return cytoEdge;
		}
		else if (predicate.getLocalName().equals(Vocabulary.IAO_IS_ABOUT.getLocalName()))
		{
			propertySuperType = Vocabulary.IAO_IS_ABOUT.getLocalName();
			cytoEdge.getData().setSupertype(propertySuperType);
			return cytoEdge;
		}
		else if (predicate.getLocalName().equals(Vocabulary.RO_TEMPORALLY_RELATED_TO.getLocalName()))
		{
			propertySuperType = Vocabulary.RO_TEMPORALLY_RELATED_TO.getLocalName();
			cytoEdge.getData().setSupertype(propertySuperType);
			return cytoEdge;
		}
		else if (predicate.getURI().startsWith(Vocabulary.QUDT_SCHEMA_PREFIX))
		{
			propertySuperType = Vocabulary.QUDT_SCHEMA_PREFIX;
			cytoEdge.getData().setSupertype(propertySuperType);
			return cytoEdge;
		}
		else if (predicate.getURI().startsWith(Vocabulary.SHACL_PREFIX))
		{
			propertySuperType = Vocabulary.SHACL_PREFIX;
			cytoEdge.getData().setSupertype(propertySuperType);
			return cytoEdge;
		}

		Set<Resource> superProperties = getSuperProperties(predicate);
		for (Resource superProperty : superProperties)
		{
			if (superProperty.getLocalName().equals(Vocabulary.BFO_HAS_PART.getLocalName()))
			{
				propertySuperType = Vocabulary.BFO_HAS_PART.getLocalName();
				cytoEdge.getData().setSupertype(propertySuperType);
				return cytoEdge;
			}
			else if (superProperty.getLocalName().equals(Vocabulary.RO_HAS_PARTICIPANT.getLocalName()))
			{
				propertySuperType = Vocabulary.RO_HAS_PARTICIPANT.getLocalName();
				cytoEdge.getData().setSupertype(propertySuperType);
				return cytoEdge;
			}
			else if (superProperty.getLocalName().equals(Vocabulary.IAO_IS_ABOUT.getLocalName()))
			{
				propertySuperType = Vocabulary.IAO_IS_ABOUT.getLocalName();
				cytoEdge.getData().setSupertype(propertySuperType);
				return cytoEdge;
			}
			else if (superProperty.getLocalName().equals(Vocabulary.RO_TEMPORALLY_RELATED_TO.getLocalName()))
			{
				propertySuperType = Vocabulary.RO_TEMPORALLY_RELATED_TO.getLocalName();
				cytoEdge.getData().setSupertype(propertySuperType);
				return cytoEdge;
			}
		}

		cytoEdge.getData().setSupertype(propertySuperType);
		return cytoEdge;
	}

	private static String createLinkLabel(Model model, Model externalResources, Resource resource)
	{
		if (resource.hasProperty(Vocabulary.SKOS_PREF_LABEL))
		{
			String title = model.listStatements(resource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).next().getLiteral().toString();
			title = addPrefix(title, resource);
			return title;
		}
		else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).hasNext())
		{
			String title = externalResources.listStatements(resource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).next().getLiteral().toString();
			title = addPrefix(title, resource);
			return title;
		}
		else if (resource.hasProperty(Vocabulary.DCT_TITLE))
		{
			String title = model.listStatements(resource, Vocabulary.DCT_TITLE, (RDFNode) null).next().getLiteral().toString();
			title = addPrefix(title, resource);
			return title;
		}
		else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.DCT_TITLE, (RDFNode) null).hasNext())
		{
			String title = externalResources.listStatements(resource, Vocabulary.DCT_TITLE, (RDFNode) null).next().getLiteral().toString();
			title = addPrefix(title, resource);
			return title;
		}
		else if (resource.hasProperty(Vocabulary.RDFS_LABEL))
		{
			String title = model.listStatements(resource, Vocabulary.RDFS_LABEL, (RDFNode) null).next().getLiteral().toString();
			title = addPrefix(title, resource);
			return title;
		}
		else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.RDFS_LABEL, (RDFNode) null).hasNext())
		{
			String title = externalResources.listStatements(resource, Vocabulary.RDFS_LABEL, (RDFNode) null).next().getLiteral().toString();
			title = addPrefix(title, resource);
			return title;
		}
		else
		{
			String prefix = RdfUtil.getNamespaceMap().get(resource.getNameSpace());
			if (prefix != null && !prefix.isEmpty())
			{
				return prefix + ":" + resource.getLocalName();
			}
			else
			{
				return resource.getURI();
			}
		}
	}

	private static CytoNode createCytoEntityForLiteral(Literal literal, String parentId)
	{
		String label = "\"" + literal.getLexicalForm() + "\"";
		String popup = literal.getDatatypeURI();
		CytoNode cytoNode = new CytoNode(new CytoNodeData(Vocabulary.URN_UUID + UUID.randomUUID(), label));
		cytoNode.getData().setPopup(popup);
		cytoNode.getData().setShape("rectangle");
		cytoNode.getData().setBg("#FFFFFF");
		cytoNode.getData().setBorderwidth("0px");
		if (Processor.hideLiteralNodes)
		{
			cytoNode.getData().setVisibility("hidden");
		}
		if (Processor.useNestedNodes && parentId != null)
		{
			cytoNode.getData().setParent(parentId);
		}
		cytoNode.getData().setDetail(literal.getLexicalForm());
		return cytoNode;
	}

	private static CytoNode determineCytoEntityForResource(Resource resource, Model model, Model externalResources, Model mergedModel)
	{
		String label = determineLabel(model, externalResources, resource);
		String popup = createPopup(model, externalResources, resource);
		CytoNode cytoNode = null;
		if (resource.isURIResource())
		{
			cytoNode = new CytoNode(new CytoNodeData(resource.getURI(), label));
		}
		else
		{
			cytoNode = new CytoNode(new CytoNodeData(resource.asNode().getBlankNodeId().getLabelString(), label));
		}

		cytoNode.getData().setPopup(popup);

		if (resource.hasProperty(Vocabulary.RDF_TYPE, Vocabulary.OWL_CLASS) || (resource.isURIResource() && (resource.getURI().startsWith(Vocabulary.AFO_PREFIX))))
		{
			cytoNode.getData().setShape("roundrectangle");
		}
		else
		{
			cytoNode.getData().setShape("rectangle");
		}

		cytoNode.getData().setBg(determineBackgroundColor(mergedModel, externalResources, resource));
		cytoNode.getData().setDetail("<span class=\"thelabel\">" + label + "</span>");
		return cytoNode;
	}

	public static String determineBackgroundColor(Model mergedModel, Model externalResources, Resource resource)
	{
		DomainEnum domain = determineDomain(mergedModel, resource);

		String bgColor = ColorScheme.domain2scheme.get(domain.name());
		return bgColor;
	}

	public static DomainEnum determineDomain(Model model, Resource resource)
	{
		DomainEnum domain = DomainEnum.OTHER;

		StmtIterator typeIterator = model.listStatements(resource, Vocabulary.RDF_TYPE, (RDFNode) null);
		while (typeIterator.hasNext())
		{
			Statement statement = typeIterator.next();
			Resource rdfType = statement.getResource();

			if (resource.isURIResource() && rdfType.equals(Vocabulary.OWL_CLASS))
			{
				// check resource itself if it is a class (ignoring anonymous classes)
				rdfType = resource;
			}

			if (Processor.visualizeOntology && resource.isURIResource() && rdfType.getURI().startsWith(Vocabulary.URN_UUID))
			{
				// check resource itself because this is an untangled type (type was replaced by uuid node)
				rdfType = resource;
			}

			if (rdfType.isAnon())
			{
				if (rdfType.hasProperty(Vocabulary.RDF_TYPE))
				{
					// handle nested rdf:type (such triples are bad practice but should be correctly parsed)
					StmtIterator nestedTypeIterator = model.listStatements(rdfType, Vocabulary.RDF_TYPE, (RDFNode) null);
					while (nestedTypeIterator.hasNext())
					{
						Statement nestedStatement = nestedTypeIterator.next();
						Resource nestedRdfType = nestedStatement.getResource();
						if (nestedRdfType.isAnon())
						{
							continue;
						}
						rdfType = nestedRdfType; // take the first nested type
						break;
					}
				}
				else
				{
					continue;
				}
			}

			if (rdfType.getURI().contains(Vocabulary.AFC_PREFIX) || (resource.isURIResource() && resource.getURI().startsWith(Vocabulary.URN_UUID) && resource.getURI().contains("AFC_")))
			{
				domain = DomainEnum.COMMON;
			}
			else if (rdfType.getURI().contains(Vocabulary.AFE_PREFIX) || (resource.isURIResource() && resource.getURI().startsWith(Vocabulary.URN_UUID) && resource.getURI().contains("AFE_")))
			{
				domain = DomainEnum.EQUIPMENT;
			}
			else if (rdfType.getURI().contains(Vocabulary.AFM_PREFIX) || (resource.isURIResource() && resource.getURI().startsWith(Vocabulary.URN_UUID) && resource.getURI().contains("AFM_")))
			{
				domain = DomainEnum.MATERIAL;
			}
			else if (rdfType.getURI().contains(Vocabulary.AFP_PREFIX) || (resource.isURIResource() && resource.getURI().startsWith(Vocabulary.URN_UUID) && resource.getURI().contains("AFP_")))
			{
				domain = DomainEnum.PROCESS;
			}
			else if (rdfType.getURI().contains(Vocabulary.AFR_PREFIX) || (resource.isURIResource() && resource.getURI().startsWith(Vocabulary.URN_UUID) && resource.getURI().contains("AFR_")))
			{
				domain = DomainEnum.RESULT;
			}
			else if (rdfType.getURI().contains(Vocabulary.AFQ_PREFIX) || (resource.isURIResource() && resource.getURI().startsWith(Vocabulary.URN_UUID) && resource.getURI().contains("AFQ_")))
			{
				domain = DomainEnum.QUALITY;
			}
			else if (rdfType.getURI().contains(Vocabulary.AFRL_PREFIX) || (resource.isURIResource() && resource.getURI().startsWith(Vocabulary.URN_UUID) && resource.getURI().contains("AFRL_")))
			{
				if (isSubClassOf(rdfType, Vocabulary.AFRL_CONTEXTUAL_ROLE, model, null))
				{
					domain = DomainEnum.CONTEXTUAL_ROLE;
				}
				else
				{
					domain = DomainEnum.REALIZABLE_ENTITY;
				}
			}
			else if (rdfType.getURI().contains(Vocabulary.AFX_PREFIX) || (resource.isURIResource() && resource.getURI().startsWith(Vocabulary.URN_UUID) && resource.getURI().contains("AFX_")))
			{
				domain = DomainEnum.PROPERTY;
			}
			else if (rdfType.getURI().contains(Vocabulary.QUDT_QUANTITY_EXT_PREFIX) || rdfType.getURI().contains(Vocabulary.QUDT_SCHEMA_EXT_PREFIX)
					|| rdfType.getURI().contains(Vocabulary.QUDT_SCHEMA_PREFIX) || rdfType.getURI().contains(Vocabulary.QUDT_UNIT_PREFIX) || rdfType.getURI().contains(Vocabulary.QUDT_UNIT_EXT_PREFIX))
			{
				if (isSubClassOf(rdfType, Vocabulary.OWL_OBJECT_PROPERTY, model, null) || isSubClassOf(rdfType, Vocabulary.OWL_DATATYPE_PROPERTY, model, null)
						|| isSubClassOf(rdfType, Vocabulary.OWL_ANNOTATION_PROPERTY, model, null))
				{
					domain = DomainEnum.PROPERTY;
				}
				else
				{
					domain = DomainEnum.INFORMATION;
				}
			}
			else if (rdfType.getURI().contains(Vocabulary.IAO_PREFIX))
			{
				if (isSubClassOf(rdfType, Vocabulary.OWL_OBJECT_PROPERTY, model, null) || isSubClassOf(rdfType, Vocabulary.OWL_DATATYPE_PROPERTY, model, null)
						|| isSubClassOf(rdfType, Vocabulary.OWL_ANNOTATION_PROPERTY, model, null))
				{
					domain = DomainEnum.PROPERTY;
				}
				else
				{
					domain = DomainEnum.INFORMATION;
				}
			}
			else if (rdfType.getURI().contains(Vocabulary.SHACL_NODE_SHAPE.getURI()))
			{
				domain = DomainEnum.EQUIPMENT;
			}
			else if (rdfType.getURI().contains(Vocabulary.SHACL_PROPERTY_SHAPE.getURI()))
			{
				domain = DomainEnum.CONTEXTUAL_ROLE;
			}
			else if (rdfType.getURI().equals(Vocabulary.OWL_OBJECT_PROPERTY.getURI()))
			{
				if (resource.hasProperty(Vocabulary.SKOS_RELATED))
				{
					Resource property = model.listStatements(resource, Vocabulary.SKOS_RELATED, (RDFNode) null).next().getResource();
					if (property.getNameSpace().equals(Vocabulary.AFX_PREFIX))
					{
						domain = DomainEnum.PROPERTY;
					}
				}
			}
			else
			{
				if (isSubClassOf(rdfType, Vocabulary.OWL_OBJECT_PROPERTY, model, null) || isSubClassOf(rdfType, Vocabulary.OWL_DATATYPE_PROPERTY, model, null)
						|| isSubClassOf(rdfType, Vocabulary.OWL_ANNOTATION_PROPERTY, model, null))
				{
					domain = DomainEnum.PROPERTY;
				}
				else if (isSubClassOf(rdfType, Vocabulary.BFO_OCCURRENT, model, null))
				{
					domain = DomainEnum.PROCESS;
				}
				else if (isSubClassOf(rdfType, Vocabulary.AFE_DEVICE, model, null) || isSubClassOf(rdfType, Vocabulary.OBI_DEVICE, model, null))
				{
					domain = DomainEnum.EQUIPMENT;
				}
				else if (isSubClassOf(rdfType, Vocabulary.BFO_MATERIAL, model, null) && !isSubClassOf(rdfType, Vocabulary.OBI_DEVICE, model, null))
				{
					domain = DomainEnum.MATERIAL;
				}
				else if (isSubClassOf(rdfType, Vocabulary.BFO_QUALITY, model, null))
				{
					domain = DomainEnum.QUALITY;
				}
				else if (isSubClassOf(rdfType, Vocabulary.BFO_ROLE, model, null) && !isSubClassOf(rdfType, Vocabulary.AFRL_CONTEXTUAL_ROLE, model, null))
				{
					domain = DomainEnum.REALIZABLE_ENTITY;
				}
				else if (isSubClassOf(rdfType, Vocabulary.BFO_FUNCTION, model, null))
				{
					domain = DomainEnum.REALIZABLE_ENTITY;
				}
				else if (isSubClassOf(rdfType, Vocabulary.IAO_INFORMATION_CONTENT_ENTITY, model, null))
				{
					domain = DomainEnum.INFORMATION;
				}
				else if (isSubClassOf(rdfType, Vocabulary.IAO_INFORMATION_CONTENT_ENTITY, model, null))
				{
					domain = DomainEnum.INFORMATION;
				}
				else if (isSubClassOf(rdfType, Vocabulary.SHACL_NODE_SHAPE, model, null))
				{
					domain = DomainEnum.EQUIPMENT;
				}
				else if (isSubClassOf(rdfType, Vocabulary.SHACL_PROPERTY_SHAPE, model, null))
				{
					domain = DomainEnum.CONTEXTUAL_ROLE;
				}
			}

			if (domain != DomainEnum.COMMON && domain != DomainEnum.OTHER)
			{
				break;
			}
		}
		return domain;
	}

	private static boolean isSubClassOf(Resource resource, Resource classType, Model model, List<Resource> visited)
	{
		if (resource.getURI().equals(classType.getURI()))
		{
			return true;
		}

		StmtIterator stmtIterator = model.listStatements(resource, Vocabulary.RDFS_SUBCLASS_OF, (RDFNode) null);
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			RDFNode parentClass = statement.getObject();
			if (parentClass.isAnon())
			{
				continue;
			}

			if (visited == null)
			{
				visited = new ArrayList<Resource>();
				visited.add(resource);
			}

			if (visited.contains(parentClass.asResource()))
			{
				continue;
			}

			visited.add(parentClass.asResource());
			if (classType.getURI().equals(parentClass.asResource().getURI()))
			{
				return true;
			}

			if (isSubClassOf(parentClass.asResource(), classType, model, visited))
			{
				return true;
			}
		}
		return false;
	}

	public static String createPopup(Model model, Model externalResources, Resource resource)
	{
		if ((resource.isURIResource() && (resource.getURI().startsWith(Vocabulary.URN_UUID + "AF") || resource.getURI().startsWith(Vocabulary.URN_UUID + "BFO")
				|| resource.getURI().startsWith(Vocabulary.URN_UUID + "IAO") || resource.getURI().startsWith(Vocabulary.URN_UUID + "OBI")))
				|| resource.hasProperty(Vocabulary.RDF_TYPE, Vocabulary.OWL_CLASS))
		{
			// this is a class or disguised class
			StringBuilder popupText = new StringBuilder();
			StmtIterator labelIterator = model.listStatements(resource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null);
			String label = StringUtils.EMPTY;
			boolean hasDefinition = false;
			while (labelIterator.hasNext())
			{
				label = labelIterator.next().getLiteral().getString();
			}
			if (label.isEmpty())
			{
				labelIterator = model.listStatements(resource, Vocabulary.RDFS_LABEL, (RDFNode) null);
				while (labelIterator.hasNext())
				{
					label = labelIterator.next().getLiteral().getString();
				}
			}
			if (label.isEmpty() && externalResources != null)
			{
				labelIterator = externalResources.listStatements(resource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null);
				while (labelIterator.hasNext())
				{
					label = labelIterator.next().getLiteral().getString();
				}
			}
			if (label.isEmpty() && externalResources != null)
			{
				labelIterator = externalResources.listStatements(resource, Vocabulary.RDFS_LABEL, (RDFNode) null);
				while (labelIterator.hasNext())
				{
					label = labelIterator.next().getLiteral().getString();
				}
			}

			StmtIterator definitionIterator = model.listStatements(resource, Vocabulary.SKOS_DEFINITION, (RDFNode) null);
			String definition = StringUtils.EMPTY;
			while (definitionIterator.hasNext())
			{
				definition = definitionIterator.next().getLiteral().getString();
				hasDefinition = true;
			}
			if (definition.isEmpty())
			{
				definitionIterator = model.listStatements(resource, Vocabulary.OBO_DEFINITION, (RDFNode) null);
				while (definitionIterator.hasNext())
				{
					definition = definitionIterator.next().getLiteral().getString();
				}
			}
			if (definition.isEmpty())
			{
				definitionIterator = model.listStatements(resource, Vocabulary.OBO_DEFINITION_DISGUISED, (RDFNode) null);
				while (definitionIterator.hasNext())
				{
					definition = definitionIterator.next().getLiteral().getString();
				}
			}
			if (definition.isEmpty() && externalResources != null)
			{
				definitionIterator = externalResources.listStatements(resource, Vocabulary.SKOS_DEFINITION, (RDFNode) null);
				while (definitionIterator.hasNext())
				{
					definition = definitionIterator.next().getLiteral().getString();
				}
			}
			if (definition.isEmpty() && externalResources != null)
			{
				definitionIterator = externalResources.listStatements(resource, Vocabulary.OBO_DEFINITION, (RDFNode) null);
				while (definitionIterator.hasNext())
				{
					definition = definitionIterator.next().getLiteral().getString();
				}
			}
			if (definition.isEmpty() && externalResources != null)
			{
				definitionIterator = externalResources.listStatements(resource, Vocabulary.OBO_DEFINITION_DISGUISED, (RDFNode) null);
				while (definitionIterator.hasNext())
				{
					definition = definitionIterator.next().getLiteral().getString();
				}
			}

			if (!label.isEmpty())
			{
				popupText.append("prefLabel: <br/>" + label + "<br/><br/>");
			}
			if (!definition.isEmpty())
			{
				popupText.append("definition: <br/>" + definition + "<br/><br/>");
			}

			if (popupText.length() == 0 && !hasDefinition && model.listStatements(resource, Vocabulary.RDF_TYPE, Vocabulary.OWL_NAMED_INDIVIDUAL).hasNext())
			{
				popupText.append("Node is a named individual.");
			}

			if (resource.hasProperty(Vocabulary.DCT_DESCRIPTION))
			{
				popupText.append("description: <br/>" + model.listStatements(resource, Vocabulary.DCT_DESCRIPTION, (RDFNode) null).next().getLiteral().toString() + "<br/><br/>");
			}
			else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.DCT_DESCRIPTION, (RDFNode) null).hasNext())
			{
				popupText.append("description: <br/>" + externalResources.listStatements(resource, Vocabulary.DCT_DESCRIPTION, (RDFNode) null).next().getLiteral().toString() + "<br/><br/>");
			}

			if (resource.hasProperty(Vocabulary.OBO_ELUCIDATION))
			{
				StmtIterator exampleIterator = model.listStatements(resource, Vocabulary.OBO_ELUCIDATION, (RDFNode) null);
				while (exampleIterator.hasNext())
				{
					Statement statement = exampleIterator.next();
					popupText.append("elucidation: <br/>" + statement.getLiteral().toString() + "<br/><br/>");
				}
			}
			else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.OBO_ELUCIDATION, (RDFNode) null).hasNext())
			{
				StmtIterator exampleIterator = externalResources.listStatements(resource, Vocabulary.OBO_ELUCIDATION, (RDFNode) null);
				while (exampleIterator.hasNext())
				{
					Statement statement = exampleIterator.next();
					popupText.append("elucidation: <br/>" + statement.getLiteral().toString() + "<br/><br/>");
				}
			}

			if (resource.hasProperty(Vocabulary.OBO_ELUCIDATION_DISGUISED))
			{
				StmtIterator exampleIterator = model.listStatements(resource, Vocabulary.OBO_ELUCIDATION_DISGUISED, (RDFNode) null);
				while (exampleIterator.hasNext())
				{
					Statement statement = exampleIterator.next();
					popupText.append("elucidation: <br/>" + statement.getLiteral().toString() + "<br/><br/>");
				}
			}
			else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.OBO_ELUCIDATION_DISGUISED, (RDFNode) null).hasNext())
			{
				StmtIterator exampleIterator = externalResources.listStatements(resource, Vocabulary.OBO_ELUCIDATION_DISGUISED, (RDFNode) null);
				while (exampleIterator.hasNext())
				{
					Statement statement = exampleIterator.next();
					popupText.append("elucidation: <br/>" + statement.getLiteral().toString() + "<br/><br/>");
				}
			}

			if (resource.hasProperty(Vocabulary.OBO_EXAMPLE))
			{
				StmtIterator exampleIterator = model.listStatements(resource, Vocabulary.OBO_EXAMPLE, (RDFNode) null);
				while (exampleIterator.hasNext())
				{
					Statement statement = exampleIterator.next();
					popupText.append("example: <br/>" + statement.getLiteral().toString() + "<br/><br/>");
				}
			}
			else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.OBO_EXAMPLE, (RDFNode) null).hasNext())
			{
				StmtIterator exampleIterator = externalResources.listStatements(resource, Vocabulary.OBO_EXAMPLE, (RDFNode) null);
				while (exampleIterator.hasNext())
				{
					Statement statement = exampleIterator.next();
					popupText.append("example: <br/>" + statement.getLiteral().toString() + "<br/><br/>");
				}
			}

			if (resource.hasProperty(Vocabulary.OBO_EXAMPLE_DISGUISED))
			{
				StmtIterator exampleIterator = model.listStatements(resource, Vocabulary.OBO_EXAMPLE_DISGUISED, (RDFNode) null);
				while (exampleIterator.hasNext())
				{
					Statement statement = exampleIterator.next();
					popupText.append("example: <br/>" + statement.getLiteral().toString() + "<br/><br/>");
				}
			}
			else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.OBO_EXAMPLE_DISGUISED, (RDFNode) null).hasNext())
			{
				StmtIterator exampleIterator = externalResources.listStatements(resource, Vocabulary.OBO_EXAMPLE_DISGUISED, (RDFNode) null);
				while (exampleIterator.hasNext())
				{
					Statement statement = exampleIterator.next();
					popupText.append("example: <br/>" + statement.getLiteral().toString() + "<br/><br/>");
				}
			}

			if (popupText.length() > 0)
			{
				String namespacePrefix = RdfUtil.getNamespaceMap().get(resource.getNameSpace());
				String id = StringUtils.EMPTY;
				if (namespacePrefix != null && !namespacePrefix.isEmpty())
				{
					id = namespacePrefix + ":";
				}
				id = id + resource.getLocalName() + "<br/><br/>";
				popupText.insert(0, id);
				// return NumericEntityEscaper.below(0x20).translate(StringEscapeUtils.escapeXml11(popupText.toString()));
				return popupText.toString();
			}
		}
		else if (resource.hasProperty(Vocabulary.RDF_TYPE))
		{
			StringBuilder popupText = new StringBuilder();
			StmtIterator parentTypeIterator = model.listStatements(resource, Vocabulary.RDF_TYPE, (RDFNode) null);
			boolean hasParentTypeWithDefinition = false;
			while (parentTypeIterator.hasNext())
			{
				Resource parentType = parentTypeIterator.next().getObject().asResource();
				StmtIterator parentTypeLabelIterator = model.listStatements(parentType, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null);
				String parentTypeLabel = StringUtils.EMPTY;
				while (parentTypeLabelIterator.hasNext())
				{
					parentTypeLabel = parentTypeLabelIterator.next().getLiteral().getString();
				}
				StmtIterator parentTypeDefinitionIterator = model.listStatements(parentType, Vocabulary.SKOS_DEFINITION, (RDFNode) null);
				String parentTypeDefinition = StringUtils.EMPTY;
				while (parentTypeDefinitionIterator.hasNext())
				{
					parentTypeDefinition = parentTypeDefinitionIterator.next().getLiteral().getString();
					hasParentTypeWithDefinition = true;
					String id = RdfUtil.getNamespaceMap().get(parentType.getNameSpace()) + ":" + parentType.getLocalName() + "<br/><br/>";
					popupText.append(id);
				}
				if (!parentTypeLabel.isEmpty())
				{
					popupText.append("class prefLabel: <br/>" + parentTypeLabel + "<br/><br/>");
				}
				else
				{
					StmtIterator parentTypeRdfsLabelIterator = model.listStatements(parentType, Vocabulary.RDFS_LABEL, (RDFNode) null);
					while (parentTypeRdfsLabelIterator.hasNext())
					{
						parentTypeLabel = parentTypeRdfsLabelIterator.next().getLiteral().getString();
					}
					if (parentTypeLabel.isEmpty())
					{
						if (externalResources != null && externalResources.listStatements(parentType, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).hasNext())
						{
							parentTypeLabel = externalResources.listStatements(parentType, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).next().getString();
						}
					}
					if (!parentTypeLabel.isEmpty())
					{
						if (externalResources != null && externalResources.listStatements(parentType, Vocabulary.RDFS_LABEL, (RDFNode) null).hasNext())
						{
							parentTypeLabel = externalResources.listStatements(parentType, Vocabulary.RDFS_LABEL, (RDFNode) null).next().getString();
						}
					}
					if (!parentTypeLabel.isEmpty())
					{
						popupText.append("class prefLabel: <br/>" + parentTypeLabel + "<br/><br/>");
					}
				}
				if (!parentTypeDefinition.isEmpty())
				{
					popupText.append("class definition: <br/>" + parentTypeDefinition + "<br/><br/>");
				}
				else
				{
					StmtIterator parentTypeOboDefinitionIterator = model.listStatements(parentType, Vocabulary.OBO_DEFINITION, (RDFNode) null);
					while (parentTypeOboDefinitionIterator.hasNext())
					{
						parentTypeDefinition = parentTypeOboDefinitionIterator.next().getLiteral().getString();
					}
					if (parentTypeDefinition.isEmpty())
					{
						if (externalResources != null && externalResources.listStatements(parentType, Vocabulary.SKOS_DEFINITION, (RDFNode) null).hasNext())
						{
							parentTypeDefinition = externalResources.listStatements(parentType, Vocabulary.SKOS_DEFINITION, (RDFNode) null).next().getString();
						}
					}
					if (parentTypeDefinition.isEmpty())
					{
						if (externalResources != null && externalResources.listStatements(parentType, Vocabulary.OBO_DEFINITION, (RDFNode) null).hasNext())
						{
							parentTypeDefinition = externalResources.listStatements(parentType, Vocabulary.OBO_DEFINITION, (RDFNode) null).next().getString();
						}
					}
					if (!parentTypeDefinition.isEmpty())
					{
						popupText.append("class definition: <br/>" + parentTypeDefinition + "<br/><br/>");
					}
				}
			}

			if (resource.hasProperty(Vocabulary.DCT_DESCRIPTION) && model.listStatements(resource, Vocabulary.DCT_DESCRIPTION, (RDFNode) null).hasNext())
			{
				popupText.append("description: <br/>" + model.listStatements(resource, Vocabulary.DCT_DESCRIPTION, (RDFNode) null).next().getLiteral().toString());
			}
			else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.DCT_DESCRIPTION, (RDFNode) null).hasNext())
			{
				popupText.append("description: <br/>" + externalResources.listStatements(resource, Vocabulary.DCT_DESCRIPTION, (RDFNode) null).next().getLiteral().toString());
			}

			if (popupText.length() == 0 && !hasParentTypeWithDefinition && (model.listStatements(resource, Vocabulary.RDF_TYPE, Vocabulary.OWL_NAMED_INDIVIDUAL).hasNext()
					|| (externalResources != null && externalResources.listStatements(resource, Vocabulary.RDF_TYPE, Vocabulary.OWL_NAMED_INDIVIDUAL).hasNext())))
			{
				popupText.append("Concept is a named individual.");
			}

			if (popupText.length() == 0 && !hasParentTypeWithDefinition && model.listStatements(resource, Vocabulary.OBO_DEFINITION_DISGUISED, (RDFNode) null).hasNext())
			{
				popupText.append(model.listStatements(resource, Vocabulary.OBO_DEFINITION_DISGUISED, (RDFNode) null).next().getString());
			}
			else if (popupText.length() == 0 && !hasParentTypeWithDefinition
					&& (externalResources != null && externalResources.listStatements(resource, Vocabulary.OBO_DEFINITION_DISGUISED, (RDFNode) null).hasNext()))
			{
				popupText.append(externalResources.listStatements(resource, Vocabulary.OBO_DEFINITION_DISGUISED, (RDFNode) null).next().getString());
			}

			if (popupText.length() > 0)
			{
				// return NumericEntityEscaper.below(0x20).translate(StringEscapeUtils.escapeXml11(popupText.toString()));
				return popupText.toString();
			}
		}
		else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.RDF_TYPE, (RDFNode) null).hasNext())
		{
			StringBuilder popupText = new StringBuilder();
			StmtIterator parentTypeIterator = externalResources.listStatements(resource, Vocabulary.RDF_TYPE, (RDFNode) null);
			boolean hasParentTypeWithDefinition = false;
			while (parentTypeIterator.hasNext())
			{
				Resource parentType = parentTypeIterator.next().getObject().asResource();
				StmtIterator parentTypeLabelIterator = externalResources.listStatements(parentType, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null);
				String parentTypeLabel = StringUtils.EMPTY;
				while (parentTypeLabelIterator.hasNext())
				{
					parentTypeLabel = parentTypeLabelIterator.next().getLiteral().getString();
				}
				StmtIterator parentTypeDefinitionIterator = externalResources.listStatements(parentType, Vocabulary.SKOS_DEFINITION, (RDFNode) null);
				String parentTypeDefinition = StringUtils.EMPTY;
				while (parentTypeDefinitionIterator.hasNext())
				{
					parentTypeDefinition = parentTypeDefinitionIterator.next().getLiteral().getString();
					hasParentTypeWithDefinition = true;
					String id = RdfUtil.getNamespaceMap().get(parentType.getNameSpace()) + ":" + parentType.getLocalName() + "<br/><br/>";
					popupText.append(id);
				}
				if (!parentTypeLabel.isEmpty())
				{
					popupText.append("class prefLabel: <br/>" + parentTypeLabel + "<br/><br/>");
				}
				else
				{
					StmtIterator parentTypeRdfsLabelIterator = externalResources.listStatements(parentType, Vocabulary.RDFS_LABEL, (RDFNode) null);
					while (parentTypeRdfsLabelIterator.hasNext())
					{
						parentTypeLabel = parentTypeRdfsLabelIterator.next().getLiteral().getString();
					}
					if (!parentTypeLabel.isEmpty())
					{
						popupText.append("class prefLabel: <br/>" + parentTypeLabel + "<br/><br/>");
					}
				}
				if (!parentTypeDefinition.isEmpty())
				{
					popupText.append("class definition: <br/>" + parentTypeDefinition + "<br/><br/>");
				}
				else
				{
					StmtIterator parentTypeOboDefinitionIterator = externalResources.listStatements(parentType, Vocabulary.OBO_DEFINITION, (RDFNode) null);
					while (parentTypeOboDefinitionIterator.hasNext())
					{
						parentTypeDefinition = parentTypeOboDefinitionIterator.next().getLiteral().getString();
					}
					if (!parentTypeDefinition.isEmpty())
					{
						popupText.append("class definition: <br/>" + parentTypeDefinition + "<br/><br/>");
					}
				}
			}

			if (externalResources.listStatements(resource, Vocabulary.DCT_DESCRIPTION, (RDFNode) null).hasNext())
			{
				popupText.append("description: <br/>" + externalResources.listStatements(resource, Vocabulary.DCT_DESCRIPTION, (RDFNode) null).next().getLiteral().toString());
			}

			if (popupText.length() == 0 && !hasParentTypeWithDefinition && (model.listStatements(resource, Vocabulary.RDF_TYPE, Vocabulary.OWL_NAMED_INDIVIDUAL).hasNext()
					|| externalResources.listStatements(resource, Vocabulary.RDF_TYPE, Vocabulary.OWL_NAMED_INDIVIDUAL).hasNext()))
			{
				popupText.append("Concept is a named individual.");
			}

			if (popupText.length() == 0 && !hasParentTypeWithDefinition && model.listStatements(resource, Vocabulary.OBO_DEFINITION_DISGUISED, (RDFNode) null).hasNext())
			{
				popupText.append(model.listStatements(resource, Vocabulary.OBO_DEFINITION_DISGUISED, (RDFNode) null).next().getString());
			}
			else if (popupText.length() == 0 && !hasParentTypeWithDefinition && externalResources.listStatements(resource, Vocabulary.OBO_DEFINITION_DISGUISED, (RDFNode) null).hasNext())
			{
				popupText.append(externalResources.listStatements(resource, Vocabulary.OBO_DEFINITION_DISGUISED, (RDFNode) null).next().getString());
			}

			if (popupText.length() > 0)
			{
				// return NumericEntityEscaper.below(0x20).translate(StringEscapeUtils.escapeXml11(popupText.toString()));
				return popupText.toString();
			}
		}
		else if (resource.hasProperty(Vocabulary.SKOS_DEFINITION))
		{
			return "definition: <br/>" + model.listStatements(resource, Vocabulary.SKOS_DEFINITION, (RDFNode) null).next().getLiteral().toString();
		}
		else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.SKOS_DEFINITION, (RDFNode) null).hasNext())
		{
			return "definition: <br/>" + externalResources.listStatements(resource, Vocabulary.SKOS_DEFINITION, (RDFNode) null).next().getLiteral().toString();
		}
		else if (resource.hasProperty(Vocabulary.DCT_DESCRIPTION))
		{
			return "description: <br/>" + model.listStatements(resource, Vocabulary.DCT_DESCRIPTION, (RDFNode) null).next().getLiteral().toString();
		}
		else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.DCT_DESCRIPTION, (RDFNode) null).hasNext())
		{
			return "description: <br/>" + externalResources.listStatements(resource, Vocabulary.DCT_DESCRIPTION, (RDFNode) null).next().getLiteral().toString();
		}
		else if (resource.hasProperty(Vocabulary.OBO_DEFINITION))
		{
			return "obo:definition: <br/>" + model.listStatements(resource, Vocabulary.OBO_DEFINITION, (RDFNode) null).next().getLiteral().toString();
		}
		else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.OBO_DEFINITION, (RDFNode) null).hasNext())
		{
			return "obo:definition: <br/>" + externalResources.listStatements(resource, Vocabulary.OBO_DEFINITION, (RDFNode) null).next().getLiteral().toString();
		}
		else if (resource.hasProperty(Vocabulary.OBO_DEFINITION_DISGUISED))
		{
			return "obo:definition: <br/>" + model.listStatements(resource, Vocabulary.OBO_DEFINITION_DISGUISED, (RDFNode) null).next().getLiteral().toString();
		}
		else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.OBO_DEFINITION_DISGUISED, (RDFNode) null).hasNext())
		{
			return "obo:definition: <br/>" + externalResources.listStatements(resource, Vocabulary.OBO_DEFINITION_DISGUISED, (RDFNode) null).next().getLiteral().toString();
		}
		else if (resource.hasProperty(Vocabulary.OBO_ELUCIDATION))
		{
			return "obo:elucidation: <br/>" + model.listStatements(resource, Vocabulary.OBO_ELUCIDATION, (RDFNode) null).next().getLiteral().toString();
		}
		else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.OBO_ELUCIDATION, (RDFNode) null).hasNext())
		{
			return "obo:elucidation: <br/>" + externalResources.listStatements(resource, Vocabulary.OBO_ELUCIDATION, (RDFNode) null).next().getLiteral().toString();
		}
		else if (resource.hasProperty(Vocabulary.OBO_ELUCIDATION_DISGUISED))
		{
			return "obo:elucidation: \n" + model.listStatements(resource, Vocabulary.OBO_ELUCIDATION_DISGUISED, (RDFNode) null).next().getLiteral().toString();
		}
		else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.OBO_ELUCIDATION_DISGUISED, (RDFNode) null).hasNext())
		{
			return "obo:elucidation: \n" + externalResources.listStatements(resource, Vocabulary.OBO_ELUCIDATION_DISGUISED, (RDFNode) null).next().getLiteral().toString();
		}

		if (resource.isURIResource())
		{
			return resource.getURI();
		}

		return "Blank node " + resource.asNode().getBlankNodeId().getLabelString();
	}

	public static String addPrefix(String labelString, Resource resource)
	{
		if (!resource.isURIResource())
		{
			return labelString;
		}
		String prefix = RdfUtil.getNamespaceMap().get(resource.getNameSpace());
		if (prefix != null && !prefix.isEmpty())
		{
			if (prefix.equals("obo"))
			{
				prefix = getPrefixForOboTermLabel(resource.getLocalName());
			}
			labelString = prefix.trim() + ":" + labelString.trim();
		}
		return labelString;
	}

	public static String getPrefixForOboTermLabel(String name)
	{
		String prefix;
		if (name.startsWith("BFO"))
		{
			prefix = "bfo";
		}
		else if (name.startsWith("IAO"))
		{
			prefix = "iao";
		}
		else if (name.startsWith("OBI"))
		{
			prefix = "obi";
		}
		else if (name.startsWith("RO"))
		{
			prefix = "ro";
		}
		else if (name.startsWith("CHEBI"))
		{
			prefix = "chebi";
		}
		else if (name.startsWith("PATO"))
		{
			prefix = "pato";
		}
		else if (name.contains("_"))
		{
			prefix = name.split("_")[0].toLowerCase();
		}
		else
		{
			prefix = "obo";
		}
		return prefix;
	}

	public static String determineLabel(Model model, Model externalResources, Resource resource)
	{
		if (resource.isAnon())
		{
			if (resource.hasProperty(Vocabulary.DCT_TITLE))
			{
				String label = model.listStatements(resource, Vocabulary.DCT_TITLE, (RDFNode) null).next().getLiteral().toString();
				return "[" + label + "]";
			}
			else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.DCT_TITLE, (RDFNode) null).hasNext())
			{
				String label = externalResources.listStatements(resource, Vocabulary.DCT_TITLE, (RDFNode) null).next().getLiteral().toString();
				return "[" + label + "]";
			}
			else if (resource.hasProperty(Vocabulary.RDF_TYPE))
			{
				StmtIterator stmtIterator = model.listStatements(resource, Vocabulary.RDF_TYPE, (RDFNode) null);
				boolean hasTypeLabel = false;
				Set<String> titles = new HashSet<String>();
				while (stmtIterator.hasNext())
				{
					String title = "";
					Statement statement = stmtIterator.next();
					Resource typeResource = statement.getResource();
					if (typeResource.hasProperty(Vocabulary.DCT_TITLE))
					{
						title = title + " " + model.listStatements(typeResource, Vocabulary.DCT_TITLE, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, typeResource);
						hasTypeLabel = true;
						titles.add(title.trim());
					}
					else if (externalResources != null && externalResources.listStatements(typeResource, Vocabulary.DCT_TITLE, (RDFNode) null).hasNext())
					{
						title = title + " " + externalResources.listStatements(typeResource, Vocabulary.DCT_TITLE, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, typeResource);
						hasTypeLabel = true;
						titles.add(title.trim());
					}
					else if (typeResource.hasProperty(Vocabulary.DCT_IDENTIFIER))
					{
						title = title + " " + model.listStatements(typeResource, Vocabulary.DCT_IDENTIFIER, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, typeResource);
						hasTypeLabel = true;
						titles.add(title.trim());
					}
					else if (externalResources != null && externalResources.listStatements(typeResource, Vocabulary.DCT_IDENTIFIER, (RDFNode) null).hasNext())
					{
						title = title + " " + externalResources.listStatements(typeResource, Vocabulary.DCT_IDENTIFIER, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, typeResource);
						hasTypeLabel = true;
						titles.add(title.trim());
					}
					else if (typeResource.hasProperty(Vocabulary.RDFS_LABEL))
					{
						title = title + " " + model.listStatements(typeResource, Vocabulary.RDFS_LABEL, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, typeResource);
						hasTypeLabel = true;
						titles.add(title.trim());
					}
					else if (externalResources != null && externalResources.listStatements(typeResource, Vocabulary.RDFS_LABEL, (RDFNode) null).hasNext())
					{
						title = title + " " + externalResources.listStatements(typeResource, Vocabulary.RDFS_LABEL, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, typeResource);
						hasTypeLabel = true;
						titles.add(title.trim());
					}
					else if (typeResource.hasProperty(Vocabulary.SKOS_PREF_LABEL))
					{
						title = title + " " + model.listStatements(typeResource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, typeResource);
						hasTypeLabel = true;
						titles.add(title.trim());
					}
					else if (externalResources != null && externalResources.listStatements(typeResource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).hasNext())
					{
						title = title + " " + externalResources.listStatements(typeResource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, typeResource);
						hasTypeLabel = true;
						titles.add(title.trim());
					}
					else
					{
						title = title + " " + typeResource.getLocalName();
						title = addPrefix(title, typeResource);
						hasTypeLabel = true;
						titles.add(title.trim());
					}
				}

				if (hasTypeLabel)
				{
					return "[" + StringUtils.join(titles, ", ") + "]";
				}
				else
				{
					return resource.getId().getBlankNodeId().getLabelString();
				}
			}
			else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.RDF_TYPE, (RDFNode) null).hasNext())
			{
				StmtIterator stmtIterator = externalResources.listStatements(resource, Vocabulary.RDF_TYPE, (RDFNode) null);
				boolean hasTypeLabel = false;
				Set<String> titles = new HashSet<String>();
				while (stmtIterator.hasNext())
				{
					String title = "";
					Statement statement = stmtIterator.next();
					Resource typeResource = statement.getResource();
					if (typeResource.hasProperty(Vocabulary.DCT_TITLE))
					{
						title = title + " " + externalResources.listStatements(typeResource, Vocabulary.DCT_TITLE, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, typeResource);
						hasTypeLabel = true;
						titles.add(title.trim());
					}
					else if (typeResource.hasProperty(Vocabulary.DCT_IDENTIFIER))
					{
						title = title + " " + externalResources.listStatements(typeResource, Vocabulary.DCT_IDENTIFIER, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, typeResource);
						hasTypeLabel = true;
						titles.add(title.trim());
					}
					else if (typeResource.hasProperty(Vocabulary.RDFS_LABEL))
					{
						title = title + " " + externalResources.listStatements(typeResource, Vocabulary.RDFS_LABEL, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, typeResource);
						hasTypeLabel = true;
						titles.add(title.trim());
					}
					else if (typeResource.hasProperty(Vocabulary.SKOS_PREF_LABEL))
					{
						title = title + " " + externalResources.listStatements(typeResource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, typeResource);
						hasTypeLabel = true;
						titles.add(title.trim());
					}
					else
					{
						title = title + " " + typeResource.getLocalName();
						title = addPrefix(title, typeResource);
						hasTypeLabel = true;
						titles.add(title.trim());
					}
				}

				if (hasTypeLabel)
				{
					return "[" + StringUtils.join(titles, ", ") + "]";
				}
				else
				{
					return resource.getId().getBlankNodeId().getLabelString();
				}
			}
		}
		else if (model.listStatements(resource, Vocabulary.DCT_TITLE, (RDFNode) null).hasNext())
		{
			String title = model.listStatements(resource, Vocabulary.DCT_TITLE, (RDFNode) null).next().getLiteral().toString();
			title = addPrefix(title, resource);
			return title;
		}
		else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.DCT_TITLE, (RDFNode) null).hasNext())
		{
			String title = externalResources.listStatements(resource, Vocabulary.DCT_TITLE, (RDFNode) null).next().getLiteral().toString();
			title = addPrefix(title, resource);
			return title;
		}
		else if (model.listStatements(resource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).hasNext())
		{
			String title = model.listStatements(resource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).next().getLiteral().toString();
			title = addPrefix(title, resource);
			return title;
		}
		else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).hasNext())
		{
			String title = externalResources.listStatements(resource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).next().getLiteral().toString();
			title = addPrefix(title, resource);
			return title;
		}
		else if (model.listStatements(resource, Vocabulary.DCT_IDENTIFIER, (RDFNode) null).hasNext())
		{
			String title = model.listStatements(resource, Vocabulary.DCT_IDENTIFIER, (RDFNode) null).next().getLiteral().toString();
			title = addPrefix(title, resource);
			return title;
		}
		else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.DCT_IDENTIFIER, (RDFNode) null).hasNext())
		{
			String title = externalResources.listStatements(resource, Vocabulary.DCT_IDENTIFIER, (RDFNode) null).next().getLiteral().toString();
			title = addPrefix(title, resource);
			return title;
		}
		else if (model.listStatements(resource, Vocabulary.RDFS_LABEL, (RDFNode) null).hasNext())
		{
			String title = model.listStatements(resource, Vocabulary.RDFS_LABEL, (RDFNode) null).next().getLiteral().toString();
			title = addPrefix(title, resource);
			return title;
		}
		else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.RDFS_LABEL, (RDFNode) null).hasNext())
		{
			String title = externalResources.listStatements(resource, Vocabulary.RDFS_LABEL, (RDFNode) null).next().getLiteral().toString();
			title = addPrefix(title, resource);
			return title;
		}
		else if (model.listStatements(resource, Vocabulary.QUDT_SYMBOL, (RDFNode) null).hasNext())
		{
			String title = model.listStatements(resource, Vocabulary.QUDT_SYMBOL, (RDFNode) null).next().getLiteral().toString();
			title = addPrefix(title, resource);
			return title;
		}
		else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.QUDT_SYMBOL, (RDFNode) null).hasNext())
		{
			String title = externalResources.listStatements(resource, Vocabulary.QUDT_SYMBOL, (RDFNode) null).next().getLiteral().toString();
			title = addPrefix(title, resource);
			return title;
		}
		else if (model.listStatements(resource, Vocabulary.RDF_TYPE, (RDFNode) null).hasNext())
		{
			StmtIterator stmtIterator = model.listStatements(resource, Vocabulary.RDF_TYPE, (RDFNode) null);
			boolean hasTypeLabel = false;
			Set<String> titles = new HashSet<String>();
			while (stmtIterator.hasNext())
			{
				String title = StringUtils.EMPTY;
				Statement statement = stmtIterator.next();
				Resource typeResource = statement.getResource();
				if (typeResource.equals(Vocabulary.OWL_NAMED_INDIVIDUAL))
				{
					continue;
				}
				else if (typeResource.hasProperty(Vocabulary.DCT_TITLE))
				{
					title = model.listStatements(typeResource, Vocabulary.DCT_TITLE, (RDFNode) null).next().getLiteral().toString();
					title = addPrefix(title, typeResource);
					titles.add(title);
					hasTypeLabel = true;
				}
				else if (externalResources != null && externalResources.listStatements(typeResource, Vocabulary.DCT_TITLE, (RDFNode) null).hasNext())
				{
					title = externalResources.listStatements(typeResource, Vocabulary.DCT_TITLE, (RDFNode) null).next().getLiteral().toString();
					title = addPrefix(title, typeResource);
					titles.add(title);
					hasTypeLabel = true;
				}
				else if (model.listStatements(typeResource, Vocabulary.DCT_IDENTIFIER, (RDFNode) null).hasNext())
				{
					title = model.listStatements(typeResource, Vocabulary.DCT_IDENTIFIER, (RDFNode) null).next().getLiteral().toString();
					title = addPrefix(title, typeResource);
					titles.add(title);
					hasTypeLabel = true;
				}
				else if (externalResources != null && externalResources.listStatements(typeResource, Vocabulary.DCT_IDENTIFIER, (RDFNode) null).hasNext())
				{
					title = externalResources.listStatements(typeResource, Vocabulary.DCT_IDENTIFIER, (RDFNode) null).next().getLiteral().toString();
					title = addPrefix(title, typeResource);
					titles.add(title);
					hasTypeLabel = true;
				}
				else if (model.listStatements(typeResource, Vocabulary.RDFS_LABEL, (RDFNode) null).hasNext())
				{
					title = model.listStatements(typeResource, Vocabulary.RDFS_LABEL, (RDFNode) null).next().getLiteral().toString();
					title = addPrefix(title, typeResource);
					titles.add(title);
					hasTypeLabel = true;
				}
				else if (externalResources != null && externalResources.listStatements(typeResource, Vocabulary.RDFS_LABEL, (RDFNode) null).hasNext())
				{
					title = externalResources.listStatements(typeResource, Vocabulary.RDFS_LABEL, (RDFNode) null).next().getLiteral().toString();
					title = addPrefix(title, typeResource);
					titles.add(title);
					hasTypeLabel = true;
				}
				else if (model.listStatements(typeResource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).hasNext())
				{
					title = model.listStatements(typeResource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).next().getLiteral().toString();
					title = addPrefix(title, typeResource);
					titles.add(title);
					hasTypeLabel = true;
				}
				else if (externalResources != null && externalResources.listStatements(typeResource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).hasNext())
				{
					title = externalResources.listStatements(typeResource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).next().getLiteral().toString();
					title = addPrefix(title, typeResource);
					titles.add(title);
					hasTypeLabel = true;
				}
				else if (typeResource.equals(Vocabulary.OWL_CLASS))
				{
					// class disguised as instance for viz
					if (model.listStatements(resource, Vocabulary.DCT_TITLE, (RDFNode) null).hasNext())
					{
						title = model.listStatements(resource, Vocabulary.DCT_TITLE, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, resource);
						titles.add(title);
						hasTypeLabel = true;
					}
					else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.DCT_TITLE, (RDFNode) null).hasNext())
					{
						title = externalResources.listStatements(resource, Vocabulary.DCT_TITLE, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, resource);
						titles.add(title);
						hasTypeLabel = true;
					}
					else if (model.listStatements(resource, Vocabulary.DCT_IDENTIFIER, (RDFNode) null).hasNext())
					{
						title = model.listStatements(resource, Vocabulary.DCT_IDENTIFIER, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, resource);
						titles.add(title);
						hasTypeLabel = true;
					}
					else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.DCT_IDENTIFIER, (RDFNode) null).hasNext())
					{
						title = externalResources.listStatements(resource, Vocabulary.DCT_IDENTIFIER, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, resource);
						titles.add(title);
						hasTypeLabel = true;
					}
					else if (model.listStatements(resource, Vocabulary.RDFS_LABEL, (RDFNode) null).hasNext())
					{
						title = model.listStatements(resource, Vocabulary.RDFS_LABEL, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, resource);
						titles.add(title);
						hasTypeLabel = true;
					}
					else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.RDFS_LABEL, (RDFNode) null).hasNext())
					{
						title = externalResources.listStatements(resource, Vocabulary.RDFS_LABEL, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, resource);
						titles.add(title);
						hasTypeLabel = true;
					}
					else if (model.listStatements(resource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).hasNext())
					{
						title = model.listStatements(resource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, resource);
						titles.add(title);
						hasTypeLabel = true;
					}
					else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).hasNext())
					{
						title = externalResources.listStatements(resource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, resource);
						titles.add(title);
						hasTypeLabel = true;
					}
					else
					{
						title = resource.getLocalName();
						title = addPrefix(title, resource);
						titles.add(title);
						hasTypeLabel = true;
					}
				}
				else
				{
					title = typeResource.getLocalName();
					title = addPrefix(title, typeResource);
					titles.add(title);
					hasTypeLabel = true;
				}
			}

			if (hasTypeLabel)
			{
				return StringUtils.join(titles, ", ");
			}
			else if (resource.hasProperty(Vocabulary.SKOS_PREF_LABEL))
			{
				String title = model.listStatements(resource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).next().getLiteral().toString();
				title = addPrefix(title, resource);
				return title;
			}
			else
			{
				if (resource.getURI().startsWith(Vocabulary.URN_UUID) && resource.getURI().toLowerCase().matches(Vocabulary.URN_UUID + "[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}"))
				{
					return "instance";
				}
				else if (resource.getURI().startsWith(Vocabulary.URN_UUID)
						&& resource.getURI().toLowerCase().matches(Vocabulary.URN_UUID + "bnode:[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}"))
				{
					return "[]";
				}
				else if (resource.getURI().startsWith(Vocabulary.URN_UUID))
				{
					return resource.getURI().replaceAll(Vocabulary.URN_UUID, "instance of ");
				}
				else
				{
					return addPrefix(resource.getLocalName(), resource);
				}
			}
		}
		else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.RDF_TYPE, (RDFNode) null).hasNext())
		{
			StmtIterator stmtIterator = externalResources.listStatements(resource, Vocabulary.RDF_TYPE, (RDFNode) null);
			boolean hasTypeLabel = false;
			Set<String> titles = new HashSet<String>();
			while (stmtIterator.hasNext())
			{
				String title = StringUtils.EMPTY;
				Statement statement = stmtIterator.next();
				Resource typeResource = statement.getResource();
				if (typeResource.equals(Vocabulary.OWL_NAMED_INDIVIDUAL))
				{
					continue;
				}
				else if (typeResource.hasProperty(Vocabulary.DCT_TITLE))
				{
					title = externalResources.listStatements(typeResource, Vocabulary.DCT_TITLE, (RDFNode) null).next().getLiteral().toString();
					title = addPrefix(title, typeResource);
					titles.add(title);
					hasTypeLabel = true;
				}
				else if (typeResource.hasProperty(Vocabulary.DCT_IDENTIFIER))
				{
					title = externalResources.listStatements(typeResource, Vocabulary.DCT_IDENTIFIER, (RDFNode) null).next().getLiteral().toString();
					title = addPrefix(title, typeResource);
					titles.add(title);
					hasTypeLabel = true;
				}
				else if (typeResource.hasProperty(Vocabulary.RDFS_LABEL))
				{
					title = externalResources.listStatements(typeResource, Vocabulary.RDFS_LABEL, (RDFNode) null).next().getLiteral().toString();
					title = addPrefix(title, typeResource);
					titles.add(title);
					hasTypeLabel = true;
				}
				else if (typeResource.hasProperty(Vocabulary.SKOS_PREF_LABEL))
				{
					title = externalResources.listStatements(typeResource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).next().getLiteral().toString();
					title = addPrefix(title, typeResource);
					titles.add(title);
					hasTypeLabel = true;
				}
				else if (typeResource.equals(Vocabulary.OWL_CLASS))
				{
					// class disguised as instance for viz
					if (resource.hasProperty(Vocabulary.DCT_TITLE))
					{
						title = model.listStatements(resource, Vocabulary.DCT_TITLE, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, resource);
						titles.add(title);
						hasTypeLabel = true;
					}
					else if (externalResources.listStatements(resource, Vocabulary.DCT_TITLE, (RDFNode) null).hasNext())
					{
						title = externalResources.listStatements(resource, Vocabulary.DCT_TITLE, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, resource);
						titles.add(title);
						hasTypeLabel = true;
					}
					else if (resource.hasProperty(Vocabulary.DCT_IDENTIFIER))
					{
						title = model.listStatements(resource, Vocabulary.DCT_IDENTIFIER, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, resource);
						titles.add(title);
						hasTypeLabel = true;
					}
					else if (externalResources.listStatements(resource, Vocabulary.DCT_IDENTIFIER, (RDFNode) null).hasNext())
					{
						title = externalResources.listStatements(resource, Vocabulary.DCT_IDENTIFIER, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, resource);
						titles.add(title);
						hasTypeLabel = true;
					}
					else if (resource.hasProperty(Vocabulary.RDFS_LABEL))
					{
						title = model.listStatements(resource, Vocabulary.RDFS_LABEL, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, resource);
						titles.add(title);
						hasTypeLabel = true;
					}
					else if (externalResources.listStatements(resource, Vocabulary.RDFS_LABEL, (RDFNode) null).hasNext())
					{
						title = externalResources.listStatements(resource, Vocabulary.RDFS_LABEL, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, resource);
						titles.add(title);
						hasTypeLabel = true;
					}
					else if (resource.hasProperty(Vocabulary.SKOS_PREF_LABEL))
					{
						title = model.listStatements(resource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, resource);
						titles.add(title);
						hasTypeLabel = true;
					}
					else if (externalResources.listStatements(resource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).hasNext())
					{
						title = externalResources.listStatements(resource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).next().getLiteral().toString();
						title = addPrefix(title, resource);
						titles.add(title);
						hasTypeLabel = true;
					}
					else
					{
						title = resource.getLocalName();
						title = addPrefix(title, resource);
						titles.add(title);
						hasTypeLabel = true;
					}
				}
				else
				{
					title = typeResource.getLocalName();
					title = addPrefix(title, typeResource);
					titles.add(title);
					hasTypeLabel = true;
				}
			}

			if (hasTypeLabel)
			{
				return StringUtils.join(titles, ", ");
			}
			else if (resource.hasProperty(Vocabulary.SKOS_PREF_LABEL))
			{
				String title = model.listStatements(resource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).next().getLiteral().toString();
				title = addPrefix(title, resource);
				return title;
			}
			else if (externalResources.listStatements(resource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).hasNext())
			{
				String title = externalResources.listStatements(resource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).next().getLiteral().toString();
				title = addPrefix(title, resource);
				return title;
			}
			else
			{
				if (resource.getURI().startsWith(Vocabulary.URN_UUID) && resource.getURI().toLowerCase().matches(Vocabulary.URN_UUID + "[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}"))
				{
					return "instance";
				}
				else if (resource.getURI().startsWith(Vocabulary.URN_UUID)
						&& resource.getURI().toLowerCase().matches(Vocabulary.URN_UUID + "bnode:[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}"))
				{
					return "[]";
				}
				else if (resource.getURI().startsWith(Vocabulary.URN_UUID))
				{
					return resource.getURI().replaceAll(Vocabulary.URN_UUID, "instance of ");
				}
				else
				{
					return resource.getURI();
				}
			}
		}
		else if (resource.hasProperty(Vocabulary.SKOS_PREF_LABEL))
		{
			String title = model.listStatements(resource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).next().getLiteral().toString();
			title = addPrefix(title, resource);
			return title;
		}
		else if (externalResources != null && externalResources.listStatements(resource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).hasNext())
		{
			String title = externalResources.listStatements(resource, Vocabulary.SKOS_PREF_LABEL, (RDFNode) null).next().getLiteral().toString();
			title = addPrefix(title, resource);
			return title;
		}
		else
		{
			if (resource.getURI().startsWith(Vocabulary.URN_UUID) && resource.getURI().toLowerCase().matches(Vocabulary.URN_UUID + "[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}"))
			{
				return "instance";
			}
			else if (resource.getURI().startsWith(Vocabulary.URN_UUID)
					&& resource.getURI().toLowerCase().matches(Vocabulary.URN_UUID + "bnode:[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}"))
			{
				return "[]";
			}
			else if (resource.getURI().startsWith(Vocabulary.URN_UUID))
			{
				return resource.getURI().replaceAll(Vocabulary.URN_UUID, "instance of ");
			}
			else if (resource.getURI().equals(Vocabulary.RDF_TYPE.getURI()))
			{
				return "rdf:type";
			}
			else if (resource.getURI().equals(Vocabulary.RDFS_SUBCLASS_OF.getURI()))
			{
				return "rdfs:subClassOf";
			}
			else
			{
				String prefixedLocalName = addPrefix(resource.getLocalName(), resource);
				if (StringUtils.isEmpty(prefixedLocalName))
				{
					return resource.getURI();
				}

				return prefixedLocalName;
			}
		}

		if (resource.isAnon())
		{
			return "[]";
		}

		return addPrefix(resource.getLocalName(), resource);
	}

	public static Resource query(String queryString, Model model)
	{
		Resource resource = null;
		Query query = QueryFactory.create(queryString);
		try (QueryExecution queryExecution = QueryExecutionFactory.create(query, model))
		{
			ResultSet results = queryExecution.execSelect();
			while (results.hasNext())
			{
				QuerySolution qs = results.next();
				resource = qs.get("resource").asResource();
				break;
			}
		}

		return resource;
	}

	public static Resource findRoot(Model model, Model externalResources)
	{
		Processor.log.info("Finding root node.");

		Model fullModel = ModelFactory.createDefaultModel();
		fullModel.add(model);
		if (externalResources != null)
		{
			fullModel.add(externalResources);
		}

		if (Processor.visualizeOntology)
		{
			return findRootClass(fullModel);
		}

		Resource injectionSuperDomainResource = null;
		if (!Processor.visualizeOntology)
		{
			injectionSuperDomainResource = query(SparqlQuery.injectionInstancesQuery, fullModel);
		}
		if (injectionSuperDomainResource != null)
		{
			return injectionSuperDomainResource;
		}

		Resource hplcResource = null;
		if (!Processor.visualizeOntology)
		{
			hplcResource = query(SparqlQuery.hplcInstancesQuery, fullModel);
		}
		if (hplcResource != null)
		{
			return hplcResource;
		}

		Resource processResource = null;
		if (!Processor.visualizeOntology)
		{
			processResource = query(SparqlQuery.processInstancesQuery, fullModel);
		}
		if (processResource != null)
		{
			return processResource;
		}

		Resource deviceResource = null;
		if (!Processor.visualizeOntology)
		{
			deviceResource = query(SparqlQuery.deviceInstancesQuery, fullModel);
		}
		if (deviceResource != null)
		{
			return deviceResource;
		}

		Resource materialResource = null;
		if (!Processor.visualizeOntology)
		{
			materialResource = query(SparqlQuery.materialInstancesQuery, fullModel);
		}
		if (materialResource != null)
		{
			return materialResource;
		}

		// no expected instance found, use a different root

		// look for named resource instances
		StmtIterator stmtIterator = model.listStatements();
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			Resource subject = statement.getSubject();
			if (subject.isAnon())
			{
				continue;
			}

			if (subject.hasProperty(Vocabulary.RDF_TYPE))
			{
				StmtIterator typeStmtIterator = model.listStatements(subject, Vocabulary.RDF_TYPE, (RDFNode) null);
				while (typeStmtIterator.hasNext())
				{
					Statement typeStatement = typeStmtIterator.next();
					Resource typeResource = typeStatement.getResource();
					if (typeResource.isAnon())
					{
						continue;
					}
					if (typeResource.equals(Vocabulary.OWL_NAMED_INDIVIDUAL) || typeResource.equals(Vocabulary.OWL_CLASS) || typeResource.equals(Vocabulary.OWL_OBJECT_PROPERTY)
							|| typeResource.equals(Vocabulary.OWL_DATATYPE_PROPERTY) || typeResource.equals(Vocabulary.OWL_ANNOTATION_PROPERTY) || typeResource.equals(Vocabulary.OWL_ONTOLOGY))
					{
						continue;
					}

					return subject;
				}
			}

			if (subject.isURIResource() && subject.getURI().startsWith(Vocabulary.URN_UUID))
			{
				return subject;
			}
		}

		// no named resource instance found, look for blanks

		stmtIterator = model.listStatements();
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			Resource subject = statement.getSubject();
			if (!subject.isAnon())
			{
				continue;
			}

			if (subject.hasProperty(Vocabulary.RDF_TYPE))
			{
				StmtIterator typeStmtIterator = model.listStatements(subject, Vocabulary.RDF_TYPE, (RDFNode) null);
				while (typeStmtIterator.hasNext())
				{
					Statement typeStatement = typeStmtIterator.next();
					Resource typeResource = typeStatement.getResource();
					if (typeResource.isAnon())
					{
						continue;
					}
					if (typeResource.equals(Vocabulary.OWL_NAMED_INDIVIDUAL) || typeResource.equals(Vocabulary.OWL_CLASS) || typeResource.equals(Vocabulary.OWL_OBJECT_PROPERTY)
							|| typeResource.equals(Vocabulary.OWL_DATATYPE_PROPERTY) || typeResource.equals(Vocabulary.OWL_ANNOTATION_PROPERTY) || typeResource.equals(Vocabulary.OWL_ONTOLOGY))
					{
						continue;
					}

					return subject;
				}
			}
		}

		// no anonymous instance found
		Processor.log.info("No root instance found.");
		if (!Processor.visualizeOntology)
		{
			return null;
		}
		Processor.log.info("Visualizing ontology.");

		Resource rootClass = findRootClass(fullModel);
		if (rootClass != null)
		{
			return rootClass;
		}

		injectionSuperDomainResource = query(SparqlQuery.injectionQuery, fullModel);
		if (injectionSuperDomainResource != null)
		{
			return injectionSuperDomainResource;
		}

		hplcResource = query(SparqlQuery.hplcQuery, fullModel);
		if (hplcResource != null)
		{
			return hplcResource;
		}

		processResource = query(SparqlQuery.processQuery, fullModel);
		if (processResource != null)
		{
			return processResource;
		}

		deviceResource = query(SparqlQuery.deviceQuery, fullModel);
		if (deviceResource != null)
		{
			return deviceResource;
		}

		materialResource = query(SparqlQuery.materialQuery, fullModel);
		if (materialResource != null)
		{
			return materialResource;
		}

		return rootClass;
	}

	private static Resource findRootClass(Model model)
	{
		StmtIterator stmtIterator;
		stmtIterator = model.listStatements();
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			Resource subject = statement.getSubject();
			if (subject.isAnon())
			{
				continue;
			}

			if (subject.hasProperty(Vocabulary.RDF_TYPE, Vocabulary.OWL_CLASS))
			{
				Set<Resource> visited = new HashSet<Resource>();
				visited.add(subject);
				Resource rootClass = getParentClass(subject, model, visited);
				return rootClass;
			}
		}

		return null;
	}

	private static Resource getParentClass(Resource resource, Model model, Set<Resource> visited)
	{
		StmtIterator stmtIterator = model.listStatements(resource, Vocabulary.RDFS_SUBCLASS_OF, (RDFNode) null);
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			Resource parentResource = statement.getResource();

			if (visited.contains(parentResource))
			{
				continue;
			}

			if (parentResource.isAnon())
			{
				continue;
			}

			if (parentResource.equals(Vocabulary.OWL_THING))
			{
				return resource;
			}

			visited.add(parentResource);
			Resource grandParentResource = getParentClass(parentResource, model, visited);

			return grandParentResource;
		}

		return resource;
	}

	private static Set<CytoEntity> determineClusters(Set<CytoEntity> entities, Model model, Model externalResources)
	{
		Processor.log.info("Identifying clusters...");
		if (entities.isEmpty())
		{
			return entities;
		}

		Set<CytoEntity> tempEntities = new HashSet<CytoEntity>(entities.size());
		tempEntities.addAll(entities);
		try
		{
			// get nodes and edges
			Map<String, CytoNode> id2node = new HashMap<String, CytoNode>();
			Map<String, CytoEdge> id2edge = new HashMap<String, CytoEdge>();
			for (CytoEntity cytoEntity : tempEntities)
			{
				if (cytoEntity instanceof CytoNode)
				{
					id2node.put(((CytoNode) cytoEntity).getData().getId(), (CytoNode) cytoEntity);
				}
				else
				{
					id2edge.put(((CytoEdge) cytoEntity).getData().getId(), (CytoEdge) cytoEntity);
				}
			}

			// get nodes and incoming/outgoing edges
			Map<String, List<String>> nodes2outs = new HashMap<String, List<String>>();
			Map<String, List<String>> nodes2ins = new HashMap<String, List<String>>();
			for (CytoNode node : id2node.values())
			{
				String nodeId = node.getData().getId();
				if (!nodes2outs.containsKey(nodeId))
				{
					List<String> outs = new ArrayList<String>();
					nodes2outs.put(nodeId, outs);
				}
				if (!nodes2ins.containsKey(nodeId))
				{
					List<String> ins = new ArrayList<String>();
					nodes2ins.put(nodeId, ins);
				}

				for (CytoEdge edge : id2edge.values())
				{
					if (edge.getData().getSource().equals(nodeId))
					{
						List<String> outs = nodes2outs.get(nodeId);
						outs.add(edge.getData().getId());
						nodes2outs.put(nodeId, outs);
					}
					if (edge.getData().getTarget().equals(nodeId))
					{
						List<String> ins = nodes2ins.get(nodeId);
						ins.add(edge.getData().getId());
						nodes2ins.put(nodeId, ins);
					}
				}
			}

			int clusterId = 1;

			for (Entry<String, CytoNode> entry : id2node.entrySet())
			{
				String nodeId = entry.getKey();
				CytoNode node = entry.getValue();

				if (visited.contains(nodeId))
				{
					continue;
				}

				if (node.getData().getLabel().trim().startsWith("\""))
				{
					// skip literals because they are always parts of clusters not centers of clusters
					continue;
				}

				visited.add(nodeId);

				tempEntities.remove(node);
				node.setClusterId(String.valueOf(clusterId));
				tempEntities.add(node);
				Processor.log.debug(node.getData().getLabel() + " (" + clusterId + ")");

				List<String> outs = nodes2outs.get(nodeId);
				for (String out : outs)
				{
					CytoEdge edge = id2edge.get(out);
					tempEntities.remove(edge);
					edge.setClusterId(String.valueOf(clusterId));
					tempEntities.add(edge);
					if (edge.pointsToLiteral())
					{
						CytoNode literalNode = id2node.get(edge.getData().getTarget());
						tempEntities.remove(literalNode);
						literalNode.setClusterId(String.valueOf(clusterId));
						tempEntities.add(literalNode);
						visited.add(literalNode.getData().getId());
						Processor.log.debug(" --> " + edge.getData().getLabel() + " --> " + literalNode.getData().getLabel() + " (" + clusterId + ")");
						continue;
					}

					Processor.log.debug(" --> " + edge.getData().getLabel());
					if (isClusteredEdge(edge))
					{
						followEdge(id2node.get(edge.getData().getTarget()), clusterId, tempEntities, id2node, id2edge, nodes2ins, nodes2outs, 1);
					}
					else
					{
						followEdge(id2node.get(edge.getData().getTarget()), ++clusterId, tempEntities, id2node, id2edge, nodes2ins, nodes2outs, 1);
						clusterId--;
					}
				}

				clusterId++;
			}

			// process remaining edges
			for (Entry<String, CytoEdge> entry : id2edge.entrySet())
			{
				CytoEdge edge = entry.getValue();

				String strClusterId = edge.getClusterId();
				if (!StringUtils.isEmpty(strClusterId))
				{
					continue;
				}

				CytoNode sourceNode = id2node.get(edge.getData().getSource());
				strClusterId = sourceNode.getClusterId();
				if (StringUtils.isEmpty(strClusterId))
				{
					strClusterId = String.valueOf(++clusterId);
					tempEntities.remove(sourceNode);
					sourceNode.setClusterId(strClusterId);
					tempEntities.add(sourceNode);
				}

				tempEntities.remove(edge);
				edge.setClusterId(strClusterId);
				tempEntities.add(edge);
			}

			List<CytoEntity> unassignedEntities = new ArrayList<CytoEntity>();
			for (Iterator<CytoEntity> iterator = tempEntities.iterator(); iterator.hasNext();)
			{
				CytoEntity cytoEntity = iterator.next();
				if (StringUtils.isEmpty(cytoEntity.getClusterId()))
				{
					String label = "";
					if (cytoEntity instanceof CytoNode)
					{
						CytoNode node = (CytoNode) cytoEntity;
						node.setClusterId(String.valueOf(++clusterId));
						label = node.getData().getLabel();
					}
					else
					{
						CytoEdge edge = (CytoEdge) cytoEntity;
						edge.setClusterId(String.valueOf(++clusterId));
						label = edge.getData().getLabel();
					}
					Processor.log.debug("Entity was not assigned to any cluster: " + label);
					unassignedEntities.add(cytoEntity);
				}
			}

			if (!unassignedEntities.isEmpty())
			{
				tempEntities.removeAll(unassignedEntities);
				tempEntities.addAll(unassignedEntities);
			}

			entities = tempEntities;
			Processor.log.info("Identified " + clusterId + " clusters");
		}
		catch (Exception e)
		{
			Processor.log.info("Error during clustering.");
			e.printStackTrace();
		}

		return entities;
	}

	private static boolean isClusteredEdge(CytoEdge edge)
	{
		String edgeSuperType = edge.getData().getSupertype();
		if (StringUtils.isEmpty(edgeSuperType))
		{
			return false;
		}

		if (edgeSuperType.equals(Vocabulary.BFO_HAS_PART.getLocalName()))
		{
			return true;
		}

		if (edgeSuperType.equals(Vocabulary.RO_TEMPORALLY_RELATED_TO.getLocalName()))
		{
			return true;
		}

		if (edgeSuperType.startsWith(Vocabulary.QUDT_SCHEMA_PREFIX))
		{
			return true;
		}

		if (edgeSuperType.startsWith(Vocabulary.SHACL_PREFIX))
		{
			return true;
		}

		return false;
	}

	public static Set<Resource> getSuperProperties(Property subProperty)
	{
		Set<Resource> superProperties = new HashSet<Resource>();
		followRecursive(superProperties, new HashSet<Resource>(), subProperty, RDFS.subPropertyOf);
		superProperties.remove(subProperty);
		return superProperties;
	}

	private static void followRecursive(Set<Resource> resources, Set<Resource> visited, Resource subject, Property predicate)
	{
		resources.add(subject);
		visited.add(subject);
		StmtIterator stmtIterator = subject.listProperties(predicate);
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			RDFNode object = statement.getObject();
			if (!object.isResource())
			{
				continue;
			}

			if (visited.contains(object.asResource()))
			{
				continue;
			}

			followRecursive(resources, visited, object.asResource(), predicate);
		}
	}

	private static Set<CytoEntity> followEdge(CytoNode node, int clusterId, Set<CytoEntity> entities, Map<String, CytoNode> id2node, Map<String, CytoEdge> id2edge, Map<String, List<String>> nodes2ins,
			Map<String, List<String>> nodes2outs, int level)
	{
		String nodeId = node.getData().getId();

		if (visited.contains(nodeId))
		{
			return entities;
		}

		visited.add(nodeId);

		entities.remove(node);
		node.setClusterId(String.valueOf(clusterId));
		entities.add(node);

		String indentation = getIndent(level);

		Processor.log.debug(indentation + node.getData().getLabel() + " (" + clusterId + ")");

		List<String> outs = nodes2outs.get(nodeId);
		for (String out : outs)
		{
			CytoEdge edge = id2edge.get(out);
			entities.remove(edge);
			edge.setClusterId(String.valueOf(clusterId));
			entities.add(edge);
			if (edge.pointsToLiteral())
			{
				CytoNode literalNode = id2node.get(edge.getData().getTarget());
				entities.remove(literalNode);
				literalNode.setClusterId(String.valueOf(clusterId));
				entities.add(literalNode);
				visited.add(literalNode.getData().getId());
				Processor.log.debug(indentation + " --> " + edge.getData().getLabel() + " --> " + literalNode.getData().getLabel() + " (" + clusterId + ")");
				return entities;
			}

			Processor.log.debug(indentation + " --> " + edge.getData().getLabel());
			if (isClusteredEdge(edge))
			{
				followEdge(id2node.get(edge.getData().getTarget()), clusterId, entities, id2node, id2edge, nodes2ins, nodes2outs, ++level);
				level--;
			}
			else
			{
				followEdge(id2node.get(edge.getData().getTarget()), ++clusterId, entities, id2node, id2edge, nodes2ins, nodes2outs, ++level);
				clusterId--;
				level--;
			}
		}
		return entities;
	}

	private static String getIndent(int num)
	{
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < num; i++)
		{
			sb.append(" ");

		}
		return sb.toString();
	}
}
