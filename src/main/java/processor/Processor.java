package processor;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import adfsee.AdfSeeMain;
import fuseki.FusekiTripleStore;
import processor.adfinfo.AdfInfoExtractor;
import processor.adfinfo.ExtractedAdfInfo;
import processor.audit.AudittrailExtractor;
import processor.audit.ExtractedAudittrail;
import processor.cytojo.CytoEntity;
import processor.datacube.DatacubeExtractor;
import processor.datacube.ExtractedCube;
import processor.datadescription.AdfDatadescriptionExtractor;
import processor.datadescription.DdEntity;
import processor.datapackage.AdfDatapackageExtractor;
import processor.datapackage.DpEntity;
import processor.layout.Layouter;
import processor.tree.InspireTree;
import processor.tree.TreeNode;
import processor.util.AfCytoUtil;
import query.QueryManager;
import restcontroller.ProcessController;
import skolemizer.ADFSkolemizer;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class Processor
{
	public static Logger log = null;
	public static String inputFile;
	public static String outputFile;
	public static boolean extractInstances;
	public static boolean visualizeOntology = false;
	public static boolean optimizeLayout = false;
	public static Integer layoutDuration = 5;
	public static Float nodeSize = 0.1f;
	public static boolean isAutoLayout = false;
	public static boolean isRadialLayout = false;
	public static boolean isCircleLayout = false;
	public static boolean isGraphVizLayout = true;
	public static String dotBinary = "";
	public static String graphVizAlgoName = "fdp";
	public static boolean breakCycles = false;
	public static boolean layoutLinks = false;
	public static String initialLayout = "diagonal";
	public static String overlap = "scalexy";
	public static Integer startseed = 1;
	public static boolean avoidLinkLinkOverlap = false;
	public static String[] additionalInputFiles = null;
	public static boolean excludeRdfTypeNodes = true;
	public static boolean useNestedNodes = false;
	public static boolean includeAllInstances = false;
	public static boolean hideLiteralNodes = true;
	public static boolean usePathFollowing = true;
	public static boolean generateTree = false;
	public static boolean writeDot = false;
	public static boolean writePdf = false;
	public static Resource rootNode = null;
	public static boolean excludeAdf = true;
	public static String endpointUrl = "";
	public static String graphUrl = "";
	public static String constructQuery = "";
	public static boolean singleGraph = true;
	public static boolean skolemize = false;
	public static boolean useGraphStream = false;
	public static boolean skipLayout = false;

	public static Map<String, String> processData(String[] args) throws Exception
	{
		Map<String, String> resultMap = new HashMap<String, String>();
		rootNode = null;
		try
		{
			ProcessController.logger.info("executing rdfcyto using arguments: " + StringUtils.join(args, " "));
			Init.init(args, log);
			log.info("reading from " + inputFile + " writing to " + outputFile);

			if (!Files.exists(Paths.get(inputFile)) && endpointUrl.isEmpty())
			{
				throw new IllegalStateException("Input file not found.");
			}

			if (AdfSeeMain.useTripleStore)
			{
				FusekiTripleStore.cleanupTripleStore();
			}

			Model model = ModelFactory.createDefaultModel();
			if (inputFile.toLowerCase().endsWith("ttl"))
			{
				model.read(new FileInputStream(Paths.get(inputFile).toFile()), null, "TTL");
			}
			else if (inputFile.toLowerCase().endsWith("adf"))
			{
				if (skolemize)
				{
					ADFSkolemizer.skolemizeGraph(inputFile, null, false);
				}
				model = AdfReader.read(Paths.get(inputFile), model, log);
				if (AdfSeeMain.useTripleStore)
				{
					FusekiTripleStore.loadAdfToTripleStore(Paths.get(inputFile));
				}
			}
			else if (inputFile.toLowerCase().endsWith("sparql"))
			{
				model = SparqlEndpointReader.read(endpointUrl, graphUrl, constructQuery, model, log);
			}
			else
			{
				throw new IllegalStateException("Only input files of format ADF or TTL supported.");
			}

			log.info("creating cytoscape json from input: " + inputFile.toString()
					+ ((additionalInputFiles != null && additionalInputFiles.length > 0) ? " using additional files: " + StringUtils.join(additionalInputFiles, ", ") : ""));
			Model externalResources = null;
			if (additionalInputFiles != null && additionalInputFiles.length > 0)
			{
				externalResources = AfCytoUtil.addTriples(additionalInputFiles);
			}

			log.info("total number of triples: " + model.listStatements().toList().size());

			if (AdfSeeMain.useTripleStore)
			{
				FusekiTripleStore.loadTriples(model, externalResources);
			}

			Model modelWithAdf = ModelFactory.createDefaultModel();
			modelWithAdf.add(model);
			if (excludeAdf && !visualizeOntology)
			{
				Set<Statement> statementsToRemove = AdfTripleExtractor.extractFrom(model);
				log.info("Removing " + statementsToRemove.size() + " triples that belong to ADF API.");
				model.remove(new ArrayList<Statement>(statementsToRemove));
			}

			if (!constructQuery.isEmpty())
			{
				model = QueryManager.constructGraph(constructQuery, model, externalResources, log);
			}

			String jsonTreeString = StringUtils.EMPTY;
			try
			{
				// generate tree
				Set<TreeNode> tree = InspireTree.create(model, externalResources);
				jsonTreeString = serializeAsJson(tree);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

			String jsonResultsString = StringUtils.EMPTY;
			try
			{
				// execute queries
				Map<String, String> results = QueryManager.execute(model, externalResources);
				jsonResultsString = serializeMapAsJson(results);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

			String jsonGraphString = StringUtils.EMPTY;
			try
			{
				// generate graph
				Set<CytoEntity> cytoEntities = Rdf2Cyto.create(model, externalResources);
				if (!skipLayout)
				{
					cytoEntities = Layouter.optimizeLayout(cytoEntities);
				}
				else
				{
					log.info("Skipping server-side layout.");
				}
				jsonGraphString = serializeAsJson(cytoEntities);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

			String datacubeResultsString = StringUtils.EMPTY;
			try
			{
				if (inputFile.toLowerCase().endsWith("adf"))
				{
					List<ExtractedCube> cubes = DatacubeExtractor.extractFrom(Paths.get(inputFile), modelWithAdf, externalResources, log);
					datacubeResultsString = serializeAsJson(cubes);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

			String audittrailResultsString = StringUtils.EMPTY;
			try
			{
				if (inputFile.toLowerCase().endsWith("adf"))
				{
					List<ExtractedAudittrail> extractedAudittrails = AudittrailExtractor.extractFrom(Paths.get(inputFile), modelWithAdf, externalResources, log);
					audittrailResultsString = serializeAsJson(extractedAudittrails);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

			String adfInfoResultsString = StringUtils.EMPTY;
			try
			{
				if (inputFile.toLowerCase().endsWith("adf"))
				{
					ExtractedAdfInfo extractedAdfInfo = AdfInfoExtractor.extractFrom(Paths.get(inputFile), modelWithAdf, externalResources, log);
					adfInfoResultsString = serializeAsJson(extractedAdfInfo);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

			String adfDataPackageResultsString = StringUtils.EMPTY;
			try
			{
				if (inputFile.toLowerCase().endsWith("adf"))
				{
					DpEntity extractedDataPackage = AdfDatapackageExtractor.extractFrom(Paths.get(inputFile), log);
					adfDataPackageResultsString = serializeAsJson(extractedDataPackage);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

			String adfDataDescriptionResultsString = StringUtils.EMPTY;
			try
			{
				if (inputFile.toLowerCase().endsWith("adf"))
				{
					DdEntity extractedDataDescription = AdfDatadescriptionExtractor.extractFrom(Paths.get(inputFile), log);
					adfDataDescriptionResultsString = serializeAsJson(extractedDataDescription);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

			resultMap.put("tree", jsonTreeString);
			resultMap.put("treeSize", String.valueOf(jsonTreeString.length()));
			resultMap.put("graph", jsonGraphString);
			resultMap.put("graphSize", String.valueOf(jsonGraphString.length()));
			resultMap.put("queryresults", jsonResultsString);
			resultMap.put("queryresultssize", String.valueOf(jsonResultsString.length()));
			resultMap.put("datacubes", datacubeResultsString);
			resultMap.put("datacubessize", String.valueOf(datacubeResultsString.length()));
			resultMap.put("audittrail", audittrailResultsString);
			resultMap.put("audittrailsize", String.valueOf(audittrailResultsString.length()));
			resultMap.put("adfinfo", adfInfoResultsString);
			resultMap.put("adfinfosize", String.valueOf(adfInfoResultsString.length()));
			resultMap.put("datapackage", adfDataPackageResultsString);
			resultMap.put("datapackagesize", String.valueOf(adfDataPackageResultsString.length()));
			resultMap.put("datadescription", adfDataDescriptionResultsString);
			resultMap.put("datadescriptionsize", String.valueOf(adfDataDescriptionResultsString.length()));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if (!endpointUrl.isEmpty())
			{
				return null;
			}
		}
		finally
		{
			rootNode = null;
			log.info("processing done");
		}
		return resultMap;
	}

	private static String serializeAsJson(Object o) throws IOException, JsonGenerationException, JsonMappingException
	{
		ObjectMapper mapper = new ObjectMapper();
		StringWriter stringWriter = new StringWriter();
		mapper.writer().writeValue(stringWriter, o);
		String serialized = stringWriter.toString();
		return serialized;
	}

	private static String serializeMapAsJson(Map<String, String> map) throws IOException, JsonGenerationException, JsonMappingException
	{
		// map has format { queryName : resultsAsJsonString, ... }
		// this is now to be transformed to pure json string
		ObjectMapper mapper = new ObjectMapper();
		JsonNodeFactory jnf = JsonNodeFactory.instance;
		ObjectNode newRoot = new ObjectNode(jnf);
		for (Entry<String, String> entry : map.entrySet())
		{
			String key = entry.getKey();
			String value = entry.getValue();
			ObjectNode queryResult = (ObjectNode) mapper.readTree(value);
			newRoot.set(key, queryResult);
		}

		String json = mapper.writeValueAsString(newRoot);
		return json;
	}
}
