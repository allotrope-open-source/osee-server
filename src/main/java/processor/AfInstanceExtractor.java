package processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.apache.commons.lang3.StringUtils;
import org.apache.jena.rdf.model.AnonId;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;

import processor.util.RdfUtil;
import processor.util.Vocabulary;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class AfInstanceExtractor
{
	public static Model createInstanceModel(Model model, Model externalResources)
	{
		Processor.log.info("creating instance model");
		Model instanceModel = ModelFactory.createDefaultModel();
		instanceModel.setNsPrefixes(RdfUtil.prefixMap);

		Model mergedModel = ModelFactory.createDefaultModel();
		mergedModel.add(model);
		if (externalResources != null && !externalResources.isEmpty())
		{
			mergedModel.add(externalResources);
		}

		Set<Statement> instanceStatements = new HashSet<>();
		StmtIterator stmtIterator = model.listStatements();

		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();

			Resource subject = statement.getSubject();
			if (isInstanceToInclude(subject, mergedModel))
			{
				instanceStatements.add(statement);
			}
		}

		instanceModel.add(new ArrayList<Statement>(instanceStatements));

		if (Processor.includeAllInstances)
		{
			long numTriplesBefore = instanceModel.listStatements().toList().size();
			Set<Statement> furtherInstanceStatements = new HashSet<>();
			Processor.log.info("including all instance nodes that have an rdf:type specified");
			stmtIterator = model.listStatements((Resource) null, Vocabulary.RDF_TYPE, (RDFNode) null);
			while (stmtIterator.hasNext())
			{
				Statement statement = stmtIterator.next();

				if (instanceStatements.contains(statement))
				{
					continue;
				}

				RDFNode object = statement.getObject();
				if (model.listStatements(object.asResource(), Vocabulary.RDF_TYPE, Vocabulary.OWL_CLASS).hasNext())
				{
					furtherInstanceStatements.add(statement);
				}
				else if (externalResources.listStatements(object.asResource(), Vocabulary.RDF_TYPE, Vocabulary.OWL_CLASS).hasNext())
				{
					furtherInstanceStatements.add(statement);
				}
			}

			Processor.log.info("found " + furtherInstanceStatements.size() + " additional instance nodes that have an rdf:type specified");
			if (!furtherInstanceStatements.isEmpty())
			{
				instanceModel.add(new ArrayList<Statement>(furtherInstanceStatements));
				long numTriplesAfter = instanceModel.listStatements().toList().size();
				Processor.log.info("added " + (numTriplesAfter - numTriplesBefore) + " triples for further instances");
			}
		}

		StmtIterator instanceIterator = instanceModel.listStatements();
		Model tempModel = ModelFactory.createDefaultModel();
		Map<RDFNode, Set<RDFNode>> node2neighbours = determineNeighbourMap(model);
		while (instanceIterator.hasNext())
		{
			Statement statement = instanceIterator.next();

			Set<Statement> singleInstanceStatements = new HashSet<Statement>();

			singleInstanceStatements = addStatementsForObjectRelationWithoutRecursion(model, statement, singleInstanceStatements, node2neighbours, 0);

			tempModel.add(new ArrayList<Statement>(singleInstanceStatements));

			break;
		}

		if (!tempModel.isEmpty())
		{
			instanceModel.add(tempModel);
		}

		if (!instanceModel.isEmpty())
		{
			instanceModel = transformToAnonymousInstances(instanceModel);

			instanceModel = replaceLiteralNodes(instanceModel);

			instanceModel = replaceNamedIndividualsOfOntologies(instanceModel, model);

			instanceModel = addClassNodes(instanceModel, model);
		}
		Processor.log.info("total number of instance triples: " + instanceModel.listStatements().toList().size());

		checkForUntypedResources(instanceModel, externalResources);

		return instanceModel;
	}

	private static Map<RDFNode, Set<RDFNode>> determineNeighbourMap(Model model)
	{
		Map<RDFNode, Set<RDFNode>> node2neighbours = new HashMap<RDFNode, Set<RDFNode>>();
		StmtIterator stmtIterator = model.listStatements();
		while (stmtIterator.hasNext())
		{
			Statement statement2 = stmtIterator.next();
			Set<RDFNode> neighbours = new HashSet<RDFNode>();
			Resource subject = statement2.getSubject();
			StmtIterator subjectIterator = model.listStatements(subject, (Property) null, (RDFNode) null);
			while (subjectIterator.hasNext())
			{
				Statement statement3 = subjectIterator.next();
				if (!statement3.getObject().isLiteral())
				{
					neighbours.add(statement3.getObject());
				}
			}
			subjectIterator = model.listStatements((Resource) null, (Property) null, subject);
			while (subjectIterator.hasNext())
			{
				Statement statement3 = subjectIterator.next();
				neighbours.add(statement3.getSubject());
			}

			node2neighbours.put(subject, neighbours);
		}

		stmtIterator = model.listStatements();
		while (stmtIterator.hasNext())
		{
			Statement statement2 = stmtIterator.next();
			if (statement2.getObject().isLiteral())
			{
				continue;
			}

			RDFNode object = statement2.getObject();
			if (node2neighbours.containsKey(object))
			{
				continue;
			}

			Set<RDFNode> neighbours = new HashSet<RDFNode>();
			StmtIterator subjectIterator = model.listStatements(object.asResource(), (Property) null, (RDFNode) null);
			while (subjectIterator.hasNext())
			{
				Statement statement3 = subjectIterator.next();
				if (!statement3.getObject().isLiteral())
				{
					neighbours.add(statement3.getObject());
				}
			}
			subjectIterator = model.listStatements((Resource) null, (Property) null, object);
			while (subjectIterator.hasNext())
			{
				Statement statement3 = subjectIterator.next();
				neighbours.add(statement3.getSubject());
			}

			node2neighbours.put(object, neighbours);
		}
		return node2neighbours;
	}

	private static boolean isInstanceToInclude(Resource subject, Model model)
	{
		Resource subjectType = null;
		StmtIterator typeIterator = model.listStatements(subject, Vocabulary.RDF_TYPE, (RDFNode) null);
		while (typeIterator.hasNext())
		{
			subjectType = typeIterator.next().getResource();

			if (isTypeToBeSkipped(subject, subjectType, model))
			{
				return false;
			}
		}

		return true;
	}

	private static boolean isTypeToBeSkipped(Resource resource, Resource resourceType, Model model)
	{
		if (resource.isURIResource() && Vocabulary.OWL_CLASS.getURI().equals(resourceType.getURI()))
		{
			return true;
		}

		StmtIterator resourceTypeIterator = model.listStatements(resourceType, Vocabulary.RDF_TYPE, (RDFNode) null);
		while (resourceTypeIterator.hasNext())
		{
			Statement resourceTypeStatement = resourceTypeIterator.next();
			Resource resourceTypeType = resourceTypeStatement.getResource();

			if (resourceTypeType.isAnon())
			{
				continue;
				// TODO improve nested anonymous types
			}

			if (Vocabulary.OWL_CLASS.getURI().equals(resourceTypeType.getURI()))
			{
				return false;
			}

			if (Vocabulary.OWL_RESTRICTION.getURI().equals(resourceTypeType.getURI()))
			{
				return true;
			}

			if (Vocabulary.OWL_AXIOM.getURI().equals(resourceTypeType.getURI()))
			{
				return true;
			}
		}

		return false;
	}

	private static void checkForUntypedResources(Model model, Model externalResources)
	{
		Set<Resource> checked = new HashSet<Resource>();
		Set<Resource> untyped = new HashSet<Resource>();
		StmtIterator stmtIterator = model.listStatements();
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			Resource subject = statement.getSubject();

			if (!checked.contains(subject))
			{
				if (subject.isURIResource() && subject.getURI().startsWith(Vocabulary.AFO_PREFIX))
				{
					checked.add(subject);
					continue;
				}

				if (statement.getPredicate().getURI().startsWith(Vocabulary.OWL_PREFIX))
				{
					checked.add(subject);
					continue;
				}

				if (statement.getPredicate().equals(Vocabulary.RDFS_SUBCLASS_OF))
				{
					checked.add(subject.asResource());
					checked.add(statement.getObject().asResource());
					continue;
				}

				if (statement.getObject().isURIResource() && (statement.getResource().equals(Vocabulary.OWL_OBJECT_PROPERTY) || statement.getResource().equals(Vocabulary.OWL_DATATYPE_PROPERTY)
						|| statement.getResource().equals(Vocabulary.OWL_ANNOTATION_PROPERTY)))
				{
					checked.add(subject.asResource());
					checked.add(statement.getObject().asResource());
					continue;
				}

				if (!subject.hasProperty(Vocabulary.RDF_TYPE))
				{
					if (externalResources == null)
					{
						untyped.add(subject);
					}
					if (externalResources != null && !externalResources.listStatements(subject, Vocabulary.RDF_TYPE, Vocabulary.OWL_CLASS).hasNext()
							&& !externalResources.listStatements(subject, Vocabulary.RDF_TYPE, Vocabulary.OWL_OBJECT_PROPERTY).hasNext()
							&& !externalResources.listStatements(subject, Vocabulary.RDF_TYPE, Vocabulary.OWL_DATATYPE_PROPERTY).hasNext()
							&& !externalResources.listStatements(subject, Vocabulary.RDF_TYPE, Vocabulary.OWL_NAMED_INDIVIDUAL).hasNext()
							&& !externalResources.listStatements(subject, Vocabulary.RDFS_SUBCLASS_OF, (RDFNode) null).hasNext())
					{
						untyped.add(subject);
					}
				}
				checked.add(subject);
			}

			RDFNode object = statement.getObject();
			if (object.isLiteral())
			{
				continue;
			}

			if (!checked.contains(object.asResource()))
			{
				if (object.isURIResource() && object.asResource().getURI().startsWith(Vocabulary.AFO_PREFIX))
				{
					checked.add(object.asResource());
					continue;
				}

				if (statement.getPredicate().getURI().startsWith(Vocabulary.OWL_PREFIX))
				{
					checked.add(object.asResource());
					continue;
				}

				if (statement.getPredicate().equals(Vocabulary.RDFS_SUBCLASS_OF))
				{
					checked.add(subject.asResource());
					checked.add(statement.getObject().asResource());
					continue;
				}

				if (object.isURIResource() && object.asResource().equals(Vocabulary.OWL_NAMED_INDIVIDUAL))
				{
					checked.add(object.asResource());
					continue;
				}

				if (object.isURIResource() && (statement.getResource().equals(Vocabulary.OWL_OBJECT_PROPERTY) || statement.getResource().equals(Vocabulary.OWL_DATATYPE_PROPERTY)
						|| statement.getResource().equals(Vocabulary.OWL_ANNOTATION_PROPERTY)))
				{
					checked.add(subject.asResource());
					checked.add(object.asResource());
					continue;
				}

				if (!object.asResource().hasProperty(Vocabulary.RDF_TYPE))
				{
					if (externalResources == null)
					{
						untyped.add(object.asResource());
					}
					if (externalResources != null && !externalResources.listStatements(object.asResource(), Vocabulary.RDF_TYPE, Vocabulary.OWL_CLASS).hasNext()
							&& !externalResources.listStatements(object.asResource(), Vocabulary.RDF_TYPE, Vocabulary.OWL_OBJECT_PROPERTY).hasNext()
							&& !externalResources.listStatements(object.asResource(), Vocabulary.RDF_TYPE, Vocabulary.OWL_DATATYPE_PROPERTY).hasNext()
							&& !externalResources.listStatements(object.asResource(), Vocabulary.RDF_TYPE, Vocabulary.OWL_NAMED_INDIVIDUAL).hasNext()
							&& !externalResources.listStatements(object.asResource(), Vocabulary.RDFS_SUBCLASS_OF, (RDFNode) null).hasNext())
					{
						untyped.add(object.asResource());
					}
				}
				checked.add(object.asResource());
			}
		}
		if (!untyped.isEmpty())
		{
			Processor.log.info("found " + untyped.size() + " untyped instances");
			long i = 0;
			for (Iterator<Resource> iterator = untyped.iterator(); iterator.hasNext();)
			{
				Resource resource = iterator.next();
				Processor.log.info(i++ + " " + resource.toString());
				if (model.listStatements(resource, (Property) null, (RDFNode) null).hasNext())
				{
					Processor.log.debug("untyped instance as subject: " + StringUtils.join(model.listStatements(resource, (Property) null, (RDFNode) null).toList(), "\n"));
				}
				if (model.listStatements((Resource) null, (Property) null, resource).hasNext())
				{
					Processor.log.debug("untyped instance as object: " + StringUtils.join(model.listStatements((Resource) null, (Property) null, resource).toList(), "\n"));
				}
			}
		}
	}

	private static Model addClassNodes(Model instanceModel, Model model)
	{
		Processor.log.debug("replacing named individuals that are not instance data but classes (e.g. chebi:17790).");

		Set<String> allClassNodes = new HashSet<String>();

		StmtIterator stmtIterator = instanceModel.listStatements((Resource) null, Vocabulary.AFV_IS_CLASS, (RDFNode) null);
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			Resource subject = statement.getSubject();
			if (allClassNodes.contains(subject.getURI()))
			{
				continue;
			}

			allClassNodes.add(subject.getURI());
		}

		Iterator<String> namedNodesIterator = allClassNodes.iterator();
		while (namedNodesIterator.hasNext())
		{
			String uri = namedNodesIterator.next();
			Resource namedNode = instanceModel.getResource(uri);

			String label = namedNode.getProperty(Vocabulary.DCT_TITLE).getString();

			Resource correspondingTermOfOntologies = RdfUtil.getResourceByLabel(model, label, false, true);
			if (correspondingTermOfOntologies == null)
			{
				String[] segments = label.split(":");
				String namespace = StringUtils.EMPTY;
				String filterLabel;
				if (segments.length > 1)
				{
					namespace = RdfUtil.prefixMap.get(segments[0]);
					if (namespace == null || namespace.isEmpty())
					{
						Processor.log.info("Missing prefix for term: " + label);
					}
					filterLabel = segments[1];
				}
				else
				{
					namespace = Vocabulary.AFO_PREFIX;
					filterLabel = label;
				}

				correspondingTermOfOntologies = ResourceFactory.createResource(namespace + filterLabel);
			}

			Set<Statement> statementsToRemove = new HashSet<Statement>();
			Set<Statement> statementsToAdd = new HashSet<Statement>();

			StmtIterator namedNodeIterator = instanceModel.listStatements(namedNode, (Property) null, (RDFNode) null);
			while (namedNodeIterator.hasNext())
			{
				Statement statement = namedNodeIterator.next();
				statementsToRemove.add(statement);
			}

			namedNodeIterator = instanceModel.listStatements((Resource) null, (Property) null, namedNode);
			while (namedNodeIterator.hasNext())
			{
				Statement statement = namedNodeIterator.next();
				statementsToRemove.add(statement);
				Statement newStatement = instanceModel.createStatement(statement.getSubject(), statement.getPredicate(), correspondingTermOfOntologies);
				statementsToAdd.add(newStatement);
			}

			instanceModel.remove(new ArrayList<Statement>(statementsToRemove));
			instanceModel.add(new ArrayList<Statement>(statementsToAdd));
		}

		return instanceModel;
	}

	private static Model replaceNamedIndividualsOfOntologies(Model instanceModel, Model model)
	{
		Set<String> allNodes = new HashSet<String>();

		StmtIterator stmtIterator = instanceModel.listStatements((Resource) null, Vocabulary.AFV_IS_NAMED_INDIVIDUAL_OF_ONTOLOGIES, (RDFNode) null);
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			Resource subject = statement.getSubject();
			if (allNodes.contains(subject.getURI()))
			{
				continue;
			}

			allNodes.add(subject.getURI());
		}

		Iterator<String> namedNodesIterator = allNodes.iterator();
		while (namedNodesIterator.hasNext())
		{
			String uri = namedNodesIterator.next();
			Resource namedNode = instanceModel.getResource(uri);

			String label = namedNode.getProperty(Vocabulary.DCT_TITLE).getString();

			Resource correspondingIndividualOfOntologies = RdfUtil.getResourceByLabel(model, label, false, true);

			Set<Statement> statementsToRemove = new HashSet<Statement>();
			Set<Statement> statementsToAdd = new HashSet<Statement>();

			StmtIterator namedNodeIterator = instanceModel.listStatements(namedNode, (Property) null, (RDFNode) null);
			while (namedNodeIterator.hasNext())
			{
				Statement statement = namedNodeIterator.next();
				statementsToRemove.add(statement);
			}

			namedNodeIterator = instanceModel.listStatements((Resource) null, (Property) null, namedNode);
			while (namedNodeIterator.hasNext())
			{
				Statement statement = namedNodeIterator.next();
				statementsToRemove.add(statement);
				Statement newStatement = instanceModel.createStatement(statement.getSubject(), statement.getPredicate(), correspondingIndividualOfOntologies);
				statementsToAdd.add(newStatement);
			}

			instanceModel.remove(new ArrayList<Statement>(statementsToRemove));
			instanceModel.add(new ArrayList<Statement>(statementsToAdd));
		}

		return instanceModel;
	}

	private static Model replaceLiteralNodes(Model model)
	{
		Set<String> allLiteralNodes = new HashSet<String>();

		StmtIterator stmtIterator = model.listStatements((Resource) null, Vocabulary.AFV_IS_LITERAL_NODE, (RDFNode) null);
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			Resource subject = statement.getSubject();
			if (allLiteralNodes.contains(subject.getURI()))
			{
				continue;
			}

			allLiteralNodes.add(subject.getURI());
		}

		Iterator<String> literalNodesIterator = allLiteralNodes.iterator();
		while (literalNodesIterator.hasNext())
		{
			String uri = literalNodesIterator.next();
			Resource literalNode = model.getResource(uri);

			Set<Statement> statementsToRemove = new HashSet<Statement>();
			Set<Statement> statementsToAdd = new HashSet<Statement>();

			String literalValue = StringUtils.EMPTY;
			StmtIterator literalNodeIterator = model.listStatements(literalNode, (Property) null, (RDFNode) null);
			while (literalNodeIterator.hasNext())
			{
				Statement statement = literalNodeIterator.next();
				statementsToRemove.add(statement);

				if (statement.getPredicate().equals(Vocabulary.DCT_TITLE))
				{
					literalValue = statement.getString();
				}
			}
			String literalValueString = StringUtils.EMPTY;
			Object literalValueAsObject = null;
			if (literalValue.contains("\"^^"))
			{
				String[] segments = literalValue.split("\\^\\^");
				literalValueString = segments[0].substring(1, segments[0].length() - 1); // cut quotes
				String[] dataTypeSegments = segments[1].split(":");
				String dataTypeIri = RdfUtil.prefixMap.get(dataTypeSegments[0]) + dataTypeSegments[1];

				if (Vocabulary.XSD_STRING.getURI().equals(dataTypeIri))
				{
					literalValueAsObject = literalValueString;
				}
				else if (Vocabulary.XSD_DOUBLE.getURI().equals(dataTypeIri))
				{
					literalValueAsObject = Double.parseDouble(literalValueString);
				}
				else if (Vocabulary.XSD_INTEGER.getURI().equals(dataTypeIri))
				{
					literalValueAsObject = Integer.parseInt(literalValueString);
				}
				else if (Vocabulary.XSD_DATETIME.getURI().equals(dataTypeIri))
				{
					// e.g. "2017-05-13T15:25:00Z"^^xsd:dateTime
					literalValueAsObject = javax.xml.bind.DatatypeConverter.parseDateTime(literalValueString);
				}
				else if (Vocabulary.XSD_BOOLEAN.getURI().equals(dataTypeIri))
				{
					literalValueAsObject = Boolean.parseBoolean(literalValueString);
				}
				else
				{
					Processor.log.info("Unknown datatype found: \"" + dataTypeIri + "\". Assuming xsd:string.");
					literalValueAsObject = literalValueString;
				}
			}
			else if (literalValue.contains("xsd:"))
			{
				literalValueAsObject = literalValue;
			}
			else
			{
				Processor.log.error("Unhandled literal datatype: " + literalValue + ". Assuming xsd:string.");
				literalValueAsObject = literalValue;
			}

			literalNodeIterator = model.listStatements((Resource) null, (Property) null, literalNode);
			while (literalNodeIterator.hasNext())
			{
				Statement statement = literalNodeIterator.next();
				statementsToRemove.add(statement);
				Statement newStatement = model.createLiteralStatement(statement.getSubject(), statement.getPredicate(), ResourceFactory.createTypedLiteral(literalValueAsObject));
				statementsToAdd.add(newStatement);
			}

			model.remove(new ArrayList<Statement>(statementsToRemove));
			model.add(new ArrayList<Statement>(statementsToAdd));
		}

		return model;
	}

	private static Model transformToAnonymousInstances(Model model)
	{
		Set<String> allBlanks = new HashSet<String>();

		StmtIterator stmtIterator = model.listStatements();
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			Resource subject = statement.getSubject();
			if (allBlanks.contains(subject.getURI()))
			{
				continue;
			}

			if (!subject.hasProperty(Vocabulary.AFV_HAS_UUID))
			{
				continue;
			}

			allBlanks.add(subject.getURI());
		}

		Iterator<String> blanksIterator = allBlanks.iterator();
		while (blanksIterator.hasNext())
		{
			String uri = blanksIterator.next();

			Set<Statement> statementsToRemove = new HashSet<Statement>();
			Set<Statement> statementsToAdd = new HashSet<Statement>();

			Resource namedResourceToReplacebyBlank = model.getResource(uri);
			Resource blankNode = model.createResource();

			StmtIterator blankStmtIterator = model.listStatements(namedResourceToReplacebyBlank, (Property) null, (RDFNode) null);
			while (blankStmtIterator.hasNext())
			{
				Statement statement = blankStmtIterator.next();
				statementsToRemove.add(statement);
				if (statement.getPredicate().equals(Vocabulary.RDF_TYPE) && statement.getObject().isURIResource() && statement.getObject().equals(Vocabulary.OWL_NAMED_INDIVIDUAL))
				{
					continue;
				}
				if (statement.getPredicate().equals(Vocabulary.AFV_HAS_UUID))
				{
					continue;
				}

				Statement newStatement;
				if (statement.getObject().isAnon())
				{
					Resource blankNodeObject = model.createResource(new AnonId(statement.getObject().asNode().getBlankNodeId()));
					newStatement = ResourceFactory.createStatement(blankNode, statement.getPredicate(), blankNodeObject);
				}
				else
				{
					newStatement = ResourceFactory.createStatement(blankNode, statement.getPredicate(), statement.getObject());
				}
				statementsToAdd.add(newStatement);
			}

			blankStmtIterator = model.listStatements((Resource) null, (Property) null, namedResourceToReplacebyBlank);
			while (blankStmtIterator.hasNext())
			{
				Statement statement = blankStmtIterator.next();
				if (statement.getPredicate().equals(Vocabulary.AFV_HAS_UUID))
				{
					continue;
				}

				statementsToRemove.add(statement);

				if (statement.getPredicate().equals(Vocabulary.AFV_HAS_UUID))
				{
					continue;
				}

				Statement newStatement;
				if (statement.getSubject().isAnon())
				{
					Resource blankNodeSubject = model.createResource(new AnonId(statement.getSubject().asNode().getBlankNodeId()));
					newStatement = ResourceFactory.createStatement(blankNodeSubject, statement.getPredicate(), blankNode);
				}
				else
				{
					newStatement = ResourceFactory.createStatement(statement.getSubject(), statement.getPredicate(), blankNode);
				}

				statementsToAdd.add(newStatement);
			}

			model.remove(new ArrayList<Statement>(statementsToRemove));
			model.add(new ArrayList<Statement>(statementsToAdd));
		}

		return model;
	}

	private static void writeWithLevel(String string, Statement statement, int level)
	{
		if (Processor.log.isDebugEnabled())
		{
			String indentation = "";
			for (int i = 0; i < level; i++)
			{
				indentation += "----";
			}
			Processor.log.debug(indentation + string + statement);
		}
	}

	public static Set<Statement> addStatementsForObjectRelationWithoutRecursion(Model model, Statement statement, Set<Statement> statements, Map<RDFNode, Set<RDFNode>> node2neighbours, int level)
	{
		Stack<RDFNode> stack = new Stack<RDFNode>();
		Set<RDFNode> visited = new HashSet<RDFNode>();
		RDFNode startNode = statement.getSubject();
		stack.push(startNode);
		while (!stack.isEmpty())
		{
			RDFNode currentNode = stack.pop();
			visited.add(currentNode);
			for (RDFNode targetNode : node2neighbours.get(currentNode))
			{
				if (!visited.contains(targetNode))
				{
					stack.push(targetNode);
					StmtIterator currentNodeIterator = model.listStatements(currentNode.asResource(), (Property) null, targetNode);
					while (currentNodeIterator.hasNext())
					{
						Statement currentNodeStatement = currentNodeIterator.next();
						statements.add(currentNodeStatement);
						writeWithLevel("", currentNodeStatement, level);
					}
				}
			}
		}
		return statements;
	}

	public static Set<Statement> addStatementsforObjectRelation(Model model, Statement statement, Set<Statement> statements, int level)
	{
		if (statement.getObject().isURIResource() && (statement.getResource().equals(Vocabulary.OWL_RESTRICTION)//
				|| statement.getResource().equals(Vocabulary.OWL_CLASS) //
				|| statement.getResource().equals(Vocabulary.OWL_NAMED_INDIVIDUAL) //
				|| statement.getResource().equals(Vocabulary.OWL_OBJECT_PROPERTY) //
				|| statement.getResource().equals(Vocabulary.OWL_DATATYPE_PROPERTY) //
				|| statement.getResource().equals(Vocabulary.OWL_ANNOTATION_PROPERTY)//
				|| statement.getResource().getURI().startsWith(Vocabulary.AFV_PREFIX)))//
		{
			return statements;
		}

		if (statement.getSubject().isURIResource() && statement.getSubject().asResource().getURI().startsWith(Vocabulary.AFV_PREFIX))//
		{
			return statements;
		}

		if (statement.getPredicate().equals(Vocabulary.RDFS_SUBCLASS_OF) || statement.getPredicate().equals(Vocabulary.RDFS_SUBPROPERTY_OF) || statement.getPredicate().equals(Vocabulary.RDFS_RANGE)
				|| statement.getPredicate().equals(Vocabulary.RDFS_DOMAIN) || statement.getPredicate().getURI().startsWith(Vocabulary.OWL_PREFIX))//
		{
			return statements;
		}

		if (statement.getPredicate().getURI().startsWith(Vocabulary.AFV_PREFIX))//
		{
			return statements;
		}

		if (statement.getObject().isAnon() || (statement.getObject().isURIResource() && !statement.getResource().getURI().startsWith(Vocabulary.AFO_PREFIX)))
		{
			Processor.log.debug("Extracting: " + statement);
			StmtIterator stmtIterator = model.listStatements(statement.getObject().asResource(), (Property) null, (RDFNode) null);
			while (stmtIterator.hasNext())
			{
				Statement objectStatement = stmtIterator.next();
				if (statements.contains(objectStatement))
				{
					continue;
				}

				statements.add(objectStatement);
				writeWithLevel("O-S ", objectStatement, level);

				if (objectStatement.getObject().isLiteral())
				{
					continue;
				}

				statements = addStatementsforObjectRelation(model, objectStatement, statements, ++level);
				level--;
			}

			stmtIterator = model.listStatements((Resource) null, (Property) null, statement.getObject().asResource());
			while (stmtIterator.hasNext())
			{
				Statement subjectStatement = stmtIterator.next();
				if (statements.contains(subjectStatement))
				{
					continue;
				}

				statements.add(subjectStatement);
				writeWithLevel("O-O ", subjectStatement, level);

				statements = addStatementsforObjectRelation(model, subjectStatement, statements, ++level);
				level--;
			}
		}

		StmtIterator stmtIterator = model.listStatements((Resource) null, (Property) null, statement.getSubject().asResource());
		while (stmtIterator.hasNext())
		{
			Statement subjectStatement = stmtIterator.next();
			if (statements.contains(subjectStatement))
			{
				continue;
			}

			statements.add(subjectStatement);
			writeWithLevel("S-O ", subjectStatement, level);

			statements = addStatementsforObjectRelation(model, subjectStatement, statements, ++level);
			level--;
		}

		if (statement.getSubject().isAnon() || (statement.getSubject().isURIResource() && !statement.getSubject().getURI().startsWith(Vocabulary.AFO_PREFIX)))
		{
			stmtIterator = model.listStatements(statement.getSubject().asResource(), (Property) null, (RDFNode) null);
			while (stmtIterator.hasNext())
			{
				Statement subjectStatement = stmtIterator.next();
				if (statements.contains(subjectStatement))
				{
					continue;
				}

				statements.add(subjectStatement);
				writeWithLevel("S-S ", subjectStatement, level);

				if (subjectStatement.getObject().isLiteral())
				{
					continue;
				}

				statements = addStatementsforObjectRelation(model, subjectStatement, statements, ++level);
				level--;
			}
		}

		return statements;
	}

}
