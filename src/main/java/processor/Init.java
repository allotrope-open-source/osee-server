package processor;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.ConsoleAppender;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.apache.logging.log4j.core.config.builder.api.AppenderComponentBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilderFactory;
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class Init
{
	private static String version = "x.y.z";

	public static void init(String[] args, Logger log) throws ParseException, NoSuchFieldException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, IOException
	{
		Processor.log = configureLog4J2();

		printTitle();

		initOptions();

		Option help = Option.builder("h").longOpt("help").required(false).desc("Show list of command line arguments.").build();
		Option debug = Option.builder("d").longOpt("debug").required(false).desc("Output debug information.").build();
		Option input = Option.builder("i").longOpt("input").required(false).argName("file").hasArg().desc("Specify input file (ADF or RDF)").build();
		Option output = Option.builder("o").longOpt("output").required(false).argName("file").hasArg().desc("Specify output file (json)").build();
		Option externalFiles = Option.builder("r").longOpt("read").required(false).argName("file-1> <file-2> <...").hasArgs()
				.desc("Specifies additional TTL files with triples to be used during conversion. Optional argument.").build();
		Option extractInstances = Option.builder("e").longOpt("instances").required(false).desc("Extract instances from RDF model").build();
		Option optimize = Option.builder("opt").longOpt("optimize").required(false).desc("Optimize layout.").build();
		Option optimizeTime = Option.builder("t").longOpt("time").required(false).hasArg().desc("Optimize layout for the given number of seconds, default:5.").build();
		Option layouter = Option.builder("a").longOpt("layout").required(false).hasArg().desc("Use the specified layout algorithm. Possible values: auto, radial, circle, graphviz, default:graphviz")
				.build();
		Option dotBinary = Option.builder("dot").longOpt("dot").required(false).hasArg().desc("Specify absolute path to dot.exe for graphviz layouting.").build();
		Option graphVizAlgo = Option.builder("g").longOpt("graphvizalgo").required(false).hasArg().desc("Specify rendering algorithm of graphviz e.g. dot, neato, fdp, default:fdp").build();
		Option breakcycles = Option.builder("x").longOpt("break").required(false).desc("Break cycles in a cyclic graph for better layout.").build();
		Option noLinkLayout = Option.builder("nl").longOpt("nolinklayout").required(false)
				.desc("Exclude links during layouting. Layout is based on concept positions only. Link labels are positioned afterwards at the centers of connected concepts.").build();
		Option nodeSize = Option.builder("z").longOpt("nodesize").required(false).hasArg().desc("Use the given node size for optimizing layout with graphviz. Default: 0.1").build();
		Option initLayout = Option.builder("y").longOpt("initlayout").required(false).hasArg()
				.desc("Initial layout as starting point for automatic layout. Possible values: diagonal, square, blob. Default: diagonal").build();
		Option overlap = Option.builder("w").longOpt("overlap").required(false).hasArg().desc("Overlap option for graphviz. true, false, scalexy. default:scalexy").build();
		Option linkoverlap = Option.builder("u").longOpt("linkoverlap").required(false)
				.desc("Avoid overlap between link labels, option for graphviz. Default is only avoiding overlap between links and nodes.").build();
		Option startseed = Option.builder("m").longOpt("startseed").required(false).hasArg()
				.desc("Start seeding parameter as option for graphviz. Influences force-directed layout. Integer value, default 1").build();
		Option includeRdfType = Option.builder("rt").longOpt("includerdftype").required(false).desc("Include explicit nodes for rdf:type links.").build();
		Option useNestedNodes = Option.builder("unn").longOpt("usenestednodes").required(false).desc("Create nested nodes for patterns that can be collapsed to get a simpler picture.").build();
		Option includeInstances = Option.builder("iai").longOpt("includeallinstances").required(false)
				.desc("Show all instances in the graph including instances that do not have IRIs with namespace urn:uuid. Every node that has an rdf:type specified that is an owl:class will be included.")
				.build();
		Option visualizeOntology = Option.builder("vo").longOpt("ontology").required(false).desc("Visualize terms of the vocabulary.").build();
		Option showLiterals = Option.builder("sl").longOpt("showliteralnodes").required(false).desc("Visualize literal nodes. This may considerably increase the number of nodes to render.").build();
		Option generateTree = Option.builder("gt").longOpt("tree").required(false).desc("Generate a textual tree representation.").build();
		Option useIteration = Option.builder("it").longOpt("useiteration").required(false)
				.desc("Traverse the RDF graph by iteration over all triples. Default is traversing by follwing the path from a root node. Try iteration if you get stack overflow.").build();
		Option writeDot = Option.builder("wd").longOpt("writedot").required(false).desc("Write prepared dotfile.").build();
		Option writePdf = Option.builder("wp").longOpt("writepdf").required(false).desc("Export graph to autolayout.pdf after layouting.").build();
		Option includeAdfApi = Option.builder("waa").longOpt("includeadfapi").required(false).desc("Include triples from ADF API.").build();
		Option endpointUrl = Option.builder("eu").longOpt("endpointurl").required(false).argName("url").hasArg().desc("URL of SPARQL endpoint to query for graph data.").build();
		Option graphUrl = Option.builder("gu").longOpt("graphurl").required(false).argName("url").hasArg().desc("URL of named graph to query for data.").build();
		Option construct = Option.builder("co").longOpt("construct").required(false).argName("query").hasArg().desc("Construct query in order to construct a graph view.").build();
		Option multiGraph = Option.builder("mg").longOpt("multigraph").required(false).desc("Transform multiple separate graphs of instances. Default is transforming a single connected graph.")
				.build();
		Option skolemize = Option.builder("sk").longOpt("skolemize").required(false).desc("Skolemize blank nodes. Default is not to skolemize.").build();
		Option nolayout = Option.builder("nl").longOpt("skiplayout").required(false).desc("Skip layout to save time. Do layout client-side instead.").build();

		Options infoOptions = new Options();
		infoOptions.addOption(help);
		infoOptions.addOption(debug);

		Options appOptions = new Options();
		appOptions.addOption(debug);
		appOptions.addOption(input);
		appOptions.addOption(output);
		appOptions.addOption(externalFiles);
		appOptions.addOption(extractInstances);
		appOptions.addOption(optimize);
		appOptions.addOption(optimizeTime);
		appOptions.addOption(layouter);
		appOptions.addOption(dotBinary);
		appOptions.addOption(graphVizAlgo);
		appOptions.addOption(breakcycles);
		appOptions.addOption(noLinkLayout);
		appOptions.addOption(nodeSize);
		appOptions.addOption(initLayout);
		appOptions.addOption(overlap);
		appOptions.addOption(linkoverlap);
		appOptions.addOption(startseed);
		appOptions.addOption(includeRdfType);
		appOptions.addOption(useNestedNodes);
		appOptions.addOption(includeInstances);
		appOptions.addOption(visualizeOntology);
		appOptions.addOption(showLiterals);
		appOptions.addOption(useIteration);
		appOptions.addOption(generateTree);
		appOptions.addOption(writeDot);
		appOptions.addOption(writePdf);
		appOptions.addOption(includeAdfApi);
		appOptions.addOption(endpointUrl);
		appOptions.addOption(graphUrl);
		appOptions.addOption(construct);
		appOptions.addOption(multiGraph);
		appOptions.addOption(skolemize);
		appOptions.addOption(nolayout);

		Options allOptions = new Options();
		allOptions.addOption(help);
		allOptions.addOption(debug);
		allOptions.addOption(input);
		allOptions.addOption(output);
		allOptions.addOption(externalFiles);
		allOptions.addOption(extractInstances);
		allOptions.addOption(optimize);
		allOptions.addOption(optimizeTime);
		allOptions.addOption(layouter);
		allOptions.addOption(dotBinary);
		allOptions.addOption(graphVizAlgo);
		allOptions.addOption(breakcycles);
		allOptions.addOption(noLinkLayout);
		allOptions.addOption(nodeSize);
		allOptions.addOption(initLayout);
		allOptions.addOption(overlap);
		allOptions.addOption(linkoverlap);
		allOptions.addOption(startseed);
		allOptions.addOption(includeRdfType);
		allOptions.addOption(useNestedNodes);
		allOptions.addOption(includeInstances);
		allOptions.addOption(visualizeOntology);
		allOptions.addOption(showLiterals);
		allOptions.addOption(useIteration);
		allOptions.addOption(generateTree);
		allOptions.addOption(writeDot);
		allOptions.addOption(writePdf);
		allOptions.addOption(includeAdfApi);
		allOptions.addOption(endpointUrl);
		allOptions.addOption(graphUrl);
		allOptions.addOption(construct);
		allOptions.addOption(multiGraph);
		allOptions.addOption(skolemize);
		allOptions.addOption(nolayout);

		CommandLineParser infoParser = new DefaultParser();
		CommandLine cmd = infoParser.parse(infoOptions, args, true);

		if (cmd.hasOption("help"))
		{
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("java -jar afcyto-" + version + ".jar", allOptions, true);
			return;
		}

		if (cmd.hasOption("debug"))
		{
			setLevel(Processor.log, Level.DEBUG);
			Processor.log.info("show debug output");
		}

		CommandLineParser parser = new DefaultParser();
		cmd = parser.parse(appOptions, args, true);

		if (cmd.hasOption("input"))
		{
			Processor.inputFile = cmd.getOptionValue("input");
		}

		if (cmd.hasOption("output"))
		{
			Processor.outputFile = cmd.getOptionValue("output");
		}

		Processor.additionalInputFiles = cmd.getOptionValues("read");

		if (cmd.hasOption("instances"))
		{
			Processor.extractInstances = true;
		}

		if (cmd.hasOption("optimize"))
		{
			Processor.optimizeLayout = true;
		}

		if (cmd.hasOption("time"))
		{
			Processor.layoutDuration = Integer.valueOf(cmd.getOptionValue("time"));
		}

		if (cmd.hasOption("nodesize"))
		{
			Processor.nodeSize = Float.valueOf(cmd.getOptionValue("nodesize"));
		}
		if (cmd.hasOption("layout"))
		{
			String value = cmd.getOptionValue("layout").trim().toLowerCase();
			if (value != null & !value.isEmpty())
			{
				if (value.equals("radial"))
				{
					Processor.isAutoLayout = false;
					Processor.isRadialLayout = true;
				}
				else if (value.equals("circle"))
				{
					Processor.isAutoLayout = false;
					Processor.isCircleLayout = true;
				}
				else if (value.equals("graphviz"))
				{
					Processor.isAutoLayout = false;
					Processor.isGraphVizLayout = true;
					if (cmd.hasOption("dot"))
					{
						Processor.dotBinary = cmd.getOptionValue("dot");
					}
					if (cmd.hasOption("graphvizalgo"))
					{
						Processor.graphVizAlgoName = cmd.getOptionValue("graphvizalgo").trim().toLowerCase();
					}
				}
			}
		}

		if (cmd.hasOption("break"))
		{
			Processor.breakCycles = true;
		}

		if (cmd.hasOption("includeadfapi"))
		{
			Processor.excludeAdf = false;
		}

		if (cmd.hasOption("nolinklayout"))
		{
			Processor.layoutLinks = false;
		}

		if (cmd.hasOption("initlayout"))
		{
			Processor.initialLayout = cmd.getOptionValue("initlayout").trim().toLowerCase();
		}

		if (cmd.hasOption("overlap"))
		{
			Processor.overlap = cmd.getOptionValue("overlap").trim().toLowerCase();
		}

		if (cmd.hasOption("startseed"))
		{
			Processor.startseed = Integer.valueOf(cmd.getOptionValue("startseed").trim().toLowerCase());
		}

		if (cmd.hasOption("linkoverlap"))
		{
			Processor.avoidLinkLinkOverlap = true;
		}

		if (cmd.hasOption("includerdftype"))
		{
			Processor.excludeRdfTypeNodes = false;
		}

		if (cmd.hasOption("usenestednodes"))
		{
			Processor.useNestedNodes = true;
		}

		if (cmd.hasOption("includeallinstances"))
		{
			Processor.includeAllInstances = true;
		}

		if (cmd.hasOption("ontology"))
		{
			Processor.visualizeOntology = true;
		}

		if (cmd.hasOption("showliteralnodes"))
		{
			Processor.hideLiteralNodes = false;
		}

		if (cmd.hasOption("useiteration"))
		{
			Processor.usePathFollowing = false;
		}

		if (cmd.hasOption("tree"))
		{
			Processor.generateTree = true;
		}

		if (cmd.hasOption("writedot"))
		{
			Processor.writeDot = true;
		}

		if (cmd.hasOption("writepdf"))
		{
			Processor.writePdf = true;
		}

		if (cmd.hasOption("multigraph"))
		{
			Processor.singleGraph = false;
		}

		if (cmd.hasOption("skolemize"))
		{
			Processor.skolemize = true;
		}

		if (cmd.hasOption("endpointurl"))
		{
			Processor.endpointUrl = cmd.getOptionValue("endpointurl").trim().toLowerCase();
		}

		if (cmd.hasOption("graphurl"))
		{
			Processor.graphUrl = cmd.getOptionValue("graphurl").trim().toLowerCase();
		}

		if (cmd.hasOption("construct"))
		{
			Processor.constructQuery = cmd.getOptionValue("construct").trim();
		}

		if (cmd.hasOption("skiplayout"))
		{
			Processor.skipLayout = true;
		}
	}

	private static void initOptions()
	{
		Processor.extractInstances = false;
		Processor.visualizeOntology = false;
		Processor.optimizeLayout = false;
		Processor.layoutDuration = 5;
		Processor.nodeSize = 0.1f;
		Processor.isAutoLayout = false;
		Processor.isRadialLayout = false;
		Processor.isCircleLayout = false;
		Processor.isGraphVizLayout = true;
		Processor.dotBinary = "";
		Processor.graphVizAlgoName = "fdp";
		Processor.breakCycles = false;
		Processor.layoutLinks = false;
		Processor.initialLayout = "diagonal";
		Processor.overlap = "scalexy";
		Processor.startseed = 1;
		Processor.avoidLinkLinkOverlap = false;
		Processor.additionalInputFiles = null;
		Processor.excludeRdfTypeNodes = true;
		Processor.useNestedNodes = false;
		Processor.includeAllInstances = false;
		Processor.hideLiteralNodes = true;
		Processor.usePathFollowing = true;
		Processor.generateTree = false;
		Processor.writeDot = false;
		Processor.writePdf = false;
		Processor.excludeAdf = true;
		Processor.endpointUrl = "";
		Processor.graphUrl = "";
		Processor.constructQuery = "";
		Processor.singleGraph = true;
		Processor.skipLayout = false;
	}

	private static Logger configureLog4J2()
	{
		System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "ERROR");
		System.setProperty("org.slf4j.impl.SimpleLogger.DEFAULT_LOG_LEVEL_KEY", "ERROR");
		System.setProperty("org.apache.logging.log4j.simplelog.StatusLogger.level", "OFF");

		ConfigurationBuilder<BuiltConfiguration> builder = ConfigurationBuilderFactory.newConfigurationBuilder();
		builder.setStatusLevel(Level.INFO);
		builder.setConfigurationName("Config");
		AppenderComponentBuilder console = builder.newAppender("Stdout", "Console").addAttribute("target", ConsoleAppender.Target.SYSTEM_OUT)
				.add(builder.newLayout("PatternLayout").addAttribute("pattern", "%d %-5level "));
		builder.add(console);
		builder.add(builder.newRootLogger(Level.INFO).add(builder.newAppenderRef("Stdout")));
		Configurator.setRootLevel(Level.INFO);
		LoggerContext loggerContext = Configurator.initialize(builder.build());
		loggerContext = Configurator.initialize(builder.build());
		Logger log = loggerContext.getLogger("LOG");
		loggerContext.updateLoggers();

		return log;
	}

	public static void setLevel(Logger logger, Level level)
	{
		final LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
		final Configuration config = ctx.getConfiguration();

		LoggerConfig loggerConfig = config.getLoggerConfig(logger.getName());
		LoggerConfig specificConfig = loggerConfig;

		if (!loggerConfig.getName().equals(logger.getName()))
		{
			specificConfig = new LoggerConfig(logger.getName(), level, true);
			specificConfig.setParent(loggerConfig);
			config.addLogger(logger.getName(), specificConfig);
		}
		specificConfig.setLevel(level);
		ctx.updateLoggers();
	}

	public static void printTitle() throws IOException
	{
		// @formatter:off
		Processor.log.info("\r\n\r\no--o  o-o   o--o   o-o       o      \r\n" +
				"|   | |  \\  |     /          |      \r\n" +
				"O-Oo  |   O O-o  O     o  o -o- o-o \r\n" +
				"|  \\  |  /  |     \\    |  |  |  | | \r\n" +
				"o   o o-o   o      o-o o--O  o  o-o \r\n" +
				"                          |         \r\n" +
				"                       o--o         \r\n" +
				getVersion() + "\r\n"); //
		// @formatter:on
	}

	private static String getVersion() throws IOException
	{
		final Properties properties = new Properties();
		properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties"));
		version = properties.getProperty("rdfcyto.version");
		return "version " + version + " ©OSTHUS 2020";
	}
}
