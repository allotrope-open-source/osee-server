/*******************************************************************
*               Notice
*
* Copyright Osthus GmbH, All rights reserved.
*
* This software is part of the Allotrope ADM project
*
* Address: Osthus GmbH
*        : Eisenbahnweg 9
*        : 52068 Aachen
*        : Germany
*
*******************************************************************/

package processor;

import org.apache.commons.lang3.StringUtils;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.sparql.engine.http.QueryEngineHTTP;
import org.apache.logging.log4j.Logger;

import query.SparqlQuery;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 */
public class SparqlEndpointReader
{

	public static Model read(String endpointUrl, String graphUrl, String constructQuery, Model model, Logger log)
	{
		try
		{
			if (graphUrl.isEmpty() || "default".equals(graphUrl.toLowerCase()))
			{
				graphUrl = "urn:x-arq:DefaultGraph";
			}
			String queryString = StringUtils.EMPTY;
			if (constructQuery != null && !constructQuery.isEmpty())
			{
				// use user-specified construct query
				queryString = constructQuery;
			}
			else
			{
				queryString = SparqlQuery.constructModelFromGraphTemplate.replaceAll("%graph%", graphUrl);
			}

			Query query = QueryFactory.create(queryString);
			QueryExecution qe = QueryExecutionFactory.sparqlService(endpointUrl, query);
			((QueryEngineHTTP) qe).addParam("timeout", "60000");
			log.info("Getting triples from endpoint: \"" + endpointUrl + "\" named graph: \"" + graphUrl + "\""
					+ ((constructQuery != null && !constructQuery.isEmpty()) ? " query: \"" + constructQuery + "\"" : ""));

			Model remotemodel = qe.execConstruct();
			log.info("Got " + remotemodel.size() + " triples from \"" + endpointUrl + "\" named graph: \"" + graphUrl + "\""
					+ ((constructQuery != null && !constructQuery.isEmpty()) ? " query: \"" + constructQuery + "\"" : ""));
			model.add(remotemodel);
		}
		catch (Exception e)
		{
			log.error("Error during querying sparql endpoint: \"" + endpointUrl + "\" named graph: \"" + graphUrl + "\""
					+ ((constructQuery != null && !constructQuery.isEmpty()) ? " query: \"" + constructQuery + "\"" : ""));
			throw e;
		}
		return model;
	}
}
