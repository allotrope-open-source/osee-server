package processor.tree;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;

import processor.Processor;
import processor.Rdf2Cyto;
import processor.util.DomainEnum;
import processor.util.Vocabulary;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class InspireTree
{
	public static Set<TreeNode> tree = new HashSet<TreeNode>();
	private static Set<Resource> visited = new HashSet<Resource>();
	private static Set<Statement> visitedStatementsForward = new HashSet<Statement>();
	private static Set<Statement> visitedStatementsBackward = new HashSet<Statement>();

	public static Set<TreeNode> create(Model model, Model externalResources)
	{
		tree.clear();
		visited.clear();
		visitedStatementsForward.clear();
		visitedStatementsBackward.clear();

		Model mergedModel = ModelFactory.createDefaultModel();
		mergedModel.add(model);
		if (externalResources != null && !externalResources.isEmpty())
		{
			mergedModel.add(externalResources);
		}

		Resource rootResource = null;
		if (Processor.rootNode == null)
		{
			rootResource = Rdf2Cyto.findRoot(model, externalResources);
		}

		if (rootResource == null)
		{
			if (model.isEmpty())
			{
				Processor.log.error("Empty model.");
				return tree;
			}
			Processor.log.error("No root node found. Random choice.");
			rootResource = model.listStatements((Resource) null, (Property) null, (RDFNode) null).next().getSubject();
		}
		else
		{
			Processor.log.info("found root node: " + (rootResource.isAnon() ? rootResource.asNode().getBlankNodeLabel() : rootResource.getURI()));
			Processor.rootNode = rootResource;
		}

		TreeNode rootNode = new TreeNode();
		String rootId = StringUtils.EMPTY;
		if (rootResource.isURIResource())
		{
			rootId = rootResource.getURI();
		}
		else
		{
			rootId = rootResource.asNode().getBlankNodeLabel();
		}

		rootNode.setId(rootId);
		String rootLabel = Rdf2Cyto.determineLabel(model, externalResources, rootResource);
		String rootColor = Rdf2Cyto.determineBackgroundColor(mergedModel, externalResources, rootResource);
		rootNode.setLabel(rootLabel);
		rootNode.setText(rootLabel);
		rootNode.setName("<span class=\"nodelabel\">" + rootLabel + "</span>");
		rootNode.setColor(rootColor);
		rootNode.setDetail(Rdf2Cyto.createPopup(model, externalResources, rootResource));
		rootNode.setDomains(getDomains(mergedModel, rootResource));
		rootNode = addLiterals(model, externalResources, mergedModel, rootResource, rootNode);
		Set<Resource> visitedOnPathForward = new HashSet<Resource>();
		Set<Resource> visitedOnPathBackward = new HashSet<Resource>();
		rootNode = addRelations(model, externalResources, mergedModel, rootResource, rootNode, visitedOnPathForward, visitedOnPathBackward, DirectionEnum.FORWARD);
		visitedOnPathForward = new HashSet<Resource>();
		visitedOnPathBackward = new HashSet<Resource>();
		rootNode = addInverseRelations(model, externalResources, mergedModel, rootResource, rootNode, visitedOnPathForward, visitedOnPathBackward, DirectionEnum.BACKWARD);
		tree.add(rootNode);
		return tree;
	}

	private static Set<String> getDomains(Model mergedModel, Resource resource)
	{
		Set<String> domains = new HashSet<String>();
		if (resource.isURIResource())
		{
			String uri = resource.getURI();
			domains.addAll(getDomainsForUri(domains, uri));
		}
		else
		{
			StmtIterator stmtIterator = mergedModel.listStatements(resource, Vocabulary.RDF_TYPE, (RDFNode) null);
			while (stmtIterator.hasNext())
			{
				Statement statement = stmtIterator.next();
				Resource type = statement.getResource();
				if (type.isAnon())
				{
					continue;
				}

				domains.addAll(getDomainsForUri(domains, type.getURI()));
			}
		}

		DomainEnum domainEnum = Rdf2Cyto.determineDomain(mergedModel, resource);

		domains.add(domainEnum.name().toUpperCase());
		return domains;
	}

	private static Set<String> getDomainsForUri(Set<String> domains, String uri)
	{
		if (uri.startsWith(Vocabulary.AFO_PREFIX) || uri.startsWith(Vocabulary.AFO2_PREFIX) || uri.toLowerCase().contains("allotrope"))
		{
			domains.add("ALLOTROPE");
		}
		else if (uri.toLowerCase().contains("obo"))
		{
			domains.add("OBO");
			if (uri.toLowerCase().contains("bfo"))
			{
				domains.add("BFO");
			}
			else if (uri.toLowerCase().contains("iao"))
			{
				domains.add("IAO");
			}
			else if (uri.toLowerCase().contains("obi"))
			{
				domains.add("OBI");
			}
			else if (uri.toLowerCase().contains("pato"))
			{
				domains.add("PATO");
			}
			else if (uri.toLowerCase().contains("ro"))
			{
				domains.add("RO");
			}
			else if (uri.toLowerCase().contains("chmo"))
			{
				domains.add("CHMO");
			}
			else if (uri.toLowerCase().contains("envo"))
			{
				domains.add("ENVO");
			}
			else if (uri.toLowerCase().contains("ncbi"))
			{
				domains.add("NCBI");
			}
			else if (uri.toLowerCase().contains("chebi"))
			{
				domains.add("CHEBI");
			}
		}
		else if (uri.toLowerCase().contains("example"))
		{
			domains.add("EXAMPLE");
		}
		else if (uri.toLowerCase().contains("uuid"))
		{
			domains.add("INSTANCE");
		}
		else
		{
			domains.add("OTHER");
		}

		return domains;
	}

	private static TreeNode getTreeNode(String id, Set<TreeNode> tree)
	{
		for (Iterator<TreeNode> iterator = tree.iterator(); iterator.hasNext();)
		{
			TreeNode treeNode = iterator.next();
			if (treeNode.getId().equals(id))
			{
				return treeNode;
			}
		}
		return null;
	}

	private static TreeNode addInverseRelations(Model model, Model externalResources, Model mergedModel, Resource resource, TreeNode treeNode, Set<Resource> visitedOnPathForward,
			Set<Resource> visitedOnPathBackward, DirectionEnum direction)
	{
		if (direction.equals(DirectionEnum.BACKWARD) && visitedOnPathBackward.contains(resource))
		{
			return treeNode;
		}

		if (direction.equals(DirectionEnum.BACKWARD))
		{
			visitedOnPathBackward.add(resource);
		}

		if (direction.equals(DirectionEnum.FORWARD) && visitedOnPathForward.contains(resource))
		{
			return treeNode;
		}

		if (direction.equals(DirectionEnum.FORWARD))
		{
			visitedOnPathForward.add(resource);
		}

		visited.add(resource);

		Set<TreeNode> relations = new HashSet<TreeNode>();

		StmtIterator stmtIterator = model.listStatements((Resource) null, (Property) null, resource);
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			Resource inverseLinkSubject = statement.getSubject();

			String relationProperty = StringUtils.EMPTY;
			Resource predicate = statement.getPredicate().asResource();
			if (predicate.equals(Vocabulary.RDF_TYPE))
			{
				continue;
			}

			if (direction.equals(DirectionEnum.FORWARD) && visitedStatementsForward.contains(statement))
			{
				continue;
			}

			if (direction.equals(DirectionEnum.BACKWARD) && visitedStatementsBackward.contains(statement))
			{
				continue;
			}

			if (direction.equals(DirectionEnum.FORWARD))
			{
				visitedStatementsForward.add(statement);
			}

			if (direction.equals(DirectionEnum.BACKWARD))
			{
				visitedStatementsBackward.add(statement);
			}

			relationProperty = Rdf2Cyto.determineLabel(model, externalResources, predicate);
			String id = StringUtils.EMPTY;
			String inverseLinkSubjectLabel = Rdf2Cyto.determineLabel(model, externalResources, inverseLinkSubject);
			String inverseLinkColor = Rdf2Cyto.determineBackgroundColor(mergedModel, externalResources, inverseLinkSubject);
			String fullRelationProperty = "^";
			String name = StringUtils.EMPTY;
			String detail = Rdf2Cyto.createPopup(model, externalResources, inverseLinkSubject);
			Set<String> domains = getDomains(mergedModel, inverseLinkSubject);
			if (inverseLinkSubject.isURIResource())
			{
				id = inverseLinkSubject.asResource().getURI();
				fullRelationProperty = fullRelationProperty + relationProperty + " <" + inverseLinkSubjectLabel + ">";
				name = "<span class=\"inverserelationlabel\"><span class=\"relationpart\">^" + relationProperty + " </span><span class=\"objectpart\"><i class=\"fa fa-angle-left\"></i>"
						+ inverseLinkSubjectLabel + "<i class=\"fa fa-angle-right\"></i></span></span>";
			}
			else
			{
				id = inverseLinkSubject.asNode().getBlankNodeLabel();
				fullRelationProperty = fullRelationProperty + relationProperty + " " + inverseLinkSubjectLabel;
				name = "<span class=\"inverserelationlabel\"><span class=\"relationpart\">^" + relationProperty + " </span><span class=\"objectpart\">" + inverseLinkSubjectLabel + "</span></span>";
			}

			TreeNode inverseLinkSubjectNode = getTreeNode(id, tree);
			if (inverseLinkSubjectNode == null)
			{
				inverseLinkSubjectNode = new TreeNode();
				inverseLinkSubjectNode.setId(id);
				inverseLinkSubjectNode.setLabel(inverseLinkSubjectLabel);
				inverseLinkSubjectNode.setText(inverseLinkSubjectLabel);
				inverseLinkSubjectNode.setName("<span class=\"inverserelationlabel\"><span class=\"objectpart\">" + inverseLinkSubjectLabel + "</span></span>");
				inverseLinkSubjectNode.setColor(inverseLinkColor);
				inverseLinkSubjectNode.setDetail(detail);
				inverseLinkSubjectNode.setDomains(domains);

				inverseLinkSubjectNode = addLiterals(model, externalResources, mergedModel, inverseLinkSubject.asResource(), inverseLinkSubjectNode);
				inverseLinkSubjectNode = addInverseRelations(model, externalResources, mergedModel, inverseLinkSubject.asResource(), inverseLinkSubjectNode, visitedOnPathForward,
						visitedOnPathBackward, direction);
				inverseLinkSubjectNode = addRelations(model, externalResources, mergedModel, inverseLinkSubject.asResource(), inverseLinkSubjectNode, visitedOnPathForward, visitedOnPathBackward,
						DirectionEnum.FORWARD);
			}

			TreeNode relation = new TreeNode();
			relation.setText(fullRelationProperty);
			relation.setLabel(fullRelationProperty);
			relation.setName(name);
			relation.setColor(inverseLinkColor);
			relation.setDetail(detail);
			relation.setDomains(domains);
			relation.setId(treeNode.getId() + "<--" + id);
			// relation.setChildren(new HashSet<TreeNode>(Arrays.asList(inverseLinkSubjectNode)));
			relation.setChildren(new HashSet<TreeNode>(inverseLinkSubjectNode.getChildren()));
			relations.add(relation);

			if (direction.equals(DirectionEnum.FORWARD))
			{
				visitedStatementsForward.add(statement);
			}
			else
			{
				visitedStatementsBackward.add(statement);
			}
		}

		Set<TreeNode> children = treeNode.getChildren();
		if (children == null)
		{
			children = new HashSet<TreeNode>();
		}
		children.addAll(relations);
		treeNode.setChildren(children);

		Set<TreeNode> neighbours = treeNode.getChildren();
		for (Iterator<TreeNode> iterator = neighbours.iterator(); iterator.hasNext();)
		{
			TreeNode neighbour = iterator.next();
			Resource neighbourResource = model.createResource(neighbour.getId());
			neighbour = addInverseRelations(model, externalResources, mergedModel, neighbourResource, neighbour, visitedOnPathForward, visitedOnPathBackward, direction);
		}
		return treeNode;
	}

	private static TreeNode addRelations(Model model, Model externalResources, Model mergedModel, Resource resource, TreeNode treeNode, Set<Resource> visitedOnPathForward,
			Set<Resource> visitedOnPathBackward, DirectionEnum direction)
	{
		if (direction.equals(DirectionEnum.FORWARD) && visitedOnPathForward.contains(resource))
		{
			return treeNode;
		}

		if (direction.equals(DirectionEnum.FORWARD))
		{
			visitedOnPathForward.add(resource);
		}

		if (direction.equals(DirectionEnum.BACKWARD) && visitedOnPathBackward.contains(resource))
		{
			return treeNode;
		}

		if (direction.equals(DirectionEnum.BACKWARD))
		{
			visitedOnPathBackward.add(resource);
		}

		visited.add(resource);
		Set<TreeNode> relations = new HashSet<TreeNode>();
		StmtIterator stmtIterator = model.listStatements(resource, (Property) null, (RDFNode) null);
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			RDFNode object = statement.getObject();
			if (object.isLiteral())
			{
				continue;
			}

			String relationProperty = StringUtils.EMPTY;
			Resource predicate = statement.getPredicate().asResource();
			relationProperty = Rdf2Cyto.determineLabel(model, externalResources, predicate);
			String id = StringUtils.EMPTY;
			String fullRelationProperty = StringUtils.EMPTY;
			String objectLabel = Rdf2Cyto.determineLabel(model, externalResources, object.asResource());
			String objectColor = Rdf2Cyto.determineBackgroundColor(mergedModel, externalResources, object.asResource());
			String objectDetail = Rdf2Cyto.createPopup(model, externalResources, object.asResource());
			Set<String> domains = getDomains(mergedModel, object.asResource());
			String name = StringUtils.EMPTY;
			if (object.isURIResource())
			{
				id = object.asResource().getURI();
				fullRelationProperty = relationProperty + " <" + objectLabel + ">";
				name = "<span class=\"relationlabel\"><span class=\"relationpart\">" + relationProperty + " </span><span class=\"objectpart\"><i class=\"fa fa-angle-left\"></i>" + objectLabel
						+ "<i class=\"fa fa-angle-right\"></i></span></span>";
			}
			else
			{
				id = object.asNode().getBlankNodeLabel();
				fullRelationProperty = relationProperty + " " + objectLabel;
				name = "<span class=\"relationlabel\"><span class=\"relationpart\">" + relationProperty + " </span><span class=\"objectpart\">" + objectLabel + "</span></span>";
			}

			TreeNode objectNode = getTreeNode(id, tree);
			if (objectNode == null)
			{
				objectNode = new TreeNode();
				objectNode.setId(id);
				objectNode.setLabel(objectLabel);
				objectNode.setText(objectLabel);
				objectNode.setName("<span class=\"relationlabel\"><span class=\"objectpart\">" + objectLabel + "</span></span>");
				objectNode.setColor(objectColor);
				objectNode.setDetail(objectDetail);
				objectNode.setDomains(domains);
				objectNode = addLiterals(model, externalResources, mergedModel, object.asResource(), objectNode);
				objectNode = addRelations(model, externalResources, mergedModel, object.asResource(), objectNode, visitedOnPathForward, visitedOnPathBackward, direction);
				objectNode = addInverseRelations(model, externalResources, mergedModel, object.asResource(), objectNode, visitedOnPathForward, visitedOnPathBackward, direction);
			}

			TreeNode relation = new TreeNode();
			relation.setText(fullRelationProperty);
			relation.setLabel(fullRelationProperty);
			relation.setName(name);
			relation.setColor(objectColor);
			relation.setId(treeNode.getId() + "-->" + id);
			relation.setDetail(objectDetail);
			relation.setDomains(domains);
			// relation.setChildren(new HashSet<TreeNode>(Arrays.asList(objectNode)));
			relation.setChildren(new HashSet<TreeNode>(objectNode.getChildren()));
			relations.add(relation);

			if (direction.equals(DirectionEnum.FORWARD))
			{
				visitedStatementsForward.add(statement);
			}
			else
			{
				visitedStatementsBackward.add(statement);
			}
		}

		Set<TreeNode> children = treeNode.getChildren();
		if (children == null)
		{
			children = new HashSet<TreeNode>();
		}
		children.addAll(relations);
		treeNode.setChildren(children);
		return treeNode;
	}

	private static TreeNode addLiterals(Model model, Model externalResources, Model mergedModel, Resource resource, TreeNode treeNode)
	{
		Set<TreeNode> literals = new HashSet<TreeNode>();
		StmtIterator stmtIterator = model.listStatements(resource, (Property) null, (RDFNode) null);
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			RDFNode object = statement.getObject();
			if (!object.isLiteral())
			{
				continue;
			}

			String literalProperty = StringUtils.EMPTY;
			Resource predicate = statement.getPredicate().asResource();
			literalProperty = Rdf2Cyto.determineLabel(model, externalResources, predicate);
			String propertyPart = literalProperty + " \"";
			String literalPart = statement.getLiteral().getLexicalForm();
			String dataTypePart = "\"^^" + statement.getLiteral().getDatatypeURI().replaceAll(Vocabulary.XSD_PREFIX, "xsd:");
			literalProperty = propertyPart + literalPart + dataTypePart;
			TreeNode literal = new TreeNode();
			literal.setId(treeNode.getId() + "-->" + literalProperty.replaceAll("\\s", "-"));
			literal.setText(literalProperty);
			literal.setLabel(literalProperty);
			literal.setName("<span class=\"literallabel\">" + propertyPart + "<span class=\"literalpart\">" + literalPart + "</span>" + dataTypePart + "</span>");
			literal.setColor("#ffffff");
			literal.setDetail(Rdf2Cyto.createPopup(model, externalResources, predicate));
			Set<String> domains = getDomains(mergedModel, predicate);
			domains.add("LITERAL");
			literal.setDomains(domains);
			literals.add(literal);
		}

		if (externalResources != null && !externalResources.isEmpty())
		{
			stmtIterator = externalResources.listStatements(resource, (Property) null, (RDFNode) null);
			while (stmtIterator.hasNext())
			{
				Statement statement = stmtIterator.next();
				RDFNode object = statement.getObject();
				if (!object.isLiteral())
				{
					continue;
				}

				String literalProperty = StringUtils.EMPTY;
				Resource predicate = statement.getPredicate().asResource();
				literalProperty = Rdf2Cyto.determineLabel(model, externalResources, predicate);
				String propertyPart = literalProperty + " \"";
				String literalPart = statement.getLiteral().getLexicalForm();
				String dataTypePart = "\"^^" + statement.getLiteral().getDatatypeURI().replaceAll(Vocabulary.XSD_PREFIX, "xsd:");
				literalProperty = propertyPart + literalPart + dataTypePart;
				TreeNode literal = new TreeNode();
				literal.setId(treeNode.getId() + "-->" + literalProperty.replaceAll("\\s", "-"));
				literal.setText(literalProperty);
				literal.setName("<span class=\"literallabel\">" + propertyPart + "<span class=\"literalpart\">" + literalPart + "</span>" + dataTypePart + "</span>");
				literal.setColor("#ffffff");
				literal.setLabel(literalProperty);
				literal.setDetail(Rdf2Cyto.createPopup(model, externalResources, predicate));
				Set<String> domains = getDomains(mergedModel, predicate);
				domains.add("LITERAL");
				literal.setDomains(domains);
				literals.add(literal);
			}
		}
		Set<TreeNode> children = treeNode.getChildren();
		if (children == null)
		{
			children = new HashSet<TreeNode>();
		}
		children.addAll(literals);
		treeNode.setChildren(children);
		return treeNode;
	}
}
