/*******************************************************************
*               Notice
*
* Copyright Osthus GmbH, All rights reserved.
*
* This software is part of the Allotrope ADM project
*
* Address: Osthus GmbH
*        : Eisenbahnweg 9
*        : 52068 Aachen
*        : Germany
*
*******************************************************************/

package processor.tree;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 */
public enum DirectionEnum
{
	FORWARD, BACKWARD;
}
