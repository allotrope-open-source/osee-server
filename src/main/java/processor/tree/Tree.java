package processor.tree;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;

import processor.Rdf2Cyto;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class Tree
{
	public static Set<TreeNode> create(Model model, Model externalResources)
	{
		Set<TreeNode> tree = new HashSet<TreeNode>();
		Set<Resource> visited = new HashSet<Resource>();
		StmtIterator stmtIterator = model.listStatements((Resource) null, (Property) null, (RDFNode) null);
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			Resource subject = statement.getSubject();
			if (visited.contains(subject))
			{
				continue;
			}

			TreeNode treeNode = new TreeNode();
			if (subject.isURIResource())
			{
				treeNode.setId(subject.getURI());
			}
			else
			{
				treeNode.setId(subject.asNode().getBlankNodeLabel());
			}
			treeNode.setLabel(Rdf2Cyto.determineLabel(model, externalResources, subject));

			treeNode = addLiterals(model, externalResources, subject, treeNode);
			treeNode = addRelations(model, externalResources, subject, treeNode);

			tree.add(treeNode);
			visited.add(subject);
		}
		return tree;
	}

	private static TreeNode addRelations(Model model, Model externalResources, Resource subject, TreeNode treeNode)
	{
		Set<String> relations = new HashSet<String>();
		StmtIterator stmtIterator = model.listStatements(subject, (Property) null, (RDFNode) null);
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			RDFNode object = statement.getObject();
			if (object.isLiteral())
			{
				continue;
			}

			String relationProperty = StringUtils.EMPTY;
			Resource predicate = statement.getPredicate().asResource();
			relationProperty = Rdf2Cyto.determineLabel(model, externalResources, predicate);
			if (object.isURIResource())
			{
				relationProperty = relationProperty + " " + object.asResource().getURI();
			}
			else
			{
				relationProperty = relationProperty + " " + object.asNode().getBlankNodeLabel();
			}

			relations.add(relationProperty);
		}

		treeNode.setRelations(relations);
		return treeNode;
	}

	private static TreeNode addLiterals(Model model, Model externalResources, Resource subject, TreeNode treeNode)
	{
		Set<String> literals = new HashSet<String>();
		StmtIterator stmtIterator = model.listStatements(subject, (Property) null, (RDFNode) null);
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			RDFNode object = statement.getObject();
			if (!object.isLiteral())
			{
				continue;
			}

			String literalProperty = StringUtils.EMPTY;
			Resource predicate = statement.getPredicate().asResource();
			literalProperty = Rdf2Cyto.determineLabel(model, externalResources, predicate);
			literalProperty = literalProperty + " " + statement.getLiteral().getLexicalForm();
			literals.add(literalProperty);
		}

		treeNode.setLiterals(literals);
		return treeNode;
	}
}
