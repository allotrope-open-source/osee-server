package processor.cytojo;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class CytoEdgeData extends CytoData
{
	String source = "";
	String target = "";
	String curveStyle = "bezier";
	String lineWidth = "2";
	String lineColor = "#000000";
	String color = "#000000";
	String IRI = "";
	String supertype = "";

	public String getSource()
	{
		return source;
	}

	public void setSource(String source)
	{
		this.source = source;
	}

	public String getTarget()
	{
		return target;
	}

	public void setTarget(String target)
	{
		this.target = target;
	}

	public String getCurveStyle()
	{
		return curveStyle;
	}

	public void setCurveStyle(String curveStyle)
	{
		this.curveStyle = curveStyle;
	}

	public String getLineWidth()
	{
		return lineWidth;
	}

	public void setLineWidth(String lineWidth)
	{
		this.lineWidth = lineWidth;
	}

	public String getLineColor()
	{
		return lineColor;
	}

	public void setLineColor(String lineColor)
	{
		this.lineColor = lineColor;
	}

	public String getColor()
	{
		return color;
	}

	public void setColor(String color)
	{
		this.color = color;
	}

	public CytoEdgeData(String id, String label, String source, String target)
	{
		super(id, label);
		this.source = source;
		this.target = target;
	}

	public String getIRI()
	{
		return IRI;
	}

	public void setIRI(String iRI)
	{
		IRI = iRI;
	}

	public String getSupertype()
	{
		return supertype;
	}

	public void setSupertype(String supertype)
	{
		this.supertype = supertype;
	}
}
