package processor.cytojo;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class CytoEdge extends CytoEntity
{
	CytoEdgeData data;
	String group = "edges";
	boolean pointsToLiteral = false;

	public CytoEdge(CytoEdgeData edgeData)
	{
		super();
		data = edgeData;
	}

	public CytoEdgeData getData()
	{
		return data;
	}

	public void setData(CytoEdgeData edgeData)
	{
		data = edgeData;
	}

	public String getGroup()
	{
		return group;
	}

	public void setGroup(String group)
	{
		this.group = group;
	}

	public boolean pointsToLiteral()
	{
		return pointsToLiteral;
	}

	public void setPointsToLiteral(boolean pointsToLiteral)
	{
		this.pointsToLiteral = pointsToLiteral;
	}
}
