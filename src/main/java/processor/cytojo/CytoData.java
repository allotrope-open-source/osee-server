package processor.cytojo;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class CytoData
{
	String id = "";
	String label = "";
	String detail = "";
	String visibility = "visible";

	public CytoData(String id, String label)
	{
		super();
		this.id = id;
		this.label = label;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getLabel()
	{
		return label;
	}

	public void setLabel(String label)
	{
		this.label = label;
	}

	public String getVisibility()
	{
		return visibility;
	}

	public void setVisibility(String visibility)
	{
		this.visibility = visibility;
	}

	public String getDetail()
	{
		return detail;
	}

	public void setDetail(String detail)
	{
		this.detail = detail;
	}

}
