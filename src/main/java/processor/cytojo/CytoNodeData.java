package processor.cytojo;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class CytoNodeData extends CytoData
{
	String parent = null;
	String bg = "#ffffff";
	String shape = "roundrectangle";
	String popup = "";
	String borderwidth = "2px";

	public String getParent()
	{
		return parent;
	}

	public void setParent(String parent)
	{
		this.parent = parent;
	}

	public String getBg()
	{
		return bg;
	}

	public void setBg(String bg)
	{
		this.bg = bg;
	}

	public String getShape()
	{
		return shape;
	}

	public void setShape(String shape)
	{
		this.shape = shape;
	}

	public String getPopup()
	{
		return popup;
	}

	public void setPopup(String popup)
	{
		this.popup = popup;
	}

	public String getBorderwidth()
	{
		return borderwidth;
	}

	public void setBorderwidth(String borderwidth)
	{
		this.borderwidth = borderwidth;
	}

	public CytoNodeData(String id, String label)
	{
		super(id, label);
	}

}
