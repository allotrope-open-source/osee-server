package processor.cytojo;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class CytoNode extends CytoEntity
{
	CytoNodeData data;
	String group = "nodes";
	Position position = new Position(0.0f, 0.0f);

	public CytoNode(CytoNodeData nodeData)
	{
		super();
		data = nodeData;
	}

	public Position getPosition()
	{
		return position;
	}

	public void setPosition(Position position)
	{
		this.position = position;
	}

	public CytoNodeData getData()
	{
		return data;
	}

	public void setData(CytoNodeData nodeData)
	{
		data = nodeData;
	}

	public String getGroup()
	{
		return group;
	}

	public void setGroup(String group)
	{
		this.group = group;
	}
}
