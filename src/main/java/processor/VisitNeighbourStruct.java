package processor;

import java.util.Set;

import org.apache.jena.rdf.model.Statement;

import processor.cytojo.CytoEntity;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class VisitNeighbourStruct
{
	Set<CytoEntity> cytoEntities;
	Set<Statement> visited;
	Set<String> resources;

	public VisitNeighbourStruct(Set<CytoEntity> cytoEntities, Set<Statement> visited, Set<String> resources)
	{
		super();
		this.cytoEntities = cytoEntities;
		this.visited = visited;
		this.resources = resources;
	}

	public Set<CytoEntity> getCytoEntities()
	{
		return cytoEntities;
	}

	public void setCytoEntities(Set<CytoEntity> cytoEntities)
	{
		this.cytoEntities = cytoEntities;
	}

	public Set<Statement> getVisited()
	{
		return visited;
	}

	public void setVisited(Set<Statement> visited)
	{
		this.visited = visited;
	}

	public Set<String> getResources()
	{
		return resources;
	}

	public void setResources(Set<String> resources)
	{
		this.resources = resources;
	}

}
