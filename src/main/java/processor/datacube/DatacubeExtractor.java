package processor.datacube;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.allotrope.adf.dc.config.selection.DataSelectionConfig;
import org.allotrope.adf.dc.config.selection.PropertyOrFirstDimension;
import org.allotrope.adf.dc.model.ComplexDataObject;
import org.allotrope.adf.dc.model.DataCube;
import org.allotrope.adf.dc.model.DataCubeDimensionComponent;
import org.allotrope.adf.dc.model.DataCubeMeasureComponent;
import org.allotrope.adf.dc.model.DataSelection;
import org.allotrope.adf.dc.model.DataType;
import org.allotrope.adf.dc.service.DataCubeService;
import org.allotrope.adf.enums.StandardDataType;
import org.allotrope.adf.model.AdfFile;
import org.allotrope.adf.service.AdfService;
import org.allotrope.adf.service.AdfServiceFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.logging.log4j.Logger;

import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.osthus.jena.repackaged.iri.IRIFactory;

import processor.Rdf2Cyto;
import processor.util.Vocabulary;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class DatacubeExtractor
{
	private static Property QUDT_NUMERIC_VALUE = ResourceFactory.createProperty(Vocabulary.QUDT_NUMERIC_VALUE.getURI());

	public static List<ExtractedCube> extractFrom(Path path, Model model, Model externalResources, Logger log)
	{
		log.info("Extracting data cubes...");
		List<ExtractedCube> cubes = new ArrayList<ExtractedCube>();
		try
		{
			AdfService adfService = AdfServiceFactory.create();
			StmtIterator stmtIterator = model.listStatements((Resource) null, Vocabulary.RDF_TYPE, Vocabulary.QB_DATASET);
			if (stmtIterator.hasNext())
			{
				try (AdfFile adfFile = adfService.openFile(path, true))
				{
					DataCubeService dcs = adfFile.getDataCubeService();
					while (stmtIterator.hasNext())
					{
						Statement statement = stmtIterator.next();
						Resource dc = statement.getSubject();
						log.info("Extracting data cube: " + dc.getURI());
						ExtractedCube edc = new ExtractedCube();
						edc.setId(dc.getURI());

						if (dc.hasProperty(Vocabulary.DCT_TITLE))
						{
							edc.setTitle(model.listStatements(dc, Vocabulary.DCT_TITLE, (RDFNode) null).next().getString());
						}

						DataCube rawdc = null;
						try
						{
							rawdc = dcs.openDataCube(IRIFactory.iriImplementation().create(dc.getURI()));
						}
						catch (Exception e)
						{
							log.info("Error opening datacube with IRI: " + dc.getURI());
							e.printStackTrace();
							continue;
						}

						HashMap<Integer, Boolean> dimIndex2Complex = new HashMap<Integer, Boolean>();

						DataCubeDimensionComponent[] dimensionComponents = rawdc.getDimensionComponents();
						for (int i = 0; i < dimensionComponents.length; i++)
						{
							DataCubeDimensionComponent dataCubeDimensionComponent = dimensionComponents[i];
							Property dimensionProperty = dataCubeDimensionComponent.getProperty();
							String dimensionPropertyUri = dimensionProperty.getURI();
							String dimensionPropertyLabel = Rdf2Cyto.determineLabel(model, externalResources, model.createProperty(dimensionPropertyUri));
							if (dimensionPropertyLabel.toLowerCase().contains("qb"))
							{
								// determine label from qb:concept
								if (model.listStatements(model.createResource(dimensionProperty.asResource().getURI()), Vocabulary.QB_CONCEPT, (RDFNode) null).hasNext())
								{
									Resource concept = model.listStatements(model.createResource(dimensionProperty.asResource().getURI()), Vocabulary.QB_CONCEPT, (RDFNode) null).next().getResource();
									dimensionPropertyLabel = Rdf2Cyto.determineLabel(model, externalResources, concept);
								}
							}
							DataType datatype = dataCubeDimensionComponent.getDataType();
							String dimensionUnitLabel = StringUtils.EMPTY;
							if (datatype != null)
							{
								Resource dimensionShape = model.getResource(datatype.getIRI().toString());
								if (dimensionShape != null)
								{
									// first try to get label from shape
									Resource dimensionPropertyShape;
									if (model.listStatements(dimensionShape, Vocabulary.SHACL_PROPERTY, (RDFNode) null).hasNext())
									{
										StmtIterator propertyShapeIterator = model.listStatements(dimensionShape, Vocabulary.SHACL_PROPERTY, (RDFNode) null);
										while (propertyShapeIterator.hasNext())
										{
											Statement propertyShapeStatement = propertyShapeIterator.next();
											dimensionPropertyShape = propertyShapeStatement.getResource();
											Resource dimensionUnit;
											if (model.listStatements(dimensionPropertyShape, Vocabulary.SHACL_HAS_VALUE, (RDFNode) null).hasNext())
											{
												dimensionUnit = model.listStatements(dimensionPropertyShape, Vocabulary.SHACL_HAS_VALUE, (RDFNode) null).next().getResource();
												dimensionUnitLabel = Rdf2Cyto.determineLabel(model, externalResources, dimensionUnit);
												break;
											}
										}
									}

									if (dimensionUnitLabel.isEmpty())
									{
										// second try to get label from qudt:unit
										if (model.listStatements(model.createResource(dimensionProperty.asResource().getURI()), Vocabulary.QUDT_UNIT, (RDFNode) null).hasNext())
										{
											Resource dimensionUnit = model.listStatements(model.createResource(dimensionProperty.asResource().getURI()), Vocabulary.QUDT_UNIT, (RDFNode) null).next()
													.getResource();
											dimensionUnitLabel = Rdf2Cyto.determineLabel(model, externalResources, dimensionUnit);
										}
									}
									edc.getDimensionUnits().add(dimensionUnitLabel);
								}
							}
							edc.getDimensions().add(dimensionPropertyLabel);
							log.info("Extracted data cube dimension: " + i + " property: " + dimensionPropertyLabel + " unit: " + (dimensionUnitLabel.isEmpty() ? "undefined" : dimensionUnitLabel));

							if (datatype != null)
							{
								if (StandardDataType.DOUBLE.equals(datatype))
								{
									dimIndex2Complex.put(i, false);
								}
								else if (StandardDataType.FLOAT.equals(datatype))
								{
									dimIndex2Complex.put(i, false);
								}
								else if (StandardDataType.INT.equals(datatype))
								{
									dimIndex2Complex.put(i, false);
								}
								else if (StandardDataType.LONG.equals(datatype))
								{
									dimIndex2Complex.put(i, false);
								}
								else if (StandardDataType.STRING.equals(datatype))
								{
									dimIndex2Complex.put(i, false);
								}
								else
								{
									dimIndex2Complex.put(i, true);
								}
							}
						}

						HashMap<Integer, Boolean> measureIndex2Complex = new HashMap<Integer, Boolean>();

						Set<DataCubeMeasureComponent> measureComponents = rawdc.getMeasureComponents();
						int measureCount = 0;
						for (Iterator<DataCubeMeasureComponent> iterator = measureComponents.iterator(); iterator.hasNext();)
						{
							DataCubeMeasureComponent dataCubeMeasureComponent = iterator.next();
							Property measureProperty = dataCubeMeasureComponent.getProperty();
							String measurePropertyUri = measureProperty.getURI();
							String measurePropertyLabel = Rdf2Cyto.determineLabel(model, externalResources, model.createProperty(measurePropertyUri));
							if (measurePropertyLabel.toLowerCase().contains("qb"))
							{
								// determine label from qb:concept
								if (model.listStatements(model.createResource(measureProperty.asResource().getURI()), Vocabulary.QB_CONCEPT, (RDFNode) null).hasNext())
								{
									Resource concept = model.listStatements(model.createResource(measureProperty.asResource().getURI()), Vocabulary.QB_CONCEPT, (RDFNode) null).next().getResource();
									measurePropertyLabel = Rdf2Cyto.determineLabel(model, externalResources, concept);
								}
							}
							DataType datatype = dataCubeMeasureComponent.getDataType();
							String measureUnitLabel = StringUtils.EMPTY;
							if (datatype != null)
							{
								Resource measureShape = model.getResource(datatype.getIRI().toString());
								if (measureShape != null)
								{
									Resource measurePropertyShape;
									if (model.listStatements(measureShape, Vocabulary.SHACL_PROPERTY, (RDFNode) null).hasNext())
									{
										measurePropertyShape = model.listStatements(measureShape, Vocabulary.SHACL_PROPERTY, (RDFNode) null).next().getResource();
										Resource measureUnit;
										if (model.listStatements(measurePropertyShape, Vocabulary.SHACL_HAS_VALUE, (RDFNode) null).hasNext())
										{
											measureUnit = model.listStatements(measurePropertyShape, Vocabulary.SHACL_HAS_VALUE, (RDFNode) null).next().getResource();
											measureUnitLabel = Rdf2Cyto.determineLabel(model, externalResources, measureUnit);
										}
									}

									if (measureUnitLabel.isEmpty())
									{
										// second try to get label from qudt:unit
										if (model.listStatements(model.createResource(measureProperty.asResource().getURI()), Vocabulary.QUDT_UNIT, (RDFNode) null).hasNext())
										{
											Resource measureUnit = model.listStatements(model.createResource(measureProperty.asResource().getURI()), Vocabulary.QUDT_UNIT, (RDFNode) null).next()
													.getResource();
											measureUnitLabel = Rdf2Cyto.determineLabel(model, externalResources, measureUnit);
										}
									}
									edc.getMeasureUnits().add(measureUnitLabel);
								}
							}
							edc.getMeasures().add(measurePropertyLabel);
							measureIndex2Complex.put(measureCount, !measureUnitLabel.isEmpty());
							log.info("Extracted data cube measure: " + measureCount++ + " property: " + measurePropertyLabel + " unit: "
									+ (measureUnitLabel.isEmpty() ? "undefined" : measureUnitLabel));
						}

						for (int i = 0; i < dimensionComponents.length; i++)
						{
							log.info("Extracting numeric values for measure and dimension: " + i);
							DataSelection srcSelection = rawdc.createSelectionAll();

							int count = srcSelection.getValueCount();

							if (dimIndex2Complex.get(i)) // is complex
							{
								ComplexDataObject[] complexValues = new ComplexDataObject[count];
								DataSelection complexSelection = dcs.wrap(complexValues).createSelectionAll();

								DataSelectionConfig complexSrcConfig = rawdc.createSelection();
								int dimCount = (int) dimensionComponents[0].getCurrentSize();
								PropertyOrFirstDimension withDimensionIndex = complexSrcConfig.withDimensionIndex(0, 1, dimCount);
								for (int j = 1; j < dimensionComponents.length; j++)
								{
									dimCount = (int) dimensionComponents[j].getCurrentSize();
									withDimensionIndex = withDimensionIndex.withDimensionIndex(0, 1, dimCount);
								}
								DataSelection complexSrc = withDimensionIndex.finish();

								complexSelection.write(complexSrc);

								int stepsize = count / 1000;
								if (count <= 1000)
								{
									stepsize = 1;
								}
								List<Double> measureValues = new ArrayList<Double>();
								for (int j = 0; j < complexValues.length; j = j + stepsize)
								{
									ComplexDataObject dataCubeValue = complexValues[j];
									double dataCubeNumericValue = dataCubeValue.getDouble(QUDT_NUMERIC_VALUE);
									measureValues.add(dataCubeNumericValue);
								}

								if (edc.getMeasureValues() == null)
								{
									edc.setMeasureValues(new ArrayList<List<Double>>());
								}
								if (edc.getMeasureValues().isEmpty())
								{
									edc.getMeasureValues().add(new ArrayList<Double>());
								}

								for (int j = 0; j < measureCount; j = j + stepsize)
								{
									if (edc.getMeasureValues().size() <= j)
									{
										edc.getMeasureValues().add(new ArrayList<Double>());
									}
									edc.getMeasureValues().get(j).addAll(measureValues); // TODO: improve for more than 1 measure
								}

								Object[] dimensionValues = rawdc.getDimensionComponents()[i].getScale().getAllValues();
								List<Double> dimensionNumericValues = new ArrayList<Double>();
								stepsize = dimensionValues.length / 1000;
								if (dimensionValues.length <= 1000)
								{
									stepsize = 1;
								}
								for (int j = 0; j < dimensionValues.length; j = j + stepsize)
								{
									ComplexDataObject dataCubeValue = (ComplexDataObject) dimensionValues[j];
									double dataCubeNumericValue = dataCubeValue.getDouble(QUDT_NUMERIC_VALUE);
									dimensionNumericValues.add(dataCubeNumericValue);
								}

								if (edc.getDimensionValues() == null)
								{
									edc.setDimensionValues(new ArrayList<List<Double>>());
								}
								if (edc.getDimensionValues().isEmpty() || edc.getDimensionValues().size() <= i)
								{
									edc.getDimensionValues().add(new ArrayList<Double>());
								}
								edc.getDimensionValues().get(i).addAll(dimensionNumericValues);
							}
							else
							{
								double[] values = new double[count];
								DataSelection selection = dcs.wrap(values).createSelectionAll();

								DataSelectionConfig srcConfig = rawdc.createSelection();
								int dimCount = (int) dimensionComponents[0].getCurrentSize();
								PropertyOrFirstDimension withDimensionIndex = srcConfig.withDimensionIndex(0, 1, dimCount);
								for (int j = 1; j < dimensionComponents.length; j++)
								{
									dimCount = (int) dimensionComponents[j].getCurrentSize();
									withDimensionIndex = withDimensionIndex.withDimensionIndex(0, 1, dimCount);
								}
								DataSelection src = withDimensionIndex.finish();

								selection.write(src);

								int stepsize = count / 1000;
								if (count <= 1000)
								{
									stepsize = 1;
								}
								List<Double> measureValues = new ArrayList<Double>();
								for (int j = 0; j < values.length; j = j + stepsize)
								{
									measureValues.add(values[j]);
								}

								if (edc.getMeasureValues() == null)
								{
									edc.setMeasureValues(new ArrayList<List<Double>>());
								}
								if (edc.getMeasureValues().isEmpty())
								{
									edc.getMeasureValues().add(new ArrayList<Double>());
								}

								for (int j = 0; j < measureCount; j = j + stepsize)
								{
									if (edc.getMeasureValues().size() <= j)
									{
										edc.getMeasureValues().add(new ArrayList<Double>());
									}
									edc.getMeasureValues().get(j).addAll(measureValues); // TODO: improve for more than 1 measure
								}

								DataType scaleDataType = rawdc.getDimensionComponents()[i].getDataType();
								List<Double> dimensionNumericValues = new ArrayList<Double>();
								if (scaleDataType.equals(StandardDataType.INT))
								{
									int[] dimensionValues = rawdc.getDimensionComponents()[i].getScaleInt().getAllValues();

									stepsize = dimensionValues.length / 1000;
									if (dimensionValues.length <= 1000)
									{
										stepsize = 1;
									}
									for (int j = 0; j < dimensionValues.length; j = j + stepsize)
									{
										dimensionNumericValues.add((double) dimensionValues[j]);
									}
								}
								else if (scaleDataType.equals(StandardDataType.DOUBLE))
								{
									double[] dimensionValues = rawdc.getDimensionComponents()[i].getScaleDouble().getAllValues();

									stepsize = dimensionValues.length / 1000;
									if (dimensionValues.length <= 1000)
									{
										stepsize = 1;
									}
									for (int j = 0; j < dimensionValues.length; j = j + stepsize)
									{
										dimensionNumericValues.add(dimensionValues[j]);
									}
								}
								else
								{
									throw new IllegalStateException("Unhandled datatype: " + scaleDataType + " of scale of datacube with iri: " + dc.getURI());
								}

								if (edc.getDimensionValues() == null)
								{
									edc.setDimensionValues(new ArrayList<List<Double>>());
								}
								if (edc.getDimensionValues().isEmpty() || edc.getDimensionValues().size() <= i)
								{
									edc.getDimensionValues().add(new ArrayList<Double>());
								}
								edc.getDimensionValues().get(i).addAll(dimensionNumericValues);
							}
						}
						cubes.add(edc);
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return cubes;
	}
}
