package processor.datacube;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class ExtractedCube
{
	public String id;
	public String title;
	public List<String> measures = new ArrayList<String>(32);
	public List<String> dimensions = new ArrayList<String>(32);
	public List<String> measureUnits = new ArrayList<String>(32);
	public List<String> dimensionUnits = new ArrayList<String>(32);
	public List<List<Double>> dimensionValues = new ArrayList<List<Double>>();
	public List<List<Double>> measureValues = new ArrayList<List<Double>>();

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public List<String> getMeasures()
	{
		return measures;
	}

	public void setMeasures(List<String> measures)
	{
		this.measures = measures;
	}

	public List<String> getDimensions()
	{
		return dimensions;
	}

	public void setDimensions(List<String> dimensions)
	{
		this.dimensions = dimensions;
	}

	public List<String> getMeasureUnits()
	{
		return measureUnits;
	}

	public void setMeasureUnits(List<String> measureUnits)
	{
		this.measureUnits = measureUnits;
	}

	public List<String> getDimensionUnits()
	{
		return dimensionUnits;
	}

	public void setDimensionUnits(List<String> dimensionUnits)
	{
		this.dimensionUnits = dimensionUnits;
	}

	public List<List<Double>> getDimensionValues()
	{
		return dimensionValues;
	}

	public void setDimensionValues(List<List<Double>> dimensionValues)
	{
		this.dimensionValues = dimensionValues;
	}

	public List<List<Double>> getMeasureValues()
	{
		return measureValues;
	}

	public void setMeasureValues(List<List<Double>> measureValues)
	{
		this.measureValues = measureValues;
	}
}
