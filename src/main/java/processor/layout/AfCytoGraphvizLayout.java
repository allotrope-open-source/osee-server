package processor.layout;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import org.gephi.graph.api.Edge;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.Node;
import org.gephi.layout.spi.LayoutBuilder;
import org.gephi.layout.spi.LayoutProperty;
import org.icculus.chunky.gephigraphviz.DotProcessError;
import org.icculus.chunky.gephigraphviz.GraphvizLayout;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

import processor.Processor;

/**
 * Code based on https://github.com/chunky/gephi_graphviz/blob/master/src/org/icculus/chunky/gephigraphviz/GraphvizLayout.java
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class AfCytoGraphvizLayout extends GraphvizLayout
{
	// http://www.graphviz.org/doc/info/attrs.html
	private String algoName;
	private String dotBinary = "dot";
	private String rankDir = "RL";
	private String overlap = "scalexy";
	// private String overlap = "false";
	private Boolean concentrate = false;

	private Process dotprocess = null;

	private Graph graph;

	@Override
	public void initAlgo()
	{
		graph = graphModel.getGraphVisible();
		setConverged(false);
	}

	public AfCytoGraphvizLayout(LayoutBuilder layoutBuilder, String algoName)
	{
		super(layoutBuilder);
		this.algoName = algoName;
	}

	@Override
	public void goAlgo()
	{
		final StringBuffer dotfile = prepareDot(algoName, rankDir, concentrate, graph, true);

		// Call Graphviz
		// we are calling it directly. However, there is also a java binding
		// http://www.graphviz.org/pdf/gv.3java.pdf
		final List<String> cmd = new ArrayList<String>();
		cmd.add(dotBinary);
		cmd.add("-Tdot");
		final ProcessBuilder pb = new ProcessBuilder(cmd);

		try
		{
			dotprocess = pb.start();

			final OutputStream out = dotprocess.getOutputStream();
			{
				final BufferedWriter inputForGraphviz = new BufferedWriter(new PrintWriter(out));
				inputForGraphviz.append(dotfile);

				if (Processor.writeDot)
				{
					Path dotFilePath = Paths.get("dotfile");
					Files.deleteIfExists(dotFilePath);
					Files.createFile(dotFilePath);
					Files.write(dotFilePath, dotfile.toString().getBytes());
				}

				inputForGraphviz.flush();
				inputForGraphviz.close();
				out.flush();
				out.close();
			}

			processOutput(dotprocess);
		}
		catch (IOException ex)
		{
			JOptionPane.showMessageDialog(null, new DotProcessError(ex), "Graphviz process error", JOptionPane.ERROR_MESSAGE);
		}
		finally
		{
			if (dotprocess != null)
			{
				dotprocess.destroy();
				dotprocess = null;
			}
			setConverged(true);
		}
	}

	public static StringBuffer prepareDot(String algoName, String rankDir, boolean concentrate, Graph graph, boolean isNative)
	{
		// Prepare input
		final StringBuffer dotfile = new StringBuffer();

		dotfile.append("digraph g { \n");
		if (isNative)
		{
			dotfile.append("start = ").append(Processor.startseed).append(";\n");
			dotfile.append("compound = true; \n");
			dotfile.append("layout = \"").append(algoName).append("\";\n");
			dotfile.append("rankdir = \"").append(rankDir).append("\";\n");
			dotfile.append("overlap = \"").append(Processor.overlap).append("\";\n");
		}
		dotfile.append("graph [ranksep=0.01, nodesep=0.01, start =").append(Processor.startseed).append("]"); //
		if (isNative)
		{
			dotfile.append("\r\n    node  [width=2.0, height=2.0]\r\n"); //
			dotfile.append("    edge  [len=0.01]\r\n"); //
			dotfile.append("charset = \"UTF-8\";\n");
			dotfile.append("sep = \"+0.01,0.01\";\n");
			dotfile.append("forcelabels = true;\n");
			dotfile.append("mindist = 0.01;\n");
			dotfile.append("ratio = 1.0;\n");
			if (concentrate)
			{
				dotfile.append("concentrate=true;\n");
			}
		}
		else
		{
			dotfile.append(";\r\n"); //
		}

		for (final Node n : graph.getNodes())
		{
			dotfile.append(n.getId());
			dotfile.append(" [");
			dotfile.append("pos=\"").append(n.x()).append(',').append(n.y()).append('"');
			dotfile.append(", ");
			dotfile.append("label=\"").append(n.getLabel().replaceAll("\"", "")).append('\"');
			dotfile.append("];\n");
		}
		for (final Edge e : graph.getEdges())
		{
			final String edgearrow = e.isDirected() ? "->" : "--";

			dotfile.append(e.getSource().getId());
			dotfile.append(edgearrow);
			dotfile.append(e.getTarget().getId());
			dotfile.append(" [weight=");
			dotfile.append(e.getWeight());
			dotfile.append("];\n");
		}
		dotfile.append("}\n");
		return dotfile;
	}

	@Override
	public void endAlgo()
	{
		if (null != dotprocess)
		{
			dotprocess.destroy();
			dotprocess = null;
		}
	}

	@Override
	public LayoutProperty[] getProperties()
	{
		List<LayoutProperty> properties = new ArrayList<LayoutProperty>();
		try
		{
			properties.add(LayoutProperty.createProperty(this, String.class, NbBundle.getMessage(getClass(), "GraphvizLayout.algorithm.desc"), null, "GraphvizLayout.algorithm.name",
					NbBundle.getMessage(getClass(), "GraphvizLayout.algorithm.name"), "getAlgoName", "setAlgoName"));

			properties.add(LayoutProperty.createProperty(this, String.class, NbBundle.getMessage(getClass(), "GraphvizLayout.dotbinary.desc"), null, "GraphvizLayout.dotbinary.name",
					NbBundle.getMessage(getClass(), "GraphvizLayout.dotbinary.name"), "getDotBinary", "setDotBinary"));

			properties.add(LayoutProperty.createProperty(this, String.class, NbBundle.getMessage(getClass(), "GraphvizLayout.rankdir.desc"), null, "GraphvizLayout.rankdir.name",
					NbBundle.getMessage(getClass(), "GraphvizLayout.rankdir.name"), "getRankDir", "setRankDir"));

			properties.add(LayoutProperty.createProperty(this, String.class, NbBundle.getMessage(getClass(), "GraphvizLayout.overlap.desc"), null, "GraphvizLayout.overlap.name",
					NbBundle.getMessage(getClass(), "GraphvizLayout.overlap.name"), "getOverlap", "setOverlap"));

			properties.add(LayoutProperty.createProperty(this, Boolean.class, NbBundle.getMessage(getClass(), "GraphvizLayout.concentrate.desc"), null, "GraphvizLayout.concentrate.name",
					NbBundle.getMessage(getClass(), "GraphvizLayout.concentrate.name"), "isConcentrate", "setConcentrate"));
		}
		catch (MissingResourceException e)
		{
			Exceptions.printStackTrace(e);
		}
		catch (NoSuchMethodException e)
		{
			Exceptions.printStackTrace(e);
		}
		return properties.toArray(new LayoutProperty[0]);
	}

	@Override
	public void resetPropertiesValues()
	{
	}

	@Override
	public String getAlgoName()
	{
		return algoName;
	}

	@Override
	public void setAlgoName(String algoName)
	{
		this.algoName = algoName;
	}

	@Override
	public String getDotBinary()
	{
		return dotBinary;
	}

	@Override
	public void setDotBinary(String dotBinary)
	{
		this.dotBinary = dotBinary;
	}

	@Override
	public String getRankDir()
	{
		return rankDir;
	}

	@Override
	public void setRankDir(String rankDir)
	{
		this.rankDir = rankDir;
	}

	@Override
	public boolean isConcentrate()
	{
		return concentrate;
	}

	@Override
	public void setConcentrate(Boolean concentrate)
	{
		this.concentrate = concentrate;
	}

	@Override
	public String getOverlap()
	{
		return overlap;
	}

	@Override
	public void setOverlap(String overlap)
	{
		this.overlap = overlap;
	}

	private void processOutput(final Process dotprocess)
	{
		assert dotprocess != null;
		InputStream in = null;
		try
		{
			in = dotprocess.getInputStream();

			final BufferedReader outputFromGraphviz = new BufferedReader(new InputStreamReader(in));
			StringBuilder entireOutput = new StringBuilder();
			String line;
			while ((line = outputFromGraphviz.readLine()) != null)
			{
				entireOutput.append(line);
				entireOutput.append("\n");
			}

			// For some reason this one wasn't working
			// Node n = graph.getNode(nodeid);
			// ... so we map all nodes temporarily
			final Map<Integer, Node> nodeMapper = new HashMap<Integer, Node>();
			for (final Node currentNode : graph.getNodes())
			{
				nodeMapper.put(Integer.valueOf((String) currentNode.getId()), currentNode);
			}

			final String regex = "^\\s*(?<nodeid>\\d+)\\s+\\[.*?[, ]?pos=\"(?<pos>[^\"]+?)\".*?\\]";
			// System.out.println(entireOutput);
			final Pattern pat = Pattern.compile(regex, Pattern.MULTILINE | Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
			Matcher matcher = pat.matcher(entireOutput.toString());
			while (matcher.find())
			{
				Integer nodeid = Integer.valueOf(matcher.group("nodeid"));
				final Node n = nodeMapper.get(nodeid);
				if (null == n)
				{
					System.err.println("Cannot find nodeid " + nodeid);
					continue;
				}
				String pos = matcher.group("pos");
				String[] pair = pos.trim().split("[, ]");
				if (pair.length != 2)
				{
					System.err.println("Don't know what to do with coordinates != 2; " + pos);
					continue;
				}
				BigDecimal x_bd = new BigDecimal(pair[0]);
				BigDecimal y_bd = new BigDecimal(pair[1]);
				// System.out.println("Node " + nodeid + " : " + pos + " = " + x_bd.floatValue() + "," + y_bd.floatValue());
				n.setX(x_bd.floatValue());
				n.setY(y_bd.floatValue());
			}

		}
		catch (IOException e)
		{
			Exceptions.printStackTrace(e);
		}
		finally
		{
			try
			{
				if (in != null)
				{
					in.close();
				}
			}
			catch (IOException e)
			{
			}
		}

		InputStream err = null;
		try
		{
			// Dump any errors
			{
				err = dotprocess.getErrorStream();
				final InputStreamReader glue = new InputStreamReader(err);
				final BufferedReader errorsFromGraphviz = new BufferedReader(glue);
				String line;
				while ((line = errorsFromGraphviz.readLine()) != null)
				{
					{
						System.err.println(line);
					}
				}
			}
		}
		catch (IOException e)
		{
			Exceptions.printStackTrace(e);
		}
		finally
		{
			try
			{
				if (err != null)
				{
					err.close();
				}
			}
			catch (IOException e)
			{
			}
		}
	}

}
