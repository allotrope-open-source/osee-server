package processor.layout;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.StringUtils;
import org.apache.jena.rdf.model.Resource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.gephi.graph.api.DirectedGraph;
import org.gephi.graph.api.Edge;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.Node;
import org.gephi.io.exporter.api.ExportController;
import org.gephi.io.importer.api.Container;
import org.gephi.io.importer.api.ImportController;
import org.gephi.io.processor.plugin.DefaultProcessor;
import org.gephi.layout.plugin.AutoLayout;
import org.gephi.layout.plugin.circularlayout.circlelayout.CircleLayout;
import org.gephi.layout.plugin.circularlayout.circlelayout.CircleLayoutBuilder;
import org.gephi.layout.plugin.circularlayout.radialaxislayout.RadialAxisLayout;
import org.gephi.layout.plugin.circularlayout.radialaxislayout.RadialAxisLayoutBuilder;
import org.gephi.layout.plugin.force.StepDisplacement;
import org.gephi.layout.plugin.force.yifanHu.YifanHuLayout;
import org.gephi.layout.plugin.forceAtlas.ForceAtlasLayout;
import org.gephi.layout.plugin.forceAtlas2.ForceAtlas2;
import org.gephi.layout.plugin.noverlap.NoverlapLayout;
import org.gephi.layout.plugin.openord.OpenOrdLayout;
import org.gephi.layout.plugin.openord.OpenOrdLayoutBuilder;
import org.gephi.preview.api.PreviewController;
import org.gephi.preview.api.PreviewModel;
import org.gephi.preview.api.PreviewProperty;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.graphstream.algorithm.Toolkit;
import org.graphstream.graph.implementations.MultiGraph;
import org.graphstream.stream.file.FileSource;
import org.graphstream.stream.file.FileSourceDOT;
import org.graphstream.ui.graphicGraph.GraphPosLengthUtils;
import org.graphstream.ui.layout.springbox.implementations.SpringBox;
import org.openide.util.Lookup;

import processor.Processor;
import processor.Rdf2Cyto;
import processor.cytojo.CytoEdge;
import processor.cytojo.CytoEntity;
import processor.cytojo.CytoNode;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class Layouter
{
	private static final Logger log = LogManager.getLogger("Logger");
	private static Map<Resource, ConceptRelation> link2conceptRelations = new HashMap<Resource, ConceptRelation>();
	private static GraphModel graphModel;
	private static DirectedGraph directedGraph;
	private static Map<Node, CytoNode> node2cytonode;

	public static Set<CytoEntity> optimizeLayout(Set<CytoEntity> cytoEntities)
	{
		initLayouter(cytoEntities);
		if (Processor.useGraphStream)
		{
			return optimizeLayoutWithGraphStream(cytoEntities);
		}

		if (cytoEntities.size() <= 25000 && directedGraph.getNodeCount() <= 12405)
		{
			try
			{
				return optimizeLayoutWithGraphViz(cytoEntities);
			}
			catch (Exception e)
			{
				log.info("Layout with graphviz failed. Trying gephi...");
				return optimizeLayoutWithGephi(cytoEntities);
			}
		}
		else
		{
			return optimizeLayoutWithGephi(cytoEntities);
		}

	}

	private static void initLayouter(Set<CytoEntity> cytoEntities)
	{
		graphModel = null;
		directedGraph = null;
		node2cytonode = null;

		// Init a project - and therefore a workspace
		ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
		pc.newProject();
		Workspace workspace = pc.getCurrentWorkspace();

		// Generate a new random graph into a container
		Container container = Lookup.getDefault().lookup(Container.Factory.class).newContainer();
		// Append container to graph structure
		ImportController importController = Lookup.getDefault().lookup(ImportController.class);
		importController.process(container, new DefaultProcessor(), workspace);

		graphModel = Lookup.getDefault().lookup(GraphController.class).getGraphModel();
		directedGraph = graphModel.getDirectedGraph();

		log.info("Cytoscape entities: " + cytoEntities.size());
		log.info("Nodes: " + directedGraph.getNodeCount());
		log.info("Edges: " + directedGraph.getEdgeCount());

		node2cytonode = createGraphFromCytoEntities(cytoEntities, graphModel, directedGraph);

		if (Processor.breakCycles)
		{
			graphModel = determineEdgesToBreakCycles(directedGraph, graphModel);
			directedGraph = graphModel.getDirectedGraph();
		}

		log.debug("Graph coordinates before layout:");
		int i = 0;
		for (Node n : directedGraph.getNodes())
		{
			log.info(String.format("%5d  X Y %14.6f %14.6f %14.6f %10s", i++, n.x(), n.y(), n.size(), n.getLabel()));
		}
	}

	public static Set<CytoEntity> optimizeLayoutWithGephi(Set<CytoEntity> cytoEntities)
	{
		log.info("Optimizing layout with Gephi...");

		try
		{
			log.info("Layout in progress...");

			final long startTime = System.currentTimeMillis();

			AutoLayout.DynamicProperty adjustBySizeProperty = AutoLayout.createDynamicProperty("forceAtlas.adjustSizes.name", Boolean.TRUE, 0.1f);
			AutoLayout.DynamicProperty repulsionProperty = AutoLayout.createDynamicProperty("forceAtlas.repulsionStrength.name", new Double(2000.), 0f);
			AutoLayout.DynamicProperty intertiaProperty = AutoLayout.createDynamicProperty("forceAtlas.inertia.name", 10d, 0f);
			AutoLayout.DynamicProperty attractionStrengthProperty = AutoLayout.createDynamicProperty("forceAtlas.attractionStrength.name", 10d, 0f);
			AutoLayout.DynamicProperty maxDisplacementProperty = AutoLayout.createDynamicProperty("forceAtlas.maxDisplacement.name", 100d, 0f);
			AutoLayout.DynamicProperty freezeBalanceProperty = AutoLayout.createDynamicProperty("forceAtlas.freezeBalance.name", true, 0f);
			AutoLayout.DynamicProperty freezeStrengthProperty = AutoLayout.createDynamicProperty("forceAtlas.freezeStrength.name", 80d, 0f);
			AutoLayout.DynamicProperty freezeInertiaProperty = AutoLayout.createDynamicProperty("forceAtlas.freezeInertia.name", 0.2, 0f);
			AutoLayout.DynamicProperty gravityProperty = AutoLayout.createDynamicProperty("forceAtlas.gravity.name", 1d, 0f);
			AutoLayout.DynamicProperty outboundAttractionDistributionProperty = AutoLayout.createDynamicProperty("forceAtlas.outboundAttractionDistribution.name", false, 0f);
			AutoLayout.DynamicProperty speedProperty = AutoLayout.createDynamicProperty("forceAtlas.speed.name", 1d, 0f);

			AutoLayout.DynamicProperty optimalDistanceProperty = AutoLayout.createDynamicProperty("YifanHu.optimalDistance.name", 1000f, 0f);
			AutoLayout.DynamicProperty relativeStrengthProperty = AutoLayout.createDynamicProperty("YifanHu.relativeStrength.name", 0.2f, 0f);
			AutoLayout.DynamicProperty initialStepSizeProperty = AutoLayout.createDynamicProperty("YifanHu.initialStepSize.name", 200f, 0f);
			AutoLayout.DynamicProperty stepRatioProperty = AutoLayout.createDynamicProperty("YifanHu.stepRatio.name", 0.99f, 0f);
			AutoLayout.DynamicProperty adaptativeCoolingProperty = AutoLayout.createDynamicProperty("YifanHu.adaptativeCooling.name", true, 0f);
			AutoLayout.DynamicProperty convergenceThresholdProperty = AutoLayout.createDynamicProperty("YifanHu.convergenceThreshold.name", 1e-4f, 0f);
			AutoLayout.DynamicProperty quadTreeMaxLevelProperty = AutoLayout.createDynamicProperty("YifanHu.quadTreeMaxLevel.name", 10, 0f);
			AutoLayout.DynamicProperty thetaProperty = AutoLayout.createDynamicProperty("YifanHu.theta.name", 1.0f, 0f);

			AutoLayout.DynamicProperty scalingRatioProperty = AutoLayout.createDynamicProperty("ForceAtlas2.scalingRatio.name", 2.0, 0f);
			AutoLayout.DynamicProperty strongGravityModeProperty = AutoLayout.createDynamicProperty("ForceAtlas2.strongGravityMode.name", false, 0f);
			AutoLayout.DynamicProperty gravity2Property = AutoLayout.createDynamicProperty("ForceAtlas2.gravity.name", 0.15, 0f);
			AutoLayout.DynamicProperty distributedAttractionProperty = AutoLayout.createDynamicProperty("ForceAtlas2.distributedAttraction.name", false, 0f);
			AutoLayout.DynamicProperty linLogModeProperty = AutoLayout.createDynamicProperty("ForceAtlas2.linLogMode.name", true, 0f);
			AutoLayout.DynamicProperty adjustSizes2Property = AutoLayout.createDynamicProperty("ForceAtlas2.adjustSizes.name", true, 0f);
			AutoLayout.DynamicProperty edgeWeightInfluenceProperty = AutoLayout.createDynamicProperty("ForceAtlas2.edgeWeightInfluence.name", 1.0, 0f);
			AutoLayout.DynamicProperty jitterToleranceProperty = AutoLayout.createDynamicProperty("ForceAtlas2.jitterTolerance.name", 1.0, 0f);
			AutoLayout.DynamicProperty barnesHutOptimizationProperty = AutoLayout.createDynamicProperty("ForceAtlas2.barnesHutOptimization.name", true, 0f);
			AutoLayout.DynamicProperty barnesHutThetaProperty = AutoLayout.createDynamicProperty("ForceAtlas2.barnesHutTheta.name", 1.2, 0f);
			AutoLayout.DynamicProperty threadsProperty = AutoLayout.createDynamicProperty("ForceAtlas2.threads.name", 4, 0f);

			AutoLayout.DynamicProperty edgecutProperty = AutoLayout.createDynamicProperty("OpenOrd.properties.edgecut.name", 0.05f, 0f);
			AutoLayout.DynamicProperty numthreadsProperty = AutoLayout.createDynamicProperty("OpenOrd.properties.numthreads.name", 4, 0f);
			AutoLayout.DynamicProperty numiterationsProperty = AutoLayout.createDynamicProperty("OpenOrd.properties.numiterations.name", 850, 0f);
			AutoLayout.DynamicProperty realtimeProperty = AutoLayout.createDynamicProperty("OpenOrd.properties.realtime.name", 0.2f, 0f);
			AutoLayout.DynamicProperty seedProperty = AutoLayout.createDynamicProperty("OpenOrd.properties.seed.name", 6308223534l, 0f);
			AutoLayout.DynamicProperty liquidProperty = AutoLayout.createDynamicProperty("OpenOrd.properties.stage.liquid.name", 4, 0f);
			AutoLayout.DynamicProperty expansionProperty = AutoLayout.createDynamicProperty("OpenOrd.properties.stage.expansion.name", 4, 0f);
			AutoLayout.DynamicProperty cooldownProperty = AutoLayout.createDynamicProperty("OpenOrd.properties.stage.cooldown.name", 4, 0f);
			AutoLayout.DynamicProperty crunchProperty = AutoLayout.createDynamicProperty("OpenOrd.properties.stage.crunch.name", 4, 0f);
			AutoLayout.DynamicProperty simmerProperty = AutoLayout.createDynamicProperty("OpenOrd.properties.stage.simmer.name", 4, 0f);

			if (cytoEntities.size() > 1000000)
			{
				log.info("1st pass OpenORD, 2nd pass noverlap... stopping after 20 minutes");
				AutoLayout autoLayout = new AutoLayout(20, TimeUnit.MINUTES);
				autoLayout.setGraphModel(graphModel);
				OpenOrdLayout openOrdLayout = new OpenOrdLayout(new OpenOrdLayoutBuilder());
				NoverlapLayout noverlapLayout = new NoverlapLayout(null);
				autoLayout.addLayout(openOrdLayout, 0.99f, new AutoLayout.DynamicProperty[] { edgecutProperty, numiterationsProperty, realtimeProperty, seedProperty });
				autoLayout.addLayout(noverlapLayout, 0.01f);
				autoLayout.execute();
			}
			else
			{
				AutoLayout autoLayout = new AutoLayout(1, TimeUnit.SECONDS);
				String message = "1st pass forceatlas2, 2nd pass noverlap...stopping after ";
				if (cytoEntities.size() > 500000)
				{
					autoLayout = new AutoLayout(900, TimeUnit.SECONDS);
					log.info(message + "900 seconds");
				}
				else if (cytoEntities.size() > 100000)
				{
					autoLayout = new AutoLayout(600, TimeUnit.SECONDS);
					log.info(message + "600 seconds");
				}
				else if (cytoEntities.size() > 50000)
				{
					autoLayout = new AutoLayout(300, TimeUnit.SECONDS);
					log.info(message + "300 seconds");
				}
				else if (cytoEntities.size() > 10000)
				{
					autoLayout = new AutoLayout(100, TimeUnit.SECONDS);
					log.info(message + "100 seconds");
				}
				else if (cytoEntities.size() > 5000)
				{
					autoLayout = new AutoLayout(50, TimeUnit.SECONDS);
					log.info(message + "50 seconds");
				}
				else if (cytoEntities.size() > 1000)
				{
					autoLayout = new AutoLayout(30, TimeUnit.SECONDS);
					log.info(message + "30 seconds");
				}
				else
				{
					log.info(message + "1 seconds");
				}

				autoLayout.setGraphModel(graphModel);
				// YifanHuLayout yifanHuLayout = new YifanHuLayout(null, new StepDisplacement(100f));
				// ForceAtlasLayout forceAtlasLayout = new ForceAtlasLayout(null);
				ForceAtlas2 forceAtlas2Layout = new ForceAtlas2(null);
				NoverlapLayout noverlapLayout = new NoverlapLayout(null);

				// autoLayout.addLayout(yifanHuLayout, 0.99f, new AutoLayout.DynamicProperty[] { optimalDistanceProperty, relativeStrengthProperty,
				// initialStepSizeProperty, stepRatioProperty,
				// adaptativeCoolingProperty, convergenceThresholdProperty, quadTreeMaxLevelProperty, thetaProperty });
				// autoLayout.addLayout(yifanHuLayout, 0.99f);

				// autoLayout.addLayout(forceAtlasLayout, 1.0f, new AutoLayout.DynamicProperty[] { adjustBySizeProperty, repulsionProperty, intertiaProperty,
				// attractionStrengthProperty,
				// maxDisplacementProperty, freezeBalanceProperty, freezeStrengthProperty, freezeInertiaProperty, outboundAttractionDistributionProperty,
				// gravityProperty, speedProperty });

				autoLayout.addLayout(forceAtlas2Layout, 0.99f, new AutoLayout.DynamicProperty[] { scalingRatioProperty, strongGravityModeProperty, gravity2Property, distributedAttractionProperty,
						linLogModeProperty, adjustSizes2Property, edgeWeightInfluenceProperty, jitterToleranceProperty, barnesHutOptimizationProperty, barnesHutThetaProperty });
				// autoLayout.addLayout(forceAtlas2Layout, 0.35f);

				autoLayout.addLayout(noverlapLayout, 0.01f);
				autoLayout.execute();
			}

			final long endTime = System.currentTimeMillis();
			log.info("Layouting with Gephi took: " + (endTime - startTime) + " ms");
		}
		catch (Exception e)
		{
			log.error("Error during layouting with Gephi toolkit.");
			e.printStackTrace();
			return cytoEntities;
		}
		log.info("Layout finished...");

		float minX = 0.0f;
		float minY = 0.0f;
		float maxX = 0.0f;
		float maxY = 0.0f;
		log.debug("Graph coordinates after layout:");
		int i = 0;
		for (Node n : directedGraph.getNodes())
		{
			log.debug(String.format("%5d  X Y SIZE %14.6f %14.6f %14.6f %10s", i++, n.x(), n.y(), n.size(), n.getLabel()));
			if (n.x() < minX)
			{
				minX = n.x();
			}
			if (n.y() < minY)
			{
				minY = n.y();
			}
			if (n.x() > maxX)
			{
				maxX = n.x();
			}
			if (n.y() > maxY)
			{
				maxY = n.y();
			}
		}
		log.info("min x: " + minX + " min y: " + minY + " max x: " + maxX + " max y: " + maxY);

		log.info("Coordinates after layouting and transformation:");
		i = 0;
		for (Node n : directedGraph.getNodes())
		{
			log.info(String.format("%5d X Y %14.6f %14.6f %14.6f %10s", i++, n.x() - minX + 100, n.y() - minY + 100, n.size(), n.getLabel()));
			CytoNode cytoNode = node2cytonode.get(n);
			cytoNode.getPosition().setX(n.x() - minX + 100);
			cytoNode.getPosition().setY(n.y() - minY + 100);
			cytoEntities.add(cytoNode);
		}

		// set linewidth depending on size of graph
		String lineWidth = "2";
		if (maxX > 2000 || maxY > 2000)
		{
			lineWidth = String.valueOf(Math.min(Math.max((int) maxX, (int) maxY) / 1000, 20));
		}
		for (Iterator<CytoEntity> iterator = cytoEntities.iterator(); iterator.hasNext();)
		{
			CytoEntity cytoEntity = iterator.next();
			if (cytoEntity instanceof CytoEdge)
			{
				CytoEdge cytoEdge = (CytoEdge) cytoEntity;
				cytoEdge.getData().setLineWidth(lineWidth);
				cytoEntities.add(cytoEdge);
			}
		}

		log.info("Node positions updated.");
		return cytoEntities;
	}

	public static Set<CytoEntity> optimizeLayoutWithGraphStream(Set<CytoEntity> cytoEntities)
	{
		log.info("Optimizing layout with GraphStream...");
		Path filePath = null;
		try
		{
			filePath = prepareInput();
		}
		catch (IOException e)
		{
			log.error("Error creating DOT file: " + (filePath != null ? filePath.toAbsolutePath().toString() : "null"));
			e.printStackTrace();
			return cytoEntities;
		}

		MultiGraph graph = new MultiGraph("multigraph");
		// graph.setAttribute("layout.force", 2);
		// graph.setAttribute("layout.stabilization-limit", 0.99);
		// graph.setAttribute("layout.gravity", 0.02);
		graph.setAttribute("layout.quality", 1);
		FileSource fs = new FileSourceDOT();
		fs.addSink(graph);
		try
		{
			fs.readAll(filePath.toAbsolutePath().toString());
		}
		catch (IOException e)
		{
			e.printStackTrace();
			log.error("Error loading DOT from file: " + filePath.toAbsolutePath().toString());
			return cytoEntities;
		}
		finally
		{
			fs.removeSink(graph);
			try
			{
				Files.deleteIfExists(filePath);
			}
			catch (IOException e)
			{
				log.error("Error deleting temporary DOT from file: " + filePath.toAbsolutePath().toString());
			}
		}

		try
		{
			graph.nodes().forEach(node -> {
				node.setAttribute("layout.weight", 30000); // for SpringBox
			});
			graph.edges().forEach(node -> {
				node.setAttribute("layout.weight", 200); // for SpringBox
			});

			log.info("Layout in progress...");
			final long startTime = System.currentTimeMillis();
			Toolkit.computeLayout(graph, new SpringBox(), 0.9);
			final long endTime = System.currentTimeMillis();
			log.info("Layouting with GraphStream took: " + (endTime - startTime) + " ms");
		}
		catch (Exception e)
		{
			log.error("Error during layouting with GraphStream.");
			e.printStackTrace();
			return cytoEntities;
		}
		log.info("Layout finished...");

		final double[] extrema = new double[4];
		log.debug("Graph coordinates after layout:");

		final AtomicInteger index = new AtomicInteger();
		graph.nodes().forEach(node -> {
			double n[] = GraphPosLengthUtils.nodePosition(node);
			log.debug(String.format("%5d X Y SIZE %14.6f %14.6f %14.6f %10s", index.getAndIncrement(), n[0], n[1], n[2], node.getAttribute("label")));
			if (n[0] < extrema[0])
			{
				extrema[0] = n[0];
			}
			if (n[1] < extrema[1])
			{
				extrema[1] = n[1];
			}
			if (n[0] > extrema[2])
			{
				extrema[2] = n[0];
			}
			if (n[1] > extrema[3])
			{
				extrema[3] = n[1];
			}
		});

		log.info("min x: " + extrema[0] + " min y: " + extrema[1] + " max x: " + extrema[2] + " max y: " + extrema[3]);

		log.info("Coordinates after layouting and transformation:");

		final AtomicInteger index2 = new AtomicInteger();
		graph.nodes().forEach(node -> {
			double n[] = GraphPosLengthUtils.nodePosition(node);
			log.info(String.format("%5d X Y %14.6f %14.6f %10s", index2.getAndIncrement(), n[0] - extrema[0] + 100, n[1] - extrema[1] + 100, node.getAttribute("label")));
			String nodeId = node.getId();
			Node graphNode = null;
			for (Node gn : node2cytonode.keySet())
			{
				if (((String) gn.getId()).equals(nodeId))
				{
					graphNode = gn;
					break;
				}
			}

			CytoNode cytoNode = node2cytonode.get(graphNode);
			cytoNode.getPosition().setX(n[0] - extrema[0] + 100);
			cytoNode.getPosition().setY(n[1] - extrema[1] + 100);
			cytoEntities.add(cytoNode);
		});

		// set linewidth depending on size of graph
		String lineWidth = "2";
		if (extrema[2] > 2000 || extrema[3] > 2000)
		{
			lineWidth = String.valueOf(Math.min(Math.max((int) extrema[2], (int) extrema[3]) / 1000, 20));
		}
		for (Iterator<CytoEntity> iterator = cytoEntities.iterator(); iterator.hasNext();)
		{
			CytoEntity cytoEntity = iterator.next();
			if (cytoEntity instanceof CytoEdge)
			{
				CytoEdge cytoEdge = (CytoEdge) cytoEntity;
				cytoEdge.getData().setLineWidth(lineWidth);
				cytoEntities.add(cytoEdge);
			}
		}

		log.info("Node positions updated.");
		return cytoEntities;
	}

	private static Path prepareInput() throws IOException
	{
		Path filePath = Files.createTempFile("graph-stream-", ".dot");
		log.info("Created temp file: " + filePath.toAbsolutePath().toString());
		String dotString = AfCytoGraphvizLayout.prepareDot("sfdp", "rl", true, graphModel.getGraphVisible(), false).toString();
		Files.write(filePath, dotString.getBytes(), StandardOpenOption.TRUNCATE_EXISTING);
		log.info("Prepared DOT file: " + filePath.toAbsolutePath().toString());
		return filePath;
	}

	public static Set<CytoEntity> optimizeLayoutWithGraphViz(Set<CytoEntity> cytoEntities)
	{
		log.info("Optimizing layout with GraphViz...");

		if (Processor.isAutoLayout)
		{
			graphModel = doAutoLayout(graphModel);
		}
		else if (Processor.isRadialLayout)
		{
			graphModel = doRadialAxisLayout(graphModel);
		}
		else if (Processor.isCircleLayout)
		{
			graphModel = doCircleLayout(graphModel);
		}
		else if (Processor.isGraphVizLayout)
		{
			graphModel = doGraphVizLayout(graphModel);
		}

		log.info("Layout finished. Optimized coordinates.");

		float minX = 0.0f;
		float minY = 0.0f;
		float maxX = 0.0f;
		float maxY = 0.0f;
		log.debug("Graph coordinates after layout:");
		int i = 0;
		for (Node n : directedGraph.getNodes())
		{
			log.debug(String.format("%5d  X Y SIZE %14.6f %14.6f %14.6f %10s", i++, n.x(), n.y(), n.size(), n.getLabel()));
			if (n.x() < minX)
			{
				minX = n.x();
			}
			if (n.y() < minY)
			{
				minY = n.y();
			}
			if (n.x() > maxX)
			{
				maxX = n.x();
			}
			if (n.y() > maxY)
			{
				maxY = n.y();
			}
		}
		log.info("min x: " + minX + " min y: " + minY + " max x: " + maxX + " max y: " + maxY);

		log.info("Coordinates after layouting and transformation:");
		i = 0;
		for (Node n : directedGraph.getNodes())
		{
			log.info(String.format("%5d X Y %14.6f %14.6f %14.6f %10s", i++, n.x() - minX + 100, n.y() - minY + 100, n.size(), n.getLabel()));
			CytoNode cytoNode = node2cytonode.get(n);
			cytoNode.getPosition().setX(n.x() - minX + 100);
			cytoNode.getPosition().setY(n.y() - minY + 100);
			cytoEntities.add(cytoNode);
		}

		// set linewidth depending on size of graph
		String lineWidth = "2";
		if (maxX > 2000 || maxY > 2000)
		{
			lineWidth = String.valueOf(Math.min(Math.max((int) maxX, (int) maxY) / 1000, 20));
		}
		for (Iterator<CytoEntity> iterator = cytoEntities.iterator(); iterator.hasNext();)
		{
			CytoEntity cytoEntity = iterator.next();
			if (cytoEntity instanceof CytoEdge)
			{
				CytoEdge cytoEdge = (CytoEdge) cytoEntity;
				cytoEdge.getData().setLineWidth(lineWidth);
				cytoEntities.add(cytoEdge);
			}
		}

		log.info("Layout finished.");

		if (Processor.writePdf)
		{
			log.info("Exporting snapshot to autolayout.pdf");
			PreviewModel previewModel = Lookup.getDefault().lookup(PreviewController.class).getModel();
			previewModel.getProperties().putValue(PreviewProperty.EDGE_CURVED, Boolean.FALSE);
			previewModel.getProperties().putValue(PreviewProperty.NODE_LABEL_FONT, previewModel.getProperties().getFontValue(PreviewProperty.NODE_LABEL_FONT).deriveFont(8));
			previewModel.getProperties().putValue(PreviewProperty.SHOW_NODE_LABELS, Boolean.TRUE);
			ExportController ec = Lookup.getDefault().lookup(ExportController.class);
			try
			{
				ec.exportFile(new File("autolayout.pdf"));
			}
			catch (IOException ex)
			{
				ex.printStackTrace();
			}
			log.info("Export done.");
		}
		return cytoEntities;
	}

	private static Map<Node, CytoNode> createGraphFromCytoEntities(Set<CytoEntity> cytoEntities, GraphModel graphModel, DirectedGraph directedGraph)
	{
		Map<CytoNode, Node> cytoNode2Node = new HashMap<>();
		Map<Node, CytoNode> node2CytoNode = new HashMap<>();
		long edgeCount = 0;
		long reused = 0;
		long added = 0;
		for (Iterator<CytoEntity> iterator = cytoEntities.iterator(); iterator.hasNext();)
		{
			CytoEntity cytoEntity = iterator.next();
			if (cytoEntity instanceof CytoNode)
			{
				continue;
			}

			edgeCount++;
			CytoEdge cytoEdge = (CytoEdge) cytoEntity;
			CytoNode from = Rdf2Cyto.getCytoNode(cytoEdge.getData().getSource(), cytoEntities);
			CytoNode to = Rdf2Cyto.getCytoNode(cytoEdge.getData().getTarget(), cytoEntities);
			float fromX = (float) from.getPosition().getX();
			float fromY = (float) from.getPosition().getY();
			float toX = (float) to.getPosition().getX();
			float toY = (float) to.getPosition().getY();
			float fromSize = 0.0f;
			float toSize = 0.0f;
			Node fromNode = null;

			if (!cytoNode2Node.containsKey(from))
			{
				fromNode = graphModel.factory().newNode(String.valueOf(cytoNode2Node.size()));
				fromNode.setLabel(from.getData().getLabel());
				if (fromSize > 0.0f && Processor.isAutoLayout)
				{
					fromNode.setSize(Math.max(Math.min(500.0f / fromSize, 100.0f), 500.0f));
				}
				else
				{
					fromNode.setSize(Processor.nodeSize);
				}

				fromNode.setX(fromX);
				fromNode.setY(fromY);
				fromNode.setZ(0.0f);
				cytoNode2Node.put(from, fromNode);
				node2CytoNode.put(fromNode, from);
				directedGraph.addNode(fromNode);
				added++;
			}
			else
			{
				fromNode = cytoNode2Node.get(from);
				reused++;
			}

			Node toNode = null;
			if (!cytoNode2Node.containsKey(to))
			{
				toNode = graphModel.factory().newNode(String.valueOf(cytoNode2Node.size()));
				toNode.setLabel(to.getData().getLabel());
				if (toSize > 0.0f && Processor.isAutoLayout)
				{
					toNode.setSize(Math.max(Math.min(500.0f / toSize, 100.0f), 500.0f));
				}
				else
				{
					toNode.setSize(Processor.nodeSize);
				}
				toNode.setX(toX);
				toNode.setY(toY);
				toNode.setZ(0.0f);
				cytoNode2Node.put(to, toNode);
				node2CytoNode.put(toNode, to);
				directedGraph.addNode(toNode);
				added++;
			}
			else
			{
				toNode = cytoNode2Node.get(to);
				reused++;
			}

			Edge edge = graphModel.factory().newEdge(fromNode, toNode, 0, true);
			edge.setWeight(0.1);
			edge.setLabel(from.getData().getLabel() + " --> " + to.getData().getLabel());
			directedGraph.addEdge(edge);
			Processor.log.debug(edgeCount + " added= " + added + " reused= " + reused + " total= " + (added + reused) + " " + from.getData().getId() + " " + to.getData().getId());
		}

		Set<CytoNode> lonelyNodes = Rdf2Cyto.checkForLonelyNodes(cytoEntities);
		if (!lonelyNodes.isEmpty())
		{
			for (Iterator<CytoNode> iterator = lonelyNodes.iterator(); iterator.hasNext();)
			{
				CytoNode cytoNode = iterator.next();
				if (!cytoNode2Node.containsKey(cytoNode))
				{
					float nodeSize = 0.0f;
					Node lonelyNode = graphModel.factory().newNode(String.valueOf(cytoNode2Node.size()));
					lonelyNode.setLabel(cytoNode.getData().getLabel());
					if (nodeSize > 0.0f && Processor.isAutoLayout)
					{
						lonelyNode.setSize(Math.max(Math.min(500.0f / nodeSize, 100.0f), 500.0f));
					}
					else
					{
						lonelyNode.setSize(Processor.nodeSize);
					}
					lonelyNode.setX((float) cytoNode.getPosition().getX());
					lonelyNode.setY((float) cytoNode.getPosition().getY());
					lonelyNode.setZ(0.0f);
					cytoNode2Node.put(cytoNode, lonelyNode);
					node2CytoNode.put(lonelyNode, cytoNode);
					directedGraph.addNode(lonelyNode);
					added++;
					Processor.log.debug(edgeCount + " added= " + added + " reused= " + reused + " total= " + (added + reused) + " lonely node " + cytoNode.getData().getId());
				}
			}
		}
		return node2CytoNode;
	}

	private static GraphModel determineEdgesToBreakCycles(DirectedGraph directedGraph, GraphModel graphModel)
	{
		Set<Edge> edgesToBreak = new HashSet<>();
		LinkedHashSet<Node> visited = new LinkedHashSet<>();
		BreakInfo breakInfo = new BreakInfo(visited, edgesToBreak);
		int numNodes = directedGraph.getNodes().toCollection().size();
		int nodeCounter = 1;
		for (Node startNode : directedGraph.getNodes())
		{
			log.info("start node (" + nodeCounter++ + "/" + numNodes + "): " + startNode.getLabel());
			visited = breakInfo.getVisited();
			visited.add(startNode);
			breakInfo.setVisitedNodes(visited);

			for (Edge outEdge : directedGraph.getOutEdges(startNode))
			{
				if (breakInfo.getEdgesToBreak().contains(outEdge))
				{
					log.info("skip broken edge: " + outEdge.getSource().getLabel() + "/" + outEdge.getSource().getId() + "->" + outEdge.getTarget().getLabel() + "/" + outEdge.getTarget().getId());
					continue;
				}

				if (visited.contains(outEdge.getTarget()))
				{
					log.info("skip start from visited node: " + outEdge.getTarget().getLabel() + "/" + outEdge.getTarget().getId());
					continue;
				}

				breakInfo = follow(startNode, outEdge.getTarget(), directedGraph, breakInfo);
			}

			for (Edge inEdge : directedGraph.getInEdges(startNode))
			{
				if (breakInfo.getEdgesToBreak().contains(inEdge))
				{
					log.info("skip broken edge: " + inEdge.getSource().getLabel() + "/" + inEdge.getSource().getId() + "->" + inEdge.getTarget().getLabel() + "/" + inEdge.getTarget().getId());
					continue;
				}

				if (visited.contains(inEdge.getSource()))
				{
					log.info("skip start from visited node: " + inEdge.getSource().getLabel() + "/" + inEdge.getSource().getId());
					continue;
				}

				breakInfo = follow(startNode, inEdge.getTarget(), directedGraph, breakInfo);
			}
		}

		graphModel = createGraphModelWithUpdatedDirectedGraph(directedGraph, edgesToBreak, graphModel);
		log.info("Found " + edgesToBreak.size() + " cycles.");
		return graphModel;
	}

	private static GraphModel createGraphModelWithUpdatedDirectedGraph(DirectedGraph directedGraph, Set<Edge> edgesToBreak, GraphModel graphModel)
	{
		if (edgesToBreak.size() > 0)
		{
			ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
			pc.newProject();
			Workspace workspace = pc.getCurrentWorkspace();
			Container container = Lookup.getDefault().lookup(Container.Factory.class).newContainer();
			ImportController importController = Lookup.getDefault().lookup(ImportController.class);
			importController.process(container, new DefaultProcessor(), workspace);
			GraphModel newGraphModel = Lookup.getDefault().lookup(GraphController.class).getGraphModel();
			DirectedGraph newDirectedGraph = newGraphModel.getDirectedGraph();
			for (Node node : directedGraph.getNodes())
			{
				Node newNode = newGraphModel.factory().newNode(node.getId());
				newNode.setLabel(node.getLabel());
				newNode.setSize(node.size());
				newNode.setX(node.x());
				newNode.setY(node.y());
				newNode.setZ(node.z());
				newDirectedGraph.addNode(newNode);
			}
			for (Edge edge : directedGraph.getEdges())
			{
				if (edgesToBreak.contains(edge))
				{
					continue;
				}

				Node fromNode = newDirectedGraph.getNode(edge.getSource().getId());
				Node toNode = newDirectedGraph.getNode(edge.getTarget().getId());
				Edge newEdge = newGraphModel.factory().newEdge(fromNode, toNode, 0, true);
				newEdge.setWeight(0.1);
				newEdge.setLabel(edge.getLabel());
				newDirectedGraph.addEdge(newEdge);
			}

			graphModel = newGraphModel;
		}
		return graphModel;
	}

	private static BreakInfo follow(Node startNode, Node currentNode, DirectedGraph directedGraph, BreakInfo breakInfo)
	{
		if (currentNode.equals(startNode))
		{
			return breakInfo;
		}

		LinkedHashSet<Node> visitedNodes = breakInfo.getVisited();
		Set<Edge> edgesToBreak = breakInfo.getEdgesToBreak();

		StringBuilder sb = new StringBuilder();
		if (log.isDebugEnabled())
		{
			sb.append("-> ");
			for (int i = 0; i < visitedNodes.size(); i++)
			{
				sb.append("    ");
			}
		}
		String indent = sb.toString();

		log.debug(indent + "(" + currentNode.getLabel() + "/" + currentNode.getId() + ") (" + printVisitedNodes(visitedNodes) + ")");
		for (Edge outEdge : directedGraph.getOutEdges(currentNode))
		{
			if (edgesToBreak.contains(outEdge))
			{
				log.info(
						indent + "skip broken edge: " + outEdge.getSource().getLabel() + "/" + outEdge.getSource().getId() + "->" + outEdge.getTarget().getLabel() + "/" + outEdge.getTarget().getId());
				continue;
			}
			Node nextNode = outEdge.getTarget();
			log.debug(indent + "(" + currentNode.getLabel() + "/" + currentNode.getId() + ") OUT: " + nextNode.getLabel() + " (" + printVisitedNodes(visitedNodes) + ")");

			if (startNode.equals(nextNode) && visitedNodes.size() >= 2 && !isFirstNeighbour(currentNode, visitedNodes))
			{
				log.info(indent + "Found cycle! Edge to break: " + startNode.getLabel() + "/" + startNode.getId() + " --> " + currentNode.getLabel() + "/" + currentNode.getId());
				edgesToBreak.add(outEdge);
				breakInfo.setEdgesToBreak(edgesToBreak);
				continue;
			}

			if (visitedNodes.contains(nextNode) && isCycleToNodeOnPath(nextNode, visitedNodes))
			{
				log.info(indent + "Found cycle! Edge to break: " + currentNode.getLabel() + "/" + currentNode.getId() + " --> " + nextNode.getLabel() + "/" + nextNode.getId());
				edgesToBreak.add(outEdge);
				breakInfo.setEdgesToBreak(edgesToBreak);
				continue;
			}

			if (visitedNodes.contains(nextNode))
			{
				continue;
			}

			visitedNodes.add(currentNode);
			breakInfo.setVisitedNodes(visitedNodes);

			breakInfo = follow(startNode, nextNode, directedGraph, breakInfo);
		}

		visitedNodes = breakInfo.getVisited();
		edgesToBreak = breakInfo.getEdgesToBreak();

		for (Edge inEdge : directedGraph.getInEdges(currentNode))
		{
			if (edgesToBreak.contains(inEdge))
			{
				log.info(indent + "skip broken edge: " + inEdge.getSource().getLabel() + "/" + inEdge.getSource().getId() + "->" + inEdge.getTarget().getLabel() + "/" + inEdge.getTarget().getId());
				continue;
			}
			Node nextNode = inEdge.getSource();
			log.debug(indent + "(" + currentNode.getLabel() + "/" + currentNode.getId() + ") IN : " + nextNode.getLabel() + " (" + printVisitedNodes(visitedNodes) + ")");

			if (startNode.equals(nextNode) && visitedNodes.size() >= 2 && !isFirstNeighbour(currentNode, visitedNodes))
			{
				log.info(indent + "Found cycle! Edge to break: " + startNode.getLabel() + "/" + startNode.getId() + " --> " + currentNode.getLabel() + "/" + currentNode.getId());
				edgesToBreak.add(inEdge);
				breakInfo.setEdgesToBreak(edgesToBreak);
				continue;
			}

			if (visitedNodes.contains(nextNode) && isCycleToNodeOnPath(nextNode, visitedNodes))
			{
				log.info(indent + "Found cycle! Edge to break: " + currentNode.getLabel() + "/" + currentNode.getId() + " --> " + nextNode.getLabel() + "/" + nextNode.getId());
				edgesToBreak.add(inEdge);
				breakInfo.setEdgesToBreak(edgesToBreak);
				continue;
			}

			if (visitedNodes.contains(nextNode))
			{
				continue;
			}

			visitedNodes.add(currentNode);
			breakInfo.setVisitedNodes(visitedNodes);

			breakInfo = follow(startNode, nextNode, directedGraph, breakInfo);
		}

		return breakInfo;
	}

	private static boolean isCycleToNodeOnPath(Node node, LinkedHashSet<Node> visitedNodes)
	{
		List<Node> nodesAsList = new ArrayList<>(visitedNodes);
		int currentPosition = visitedNodes.size();
		int nodePosition = nodesAsList.indexOf(node);
		if (currentPosition - nodePosition >= 4)
		{
			return true;
		}
		return false;
	}

	private static boolean isFirstNeighbour(Node currentNode, LinkedHashSet<Node> visitedNodes)
	{
		Iterator<Node> iterator = visitedNodes.iterator();
		if (!iterator.hasNext())
		{
			return false;
		}

		Node node = iterator.next();
		if (!iterator.hasNext())
		{
			return false;
		}

		node = iterator.next();
		if (node.equals(currentNode))
		{
			return true;
		}

		return false;
	}

	private static String printVisitedNodes(LinkedHashSet<Node> visitedNodes)
	{
		List<String> visitedNodeLabels = new ArrayList<>();
		for (Iterator<Node> iterator = visitedNodes.iterator(); iterator.hasNext();)
		{
			Node node = iterator.next();
			visitedNodeLabels.add(node.getLabel() + "/" + node.getId());
		}

		return StringUtils.join(visitedNodeLabels, " - ");
	}

	private static GraphModel doGraphVizLayout(GraphModel graphModel)
	{
		log.info("Computing layout with graphviz: " + Processor.graphVizAlgoName + ". Path to dot.exe: " + Processor.dotBinary);
		AfCytoGraphvizLayout graphvizLayout = new AfCytoGraphvizLayout(null, Processor.graphVizAlgoName);
		graphvizLayout.setGraphModel(graphModel);
		graphvizLayout.setDotBinary(Processor.dotBinary);

		graphvizLayout.initAlgo();
		for (int i = 0; i < 100 && graphvizLayout.canAlgo(); i++)
		{
			graphvizLayout.goAlgo();
		}
		graphvizLayout.endAlgo();
		return graphModel;
	}

	private static GraphModel doAutoLayout(GraphModel graphModel)
	{
		log.info("Autolayout with YifanHu and force atlas for " + Processor.layoutDuration + " seconds.");
		AutoLayout autoLayout = new AutoLayout(Processor.layoutDuration, TimeUnit.SECONDS);
		autoLayout.setGraphModel(graphModel);
		YifanHuLayout firstLayout = new YifanHuLayout(null, new StepDisplacement(1f));
		ForceAtlasLayout secondLayout = new ForceAtlasLayout(null);
		ForceAtlasLayout thirdLayout = new ForceAtlasLayout(null);

		AutoLayout.DynamicProperty optimalDistanceProperty = AutoLayout.createDynamicProperty("YifanHu.optimalDistance.name", 1000.0f, 0f);
		AutoLayout.DynamicProperty adjustBySizeProperty = AutoLayout.createDynamicProperty("forceAtlas.adjustSizes.name", Boolean.TRUE, 0.2f);
		AutoLayout.DynamicProperty repulsionProperty = AutoLayout.createDynamicProperty("forceAtlas.repulsionStrength.name", 500.0, 0f);
		AutoLayout.DynamicProperty strongerAdjustBySizeProperty = AutoLayout.createDynamicProperty("forceAtlas.adjustSizes.name", Boolean.TRUE, 0.9f);
		AutoLayout.DynamicProperty strongerRepulsionProperty = AutoLayout.createDynamicProperty("forceAtlas.repulsionStrength.name", 10000.0, 0f);
		AutoLayout.DynamicProperty strongerAutoStabProperty = AutoLayout.createDynamicProperty("forceAtlas.freezeStrength.name", 1000.0, 0f);
		autoLayout.addLayout(firstLayout, 0.7f, new AutoLayout.DynamicProperty[] { optimalDistanceProperty });
		autoLayout.addLayout(secondLayout, 0.25f, new AutoLayout.DynamicProperty[] { adjustBySizeProperty, repulsionProperty });
		autoLayout.addLayout(thirdLayout, 0.05f, new AutoLayout.DynamicProperty[] { strongerAdjustBySizeProperty, strongerRepulsionProperty, strongerAutoStabProperty });
		autoLayout.execute();
		return graphModel;
	}

	private static GraphModel doRadialAxisLayout(GraphModel graphModel)
	{
		log.info("Computing radial axis layout.");
		RadialAxisLayout radialAxisLayout = new RadialAxisLayout(new RadialAxisLayoutBuilder(), 100.0, false);
		radialAxisLayout.resetPropertiesValues();
		radialAxisLayout.setGraphModel(graphModel);
		radialAxisLayout.setResizeNode(true);
		radialAxisLayout.setSparSpiral(false);
		radialAxisLayout.setSparNodePlacement("NodeId"); // Random
		radialAxisLayout.setNodePlacementNoOverlap(true);
		radialAxisLayout.setSparCount(100);

		radialAxisLayout.initAlgo();
		for (int i = 0; i < 100 && radialAxisLayout.canAlgo(); i++)
		{
			radialAxisLayout.goAlgo();
		}
		radialAxisLayout.endAlgo();
		return graphModel;
	}

	private static GraphModel doCircleLayout(GraphModel graphModel)
	{
		log.info("Computing circle layout.");
		CircleLayout circleLayout = new CircleLayout(new CircleLayoutBuilder(), 100.0, false);
		circleLayout.resetPropertiesValues();
		circleLayout.setGraphModel(graphModel);
		circleLayout.initAlgo();
		for (int i = 0; i < 100 && circleLayout.canAlgo(); i++)
		{
			circleLayout.goAlgo();
		}
		circleLayout.endAlgo();
		return graphModel;
	}
}
