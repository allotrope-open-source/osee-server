package processor.layout;

import java.util.Set;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class TreeNode
{
	String id;
	String label;
	String text;
	String name;
	String color;
	String detail;
	Set<String> domains;

	Set<TreeNode> children;

	Set<String> literals;
	Set<String> relations;

	public TreeNode()
	{
		super();
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getLabel()
	{
		return label;
	}

	public void setLabel(String label)
	{
		this.label = label;
	}

	public Set<String> getLiterals()
	{
		return literals;
	}

	public void setLiterals(Set<String> literals)
	{
		this.literals = literals;
	}

	public Set<String> getRelations()
	{
		return relations;
	}

	public void setRelations(Set<String> relations)
	{
		this.relations = relations;
	}

	public String getText()
	{
		return text;
	}

	public void setText(String text)
	{
		this.text = text;
	}

	public Set<TreeNode> getChildren()
	{
		return children;
	}

	public void setChildren(Set<TreeNode> children)
	{
		this.children = children;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getColor()
	{
		return color;
	}

	public void setColor(String color)
	{
		this.color = color;
	}

	public String getDetail()
	{
		return detail;
	}

	public void setDetail(String detail)
	{
		this.detail = detail;
	}

	public Set<String> getDomains()
	{
		return domains;
	}

	public void setDomains(Set<String> domains)
	{
		this.domains = domains;
	}

}
