package processor.datapackage;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import org.allotrope.adf.dd.utils.RDFUtil;
import org.allotrope.adf.dp.model.DataPackage;
import org.allotrope.adf.dp.model.DpFile;
import org.allotrope.adf.dp.model.DpFolder;
import org.allotrope.adf.dp.model.DpNode;
import org.allotrope.adf.dp.service.DpInputStream;
import org.allotrope.adf.enums.DpNodeType;
import org.allotrope.adf.model.AdfFile;
import org.allotrope.adf.service.AdfService;
import org.allotrope.adf.service.AdfServiceFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.jfree.util.Log;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriUtils;

import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.vocabulary.DCTerms;

import adfsee.TempFolderCreator;
import processor.util.Zipper;
import restcontroller.ProcessController;

@Component
/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class AdfDatapackageExtractor
{
	protected static final String FILE_ATTRIBUTE_CREATION_TIME = "creationTime";
	protected static final String FILE_ATTRIBUTE_LAST_MODIFIED = "lastModifiedTime";
	protected static final String FILE_ATTRIBUTE_SIZE = "size";
	protected static final String BASE_PAV = "http://purl.org/pav/";
	protected static final Property PAV_RETRIEVED_FROM = ResourceFactory.createProperty(BASE_PAV + "retrievedFrom");
	protected static final Property PAV_RETRIEVED_ON = ResourceFactory.createProperty(BASE_PAV + "retrievedOn");
	protected static final Property PAV_RETRIEVED_BY = ResourceFactory.createProperty(BASE_PAV + "retrievedBy");
	protected static final Property PAV_CREATED_BY = ResourceFactory.createProperty(BASE_PAV + "createdBy");
	protected static final Property PAV_CREATED_ON = ResourceFactory.createProperty(BASE_PAV + "createdOn");

	public static DpEntity extractFrom(Path path, Logger log)
	{
		log.info("Extracting datapackage...");

		DpEntity extractedDatapackage = null;
		try
		{
			AdfService adfService = AdfServiceFactory.create();
			try (AdfFile adfFile = adfService.openFile(path, true))
			{
				DataPackage dataPackage = adfFile.getDataPackage();
				DpFolder rootFolder = dataPackage.openRootFolder();
				List<DpNode> nodes = rootFolder.contents();
				if (nodes == null || nodes.isEmpty())
				{
					return new DpEntity();
				}

				String targetFolder = TempFolderCreator.DOWNLOAD_FOLDER_PATH_NAME + "datapackage";
				Path targetPath = Paths.get(targetFolder);
				if (Files.isRegularFile(targetPath))
				{
					Files.deleteIfExists(targetPath);
				}
				else if (Files.isDirectory(targetPath))
				{
					FileUtils.cleanDirectory(targetPath.toFile());
					Files.delete(targetPath);
				}
				Files.createDirectory(targetPath);
				extractedDatapackage = exportDpFolderToDirectory(rootFolder, targetPath, Paths.get(rootFolder.getAbsolutePath()), dataPackage, 0, null);
				Zipper.compressZipfile(targetFolder, targetFolder + ".zip");
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return extractedDatapackage;
	}

	private static DpEntity exportDpFolderToDirectory(DpFolder source, Path target, Path sourceRoot, DataPackage dataPackage, int level, DpEntity parentEntity) throws IOException
	{
		if (parentEntity == null)
		{
			parentEntity = extractFolderMetadata(source);
			URI uri;
			try
			{
				uri = new URI(ProcessController.baseFileUrl + "datapackage.zip");
				parentEntity.setUrl(uri.toASCIIString());
			}
			catch (URISyntaxException e)
			{
				e.printStackTrace();
			}

		}
		List<DpEntity> children = new ArrayList<DpEntity>();

		String indentation = StringUtils.EMPTY;
		for (int i = 0; i < level; i++)
		{
			indentation = indentation + "    ";
		}

		Path absoluteTarget = target.toAbsolutePath();
		List<DpNode> contents = source.contents();
		for (DpNode dpNode : contents)
		{
			System.out.println(indentation + "--> " + dpNode.getName());
			Path pathToTargetNode = absoluteTarget.resolve(sourceRoot.relativize(Paths.get(dpNode.getAbsolutePath())));
			if (dpNode.getNodeType().equals(DpNodeType.DP_FILE))
			{
				DpFile dpFile = dataPackage.openFile(dpNode.getAbsolutePath());
				createSystemFile(dpFile, pathToTargetNode);
				DpEntityFile child = extractFileMetadata(pathToTargetNode, dpFile);
				children.add(child);
			}
			else
			{
				DpFolder dpFolder = dataPackage.openFolder(dpNode.getAbsolutePath());
				createDirectory(dpFolder, pathToTargetNode);

				DpEntity child = extractFolderMetadata(dpFolder);
				child = exportDpFolderToDirectory(dpFolder, absoluteTarget, sourceRoot, dataPackage, level++, child);
				children.add(child);
				level--;
			}
		}
		parentEntity.setChildren(children);
		return parentEntity;
	}

	private static DpEntityFile extractFileMetadata(Path pathToTargetNode, DpFile dpFile)
	{
		DpEntityFile child = new DpEntityFile();
		child.setName(dpFile.getName());
		child.setLabel(dpFile.getName());
		child.setText(dpFile.getName());
		child.setColor("#AAD5FF");
		child.setCharset((dpFile.getCharset() != null ? dpFile.getCharset().displayName() : ""));
		child.setCreatedBy((dpFile.getCreatedBy() != null ? dpFile.getCreatedBy() : ""));
		if (dpFile.getCreatedOn() > -1)
		{
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(dpFile.getCreatedOn());
			Date date = calendar.getTime();
			child.setCreatedOn(date.toString());
		}
		else
		{
			child.setCreatedOn(StringUtils.EMPTY);
		}

		child.setFormat((dpFile.getFormat() != null ? dpFile.getFormat().name() : ""));
		child.setLineSeparator((dpFile.getLineSeparator() != null ? dpFile.getLineSeparator().name() : ""));
		child.setMessageDigest((dpFile.getMessageDigest() != null ? dpFile.getMessageDigest() : ""));
		child.setMessageDigestAlgorithm((dpFile.getMessageDigestAlgorithm() != null ? dpFile.getMessageDigestAlgorithm().algorithm() : ""));
		child.setSize((dpFile.getSize() > -1 ? String.valueOf(dpFile.getSize()) : ""));
		child.setUpdatedBy((dpFile.getUpdatedBy() != null ? dpFile.getUpdatedBy() : ""));
		if (dpFile.getUpdatedOn() > -1)
		{
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(dpFile.getUpdatedOn());
			Date date = calendar.getTime();
			child.setUpdatedOn(date.toString());
		}
		else
		{
			child.setUpdatedOn(StringUtils.EMPTY);
		}

		child.setUuid((dpFile.getUuid() != null ? dpFile.getUuid() : ""));
		child.setId(child.getUuid());
		child.setCompressed(String.valueOf(dpFile.isCompressed()));
		child.setPath(dpFile.getAbsolutePath());
		try
		{
			String escapedFilePath = pathToTargetNode.toString().substring(TempFolderCreator.DOWNLOAD_FOLDER_PATH_NAME.length()).replaceAll(Pattern.quote(File.separator), "/").replaceAll("\\s", TempFolderCreator.SPACE_REPLACE);
			URI uri = new URI(ProcessController.baseFileUrl.toString() + escapedFilePath);
			child.setUrl(uri.toASCIIString());
		}
		catch (URISyntaxException e)
		{
			Log.error("Error during creating URI from file name: " + pathToTargetNode.toAbsolutePath().toString());
			e.printStackTrace();
		}
		return child;
	}

	private static DpEntity extractFolderMetadata(DpFolder dpFolder)
	{
		DpEntity child = new DpEntity();
		child.setName((dpFolder.getName() != null ? dpFolder.getName() : ""));
		child.setLabel(dpFolder.getName());
		child.setText(dpFolder.getName());
		child.setColor("#000000");
		child.setUuid((dpFolder.getUuid() != null ? dpFolder.getUuid() : ""));
		child.setId(child.getUuid());
		child.setCreatedBy((dpFolder.getCreatedBy() != null ? dpFolder.getCreatedBy() : ""));
		if (dpFolder.getCreatedOn() > -1)
		{
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(dpFolder.getCreatedOn());
			Date date = calendar.getTime();
			child.setCreatedOn(date.toString());
		}
		else
		{
			child.setCreatedOn(StringUtils.EMPTY);
		}
		child.setUpdatedBy((dpFolder.getUpdatedBy() != null ? dpFolder.getUpdatedBy() : ""));

		if (dpFolder.getUpdatedOn() > -1)
		{
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(dpFolder.getUpdatedOn());
			Date date = calendar.getTime();
			child.setUpdatedOn(date.toString());
		}
		else
		{
			child.setUpdatedOn(StringUtils.EMPTY);
		}
		child.setPath(dpFolder.getAbsolutePath());
		return child;
	}

	private static void createDirectory(DpFolder source, Path target) throws IOException
	{
		Files.createDirectories(target);
		reconstructProvenanceMetadata(source, target);
	}

	protected static void createSystemFile(DpFile source, Path target) throws IOException
	{
		// create file
		try (InputStream in = new DpInputStream(source))
		{
			Files.copy(in, target);
		}
		reconstructProvenanceMetadata(source, target);
	}

	protected static void reconstructProvenanceMetadata(DpNode source, Path target) throws IOException
	{
		Resource rdfResource = source.getRDFResource();

		// ---- reconstruct original file metadata
		Resource originalMetadata = rdfResource.getPropertyResourceValue(PAV_RETRIEVED_FROM);

		if (originalMetadata != null)
		{ // no metadata set if the node was originally not imported
			// original owner will not be used in export

			// set creation time
			Statement stmt = originalMetadata.getProperty(PAV_CREATED_ON);
			if (stmt != null)
			{
				Calendar createdOn = getTime(stmt.getObject());
				Files.setAttribute(target, FILE_ATTRIBUTE_CREATION_TIME, FileTime.fromMillis(createdOn.getTimeInMillis()));
			}

			// last modified
			stmt = originalMetadata.getProperty(DCTerms.modified);
			if (stmt != null)
			{
				Calendar lastModified = getTime(stmt.getObject());
				Files.setAttribute(target, FILE_ATTRIBUTE_LAST_MODIFIED, FileTime.fromMillis(lastModified.getTimeInMillis()));
			}
		}
	}

	public static Calendar getTime(RDFNode node)
	{
		return RDFUtil.toCalendar(node);
	}
}
