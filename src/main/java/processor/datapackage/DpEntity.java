package processor.datapackage;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class DpEntity
{
	@JsonInclude(Include.NON_NULL)
	public String name;

	@JsonInclude(Include.NON_NULL)
	public String uuid;

	@JsonInclude(Include.NON_NULL)
	public String id;

	@JsonInclude(Include.NON_NULL)
	public String label;

	@JsonInclude(Include.NON_NULL)
	public String text;

	@JsonInclude(Include.NON_NULL)
	public String url;

	@JsonInclude(Include.NON_NULL)
	public String color;

	@JsonInclude(Include.NON_NULL)
	public String detail;

	@JsonInclude(Include.NON_NULL)
	public List<DpEntity> children;

	@JsonInclude(Include.NON_NULL)
	public String createdBy;

	@JsonInclude(Include.NON_NULL)
	public String createdOn;

	@JsonInclude(Include.NON_NULL)
	public String updatedBy;

	@JsonInclude(Include.NON_NULL)
	public String updatedOn;

	@JsonInclude(Include.NON_NULL)
	public String path;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getUuid()
	{
		return uuid;
	}

	public void setUuid(String uuid)
	{
		this.uuid = uuid;
	}

	public List<DpEntity> getChildren()
	{
		return children;
	}

	public void setChildren(List<DpEntity> children)
	{
		this.children = children;
	}

	public String getCreatedBy()
	{
		return createdBy;
	}

	public void setCreatedBy(String createdBy)
	{
		this.createdBy = createdBy;
	}

	public String getCreatedOn()
	{
		return createdOn;
	}

	public void setCreatedOn(String createdOn)
	{
		this.createdOn = createdOn;
	}

	public String getUpdatedBy()
	{
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy)
	{
		this.updatedBy = updatedBy;
	}

	public String getUpdatedOn()
	{
		return updatedOn;
	}

	public void setUpdatedOn(String updatedOn)
	{
		this.updatedOn = updatedOn;
	}

	public String getPath()
	{
		return path;
	}

	public void setPath(String path)
	{
		this.path = path;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getLabel()
	{
		return label;
	}

	public void setLabel(String label)
	{
		this.label = label;
	}

	public String getText()
	{
		return text;
	}

	public void setText(String text)
	{
		this.text = text;
	}

	public String getColor()
	{
		return color;
	}

	public void setColor(String color)
	{
		this.color = color;
	}

	public String getDetail()
	{
		return detail;
	}

	public void setDetail(String detail)
	{
		this.detail = detail;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}
}
