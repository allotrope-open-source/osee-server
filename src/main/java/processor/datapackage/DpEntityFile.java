package processor.datapackage;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class DpEntityFile extends DpEntity
{
	@JsonInclude(Include.NON_NULL)
	public String charset;

	@JsonInclude(Include.NON_NULL)
	public String format;

	@JsonInclude(Include.NON_NULL)
	public String lineSeparator;

	@JsonInclude(Include.NON_NULL)
	public String messageDigest;

	@JsonInclude(Include.NON_NULL)
	public String messageDigestAlgorithm;

	@JsonInclude(Include.NON_NULL)
	public String size;

	@JsonInclude(Include.NON_NULL)
	public String compressed;

	public String url;

	public String getCharset()
	{
		return charset;
	}

	public void setCharset(String charset)
	{
		this.charset = charset;
	}

	public String getFormat()
	{
		return format;
	}

	public void setFormat(String format)
	{
		this.format = format;
	}

	public String getLineSeparator()
	{
		return lineSeparator;
	}

	public void setLineSeparator(String lineSeparator)
	{
		this.lineSeparator = lineSeparator;
	}

	public String getMessageDigest()
	{
		return messageDigest;
	}

	public void setMessageDigest(String messageDigest)
	{
		this.messageDigest = messageDigest;
	}

	public String getMessageDigestAlgorithm()
	{
		return messageDigestAlgorithm;
	}

	public void setMessageDigestAlgorithm(String messageDigestAlgorithm)
	{
		this.messageDigestAlgorithm = messageDigestAlgorithm;
	}

	public String getSize()
	{
		return size;
	}

	public void setSize(String size)
	{
		this.size = size;
	}

	public String getCompressed()
	{
		return compressed;
	}

	public void setCompressed(String compressed)
	{
		this.compressed = compressed;
	}

	@Override
	public String getUrl()
	{
		return url;
	}

	@Override
	public void setUrl(String url)
	{
		this.url = url;
	}

}
