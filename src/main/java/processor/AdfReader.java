package processor;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.allotrope.adf.dd.model.DataDescription;
import org.allotrope.adf.model.AdfFile;
import org.allotrope.adf.service.AdfService;
import org.allotrope.adf.service.AdfServiceFactory;
import org.apache.logging.log4j.Logger;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;

import de.osthus.ambeth.exception.RuntimeExceptionUtil;
import processor.util.RdfUtil;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class AdfReader
{
	public static Map<String, org.apache.jena.rdf.model.Model> graphMap = new HashMap<String, org.apache.jena.rdf.model.Model>();
	private static Logger logger;

	public static org.apache.jena.rdf.model.Model read(Path adfPath, org.apache.jena.rdf.model.Model model, Logger log) throws IOException
	{
		log.info("Reading model from ADF " + adfPath.toAbsolutePath().toString());
		logger = log;
		AdfService adfService = AdfServiceFactory.create();
		try (AdfFile adfFile = adfService.openFile(adfPath, true))
		{
			DataDescription dataDescription = adfFile.getDataDescription();
			org.apache.jena.rdf.model.Model newModel = readModel(dataDescription);
			model.add(newModel);
		}
		catch (Exception e)
		{
			throw RuntimeExceptionUtil.mask(e);
		}

		return model;
	}

	public static Map<String, org.apache.jena.rdf.model.Model> extractDataDescription(Path adfPath, org.slf4j.Logger log)
	{
		graphMap = new HashMap<String, org.apache.jena.rdf.model.Model>();
		log.info("Extracting graphs from data description from ADF " + adfPath.toAbsolutePath().toString());
		AdfService adfService = AdfServiceFactory.create();
		try (AdfFile adfFile = adfService.openFile(adfPath, true))
		{
			Dataset dataset = adfFile.getDataset();

			// handle default graph
			String graphName = RdfUtil.defaultGraph;
			Model tempModel = dataset.getDefaultModel();
			org.apache.jena.rdf.model.Model rereadModel = readModel(tempModel);
			graphMap.put(graphName, rereadModel);
			log.info("Extracted default graph from data description. #triples = " + rereadModel.size());

			// handle named graphs
			Iterator<String> graphNameIterator = dataset.listNames();
			while (graphNameIterator.hasNext())
			{
				graphName = graphNameIterator.next();
				tempModel = dataset.getNamedModel(graphName);
				rereadModel = readModel(tempModel);
				graphMap.put(graphName, rereadModel);
				log.info("Extracted graph from data description: " + graphName + " #triples = " + rereadModel.size());
			}
		}
		catch (Exception e)
		{
			log.info("Exception during graph extraction from ADF " + adfPath.toAbsolutePath().toString());
			e.printStackTrace();
		}

		return graphMap;
	}

	public static org.apache.jena.rdf.model.Model readModel(Model model)
	{
		org.apache.jena.rdf.model.Model newModel = org.apache.jena.rdf.model.ModelFactory.createDefaultModel();
		StmtIterator stmtIterator = model.listStatements();
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			Resource subject = statement.getSubject();
			Property property = statement.getPredicate();
			RDFNode object = statement.getObject();

			org.apache.jena.rdf.model.Resource newSubject;
			org.apache.jena.rdf.model.Property newProperty;
			org.apache.jena.rdf.model.RDFNode newObject;

			if (subject.isURIResource())
			{
				newSubject = newModel.createResource(subject.getURI());
			}
			else
			{
				newSubject = newModel.createResource(new org.apache.jena.rdf.model.AnonId(subject.asNode().getBlankNodeId().getLabelString()));
			}

			newProperty = newModel.createProperty(property.getURI());

			if (object.isURIResource())
			{
				newObject = newModel.createResource(object.asResource().getURI());
			}
			else if (object.isAnon())
			{
				newObject = newModel.createResource(new org.apache.jena.rdf.model.AnonId(object.asNode().getBlankNodeId().getLabelString()));
			}
			else
			{
				String datatypeUri = object.asLiteral().getDatatypeURI();
				if (datatypeUri != null && datatypeUri.contains("string"))
				{
					newObject = newModel.createTypedLiteral(object.asLiteral().getString());
				}
				else if (datatypeUri != null && (datatypeUri.contains("integer")))
				{
					try
					{
						newObject = newModel.createTypedLiteral(BigInteger.valueOf(object.asLiteral().getInt()));
					}
					catch (Exception e)
					{
						logger.error("Found invalid integer value: \"" + object.asLiteral().toString() + "\". Replacing with string in view.");
						e.printStackTrace();
						newObject = newModel.createTypedLiteral("Invalid integer value: \"" + object.asLiteral().toString() + "\"");
					}
				}
				else if (datatypeUri != null && (datatypeUri.contains("int")))
				{
					try
					{
						newObject = newModel.createTypedLiteral(object.asLiteral().getInt());
					}
					catch (Exception e)
					{
						logger.error("Found invalid int value: \"" + object.asLiteral().toString() + "\". Replacing with string in view.");
						e.printStackTrace();
						newObject = newModel.createTypedLiteral("Invalid int value: \"" + object.asLiteral().toString() + "\"");
					}
				}
				else if (datatypeUri != null && (datatypeUri.contains("decimal")))
				{
					try
					{
						newObject = newModel.createTypedLiteral(BigDecimal.valueOf(object.asLiteral().getDouble()));
					}
					catch (Exception e)
					{
						logger.error("Found invalid decimal value: \"" + object.asLiteral().toString() + "\". Replacing with string in view.");
						e.printStackTrace();
						newObject = newModel.createTypedLiteral("Invalid decimal value: \"" + object.asLiteral().toString() + "\"");
					}
				}
				else if (datatypeUri != null && datatypeUri.contains("double"))
				{
					try
					{
						newObject = newModel.createTypedLiteral(object.asLiteral().getDouble());
					}
					catch (Exception e)
					{
						logger.error("Found invalid double value: \"" + object.asLiteral().toString() + "\". Replacing with string in view.");
						e.printStackTrace();
						newObject = newModel.createTypedLiteral("Invalid double value: \"" + object.asLiteral().toString() + "\"");
					}
				}
				else if (datatypeUri != null && datatypeUri.contains("float"))
				{
					try
					{
						newObject = newModel.createTypedLiteral(object.asLiteral().getFloat());
					}
					catch (Exception e)
					{
						logger.error("Found invalid float value: \"" + object.asLiteral().toString() + "\". Replacing with string in view.");
						e.printStackTrace();
						newObject = newModel.createTypedLiteral("Invalid float value: \"" + object.asLiteral().toString() + "\"");
					}
				}
				else if (datatypeUri != null && datatypeUri.contains("long"))
				{
					try
					{
						newObject = newModel.createTypedLiteral(object.asLiteral().getLong());
					}
					catch (Exception e)
					{
						logger.error("Found invalid long value: \"" + object.asLiteral().toString() + "\". Replacing with string in view.");
						e.printStackTrace();
						newObject = newModel.createTypedLiteral("Invalid long value: \"" + object.asLiteral().toString() + "\"");
					}
				}
				else if (datatypeUri != null && datatypeUri.contains("boolean"))
				{
					try
					{
						newObject = newModel.createTypedLiteral(object.asLiteral().getBoolean());
					}
					catch (Exception e)
					{
						logger.error("Found invalid boolean value: \"" + object.asLiteral().toString() + "\". Replacing with string in view.");
						e.printStackTrace();
						newObject = newModel.createTypedLiteral("Invalid boolean value: \"" + object.asLiteral().toString() + "\"");
					}
				}
				else if (datatypeUri != null && datatypeUri.contains("dateTime"))
				{
					String literalValue = object.asLiteral().getString();
					if (literalValue.contains("\"^^"))
					{
						String[] segments = literalValue.split("\\^\\^");
						literalValue = segments[0].substring(1, segments[0].length() - 1); // cut quotes
					}
					try
					{
						Object literalValueAsObject = javax.xml.bind.DatatypeConverter.parseDateTime(literalValue);
						newObject = newModel.createTypedLiteral(literalValueAsObject);
					}
					catch (Exception e)
					{
						logger.error("Found invalid xsd:dateTime: \"" + literalValue.toString() + "\". Replacing with string in view.");
						e.printStackTrace();
						newObject = newModel.createTypedLiteral("Invalid xsd:dateTime: \"" + literalValue.toString() + "\"");
					}
				}
				else
				{
					logger.info("Unsupported datatype: \"" + datatypeUri + "\" of literal value: \"" + object.asLiteral().getString() + "\". Replacing with string in view.");
					newObject = newModel.createTypedLiteral(object.asLiteral().getString());
				}
			}

			newModel.add(newSubject, newProperty, newObject);
		}
		return newModel;
	}
}
