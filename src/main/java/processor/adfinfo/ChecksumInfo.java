package processor.adfinfo;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class ChecksumInfo
{
	public String adfChecksum;
	public String adfChecksumAlgo;
	public String adfHdfChecksum;

	public String getAdfChecksum()
	{
		return adfChecksum;
	}

	public void setAdfChecksum(String adfChecksum)
	{
		this.adfChecksum = adfChecksum;
	}

	public String getAdfChecksumAlgo()
	{
		return adfChecksumAlgo;
	}

	public void setAdfChecksumAlgo(String adfChecksumAlgo)
	{
		this.adfChecksumAlgo = adfChecksumAlgo;
	}

	public String getAdfHdfChecksum()
	{
		return adfHdfChecksum;
	}

	public void setAdfHdfChecksum(String adfHdfChecksum)
	{
		this.adfHdfChecksum = adfHdfChecksum;
	}
}
