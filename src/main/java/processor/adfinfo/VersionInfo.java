package processor.adfinfo;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class VersionInfo
{
	public String adfVersion;
	public String adfLibVersion;
	public String minFileVersion;
	public String minLibVersionReading;
	public String minLibVersionWriting;

	public String getAdfVersion()
	{
		return adfVersion;
	}

	public void setAdfVersion(String adfVersion)
	{
		this.adfVersion = adfVersion;
	}

	public String getAdfLibVersion()
	{
		return adfLibVersion;
	}

	public void setAdfLibVersion(String adfLibVersion)
	{
		this.adfLibVersion = adfLibVersion;
	}

	public String getMinFileVersion()
	{
		return minFileVersion;
	}

	public void setMinFileVersion(String minFileVersion)
	{
		this.minFileVersion = minFileVersion;
	}

	public String getMinLibVersionReading()
	{
		return minLibVersionReading;
	}

	public void setMinLibVersionReading(String minLibVersionReading)
	{
		this.minLibVersionReading = minLibVersionReading;
	}

	public String getMinLibVersionWriting()
	{
		return minLibVersionWriting;
	}

	public void setMinLibVersionWriting(String minLibVersionWriting)
	{
		this.minLibVersionWriting = minLibVersionWriting;
	}
}
