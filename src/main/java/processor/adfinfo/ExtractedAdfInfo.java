package processor.adfinfo;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class ExtractedAdfInfo
{
	public HdfStructure hdfStructure;
	public DdStructure ddStructure;
	public AdfInfo adfInfo;
	public VersionInfo versionInfo;
	public ChecksumInfo checksumInfo;

	public HdfStructure getHdfStructure()
	{
		return hdfStructure;
	}

	public void setHdfStructure(HdfStructure hdfStructure)
	{
		this.hdfStructure = hdfStructure;
	}

	public DdStructure getDdStructure()
	{
		return ddStructure;
	}

	public void setDdStructure(DdStructure ddStructure)
	{
		this.ddStructure = ddStructure;
	}

	public AdfInfo getAdfInfo()
	{
		return adfInfo;
	}

	public void setAdfInfo(AdfInfo adfInfo)
	{
		this.adfInfo = adfInfo;
	}

	public VersionInfo getVersionInfo()
	{
		return versionInfo;
	}

	public void setVersionInfo(VersionInfo versionInfo)
	{
		this.versionInfo = versionInfo;
	}

	public ChecksumInfo getChecksumInfo()
	{
		return checksumInfo;
	}

	public void setChecksumInfo(ChecksumInfo checksumInfo)
	{
		this.checksumInfo = checksumInfo;
	}
}
