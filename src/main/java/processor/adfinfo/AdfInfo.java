package processor.adfinfo;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class AdfInfo
{
	public String id;
	public String fileId;
	public String hashHex;
	public String readonly;
	public String closed;
	public String updateable;
	public String hashActive;
	public String auditActive;
	public String ddInfo;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getHashHex()
	{
		return hashHex;
	}

	public void setHashHex(String hashHex)
	{
		this.hashHex = hashHex;
	}

	public String getReadonly()
	{
		return readonly;
	}

	public void setReadonly(String readonly)
	{
		this.readonly = readonly;
	}

	public String getClosed()
	{
		return closed;
	}

	public void setClosed(String closed)
	{
		this.closed = closed;
	}

	public String getUpdateable()
	{
		return updateable;
	}

	public void setUpdateable(String updateable)
	{
		this.updateable = updateable;
	}

	public String getHashActive()
	{
		return hashActive;
	}

	public void setHashActive(String hashActive)
	{
		this.hashActive = hashActive;
	}

	public String getAuditActive()
	{
		return auditActive;
	}

	public void setAuditActive(String auditActive)
	{
		this.auditActive = auditActive;
	}

	public String getDdInfo()
	{
		return ddInfo;
	}

	public void setDdInfo(String ddInfo)
	{
		this.ddInfo = ddInfo;
	}

	public String getFileId()
	{
		return fileId;
	}

	public void setFileId(String fileId)
	{
		this.fileId = fileId;
	}
}
