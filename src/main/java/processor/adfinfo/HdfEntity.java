package processor.adfinfo;

import java.util.HashMap;
import java.util.List;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class HdfEntity
{
	public String name;
	public HashMap<String, String> attribute2Value;
	public List<HdfEntity> hdfEntities;
	public String rank;
	public String datatype;
	public List<String> chunkSizes;
	public List<String> currentSizes;
	public String type; // dataset or group

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public HashMap<String, String> getAttribute2Value()
	{
		return attribute2Value;
	}

	public void setAttribute2Value(HashMap<String, String> attribute2Value)
	{
		this.attribute2Value = attribute2Value;
	}

	public String getRank()
	{
		return rank;
	}

	public void setRank(String rank)
	{
		this.rank = rank;
	}

	public String getDatatype()
	{
		return datatype;
	}

	public void setDatatype(String datatype)
	{
		this.datatype = datatype;
	}

	public List<String> getChunkSizes()
	{
		return chunkSizes;
	}

	public void setChunkSizes(List<String> chunkSizes)
	{
		this.chunkSizes = chunkSizes;
	}

	public List<String> getCurrentSizes()
	{
		return currentSizes;
	}

	public void setCurrentSizes(List<String> currentSizes)
	{
		this.currentSizes = currentSizes;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public List<HdfEntity> getHdfEntities()
	{
		return hdfEntities;
	}

	public void setHdfEntities(List<HdfEntity> hdfEntities)
	{
		this.hdfEntities = hdfEntities;
	}
}
