package processor.adfinfo;

import java.util.HashMap;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class DdStructure
{
	public HashMap<String, String> graphUri2Size;

	public HashMap<String, String> getGraphUri2Size()
	{
		return graphUri2Size;
	}

	public void setGraphUri2Size(HashMap<String, String> graphUri2Size)
	{
		this.graphUri2Size = graphUri2Size;
	}
}
