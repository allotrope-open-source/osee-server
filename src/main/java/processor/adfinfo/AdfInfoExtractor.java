package processor.adfinfo;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.allotrope.adf.hdf5.model.HDF5Dataset;
import org.allotrope.adf.hdf5.model.HDF5Entity;
import org.allotrope.adf.hdf5.model.HDF5Group;
import org.allotrope.adf.model.AdfFile;
import org.allotrope.adf.model.AdfFileInternal;
import org.allotrope.adf.service.AdfService;
import org.allotrope.adf.service.AdfServiceFactory;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.logging.log4j.Logger;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class AdfInfoExtractor
{

	public static ExtractedAdfInfo extractFrom(Path path, Model modelWithAdf, Model externalResources, Logger log)
	{
		log.info("Extracting adf info...");
		ExtractedAdfInfo extractedAdfInfo = new ExtractedAdfInfo();
		AdfInfo adfInfo = new AdfInfo();
		VersionInfo versionInfo = new VersionInfo();
		ChecksumInfo checksumInfo = new ChecksumInfo();
		DdStructure ddStructure = new DdStructure();
		HdfStructure hdfStructure = new HdfStructure();

		try
		{
			String id = StringUtils.EMPTY;
			String hash = StringUtils.EMPTY;
			String canUpdate = StringUtils.EMPTY;
			String isAuditTrailActive = StringUtils.EMPTY;
			String isClosed = StringUtils.EMPTY;
			String isHashActive = StringUtils.EMPTY;
			String isReadonly = StringUtils.EMPTY;
			Map<String, Long> graphUrisToSizes = new HashMap<String, Long>();
			String ddInfo = StringUtils.EMPTY;
			String adfVersion = StringUtils.EMPTY;
			String adfLibVersion = StringUtils.EMPTY;
			String minFileVersion = StringUtils.EMPTY;
			String minLibVersionReading = StringUtils.EMPTY;
			String minLibVersionWriting = StringUtils.EMPTY;
			String adfChecksum = StringUtils.EMPTY;
			String adfFileId = StringUtils.EMPTY;
			String adfChecksumAlgo = StringUtils.EMPTY;
			String adfHdfChecksum = StringUtils.EMPTY;

			AdfService adfService = AdfServiceFactory.create();
			try
			{
				byte[] hashValue = adfService.readHashValue(path);
				char[] hashValueAsHex = Hex.encodeHex(hashValue);
				hash = new String(hashValueAsHex);
				adfInfo.setHashHex(hash);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

			try (AdfFile adfFile = adfService.openFile(path, true))
			{
				id = adfFile.getId();
				adfInfo.setId(id);

				canUpdate = String.valueOf(adfFile.canUpdate());
				adfInfo.setUpdateable(canUpdate);

				isAuditTrailActive = String.valueOf(adfFile.isAuditTrailActive());
				adfInfo.setAuditActive(isAuditTrailActive);

				isClosed = String.valueOf(adfFile.isClosed());
				adfInfo.setClosed(isClosed);

				isHashActive = String.valueOf(adfFile.isHashComputationActive());
				adfInfo.setHashActive(isHashActive);

				isReadonly = String.valueOf(adfFile.isReadOnly());
				adfInfo.setReadonly(isReadonly);

				HashMap<String, String> uri2size = new HashMap<String, String>();
				long size = modelWithAdf.size();
				graphUrisToSizes.put("default", size);
				uri2size.put("default", String.valueOf(size));

				Iterator<String> names = adfFile.getDataset().listNames();
				while (names.hasNext())
				{
					String graphName = names.next();
					com.hp.hpl.jena.rdf.model.Model model = adfFile.getDataset().getNamedModel(graphName);
					size = model.size();
					graphUrisToSizes.put(graphName, size);
					uri2size.put(graphName, String.valueOf(size));
				}

				ddStructure.setGraphUri2Size(uri2size);
				extractedAdfInfo.setDdStructure(ddStructure);
				adfFile.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

			try (AdfFileInternal adfFileInternal = (AdfFileInternal) adfService.openFile(path, true))
			{
				try (HDF5Group rootGroup = (HDF5Group) adfFileInternal.getRootGroup())
				{
					try
					{
						adfVersion = rootGroup.retrieveAttribute("adf-version");
						versionInfo.setAdfVersion(adfVersion);
					}
					catch (Exception e)
					{
						// e.printStackTrace();
					}
					try
					{
						adfLibVersion = rootGroup.retrieveAttribute("adf-lib-version");
						versionInfo.setAdfLibVersion(adfLibVersion);
					}
					catch (Exception e)
					{
						// e.printStackTrace();
					}
					try
					{
						minFileVersion = rootGroup.retrieveAttribute("min-file-version");
						versionInfo.setMinFileVersion(minFileVersion);
					}
					catch (Exception e)
					{
						// e.printStackTrace();
					}
					try
					{
						minLibVersionReading = rootGroup.retrieveAttribute("min-lib-version-for-reading");
						versionInfo.setMinLibVersionReading(minLibVersionReading);
					}
					catch (Exception e)
					{
						// e.printStackTrace();
					}
					try
					{
						minLibVersionWriting = rootGroup.retrieveAttribute("min-lib-version-for-writing");
						versionInfo.setMinLibVersionWriting(minLibVersionWriting);
					}
					catch (Exception e)
					{
						// e.printStackTrace();
					}
					try
					{
						adfChecksum = rootGroup.retrieveAttribute("ADF_CHECKSUM");
						checksumInfo.setAdfChecksum(adfChecksum);
					}
					catch (Exception e)
					{
						// e.printStackTrace();
					}
					try
					{
						adfFileId = rootGroup.retrieveAttribute("adf-file-id");
						adfInfo.setFileId(adfFileId);
					}
					catch (Exception e)
					{
						// e.printStackTrace();
					}
					try
					{
						adfChecksumAlgo = rootGroup.retrieveAttribute("adf-hdf-checksum-algorithm");
						checksumInfo.setAdfChecksumAlgo(adfChecksumAlgo);
					}
					catch (Exception e)
					{
						// e.printStackTrace();
					}
					try
					{
						adfHdfChecksum = rootGroup.retrieveAttribute("checksum-adf-hdf-2.0");
						checksumInfo.setAdfHdfChecksum(adfHdfChecksum);
					}
					catch (Exception e)
					{
						// e.printStackTrace();
					}

					extractedAdfInfo.setAdfInfo(adfInfo);
					extractedAdfInfo.setChecksumInfo(checksumInfo);
					extractedAdfInfo.setVersionInfo(versionInfo);

					System.out.println("HDF structure:");
					System.out.println(rootGroup.getName());

					hdfStructure.setName(rootGroup.getName());
					List<HdfEntity> hdfEntities = new ArrayList<HdfEntity>();

					String[] names = rootGroup.listNames();
					int level = 0;
					for (int i = 0; i < names.length; i++)
					{
						String name = names[i];
						String indentation = "";
						for (int j = 0; j < level; j++)
						{
							indentation = indentation + " ";
						}
						indentation = indentation + "--> ";
						System.out.println(indentation + name);
						try (HDF5Entity hdf5entity = rootGroup.open(name))
						{
							hdfEntities = writeHdf5Structure(hdf5entity, level, hdfEntities);
							hdf5entity.close();
						}
						catch (Exception e)
						{
							// e.printStackTrace();
						}
					}
					hdfStructure.setHdfEntities(hdfEntities);
					extractedAdfInfo.setHdfStructure(hdfStructure);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

			if (!graphUrisToSizes.isEmpty())
			{
				int i = 0;
				for (Entry<String, Long> entry : graphUrisToSizes.entrySet())
				{
					String graphUri = entry.getKey();
					Long size = entry.getValue();
					ddInfo = ddInfo + graphUri + " [" + size + "]";
					i++;
					if (i < graphUrisToSizes.size())
					{
						ddInfo = ddInfo + ", ";
					}
				}
			}

			System.out.println("ADF id: \"" + id + "\" hash hex: \"" + hash + "\" readonly: \"" + isReadonly + "\" is closed: \"" + isClosed + "\" can be updated: \"" + canUpdate
					+ "\" hash active: \"" + isHashActive + "\" audit active: \"" + isAuditTrailActive + "\" data description: \"" + ddInfo + "\"");

			System.out.println("ADF version: \"" + adfVersion + "\" lib version: \"" + adfLibVersion + "\" min file version: \"" + minFileVersion + "\" min lib version for reading: \""
					+ minLibVersionReading + "\" min lib version for writing: \"" + minLibVersionWriting + "\"");

			System.out.println("ADF HDF file id: \"" + adfFileId + "\" adf checksum: \"" + adfChecksum + "\" checksum algo: \"" + adfChecksumAlgo + "\" adf hdf checksum: \"" + adfHdfChecksum + "\"");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return extractedAdfInfo;
	}

	private static List<HdfEntity> writeHdf5Structure(HDF5Entity hdf5entity, int level, List<HdfEntity> hdfEntities)
	{
		HdfEntity hdfEntity = new HdfEntity();
		if (hdf5entity instanceof HDF5Group)
		{
			HDF5Group hdf5group = (HDF5Group) hdf5entity;
			hdfEntity.setType("group");
			hdfEntity.setName(hdf5group.getName());
			level++;
			String[] names = hdf5group.listNames();
			List<HdfEntity> children = new ArrayList<HdfEntity>();
			for (int i = 0; i < names.length; i++)
			{
				String name = names[i];
				String indentation = "";
				for (int j = 0; j < level; j++)
				{
					indentation = indentation + "  ";
				}
				indentation = indentation + "--> GROUP ";
				System.out.println(indentation + name);

				String[] attributeNames = hdf5group.getAttributeNames();
				if (attributeNames != null && attributeNames.length > 0)
				{
					HashMap<String, String> attributes2Values = new HashMap<String, String>();
					level++;
					indentation = "";
					for (int j = 0; j < level; j++)
					{
						indentation = indentation + "  ";
					}
					indentation = indentation + "--> ";
					for (int k = 0; k < attributeNames.length; k++)
					{
						String attributeName = attributeNames[k];
						String attributeValue = StringUtils.EMPTY;
						try
						{
							Object o = hdf5group.retrieveAttributeValue(attributeName);
							if (o instanceof String)
							{
								attributeValue = (String) o;
							}
							else if (o instanceof Integer)
							{
								attributeValue = String.valueOf((int) o);
							}
							else if (o instanceof Long)
							{
								attributeValue = String.valueOf((long) o);
							}
							else
							{
								attributeValue = "unknown data type";
							}
						}
						catch (Exception e)
						{
							// e.printStackTrace();
						}
						System.out.println(indentation + attributeName + ": \"" + attributeValue + "\"");
						attributes2Values.put(attributeName, attributeValue);
					}
					hdfEntity.setAttribute2Value(attributes2Values);
					level--;
				}

				try (HDF5Entity hdf5ChildEntity = hdf5group.open(name))
				{
					children = writeHdf5Structure(hdf5ChildEntity, level, children);
					hdf5ChildEntity.close();
				}
				catch (Exception e)
				{
					// e.printStackTrace();
				}
			}

			if (!children.isEmpty())
			{
				hdfEntity.setHdfEntities(children);
			}
		}
		else if (hdf5entity instanceof HDF5Dataset)
		{
			HDF5Dataset hdf5dataset = (HDF5Dataset) hdf5entity;
			hdfEntity.setType("datset");
			level++;
			String indentation = "";
			for (int j = 0; j < level; j++)
			{
				indentation = indentation + "  ";
			}
			indentation = indentation + "--> DATASET ";

			String name = hdf5dataset.getName();
			hdfEntity.setName(name);

			String numdimensions = String.valueOf(hdf5dataset.getRank());
			hdfEntity.setRank(numdimensions);

			String datatypeName = StringUtils.EMPTY;
			try
			{
				datatypeName = (hdf5dataset.getDataType() != null ? hdf5dataset.getDataType().toString() : "");
				hdfEntity.setDatatype(datatypeName);
			}
			catch (Exception e1)
			{
				// e1.printStackTrace();
			}
			List<String> chunkSizes = new ArrayList<String>();
			if (hdf5dataset.getChunkSizes() != null)
			{
				for (int i = 0; i < hdf5dataset.getChunkSizes().length; i++)
				{
					long size = hdf5dataset.getChunkSizes()[i];
					chunkSizes.add(String.valueOf(size));
				}
			}
			if (!chunkSizes.isEmpty())
			{
				hdfEntity.setChunkSizes(chunkSizes);
			}
			String chunkSizesString = (!chunkSizes.isEmpty() ? StringUtils.join(chunkSizes, ", ") : "");

			List<String> currentSizes = new ArrayList<String>();
			if (hdf5dataset.getCurrentSizes() != null)
			{
				for (int i = 0; i < hdf5dataset.getCurrentSizes().length; i++)
				{
					long size = hdf5dataset.getCurrentSizes()[i];
					currentSizes.add(String.valueOf(size));
				}
			}
			if (!currentSizes.isEmpty())
			{
				hdfEntity.setCurrentSizes(currentSizes);
			}
			String currentSizesString = (!currentSizes.isEmpty() ? StringUtils.join(currentSizes, ", ") : "");
			System.out.println(
					indentation + name + " #dim: \"" + numdimensions + "\" datatype: \"" + datatypeName + "\" chunk sizes: \"" + chunkSizesString + "\" current sizes: \"" + currentSizesString + "\"");

			String[] attributeNames = hdf5dataset.getAttributeNames();
			if (attributeNames != null && attributeNames.length > 0)
			{
				HashMap<String, String> attributes2Values = new HashMap<String, String>();
				level++;
				indentation = "";
				for (int j = 0; j < level; j++)
				{
					indentation = indentation + "  ";
				}
				indentation = indentation + "--> ";
				for (int i = 0; i < attributeNames.length; i++)
				{
					String attributeName = attributeNames[i];
					String attributeValue = StringUtils.EMPTY;
					try
					{
						Object o = hdf5dataset.retrieveAttributeValue(attributeName);
						if (o instanceof String)
						{
							attributeValue = (String) o;
						}
						else if (o instanceof Integer)
						{
							attributeValue = String.valueOf((int) o);
						}
						else if (o instanceof Long)
						{
							attributeValue = String.valueOf((long) o);
						}
						else
						{
							attributeValue = "unknown data type";
						}
					}
					catch (Exception e)
					{
						// e.printStackTrace();
					}
					System.out.println(indentation + attributeName + ": \"" + attributeValue + "\"");
					attributes2Values.put(attributeName, attributeValue);
				}
				hdfEntity.setAttribute2Value(attributes2Values);
			}
		}
		else
		{
			System.out.println("other");
		}

		hdfEntities.add(hdfEntity);
		return hdfEntities;
	}
}
