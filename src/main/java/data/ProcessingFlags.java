package data;

import org.springframework.stereotype.Component;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
@Component
public class ProcessingFlags
{
	private boolean useExample = false;
	private boolean includeAdfApi = false;
	private boolean loadAfo = false;
	private boolean loadAft = false;
	private boolean doLayout = false;
	private boolean isAdfInput = false;
	private boolean visualizeOntology = false;
	private boolean visualizeInstances = false;
	private boolean endpoint = false;
	private String endpointUrl = "";
	private String graphUrl = "";
	private String constructQuery = "";
	private boolean excludeLiterals = false;
	private boolean multiGraph = false;
	private boolean skolemize = false;

	public void reset()
	{
		useExample = false;
		includeAdfApi = false;
		loadAfo = false;
		loadAft = false;
		doLayout = false;
		isAdfInput = false;
		visualizeOntology = false;
		visualizeInstances = false;
		endpoint = false;
		endpointUrl = "";
		graphUrl = "";
		constructQuery = "";
		setExcludeLiterals(false);
		setMultiGraph(false);
		setSkolemize(false);
	}

	public boolean isUseExample()
	{
		return useExample;
	}

	public void setUseExample(boolean useExample)
	{
		this.useExample = useExample;
	}

	public boolean isLoadAfo()
	{
		return loadAfo;
	}

	public void setLoadAfo(boolean loadAfo)
	{
		this.loadAfo = loadAfo;
	}

	public boolean isAdfInput()
	{
		return isAdfInput;
	}

	public void setAdfInput(boolean isAdfInput)
	{
		this.isAdfInput = isAdfInput;
	}

	public boolean isVisualizeOntology()
	{
		return visualizeOntology;
	}

	public void setVisualizeOntology(boolean visualizeOntology)
	{
		this.visualizeOntology = visualizeOntology;
	}

	public boolean isLoadAft()
	{
		return loadAft;
	}

	public void setLoadAft(boolean loadAft)
	{
		this.loadAft = loadAft;
	}

	public boolean isDoLayout()
	{
		return doLayout;
	}

	public void setDoLayout(boolean doLayout)
	{
		this.doLayout = doLayout;
	}

	public boolean isIncludeAdfApi()
	{
		return includeAdfApi;
	}

	public void setIncludeAdfApi(boolean includeAdfApi)
	{
		this.includeAdfApi = includeAdfApi;
	}

	public boolean isVisualizeInstances()
	{
		return visualizeInstances;
	}

	public void setVisualizeInstances(boolean visualizeInstances)
	{
		this.visualizeInstances = visualizeInstances;
	}

	public boolean isEndpoint()
	{
		return endpoint;
	}

	public void setEndpoint(boolean endpoint)
	{
		this.endpoint = endpoint;
	}

	public String getEndpointUrl()
	{
		return endpointUrl;
	}

	public void setEndpointUrl(String endpointUrl)
	{
		this.endpointUrl = endpointUrl;
	}

	public String getGraphUrl()
	{
		return graphUrl;
	}

	public void setGraphUrl(String graphUrl)
	{
		this.graphUrl = graphUrl;
	}

	public boolean isExcludeLiterals()
	{
		return excludeLiterals;
	}

	public void setExcludeLiterals(boolean excludeLiterals)
	{
		this.excludeLiterals = excludeLiterals;
	}

	public boolean isMultiGraph()
	{
		return multiGraph;
	}

	public void setMultiGraph(boolean multiGraph)
	{
		this.multiGraph = multiGraph;
	}

	public boolean isSkolemize()
	{
		return skolemize;
	}

	public void setSkolemize(boolean skolemize)
	{
		this.skolemize = skolemize;
	}

	public String getConstructQuery()
	{
		return constructQuery;
	}

	public void setConstructQuery(String constructQuery)
	{
		this.constructQuery = constructQuery;
	}
}
