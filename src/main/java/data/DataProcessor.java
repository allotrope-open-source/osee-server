package data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
@Component
public class DataProcessor
{
	public static ProcessingFlags processingFlags;

	@Autowired
	public DataProcessor(ProcessingFlags processingFlags)
	{
		DataProcessor.processingFlags = processingFlags;
	}
}
